open Core
open Utils


module RawProgramSynchronizer = struct
  module Source = struct
    module S = Gsdl.SanitizedParsetree
    type t = S.Output.statement list
    type data = string
    type state = S.IdentifierTable.t

    module Tag = struct
      (* dep is simply the filename *)
      type t = string [@@deriving sexp, ord, show, hash]
    end

    let load_file
        (tbl: state)
        (Sync.MkDependency (filename, data):(Tag.t, string) Sync.dependency) : (state * data, Error.t) Comp.t =
      let open Comp.Syntax in
      Comp.return @@ (tbl, data)

    let of_dependencies  (tbl:state) 
        (deps: (Tag.t, data) Sync.dependency list) : (state * t option, Error.t) Comp.t =
      let open Comp.Syntax in
      let data = List.map ~f:(fun (Sync.MkDependency (filename, data)) -> (filename,data)) deps  in
      let+ result =
        Comp.suppress_error @@
        Comp.fold_map data
          ~init:tbl ~f:(fun tbl (filename, data) -> Scene_parser.parse_text ~tbl filename data) in
      let tbl = match result with Some (tbl, _) -> tbl | None -> tbl in
      let result = Option.map ~f:(fun (_,b) -> List.concat b) result in
      Comp.return (tbl, result)

    let clean_partial_results (state: state) (_deps:(Tag.t, data) Sync.dependency list) : (state, Error.t) Comp.t
      = Comp.return state

    let delete_result (state: state) (_raw_object: t) : (state, Error.t) Comp.t =
      Comp.return state
  end

  include Sync.File.Make (Source)

end


module CompilationManager = struct

  module S = Gsdl.SanitizedParsetree
  module C = Gsdl.CompiledParsetree

  module Source = struct

    type t = Scene.Object.program * S.Dependencies.t

    type src_tag = RawProgramSynchronizer.ID.t * RawProgramSynchronizer.Timestamp.t

    type src = RawProgramSynchronizer.Source.t

    type dep = Sync.FileTag.ID.t * Sync.FileTag.Timestamp.t

    type state =
      ((Image_synchronizer.Syncable.state * Image_synchronizer.t) *
       (Shaders.BasicShaderSynchronizer.BasicShaderSyncable.state * Shaders.BasicShaderSynchronizer.t) *
       (Geometry_synchronizer.Syncable.state * Geometry_synchronizer.t)) *
      S.IdentifierTable.t * RawProgramSynchronizer.t * Sync.FileTag.t

    let build_from_src ((other, tbl, sync, shader): state) (src: src) : (state * t, Error.t) Comp.t =
      let open Comp.Syntax in
      let+ program = Scene_parser.compile_with_error (tbl,src) in
      let dependencies = S.Dependencies.dependencies_program S.Dependencies.empty src in
      let+ (other, program) = Scene.Object.OfCompiled.of_program other program in
      ret ((other, tbl, sync, shader), (program, dependencies))

    let get_src ((other, _, sync, shader): state) (src_tag, timestamp) : (src option, Error.t) Comp.t =
      RawProgramSynchronizer.get_data src_tag sync

    let get_dependencies ((other, tbl, sync, shader): state) (src:src) : (state * dep list, 'b) Comp.t =
      let shader_files = S.Dependencies.dependencies_program S.Dependencies.empty src
                   |> fun a -> a.shaders |> S.Dependencies.S.to_list in
      let open Comp.Syntax in
      let+ (shader, deps) = Comp.fold_map shader_files ~init:shader ~f:(fun shader shader_file ->
          let open Comp.Syntax in
          let+ (id, shader) = Sync.FileTag.load_data shader_file shader in
          let+ timestamp = Sync.FileTag.get_timestamp id shader in
          ret @@ (shader,(id, timestamp))
        ) in
      ret @@ ((other, tbl, sync,shader), deps)

    let cleanup_resources state elem = Comp.return state

    let is_dep_up_to_date ((other, _,sync,shader): state) (src_tag, timestamp: dep) : (bool, Error.t) Comp.t =
      Sync.FileTag.is_up_to_date src_tag timestamp shader

    let is_src_up_to_date (other, _,sync,shader) (src_tag, timestamp: src_tag) =
      let open Comp.Syntax in
      let+ is_up_to_date = RawProgramSynchronizer.is_up_to_date src_tag timestamp sync in
      if is_up_to_date
      then ret None
      else begin
        let+ new_timestamp = RawProgramSynchronizer.get_timestamp src_tag sync in
        ret @@ Some ((src_tag, new_timestamp))
      end


  end

  include Sync.Dynamic.Make(Source)

end

module SerializationManager = struct

  module Source (* : Sync.Dynamic.SOURCE *) = struct
    type state = Window.graphics_state * CompilationManager.Source.state * CompilationManager.t
    type src_tag = CompilationManager.ID.t * CompilationManager.Timestamp.t
    type src = Scene.Object.program
    type dep = Scene.Object.dep
    type t = Scene.DrawableObject.draw_operation list

    let get_src ((_, _,manager) :state)  ((src_tag, _): src_tag) : (src option, Core.Error.t) Utils.Comp.t =
      let open Comp.Syntax in
      let+ program = CompilationManager.get_data src_tag manager in
      ret (program |> Option.map ~f:(fun (program, _) -> program))

    let get_dependencies ((graphics, ((other, _, _, _) as state), manager): state) (src:src) :
      (state * dep list, Core.Error.t) Utils.Comp.t =
      let open Comp.Syntax in
      let+ deps = Scene.Object.Dependencies.of_program other src in
      ret ((graphics, state, manager), deps)

    let is_src_up_to_date
        (_, _, manager: state) ((src_tag, timestamp): src_tag) : (src_tag option, Core.Error.t) Utils.Comp.t =
      let open Comp.Syntax in
      let+ is_up_to_date = CompilationManager.is_up_to_date src_tag timestamp manager in
      if is_up_to_date
      then ret None
      else begin
        let+ new_timestamp = CompilationManager.get_timestamp src_tag manager in
        ret @@ Some ((src_tag, new_timestamp))
      end

    let is_dep_up_to_date
        ((_, (((_, image_sync), (_, shader_sync), (_, mesh_sync)), _, _, _), _): state)
        (dep:dep) : (bool, Core.Error.t) Utils.Comp.t =
      match dep with
      | Scene.Object.TextureDep (id, ts) ->
        Image_synchronizer.is_up_to_date id ts image_sync
      | Scene.Object.ShaderDep (id, ts) ->
        Shaders.BasicShaderSynchronizer.is_up_to_date id ts shader_sync
      | Scene.Object.MeshDep (id, ts) ->
        Geometry_synchronizer.is_up_to_date id ts mesh_sync
      | Scene.Object.ScreenDep -> Comp.return true

    let cleanup_resources (state: state) (compiled:t) =
      Scene.DrawableObject.Deletion.delete compiled;
      Comp.return state

    let build_from_src
        ((graphics, ((((_, im), (_, sm), (_, mm)), _, _, _)), shader) as state: state)
        (src: src) : (state * t, Core.Error.t) Utils.Comp.t =
      let open Comp.Syntax in
      let+ compiled = Scene.DrawableObject.Compile.compile
          ~width:graphics.width ~height:graphics.height
          ~logical_width:graphics.logical_width ~logical_height:graphics.logical_height
          ~shader_manager:sm ~image_manager:im ~mesh_manager:mm src in
      ret (state, compiled)

  end

  include Sync.Dynamic.Make(Source)
end


