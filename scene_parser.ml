open Core
(* open Scene_lexer *)
open Lexing
open Utils
open Gsdl


module Typing = TypedParsetree.Make (PrettyError.FlycheckOut)
module Sanitized = SanitizedParsetree.Sanitizer (PrettyError.FlycheckOut)
module Compiled = CompiledParsetree.OfTyped


module CustomOut = struct
  module type S = sig val f : string -> unit end

  module Make  (Out: S)  = struct
    let output_error ({loc_start; loc_end}: Gsdl.Location.t) =
      let to_str loc = let open Lexing in Printf.sprintf "%d-%d" loc.pos_lnum (loc.pos_cnum - loc.pos_bol + 1) in
      Printf.ksprintf (fun str ->
          Out.f (to_str loc_start ^ "::" ^ to_str loc_end ^ ": " ^ str)
        )
  end
end

let parse_with_error ?tbl lexbuf =
  let errors = ref [] in
  let (let+) x f = Option.bind x ~f in
  let module Parse = Parse.Make (CustomOut.Make (struct let f str = errors := str :: !errors end)) in
  let module Sanitized =
    SanitizedParsetree.Sanitizer (CustomOut.Make (struct let f str = errors := str :: !errors end)) in
  match
    let tbl = match tbl with Some s->s|None -> SanitizedParsetree.IdentifierTable.init () in
    let+ unsanitized_parsetree = Parse.parse_safe lexbuf in

    let+ sanitized_parsetree =
      Sanitized.sanitize_program tbl unsanitized_parsetree
      |> SanitizedParsetree.Output.extract_program in
    Option.return (tbl, sanitized_parsetree)
  with
  | Some value ->
    Comp.return value
  | None ->
    let result = String.concat ~sep:"\n" (List.rev !errors) in
    Comp.fail (Error.of_string result)

let compile_with_error (tbl, program) =
  let errors = ref [] in
  let module Typing = TypedParsetree.Make (CustomOut.Make (struct let f str = errors := str :: !errors end)) in
  let expanded_program = Typing.type_check_and_expand tbl program in
  match expanded_program with
  | Some program ->
    let open Comp.Syntax in
    let+ compiled_program =
      try
        ret @@ CompiledParsetree.OfTyped.eval_program
          (SanitizedParsetree.IdentifierTable.serialize tbl) program
      with
     en -> Comp.fail (Error.of_exn en)
    in
    Comp.return compiled_program
  | None ->
    let result = String.concat ~sep:"\n" (List.rev !errors) in
    Comp.fail (Error.of_string result)

let parse_text ?tbl filename text =
  try
    let lexbuf = Lexing.from_string text in
    lexbuf.lex_curr_p <- { lexbuf.lex_curr_p with pos_fname = filename };
    parse_with_error ?tbl lexbuf
  with
    Sys_error e -> Comp.fail (Error.of_string e)      
  | Failure f -> Comp.fail (Error.of_string f)

let parse_file ?tbl filename =
  try
  In_channel.with_file filename ~f:(fun inx ->
      let lexbuf = Lexing.from_channel inx in
      lexbuf.lex_curr_p <- { lexbuf.lex_curr_p with pos_fname = filename };
      parse_with_error lexbuf
    )
  with
    Sys_error e -> Comp.fail (Error.of_string e)
  | Failure f -> Comp.fail (Error.of_string f)





