[@@@warning "-33"]
open Core
open Tsdl
open Tsdl_image
open Tgl3
open Utils


type t = {
  window: Sdl.window;
  context: Sdl.gl_context;
  exit_requested: bool;
  resized: bool;
  logical_width: int; logical_height: int;
  width: int; height: int; x: int; y: int;
}

let sdle (e: 'a Sdl.result) =
  Comp.of_result  @@
  match e with
  | Error (`Msg e) -> Result.Error (Error.of_string e)
  | Ok v -> Result.Ok v

let setup_vsync () =
  let open Comp.Syntax in
  match Sdl.gl_set_swap_interval (-1) with
    | Error (`Msg e) ->
      let+ () = Comp.warn (Printf.sprintf "error in setting vsync: %s" e) in
      let+ () = sdle (Sdl.gl_set_swap_interval 1) in
      ret @@ ()
    | Ok v ->  ret @@ v


let set_title graphics str =
  Sdl.set_window_title graphics.window str

let logical_dims graphics = (graphics.logical_width, graphics.logical_height)

let handle_resize graphics ~width:new_w ~height:new_h =
  let logical_width =  graphics.logical_width in
  let logical_height = graphics.logical_height in
  let (x,y,w,h) = if (new_w * logical_height > new_h * logical_width)
    then let viewport_width = new_h * logical_width / logical_height in
      ((new_w - viewport_width) / 2,0, viewport_width,new_h)
    else let viewport_height = new_w * logical_height / logical_width in
      (0,(new_h - viewport_height)/2,new_w,viewport_height)
  in
  Gl.viewport x y w h;
  Gl.scissor x y w h;
  (w,h,x,y)

let init ?logical_dimensions () =
  let logical_width,logical_height =
    match logical_dimensions with | None -> 1920, 1080 | Some dims -> dims in 
  let open Comp.Syntax in
  let+ () = sdle (Sdl.init Sdl.Init.(video)) in
  let+ () = sdle (Sdl.gl_set_attribute Sdl.Gl.context_major_version 3) in
  let+ () = sdle (Sdl.gl_set_attribute Sdl.Gl.context_minor_version 3) in
  let+ () = sdle (Sdl.gl_set_attribute Sdl.Gl.context_profile_mask Sdl.Gl.context_profile_core) in
  (* Todo: add note of what this does*)
  let+ () = sdle (Sdl.gl_set_attribute Sdl.Gl.stencil_size 8) in
  (* enable double buffering *)
  let+ () = sdle (Sdl.gl_set_attribute Sdl.Gl.doublebuffer 1) in
  let () =
    let flags =  Image.Init.(png) in
    assert Poly.(Image.init flags = flags) 
  in
  let flags = let open Sdl.Window in opengl + resizable + (* mouse_capture + *)
                                     (* mouse_focus + *) input_focus (* + input_grabbed *) in
  let+ window = sdle (
      Sdl.create_window
        "Stl-viewer"
        ~w: logical_width
        ~h: logical_height
        flags) in
  let+ context = sdle (Sdl.gl_create_context window) in
  let+ () = sdle (Sdl.gl_make_current window context) in
  let+ () = setup_vsync () in
  (* let () = (Sdl.set_window_grab window true) in *)
  let () = (Sdl.set_window_grab window false) in
  (* enable vsync *)
  (* TODO: make config parameter *)
  ret @@ {window; context; exit_requested=false;
          height=logical_height; width=logical_width;
          logical_height=logical_height; logical_width=logical_width;x=0;y=0;
          resized=false;}


let proc_event graphics (evt: Sdl.event) : (t * _ option, 'b) Comp.t =
  let open Comp.Syntax in
  match (Sdl.Event.get evt Sdl.Event.typ) with
  | typ when typ = Sdl.Event.quit ->
    Comp.return ({graphics with exit_requested = true}, None)
  | typ when typ = Sdl.Event.mouse_button_down ->
    let state = Sdl.Event.get evt Sdl.Event.mouse_button_button in
    let is_left_clicked = Int.(state land Sdl.Button.left > Int.zero)   in
    let+ () =
      if is_left_clicked
      then begin
        let+ _b = sdle (Sdl.show_cursor false) in
        let+ () = sdle (Sdl.set_relative_mouse_mode true) in
        Comp.return ()
      end else Comp.return ()
    in
    Comp.return (graphics, None)
  | typ when typ = Sdl.Event.mouse_button_up ->
    let state = Sdl.Event.get evt Sdl.Event.mouse_button_button in
    let is_left_clicked = Int.(state land Sdl.Button.left > Int.zero)   in
    let+ () =
      if is_left_clicked
      then begin
        let+ _b = sdle (Sdl.show_cursor true) in
        let+ () = sdle (Sdl.set_relative_mouse_mode false) in
        Comp.return ()
      end else Comp.return () in
    Comp.return (graphics, None)
  | typ when typ = Sdl.Event.mouse_motion ->
    let state = Sdl.Event.get evt Sdl.Event.mouse_motion_state in
    let is_left_clicked = Int32.(state land Sdl.Button.lmask > Int32.zero)   in
    if is_left_clicked then
      Comp.return (graphics,
      Some (
        Input.MouseMotion
          (Sdl.Event.get evt Sdl.Event.mouse_motion_xrel, Sdl.Event.get evt Sdl.Event.mouse_motion_yrel)
      ))
    else Comp.return (graphics,None)
  | typ when typ = Sdl.Event.window_event ->
    let width, height,x,y = if Sdl.Event.get evt Sdl.Event.window_event_id = Sdl.Event.window_event_resized then
      begin
        let new_w = Int.of_int32_exn (Sdl.Event.get evt Sdl.Event.window_data1) in
        let new_h = Int.of_int32_exn (Sdl.Event.get evt Sdl.Event.window_data2) in
        handle_resize graphics ~width:new_w ~height:new_h;
      end
      else (graphics.width, graphics.height, graphics.x, graphics.y)
    in
    let resized = (not Int.(width = graphics.width) || not Int.(height = graphics.height)) || graphics.resized in
    Comp.return ({graphics with resized; width; height;x;y}, None)
  | typ when typ = Sdl.Event.key_down ->
    Comp.return (graphics, Input.process_sdl_event
      (Sdl.Event.get evt Sdl.Event.keyboard_keycode)
      Int.(Sdl.Event.get evt Sdl.Event.keyboard_repeat = 1)
      false)
  | typ when typ = Sdl.Event.key_up ->
    Comp.return (graphics, Input.process_sdl_event
      (Sdl.Event.get evt Sdl.Event.keyboard_keycode)
      Int.(Sdl.Event.get evt Sdl.Event.keyboard_repeat = 1)
      true)
  | _ -> Comp.return (graphics, None)

let poll_events graphics =
  let open Comp.Syntax in
  let sdl_evnt = Sdl.Event.create () in
  Comp.loop
    ~init:(([], graphics),true)
    ~cont:(fun (_, cont) -> cont)
    ~fin:(fun ((msgs, graphics),_) -> (graphics, msgs))
    ~f:(fun ((msgs, graphics),_) ->
        if (Sdl.poll_event (Some sdl_evnt))
        then begin
          let+ graphics, o_msg = proc_event graphics sdl_evnt in
          match o_msg with
          | None -> Comp.return ((msgs, graphics), true)
          | Some msg -> Comp.return (((msg :: msgs), graphics), true)
        end
        else Comp.return ((List.rev msgs, graphics), false)
      )

let queue_exit graphics = {graphics with exit_requested = true}

let mouse_state (_graphics:t) =
  let (state, (x,y)) = Sdl.get_mouse_state () in
  (x,y),
  Int32.(state land (of_int_exn Sdl.Button.left) > of_int_exn 0),
  Int32.(state land (of_int_exn Sdl.Button.right) > of_int_exn 0),
  Int32.(state land (of_int_exn Sdl.Button.middle) > of_int_exn 0)

let pre_draw () =
  Gl.disable Gl.scissor_test;
  Gl.clear_color 0.0 0.0 0.0 0.0;
  Gl.clear (Gl.color_buffer_bit lor Gl.depth_buffer_bit lor Gl.stencil_buffer_bit);
  Gl.enable Gl.scissor_test

let swap_window graphics = Sdl.gl_swap_window graphics.window

let post_draw () =
  ()

let main_loop ?(debug=true) ~(init:'a) (graphics:t) ~(f: 'a -> t -> ('a * t, 'c) Comp.t) =
  let open Comp.Syntax in
  Comp.loop ~init:(init, graphics) ~cont:(fun (_, graphics) -> not graphics.exit_requested) ~fin:fst
    ~f:(fun (state,graphics) ->
        pre_draw ();
        let+ state, graphics =  (f state graphics) in
        swap_window graphics;
        post_draw ();
        let+ warnings = Comp.warnings in
        Out_channel.flush Out_channel.stdout;
        let () = if debug
          then begin
            let warnings = (String.concat ~sep:"\n" warnings) in
            if not (String.is_empty warnings)
            then print_endline warnings
            else ()
          end else () in
        let graphics = {graphics with resized=false} in
        ret (state, graphics)
      )

type graphics_state = {width:int; height:int; logical_width: int; logical_height: int; resized:bool; x:int; y:int;}

let to_graphics_state ({resized; logical_width; logical_height; width; height; x;y; _}: t) =
  {logical_width; logical_height; width; height; resized; x; y;}
