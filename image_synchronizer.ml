open Core
open Utils
open Sync

module Tag = struct
  type t = unit [@@deriving show, sexp, ord,hash]
end

  module Syncable = struct
    type t = Image.t
    type state = unit
    type data = string

    module Tag = Tag

    let of_dependencies ((): state) (images: (Tag.t, data) dependency list)
      : (state * t option, Error.t) Comp.t =
      let open Comp.Syntax in
      let result = match images with
        | (MkDependency ((), filename)) :: [] ->
          begin match Image.load_file filename with
            | Ok image -> Some image
            | _ -> None
          end
        | _ -> None in
      ret @@ ((), result)

    let load_file () (MkDependency ((), filename) : (state, data) dependency)  =
      Comp.return ((), filename)


    let clean_partial_results () _ = Comp.return ()

    let delete_result () (_image: t) =
      Comp.return ()

  end

include (RawFile.Make (Syncable))

