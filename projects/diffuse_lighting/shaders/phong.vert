#version 330 core

layout (location = 0) in vec3 vertex_position;
layout (location = 1) in vec3 vertex_normals;

out vec3 intensity;

uniform vec3 light_position;
uniform vec3 light_ambient;
uniform vec3 light_diffuse;
uniform vec3 light_specular;

uniform vec3 material_ambient_reflectivity;
uniform vec3 material_diffuse_reflectivity;
uniform vec3 material_specular_reflectivity;


uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() {
  vec3 t_norm = normalize(mat3(view * model) * vertex_normals);
  vec4 eye_coords =
    view * model * vec4(vertex_position, 1.0);

  vec3 s = normalize(light_position - vec3(eye_coords)); // vector from eye->light
  vec3 v = normalize(-eye_coords.xyz); // vector object-to->eye
  vec3 r = reflect(-s, t_norm); // reflect eye->light on surface normal
  vec3 ambient = light_ambient * material_ambient_reflectivity;
  float s_dot_n = max(dot(s,t_norm), 0.0);
  vec3 diffuse = light_diffuse * material_diffuse_reflectivity * s_dot_n;
  float material_shininess = 5.0;
  vec3 spec = vec3(0.0);
  if(s_dot_n > 0.0)
    spec = light_specular * material_specular_reflectivity *
      pow(max(dot(r,v), 0.0), material_shininess);

  intensity = ambient + diffuse + spec;

  gl_Position =
    projection * view * model *
    vec4(vertex_position, 1.0);
}
