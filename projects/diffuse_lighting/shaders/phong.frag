#version 330 core
in vec3 intensity;

out vec4 frag_color;

void main() {
  frag_color = vec4(intensity, 0.8);
}
