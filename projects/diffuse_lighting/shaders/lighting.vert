#version 330 core

layout (location = 0) in vec3 vertex_position;
layout (location = 1) in vec3 vertex_normals;

out vec3 intensity;

uniform vec3 light_position;
uniform vec3 light_reflectivity;
uniform vec3 light_intensity;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() {
  vec3 t_norm = normalize(mat3(view * model) * vertex_normals);
  vec4 eye_coords =
    view * model * vec4(vertex_position, 1.0);
  vec3 s = normalize(light_position - vec3(eye_coords));
  intensity =
    light_intensity * light_reflectivity *
    max(dot(s,t_norm), 0.0);
  gl_Position =
    projection * view * model *
    vec4(vertex_position, 1.0);
}
