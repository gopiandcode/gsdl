#version 330 core

in vec3 vertex_position;
in vec3 vertex_normals;

out vec3 frag_normal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() {
  frag_normal = vertex_normals;
  gl_Position =
    projection * view * model *
    vec4(vertex_position, 1.0);
}
