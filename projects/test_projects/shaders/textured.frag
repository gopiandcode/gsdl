#version 330 core

in vec2 tex_coords_frag;
out vec4 frag_colour;

uniform sampler2D model_texture;

void main() {
  frag_colour = vec4(texture(model_texture, tex_coords_frag).xyz,1.0);
}
