#version 330 core

layout (location=0) out vec4 albedo;
layout (location=1) out vec4 position;
layout (location=2) out vec4 normal;

in vec2 tex_coords_frag;
in vec3 vertex_position_frag;
in vec3 vertex_normals_frag;

uniform sampler2D model_texture;

void main() {
  position = vec4(vertex_position_frag, 1.0);
  normal = vec4(normalize(vertex_normals_frag), 1.0);
  albedo = texture(model_texture, tex_coords_frag);
}
