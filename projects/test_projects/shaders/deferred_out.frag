#version 330 core
out vec4 frag_color;

in vec2 tex_coords_frag;

uniform sampler2D albedo_colour;
uniform sampler2D position_colour;
uniform sampler2D normal_colour;

uniform vec3 view_position;
uniform vec3 light_position;

void main () {
  vec3 frag_position = texture(position_colour, tex_coords_frag).rgb;
  vec3 normal = texture(normal_colour, tex_coords_frag).rgb;
  vec3 albedo = texture(albedo_colour, tex_coords_frag).rgb;
  float specular = texture(albedo_colour, tex_coords_frag).a;

  vec3 light_direction = normalize(light_position - frag_position);
  vec3 diffuse =  max(dot(normal, light_direction), 0.1) * albedo * vec3(0.9, 0.7, 0.6);

  frag_color = vec4(diffuse, 0.25);
}
