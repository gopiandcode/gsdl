#version 330 core

layout (location = 0) in vec3 vertex_position;
layout (location = 1) in vec3 vertex_colour;

out vec3 colour;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() {
  // move model to position in world
  //vec4 world_position = transform_matrix * vec4(pos, 1.0);
  vec4 world_position = model * vec4(vertex_position, 1.0);
  vec4 relative_pos = view * world_position;
  gl_Position = projection * relative_pos;
  //vec4 relative_position = view_matrix * world_position;
  //gl_Positi on = projection_matrix * relative_position;
  colour = vertex_colour;
}
