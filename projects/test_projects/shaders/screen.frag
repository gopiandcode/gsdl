#version 330 core

in vec2 tex_coords_frag;
out vec4 frag_colour;

uniform sampler2D screen_texture;

void main() {
  vec4 tex_colour = texture(screen_texture, tex_coords_frag);

  if(tex_colour.w <= 0.2) discard;
  frag_colour = vec4(tex_colour.xzy, 0.3);
}
