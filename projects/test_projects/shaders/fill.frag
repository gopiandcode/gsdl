#version 330 core

uniform vec4 fill_colour;
out vec4 frag_colour;


void main() {
  frag_colour = fill_colour;
}


