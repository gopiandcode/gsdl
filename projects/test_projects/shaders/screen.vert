#version 330 core

layout (location = 0) in vec2 vertex_position;
layout (location = 1) in vec2 tex_coords;
out vec2 tex_coords_frag;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() {
  // move model to position in world
  //vec4 world_position = transform_matrix * vec4(pos, 1.0);
  // vec4 world_position = model * vec4(vertex_position, 1.0);
  // vec4 relative_pos = view * world_position;
  gl_Position = model * vec4(vertex_position,0.0,1.0);
  //vec4 relative_position = view_matrix * world_position;
  //gl_Positi on = projection_matrix * relative_position;
  tex_coords_frag = tex_coords;
}
