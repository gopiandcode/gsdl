#version 330 core
in vec2 vertex_position;
in vec2 tex_coords;

out vec2 tex_coords_frag;

void main () {

  tex_coords_frag = tex_coords;
  gl_Position = vec4(vertex_position, 0.0, 1.0);
}
