#version 330 core
layout (location = 0) in vec3 vertex_position;
out vec3 skybox_coords;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

void main () {
  skybox_coords = vertex_position * 2 - 1;

  vec4 world_position = 
    projection * 
    view *
    model *
    vec4(vertex_position, 1.0);

  gl_Position = world_position.xyww;
}
