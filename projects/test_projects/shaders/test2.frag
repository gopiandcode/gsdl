#version 330 core

in vec3 colour;
in vec3 colour_alt;
out vec4 frag_colour;

void main() {
  frag_colour = vec4(0.3, 0.3, 0.8, 0.8 * colour.z);
}
