#version 330 core

in vec2 tex_coords_frag;
out vec4 frag_colour;

void main() {
  frag_colour = texture(model_texture, tex_coords_frag);
}
