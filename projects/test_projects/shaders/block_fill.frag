#version 330 core

out vec4 frag_colour;

uniform vec4 fill_colour;

void main() {
  frag_colour = fill_colour;
}
