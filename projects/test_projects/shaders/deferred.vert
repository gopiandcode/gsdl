#version 330 core

in vec3 vertex_position;
in vec3 vertex_normals;
in vec2 tex_coords;

out vec2 tex_coords_frag;
out vec3 vertex_position_frag;
out vec3 vertex_normals_frag;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() {

  vec4 world_position =
        model * vec4(vertex_position, 1.0);
  vec4 relative_pos =
        view * world_position;

  mat3 normal_matrix = transpose(inverse(mat3(model)));

  tex_coords_frag = tex_coords;
  vertex_position_frag = world_position.xyz;
  vertex_normals_frag = normal_matrix * vertex_normals;
  
  gl_Position = projection * relative_pos;

}
