#version 330 core

in vec3 colour;
in vec3 colour_alt;
out vec4 frag_colour;

void main() {
  frag_colour[i] = vec4(colour_alt.x, 0.1, colour.z * colour_alt.y, colour.z);
}
