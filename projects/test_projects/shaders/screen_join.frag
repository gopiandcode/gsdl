#version 330 core

in vec2 tex_coords_frag;
out vec4 frag_colour;

uniform sampler2D screen_texture1;
uniform sampler2D screen_texture2;
uniform sampler2D screen_texture3;

void main() {
  vec3 tex_colour1 = texture(screen_texture1, tex_coords_frag).rgb;
  float col1 = texture(screen_texture1, tex_coords_frag).a;
  vec3 tex_colour2 = texture(screen_texture2, tex_coords_frag).rgb;
  float col2 = texture(screen_texture2, tex_coords_frag).a;
  vec3 tex_colour3 = texture(screen_texture3, tex_coords_frag).rgb;
  float col3 = texture(screen_texture3, tex_coords_frag).a;

  frag_colour =
    vec4(
         tex_colour1 * col1 +
         tex_colour2 * col2 +
         tex_colour3 * col3,
         1.0
         );
//  frag_colour = vec4(1.0, 1.0, 1.0, 1.0);
}
