#version 330 core
out vec4 frag_colour;
in vec3 skybox_coords;

uniform samplerCube sky_texture;

void main() {
  frag_colour = texture(
                        sky_texture,
                  skybox_coords
            );
}
