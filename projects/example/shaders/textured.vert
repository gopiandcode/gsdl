#version 330 core

layout (location = 0) in vec3 vertex_position;
layout (location = 1) in vec2 tex_coords;

out vec2 tex_coords_frag;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() {

  vec4 world_position = model * vec4(vertex_position, 1.0);
  vec4 relative_pos = view * world_position;
  gl_Position = projection * relative_pos;

  tex_coords_frag = tex_coords;
}
