#version 330 core

uniform vec4 fill_colour;

in vec3 frag_normal;
out vec4 frag_colour;

void main() {
  frag_colour = vec4(
                     normalize(frag_normal) * 0.2
                     + fill_colour.rgb * 0.8,
                     fill_colour.a
                     );
}
