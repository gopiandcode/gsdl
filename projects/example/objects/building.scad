
building_width = 80;
building_depth = 80;
building_height = 180;
window_offset = 5;
window_space = 5;
window_width = 15;
window_height = 10;
window_depth = 3;
window_height_offset = 5;
window_height_space = 8;
column_width = 5;
base_height = 80/3;

width_windows =
     floor((building_width - 2 * window_offset + window_space) /
     (window_width + window_space));

depth_windows =
     floor((building_depth - 2 * window_offset + window_space) /
     (window_width + window_space));

height_windows =
     floor((building_height - 2 * window_height_offset + window_height_space) /
     (window_height + window_height_space));

actual_width_window_space =
     window_space + 
     ((building_width - 2 * window_offset + window_space) -
     (window_width + window_space) * width_windows) / width_windows;

actual_depth_window_space =
     window_space + 
     ((building_depth - 2 * window_offset + window_space) -
     (window_width + window_space) * depth_windows) / depth_windows;

actual_height_window_space =
     window_height_space + 
     ((building_height - 2 * window_height_offset + window_height_space) -
     (window_height + window_height_space) * height_windows) / height_windows;


module window_base(level) {
   for(i = [0:width_windows - 1]) {
        translate([
            building_depth/2 - window_depth/4,
            -building_width/2 +
            window_offset + (window_width + actual_width_window_space) * i
            + window_width /2,
            level])
        cube([window_depth, window_width, window_height], center=true);
        translate([
            -building_depth/2 + window_depth/4,
            -building_width/2 +
            window_offset + (window_width + actual_width_window_space) * i
            + window_width /2,
            level])
        cube([window_depth, window_width, window_height], center=true);
   }

   for(i = [0:depth_windows - 1]) {
        translate([
            -building_depth/2
            + window_offset  + (window_width + actual_depth_window_space) * i + window_width/2,
            -building_width/2 + window_depth/4,
            level])
        cube([window_width, window_depth, window_height], center=true);
        translate([
            -building_depth/2
            + window_offset  + (window_width + actual_depth_window_space) * i + window_width/2,
            building_width/2 - window_depth/4,
            level])
        cube([window_width, window_depth, window_height], center=true);
   }
}
draw_main = true;


if(draw_main)
union() {
difference() {
   cube([building_depth,building_width,building_height], center=true);

   for(i = [0:height_windows - 1]) {
        window_base(
             -building_height/2
             + window_height/2 
             + window_height_offset 
             + (window_height + window_height_space) * i);
   }
for(i = [0:height_windows - 1]) {
     translate([0,0,
                -building_height/2 +
                window_height +
                window_height_offset +
                (window_height + window_height_space) * i + window_height_space/2
                    ])
          difference() {
          cube([building_depth * 1.1,building_width * 1.1, 1], center=true);
          cube([building_depth * 0.95,building_width * 0.95, 2], center=true);
     }

}
   
}

translate([0, 0, building_height/2])
     cube([building_depth + column_width * 2, building_width + column_width *2, 3], center=true);

translate([0, 0, building_height/2 + 3/2 + 5]) difference() {
     cube([building_depth + column_width * 2 - 10, building_width + column_width *2 - 10, 10], center=true);
     cube([building_depth + column_width * 2 - 14, building_width + column_width *2 - 14, 11], center=true);     
}



translate([
               building_depth/2 + column_width/4,
               building_width/2 + column_width/4,
               0
               ])
cube([column_width, column_width, building_height], center=true);

translate([
               -(building_depth/2 + column_width/4),
               building_width/2 + column_width/4,
               0
               ])
cube([column_width, column_width, building_height], center=true);

translate([
               (building_depth/2 + column_width/4),
               -(building_width/2 + column_width/4),
               0
               ])
cube([column_width, column_width, building_height], center=true);

translate([
               -(building_depth/2 + column_width/4),
               -(building_width/2 + column_width/4),
               0
               ])
cube([column_width, column_width, building_height], center=true);

}

translate([0, 0, -building_height/2 - base_height/2]) {
cube([building_depth,building_width,base_height], center=true);

translate([0, 0, base_height/2])
     cube([building_depth + column_width * 2, building_width + column_width *2, 3], center=true);

translate([
               building_depth/2 + column_width/4,
               building_width/2 + column_width/4,
               0
               ])
cube([column_width, column_width, base_height], center=true);


translate([
               -(building_depth/2 + column_width/4),
               building_width/2 + column_width/4,
               0
               ])
cube([column_width, column_width, base_height], center=true);

translate([
               (building_depth/2 + column_width/4),
               -(building_width/2 + column_width/4),
               0
               ])
cube([column_width, column_width, base_height], center=true);

translate([
               -(building_depth/2 + column_width/4),
               -(building_width/2 + column_width/4),
               0
               ])
cube([column_width, column_width, base_height], center=true);

}
