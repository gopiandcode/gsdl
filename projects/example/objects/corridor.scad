
bar_height = 8;
corridor_width = 10;
wall_height= 2 * bar_height + 5;
window_height = 10;
window_width = 10;
wall_length = 200;



for(i=[0:wall_length/5 + 2])
     translate([1,0.9 + i * 5,0])
          cylinder(bar_height, 0.5, 0.5);

translate([1, wall_length + 10, 0.5])
for(i=[0:3])
     translate([2.0 + i * 5,-0.5,0])
          cylinder(bar_height, 0.5, 0.5);

translate([0.5,wall_length + 10 - 1.0,bar_height])
cube([20.5,1.5,0.5]);


translate([0.5,0,bar_height])
cube([1.5,wall_length + 10,0.5]);

cube([corridor_width + 10,wall_length + 10,1]);

translate([0, 0, wall_height])
cube([corridor_width,wall_length,1]);

translate([corridor_width, 0, 0]) {
     for(i=[0:4]) {
          translate([-0.5,5 + i * 40,bar_height + 2]) {
               translate([0,0,window_height])
                    cube([1,window_width,0.5]);

               translate([0.6,0,window_height/2 + 0.25])
                    cube([0.1, window_width, 0.5]);

               translate([0.6,window_width/2 + 0.25,0.0])
                    cube([0.1, 0.5, window_height]);

               translate([-0.0, 0.5, 0.0])
                    cube([2.5, window_width, 0.5]);

          }
     }
     difference() {
          cube([1,wall_length,wall_height]);

          for(i=[0:4]) {
               translate([-0.5,5 + i * 40,bar_height + 2]) {
                    difference() {
                         cube([1.0,window_width,window_height]);

                         translate([0.75,0,window_height/2])
                              cube([0.5, window_width, 1.0]);

                         translate([0.75,window_width/2,0.0])
                              cube([0.5, 1.0, window_height]);
                    }
               };

               difference() {
                    translate([-0.5,21.5 + i * 40,0])
                         cube([1, window_width,wall_height - 3]);

                    translate([0.2,21.5 + i * 40 + 1,0 + 12])
                         cube([0.3, window_width - 2,wall_height - 3 - 13]);

                    translate([0.2,21.5 + i * 40 + 1,0 + 2])
                         cube([0.3, window_width - 2,wall_height - 3 - 10]);

               }
          }
          
     };
};
