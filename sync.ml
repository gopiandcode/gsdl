open Core
open Utils

type ('tag,'data) dependency = MkDependency of 'tag * 'data


module type S = sig
  (* type of the data of interest *)
  type t
  (* type of additional state required *)
  type state
  (* type of tag *)
  module ID : sig
    type t
    val pp: Format.formatter -> t -> unit
    val compare : t -> t -> Core_kernel__.Import.int
    val t_of_sexp : Sexplib0.Sexp.t -> t
    val sexp_of_t : t -> Sexplib0.Sexp.t
    val hash_fold_t : Ppx_hash_lib.Std.Hash.state -> t -> Ppx_hash_lib.Std.Hash.state                           
    val hash : t -> Ppx_hash_lib.Std.Hash.hash_value
  end

  (* given a tag attempt to retrieve the referenced data using the state *)
  val get_data : ID.t -> state -> (t option, 'b) Comp.t

end

module type S_WITH_TIMESTAMP = sig
  include S

  (* type of timestamps used to track whether ids are up to date *)
  module Timestamp : Map.Key

  (* given a tag  retreive a timestamp from the state for tracking last update *)
  val get_timestamp: ID.t -> state -> (Timestamp.t, 'b) Comp.t
  (*  given a tag and timestamp, query whether the timestamp is up to date with the data   *)
  val is_up_to_date: ID.t -> Timestamp.t -> state -> (bool, 'b) Comp.t
end


(** Module to construct a synchronizer (S) given a set of primitives  *)
module Generic = struct

  module type SPEC = sig
    (** data type of interest (i.e shader, something else etc.) *)
    type t
    (** metadata wrapper of datatype of interest  *)
    type elem
    (** external state required to construct data type of interest  *)
    type state
    (** dependnecy specification to construct data type of interest  *)
    module Deps : Map.Key

    (** convert wrapper type to element *)
    val to_data_opt: elem -> t option

    (** computation to build metadata wrapper given dependencies   *)
    val load_data_comp: state -> Deps.t  -> (state * elem, Error.t) Comp.t

    (** computation to update wrapper *)
    val update_elem : state -> Deps.t -> elem -> (state * elem, Error.t) Comp.t

  end

  module Make (Dynamic: SPEC) = struct
    module M = IDMap.Make (Dynamic.Deps)

    module ID = M.ID

    type state = Dynamic.elem M.t
    type t = Dynamic.t

    let init () = Comp.return @@ M.empty

    let get_data id (mapping:state) =
      let result = M.get_data mapping id in
      Comp.return @@ (Dynamic.to_data_opt result)

    let load_data (deps: Dynamic.Deps.t) 
        (state: Dynamic.state)  (mapping: state)
      : (Dynamic.state * M.ID.t * state, Error.t) Comp.t =
      let open Comp.Syntax in
      let state_r = ref state in
      let comp =
        let+ state, elem = (Dynamic.load_data_comp state deps) in
        state_r := state;
        ret @@ elem
      in
      let+ (id, mapping) = M.add_mapping_lazy deps comp mapping in
      ret @@ (!state_r, id, mapping)

    let update_mapping (mapping: state)  (state: Dynamic.state) : (Dynamic.state * state, Error.t) Comp.t =
      M.foldmap_keyed_data_lazy mapping
        state ~f:(fun state id elem ->  Dynamic.update_elem state id elem)
  end

end

(** File Synchonizer *)
module File = struct

  module type SOURCE = sig
    type t
    type data
    type state

    module Tag : sig
      type t
      val compare : t -> t -> Core_kernel__.Import.int
      val t_of_sexp : Sexplib0.Sexp.t -> t
      val sexp_of_t : t -> Sexplib0.Sexp.t
      val hash_fold_t : Ppx_hash_lib.Std.Hash.state -> t -> Ppx_hash_lib.Std.Hash.state                           
    end

    (** load dependent data from file  *)
    val load_file: state -> (Tag.t, string) dependency -> (state * data, Error.t) Comp.t 

    (** construct object from data  *)
    val of_dependencies: state -> (Tag.t, data) dependency list -> (state * t option, Error.t) Comp.t 

    (** clean dependent data if build fails  *)
    val clean_partial_results: state -> (Tag.t, data) dependency list -> (state, Error.t) Comp.t 

    (** delete previous result when dependencies update  *)
    val delete_result: state -> t  -> (state, Error.t) Comp.t 

  end

  module TaggedDataList (Syncable: SOURCE)  = struct
    type t = (Syncable.Tag.t * string) list [@@deriving sexp, ord, hash]
  end


  module type S = functor (Syncable : SOURCE) ->
  sig
    type t
    module ID : sig
      type t
      val pp: Format.formatter -> t -> unit
      val compare : t -> t -> Core_kernel__.Import.int
      val t_of_sexp : Sexplib0.Sexp.t -> t
      val sexp_of_t : t -> Sexplib0.Sexp.t
      val hash_fold_t : Ppx_hash_lib.Std.Hash.state -> t -> Ppx_hash_lib.Std.Hash.state
      val hash : t -> Ppx_hash_lib.Std.Hash.hash_value
    end

    module Timestamp : Map.Key

    val id_to_string: ID.t -> string

    val init : unit -> (t, 'b) Comp.t
    val get_data : ID.t -> t -> (Syncable.t option, 'b) Comp.t
    val get_timestamp: ID.t -> t -> (Timestamp.t, 'b) Comp.t
    val is_up_to_date: ID.t -> Timestamp.t -> t -> (bool, 'b) Comp.t
    val load_data : Syncable.state -> (Syncable.Tag.t * string) list -> t ->
      (Syncable.state * ID.t * t, Error.t) Comp.t
    val update_mapping : Syncable.state -> t -> (Syncable.state * t, Error.t) Comp.t
  end


  module Make : S = functor (Syncable: SOURCE) -> struct
    module M  = IDMap.Make (TaggedDataList (Syncable))

    type fp =
        Valid of {
          access_time: float;
          data: Syncable.t;
        }
      | Invalid of { access_time: float option; }

    type t = fp M.t

    module ID = M.ID

    let id_to_string = M.id_to_string

    module Timestamp = struct
      type t = float option [@@deriving sexp]

      let compare a b =
        Option.compare Float.compare a b

    end

    let init () = Comp.return @@ M.empty

    let get_data id (mapping: t) : (Syncable.t option, 'b) Comp.t =
      let result = M.get_data mapping id in
      match result with
      | Valid {data;_} -> Comp.return @@ Some data
      | Invalid _ -> Comp.return @@ None

    let get_timestamp id (mapping: t) : (Timestamp.t, 'b) Comp.t =
      let result = M.get_data mapping id in
      match result with
      | Valid {access_time; _} ->
        Comp.return @@ (Some access_time)
      | Invalid {access_time; _} ->
        Comp.return @@ access_time

    let is_up_to_date id timestamp (mapping: t) : (bool, 'b) Comp.t =
      let result = M.get_data mapping id in
      match result with
      | Valid {access_time; _} ->
        begin
          Comp.return @@ match timestamp with
          | Some timestamp when Float.(timestamp >= access_time) ->
            true
          | _ ->
            false
        end
      | Invalid { access_time=_access_time; } ->
        Comp.return @@ false

    let find_latest_access_time deps_spec =
      let open Comp.Syntax in
      Comp.fold deps_spec
        ~init:(None)
        ~f:(fun (last_modification_time) (_, filename) ->
            let+ file_status =
              Comp.suppress_error @@ 
              try ret @@ Unix.stat filename with
                Sys_error e -> (Comp.fail (Error.of_string e))
              | Unix.Unix_error (_, e,_) -> (Comp.fail (Error.of_string e))
            in
            let modification_time = Option.map file_status ~f:(fun fs -> fs.st_mtime) in
            let last_modification_time = match last_modification_time, modification_time with
              | None, Some modification_time -> Some modification_time
              | Some prev_modification_time, None -> Some prev_modification_time
              | Some prev_modification_time, Some modification_time ->
                if Float.(prev_modification_time < modification_time)
                then Some modification_time else Some prev_modification_time
              | None, None -> None
            in
            ret @@ (last_modification_time)
          ) 


    let load_file_comp ori_state ?prev_value deps_spec =
      let open Comp.Syntax in
      let partial_results = ref [] in
      let state_r = ref ori_state in
      let+ result =
        Comp.suppress_error @@
        Comp.fold deps_spec
          ~init:([], None)
          ~f:(fun (ls, last_modification_time) (tag, filename) ->
              let+ file_status =
                try ret @@ Unix.stat filename with
                  Sys_error e -> (Comp.fail (Error.of_string e))
                | Unix.Unix_error (_, e,_) -> (Comp.fail (Error.of_string e))
              in
              let modification_time = file_status.st_mtime in
              let+ file_contents =
                try ret (In_channel.read_all filename) with
                  Sys_error e -> (Comp.fail (Error.of_string e))
              in
              let+ (state, data) = Syncable.load_file !state_r (MkDependency (tag, file_contents)) in
              state_r := state;
              (* track the partial results *)
              partial_results := MkDependency (tag,data) :: !partial_results;
              let last_modification_time = match last_modification_time with
                | None -> Some modification_time
                | Some prev_modification_time ->
                  if Float.(prev_modification_time < modification_time)
                  then Some modification_time
                  else Some prev_modification_time
              in
              ret @@ (MkDependency (tag, data) :: ls, last_modification_time)
            ) in
      match result with
      | Some (deps, latest_modification_time) -> 
        let+ (state, data) = Syncable.of_dependencies !state_r deps in
        begin
          match data, latest_modification_time with
          | Some data, Some access_time ->
            let+ state =
              match prev_value with
                None -> ret @@ state
              | Some previous_data -> Syncable.delete_result state previous_data in
            ret (state, Valid {data;access_time})
          | _, access_time ->
            (* building from dependencies failed, clean up partial results *)
            let+ state = Syncable.clean_partial_results !state_r !partial_results in 
            let+ () = Comp.warn "Could not build from dependencies - using placeholder" in
            match prev_value, access_time with
            | Some previous_data, None ->
              let+ state = Syncable.delete_result state previous_data in
              ret @@ (state, Invalid {access_time=None})
            | Some previous_data, Some access_time -> ret @@ (state, Valid {access_time; data=previous_data})
            | None, access_time -> ret @@ (state, Invalid {access_time})
        end
      | None ->
        (* loading failed, clean up partial results *)
        let+ state = Syncable.clean_partial_results !state_r !partial_results in 
        (* store a reference to the invalid data *)
        let+ () = Comp.warn "Could load all dependencies - using placeholder" in
        let+ access_time = find_latest_access_time deps_spec in
        match prev_value, access_time with
        | Some previous_data, None ->
          let+ state = Syncable.delete_result state previous_data in
          ret @@ (state, Invalid {access_time=None})
        | Some previous_data, Some access_time -> ret @@ (state, Valid {access_time; data=previous_data})
        | None, access_time -> ret @@ (state, Invalid {access_time})

    (** loads data from dependencies, returning tag *)
    let load_data ori_state (deps_spec: (Syncable.Tag.t * string) list) (mapping:t)  =
      let open Comp.Syntax in
      let state_r = ref ori_state in
      let comp =
        let+ state, elem = (load_file_comp !state_r deps_spec) in
        state_r := state;
        ret @@ elem in
      let+ (id, mapping) = M.add_mapping_lazy deps_spec comp mapping in
      ret @@ (!state_r, id, mapping)

    (** updates mapping, reloading files if dependencies have been updated *)
    let update_mapping ori_state (mapping:t)  =
      let open Comp.Syntax in
      M.foldmap_keyed_data_lazy mapping ori_state
        ~f:(fun state deps_spec data ->
            let+ latest_access_time = find_latest_access_time deps_spec in
            match latest_access_time, data with
            (* when change has occurred to valid data  *)
            | Some latest_access_time, Valid {access_time;data}
              when Float.(latest_access_time > access_time)     ->
              let+ state, new_data = load_file_comp state
                  ~prev_value:data
                  deps_spec in
              (* we were able to rebuild the value - delete the previous instance *)
              ret @@ (state, new_data)
            (* when change has occurred to data that was never valid  *)
            | Some latest_access_time, Invalid {access_time = Some access_time;_} 
              when Float.(latest_access_time > access_time)  ->
              (* try and build the file again *)
              let+ (state, new_data) = load_file_comp state deps_spec in
              ret @@ (state, new_data)
            (* edge cases - shouldn't occur in practice *)
            | Some _latest_access_time, Invalid {access_time = None;_}  ->
              let+ (state, new_data) = load_file_comp state deps_spec in
              ret @@ (state, new_data)
            (* when unknwon whether files have changed  *)
            | None, _data ->
              let+ state, new_data = load_file_comp state deps_spec in
              ret @@ (state, new_data)
            (* all other cases *)
            | _ -> ret @@ (state, data)
          )

  end
end

(** File Tag Synchonizer (i.e don't read file, just track date)
    tracks file changes without reading contents *)
module FileTag = struct

  module type S = sig
    type t
    module ID : sig
      type t
      val pp: Format.formatter -> t -> unit
      val compare : t -> t -> int
      val t_of_sexp : t -> t
      val sexp_of_t : t -> t
      val hash_fold_t : Ppx_hash_lib.Std.Hash.state -> t -> Ppx_hash_lib.Std.Hash.state
      val hash : t -> Ppx_hash_lib.Std.Hash.hash_value
    end

    module Timestamp : sig
      type t
      val compare : t -> t -> int
      val t_of_sexp : t -> t
      val sexp_of_t : t -> t
    end

    val id_to_string: ID.t -> string
    (* create new file tagger *)
    val init : unit -> (t, 'b) Comp.t
    (* retrieve the timestamp for a element *)
    val get_timestamp: ID.t -> t -> (Timestamp.t, 'b) Comp.t
    (* check if a tag is up to date *)
    val is_up_to_date: ID.t -> Timestamp.t -> t -> (bool, 'b) Comp.t
    val load_data :  string -> t -> (ID.t * t, Error.t) Comp.t
    val update_mapping : t -> (t, Error.t) Comp.t
  end

  module M  = IDMap.Make (String)

  type fp = float option

  type t = fp M.t

  module IDI = M.ID

  module ID = struct
    include IDI
    let hash_fold_t = hash_fold_string
  end
  

  let id_to_string = M.id_to_string

  module Timestamp = struct
    type t = float option [@@deriving sexp, ord, eq,show, hash]
  end

  let init () = Comp.return @@ M.empty

  let get_timestamp id (mapping: t) : (Timestamp.t, 'b) Comp.t =
    let result = M.get_data mapping id in
    match result with
    | Some access_time ->
      Comp.return @@ (Some access_time)
    | access_time ->
      Comp.return @@ access_time

  let is_up_to_date id timestamp (mapping: t) : (bool, 'b) Comp.t =
    let result = M.get_data mapping id in
    match result with
    | Some access_time ->
      begin
        Comp.return @@ match timestamp with
        | Some timestamp when Float.(timestamp >= access_time) ->
          true
        | _ ->
          false
      end
    | None ->
      Comp.return @@ false

  let find_latest_access_time deps_spec =
    let open Comp.Syntax in
    Comp.fold [deps_spec]
      ~init:(None)
      ~f:(fun (last_modification_time) filename ->
          let+ file_status =
            Comp.suppress_error @@ 
            try ret @@ Unix.stat filename with
              Sys_error e -> (Comp.fail (Error.of_string e))
            | Unix.Unix_error (_, e,_) -> (Comp.fail (Error.of_string e))
          in
          let modification_time = Option.map file_status ~f:(fun fs -> fs.st_mtime) in
          let last_modification_time = match last_modification_time, modification_time with
            | None, Some modification_time -> Some modification_time
            | Some prev_modification_time, None -> Some prev_modification_time
            | Some prev_modification_time, Some modification_time ->
              if Float.(prev_modification_time < modification_time)
              then Some modification_time else Some prev_modification_time
            | None, None -> None
          in
          ret @@ (last_modification_time)
        ) 


  let load_file_comp deps_spec =
    let open Comp.Syntax in
    let+ result =
      Comp.suppress_error @@
      Comp.fold [deps_spec]
        ~init:(None)
        ~f:(fun last_modification_time filename ->
            let+ file_status =
              try ret @@ Unix.stat filename with
                Sys_error e -> (Comp.fail (Error.of_string e))
              | Unix.Unix_error (_, e,_) -> (Comp.fail (Error.of_string e))
            in
            let modification_time = file_status.st_mtime in
            (* track the partial results *)
            let last_modification_time = match last_modification_time with
              | None -> Some modification_time
              | Some prev_modification_time ->
                if Float.(prev_modification_time < modification_time)
                then Some modification_time
                else Some prev_modification_time
            in
            ret @@ last_modification_time
          ) in
    Comp.return @@ match result with
    | None -> None
    | Some None -> None
    | Some Some v -> Some v



  (** loads data from dependencies, returning tag *)
  let load_data (deps_spec: string) (mapping:t)  =
    let open Comp.Syntax in
    let comp =
      let+ elem = (load_file_comp deps_spec) in
      ret @@ elem in
    let+ (id, mapping) = M.add_mapping_lazy deps_spec comp mapping in
    ret @@ (id, mapping)

  (** updates mapping, reloading files if dependencies have been updated *)
  let update_mapping (mapping:t)  =
    let open Comp.Syntax in
    M.map_keyed_data_lazy mapping 
      ~f:(fun deps_spec data ->
          let+ latest_access_time = find_latest_access_time deps_spec in
          match latest_access_time, data with
          (* when change has occurred to valid data  *)
          | Some latest_access_time, Some access_time
            when Float.(latest_access_time > access_time)     ->
            (* we were able to rebuild the value - delete the previous instance *)
            ret @@ (Some latest_access_time)
          (* when change has occurred to data that was never valid  *)
          | Some latest_access_time, None ->
            ret @@ (Some latest_access_time)
          (* when unknwon whether files have changed  *)
          | None, _data ->
            ret @@ ( _data)
          (* all other cases *)
          | _ -> ret @@ ( data)
        )

end


(* constructs a synchronizer for two  synchronizable sources *)
module And (A: S_WITH_TIMESTAMP) (B: S_WITH_TIMESTAMP)  = struct

  module type S = sig

    (* type of additional state used in construction *)
    type state

    (* type of data constructed using outputs of A and B *)
    type t

    (* load the combined value from its dependencies *)
    val load_from_dependencies: A.t -> B.t -> state -> (state * t, 'b) Comp.t

  end

  module Make (Joint: S) = struct

    module ID = struct
      type t = A.ID.t * B.ID.t [@@deriving sexp, ord]
    end

    module M = IDMap.Make (ID)


    type elem = {
      (* timestamp of A component *)
      a_timestamp: A.Timestamp.t;
      (* timestamp of B component *)
      b_timestamp: B.Timestamp.t;
      (* possibly data - if built successfully *)
      data: Joint.t option;
    }

    type t = elem M.t

    let init () = Comp.return @@ M.empty

    let get_data id (mapping: t) =
      let result = M.get_data mapping id in
      match result with | {  data; _ } -> Comp.return @@ data

    let load_data_comp (a,b : A.ID.t * B.ID.t) (state_a, state_b, state: A.state * B.state * Joint.state) =
      let open Comp.Syntax in
      let+ a_timestamp  = A.get_timestamp a state_a in
      let+ b_timestamp  = B.get_timestamp b state_b in
      let+ (state, data) =
        let+ a = A.get_data a state_a in
        let+ b = B.get_data b state_b in
        match a,b with
        | Some a, Some b ->
          begin
          let+ result = Comp.suppress_error @@ Joint.load_from_dependencies a b state in
              ret @@ match result with
              | None -> (state, None)
              | Some (state,value) -> (state, Some value)
           end
        | _ -> Comp.return @@ (state, None)
      in
      Comp.return @@ (state, {
        a_timestamp;
        b_timestamp;
        data
      })

    let load_data
        (a,b : A.ID.t * B.ID.t)
        (state_a,state_b,state: A.state * B.state * Joint.state)
        (mapping: t) =
      let open Comp.Syntax in
      let state_r = ref state in
      let comp =
        let+ state, elem = (load_data_comp (a,b) (state_a,state_b,state)) in
        state_r := state;
        ret @@ elem
      in
      let+ (id, mapping) = M.add_mapping_lazy (a,b) comp mapping in
      ret @@ (!state_r, id, mapping)

    let update_mapping (mapping: t) (a_state, b_state, state: A.state * B.state * Joint.state)
      : ((A.state * B.state * Joint.state) * t, Error.t) Comp.t =
      let open Comp.Syntax in
      M.foldmap_keyed_data_lazy mapping (a_state, b_state, state)
        ~f:(fun (a_state, b_state, state) (a,b) ({ a_timestamp; b_timestamp; data } as elem) ->
            let+ a_uptodate = A.is_up_to_date a a_timestamp a_state in
            let+ b_uptodate = B.is_up_to_date b b_timestamp b_state in
            match data, a_uptodate, b_uptodate with
            (* if we are up to date *)
            | _, true, true -> Comp.return ((a_state, b_state, state), elem)
            (* otherwise, re-build *)
            | _ ->
              let+ a_timestamp = A.get_timestamp a a_state in
              let+ b_timestamp = B.get_timestamp b b_state in
              let+ (state, data) =
                let+ a = A.get_data a a_state in
                let+ b = B.get_data b b_state in
                match a,b with
                | Some a, Some b ->
                  begin
                    let+ result = Comp.suppress_error @@ Joint.load_from_dependencies a b state in
                    ret @@ match result with
                    | None -> ((a_state, b_state, state), None)
                    | Some (state,value) -> ((a_state, b_state, state), Some value)
                  end
                | _ -> Comp.return @@ ((a_state, b_state, state), None)
              in
              Comp.return @@ (state, {
                  a_timestamp;
                  b_timestamp;
                  data
                })
          )

  end
end



(** RawFile Synchonizer *)
module RawFile = struct

  module type SOURCE = sig
    type t
    type data
    type state

    module Tag : Map.Key

    val load_file: state -> (Tag.t, string) dependency -> (state * data, Error.t) Comp.t 

    val of_dependencies: state -> (Tag.t, data) dependency list -> (state * t option, Error.t) Comp.t 

    val clean_partial_results: state -> (Tag.t, data) dependency list -> (state, Error.t) Comp.t 

    val delete_result: state -> t  -> (state, Error.t) Comp.t 

  end

  module TaggedDataList (Syncable: SOURCE)  = struct
    type t = (Syncable.Tag.t * string) list [@@deriving sexp, ord]
  end


  module type S = functor (Syncable : SOURCE) ->
  sig
    type t
    module ID : sig
      type t
      val pp: Format.formatter -> t -> unit
      val compare : t -> t -> int
      val t_of_sexp : Sexplib0.Sexp.t -> t
      val sexp_of_t : t -> Sexplib0.Sexp.t
      val hash_fold_t : Ppx_hash_lib.Std.Hash.state -> t -> Ppx_hash_lib.Std.Hash.state
      val hash : t -> Ppx_hash_lib.Std.Hash.hash_value
    end
    module Timestamp : Map.Key

    val id_to_string: ID.t -> string

    val init : unit -> (t, 'b) Comp.t
    val get_data : ID.t -> t -> (Syncable.t option, 'b) Comp.t
    val get_timestamp: ID.t -> t -> (Timestamp.t, 'b) Comp.t
    val is_up_to_date: ID.t -> Timestamp.t -> t -> (bool, 'b) Comp.t
    val load_data : Syncable.state -> (Syncable.Tag.t * string) list -> t ->
      (Syncable.state * ID.t * t, Error.t) Comp.t
    val update_mapping : Syncable.state -> t -> (Syncable.state * t, Error.t) Comp.t
  end


  module Make : S = functor (Syncable: SOURCE) -> struct
    module M  = IDMap.Make (TaggedDataList (Syncable))

    type fp =
        Valid of {
          access_time: float;
          data: Syncable.t;
        }
      | Invalid of { access_time: float option; }

    type t = fp M.t

    module ID = M.ID

    let id_to_string = M.id_to_string

    module Timestamp = struct
      type t = float option [@@deriving sexp]

      let compare a b =
        Option.compare Float.compare a b

    end

    let init () = Comp.return @@ M.empty

    let get_data id (mapping: t) : (Syncable.t option, 'b) Comp.t =
      let result = M.get_data mapping id in
      match result with
      | Valid {data;_} -> Comp.return @@ Some data
      | Invalid _ -> Comp.return @@ None

    let get_timestamp id (mapping: t) : (Timestamp.t, 'b) Comp.t =
      let result = M.get_data mapping id in
      match result with
      | Valid {access_time; _} ->
        Comp.return @@ (Some access_time)
      | Invalid {access_time; _} ->
        Comp.return @@ access_time

    let is_up_to_date id timestamp (mapping: t) : (bool, 'b) Comp.t =
      let result = M.get_data mapping id in
      match result with
      | Valid {access_time; _} ->
        begin
          Comp.return @@ match timestamp with
          | Some timestamp when Float.(timestamp >= access_time) ->
            true
          | _ ->
            false
        end
      | Invalid { access_time=_access_time; } ->
        Comp.return @@ false

    let find_latest_access_time deps_spec =
      let open Comp.Syntax in
      Comp.fold deps_spec
        ~init:(None)
        ~f:(fun (last_modification_time) (_, filename) ->
            let+ file_status =
              Comp.suppress_error @@ 
              try ret @@ Unix.stat filename with
                Sys_error e -> (Comp.fail (Error.of_string e))
              | Unix.Unix_error (_, e,_) -> (Comp.fail (Error.of_string e))
            in
            let modification_time = Option.map file_status ~f:(fun fs -> fs.st_mtime) in
            let last_modification_time = match last_modification_time, modification_time with
              | None, Some modification_time -> Some modification_time
              | Some prev_modification_time, None -> Some prev_modification_time
              | Some prev_modification_time, Some modification_time ->
                if Float.(prev_modification_time < modification_time)
                then Some modification_time else Some prev_modification_time
              | None, None -> None
            in
            ret @@ (last_modification_time)
          ) 


    let load_file_comp ori_state ?prev_value deps_spec =
      let open Comp.Syntax in
      let partial_results = ref [] in
      let state_r = ref ori_state in
      let+ result =
        Comp.suppress_error @@
        Comp.fold deps_spec
          ~init:([], None)
          ~f:(fun (ls, last_modification_time) (tag, filename) ->
              let+ file_status =
                try ret @@ Unix.stat filename with
                  Sys_error e -> (Comp.fail (Error.of_string e))
                | Unix.Unix_error (_, e,_) -> (Comp.fail (Error.of_string e))
              in
              let modification_time = file_status.st_mtime in
              let+ (state, data) = Syncable.load_file !state_r (MkDependency (tag, filename)) in
              state_r := state;
              (* track the partial results *)
              partial_results := MkDependency (tag,data) :: !partial_results;
              let last_modification_time = match last_modification_time with
                | None -> Some modification_time
                | Some prev_modification_time ->
                  if Float.(prev_modification_time < modification_time)
                  then Some modification_time
                  else Some prev_modification_time
              in
              ret @@ (MkDependency (tag, data) :: ls, last_modification_time)
            ) in
      match result with
      | Some (deps, latest_modification_time) -> 
        let+ (state, data) = Syncable.of_dependencies !state_r deps in
        begin
          match data, latest_modification_time with
          | Some data, Some access_time ->
            let+ state =
              match prev_value with
                None -> ret @@ state
              | Some previous_data -> Syncable.delete_result state previous_data in
            ret (state, Valid {data;access_time})
          | _, access_time ->
            (* building from dependencies failed, clean up partial results *)
            let+ state = Syncable.clean_partial_results !state_r !partial_results in 
            let+ () = Comp.warn "Could not build from dependencies - using placeholder" in
            match prev_value, access_time with
            | Some previous_data, None ->
              let+ state = Syncable.delete_result state previous_data in
              ret @@ (state, Invalid {access_time=None})
            | Some previous_data, Some access_time -> ret @@ (state, Valid {access_time; data=previous_data})
            | None, access_time -> ret @@ (state, Invalid {access_time})
        end
      | None ->
        (* loading failed, clean up partial results *)
        let+ state = Syncable.clean_partial_results !state_r !partial_results in 
        (* store a reference to the invalid data *)
        let+ () = Comp.warn "Could load all dependencies - using placeholder" in
        let+ access_time = find_latest_access_time deps_spec in
        match prev_value, access_time with
        | Some previous_data, None ->
          let+ state = Syncable.delete_result state previous_data in
          ret @@ (state, Invalid {access_time=None})
        | Some previous_data, Some access_time -> ret @@ (state, Valid {access_time; data=previous_data})
        | None, access_time -> ret @@ (state, Invalid {access_time})

    (* loads data from dependencies, returning tag *)
    let load_data ori_state (deps_spec: (Syncable.Tag.t * string) list) (mapping:t)  =
      let open Comp.Syntax in
      let state_r = ref ori_state in
      let comp =
        let+ state, elem = (load_file_comp !state_r deps_spec) in
        state_r := state;
        ret @@ elem in
      let+ (id, mapping) = M.add_mapping_lazy deps_spec comp mapping in
      ret @@ (!state_r, id, mapping)

    (* updates mapping, reloading files if dependencies have been updated *)
    let update_mapping ori_state (mapping:t)  =
      let open Comp.Syntax in
      M.foldmap_keyed_data_lazy mapping ori_state
        ~f:(fun state deps_spec data ->
            let+ latest_access_time = find_latest_access_time deps_spec in
            match latest_access_time, data with
            (* when change has occurred to valid data  *)
            | Some latest_access_time, Valid {access_time;data}
              when Float.(latest_access_time > access_time)     ->
              let+ state, new_data = load_file_comp state
                  ~prev_value:data
                  deps_spec in
              (* we were able to rebuild the value - delete the previous instance *)
              ret @@ (state, new_data)
            (* when change has occurred to data that was never valid  *)
            | Some latest_access_time, Invalid {access_time = Some access_time;_} 
              when Float.(latest_access_time > access_time)  ->
              (* try and build the file again *)
              let+ (state, new_data) = load_file_comp state deps_spec in
              ret @@ (state, new_data)
            (* edge cases - shouldn't occur in practice *)
            | Some _latest_access_time, Invalid {access_time = None;_}  ->
              let+ (state, new_data) = load_file_comp state deps_spec in
              ret @@ (state, new_data)
            (* when unknwon whether files have changed  *)
            | None, _data ->
              let+ state, new_data = load_file_comp state deps_spec in
              ret @@ (state, new_data)
            (* all other cases *)
            | _ -> ret @@ (state, data)
          )

  end
end

(** RawFile Synchonizer *)
module UnaryRawFile = struct

  module type SOURCE = sig
    type t
    type data
    type state

    module Tag : Map.Key

    val load_file: state -> (Tag.t, string) dependency -> (state * data, Error.t) Comp.t 

    val of_dependency: state -> (Tag.t, data) dependency -> (state * t option, Error.t) Comp.t 

    val clean_partial_results: state -> (Tag.t, data) dependency list -> (state, Error.t) Comp.t 

    val delete_result: state -> t  -> (state, Error.t) Comp.t 

  end

  module TaggedDataList (Syncable: SOURCE)  = struct
    type t = (Syncable.Tag.t * string)  [@@deriving sexp, ord]
  end


  module type S = functor (Syncable : SOURCE) ->
  sig
    type t
    module ID : sig
      type t
      val pp: Format.formatter -> t -> unit
      val compare : t -> t -> int
      val t_of_sexp : Sexplib0.Sexp.t -> t
      val sexp_of_t : t -> Sexplib0.Sexp.t
      val hash_fold_t : Ppx_hash_lib.Std.Hash.state -> t -> Ppx_hash_lib.Std.Hash.state
      val hash : t -> Ppx_hash_lib.Std.Hash.hash_value
    end
    module Timestamp : Map.Key

    val id_to_string: ID.t -> string

    val init : unit -> (t, 'b) Comp.t
    val get_data : ID.t -> t -> (Syncable.t option, 'b) Comp.t
    val get_timestamp: ID.t -> t -> (Timestamp.t, 'b) Comp.t
    val is_up_to_date: ID.t -> Timestamp.t -> t -> (bool, 'b) Comp.t
    val load_data : Syncable.state -> Syncable.Tag.t * string -> t ->
      (Syncable.state * ID.t * t, Error.t) Comp.t
    val update_mapping : Syncable.state -> t -> (Syncable.state * t, Error.t) Comp.t
  end


  module Make : S = functor (Syncable: SOURCE) -> struct
    module M  = IDMap.Make (TaggedDataList (Syncable))

    type fp =
        Valid of {
          access_time: float;
          data: Syncable.t;
        }
      | Invalid of { access_time: float option; }

    type t = fp M.t

    module ID = M.ID


    let id_to_string = M.id_to_string

    module Timestamp = struct
      type t = float option [@@deriving sexp]

      let compare a b =
        Option.compare Float.compare a b

    end

    let init () = Comp.return @@ M.empty

    let get_data id (mapping: t) : (Syncable.t option, 'b) Comp.t =
      let result = M.get_data mapping id in
      match result with
      | Valid {data;_} -> Comp.return @@ Some data
      | Invalid _ -> Comp.return @@ None

    let get_timestamp id (mapping: t) : (Timestamp.t, 'b) Comp.t =
      let result = M.get_data mapping id in
      match result with
      | Valid {access_time; _} ->
        Comp.return @@ (Some access_time)
      | Invalid {access_time; _} ->
        Comp.return @@ access_time

    let is_up_to_date id timestamp (mapping: t) : (bool, 'b) Comp.t =
      let result = M.get_data mapping id in
      match result with
      | Valid {access_time; _} ->
        begin
          Comp.return @@ match timestamp with
          | Some timestamp when Float.(timestamp >= access_time) ->
            true
          | _ ->
            false
        end
      | Invalid { access_time=_access_time; } ->
        Comp.return @@ false

    let find_latest_access_time deps_spec =
      let open Comp.Syntax in
      Comp.fold [deps_spec]
        ~init:(None)
        ~f:(fun (last_modification_time) (_, filename) ->
            let+ file_status =
              Comp.suppress_error @@ 
              try ret @@ Unix.stat filename with
                Sys_error e -> (Comp.fail (Error.of_string e))
              | Unix.Unix_error (_, e,_) -> (Comp.fail (Error.of_string e))
            in
            let modification_time = Option.map file_status ~f:(fun fs -> fs.st_mtime) in
            let last_modification_time = match last_modification_time, modification_time with
              | None, Some modification_time -> Some modification_time
              | Some prev_modification_time, None -> Some prev_modification_time
              | Some prev_modification_time, Some modification_time ->
                if Float.(prev_modification_time < modification_time)
                then Some modification_time else Some prev_modification_time
              | None, None -> None
            in
            ret @@ (last_modification_time)
          ) 


    let load_file_comp ori_state ?prev_value deps_spec =
      let open Comp.Syntax in
      let partial_results = ref [] in
      let state_r = ref ori_state in
      let+ result =
        Comp.suppress_error @@
        (
              let last_modification_time = None in
              let (tag,filename) = deps_spec in
              let+ file_status =
                try ret @@ Unix.stat filename with
                  Sys_error e -> (Comp.fail (Error.of_string e))
                | Unix.Unix_error (_, e,_) -> (Comp.fail (Error.of_string e))
              in
              let modification_time = file_status.st_mtime in
              let+ (state, data) = Syncable.load_file !state_r (MkDependency (tag, filename)) in
              state_r := state;
              (* track the partial results *)
              partial_results := MkDependency (tag,data) :: !partial_results;
              let last_modification_time = match last_modification_time with
                | None -> Some modification_time
                | Some prev_modification_time ->
                  if Float.(prev_modification_time < modification_time)
                  then Some modification_time
                  else Some prev_modification_time
              in
              ret @@ (MkDependency (tag, data), last_modification_time)
        ) in
      match result with
      | Some (deps, latest_modification_time) -> 
        let+ (state, data) = Syncable.of_dependency !state_r deps in
        begin
          match data, latest_modification_time with
          | Some data, Some access_time ->
            let+ state =
              match prev_value with
                None -> ret @@ state
              | Some previous_data -> Syncable.delete_result state previous_data in
            ret (state, Valid {data;access_time})
          | _, access_time ->
            (* building from dependencies failed, clean up partial results *)
            let+ state = Syncable.clean_partial_results !state_r !partial_results in 
            let+ () = Comp.warn "Could not build from dependencies - using placeholder" in
            match prev_value, access_time with
            | Some previous_data, None ->
              let+ state = Syncable.delete_result state previous_data in
              ret @@ (state, Invalid {access_time=None})
            | Some previous_data, Some access_time -> ret @@ (state, Valid {access_time; data=previous_data})
            | None, access_time -> ret @@ (state, Invalid {access_time})
        end
      | None ->
        (* loading failed, clean up partial results *)
        let+ state = Syncable.clean_partial_results !state_r !partial_results in 
        (* store a reference to the invalid data *)
        let+ () = Comp.warn "Could load all dependencies - using placeholder" in
        let+ access_time = find_latest_access_time deps_spec in
        match prev_value, access_time with
        | Some previous_data, None ->
          let+ state = Syncable.delete_result state previous_data in
          ret @@ (state, Invalid {access_time=None})
        | Some previous_data, Some access_time -> ret @@ (state, Valid {access_time; data=previous_data})
        | None, access_time -> ret @@ (state, Invalid {access_time})

    (* loads data from dependencies, returning tag *)
    let load_data ori_state (deps_spec: (Syncable.Tag.t * string)) (mapping:t)  =
      let open Comp.Syntax in
      let state_r = ref ori_state in
      let comp =
        let+ state, elem = (load_file_comp !state_r deps_spec) in
        state_r := state;
        ret @@ elem in
      let+ (id, mapping) = M.add_mapping_lazy deps_spec comp mapping in
      ret @@ (!state_r, id, mapping)

    (* updates mapping, reloading files if dependencies have been updated *)
    let update_mapping ori_state (mapping:t)  =
      let open Comp.Syntax in
      M.foldmap_keyed_data_lazy mapping ori_state
        ~f:(fun state deps_spec data ->
            let+ latest_access_time = find_latest_access_time deps_spec in
            match latest_access_time, data with
            (* when change has occurred to valid data  *)
            | Some latest_access_time, Valid {access_time;data}
              when Float.(latest_access_time > access_time)     ->
              let+ state, new_data = load_file_comp state
                  ~prev_value:data
                  deps_spec in
              (* we were able to rebuild the value - delete the previous instance *)
              ret @@ (state, new_data)
            (* when change has occurred to data that was never valid  *)
            | Some latest_access_time, Invalid {access_time = Some access_time;_} 
              when Float.(latest_access_time > access_time)  ->
              (* try and build the file again *)
              let+ (state, new_data) = load_file_comp state deps_spec in
              ret @@ (state, new_data)
            (* edge cases - shouldn't occur in practice *)
            | Some _latest_access_time, Invalid {access_time = None;_}  ->
              let+ (state, new_data) = load_file_comp state deps_spec in
              ret @@ (state, new_data)
            (* when unknwon whether files have changed  *)
            | None, _data ->
              let+ state, new_data = load_file_comp state deps_spec in
              ret @@ (state, new_data)
            (* all other cases *)
            | _ -> ret @@ (state, data)
          )

  end
end


module Dynamic = struct

  module type SOURCE = sig

    (** external state *)
    type state 
    (** type of interest *)
    type t 

    (* wrapped source element *)
    type src_tag
    (** source element (compliedparsetree)  *)  
    type src

    (** dependency - (shaderfiles)  *)
    type dep

    val build_from_src: state -> src -> (state * t, Error.t) Comp.t

    val get_src : state -> src_tag -> (src option, Error.t) Comp.t
    
    val get_dependencies : state -> src -> (state * dep list, Error.t) Comp.t

    (** cleans up the resources of the object *)
    val cleanup_resources: state -> t -> (state, Error.t) Comp.t

    (* tests whether a src tag is up to date *)
    val is_src_up_to_date: state -> src_tag -> (src_tag option, Error.t) Comp.t

    (* tests whether the dependencies are up to date *)
    val is_dep_up_to_date: state -> dep -> (bool, Error.t) Comp.t

  end

  module Make (Source: SOURCE) = struct

    open Source

    module ID = struct type t = int[@@deriving sexp, eq, show,ord] end

    module Timestamp = struct type t = int[@@deriving sexp,eq,show,ord] end

    type fp =
      (* source is not up to date - no dependencies *)
      | UnresolvedSource of src_tag * Timestamp.t
      (* source is up to date, but dependencies are not *)
      | UnresolvedDependencies of src_tag * dep list * Timestamp.t
      (* all up to date *)
      | UptoDate of src_tag * dep list * t * Timestamp.t

    module Map = Map.Make(ID)

    type t = {map:fp Map.t; next_id: ID.t }

    let init () =
      Comp.return { map=Map.empty; next_id= 0}

    (** invalidate all data in the table  *)
    let flush_all state (t: t)  =
      let open Comp.Syntax in
      let to_delete = ref [] in
      let map = Map.map t.map  ~f:(fun fp -> match fp with
          | UnresolvedSource (src, ts) -> UnresolvedSource (src, ts)
          | UnresolvedDependencies (src, _, ts) -> UnresolvedSource (src, ts+1)
          | UptoDate (src, _, data, ts) -> to_delete := data :: !to_delete; UnresolvedSource (src, ts+1)) in
      let+ state = Comp.fold ~init:state !to_delete ~f:(fun state data ->
          Source.cleanup_resources state data
        ) in
      ret (state, {t with map})

    (** invalidate a specific component  *)
    let flush state (id: ID.t) (t: t) =
      let open Comp.Syntax in
      let to_delete = ref None in
      let map = Map.update t.map id  ~f:(fun fp -> match fp with
          | None -> assert false
          | Some fp -> match fp with
            | UnresolvedSource (src, ts) -> UnresolvedSource (src, ts)
            | UnresolvedDependencies (src, _, ts) -> UnresolvedSource (src, ts+1)
            | UptoDate (src, _, data, ts) -> to_delete := Some data; UnresolvedSource (src, ts+1)) in
      let+ state = match !to_delete with
        | None -> ret state
        | Some data -> Source.cleanup_resources state data in
      ret (state, {t with map})


    (** load data into compiler manager and return dependency tag *)
    let load_data (src_tag: src_tag)  (t:t) : (t * ID.t, 'b) Comp.t =
      let id, next_id = t.next_id, t.next_id + 1 in
      let map = Map.set t.map ~key:id ~data:(UnresolvedSource (src_tag, 0))  in
      Comp.return @@ ({next_id;map}, id)

    (* retrieve the timestamp corresponding to the id *)
    let get_timestamp (id: ID.t) (t: t) : (Timestamp.t, 'b) Comp.t  =
      let open Comp.Syntax in
      let+ result = Comp.of_option ~error:(Error.of_string "id not found") @@ Map.find t.map id in
      ret @@ match result with
      | UnresolvedSource (_, n)|UnresolvedDependencies (_, _, n)|UptoDate (_, _, _, n) -> n

    (* check whether a timestamp is up to date *)
    let is_up_to_date (id: ID.t) (prev_timestamp: Timestamp.t) (t: t) : (bool, 'b) Comp.t =
      let open Comp.Syntax in
      let+ result = Comp.of_option ~error:(Error.of_string "id not found") @@ Map.find t.map id in
      let latest_timestamp = match result with
          UnresolvedSource (_, n) |UnresolvedDependencies (_, _, n)|UptoDate (_, _, _, n) -> n in
      ret @@ (prev_timestamp >= latest_timestamp)
      
    (* attempt to retrieve the data associated with a tag *)
    let get_data  (id: ID.t) (t: t) : (Source.t option, 'b) Comp.t  =
      let open Comp.Syntax in
      let+ result = Comp.of_option ~error:(Error.of_string "id not found") @@ Map.find t.map id in
      let obj = match result with
        | UptoDate (_,_, src, _) -> Some src | _ -> None in
      ret obj

    (* update the tag *)
    let update (state: state) (t:t) : (state * t, 'b) Comp.t =
      let open Comp.Syntax in
      let to_delete = ref [] in
      Map.to_alist t.map |> Comp.fold  ~init:(state,t) ~f:(fun (state,t) ((key,value): Map.Key.t * fp) ->
          let+ (state, data) = match value with
            | UnresolvedSource (src_tag, ts) ->
              let+ src = get_src state src_tag in
              begin match src with
                | None -> Comp.return @@ (state, value)
                | Some src ->
                  let+ (state,deps) = get_dependencies state src in
                  let+ deps_up_to_date = Comp.fold ~init:true deps ~f:(fun is_up_to_date dep ->
                      if is_up_to_date then Source.is_dep_up_to_date state dep else Comp.return false
                    ) in
                  match deps_up_to_date with
                  | false -> Comp.return (state, UnresolvedDependencies (src_tag, deps, ts + 1))
                  | true ->  begin
                      let+ result = Comp.suppress_error @@ build_from_src state src in
                      match result with
                      | None -> Comp.return (state, UnresolvedDependencies (src_tag, deps, ts + 1))
                      | Some (state, compiled) ->
                        Comp.return (state, UptoDate (src_tag, deps, compiled, ts + 1))
                    end
              end
            | UnresolvedDependencies (src_tag, deps, ts) ->
              let+ src_up_to_date = is_src_up_to_date state src_tag in
              begin match src_up_to_date with
                | Some src_tag ->
                  let+ src = get_src state src_tag in
                  begin match src with
                    | None ->
                      Comp.return @@ (state, UnresolvedSource (src_tag, ts + 1))
                    | Some src ->
                      let+ (state,deps) = get_dependencies state src in
                      let+ deps_up_to_date = Comp.fold ~init:true deps ~f:(fun is_up_to_date dep ->
                          if is_up_to_date then Source.is_dep_up_to_date state dep else Comp.return false
                        ) in
                      match deps_up_to_date with
                      | false ->
                        Comp.return (state, UnresolvedDependencies (src_tag, deps, ts + 1))
                      | true ->  begin
                          let+ result = Comp.suppress_error @@ build_from_src state src in
                          match result with
                          | None ->
                            Comp.return (state, UnresolvedDependencies (src_tag, deps, ts + 1))
                          | Some (state, compiled) ->
                            Comp.return (state, UptoDate (src_tag, deps, compiled, ts + 1))
                        end
                  end
                | None -> 
                  let+ deps_up_to_date = Comp.fold ~init:true deps ~f:(fun is_up_to_date dep ->
                      if is_up_to_date then Source.is_dep_up_to_date state dep else Comp.return false
                  ) in
                  match deps_up_to_date with
                  | false ->
                    Comp.return (state, UnresolvedSource (src_tag, ts + 1))
                  | true -> begin
                      let+ src = get_src state src_tag in
                      match src with
                      | None ->
                        Comp.return (state, UnresolvedSource (src_tag, ts + 1))
                      | Some src ->

                        let+ result = Comp.suppress_error @@ build_from_src state src in
                        match result with
                        | None ->
                          Comp.return (state, UnresolvedDependencies (src_tag, deps, ts))
                        | Some (state, compiled) ->
                          Comp.return (state, UptoDate (src_tag, deps, compiled, ts + 1))
                    end
              end
            | UptoDate (src_tag, deps, compiled, ts) ->
              let+ src_up_to_date = is_src_up_to_date state src_tag in
              begin match src_up_to_date with
                | Some src_tag ->
                  let+ src = get_src state src_tag in
                  begin match src with
                    | None ->
                      to_delete := compiled :: !to_delete;
                      Comp.return @@ (state, UnresolvedSource (src_tag, ts + 1))
                    | Some src ->
                      let+ (state,deps) = get_dependencies state src in
                      let+ deps_up_to_date = Comp.fold ~init:true deps ~f:(fun is_up_to_date dep ->
                          if is_up_to_date then Source.is_dep_up_to_date state dep else Comp.return false
                        ) in
                      match deps_up_to_date with
                      | false ->
                        to_delete := compiled :: !to_delete;
                        Comp.return (state, UnresolvedDependencies (src_tag, deps, ts + 1))
                      | true ->  begin
                          let+ result = Comp.suppress_error @@ build_from_src state src in
                          to_delete := compiled :: !to_delete;
                          match result with
                          | None ->
                            Comp.return (state, UnresolvedDependencies (src_tag, deps, ts + 1))
                          | Some (state, compiled) ->
                            Comp.return (state, UptoDate (src_tag, deps, compiled, ts + 1))
                        end
                  end
                | None -> 
                  let+ deps_up_to_date = Comp.fold ~init:true deps ~f:(fun is_up_to_date dep ->
                      if is_up_to_date then Source.is_dep_up_to_date state dep else Comp.return false
                  ) in
                  match deps_up_to_date with
                  | false ->
                    to_delete := compiled :: !to_delete;
                    Comp.return (state, UnresolvedSource (src_tag, ts + 1))
                  | true ->
                    Comp.return (state, value)
              end
          in
          let+ state = Comp.fold ~init:state !to_delete ~f:(fun state data ->
              Source.cleanup_resources state data
            ) in
          Comp.return @@ (state, {t with map=Map.set t.map ~key ~data})) 




  end

end
