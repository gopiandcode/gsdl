open Core
open Geometry
open Utils

module Syncable = struct
  open Sync

  type t = Mesh.t
  type state = unit
  type data = Mesh.t
  module Tag = struct type t = unit[@@deriving sexp,show,eq,ord] end

  let load_file () (MkDependency ((), file)) : (state * data, Error.t) Comp.t =
    let open Comp.Syntax in
    let+ extension =
      String.split ~on:'.' file
      |> List.last
      |> Comp.of_option ~error:(Error.of_string (Printf.sprintf "invalid filename %s" file)) in
    match extension with
    | "stl" ->
      let+ triangles = Stl_parser.parse_file file in
      ret @@ ((),Mesh.of_triangle_list triangles)
    | "obj" ->
      let+ mesh = Obj_parser.parse_file file in
      let+ mesh = Comp.of_option ~error:(Error.of_string @@ Printf.sprintf "Invalid obj file %s" file) mesh in
      ret @@ ((), mesh)
    | ext ->
      Comp.fail (Error.of_string (Printf.sprintf  
          "unknown file extension %s in filename %s" ext file
        ))

  let of_dependency (state: state) (MkDependency ((), mesh):  (Tag.t, data) dependency) : (state * t option, Error.t) Comp.t =
    Comp.return ((), Some mesh)

  let clean_partial_results ((): state) (_: (Tag.t, data) dependency list) : (state, Error.t) Comp.t = Comp.return ()

  let delete_result ((): state) (_: t) : (state, Error.t) Comp.t = Comp.return ()

end

include Sync.UnaryRawFile.Make(Syncable)
