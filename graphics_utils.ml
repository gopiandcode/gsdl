[@@@warning "-33"]
open Tgl3
open Gg
open Core
open Utils

let m4list_to_array vlist =
  let sz = 16 * (List.length vlist) in
  let arr = (Bigarray.Array1.create Bigarray.Float32 Bigarray.C_layout sz) in
  List.iteri vlist ~f:(fun index mat ->
      let open M4 in
      arr.{index * 16 + 0} <- (e00 mat);      arr.{index * 16 + 1} <- (e01 mat);        arr.{index * 16 + 2} <- (e02 mat);  arr.{index * 16 + 3} <- (e03 mat);
      arr.{index * 16 + 4} <- (e10 mat);      arr.{index * 16 + 5} <- (e11 mat);        arr.{index * 16 + 6} <- (e12 mat);  arr.{index * 16 + 7} <- (e13 mat);
      arr.{index * 16 + 8} <- (e20 mat);      arr.{index * 16 + 9} <- (e21 mat);        arr.{index * 16 + 10} <- (e22 mat); arr.{index * 16 + 11} <- (e23 mat);
      arr.{index * 16 + 12} <- (e30 mat);      arr.{index * 16 + 13} <- (e31 mat);      arr.{index * 16 + 14} <- (e32 mat); arr.{index * 16 + 15} <- (e33 mat);
    );
  arr

let m3list_to_array vlist =
  let sz = 9 * (List.length vlist) in
  let arr = (Bigarray.Array1.create Bigarray.Float32 Bigarray.C_layout sz) in
  List.iteri vlist ~f:(fun index mat ->
      let open M3 in
      arr.{index * 9 + 0} <- (e00 mat);      arr.{index * 9 + 1} <- (e01 mat);      arr.{index * 9 + 2} <- (e02 mat);
      arr.{index * 9 + 3} <- (e10 mat);      arr.{index * 9 + 4} <- (e11 mat);      arr.{index * 9 + 5} <- (e12 mat);
      arr.{index * 9 + 6} <- (e20 mat);      arr.{index * 9 + 7} <- (e21 mat);      arr.{index * 9 + 8} <- (e22 mat);
    );
  arr

let m2list_to_array vlist =
  let sz = 4 * (List.length vlist) in
  let arr = (Bigarray.Array1.create Bigarray.Float32 Bigarray.C_layout sz) in
  List.iteri vlist ~f:(fun index mat ->
      let open M2 in
      arr.{index * 4 + 0} <- (e00 mat);      arr.{index * 4 + 1} <- (e01 mat);
      arr.{index * 4 + 2} <- (e10 mat);      arr.{index * 4 + 3} <- (e11 mat);
    );
  arr

let v4list_to_array vlist =
  let sz = 4 * (List.length vlist) in
  let arr = (Bigarray.Array1.create Bigarray.Float32 Bigarray.C_layout sz) in
  List.iteri vlist ~f:(fun index vec ->
      arr.{index * 4 + 0} <- V4.x vec;
      arr.{index * 4 + 1} <- V4.y vec;
      arr.{index * 4 + 2} <- V4.z vec;
      arr.{index * 4 + 3} <- V4.w vec;
    );
  arr

let v3list_to_array vlist =
  let sz = 3 * (List.length vlist) in
  let arr = (Bigarray.Array1.create Bigarray.Float32 Bigarray.C_layout sz) in
  List.iteri vlist ~f:(fun index vec ->
      arr.{index * 3 + 0} <- V3.x vec;
      arr.{index * 3 + 1} <- V3.y vec;
      arr.{index * 3 + 2} <- V3.z vec;
    );
  arr

let v2list_to_array vlist =
  let sz = 2 * (List.length vlist) in
  let arr = (Bigarray.Array1.create Bigarray.Float32 Bigarray.C_layout sz) in
  List.iteri vlist ~f:(fun index vec ->
      arr.{index * 2 + 0} <- V2.x vec;
      arr.{index * 2 + 1} <- V2.y vec;
    );
  arr

let floatlist_to_array vlist =
  let sz = List.length vlist in
  let arr = (Bigarray.Array1.create Bigarray.Float32 Bigarray.C_layout sz) in
  List.iteri vlist ~f:(fun index vec ->
      arr.{index + 0} <- vec;
    );
  arr

let doublelist_to_array vlist =
  let sz = List.length vlist in
  let arr = (Bigarray.Array1.create Bigarray.Float64 Bigarray.C_layout sz) in
  List.iteri vlist ~f:(fun index vec ->
      arr.{index + 0} <- vec;
    );
  arr

let intlist_to_array vlist =
  let sz = List.length vlist in
  let arr = (Bigarray.Array1.create Bigarray.Int32 Bigarray.C_layout sz) in
  List.iteri vlist ~f:(fun index vec ->
      arr.{index + 0} <- Int32.of_int_exn vec;
    );
  arr

let boollist_to_array vlist =
  let sz = List.length vlist in
  let arr = (Bigarray.Array1.create Bigarray.Int32 Bigarray.C_layout sz) in
  List.iteri vlist ~f:(fun index vec ->
      arr.{index + 0} <- Int32.of_int_exn (Bool.to_int vec);
    );
  arr

let array_of_double value =
  let arr = (Bigarray.Array1.create Bigarray.Float64 Bigarray.C_layout 1) in
  arr.{0} <- value;
  arr

let array_of_float value =
  let arr = (Bigarray.Array1.create Bigarray.Float32 Bigarray.C_layout 1) in
  arr.{0} <- value;
  arr

let array_of_int value =
  let arr = (Bigarray.Array1.create Bigarray.Int32 Bigarray.C_layout 1) in
  arr.{0} <- Int32.of_int_exn value;
  arr

let array_of_bool value =
  let arr = (Bigarray.Array1.create Bigarray.Int32 Bigarray.C_layout 1) in
  arr.{0} <- Int32.of_int_exn (Bool.to_int value);
  arr

let array_of_v2 vec =
  let arr = (Bigarray.Array1.create Bigarray.Float32 Bigarray.C_layout 2) in
  arr.{0} <- V2.x vec;
  arr.{1} <- V2.y vec;
  arr

let array_of_v3 vec =
  let arr = (Bigarray.Array1.create Bigarray.Float32 Bigarray.C_layout 3) in
  arr.{0} <- V3.x vec;
  arr.{1} <- V3.y vec;
  arr.{2} <- V3.z vec;
  arr

let array_of_v4 vec =
  let arr = (Bigarray.Array1.create Bigarray.Float32 Bigarray.C_layout 4) in
  arr.{0} <- V4.x vec;
  arr.{1} <- V4.y vec;
  arr.{2} <- V4.z vec;
  arr.{3} <- V4.w vec;
  arr

let array_of_m2 (mat: m2) = let open M2 in
  let arr = (Bigarray.Array1.create Bigarray.Float32 Bigarray.C_layout 4) in
  arr.{0} <- (e00 mat);      arr.{1} <- (e01 mat);
  arr.{2} <- (e10 mat);      arr.{3} <- (e11 mat);
  arr

let array_of_m3 (mat: m3) = let open M3 in
  let arr = (Bigarray.Array1.create Bigarray.Float32 Bigarray.C_layout 9) in
  arr.{0} <- (e00 mat);      arr.{1} <- (e01 mat);      arr.{2} <- (e02 mat);
  arr.{3} <- (e10 mat);      arr.{4} <- (e11 mat);      arr.{5} <- (e12 mat);
  arr.{6} <- (e20 mat);      arr.{7} <- (e21 mat);      arr.{8} <- (e22 mat);
  arr

let array_of_m4 (mat: m4) = let open M4 in
  let arr =(Bigarray.Array1.create Bigarray.Float32 Bigarray.C_layout 16) in
  arr.{0} <- (e00 mat);      arr.{1} <- (e01 mat);        arr.{2} <- (e02 mat);  arr.{3} <- (e03 mat);
  arr.{4} <- (e10 mat);      arr.{5} <- (e11 mat);        arr.{6} <- (e12 mat);  arr.{7} <- (e13 mat);
  arr.{8} <- (e20 mat);      arr.{9} <- (e21 mat);        arr.{10} <- (e22 mat); arr.{11} <- (e23 mat);
  arr.{12} <- (e30 mat);      arr.{13} <- (e31 mat);      arr.{14} <- (e32 mat); arr.{15} <- (e33 mat);
  arr

let default_quad = Bigarray.Array1.of_array Bigarray.float32 Bigarray.C_layout
    ([| -1.0; -1.0; 1.0; -1.0; -1.0; 1.0; 1.0; -1.0; 1.0; 1.0; -1.0; 1.0; |])

let default_transform = array_of_m4 M4.id

let bigarray_create k len = Bigarray.(Array1.create k c_layout len)

let get_int =
  let a = bigarray_create Bigarray.int32 1 in
  fun f -> f a; Int32.to_int a.{0}

let get_string len f =
  let a = bigarray_create Bigarray.char len in
  f a; Gl.string_of_bigarray a

let get_shader sid e = get_int (Gl.get_shaderiv sid e)

let set_int =
  let a = bigarray_create Bigarray.int32 1 in
  fun (f: _ -> unit) i -> ( let _ = Option.(>>|) (Int32.of_int i) (fun i ->  a.{0} <- i) in f a)

let print_v3 vc =
  let open V3 in
  Printf.printf "vec: [%3f;%3f;%3f]\n" (x vc) (y vc) (z vc) 

let print_v4 vc =
  let open V4 in
  Printf.printf "vec: [%3f;%3f;%3f;%3f]\n" (x vc) (y vc) (z vc) (w vc) 

let print_m3 (mat: m3) =
  let open M3 in
  Printf.printf 
    "mat: [[%3f;%3f;%3f]\n           [%3f;%3f;%3f]\n           [%3f;%3f;%3f]]\n"
    (e00 mat) (e01 mat) (e02 mat)
    (e10 mat) (e11 mat) (e12 mat)
    (e20 mat) (e21 mat) (e22 mat)

let print_m4 (mat: m4) =
  let open M4 in
  let str1 = Printf.sprintf "[%3f;%3f;%3f;%3f]" (e00 mat) (e01 mat) (e02 mat) (e03 mat) in
  let str2 = Printf.sprintf "[%3f;%3f;%3f;%3f]" (e10 mat) (e11 mat) (e12 mat) (e13 mat) in
  let str3 = Printf.sprintf "[%3f;%3f;%3f;%3f]" (e20 mat) (e21 mat) (e22 mat) (e23 mat) in
  let str4 = Printf.sprintf "[%3f;%3f;%3f;%3f]" (e30 mat) (e31 mat) (e32 mat) (e33 mat) in
  Printf.printf 
    "mat: [%s\n       %s\n       %s\n       %s]\n"
    str1 str2 str3 str4

module VBO : sig
  type t
  val create : unit -> t
  val buffer_data: t -> int -> ('a, 'b, Bigarray.c_layout) Bigarray.Array1.t -> unit
  val bind: t -> int -> unit
  val delete: t -> unit
end = struct

  type t = MkT of int 

  let create () : t =
    MkT (get_int (Gl.gen_buffers 1) |> (fun v -> Option.value_exn v))

  let buffer_data ((MkT buffer): t) (buffer_type: int) array =
    Gl.bind_buffer buffer_type buffer;
    Gl.buffer_data buffer_type (Bigarray.Array1.size_in_bytes array) (Some array) Gl.static_draw

  let bind (MkT bid) buffer_type =
    Gl.bind_buffer buffer_type bid

  let delete (MkT bid) =
    let array = Bigarray.Array1.create Bigarray.Int32 Bigarray.C_layout 1 in
    array.{0} <- Int32.of_int_exn bid;
    Gl.delete_buffers 1 array

end 

module VAO : sig
  type t
  val create : unit -> t
  val bind: t -> unit
  val delete: t -> unit
end = struct

  type t = MkT of {id:int; mutable deleted:bool;} 

  let create () : t =
    MkT {id=(get_int (Gl.gen_vertex_arrays 1) |> (fun v -> Option.value_exn v)); deleted=false;}

  let bind (MkT b) =
    Gl.bind_vertex_array b.id

  let delete (MkT b) =
    if not b.deleted then begin
      let array = Bigarray.Array1.create Bigarray.Int32 Bigarray.C_layout 1 in
      array.{0} <- Int32.of_int_exn b.id;
      Gl.delete_vertex_arrays 1 array;
      b.deleted <- true;
    end

end


module Texture = struct

  module Param = struct

    (** texture coordinates are mapped to a [0-1] * [0-1] space,
       wrap_option specifies how to treat coordinates outside this range
    *)
    type wrap_option =
      | Repeat         (** ignore integer part of coordinate resulting in a repeating pattern *)
      | MirroredRepeat (** ignore integer part, but invert coordinate if integer is odd *)
      | ClampToEdge    (** clamp values to 0 and 1 leading to the outer border of the image
                           being stretched out*)
      | ClampToBorder  (** values outside the image space will be given a specific colour specified by
                           texture border colour*)        
    [@@deriving sexp, show, eq]

    let wrap_option_to_gl = function
      | Repeat -> Gl.repeat
      | MirroredRepeat -> Gl.mirrored_repeat
      | ClampToEdge -> Gl.clamp_to_edge
      | ClampToBorder -> Gl.clamp_to_border

    (* how to map texture coordinates to pixels *)
    type filter_option =
      | Nearest                 (** use the nearest pixel to the coordinates *)
      | Linear                  (** return the weighted average of the 4 pixel surrounding the coordinates *)
      | NearestMipmapNearest    (** sample from the nearest pixel from the nearest mipmap *)
      | NearestMipmapLinear     (** sample a weighted average from the nearest mipmap *)
      | LinearMipmapNearest     (** sample the nearest pixel from a weighted linear combination of the
                                    mipmaps *)
      | LinearMipmapLinear      (** sample a weighted average pixel from a weighted linear combination of
                                    the mipmaps *)
    [@@deriving sexp, show, eq]

    let filter_option_to_gl = function
      | Nearest -> Gl.nearest
      | Linear -> Gl.linear
      | NearestMipmapNearest -> Gl.nearest_mipmap_nearest
      | NearestMipmapLinear -> Gl.nearest_mipmap_linear
      | LinearMipmapNearest -> Gl.linear_mipmap_nearest
      | LinearMipmapLinear -> Gl.linear_mipmap_linear

    type t =
      | TextureBaseLevel of int (** smallest mipmap size to be considered available with the texture *)
      | TextureMaxLevel of int  (** largest mipmap size to be considered available with the texture  *)
      | TextureBorderColour of V4.t
      | WrapS of wrap_option
      | WrapT of wrap_option
      | MinFilter of filter_option  (** mapping texture coordinates to pixels when scaling down *)
      | MagFilter of filter_option  (** mapping texture coordinates to pixels when scaling up *)
    [@@deriving show, eq]

    let default = [
      WrapS Repeat;
      WrapT Repeat;
      MinFilter Linear;
      MagFilter Linear;
    ]

  
  end

  module Mk (S : sig val binding_pos : int end) = struct
    type t = MkT of {id: int; mutable deleted:bool; }

    let create () : t =
      MkT {id=(get_int (Gl.gen_textures 1)  |> (fun v -> Option.value_exn v)); deleted=false;}

    let bind (MkT t) =
      Gl.bind_texture S.binding_pos t.id
    
    let delete (MkT t) =
      if not t.deleted then begin
        let array = Bigarray.Array1.create Bigarray.Int32 Bigarray.C_layout 1 in
        array.{0} <- Int32.of_int_exn t.id;
        Gl.delete_textures 1 array;
        t.deleted <- true;
      end

    let bind_param ?(bind_texture=true) (MkT tid) (param: Param.t) =
      if bind_texture then bind (MkT tid);
      match param with
      | Param.TextureBaseLevel level ->
        Gl.tex_parameteri
          S.binding_pos
          Gl.texture_base_level
          level
      | Param.TextureMaxLevel level ->
        Gl.tex_parameteri
          S.binding_pos
          Gl.texture_max_level
          level
      | Param.TextureBorderColour colour ->
        Gl.tex_parameterfv
          S.binding_pos
          Gl.texture_border_color
          (array_of_v4 colour)
      | Param.WrapS wrap_option ->
        Gl.tex_parameteri
          S.binding_pos
          Gl.texture_wrap_s
          (Param.wrap_option_to_gl wrap_option)
      | Param.WrapT wrap_option ->
        Gl.tex_parameteri
          S.binding_pos
          Gl.texture_wrap_t
          (Param.wrap_option_to_gl wrap_option)
      | Param.MinFilter filter_option ->
        Gl.tex_parameteri
          S.binding_pos
          Gl.texture_min_filter
          (Param.filter_option_to_gl filter_option)
      | Param.MagFilter filter_option ->
        Gl.tex_parameteri
          S.binding_pos
          Gl.texture_mag_filter
          (Param.filter_option_to_gl filter_option)

  end
  
  module Texture1D = struct
    include Mk (struct let binding_pos = Gl.texture_1d end)

    let create_empty ?(params=[]) width =
      let params = params @ [
          Param.MagFilter Param.Nearest;
          Param.MinFilter Param.Nearest;
        ] in
      let texture = create () in bind texture;
      List.iter params ~f:(bind_param ~bind_texture:false texture);
      Gl.tex_image1d
        Gl.texture_1d
        (* level *) 0
        Gl.rgba
        width
        (* border (must be 0) *) 0
        Gl.rgba
        Gl.unsigned_byte
        (`Offset 0);
      texture

  end 

  module Texture2D = struct
    include Mk (struct let binding_pos = Gl.texture_2d end)

    let create_empty ?(params=[]) width height =
      let params = params @ [
          Param.MagFilter Param.Nearest;
          Param.MinFilter Param.Nearest;
        ] in
      let texture = create () in bind texture;
      List.iter params ~f:(bind_param ~bind_texture:false texture);
      Gl.tex_image2d
        Gl.texture_2d
        (* level *) 0
        Gl.rgba
        width height
        (* border (must be 0) *) 0
        Gl.rgba
        Gl.unsigned_byte
        (`Offset 0);
      texture

  end

  module Texture3D = struct
    include Mk (struct let binding_pos = Gl.texture_3d end)
  end

  module TextureCube = struct
    include Mk (struct let binding_pos = Gl.texture_cube_map end)
  end 


  type t =
    | Texture_1D of Texture1D.t
    | Texture_2D of Texture2D.t
    | Texture_3D of Texture3D.t
    | Texture_Cube of TextureCube.t

  type texture_unit = MkUnit of {uniform_id: int; texture_unit: int}

  let delete = function
    | Texture_1D t -> Texture1D.delete t
    | Texture_2D t -> Texture2D.delete t
    | Texture_3D t -> Texture3D.delete t
    | Texture_Cube t -> TextureCube.delete t
    


  let use_texture (texture: t) (MkUnit { uniform_id; texture_unit }: texture_unit) =
    Gl.uniform1i uniform_id texture_unit;
    Gl.active_texture (Gl.texture0 + texture_unit);
    match texture with
    | Texture_1D tex -> Texture1D.bind tex
    | Texture_2D tex -> Texture2D.bind tex
    | Texture_3D tex -> Texture3D.bind tex
    | Texture_Cube tex -> TextureCube.bind tex

end

module RBO : sig
  type internal_format = DepthComponent16 | DepthComponent24 | DepthComponent32F | Depth24Stencil8
                       | Depth32FStencil8 | StencilIndex8 | RGBA8
  type base_format = DepthComponent | DepthStencil | Stencil | Colour0
  type t
  val internal_format_to_gl : internal_format -> Tgl3.Gl.enum
  val base_format_to_gl_attachment : base_format -> Tgl3.Gl.enum
  val base_format_to_internal_format : base_format -> internal_format
  val internal_format_to_base_format : internal_format -> base_format
  val create : internal_format:internal_format -> width:int -> height:int -> unit -> t
  val from_depth_or_stencil : stencil:bool -> depth:bool -> width:int -> height:int -> unit -> t
  val bind : t -> unit
  val get_internal_format : t -> internal_format
  val get_buffer_id: t -> int
  val delete : t -> unit
end  = struct

  type internal_format =
    | DepthComponent16
    | DepthComponent24
    | DepthComponent32F
    | Depth24Stencil8
    | Depth32FStencil8
    | StencilIndex8
    | RGBA8

  type base_format =
    | DepthComponent
    | DepthStencil
    | Stencil
    | Colour0

  type t = MkT of int * internal_format

  let internal_format_to_gl = function
    | DepthComponent16 -> Gl.depth_component16
    | DepthComponent24 -> Gl.depth_component24
    | DepthComponent32F -> Gl.depth_component32f
    | Depth24Stencil8 -> Gl.depth24_stencil8
    | Depth32FStencil8 -> Gl.depth32f_stencil8
    | StencilIndex8 -> Gl.stencil_index8
    | RGBA8 -> Gl.rgba8i

  let base_format_to_gl_attachment = function
    | DepthComponent -> Gl.depth_attachment
    | DepthStencil -> Gl.depth_stencil_attachment
    | Stencil -> Gl.stencil_attachment
    | Colour0 -> Gl.color_attachment0

  (** retrieves the best internal format for a desired base format *)
  let base_format_to_internal_format = function
    | DepthComponent -> DepthComponent24
    | DepthStencil -> Depth24Stencil8
    | Stencil -> StencilIndex8
    | Colour0 -> RGBA8
  (** converts an internal format to a supported base format *)
  let internal_format_to_base_format = function
    | DepthComponent16 -> DepthComponent
    | DepthComponent24 -> DepthComponent
    | DepthComponent32F -> DepthComponent
    | Depth24Stencil8 -> DepthStencil
    | Depth32FStencil8 -> DepthStencil
    | StencilIndex8 -> Stencil
    | RGBA8 -> Colour0
  
  let create ~internal_format ~width ~height () : t =
    let rbo = (get_int (Gl.gen_renderbuffers 1) |> (fun v -> Option.value_exn v)) in
    Gl.bind_renderbuffer Gl.renderbuffer rbo;
    let () =
      let internal_format = internal_format_to_gl internal_format in
      Gl.renderbuffer_storage Gl.renderbuffer internal_format width height in
    MkT (rbo, internal_format)

  let from_depth_or_stencil ~stencil ~depth ~width ~height () : t =
    let base_format = match stencil, depth with
      | true, false -> Stencil | false, true -> DepthComponent | _ -> DepthStencil in
    let internal_format = base_format_to_internal_format base_format in
    create ~internal_format ~width ~height ()

  let bind (MkT (rbo, _)) =
    Gl.bind_renderbuffer Gl.renderbuffer rbo

  let get_internal_format (MkT (_, internal_format)) = internal_format
  let get_buffer_id (MkT (bid, internal_format)) = bid

  let delete (MkT (rbo, _)) : unit =
    let array = Bigarray.Array1.create Bigarray.Int32 Bigarray.C_layout 1 in
    array.{0} <- Int32.of_int_exn rbo;
    Gl.delete_renderbuffers 1 array

end

module FBO : sig
  type t
  type binding_pos = Both | Read | Write [@@deriving show, eq, ord, sexp]

  val pp: Format.formatter -> t -> unit
  val show : t -> string

  val default_fbo: t
  val create : unit -> t
  val buffer_data: t -> int -> ('a, 'b, Bigarray.c_layout) Bigarray.Array1.t -> unit
  val bind: t -> binding_pos -> unit
  val validate: ?should_bind:bool -> t -> bool
  val validate_exn: ?should_bind:bool -> t -> unit
  val delete: t -> unit
  val bind_texture: ?should_bind:bool -> t -> Texture.t -> int -> binding_pos -> unit
  val bind_stencil_texture: ?should_bind:bool -> t -> Texture.t -> binding_pos -> unit
  val bind_depth_texture: ?should_bind:bool -> t -> Texture.t  -> binding_pos -> unit
  val bind_renderbuffer : ?should_bind:binding_pos option -> t -> RBO.t -> unit
  val read_framebuffer: ?attachment:int -> ?x:int -> ?y:int -> t -> w:int -> h:int -> Image.t
  val set_draw_buffers: int -> unit
  val unset_draw_buffers: int -> unit
  val blit_between_framebuffers: ?x_src:int -> ?y_src:int -> ?x_dst:int -> ?y_dst:int ->
    ?w_src:int -> ?h_src:int -> from_buffer:t -> to_buffer:t -> w:int -> h:int -> RBO.base_format -> unit    

end = struct

  type t = MkT of int

  let pp fmt (MkT id) =
    Format.pp_open_hbox fmt ();
    Format.pp_print_string fmt "FBO(";
    Format.pp_print_int fmt id;
    Format.pp_print_char fmt ')';
    Format.pp_close_box fmt ()

 let show fbo = pp Format.str_formatter fbo; Format.flush_str_formatter ()



  type binding_pos = Both | Read | Write [@@deriving show, eq, ord, sexp]

  let default_fbo = MkT 0

  let create () : t =
    MkT (get_int (Gl.gen_framebuffers 1) |> (fun v -> Option.value_exn v))

  let buffer_data ((MkT buffer): t) (buffer_type: int) array =
    Gl.bind_buffer buffer_type buffer;
    Gl.buffer_data buffer_type (Bigarray.Array1.size_in_bytes array) (Some array) Gl.static_draw


  let to_colour_attachment texture_count = match texture_count with
      |0->Gl.color_attachment0|1->Gl.color_attachment1|2->Gl.color_attachment2|3->Gl.color_attachment3
      |4->Gl.color_attachment4|5->Gl.color_attachment5|6->Gl.color_attachment6|7->Gl.color_attachment7
      |8->Gl.color_attachment8|9->Gl.color_attachment9|10->Gl.color_attachment10|11->Gl.color_attachment11
      |12->Gl.color_attachment12|13->Gl.color_attachment13|14->Gl.color_attachment14|15->Gl.color_attachment15
      |16->Gl.color_attachment16|17->Gl.color_attachment17|18->Gl.color_attachment18|19->Gl.color_attachment19
      |20->Gl.color_attachment20|21->Gl.color_attachment21|22->Gl.color_attachment22|23->Gl.color_attachment23
      |24->Gl.color_attachment24|25->Gl.color_attachment25|26->Gl.color_attachment26|27->Gl.color_attachment27
      |28->Gl.color_attachment28|29->Gl.color_attachment29|30->Gl.color_attachment30|31->Gl.color_attachment31
      | _ -> assert false

  let blit_between_framebuffers
      ?(x_src=0) ?(y_src=0) ?(x_dst=0) ?(y_dst=0)
      ?(w_src) ?(h_src)
      ~from_buffer:(MkT fid) ~to_buffer:(MkT tid) ~w ~h (fmt: RBO.base_format) =
    let w_src, h_src = match w_src,h_src with
      | None,None -> w, h | None, Some h -> w, h | Some w, None -> w, h | Some w, Some h -> w, h in

    let mask =
      match fmt with RBO.DepthComponent -> Gl.depth_buffer_bit | RBO.Stencil -> Gl.stencil_buffer_bit
                   | RBO.DepthStencil -> Gl.depth_buffer_bit lor Gl.stencil_buffer_bit
                   | RBO.Colour0 -> Gl.color_buffer_bit in
    Gl.bind_framebuffer Gl.read_framebuffer fid;
    Gl.bind_framebuffer Gl.draw_framebuffer tid;
    Gl.blit_framebuffer x_src y_src w_src h_src x_dst y_dst w_src h_src mask Gl.nearest

  external gl_read_pixels: int -> int -> int -> int ->
    ('a, 'b, Bigarray.c_layout) Bigarray.Array1.t -> unit = "gl_read_pixels"


  let read_framebuffer ?(attachment=0) ?(x=0) ?(y=0) (MkT fid) ~w ~h =
    Gl.bind_framebuffer Gl.framebuffer fid;
    Gl.read_buffer (to_colour_attachment attachment);
    let size = ((w * h) * 4) in
    let data = Bigarray.Array1.create Bigarray.Int8_unsigned Bigarray.C_layout size in
    let reversed_data = Bigarray.Array1.create Bigarray.Int8_unsigned Bigarray.C_layout size in
    Gl.pixel_storei Gl.pack_alignment 4;
    Gl.read_pixels x y w h Gl.rgba Gl.unsigned_byte (`Data data);
    (* gl_read_pixels w h Gl.rgba Gl.byte data; *)
    Gl.finish ();
    for i = 0 to h - 1 do
      for j = 0 to w - 1 do
        let swap x y = reversed_data.{x} <- data.{y} in
        let ij_to_ind (i,j) = w * 4 * i + j * 4 in
        let base_ind = ij_to_ind(i,j) in
        let inv_ind = ij_to_ind(h - i - 1, j) in
        swap base_ind inv_ind;
        swap (base_ind + 1) (inv_ind + 1);
        swap (base_ind + 2) (inv_ind + 2);
        swap (base_ind + 3) (inv_ind + 3)
      done;
    done;
    (* let () =
     *   let open Graphics in
     *   let cbuf =
     *     Array.init h ~f:(fun row ->
     *         Array.init w ~f:(fun column ->
     *             let index0 = row * w * 4 + column * 4 in
     *             Graphics.rgb (data.{index0}) (data.{index0 + 1}) (data.{index0 + 2})
     *           )
     *       )
     *   in
     *   Graphics.open_graph " 1920x1080";
     *   let image = Graphics.make_image cbuf in
     *   try
     *     while true do 
     *         Graphics.clear_graph ();
     *         draw_image image 0 0;
     *         let st = Graphics.wait_next_event [ Key_pressed ] in
     *         Printf.printf "key pressed: %c\n" Char.(st.key);
     *         if Char.(st.key = 'q') then raise Exit;
     *   done;
     *   with Exit -> ()
     *      | Graphic_failure _ -> ();
     * in *)
    Image.{format=Image.RGBA; width=w; height=h; data=reversed_data }
  
  let bind (MkT bid) (bpos: binding_pos) =
    match bpos with
    | Both -> Gl.bind_framebuffer Gl.framebuffer bid
    | Read -> Gl.bind_framebuffer Gl.read_framebuffer bid
    | Write -> Gl.bind_framebuffer Gl.draw_framebuffer bid

  let set_draw_buffers count =
    Gl.draw_buffers
      count
      (Bigarray.Array1.of_array Bigarray.int32 Bigarray.C_layout
         (Array.init count ~f:(fun v -> to_colour_attachment v |> Int32.of_int_exn )))

  let unset_draw_buffers count =
    Gl.draw_buffers
      count
      (Bigarray.Array1.of_array Bigarray.int32 Bigarray.C_layout
         (Array.init count ~f:(fun v -> (if v = 0 then Gl.color_attachment0 else Gl.none)
                                        |> Int32.of_int_exn)))

  let validate ?(should_bind=true) fbo =
    if should_bind then bind fbo Both;
    Gl.check_framebuffer_status Gl.framebuffer = Gl.framebuffer_complete

  let validate_exn ?(should_bind=true) (MkT bid) =
    if should_bind then bind (MkT bid) (Both);
    match Gl.check_framebuffer_status Gl.framebuffer with
    | v when v = Gl.framebuffer_complete -> ()
    | v when v = Gl.framebuffer_undefined ->
      raise (Failure "GL_FRAMEBUFFER_UNDEFINED - either the specified \
                      framebuffer is the default read or draw \
                      framebuffer, but the default framebuffer does \
                      not exist.")
    | v when v = Gl.framebuffer_incomplete_attachment ->
      raise (Failure "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT - one or \
                      more of the framebuffer attachment points are \
                      framebuffer incomplete")
    | v when v = Gl.framebuffer_incomplete_missing_attachment ->
      raise (Failure "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT - \
                      the framebuffer does not have at least one image \
                      attached to it.")
    | v when v = Gl.framebuffer_incomplete_draw_buffer ->
      raise (Failure "GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER - one or \
                      more of GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE is \
                      GL_NONE for any color attachment point named by GL_DRAW_BUFFERi")
    | v when v = Gl.framebuffer_incomplete_read_buffer ->
      raise (Failure "GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER - \
                      GL_READBUFFER is not none and the value \
                      GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE is GL_NONE \
                      for the color attachment point named by \
                      GL_READ_BUFFER.")
    | v when v = Gl.framebuffer_unsupported ->
      raise (Failure "GL_FRAMEBUFFER_UNSUPPORTED - the combination of \
                      internal formats of attached images violates the \
                      implementation dependent set of restrictions.")
    | v when v = Gl.framebuffer_incomplete_multisample ->
      raise (Failure "GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE - either \
                      the value of GL_RENDERBUFFER_SAMPLES is not same \
                      for all attached renderbuffers - if the value of \
                      GL_TEXTURE_SAMPLES is not the same for all \
                      attached textures, or the value of \
                      GL_RENDERBUFFER_SAMPLES does not match the value \
                      of GL_TEXTURE_SAMPLES.")
    | v when v = Gl.framebuffer_incomplete_layer_targets ->
      raise (Failure "GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS - either \
                      one or more of the framebuffer attachments are \
                      layered and not all the other attachments are \
                      layered, or not all the populated color \
                      attachments are not from textures of the same \
                      target.")
    | d -> raise (Failure (Printf.sprintf "Unknown OpenGL error (%d)" d))

    
  let delete (MkT bid) =
    let array = Bigarray.Array1.create Bigarray.Int32 Bigarray.C_layout 1 in
    array.{0} <- Int32.of_int_exn bid;
    Gl.delete_framebuffers 1 array


  let bind_texture_internal texture binding_pos attachment_point =
    begin match texture with
      | Texture.Texture_1D Texture.Texture1D.MkT texture ->
        Gl.framebuffer_texture1d binding_pos attachment_point Gl.texture_1d texture.id 0
      | Texture.Texture_2D Texture.Texture2D.MkT texture ->
        Gl.framebuffer_texture2d binding_pos attachment_point Gl.texture_2d texture.id 0
      | Texture.Texture_3D Texture.Texture3D.MkT texture ->
        Gl.framebuffer_texture3d binding_pos attachment_point Gl.texture_3d texture.id 0 0
      | Texture.Texture_Cube _ -> assert false
    end


  let bind_texture
      ?(should_bind=false)
      ((MkT bid) as fbo) (texture: Texture.t) texture_count bpos =
    if should_bind then bind fbo bpos;
    let binding_pos =
      match bpos with Both->Gl.framebuffer|Read->Gl.read_framebuffer|Write->Gl.draw_framebuffer in
    let attachment_point = to_colour_attachment texture_count in
    bind_texture_internal texture  binding_pos attachment_point

  let bind_stencil_texture
      ?(should_bind=false)
      ((MkT bid) as fbo) (texture: Texture.t)  bpos =
    if should_bind then bind fbo bpos;
    let binding_pos =
      match bpos with Both->Gl.framebuffer|Read->Gl.read_framebuffer|Write->Gl.draw_framebuffer in
    let attachment_point = Gl.stencil_attachment in
    bind_texture_internal texture  binding_pos attachment_point

  let bind_depth_texture
      ?(should_bind=false)
      ((MkT bid) as fbo) (texture: Texture.t) bpos =
    if should_bind then bind fbo bpos;
    let binding_pos =
      match bpos with Both->Gl.framebuffer|Read->Gl.read_framebuffer|Write->Gl.draw_framebuffer in
    let attachment_point = Gl.depth_attachment in
    bind_texture_internal texture  binding_pos attachment_point  

  let bind_renderbuffer
      ?(should_bind=Some Both)
      fbo rbo =
    begin match should_bind with | None -> () | Some bpos -> bind fbo bpos end;
    let attachment = RBO.get_internal_format rbo
                       |> RBO.internal_format_to_base_format
                       |> RBO.base_format_to_gl_attachment in
    Gl.framebuffer_renderbuffer Gl.framebuffer attachment Gl.renderbuffer (RBO.get_buffer_id rbo)

end

(* abstract definition of a core shader module *)
module GenericShader = struct

  module Types = struct

    type 'a t = Mat4x4_Float : M4.t t
              | Mat3x3_Float : M3.t t
              | Mat2x2_Float : M2.t t
              | Vec4_Float : V4.t t
              | Vec3_Float : V3.t t
              | Vec2_Float : V2.t t
              | Float : float t
              | Double : float t
              | Int : int t
              | Bool : bool t
              | Sampler1D : Image.t t
              | Sampler2D : Image.t t
              | Sampler3D : Image.t t
              | SamplerCube: (Image.t * Image.t * Image.t * Image.t * Image.t * Image.t) t

    module Data = struct

      module Numeric = struct

        type 'a t =  Mat4x4_Float : M4.t t
                  | Mat3x3_Float : M3.t t
                  | Mat2x2_Float : M2.t t
                  | Vec4_Float : V4.t t
                  | Vec3_Float : V3.t t
                  | Vec2_Float : V2.t t
                  | Float : float t
                  | Double : float t
                  | Int : int t
                  | Bool : bool t

        type 'a loc = int * 'a t

        type data = MkData : 'a * 'a t -> data
                  | MkBulkData : 'a list * 'a t -> data
                  | MkRawDataFloat : (float,Bigarray.float32_elt,Bigarray.c_layout) Bigarray.Array1.t -> data
                  | MkRawDataInt : (int32,Bigarray.int32_elt,Bigarray.c_layout) Bigarray.Array1.t -> data
        module Unwrapped = struct
          type a_t =  Mat4x4_Float | Mat3x3_Float | Mat2x2_Float 
                 | Vec4_Float | Vec3_Float | Vec2_Float | Float
                 | Double | Int | Bool [@@deriving sexp, ord,hash, show]

          type 'a or_multi = One of 'a | Multi of 'a list [@@deriving sexp, ord,hash, show]

          type data_t = Mat4x4_Float of M4.t or_multi
                      | Mat3x3_Float of M3.t or_multi
                      | Mat2x2_Float of M2.t or_multi
                      | Vec4_Float of V4.t or_multi
                      | Vec3_Float of V3.t or_multi
                      | Vec2_Float of V2.t or_multi
                      | Float of Float.t or_multi
                      | Double of Float.t or_multi
                      | Int of Int.t or_multi
                      | Bool of Bool.t or_multi
                      | FlArray of Float.t list
                      | InArray of Int32.t list [@@deriving sexp, ord,hash, show]

          let to_unwrapped : 'a t -> a_t = function
            Mat4x4_Float -> Mat4x4_Float | Mat3x3_Float -> Mat3x3_Float | Mat2x2_Float -> Mat2x2_Float
            | Vec4_Float -> Vec4_Float | Vec3_Float -> Vec3_Float | Vec2_Float -> Vec2_Float
            | Float -> Float | Double -> Double | Int -> Int | Bool -> Bool

          let to_unwrapped_data : data -> data_t = function
            | MkData (v, Mat4x4_Float) -> Mat4x4_Float (One v)
            | MkData (v, Mat3x3_Float) -> Mat3x3_Float (One v)
            | MkData (v, Mat2x2_Float) -> Mat2x2_Float (One v)
            | MkData (v, Vec4_Float) -> Vec4_Float (One v)
            | MkData (v, Vec3_Float) -> Vec3_Float (One v)
            | MkData (v, Vec2_Float) -> Vec2_Float (One v)
            | MkData (v, Float) -> Float (One v)
            | MkData (v, Double) -> Double (One v)
            | MkData (v, Int) -> Int (One v)
            | MkData (v, Bool) -> Bool (One v)
            | MkBulkData (v, Mat4x4_Float) -> Mat4x4_Float (Multi v)
            | MkBulkData (v, Mat3x3_Float) -> Mat3x3_Float (Multi v)
            | MkBulkData (v, Mat2x2_Float) -> Mat2x2_Float (Multi v)
            | MkBulkData (v, Vec4_Float) -> Vec4_Float (Multi v)
            | MkBulkData (v, Vec3_Float) -> Vec3_Float (Multi v)
            | MkBulkData (v, Vec2_Float) -> Vec2_Float (Multi v)
            | MkBulkData (v, Float) -> Float (Multi v)
            | MkBulkData (v, Double) -> Double (Multi v)
            | MkBulkData (v, Int) -> Int (Multi v)
            | MkBulkData (v, Bool) -> Bool (Multi v)
            | MkRawDataFloat v -> FlArray (List.init (Bigarray.Array1.dim v) ~f:(fun i -> v.{i}))
            | MkRawDataInt v -> InArray (List.init (Bigarray.Array1.dim v) ~f:(fun i -> v.{i}))

          let to_wrapped_data : data_t -> data = function
            | Mat4x4_Float (One v) ->  MkData (v, Mat4x4_Float)
            | Mat3x3_Float (One v) ->  MkData (v, Mat3x3_Float)
            | Mat2x2_Float (One v) ->  MkData (v, Mat2x2_Float)
            | Vec4_Float (One v) ->  MkData (v, Vec4_Float)
            | Vec3_Float (One v) ->  MkData (v, Vec3_Float)
            | Vec2_Float (One v) ->  MkData (v, Vec2_Float)
            | Float (One v) ->  MkData (v, Float)
            | Double (One v) ->  MkData (v, Double)
            | Int (One v) ->  MkData (v, Int)
            | Bool (One v) ->  MkData (v, Bool)
            | Mat4x4_Float (Multi v) ->  MkBulkData (v, Mat4x4_Float)
            | Mat3x3_Float (Multi v) ->  MkBulkData (v, Mat3x3_Float)
            | Mat2x2_Float (Multi v) ->  MkBulkData (v, Mat2x2_Float)
            | Vec4_Float (Multi v) ->  MkBulkData (v, Vec4_Float)
            | Vec3_Float (Multi v) ->  MkBulkData (v, Vec3_Float)
            | Vec2_Float (Multi v) ->  MkBulkData (v, Vec2_Float)
            | Float (Multi v) ->  MkBulkData (v, Float)
            | Double (Multi v) ->  MkBulkData (v, Double)
            | Int (Multi v) ->  MkBulkData (v, Int)
            | Bool (Multi v) ->  MkBulkData (v, Bool)
            | FlArray arr ->  MkRawDataFloat (Bigarray.Array1.of_array Bigarray.Float32 Bigarray.C_layout
                                                (Array.of_list arr))
            | InArray arr ->  MkRawDataInt  (Bigarray.Array1.of_array Bigarray.Int32 Bigarray.C_layout
                                               (Array.of_list arr))
        end        

        let pp_data fmt data =
          Unwrapped.pp_data_t fmt (Unwrapped.to_unwrapped_data data)

        let compare_data d1 d2 =
          Unwrapped.compare_data_t (Unwrapped.to_unwrapped_data d1) (Unwrapped.to_unwrapped_data d2)

        let sexp_of_data data =
          Unwrapped.sexp_of_data_t (Unwrapped.to_unwrapped_data data) 

        let data_of_sexp data =
          Unwrapped.data_t_of_sexp data |> Unwrapped.to_wrapped_data 

        let hash_fold_data hs data =
          Unwrapped.hash_fold_data_t hs (Unwrapped.to_unwrapped_data data)

        type field_type = MkFieldType : 'a t -> field_type

        type field = MkField : 'a loc -> field

        type payload = MkPayload : 'a * 'a loc -> payload
                     | MkBulkPayload : 'a list * 'a loc -> payload
                     | MkRawFloatPayload : (float,Bigarray.float32_elt,Bigarray.c_layout) Bigarray.Array1.t * 'a loc -> payload
                     | MkRawIntPayload : (int32,Bigarray.int32_elt,Bigarray.c_layout) Bigarray.Array1.t * 'a loc -> payload

        let pp_data formatter (data:data) =
          match data with
          | MkRawDataFloat data ->
            Format.pp_print_list
              ~pp_sep:(fun fmt () -> Format.pp_print_string fmt ", ")
              Float.pp
              formatter
              (List.range 0 (Bigarray.Array1.dim data)
               |> List.map ~f:(Bigarray.Array1.unsafe_get data))
          | MkRawDataInt data ->
            Format.pp_print_list
              ~pp_sep:(fun fmt () -> Format.pp_print_string fmt ", ")
              Int32.pp
              formatter
              (List.range 0 (Bigarray.Array1.dim data)
               |> List.map ~f:(Bigarray.Array1.unsafe_get data))
          (* | MkData (value, Sampler2D) ->
           *   Image.pp formatter value *)
          | MkData (value, Mat4x4_Float) ->
            M4.pp formatter value
          | MkData (value, Mat3x3_Float) ->
            M3.pp formatter value
          | MkData (value, Mat2x2_Float) ->
            M2.pp formatter value
          | MkData (value, Vec4_Float) ->
            V4.pp formatter value
          | MkData (value, Vec3_Float) ->
            V3.pp formatter value
          | MkData (value, Vec2_Float) ->
            V2.pp formatter value
          | MkData (value, Float) ->
            Float.pp formatter value
          | MkData (value, Double) ->
            Float.pp formatter value
          | MkData (value, Int) ->
            Int.pp formatter value
          | MkData (value, Bool) ->
            Bool.pp formatter value        
          (* | MkBulkData (value, Sampler2D) ->
           *   Format.pp_print_list
           *     (fun formatter value -> Image.pp formatter value) formatter value *)
          | MkBulkData (value, Mat4x4_Float) ->
            Format.pp_print_list
              (fun formatter value -> M4.pp formatter value) formatter value
          | MkBulkData (value, Mat3x3_Float) ->
            Format.pp_print_list
              (fun formatter value -> M3.pp formatter value) formatter value
          | MkBulkData (value, Mat2x2_Float) ->
            Format.pp_print_list
              (fun formatter value -> M2.pp formatter value) formatter value
          | MkBulkData (value, Vec4_Float) ->
            Format.pp_print_list
              (fun formatter value -> V4.pp formatter value) formatter value
          | MkBulkData (value, Vec3_Float) ->
            Format.pp_print_list
              (fun formatter value -> V3.pp formatter value) formatter value
          | MkBulkData (value, Vec2_Float) ->
            Format.pp_print_list
              (fun formatter value -> V2.pp formatter value) formatter value
          | MkBulkData (value, Float) ->
            Format.pp_print_list
              (fun formatter value -> Float.pp formatter value) formatter value
          | MkBulkData (value, Double) ->
            Format.pp_print_list
              (fun formatter value -> Float.pp formatter value) formatter value
          | MkBulkData (value, Int) ->
            Format.pp_print_list
              (fun formatter value -> Int.pp formatter value) formatter value
          | MkBulkData (value, Bool) ->
            Format.pp_print_list
              (fun formatter value -> Bool.pp formatter value)   formatter value

        let make_payload (data: data) (field: field) =
          match field,data with
          | (MkField (loc, Mat4x4_Float), MkData (value, Mat4x4_Float)) -> Some (MkPayload (value, (loc, Mat4x4_Float)))
          | (MkField (loc, Mat4x4_Float), MkBulkData (value, Mat4x4_Float)) -> Some (MkBulkPayload (value, (loc, Mat4x4_Float)))
          | (MkField (loc, Mat3x3_Float), MkData (value, Mat3x3_Float)) -> Some (MkPayload (value, (loc, Mat3x3_Float)))
          | (MkField (loc, Mat3x3_Float), MkBulkData (value, Mat3x3_Float)) -> Some (MkBulkPayload (value, (loc, Mat3x3_Float)))
          | (MkField (loc, Mat2x2_Float), MkData (value, Mat2x2_Float)) -> Some (MkPayload (value, (loc, Mat2x2_Float)))
          | (MkField (loc, Mat2x2_Float), MkBulkData (value, Mat2x2_Float)) -> Some (MkBulkPayload (value, (loc, Mat2x2_Float)))
          | (MkField (loc, Vec4_Float), MkData (value, Vec4_Float)) -> Some (MkPayload (value, (loc, Vec4_Float)))
          | (MkField (loc, Vec4_Float), MkBulkData (value, Vec4_Float)) -> Some (MkBulkPayload (value, (loc, Vec4_Float)))
          | (MkField (loc, Vec3_Float), MkData (value, Vec3_Float)) -> Some (MkPayload (value, (loc, Vec3_Float)))
          | (MkField (loc, Vec3_Float), MkBulkData (value, Vec3_Float)) -> Some (MkBulkPayload (value, (loc, Vec3_Float)))
          | (MkField (loc, Vec2_Float), MkData (value, Vec2_Float)) -> Some (MkPayload (value, (loc, Vec2_Float)))
          | (MkField (loc, Vec2_Float), MkBulkData (value, Vec2_Float)) -> Some (MkBulkPayload (value, (loc, Vec2_Float)))
          | (MkField (loc, Float), MkData (value, Float)) -> Some (MkPayload (value, (loc, Float)))
          | (MkField (loc, Float), MkBulkData (value, Float)) -> Some (MkBulkPayload (value, (loc, Float)))
          | (MkField (loc, Double), MkData (value, Double)) -> Some (MkPayload (value, (loc, Double)))
          | (MkField (loc, Double), MkBulkData (value, Double)) -> Some (MkBulkPayload (value, (loc, Double)))
          | (MkField (loc, Int), MkData (value, Int)) -> Some (MkPayload (value, (loc, Int)))
          | (MkField (loc, Int), MkBulkData (value, Int)) -> Some (MkBulkPayload (value, (loc, Int)))
          | (MkField (loc, Bool), MkData (value, Bool)) -> Some (MkPayload (value, (loc, Bool)))
          | (MkField (loc, Bool), MkBulkData (value, Bool)) -> Some (MkBulkPayload (value, (loc, Bool)))

          | (MkField (loc, Mat4x4_Float), MkRawDataFloat array)
            when Int.((Bigarray.Array1.dim array) % 16 = 0) ->
            Some (MkRawFloatPayload (array, (loc, Mat4x4_Float)))
          | (MkField (loc, Mat3x3_Float), MkRawDataFloat array)
            when Int.((Bigarray.Array1.dim array) % 9 = 0)
            -> Some (MkRawFloatPayload (array, (loc, Mat3x3_Float)))
          | (MkField (loc, Mat2x2_Float), MkRawDataFloat array)
            when Int.((Bigarray.Array1.dim array) % 4 = 0)
            -> Some (MkRawFloatPayload (array, (loc, Mat2x2_Float)))
          | (MkField (loc, Vec4_Float), MkRawDataFloat array)
            when Int.((Bigarray.Array1.dim array) % 4 = 0)
            -> Some (MkRawFloatPayload (array, (loc, Vec4_Float)))
          | (MkField (loc, Vec3_Float), MkRawDataFloat array)
            when Int.((Bigarray.Array1.dim array) % 3 = 0)
            -> Some (MkRawFloatPayload (array, (loc, Vec3_Float)))
          | (MkField (loc, Vec2_Float), MkRawDataFloat array)
            when Int.((Bigarray.Array1.dim array) % 2 = 0)
            -> Some (MkRawFloatPayload (array, (loc, Vec2_Float)))
          | (MkField (loc, Float), MkRawDataFloat array) ->
            Some (MkRawFloatPayload (array, (loc, Float)))
          | (MkField (loc, Int), MkRawDataInt array)
            -> Some (MkRawIntPayload (array, (loc, Int)))
          | _ -> None

        let field_type_eq (a: field_type) (b: field_type) =
          let value = a,b in
          match value with
          | (MkFieldType (Mat4x4_Float), MkFieldType (Mat4x4_Float))
          | (MkFieldType (Mat3x3_Float), MkFieldType (Mat3x3_Float))
          | (MkFieldType (Mat2x2_Float), MkFieldType (Mat2x2_Float))
          | (MkFieldType (Vec4_Float), MkFieldType (Vec4_Float))
          | (MkFieldType (Vec3_Float), MkFieldType (Vec3_Float))
          | (MkFieldType (Vec2_Float), MkFieldType (Vec2_Float))
          | (MkFieldType (Float), MkFieldType (Float))
          | (MkFieldType (Double), MkFieldType (Double))
          | (MkFieldType (Int), MkFieldType (Int))
          | (MkFieldType (Bool), MkFieldType (Bool)) ->  true
          | _ -> false

        let of_gl : int -> field_type option = function
          | v when v = Gl.int -> Some (MkFieldType Int)
          | v when v = Gl.float -> Some (MkFieldType Float)
          | v when v = Gl.double -> Some (MkFieldType Double)
          | v when v = Gl.bool -> Some (MkFieldType Bool)
          | v when v = Gl.float_vec2 -> Some (MkFieldType Vec2_Float)
          | v when v = Gl.float_vec3 -> Some (MkFieldType Vec3_Float)
          | v when v = Gl.float_vec4 -> Some (MkFieldType Vec4_Float)
          | v when v = Gl.float_mat2 -> Some (MkFieldType Mat2x2_Float)
          | v when v = Gl.float_mat3 -> Some (MkFieldType Mat3x3_Float)
          | v when v = Gl.float_mat4 -> Some (MkFieldType Mat4x4_Float)
          (* | v when v = Gl.sampler_2d -> Some (MkFieldType Sampler2D) *)
          | _ -> None

        let to_gl : field_type -> int = function
          | MkFieldType Int -> Gl.int
          | MkFieldType Float -> Gl.float
          | MkFieldType Double -> Gl.double
          | MkFieldType Bool -> Gl.bool
          | MkFieldType Vec2_Float -> (* Gl.float_vec2 *) Gl.float
          | MkFieldType Vec3_Float -> (* Gl.float_vec3 *) Gl.float
          | MkFieldType Vec4_Float -> (* Gl.float_vec4 *) Gl.float
          | MkFieldType Mat2x2_Float -> (* Gl.float_mat2 *) Gl.float
          | MkFieldType Mat3x3_Float -> (* Gl.float_mat3 *) Gl.float
          | MkFieldType Mat4x4_Float -> (* Gl.float_mat4 *) Gl.float
        (* | MkFieldType Sampler2D -> (\* Gl.float_mat4 *\) Gl.float *)

        let to_byte_size : field_type -> int = function
          | MkFieldType Int -> 4
          | MkFieldType Float -> 4
          | MkFieldType Double -> 8
          | MkFieldType Bool -> 4
          | MkFieldType Vec2_Float -> (4 * 2)
          | MkFieldType Vec3_Float -> (4 * 3)
          | MkFieldType Vec4_Float -> (4 * 4)
          | MkFieldType Mat2x2_Float -> (4 * 2) * 2
          | MkFieldType Mat3x3_Float -> (4 * 3) * 3
          | MkFieldType Mat4x4_Float -> (4 * 4) * 4

        let to_string (f: field_type) : string =
          match f with
          | MkFieldType (Mat4x4_Float) -> "Mat4x4_Float"
          | MkFieldType (Mat3x3_Float) -> "Mat3x3_Float"
          | MkFieldType (Mat2x2_Float) -> "Mat2x2_Float"
          | MkFieldType (Vec4_Float) -> "Vec4_Float"
          | MkFieldType (Vec3_Float) -> "Vec3_Float"
          | MkFieldType (Vec2_Float) -> "Vec2_Float"
          | MkFieldType (Float) -> "Float"
          | MkFieldType (Double) -> "Double"
          | MkFieldType (Int) -> "Int"
          | MkFieldType (Bool) -> "Bool"

        let data_count (data: data) =
          match data with
          | MkData (_, _) -> Some 1
          | MkBulkData (ls, _) -> Some (List.length ls)
          | MkRawDataFloat _ -> None
          | MkRawDataInt _ -> None


        let payload_count (payload: payload) =
          match payload with
          | MkPayload (_, _) -> 1
          | MkBulkPayload (ls, _) -> (List.length ls)
          | MkRawFloatPayload (data, (_, Mat4x4_Float)) ->
            let size = Bigarray.Array1.dim data in
            (size / 16)
          | MkRawFloatPayload (data, (_, Mat3x3_Float)) ->
            let size = Bigarray.Array1.dim data in
            (size / 9)
          | MkRawFloatPayload (data, (_, Mat2x2_Float)) ->
            let size = Bigarray.Array1.dim data in
            (size / 4)
          | MkRawFloatPayload (data, (_, Vec4_Float)) ->
            let size = Bigarray.Array1.dim data in
            (size / 4)
          | MkRawFloatPayload
              (data, (_, Vec3_Float)) ->
            let size = Bigarray.Array1.dim data in
            (size / 3)
          | MkRawFloatPayload (data, (_, Vec2_Float)) ->
            let size = Bigarray.Array1.dim data in
            (size / 2)
          | MkRawFloatPayload (data, (_, Float)) ->
            let size = Bigarray.Array1.dim data in
            size 
          | MkRawIntPayload (data, (_, Int)) -> 
            let size = Bigarray.Array1.dim data in
            size 
          | _ -> assert false

        let extend_to_count count (payload: payload) = match payload with
          | MkPayload (data, loc) ->
            Comp.return @@ MkBulkPayload (List.init count ~f:(fun _ -> data), loc)
          | MkBulkPayload (data, loc) ->
            let length = List.length data in
            let tmp_dat = Array.of_list data in
            Comp.return @@ MkBulkPayload (List.init count ~f:(fun ind -> tmp_dat.(ind % length)), loc)
          | MkRawIntPayload (data, (loc, Int)) ->
            let length = Bigarray.Array1.dim data in
            let data =
              Bigarray.Array1.of_array
                Bigarray.Int32
                Bigarray.C_layout
                (Array.init count  ~f:(fun ind -> data.{ind % length})) in
            Comp.return @@ MkRawIntPayload (data, (loc, Int))
          | MkRawFloatPayload (data, (loc, Mat4x4_Float)) ->
            let count = 16 * count in
            let length = Bigarray.Array1.dim data in
            let data =
              Bigarray.Array1.of_array
                Bigarray.Float32
                Bigarray.C_layout
                (Array.init count  ~f:(fun ind -> data.{ind % length})) in
            Comp.return @@ MkRawFloatPayload (data, (loc, Mat4x4_Float))
          | MkRawFloatPayload (data, (loc, Mat3x3_Float)) ->
            let count = 9 * count in
            let length = Bigarray.Array1.dim data in
            let data =
              Bigarray.Array1.of_array
                Bigarray.Float32
                Bigarray.C_layout
                (Array.init count  ~f:(fun ind -> data.{ind % length})) in
            Comp.return @@ MkRawFloatPayload (data, (loc, Mat3x3_Float))
          | MkRawFloatPayload (data, (loc, Mat2x2_Float)) ->
            let count = 4 * count in
            let length = Bigarray.Array1.dim data in
            let data =
              Bigarray.Array1.of_array
                Bigarray.Float32
                Bigarray.C_layout
                (Array.init count  ~f:(fun ind -> data.{ind % length})) in
            Comp.return @@ MkRawFloatPayload (data, (loc, Mat2x2_Float))
          | MkRawFloatPayload (data, (loc, Vec4_Float)) ->
            let count = 4 * count in
            let length = Bigarray.Array1.dim data in
            let data =
              Bigarray.Array1.of_array
                Bigarray.Float32
                Bigarray.C_layout
                (Array.init count  ~f:(fun ind -> data.{ind % length})) in
            Comp.return @@ MkRawFloatPayload (data, (loc, Vec4_Float))
          | MkRawFloatPayload (data, (loc, Vec3_Float)) ->
            let count = 3 * count in
            let length = Bigarray.Array1.dim data in
            let data =
              Bigarray.Array1.of_array
                Bigarray.Float32
                Bigarray.C_layout
                (Array.init count  ~f:(fun ind -> data.{ind % length})) in
            Comp.return @@ MkRawFloatPayload (data, (loc, Vec3_Float))
          | MkRawFloatPayload (data, (loc, Vec2_Float)) ->
            let count = 2 * count in
            let length = Bigarray.Array1.dim data in
            let _to_str arr = 
              Array.init (Bigarray.Array1.dim arr) ~f:(Bigarray.Array1.unsafe_get arr) 
              |> Array.to_list
              |> List.to_string ~f:(Float.to_string) in
            let data =
              Bigarray.Array1.of_array
                Bigarray.Float32
                Bigarray.C_layout
                (Array.init count  ~f:(fun ind -> data.{ind % length})) in

            Comp.return @@ MkRawFloatPayload (data, (loc, Vec2_Float))
          | MkRawFloatPayload (data, (loc, Float)) ->
            let length = Bigarray.Array1.dim data in
            let data =
              Bigarray.Array1.of_array
                Bigarray.Float32
                Bigarray.C_layout
                (Array.init count  ~f:(fun ind -> data.{ind % length})) in
            Comp.return @@ MkRawFloatPayload (data, (loc, Float))
          | _ -> assert false
        

        let to_vbo (payload: payload) : int * int * VBO.t =
          let load_data f loc sz value =
            let data = f value in
            let vbo = VBO.create () in
            VBO.buffer_data vbo Gl.array_buffer data;
            sz, loc, vbo in
          let count = payload_count payload in
          match payload with
          | MkRawIntPayload (data, (loc, _)) ->
            let vbo = VBO.create () in
            VBO.buffer_data vbo Gl.array_buffer data;
            count, loc, vbo 
          | MkRawFloatPayload (data, (loc, _)) ->
            let vbo = VBO.create () in
            VBO.buffer_data vbo Gl.array_buffer data;
            count, loc, vbo 
          | MkPayload (value, (loc, Mat4x4_Float)) ->
            load_data array_of_m4 loc count value
          | MkBulkPayload (value, (loc, Mat4x4_Float)) ->
            load_data m4list_to_array loc count value
          | MkPayload (value, (loc, Mat3x3_Float)) ->
            load_data array_of_m3 loc count value
          | MkBulkPayload (value, (loc, Mat3x3_Float)) ->
            load_data m3list_to_array loc count value
          | MkPayload (value, (loc, Mat2x2_Float)) ->
            load_data array_of_m2 loc count value
          | MkBulkPayload (value, (loc, Mat2x2_Float)) ->
            load_data m2list_to_array loc count value
          | MkPayload (value, (loc, Vec4_Float)) ->
            load_data array_of_v4 loc count value
          | MkBulkPayload (value, (loc, Vec4_Float)) ->
            load_data v4list_to_array loc count value
          | MkPayload (value, (loc, Vec3_Float)) ->
            load_data array_of_v3 loc count value
          | MkBulkPayload (value, (loc, Vec3_Float)) ->
            load_data v3list_to_array loc count value
          | MkPayload (value, (loc, Vec2_Float)) ->
            load_data array_of_v2 loc count value
          | MkBulkPayload (value, (loc, Vec2_Float)) ->
            load_data v2list_to_array loc count value
          | MkPayload (value, (loc, Float)) ->
            load_data array_of_float loc count value
          | MkBulkPayload (value, (loc, Float)) ->
            load_data floatlist_to_array loc count value
          | MkPayload (value, (loc, Double)) ->
            load_data array_of_double loc count value
          | MkBulkPayload (value, (loc, Double)) ->
            load_data doublelist_to_array loc count value
          | MkPayload (value, (loc, Int)) ->
            load_data array_of_int loc count value
          | MkBulkPayload (value, (loc, Int)) ->
            load_data intlist_to_array loc count value
          | MkPayload (value, (loc, Bool)) ->
            load_data array_of_bool loc count value        
          | MkBulkPayload (value, (loc, Bool)) ->
            load_data boollist_to_array loc count value

        let bind_attribute (type a) ?(normalize=false) (value: a loc) =
          let bind_mat ~size ~field_typ ~vec_typ ~rows loc =
            let typ = to_gl field_typ in
            let stride = to_byte_size field_typ  in
            let vec_size = to_byte_size vec_typ  in
            let bind_w_offset i =
              Gl.vertex_attrib_pointer
                (* index *) (loc + i)
                (* size *)  size
                (* type *)  typ 
                (* normalize *) normalize
                (* stride *) stride
                (* pointer *) (`Offset (vec_size * i));
              Gl.enable_vertex_attrib_array (loc + i)
            in
            List.range 0 rows
            |> List.iter ~f:bind_w_offset
          in
          let bind_vec ~size ~field_typ loc =
            let typ = to_gl field_typ in
            Gl.vertex_attrib_pointer
              (* index *) loc
              (* size *)  size
              (* type *)  typ 
              (* normalize *) normalize
              (* stride *) 0
              (* pointer *) (`Offset 0);
            Gl.enable_vertex_attrib_array loc
          in
          match value with
          | (loc, Mat4x4_Float) ->
            bind_mat ~size:4 ~field_typ:(MkFieldType Mat4x4_Float) ~vec_typ:(MkFieldType Vec4_Float) ~rows:4 loc
          | (loc, Mat3x3_Float) ->
            bind_mat ~size:3 ~field_typ:(MkFieldType Mat3x3_Float) ~vec_typ:(MkFieldType Vec3_Float) ~rows:3 loc
          | (loc, Mat2x2_Float) ->
            bind_mat ~size:2 ~field_typ:(MkFieldType Mat2x2_Float) ~vec_typ:(MkFieldType Vec2_Float) ~rows:2 loc
          | (loc, Vec4_Float) ->
            bind_vec ~size:4 ~field_typ:(MkFieldType Vec4_Float) loc
          | (loc, Vec3_Float) ->
            bind_vec ~size:3 ~field_typ:(MkFieldType Vec3_Float) loc
          | (loc, Vec2_Float) ->
            bind_vec ~size:2 ~field_typ:(MkFieldType Vec2_Float) loc
          | (loc, Float) ->
            bind_vec ~size:1 ~field_typ:(MkFieldType Float) loc
          | (loc, Double) ->
            bind_vec ~size:1 ~field_typ:(MkFieldType Double) loc
          | (loc, Int) ->
            bind_vec ~size:1 ~field_typ:(MkFieldType Int) loc
          | (loc, Bool) ->
            bind_vec ~size:1 ~field_typ:(MkFieldType Int) loc        

        let load_data ?(normalize=false) (vao: VAO.t) payload : int * VBO.t =
          let (size, _, vbo) = to_vbo payload in
          VAO.bind vao;
          VBO.bind vbo  Gl.array_buffer;
          let () =
            match payload with
            | MkPayload (_, loc) -> bind_attribute ~normalize loc
            | MkBulkPayload (_, loc) -> bind_attribute ~normalize loc
            | MkRawFloatPayload (_, loc) -> bind_attribute ~normalize loc
            | MkRawIntPayload (_, loc) -> bind_attribute ~normalize loc
          in
          size, vbo

        let load_data_list ?(raw_data=[]) ?(normalize=false) (vao: VAO.t) payload_list =
          VAO.bind vao;
          List.map raw_data ~f:(fun (name, ((size,vbo),loc)) ->
              VBO.bind vbo  Gl.array_buffer;
              begin match loc with MkField loc -> bind_attribute ~normalize loc end;
              (name, (size, vbo))
            ),
          List.map payload_list ~f:(fun (name, payload) ->
              let (size, _, vbo) = to_vbo payload in
              VBO.bind vbo  Gl.array_buffer;
              begin match payload with
                | MkPayload (_, loc) -> bind_attribute ~normalize loc
                | MkBulkPayload (_, loc) -> bind_attribute ~normalize loc
                | MkRawFloatPayload (_, loc) -> bind_attribute ~normalize loc
                | MkRawIntPayload (_, loc) -> bind_attribute ~normalize loc
              end;
              name, (size, vbo)
            )

      end

      module Texture = struct

        type 'a t =
            Sampler1D : Texture.Texture1D.t t
          | Sampler2D : Texture.Texture2D.t t
          | Sampler3D : Texture.Texture3D.t t
          | SamplerCube : Texture.TextureCube.t t

        type 'a loc = { uniform_id: int; texture_unit: Texture.texture_unit;  texture_type: 'a t}

        (* raw data that is supported by this component *)
        type data = MkData : Image.t list -> data

        (* raw data + location information - ready to upload *)
        type field_type = MkFieldType : 'a t -> field_type
        type field = MkField : 'a loc -> field

        type payload = MkPayload : Image.t list * Texture.Param.t list * 'a loc -> payload

        let pp_data formatter (MkData data) =
          Format.pp_print_list
            ~pp_sep:(fun fmt () -> Format.pp_print_string fmt ";"; Format.pp_print_space fmt ())
            Image.pp formatter data

        let make_payload (MkData image: data) params (MkField field: field) =
          match image, field.texture_type with
          | [image], Sampler1D ->
            if Int.(image.height = 1) then Some (MkPayload ([image], params, field)) else None
          | [image], Sampler2D ->
            Some (MkPayload ([image], params, field))
          | [right], SamplerCube ->
            Some (MkPayload ([right; right; right; right; right; right], params, field))
          | [right;left], SamplerCube ->
            Some (MkPayload ([right; left; right; left; right; left], params, field))
          | [right;left;top], SamplerCube ->
            Some (MkPayload ([right; left; top; right; left; top], params, field))
          | [right; left; top; bottom; back; front], SamplerCube ->
            Some (MkPayload ([right; left; top; bottom; back; front], params, field))
          | _ -> None

        let field_type_eq (a: field_type) (b: field_type) =
          match a,b with
          | (MkFieldType (Sampler1D), MkFieldType (Sampler1D)) -> true
          | (MkFieldType (Sampler2D), MkFieldType (Sampler2D)) -> true
          | (MkFieldType (Sampler3D), MkFieldType (Sampler3D)) -> true
          | (MkFieldType (SamplerCube), MkFieldType (SamplerCube)) -> true
          | _ -> false

        let of_gl = function
          | v when v = Gl.sampler_1d -> Some (MkFieldType Sampler1D)
          | v when v = Gl.sampler_2d -> Some (MkFieldType Sampler2D)
          | v when v = Gl.sampler_3d -> Some (MkFieldType Sampler3D)
          | v when v = Gl.sampler_cube -> Some (MkFieldType SamplerCube)
          | _ -> None

        let to_gl = function
          | MkFieldType Sampler1D -> Gl.sampler_1d
          | MkFieldType Sampler2D -> Gl.sampler_2d
          | MkFieldType Sampler3D -> Gl.sampler_3d
          | MkFieldType SamplerCube -> Gl.sampler_cube

        let load_data
          ?(mipmaps_needed=false)
            (MkPayload (image, params, { texture_unit; texture_type; _ }): payload)
          : Texture.texture_unit * Texture.t =
          match image, texture_type with
          | _, Sampler3D -> assert false (* unsupported *)
          | [right; left; top; bottom; back; front], SamplerCube ->
            let texture = Texture.TextureCube.create () in
            Texture.TextureCube.bind texture;
            List.iter params
              ~f:(Texture.TextureCube.bind_param ~bind_texture:false texture);
            let load_image tag image =
              let open Image in
              Gl.tex_image2d tag 0
              (Image.format_to_gl image.format)
              image.width image.height 0 (Image.format_to_gl image.format)
              (Image.to_gl_element_type image.format) (`Data image.data) in
            load_image Gl.texture_cube_map_positive_x right;
            load_image Gl.texture_cube_map_negative_x left;
            load_image Gl.texture_cube_map_positive_y top;
            load_image Gl.texture_cube_map_negative_y bottom;
            load_image Gl.texture_cube_map_positive_z back;
            load_image Gl.texture_cube_map_negative_z front;
            if mipmaps_needed then Gl.generate_mipmap Gl.texture_1d;
            texture_unit, Texture.Texture_Cube texture
          | [image], Sampler1D ->
            let texture = Texture.Texture1D.create () in            
            Texture.Texture1D.bind texture;
            List.iter params
              ~f:(Texture.Texture1D.bind_param ~bind_texture:false texture);
            Gl.tex_image1d
              Gl.texture_1d
              0
              (Image.format_to_component_count image.format)
              image.width
              0
              (Image.format_to_gl image.format)
              (Image.to_gl_element_type image.format)
              (`Data image.data);
            if mipmaps_needed then Gl.generate_mipmap Gl.texture_1d;
            texture_unit, Texture.Texture_1D texture
          | [image], Sampler2D ->
            let texture = Texture.Texture2D.create () in
            Texture.Texture2D.bind texture;
            List.iter params
              ~f:(Texture.Texture2D.bind_param ~bind_texture:false texture);
            Gl.tex_image2d
              Gl.texture_2d
              0
              (Image.format_to_gl image.format)
              image.width
              image.height
              0
              (Image.format_to_gl image.format)
              (Image.to_gl_element_type image.format)
              (`Data image.data);
            if mipmaps_needed then Gl.generate_mipmap Gl.texture_2d;
            texture_unit, Texture.Texture_2D texture
          | _ -> assert false

        let load_data_list image_list : (Texture.texture_unit * Texture.t) list =
          List.map image_list
            ~f:(fun (payload) -> load_data payload)

      end

      type t =
        | Numeric : 'a Numeric.t -> t 
        | Texture : 'a Texture.t -> t 

      let of_gl (vl: int) : t option =
        match Numeric.of_gl vl, Texture.of_gl vl with
        | (Some Numeric.MkFieldType vl, _) -> Some (Numeric vl)
        | (_, Some Texture.MkFieldType vl) -> Some (Texture vl)
        | _ -> None

      let to_gl (value: t) : int = match value with
        | Numeric vl -> Numeric.to_gl (Numeric.MkFieldType vl)
        | Texture vl -> Texture.to_gl (Texture.MkFieldType vl)

    end
  end

  type t = {
    pid:  int;
    mutable textures_bound: bool;
  }

  type shader = int

  let textures_raw pid =
    let open Comp.Syntax in
    let+ no_uniforms =
      get_int (Gl.get_programiv pid Gl.active_uniforms)
      |> Comp.of_option ~error:(Error.of_string "Can not get program uniform count") in
    let buffer_size = 128 in
    let int_buffer = Bigarray.Array1.create Bigarray.int32 Bigarray.c_layout 3 in
    let buffer = Bigstring.create buffer_size in
    let+ textures =
    List.range 0 no_uniforms
    |> List.map ~f:begin fun ind ->
      Gl.get_active_uniform
        pid ind buffer_size
        (Some int_buffer)
        (Bigarray.Array1.sub int_buffer 1 1)
        (Bigarray.Array1.sub int_buffer 2 1)
        buffer;
      let len = Bigarray.Array1.unsafe_get int_buffer 0 in
      let size = Bigarray.Array1.unsafe_get int_buffer 1 in
      let typ = Bigarray.Array1.unsafe_get int_buffer 2 in
      let name = Bigstring.to_string ~len:(Int.of_int32_exn len) buffer in
      let gl_typ = Types.Data.Texture.of_gl (Int.of_int32_exn typ) in
      match gl_typ with
        | None ->
          Comp.return None
        | Some typ -> Comp.return (Some ((ind, name,size,typ)))
    end |> Comp.all |> Comp.map ~f:(List.filter_opt) in
    ret textures  

  let compile_shader_exn src typ =
        let sid = Gl.create_shader typ in
        Gl.shader_source sid src;
        Gl.compile_shader sid;
        let arr = Bigarray.Array1.create Bigarray.Char Bigarray.c_layout 2048 in
        let read_size = get_int (fun ind -> Gl.get_shader_info_log sid 2048 (Some ind) (arr))
                    |> (fun v -> Option.value_exn v)  in
        Printf.printf "shader_log: %s\n" @@ Bigstring.to_string ~len:read_size arr;
        if Poly.(get_shader sid Gl.compile_status = Option.some (Gl.true_))
        then sid
        else 
            let len =
        Option.value_exn (get_shader sid Gl.info_log_length)
          ~error:(Error.of_string "Could not get infolog length") in
            let log = get_string len (Gl.get_shader_info_log sid len None) in
            (Gl.delete_shader sid;
              raise  (Failure (Printf.sprintf "Compile error %s" log)))

  let compile_shader src typ =
    let open Comp.Syntax in
    let sid = Gl.create_shader typ in
    Gl.shader_source sid src;
    Gl.compile_shader sid;
    let arr = Bigarray.Array1.create Bigarray.Char Bigarray.c_layout 2048 in
    let read_size = get_int (fun ind -> Gl.get_shader_info_log sid 2048 (Some ind) (arr))
                    |> (fun v -> Option.value_exn v)  in
    let compile_output = (Bigstring.to_string ~len:read_size arr) in
    let+ () = if not @@ String.is_empty compile_output
      then Comp.warn (Printf.sprintf "shader compilation error: %s" compile_output)
      else Comp.return () in
    if Poly.(get_shader sid Gl.compile_status = Option.some (Gl.true_))
    then Comp.return sid
    else 
      let+ len =
        Comp.of_option (get_shader sid Gl.info_log_length)
          ~error:(Error.of_string "Could not get infolog length") in
      let log = get_string len (Gl.get_shader_info_log sid len None) in
      Gl.delete_shader sid;
      Comp.fail (Error.of_string (Printf.sprintf "shader compilation error: %s" log))  

  let delete_shader = Gl.delete_shader

  let get_uniform_location_exn ~shader ~uniform =
    let id = Gl.get_uniform_location shader.pid uniform in
    if id >= 0 then begin
      id
    end else
      raise (Failure (Printf.sprintf "Got uniform position of %d" id)) 

  let get_uniform_location_error ~shader ~uniform =
    let id = Gl.get_uniform_location shader.pid uniform in
    if id >= 0 || Int.(id = -1) then begin
      Ok id
    end else
      Error (Printf.sprintf "Got uniform position of %d" id) 

  let get_uniform_location ~shader ~uniform =
    let id = Gl.get_uniform_location shader.pid uniform in
    if id >= 0 || Int.(id = -1) then begin
      Comp.return id
    end else
      Comp.fail (Error.of_string (Printf.sprintf "Got uniform position of %d" id)) 

  let build_exn ~shaders ~attribs = 
    let pid = Gl.create_program () in
    let get_program e = get_int (Gl.get_programiv pid e) in 
    let shaders =  (List.map shaders ~f:(fun (shader,typ) ->
        let vid = compile_shader_exn shader typ in
        Gl.attach_shader pid vid;
        vid
      )) in
    (* bind attrib locations of each attribute *)
    List.iter attribs ~f:(fun (name,pos) -> Gl.bind_attrib_location pid pos name);
    Gl.link_program pid;
    (* Delete provided shaders, as not needed anymore *)
    List.iter shaders ~f:(fun sid -> Gl.delete_shader sid);
    (* return result *)
    if Poly.(get_program Gl.link_status = Some Gl.true_)
    then 
      { pid=pid; textures_bound=false; }
    else begin
      let len =
        Option.value_exn (get_program Gl.info_log_length)
          ~error:(Error.of_string "Could not get infolog length") in
      let log = get_string len (Gl.get_program_info_log pid len None) in
      (Gl.delete_program pid; raise (Failure (Printf.sprintf "Link error %s" log)))
    end

  let build ~shaders ~attribs =
    let open Comp.Syntax in
    let pid = Gl.create_program () in
    let get_program e = get_int (Gl.get_programiv pid e)
                        |> Comp.of_option ~error:(Error.of_string "Could not get program variable")  in 
    let+ shaders = 
      List.map shaders
        ~f:(fun (shader,typ) ->
            compile_shader shader typ )
      |> Comp.all in
    List.iter shaders ~f:(fun vid -> Gl.attach_shader pid vid);
    (* bind attrib locations of each attribute *)
    List.iter attribs ~f:(fun (name,pos) -> Gl.bind_attrib_location pid pos name);
    (* link the program *)
    Gl.link_program pid;
    (* Delete provided shaders, as not needed anymore *)
    List.iter shaders ~f:(fun sid -> Gl.delete_shader sid);
    (* return result *)
    let+ link_status = get_program Gl.link_status in
    if Poly.(link_status = Gl.true_)
    then 
      ret @@ { pid=pid; textures_bound = false; }
    else begin
      let+ len = (get_program Gl.info_log_length) in
      let log = get_string len (Gl.get_program_info_log pid len None) in
      Gl.delete_program pid;
      Comp.fail (Error.of_string (Printf.sprintf "shader linking error: %s" log))
    end

  let build_raw ~shaders ~attribs =
    let open Comp.Syntax in
    let pid = Gl.create_program () in
    let get_program e = get_int (Gl.get_programiv pid e)
                        |> Comp.of_option ~error:(Error.of_string "Could not get program variable")  in 
    List.iter shaders ~f:(fun vid -> Gl.attach_shader pid vid);
    (* bind attrib locations of each attribute *)
    List.iter attribs ~f:(fun (name,pos) -> Gl.bind_attrib_location pid pos name);
    (* link the program *)
    Gl.link_program pid;
    (* Delete provided shaders, as not needed anymore *)
    List.iter shaders ~f:(fun sid -> Gl.delete_shader sid);
    (* return result *)
    let+ link_status = get_program Gl.link_status in
    if Poly.(link_status = Gl.true_)
    then begin
      ret @@ { pid=pid; textures_bound=false; }
    end
    else begin
      let+ len = (get_program Gl.info_log_length) in
      let log = get_string len (Gl.get_program_info_log pid len None) in
      Gl.delete_program pid;
      Comp.fail (Error.of_string (Printf.sprintf "shader error: %s" log))
    end
  
  let use_program program =
    Gl.use_program program.pid

  let unuse_program () =
    Gl.use_program 0

  let load_uniforms ?(use_shader=false) program (uniforms: Types.Data.Numeric.payload list)  =
    if use_shader then use_program program;
    List.iter uniforms ~f:(fun uniform ->
        let count = Types.Data.Numeric.payload_count uniform in
        match uniform with
        | Types.Data.Numeric.MkPayload (data, (loc, Types.Data.Numeric.Mat4x4_Float)) ->
          Gl.uniform_matrix4fv
            loc count true (array_of_m4 data)
        | Types.Data.Numeric.MkBulkPayload (data, (loc, Types.Data.Numeric.Mat4x4_Float)) ->
          Gl.uniform_matrix4fv
            loc count true (m4list_to_array data)
        | Types.Data.Numeric.MkPayload (data, (loc, Types.Data.Numeric.Mat3x3_Float)) ->
          Gl.uniform_matrix3fv
            loc count true (array_of_m3 data)
        | Types.Data.Numeric.MkBulkPayload (data, (loc, Types.Data.Numeric.Mat3x3_Float)) ->
          Gl.uniform_matrix3fv
            loc count true (m3list_to_array data)
        | Types.Data.Numeric.MkPayload (data, (loc, Types.Data.Numeric.Mat2x2_Float)) ->
          Gl.uniform_matrix2fv
            loc count true (array_of_m2 data)
        | Types.Data.Numeric.MkBulkPayload (data, (loc, Types.Data.Numeric.Mat2x2_Float)) ->
          Gl.uniform_matrix2fv
            loc count true (m2list_to_array data)
        | Types.Data.Numeric.MkPayload (data, (loc, Types.Data.Numeric.Vec4_Float)) ->
          Gl.uniform4fv
            loc count (array_of_v4 data)
        | Types.Data.Numeric.MkBulkPayload (data, (loc, Types.Data.Numeric.Vec4_Float)) ->
          Gl.uniform4fv
            loc count (v4list_to_array data)
        | Types.Data.Numeric.MkPayload (data, (loc, Types.Data.Numeric.Vec3_Float)) ->
          Gl.uniform3fv
            loc count (array_of_v3 data)
        | Types.Data.Numeric.MkBulkPayload (data, (loc, Types.Data.Numeric.Vec3_Float)) ->
          Gl.uniform3fv
            loc count (v3list_to_array data)
        | Types.Data.Numeric.MkPayload (data, (loc, Types.Data.Numeric.Vec2_Float)) ->
          Gl.uniform2fv
            loc count (array_of_v2 data)
        | Types.Data.Numeric.MkBulkPayload (data, (loc, Types.Data.Numeric.Vec2_Float)) ->
          Gl.uniform2fv
            loc count (v2list_to_array data)
        | Types.Data.Numeric.MkPayload (data, (loc, Types.Data.Numeric.Int)) ->
          Gl.uniform1i loc data
        | Types.Data.Numeric.MkBulkPayload (data, (loc, Types.Data.Numeric.Int)) ->
          Gl.uniform1iv loc count (intlist_to_array data)
        | Types.Data.Numeric.MkPayload (data, (loc, Types.Data.Numeric.Float)) ->
          Gl.uniform1f loc data
        | Types.Data.Numeric.MkBulkPayload (data, (loc, Types.Data.Numeric.Float)) ->
          Gl.uniform1fv loc count (floatlist_to_array data)
        | Types.Data.Numeric.MkPayload (data, (loc, Types.Data.Numeric.Bool)) ->
          Gl.uniform1i loc (Bool.to_int data)
        | Types.Data.Numeric.MkBulkPayload (data, (loc, Types.Data.Numeric.Bool)) ->
          Gl.uniform1iv loc count (intlist_to_array (List.map ~f:Bool.to_int data))
        | Types.Data.Numeric.MkPayload (_, (_, Types.Data.Numeric.Double)) -> ()
        | Types.Data.Numeric.MkBulkPayload (_, (_, Types.Data.Numeric.Double)) -> ()

        | Types.Data.Numeric.MkRawFloatPayload (data, (loc, Types.Data.Numeric.Mat4x4_Float)) ->
          Gl.uniform_matrix4fv
            loc count true data
        | Types.Data.Numeric.MkRawFloatPayload (data, (loc, Types.Data.Numeric.Mat3x3_Float)) ->
          Gl.uniform_matrix3fv
            loc count true data
        | Types.Data.Numeric.MkRawFloatPayload (data, (loc, Types.Data.Numeric.Mat2x2_Float)) ->
          Gl.uniform_matrix2fv
            loc count true data
        | Types.Data.Numeric.MkRawFloatPayload (data, (loc, Types.Data.Numeric.Vec4_Float)) ->
          Gl.uniform4fv
            loc count data
        | Types.Data.Numeric.MkRawFloatPayload (data, (loc, Types.Data.Numeric.Vec3_Float)) ->
          Gl.uniform3fv
            loc count data
        | Types.Data.Numeric.MkRawFloatPayload (data, (loc, Types.Data.Numeric.Vec2_Float)) ->
          Gl.uniform2fv
            loc count data
        | Types.Data.Numeric.MkRawFloatPayload (data, (loc, Types.Data.Numeric.Float)) ->
          Gl.uniform1fv
            loc count data
        | Types.Data.Numeric.MkRawIntPayload (data, (loc, Types.Data.Numeric.Int)) ->
          Gl.uniform1iv
            loc count data
        | Types.Data.Numeric.MkRawFloatPayload (_, (_, _)) ->
          assert false
        | Types.Data.Numeric.MkRawIntPayload (_, (_, _)) ->
          assert false
      )

  let attributes_option {pid; _} =
    let open Option.Syntax in
    let+ no_attributes = get_int (Gl.get_programiv pid Gl.active_attributes) in
    let buffer_size = 128 in
    let int_buffer = Bigarray.Array1.create Bigarray.int32 Bigarray.c_layout 3 in
    let buffer = Bigstring.create buffer_size in
    let attributes =
    List.range 0 no_attributes
    |> List.map ~f:begin fun ind ->
      Gl.get_active_attrib
        pid ind buffer_size
        (Some int_buffer)
        (Bigarray.Array1.sub int_buffer 1 1)
        (Bigarray.Array1.sub int_buffer 2 1)
        buffer;
      let len = Bigarray.Array1.unsafe_get int_buffer 0 in
      let size = Bigarray.Array1.unsafe_get int_buffer 1 in
      let typ = Bigarray.Array1.unsafe_get int_buffer 2 in
      let name = Bigstring.to_string ~len:(Int.of_int32_exn len) buffer in
      (name,size, Types.Data.Numeric.of_gl (Int.of_int32_exn typ))
    end in
    ret attributes

  let attributes_with_options {pid; _} =
    let open Comp.Syntax in
    let+ no_attributes =
      get_int (Gl.get_programiv pid Gl.active_attributes)
      |> Comp.of_option ~error:(Error.of_string "Can not get program attribute count") in
    let buffer_size = 128 in
    let int_buffer = Bigarray.Array1.create Bigarray.int32 Bigarray.c_layout 3 in
    let buffer = Bigstring.create buffer_size in
    let+ attributes =
    List.range 0 no_attributes
    |> List.map ~f:begin fun ind ->
      Gl.get_active_attrib
        pid ind buffer_size
        (Some int_buffer)
        (Bigarray.Array1.sub int_buffer 1 1)
        (Bigarray.Array1.sub int_buffer 2 1)
        buffer;
      let len = Bigarray.Array1.unsafe_get int_buffer 0 in
      let size = Bigarray.Array1.unsafe_get int_buffer 1 in
      let typ = Bigarray.Array1.unsafe_get int_buffer 2 in
      let name = Bigstring.to_string ~len:(Int.of_int32_exn len) buffer in
      let gl_typ = Types.Data.Numeric.of_gl (Int.of_int32_exn typ) in
      let+ () =
        match gl_typ with
        | None ->
          let+ () = Comp.warn (Printf.sprintf "variable %s has unsupported type %ld" name typ) in
          Comp.return ()
        | Some _ -> Comp.return ()
      in
      ret @@ (name,size,gl_typ)
    end |> Comp.all in
    ret attributes  

  let attributes {pid; _} =
    let open Comp.Syntax in
    let+ no_attributes =
      get_int (Gl.get_programiv pid Gl.active_attributes)
      |> Comp.of_option ~error:(Error.of_string "Can not get program attribute count") in
    let buffer_size = 128 in
    let int_buffer = Bigarray.Array1.create Bigarray.int32 Bigarray.c_layout 3 in
    let buffer = Bigstring.create buffer_size in
    let+ attributes =
    List.range 0 no_attributes
    |> List.map ~f:begin fun ind ->
      Gl.get_active_attrib
        pid ind buffer_size
        (Some int_buffer)
        (Bigarray.Array1.sub int_buffer 1 1)
        (Bigarray.Array1.sub int_buffer 2 1)
        buffer;
      let len = Bigarray.Array1.unsafe_get int_buffer 0 in
      let size = Bigarray.Array1.unsafe_get int_buffer 1 in
      let typ = Bigarray.Array1.unsafe_get int_buffer 2 in
      let name = Bigstring.to_string ~len:(Int.of_int32_exn len) buffer in
      let gl_typ = Types.Data.Numeric.of_gl (Int.of_int32_exn typ) in
      match gl_typ with
        | None ->
          let+ () = Comp.warn (Printf.sprintf "attribute variable %s has unsupported type %ld" name typ) in
          Comp.return None
        | Some typ -> Comp.return (Some ((ind, name,size,typ)))
    end |> Comp.all |> Comp.map ~f:(List.filter_opt) in
    ret attributes  
  
  let uniforms_option {pid; _} =
    let open Option.Syntax in
    let+ no_attributes = get_int (Gl.get_programiv pid Gl.active_uniforms) in
    let buffer_size = 128 in
    let int_buffer = Bigarray.Array1.create Bigarray.int32 Bigarray.c_layout 3 in
    let buffer = Bigstring.create buffer_size in
    let uniforms =
    List.range 0 no_attributes
    |> List.map ~f:begin fun ind ->
      Gl.get_active_uniform
        pid ind buffer_size
        (Some int_buffer)
        (Bigarray.Array1.sub int_buffer 1 1)
        (Bigarray.Array1.sub int_buffer 2 1)
        buffer;
      let len = Bigarray.Array1.unsafe_get int_buffer 0 in
      let size = Bigarray.Array1.unsafe_get int_buffer 1 in
      let typ = Bigarray.Array1.unsafe_get int_buffer 2 in
      let name = Bigstring.to_string ~len:(Int.of_int32_exn len) buffer in
      (name,size,Types.Data.Numeric.of_gl (Int.of_int32_exn typ))
    end in
    ret uniforms
  
  let uniforms_with_options {pid; _} =
    let open Comp.Syntax in
    let+ no_uniforms =
      get_int (Gl.get_programiv pid Gl.active_uniforms)
      |> Comp.of_option ~error:(Error.of_string "Can not get program attribute count") in
    let buffer_size = 128 in
    let int_buffer = Bigarray.Array1.create Bigarray.int32 Bigarray.c_layout 3 in
    let buffer = Bigstring.create buffer_size in
    let+ uniforms =
    List.range 0 no_uniforms
    |> List.map ~f:begin fun ind ->
      Gl.get_active_uniform
        pid ind buffer_size
        (Some int_buffer)
        (Bigarray.Array1.sub int_buffer 1 1)
        (Bigarray.Array1.sub int_buffer 2 1)
        buffer;
      let len = Bigarray.Array1.unsafe_get int_buffer 0 in
      let size = Bigarray.Array1.unsafe_get int_buffer 1 in
      let typ = Bigarray.Array1.unsafe_get int_buffer 2 in
      let name = Bigstring.to_string ~len:(Int.of_int32_exn len) buffer in
      let gl_typ = Types.Data.Numeric.of_gl (Int.of_int32_exn typ) in
      let+ () =
        match gl_typ with
        | None ->
          let+ () = Comp.warn (Printf.sprintf "uniform variable %s has unsupported type %ld" name typ) in
          Comp.return ()
        | Some _ -> Comp.return ()
      in
      ret @@ (name,size,gl_typ)
    end |> Comp.all in
    ret uniforms

  let uniforms {pid; _} =
    let open Comp.Syntax in
    let+ no_uniforms =
      get_int (Gl.get_programiv pid Gl.active_uniforms)
      |> Comp.of_option ~error:(Error.of_string "Can not get program attribute count") in
    let buffer_size = 128 in
    let int_buffer = Bigarray.Array1.create Bigarray.int32 Bigarray.c_layout 3 in
    let buffer = Bigstring.create buffer_size in
    let+ uniforms =
    List.range 0 no_uniforms
    |> List.map ~f:begin fun ind ->
      Gl.get_active_uniform
        pid ind buffer_size
        (Some int_buffer)
        (Bigarray.Array1.sub int_buffer 1 1)
        (Bigarray.Array1.sub int_buffer 2 1)
        buffer;
      let len = Bigarray.Array1.unsafe_get int_buffer 0 in
      let size = Bigarray.Array1.unsafe_get int_buffer 1 in
      let typ = Bigarray.Array1.unsafe_get int_buffer 2 in
      let name = Bigstring.to_string ~len:(Int.of_int32_exn len) buffer in
      let gl_typ = Types.Data.Numeric.of_gl (Int.of_int32_exn typ) in
      match gl_typ with
      | None ->
        let+ () =
          if Option.is_some @@ Types.Data.Texture.of_gl (Int.of_int32_exn typ) then Comp.return () else
          Comp.warn (Printf.sprintf "uniform variable %s has unsupported type %ld" name typ) in
        Comp.return None
      | Some typ -> Comp.return (Some ((ind, name,size,typ)))
    end |> Comp.all |> Comp.map ~f:(List.filter_opt) in
    ret uniforms

  (** returns a mapping of sampler_name, texture_type, uniform_id, texture_unit *)
  let textures program =
    let open Comp.Syntax in
    let+ textures = textures_raw program.pid in
    let textures =
      if program.textures_bound
      then List.mapi textures
          ~f:(fun ind (uniform_id, uniform_name, _size, Types.Data.Texture.MkFieldType
                         typ) ->
              Gl.uniform1i uniform_id ind;
              uniform_name,
              Types.Data.Texture.MkField
                (Types.Data.Texture.{
                    uniform_id;
                    texture_unit=Texture.MkUnit {uniform_id; texture_unit=ind};
                    texture_type=typ;
                  })
            )
      else List.mapi textures
          ~f:(fun ind (uniform_id, uniform_name, _size, Types.Data.Texture.MkFieldType
                         typ) ->
              uniform_name,
              Types.Data.Texture.MkField
                (Types.Data.Texture.{
                    uniform_id;
                    texture_unit=Texture.MkUnit {uniform_id; texture_unit=ind};
                    texture_type=typ;
                  })
            )
    in
    program.textures_bound <- true;
    Comp.return textures
  
  let delete_program program =
    Gl.delete_program program.pid

end


module Camera = struct

  type move_state =
    | Stationary of v3
    | Moving of v3 * v3

  let move (move_state: move_state) (f: v3 -> v3) =
    match move_state with
    | Stationary start -> Moving (start, f start)
    | Moving (start, desired) ->
      let new_pos = f desired in
      let new_direction = V3.(new_pos - start) in
      let new_mag = V3.norm new_direction  in
      if Float.(new_mag >= 5.0) then begin
        let old_mag = V3.norm V3.(start - desired) in
        let new_offset = V3.(old_mag * (new_direction / new_mag)) in
        Moving (start, V3.(start + new_offset))
      end
      else Moving (start, new_pos)

  let update (move_state: move_state) (max_distance_delta: float) =
    match move_state with
    | Stationary start -> Stationary start
    | Moving (start, desired) ->
      let direction = V3.(desired - start)  in
      let distance = V3.norm direction in
      if (Float.(max_distance_delta >= distance) || Float.(abs distance <= 0.0001))
      then Stationary desired
      else Moving (V3.(start +  max_distance_delta * (direction / distance)), desired)

  let to_vec (move_state: move_state) =
    match move_state with
    | Stationary v -> v
    | Moving (current, _) -> current

  
  type t = {
    camera_pos: move_state;
    camera_up: v3;
    yaw: float;
    pitch: float; fov: float;
    base_camera_speed: float;
  }

  let init () = {
    camera_pos = Stationary (V3.v 0.0 0.0 3.0);
    camera_up = V3.oy;
    yaw = 270.0;
    pitch = 0.0;
    fov = 40.0;
    base_camera_speed = 3.0;
  }

  let reset (camera: t) =
    {camera with camera_up=V3.oy; fov=40.0}
  

  let camera_front camera =
    let direction =
      let open Float in
      V3.v
        (cos (Gg.Float.rad_of_deg camera.yaw) *. cos (Gg.Float.rad_of_deg camera.pitch))
        (sin (Gg.Float.rad_of_deg camera.pitch))
        (sin (Gg.Float.rad_of_deg camera.yaw) *. cos (Gg.Float.rad_of_deg camera.pitch))
    in
    (let open V3 in direction / norm direction)  

  let update (camera: t) dt =
    {camera with camera_pos = update camera.camera_pos (camera.base_camera_speed *. dt)}

  let process_event camera evt _delta =
    let camera_speed = 10. (* camera.base_camera_speed *) *. _delta in
    let camera_front = camera_front camera in
    let open Input in 
    match evt with
    | CharacterInput ('e', _, _) ->
      {camera with camera_up = V3.ltr (M3.rot3_axis camera_front (0.1 *. camera_speed)) camera.camera_up }
    | CharacterInput ('q', _, _) ->
      {camera with camera_up = V3.ltr (M3.rot3_axis camera_front (-.0.1 *. camera_speed)) camera.camera_up }
    | CharacterInput ('r', _, _) -> reset camera

    | CharacterInput ('1', _, _) ->
      {camera with camera_pos = move camera.camera_pos (fun pos -> V3.(pos + camera_speed * V3.oy)) }
    | CharacterInput ('2', _, _) ->
      {camera with camera_pos = move camera.camera_pos (fun pos -> V3.(pos - camera_speed * V3.oy)) }

    | CharacterInput ('3', _, _) ->
      {camera with fov = camera.fov +. (3.0 *. camera_speed) }
    | CharacterInput ('4', _, _) ->
      {camera with fov = camera.fov -. (3.0 *. camera_speed) }

    | CharacterInput ('w', _, _) ->
      {camera with camera_pos =
                     move camera.camera_pos (fun pos -> V3.(pos + camera_speed * camera_front))
      }
    | CharacterInput ('s', _, _) ->
      {
        camera with camera_pos =
                      move camera.camera_pos (fun pos -> V3.(pos - camera_speed * camera_front))
      } 
    | CharacterInput ('a', _, _) ->
      {camera with camera_pos =
                     move camera.camera_pos
                     (fun pos  -> V3.(pos - camera_speed *
                                                   (let vec = cross camera_front camera.camera_up in
                                                    vec / norm vec)))} 
    | CharacterInput ('d', _, _) ->
      {camera with camera_pos =
                     move camera.camera_pos
                       (fun pos -> V3.(pos + camera_speed *
                                                   (let vec = cross camera_front camera.camera_up in
                                                    vec / norm vec)))} 
    | MouseMotion (x_offset, y_offset) ->
      let sensitivity = 0.1 in
      let x_offset = sensitivity *. Float.of_int x_offset in
      let y_offset = sensitivity *. Float.of_int y_offset in
      let camera_yaw =  camera.yaw +. x_offset in
      let camera_pitch = camera.pitch -. y_offset in
      let camera_pitch =
        if Float.(camera_pitch > 89.0) then 89.0
        else if Float.(camera_pitch < -. 89.0) then -.89.0
        else camera_pitch
      in
      {
        camera with
        yaw = camera_yaw;
        pitch = camera_pitch
      }
    | _ -> camera

  let to_view_projection graphics camera =
    let camera_front = camera_front camera in
    let pos = (to_vec camera.camera_pos) in
    let view =
      Glm.M4.look_at
        ~eye:pos
        ~target:V3.(pos + camera_front)
        ~updir:camera.camera_up in
    let proj =
      Glm.M4.perspective
        ~fov:(Gg.Float.rad_of_deg camera.fov)
        ~aspect:Float.(let w,h = Window.logical_dims graphics in
                       of_int w /. of_int h)
        ~near:(0.001)
        ~far:(100000.0) in
    (view,proj)

end
