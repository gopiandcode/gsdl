open Core
open Utils

let buf_size = 256

let parse_file filename =
  let open Comp.Syntax in
  (* let open Option.Syntax in *)
  let+ binary_proportion = 
    let+ file =
      try ret @@ Unix.openfile ~mode:[Unix.O_RDONLY] filename with
        Sys_error e -> Comp.fail (Error.of_string e)            
      | Unix.Unix_error (_, e,_) -> (Comp.fail (Error.of_string e))
    in
    let buf = Bigstring.create buf_size in
    let read_count = Bigstring.read file ~len:buf_size buf in
    Unix.close file;
    let rec loop acc i =
      if i < read_count
      then begin
        let value = Bigstring.get_uint8 buf ~pos:i in
        if (value >= 0x20 && value <= 0x7f) ||
           (value = Char.to_int '\n') ||
           (value = Char.to_int '\t') ||
           (value = Char.to_int '\r')
        then loop (acc + 1) (i + 1)
        else loop acc (i + 1)
      end
      else read_count - acc
    in
    let binary_proportion = Float.of_int (loop 0 0) /. Float.of_int read_count in
    Comp.return binary_proportion in
  if Float.(binary_proportion > 0.05)
  then Binary.parse_file filename
  else Ascii.parse_file filename



