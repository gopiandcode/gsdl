import sys
import os
import re
from pathlib import *
from krita import *

def eprint(string):
        print(string, file=sys.stderr)

def __main__(args):
    if len(args) != 3:
        eprint("Invalid number of arguments (expecting 3 got {})".format(len(args)))
        return
    filename, basename, image_dir = args
    instance = Krita.instance()
    document = instance.openDocument(filename) # open document - no way to handle errors, so just let it explode
    root = document.rootNode()

    layer_name = "{}-out".format(basename)
    out_layer = document.nodeByName(layer_name)

    if not out_layer:
        out_layer = document.createGroupLayer(layer_name)
        root.addChildNode(out_layer, None)
    if not out_layer:
        eprint("Could not create/find output layer {}".format(layer_name))
        return

    image_dir = Path(image_dir)
    if not image_dir.exists() or not image_dir.is_dir():
        eprint("image dir {} is not a directory or does not exist".format(image_dir))
        return
    image_files = {}
    for path in [path for path in image_dir.iterdir() if path.is_file()]:
        match = re.match("export(?P<count>[0-9]*)(?P<position>_[0-9]*)?_(?P<name>.+)", path.stem)

        if match:
            name,count,position = \
                match.group('name'),\
                int(match.group('count') or "1"),\
                int((match.group('position') or "_0")[1:])
            if not name in image_files or image_files[name][1] < count:
                image_files[name] = (path,count,position)
    image_files = list((key, value[0]) for key, value in
                       sorted(image_files.items(), key=lambda x: x[1][2])
    )

    children = {}
    for child in out_layer.childNodes():
        children[child.name()] = child
    
    for (name, path) in image_files:
        full_path = str(path.resolve())
        position = out_layer.position()
        opacity = out_layer.opacity()

        if name in children and children[name].path() != full_path:
            # file exists, but uses wrong path
            layer = document.createFileLayer(name, full_path, "None")
            child = children[name]
            # copy over properties from child
            alpha_locked = child.alphaLocked()
            inherit_alpha = child.inheritAlpha()
            blending_mode = child.blendingMode()
            color_label = child.colorLabel()
            locked = child.locked()
            visible = child.visible()
            position, opacity = child.position(), child.opacity()
            animated = child.animated()
            
            if animated:
                layer.enableAnimation()
            layer.setColorLabel(color_label)
            layer.setAlphaLocked(alpha_locked)
            layer.setBlendingMode(blending_mode)
            layer.setInheritAlpha(inherit_alpha)
            layer.setLocked(locked)
            layer.move(position.x(), position.y())
            layer.setOpacity(opacity)
            layer.setVisible(visible)

            # insert updated layer below old version, then delete
            out_layer.addChildNode(layer, child)
            out_layer.removeChildNode(child)
        elif not name in children:
            # file does not exist
            layer = document.createFileLayer(name, full_path, "None")
            layer.move(position.x(), position.y())
            layer.setOpacity(opacity)
            out_layer.addChildNode(layer, None)

    document.save()
    document.refreshProjection()

