FILE=${1?Scene file to debug}
opam exec dune build && apitrace trace ./_build/default/main.exe -output ./images "$FILE"
