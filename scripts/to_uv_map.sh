#!/bin/bash
## Script to convert an STL file to an wavefront .obj file with automatic texture mapping (Requires blender)

read -r -d '' OBJ_CONVERT_SCRIPT <<EOF  
#!/bin/python3
import bpy, bmesh
import sys
import pathlib

filename =  sys.argv[5]
outname =  sys.argv[6]
pure_name = pathlib.Path(filename).stem.capitalize()

print(pure_name)

bpy.ops.import_mesh.stl(filepath=filename)
print(list(bpy.data.objects.keys()))
dir(bpy.context.scene.objects)
bpy.context.view_layer.objects.active = bpy.data.objects[pure_name]
obj = bpy.context.active_object
me = obj.data
bpy.ops.object.mode_set(mode = 'EDIT')


bm = bmesh.from_edit_mesh(me)
bpy.ops.uv.smart_project()
# mat = bpy.data.materials.new(pure_name)
# me.materials.append(mat)
bmesh.update_edit_mesh(me)

for (obj,name) in bpy.context.scene.objects.items():
    if obj != pure_name:
        bpy.data.objects.remove(name)

bpy.ops.export_scene.obj(filepath=outname)
EOF

FILE=${1:?stl File to uvmap}
OUT_FILE=${2:?Name of output file}

base_name=$(basename $FILE)
stem=${base_name/.stl/}
stem="${stem^}"

script_file=$(mktemp)
echo "$OBJ_CONVERT_SCRIPT" > "$script_file"

blender -b --python  "$script_file"  -- $FILE $OUT_FILE
