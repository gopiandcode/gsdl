[@@@warning "-33"]
[@@@warning "-26"]
(* open Utils *)
(* open Bin_prot.Std *)
open Gsdl
open Gg
open Tgl3
open Core
open Utils

module State = struct

  type t = {
    raw_program_manager: Scene_definition_language.RawProgramSynchronizer.t;
    compilation_manager: Scene_definition_language.CompilationManager.t;
    serialization_manager: Scene_definition_language.SerializationManager.t;
    shader_manager: Shaders.BasicShaderSynchronizer.t;
    texture_manager: Image_synchronizer.t;
    mesh_manager: Geometry_synchronizer.t;
    tbl: Scene_definition_language.RawProgramSynchronizer.Source.state;
    shader_tracker: Sync.FileTag.t;
  }

  let create
    ~raw_program_manager ~compilation_manager ~serialization_manager
    ~shader_manager ~texture_manager ~mesh_manager
    ~tbl ~shader_tracker = {
    raw_program_manager; compilation_manager; serialization_manager;
    shader_manager; texture_manager; mesh_manager; tbl; shader_tracker;
  }

  open Comp.Syntax 

  let update_shader_manager state =
    let+ ((), shader_manager) = Shaders.BasicShaderSynchronizer.update_mapping () state.shader_manager in
    ret {state with shader_manager}

  let update_texture_manager state =
    let+ ((), texture_manager) = Image_synchronizer.update_mapping () state.texture_manager in
    ret {state with texture_manager}

  let update_mesh_manager state =
    let+ ((), mesh_manager) = Geometry_synchronizer.update_mapping () state.mesh_manager in
    ret {state with mesh_manager}


  let update_shader_tracker state =
    let+ shader_tracker = Sync.FileTag.update_mapping state.shader_tracker in
    ret {state with shader_tracker}

  let update_program_manager state =
    let+ (tbl, raw_program_manager) =
      Scene_definition_language.RawProgramSynchronizer.update_mapping
        state.tbl state.raw_program_manager in
    ret {state with tbl; raw_program_manager}


  let update_compilation_manager state =
        let+ (((((), texture_manager), ((), shader_manager), ((), mesh_manager)), tbl, raw_program_manager,
               shader_tracker), compilation_manager) = Scene_definition_language.CompilationManager.update
            ((((), state.texture_manager), ((), state.shader_manager), ((), state.mesh_manager)), state.tbl, state.raw_program_manager,
             state.shader_tracker) state.compilation_manager in
        ret {state with texture_manager; shader_manager; mesh_manager; tbl;
                        raw_program_manager; shader_tracker; compilation_manager }
    

  let update_serialization_manager state graphics_state =
        let+ (_, ((((), texture_manager), ((), shader_manager), ((), mesh_manager)), tbl, raw_program_manager,
                               shader_tracker), compilation_manager), serialization_manager =
          Scene_definition_language.SerializationManager.update (
            graphics_state, ((((), state.texture_manager), ((), state.shader_manager), ((), state.mesh_manager)),
                             state.tbl, state.raw_program_manager, state.shader_tracker), state.compilation_manager)
            state.serialization_manager in
        ret {texture_manager; shader_manager; mesh_manager; tbl;
                        raw_program_manager; shader_tracker; compilation_manager; serialization_manager }
    

  let state_on_resize state graphics_state =
    let+ (_,
          ((((), texture_manager),
            ((), shader_manager),
            ((), mesh_manager)), tbl,
           raw_program_manager, shader_tracker),
          compilation_manager),
         serialization_manager =
      Scene_definition_language.SerializationManager.flush_all (
            graphics_state, ((((), state.texture_manager), ((), state.shader_manager), ((), state.mesh_manager)),
                             state.tbl, state.raw_program_manager, state.shader_tracker), state.compilation_manager)
            state.serialization_manager in
        ret {texture_manager; shader_manager; mesh_manager; tbl;
                        raw_program_manager; shader_tracker; compilation_manager; serialization_manager }





  let update_state state graphics_state =
    let+ state = update_shader_manager state in
    let+ state = update_texture_manager state in
    let+ state = update_mesh_manager state in
    let+ state = update_shader_tracker state in
    let+ state = update_program_manager state in
    let+ state = update_compilation_manager state in
    let+ state = update_serialization_manager state graphics_state in
    ret state

end

let main ?logical_dimensions ?output_dir scene_files =
  let module Camera = Graphics_utils.Camera in
  let module Timer = Utils.Timer in

  let open Comp.Syntax in
  let+ g = Window.init ?logical_dimensions () in
  let camera = Camera.init () in
  (* identifiers will be shared across reloads *)

  let+ state, program_ids =
    let tbl = Gsdl.SanitizedParsetree.IdentifierTable.init () in
    let+ raw_program_manager = Scene_definition_language.RawProgramSynchronizer.init () in
    let+ compilation_manager = Scene_definition_language.CompilationManager.init () in
    let+ serialization_manager = Scene_definition_language.SerializationManager.init () in
    let+ shader_manager = Shaders.BasicShaderSynchronizer.init () in
    let+ mesh_manager = Geometry_synchronizer.init () in
    let+ shader_tracker = Sync.FileTag.init () in
    let+ texture_manager = Image_synchronizer.init () in

    let+ ((tbl, raw_program_manager), program_ids) =
      Comp.fold_map ~init:(tbl, raw_program_manager) scene_files ~f:(fun (tbl, raw_program_manager) file ->
          let+ (tbl, program_id, raw_program_manager) =
            Scene_definition_language.RawProgramSynchronizer.load_data tbl
              [(file,file)]
              raw_program_manager in
          let+ time_stamp =
            Scene_definition_language.RawProgramSynchronizer.get_timestamp program_id raw_program_manager in
          ret ((tbl, raw_program_manager), (program_id, time_stamp))
        )
    in
    let+ (compilation_manager, program_ids) =
      Comp.fold_map ~init:compilation_manager program_ids ~f:(fun state program_id ->
          let+ (state, program_id) =
            Scene_definition_language.CompilationManager.load_data program_id state in
          let+ time_stamp = Scene_definition_language.CompilationManager.get_timestamp program_id state in
          ret (state, (program_id, time_stamp))
        ) in

    let+ serialization_manager, program_ids =
      Comp.fold_map ~init:serialization_manager program_ids ~f:(fun serialization_manager program_id ->
          let+ serialization_manager, program_id =
            Scene_definition_language.SerializationManager.load_data program_id serialization_manager in
          let+ timestamp =
            Scene_definition_language.SerializationManager.get_timestamp program_id serialization_manager in
          ret (serialization_manager, (program_id,timestamp))
        )
    in
    ret @@ (State.create
      ~raw_program_manager
      ~compilation_manager
      ~serialization_manager
      ~shader_manager
      ~texture_manager
      ~mesh_manager
      ~tbl
      ~shader_tracker, program_ids)
     in
    
  

  (* Gl.polygon_mode Gl.front_and_back Gl.line; *)
  Gl.point_size 40.0;

  Gl.enable Gl.depth_test;
  Gl.disable Gl.cull_face_enum;
  Gl.cull_face Gl.back;
  Gl.front_face Gl.ccw;

  let timer = Timer.init () in

  let+ hot_reload_update_timer =
    Timer.Periodic.create timer
      ~periodicity:0.5
      (fun
        (state, graphics_state) ->
        let+ state = State.update_state state graphics_state in
        ret (state, graphics_state) ) in

  Window.main_loop g ~init:(camera, state, program_ids, timer,
                            hot_reload_update_timer, false, 0
                           )
    ~f:(fun
         (camera, state, program_ids, timer, hot_reload_update_timer, draw_lines, output_count)
         graphics ->

         let+ graphics, events = (Window.poll_events graphics) in

         (* handle input *)
         let graphics, camera, draw_lines, save_output =
           List.fold events
             ~init:(graphics,camera, draw_lines, false)
             ~f:(fun (graphics,camera, draw_lines, save_output) evt -> let open Input in
                  let graphics,camera, draw_lines, save_output =
                    begin match evt with
                        CharacterInput ('x', _, _) -> Window.queue_exit graphics, camera, draw_lines, save_output
                      | CharacterInput ('l', _, _) -> graphics, camera, not draw_lines, save_output
                      | CharacterInput ('p', _, _) -> graphics, camera, draw_lines, true
                      | _ -> graphics, camera, draw_lines, save_output end in
                  graphics, Camera.process_event camera evt timer.delta_time, draw_lines, save_output) in

         let (_current_time, timer, o_fps) =
           Timer.update timer in

         let graphics_state = Window.to_graphics_state graphics in

         let+ state =
           if graphics_state.resized          
           then State.state_on_resize state graphics_state
           else ret state in

         let+ (hot_reload_update_timer, (state, _)) =
           Timer.Periodic.update timer hot_reload_update_timer
             (state, graphics_state) in
         begin 
           match o_fps with
           | None -> ()
           | Some fps ->
             Window.set_title g (Printf.sprintf "stl-viewer @ fps: %.2f" fps)
         end;

         let camera = Camera.update camera timer.delta_time in
         let (view,proj) = Camera.to_view_projection graphics camera in

         let output_count = if save_output then output_count + 1 else output_count in

         let+ () = Comp.fold ~init:() program_ids ~f:(fun () (program_id, _) ->
             let+ drawable =
               Scene_definition_language.SerializationManager.get_data program_id state.serialization_manager in
             match drawable with
             | None -> ret ()
             | Some drawable ->
               let+ () = Scene.DrawableObject.Render.draw
                   ~camera_front:(Camera.camera_front camera) ~camera_position:(Camera.to_vec camera.camera_pos)
                   ~camera_yaw:camera.yaw ~camera_pitch:camera.pitch ~camera_up:camera.camera_up
                   ~width:graphics_state.width ~height:graphics_state.height
                   ~logical_width:graphics_state.logical_width ~logical_height:graphics_state.logical_height
                   ~x:graphics_state.x ~y:graphics_state.y
                   ~projection:proj ~view:view
                   ~draw_operation:(if draw_lines then Shaders.BasicShader.Lines else Shaders.BasicShader.Triangles)
                   drawable in
               let+ () = match save_output, output_dir with
                   true, Some dir -> begin
                     let+ () = Comp.warn (Printf.sprintf "exporting drawables to %s (%dx%d + %d,%d)" dir
                                            graphics_state.width graphics_state.height
                                            graphics_state.x graphics_state.y) in
                     let+ images = Scene.DrawableObject.Render.print
                         ~camera_front:(Camera.camera_front camera) ~camera_position:(Camera.to_vec camera.camera_pos)
                         ~camera_yaw:camera.yaw ~camera_pitch:camera.pitch ~camera_up:camera.camera_up
                         ~width:graphics_state.width ~height:graphics_state.height
                         ~logical_width:graphics_state.logical_width
                         ~logical_height:graphics_state.logical_height
                         ~x:graphics_state.x ~y:graphics_state.y
                         ~projection:proj ~view:view
                         ~draw_operation:(if draw_lines then Shaders.BasicShader.Lines else Shaders.BasicShader.Triangles)
                         drawable in
                     let images =
                       List.map images ~f:(fun (name, image) ->
                           Printf.sprintf "%s/export%d_%s.png" dir output_count name, image
                         ) in
                     Comp.fold images ~init:() ~f:(fun () (filename, image) ->
                         Comp.try_with (fun () -> Image.write_to_file ~filename image)
                         |> Comp.map_error ~f:(fun exn -> Error.of_exn exn)
                       )
                   end
                 | _ ->  ret () in
               ret ()
           ) in
         ret @@ ((camera, state,
                  program_ids, timer, hot_reload_update_timer, draw_lines, output_count), graphics)
       )


let run_main ?logical_dimensions ?output_dir shader_files =
  let values = (Comp.run (main ?logical_dimensions ?output_dir shader_files)) in
  match values with
  | Ok (_, warnings) ->
    let warnings = String.concat ~sep:"\n" warnings in
    print_endline (Printf.sprintf "Completed Successfully with remaining warnings: %s" warnings)
  | Error e -> print_endline (Printf.sprintf "Errored out with error: %s" (Error.to_string_hum e))

let run () =
  let dimension_param : (int * int) Command.Arg_type.t =
    Command.Arg_type.create
      (fun str ->
         match String.split_on_chars ~on:['x'] str with
         | [width; height] -> Int.of_string width, Int.of_string height
         | _ ->
           raise (Failure(
               Printf.sprintf "\"%s\" is not a valid dimension specifier (expecting widthxheight)" str))
      )  in
  let command =
    let open Command.Let_syntax in
    Command.basic
      ~summary:"Render the provided scene definition file with hot reloading"
      [%map_open
        let output_dir = flag  "output"
            (optional string)
            ~doc:"Directory to output rendered images to (must exist)."
        and logical_dimensions = flag "dimensions"
            (optional dimension_param)
            ~doc:"Tuple representing the width x height of the output."
        and files = anon (non_empty_sequence_as_list ("scene_definition_files" %: string))
        in
        fun () ->
          run_main ?logical_dimensions ?output_dir files
      ] in
  Command.run
    command
  
let () =
  run ()
