open Gg
open Core
open Utils


type ast =
  | Vertex of float * float * float
  | Texture of float * float
  | Normal of float * float * float
  | Face of int list
  | FaceTex of (int * int) list
  | FaceNorm of (int * int) list
  | FaceTexNorm of (int * int * int) list
[@@deriving show]

type data = 
  | VertexData of float * float * float
  | TextureData of float * float
  | NormalData of float * float * float

type face = 
  | FaceData of int list
  | FaceTexData of (int * int) list
  | FaceNormData of (int * int) list
  | FaceTexNormData of (int * int * int) list


let split_ast (elem: ast) =
  let left v = (Some v, None) in
  let right v = (None, Some v) in
  match elem with
  | Vertex (a, b, c) -> left (VertexData (a,b,c))
  | Texture (a, b) -> left (TextureData (a,b))
  | Normal (a, b, c) -> left (NormalData (a,b,c))
  | Face f -> right (FaceData f)
  | FaceTex f -> right (FaceTexData f)
  | FaceNorm f -> right (FaceNormData f)
  | FaceTexNorm f -> right (FaceTexNormData f)

let split_parsed (ls: ast list) =
  let data, faces = List.map ~f:split_ast ls |> List.unzip in
  let data, faces = List.filter_opt data, List.filter_opt faces in
  data,faces

let to_mesh (data: data list) (faces: face list) =
  let (vertex_data, normal_data, texture_data) =
    List.fold ~init:([],[],[]) data
      ~f:(fun (vertex_data, normal_data, texture_data) args ->
          match args with
          | VertexData (v1, v2, v3) ->
            (V3.v v1 v2 v3 :: vertex_data, normal_data, texture_data)
          | TextureData (v1, v2) ->
            vertex_data, normal_data, V2.v v1 v2 :: texture_data
          | NormalData (v1, v2, v3) ->
            vertex_data, V3.v v1 v2 v3 :: normal_data, texture_data
        ) in
  let (vertex_data, normal_data, texture_data) =
    (Array.of_list @@ List.rev vertex_data,
     Array.of_list @@ List.rev normal_data,
     Array.of_list @@ List.rev texture_data) in
  let triangulate f a b cs =
    let rec loop acc x1 x2 xs = 
      match xs with
      | [] -> List.rev acc
      | x3 :: xs -> let result = f x1 x2 x3 in
        match result with
        | None -> loop acc x1 x3 xs
        | Some result ->  loop (result :: acc) x1 x3 xs
    in
    loop [] a b cs
  in
  let face_data =
    List.map faces
      ~f:(fun args ->
          let process3 ?(print=false) ls f g  =
            begin
              match ls with
              | [l1; l2; l3] ->
                if print then Printf.printf "\tno reduction needed (1 face)\n";
                let data = f l1 l2 l3 in
                Option.map data ~f:(fun data -> (g [data]))
              | x1 :: x2 :: xs when List.length xs > 0 ->
                let result =
                  triangulate (fun l1 l2 l3 -> f l1 l2 l3) x1 x2 xs  in
                if print then Printf.printf "\treduced to %d elements\n" (List.length result);
                Some (g result)
              | _ -> None   
            end in
          let open Geometry in
          let normalize arr ind =
            let len = Array.length arr in
            let ind =
              if ind <= 0
              then len + ind
              else ind - 1 in
            if ind < len
            then Some ind
            else None  in
          let norm3 arr i1 i2 i3 =
            let i1 = normalize arr i1 in
            let i2 = normalize arr i2 in
            let i3 = normalize arr i3 in
            match i1,i2,i3 with
            | Some i1, Some i2, Some i3 -> Some (i1, i2, i3)
            | _ -> None
          in
          match args with
          | FaceData ls ->
            process3 ls
              (fun l1 l2 l3 ->
                 match norm3 vertex_data l1 l2 l3  with
                 | None -> None
                 | Some (l1, l2, l3) ->  Some (Mesh.Vertex (l1,l2,l3)))
              (fun v -> Mesh.MkDataV v)
          | FaceTexData ls ->
            process3 ls
              (fun (v1, t1) (v2, t2) (v3, t3) ->
                 let vs = norm3 vertex_data v1 v2 v3 in
                 let ts = norm3 texture_data t1 t2 t3 in
                 match vs,ts with
                 | Some (v1,v2,v3), Some (t1,t2,t3) -> 
                   Some (Mesh.Vertex_with_textures {v1;t1;v2;t2;v3;t3})
                 | _ -> None
              )
              (fun v -> Mesh.MkDataVT v)
          | FaceNormData ls ->
            process3 ls
              (fun (v1, n1) (v2, n2) (v3, n3) ->
                 let vs = norm3 vertex_data v1 v2 v3 in
                 let ns = norm3 normal_data n1 n2 n3 in
                 match vs,ns with
                 | Some (v1,v2,v3), Some (n1,n2,n3) ->
                   Some (Mesh.Vertex_with_normals {v1;n1;v2;n2;v3;n3})
                 | _ -> None
              )
              (fun v -> Mesh.MkDataVN v)
          | FaceTexNormData ls ->
            process3  ls
              (fun (v1, t1, n1) (v2, t2, n2) (v3, t3, n3) ->
                 let vs = norm3 vertex_data v1 v2 v3 in
                 let ns = norm3 normal_data n1 n2 n3 in
                 let ts = norm3 texture_data t1 t2 t3 in
                 match vs, ts, ns with
                 | Some (v1,v2,v3), Some (t1,t2,t3), Some (n1,n2,n3) ->
                   Some (Mesh.Vertex_with_texture_and_normals {v1;t1;n1;v2;t2;n2;v3;t3;n3})
                 | _ -> None
              )
              (fun v -> Mesh.MkDataVTN v)
        )
    (* |> (fun ls ->
     *     let len = List.length ls in
     *     let len_filtered = List.filter_opt ls |> List.length in
     *     Printf.printf "filtering length from %d to %d\n" len len_filtered;
     *     ls
     *   ) *)
    |> List.filter_opt
  in
  let face_data =
    let open Geometry in
    match face_data with
    | [] -> None
    | ((MkDataV vl) :: t) as ls ->
      List.filter_map ls
        ~f:(fun l ->
            match l with
            | Mesh.MkDataV l -> Some l
            | _ -> None)
      |> List.concat
      |> (fun l -> Some (Mesh.MkDataV l))
    | ((MkDataVN vl) :: t) as ls ->
      List.filter_map ls
        ~f:(fun l ->
            match l with
            | Mesh.MkDataVN l -> Some l
            | _ -> None)
      |> List.concat
      |> (fun l -> Some (Mesh.MkDataVN l))
    | ((MkDataVT vl) :: t) as ls ->
      List.filter_map ls
        ~f:(fun l ->
            match l with
            | Mesh.MkDataVT l -> Some l
            | _ -> None)
      |> List.concat
      |> (fun l -> Some (Mesh.MkDataVT l))
    | ((MkDataVTN vl) :: t) as ls ->
      List.filter_map ls
        ~f:(fun l ->
            match l with
            | Mesh.MkDataVTN l -> Some l
            | _ -> None)
      |> List.concat
      |> (fun l -> Some (Mesh.MkDataVTN l))
  in
  match face_data with
  | None -> None
  | Some faces ->
    Some Geometry.Mesh.{vertex_data; normal_data; texture_data; faces}

let parse_file file =
  let open Comp.Syntax in 
  let+ lines =
    try
      ret (In_channel.read_lines file)
    with
      Sys_error e -> (Comp.fail (Error.of_string e)) in

  (*  remove whitespace and comments *)
  let lines =   List.map ~f:(String.strip) lines
                |> List.filter_mapi
                  ~f:(fun ind line -> if String.is_prefix ~prefix:"#" line then None else Some (ind, line))  in

  let+ lines =
    List.map lines
      ~f:(fun (ind, line) ->
          let components = String.split ~on:' ' line |> List.map ~f:String.strip
                           |> List.filter ~f:(fun component -> not @@ String.is_empty component)  in
          match components with
          | "f" :: rs -> begin
              match List.map ~f:(String.split ~on:'/') rs with
              | ([v1] :: rs) as ls  ->
                let parsed =
                  List.map ls ~f:(fun component ->
                      match List.map ~f:(Int.of_string) component with
                      | [a] -> Some (a)
                      | _ -> None
                    ) in
                begin
                  match Option.all parsed with
                  | None ->
                    let+ () = Comp.warn (Printf.sprintf  "invalid face specification at line %d" ind) in
                    Comp.return @@ None
                  | Some ls ->
                    Comp.return @@ Some (Face ls)
                end
              | ([v1; t1] :: rs) as ls  ->
                let parsed =
                  List.map ls ~f:(fun component ->
                      match List.map ~f:(Int.of_string) component with
                      | [a;b] -> Some (a,b)
                      | _ -> None
                    ) in
                begin
                  match Option.all parsed with
                  | None ->
                    let+ () = Comp.warn (Printf.sprintf  "invalid face with texture specification at line %d" ind) in
                    Comp.return @@ None
                  | Some ls ->
                    Comp.return @@ Some (FaceTex ls)
                end
              | ([v1; "";  n1] :: rs ) as ls ->
                let parsed =
                  List.map ls ~f:(fun component ->
                      let component = List.map ~f:(String.strip) component
                                      |> List.filter ~f:(fun elem -> not @@ String.is_empty elem) in
                      match List.map ~f:(Int.of_string) component with
                      | [a;b] -> Some (a,b)
                      | _ -> None
                    ) in
                begin
                  match Option.all parsed with
                  | None ->
                    let+ () = Comp.warn (Printf.sprintf  "invalid face with normal specification at line %d" ind) in
                    Comp.return @@ None
                  | Some ls ->
                    Comp.return @@ Some (FaceNorm ls)
                end
              | ([v1; t1;  n1] :: rs) as ls  ->
                let parsed =
                  List.map ls ~f:(fun component ->
                      match List.map ~f:(Int.of_string) component with
                      | [a;b;c] -> Some (a,b,c)
                      | _ -> None
                    ) in
                begin
                  match Option.all parsed with
                  | None ->
                    let+ () = Comp.warn (Printf.sprintf  "invalid face with texture specification at line %d" ind) in
                    Comp.return @@ None
                  | Some ls ->
                    Comp.return @@ Some (FaceTexNorm ls)
                end
              | _ -> 
                let+ () = Comp.warn (Printf.sprintf  "invalid face specification at line %d" ind) in
                Comp.return @@ None
            end
          | "v" :: rs -> begin
              match List.map ~f:(fun vl -> Float.of_string vl) rs with
              | [v1; v2; v3] -> Comp.return @@ Some (Vertex (v1,v2,v3))
              | v1 :: v2 :: v3 :: rs ->
                let+ () = 
                  if List.length rs > 0
                  then Comp.warn ("discarding extra components of vector")
                  else ret () in
                Comp.return @@ Some (Vertex (v1,v2,v3))
              | _ ->
                let+ () = Comp.warn (Printf.sprintf  "invalid vertex specification at line %d" ind) in
                Comp.return @@ None
            end
          | "vn" :: rs -> begin
              match List.map ~f:(fun vl -> Float.of_string vl) rs with
              | [v1; v2; v3] -> Comp.return @@ Some (Normal (v1,v2,v3))
              | v1 :: v2 :: v3 :: rs ->
                let+ () = 
                  if List.length rs > 0
                  then Comp.warn ("discarding extra components of vector")
                  else ret () in
                Comp.return @@ Some (Normal (v1,v2,v3))
              | _ ->
                let+ () = Comp.warn (Printf.sprintf  "invalid vertex specification at line %d" ind) in
                Comp.return @@ None
            end
          | "vt" :: rs -> begin
              match List.map ~f:(fun vl -> Float.of_string vl) rs with
              | [v1; v2] -> Comp.return @@ Some (Texture (v1,v2))
              | v1 :: v2 :: rs ->
                let+ () = 
                  if List.length rs > 0
                  then Comp.warn ("discarding extra components of vector")
                  else ret () in
                Comp.return @@ Some (Texture (v1,v2))
              | _ ->
                let+ () = Comp.warn (Printf.sprintf  "invalid texture vertex specification at line %d" ind) in
                Comp.return @@ None
            end
          | "l" :: _ ->
            let+ () = Comp.warn (Printf.sprintf  "ignoring unsupported line data at line %d" ind) in
            Comp.return @@ None
          | "g" :: _ ->
            let+ () = Comp.warn (Printf.sprintf  "ignoring unsupported group data at line %d" ind) in
            Comp.return @@ None
          | "usemtl" :: _
          | "mtllib" :: _ -> 
            let+ () = Comp.warn (Printf.sprintf  "ignoring mtl usage at line %d" ind) in
            Comp.return None
          | _ ->
            let+ () = Comp.warn (Printf.sprintf  "ignoring unknown line %s usage at line %d" line ind) in
            Comp.return None
        )
    |> Comp.all
  in
  let lines = List.filter_opt lines in
  let (data, faces) = split_parsed lines in
  Comp.return @@ (to_mesh data faces)

