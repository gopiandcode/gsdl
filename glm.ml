[@@@warning "-33"]
open Tgl3
open Gg
open Core

module M4 = struct

  let translation_from_raw ~x ~y ~z =
    let open Float in
    M4.v
      1.0 0.0 0.0 x
      0.0 1.0 0.0 y
      0.0 0.0 1.0 z
      0.0 0.0 0.0 1.0

  let scale_from_raw ~x ~y ~z =
    let open Float in
    M4.v
      x   0.0 0.0 0.0
      0.0 y   0.0 0.0
      0.0 0.0 z   0.0
      0.0 0.0 0.0 1.0

  let translation_from_vec v =
    let open Float in
    M4.v
      1.0 0.0 0.0 (V3.x v)
      0.0 1.0 0.0 (V3.y v)
      0.0 0.0 1.0 (V3.z v)
      0.0 0.0 0.0 1.0

  let scale_from_vec v =
    let open Float in
    M4.v
      (V3.x v)   0.0        0.0        0.0
      0.0        (V3.y v)   0.0        0.0
      0.0        0.0        (V3.z v)   0.0
      0.0        0.0        0.0        1.0

  let x_rotate a     =
    let open Float in
    M4.v
      1.0        0.0        0.0        0.0
      0.0        (cos a)    (-. sin a)  0.0
      0.0        (sin a)    (cos a)    0.0
      0.0        0.0        0.0        1.0

  let y_rotate a     =
    let open Float in
    M4.v
      (cos a)    0.0        (sin a)    0.0
      0.0        1.0        0.0        0.0
      (-. sin a)  0.0        (cos a)    0.0
      0.0        0.0        0.0        1.0

  let z_rotate a     =
    let open Float in
    M4.v
      (cos a)    (-.sin a)   0.0        0.0
      (sin a)    (cos a)    0.0        0.0
      0.0        0.0        1.0        0.0
      0.0        0.0        0.0        1.0

  let rotation ~x ~y ~z =
    let open Float in
    let a = cos x in
    let b = sin x in
    let c = cos y in
    let d = sin y in
    let e = cos z in
    let f = sin z in
    let ad = a *. d in
    let bd = b *. d in
    M4.v
      (c *. e)              (-. c *. f)                d             0.0
      (bd *. e +. a *. f)     (-. bd *. f +. a *. e)       (-. b *. c)     0.0
      (-. ad *. e +. b *. f)   (ad *. f +. b *. e)         (a *. c)       0.0
      0.0                  0.0                      0.0           1.0

  let projection ~width ~height ~fov ~near ~far =
    let open Float in
    let aspect_ratio = width / height in
    let n = near in
    let t = tan (fov / 2.) * near in
    let b = -t in
    let r = t * aspect_ratio in
    let l = -t * aspect_ratio in
    let f = far in
    M4.v
      (2. * n / (r - l))                0.0                    0.0                   0.0
      0.0                               (2.* n/(t-b))          0.0                   0.0
      ((r+l)/(r-l))                     ((t+b)/(t-b))         (-(f+n)/(f-n))         (-1.)
      0.0                               0.0                   (-2.*f*n/(f-n))        0.0

  let perspective ~fov ~aspect ~near ~far =
    let open Float in
    let top = tan (fov / 2.) * near in
    let bot = -top in
    let right = top * aspect in
    let left = -top * aspect in
    M4.persp
      ~left ~right
      ~bot ~top
      ~near ~far


  let look_at ~eye ~target ~updir =
    let f =
      let forward = V3.(eye - target) in
      V3.(forward / norm forward) in

    let l =
      let left = V3.cross updir f in
      V3.(left / norm left) in

    let u = V3.cross f l in

    let t1 =
      V3.(-. (x l) *. (x eye) -. (y l) *. (y eye) -.  (z l) *. (z eye))
    in
    let t2 =
      V3.(-. (x u) *. (x eye) -. (y u) *. (y eye) -.  (z u) *. (z eye))
    in
    let t3 =
      V3.(-. (x f) *. (x eye) -. (y f) *. (y eye) -.  (z f) *. (z eye))
    in
    let open Float in
    M4.v
      (V3.x l)            (V3.y l)             (V3.z l)            t1
      (V3.x u)            (V3.y u)             (V3.z u)            t2    
      (V3.x f)            (V3.y f)             (V3.z f)            t3
      0.0                 0.0                  0.0                 1.0    

  let translate mat vec =
    let col0 = (M4.col 0 mat) in
    let col1 = (M4.col 1 mat) in
    let col2 = (M4.col 2 mat) in
    let col3 = (M4.col 3 mat) in
    let col3' =
      let open V4 in
      (x vec) * col0  +
      (y vec) * col1  +
      (z vec) * col2  +
      col3
    in
    M4.of_cols
      col0
      col1
      col2
      col3'

  let look_at_rh ~eye ~center ~up =
    let f = V3.(let vec = (center - eye) in vec / norm vec) in
    let s = V3.(let vec = cross f up in vec / norm vec) in
    let u = V3.(cross s f) in

    M4.v
      V3.(x s)               V3.(y s)               V3.(z s)                0.0
      V3.(x u)               V3.(y u)               V3.(z u)                0.0
      V3.(x f)               V3.(y f)               V3.(z f)                0.0
      (-. V3.(dot s eye))     (-. V3.(dot u eye))     V3.(dot f eye)          1.0

  let look_at_lh ~eye ~center ~up =
    let f = V3.(let vec = (center - eye) in vec / norm vec) in
    let s = V3.(let vec = cross up f in vec / norm vec) in
    let u = V3.(cross f s) in

    M4.v
      V3.(x s)               V3.(y s)               V3.(z s)                0.0
      V3.(x u)               V3.(y u)               V3.(z u)                0.0
      V3.(x f)               V3.(y f)               V3.(z f)                0.0
      (-. V3.(dot s eye))     (-. V3.(dot u eye))     (-. V3.(dot f eye))      1.0

  let scale mat vec =
    M4.of_cols
        V4.(V3.(x vec)  * M4.(col 0 mat))
        V4.(V3.(y vec)  * M4.(col 1 mat))
        V4.(V3.(z vec)  * M4.(col 2 mat))
        M4.(col 3 mat)

  let scale_slow mat vec =
    let res =
      M4.v
        V3.(x vec)         0.0                0.0                0.0
        0.0                V3.(y vec)         0.0                0.0
        0.0                0.0                V3.(z vec)         0.0
        0.0                0.0                0.0                1.0
    in
    M4.mul mat res



end

