{
open Lexing
open Stl_grammar

exception SyntaxError of string

let next_line lexbuf =
  let pos = lexbuf.lex_curr_p in
  lexbuf.lex_curr_p <- {
    pos with pos_bol = lexbuf.lex_curr_pos;
             pos_lnum = pos.pos_lnum + 1
  }
}

let digit = ['0' - '9']
let frac = '.' digit*
let exp = ['e' 'E'] ['-' '+']? digit+
let float = '-'? digit+ frac? exp?

let white = [' ' '\t']+
let newline = '\r' | '\n' | "\r\n"
let id = ['a'-'z' 'A'-'Z' '_'] ['a'-'z' 'A'-'Z' '0'-'9' '_']*

         rule read =
           parse
           | white                 { read lexbuf }
           | newline               { next_line lexbuf; read lexbuf }
           | float                 { FLOAT (float_of_string (Lexing.lexeme lexbuf))}
           | "solid"               { SOLID_START }
           | "endsolid"            { SOLID_END }
           | "facet"               { FACET_START }
           | "endfacet"            { FACET_END }
           | "outer"               { OUTER_START }
           | "loop"                { LOOP }
           | "endloop"             { OUTER_END }
           | "vertex"              { VERTEX }
           | "normal"              { NORMAL }
           | id                    { ID (Lexing.lexeme lexbuf) }
           | eof                   { EOF }


