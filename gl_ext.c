#define GLEW_STATIC
#include<stdio.h>
#include <GL/gl.h>
#include <caml/mlvalues.h>
#include <caml/bigarray.h>

CAMLprim value gl_luminance(value unit) {
  return Val_int(GL_LUMINANCE);
}

CAMLprim value gl_luminance_alpha(value unit) {
  return Val_int(GL_LUMINANCE_ALPHA);
}

CAMLprim value gl_alpha_test(value unit) {
  return Val_int(GL_ALPHA_TEST);
}

CAMLprim value gl_depth_test(value unit) {
  return Val_int(GL_DEPTH_TEST);
}

CAMLprim value gl_stencil_test(value unit) {
  return Val_int(GL_STENCIL_TEST);
}

CAMLprim value gl_alpha_func(value cf1, value rf1) {
  glAlphaFunc(Int_val(cf1), Double_val(rf1));
  return Val_unit;
}

CAMLprim value gl_read_pixels(value wi, value hi, value rgbi, value typei, value datai) {
  int x = 0;
  int y = 0;
  int w = Int_val(wi);
  int h = Int_val(hi);
  int rgb = Int_val(rgbi);
  int type = Int_val(typei);
  void *data = Caml_ba_data_val(datai);

  glReadPixels(x, y, w, h, rgb, type,data);
  
  return Val_unit;
}
