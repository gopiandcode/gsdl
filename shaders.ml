[@@@warning "-33"]
open Tgl3
open Gg
open Core
open Utils

module GenericShader = Graphics_utils.GenericShader
module Numeric_Types = GenericShader.Types.Data.Numeric
module Texture_Types = GenericShader.Types.Data.Texture

module BasicShader = struct

  open Graphics_utils

  type t = {
    (* shader program *)
    program: GenericShader.t;

    (* attributes *)
    attributes:  Numeric_Types.field StringMap.t;

    (** uniforms  *)
    uniforms:  Numeric_Types.field StringMap.t;

    textures: Texture_Types.field StringMap.t
  }

  let pp fmt ({ program={ pid; _ }; _  }: t) = let open Format in
    pp_open_hbox fmt ();
    pp_print_string fmt "shader(pid="; pp_print_int fmt pid; pp_print_string fmt ");";
    pp_close_box fmt ()


  let texture_set t = StringMap.key_set t.textures

  let texture_find_binding t name = StringMap.find t.textures name
  let uniform_find_binding t name = StringMap.find t.uniforms name
  let attribute_find_binding t name = StringMap.find t.attributes name

  type texture_data = {
    (* textures *)
    values: (Graphics_utils.Texture.texture_unit * Graphics_utils.Texture.t) list;
    (* raw textures that should be managed externally *)
    raw_values: (Graphics_utils.Texture.texture_unit * Graphics_utils.Texture.t) list;
    (* binding of textures to texture units *)
    bound_textures: Graphics_utils.Texture.texture_unit StringMap.t;
    unbound_textures: string list;
  }

  type data = {
    id: VAO.t;
    buffers: VBO.t list;
    (** number of components encoded in data *)
    length: int;
    (* (\* binding of attributes by data  *\)
     * bound_attributes: int StringMap.t;
     * (\* attributes present in data but not in shader  *\)
     * unbound_attributes: string list; *)
  }

  let delete_data ({ id; buffers; _ }: data) =
    VAO.delete id;
    List.iter ~f:VBO.delete buffers

  let delete_texture_data ({ values; _ }: texture_data) =
    List.iter values ~f:(fun (_, value) -> Texture.delete value)

  let clone_data ({ id; buffers; length }: data) = {id; length; buffers=[]}

  let load_data_raw ?raw_data ?(normalize=false) (shader: t) (attributes: (string * Numeric_Types.data) list) =
    let open Comp.Syntax in
    let+ ((bound_attributes, unbound_attributes), named_payloads_sizes) =
      Comp.fold_map attributes
        ~init:((StringMap.empty, []))
        ~f:(fun (bound_attributes, unbound_attributes) (name, data) ->
            let field = StringMap.find shader.attributes name in
                match field with
              | None ->
                  (* let+ () = Comp.warn (Printf.sprintf "unknown attribute %s supplied to basic shader" name) in *)
                ret @@ ((bound_attributes, name :: unbound_attributes), None)
              | Some field ->
                begin
                  match Numeric_Types.make_payload data field with
                  | None ->
                      let+ () = Comp.warn (Printf.sprintf "could not construct payload for %s" name) in
                    ret @@ ((bound_attributes, name :: unbound_attributes), None)
                  | Some v ->
                      let bound_attributes =
                      StringMap.set
                        bound_attributes
                        ~key:name
                        ~data:(let Numeric_Types.(MkField (loc,_)) = field in loc)
                    in
                    let count = Numeric_Types.payload_count v in
                    ret @@ ((bound_attributes, unbound_attributes), (Some ((name,v),count)))
                end
          ) in
    let named_payloads_and_sizes = List.filter_opt named_payloads_sizes in
    (* let max_size = List.max_elt ~compare:Int.compare sizes in *)
    (* raw_data is present     && max_size is present ->
           if that both are same
                then return (extend payload to max_size, raw_data)
                else return (extend payload_to_max_size, None)
       raw_data is present     && max_size is not present ->
           return (extend_payload to max_size, raw_data)
       raw_data is not present && max_size is present ->
           return (extend_payload to max_size, None)
    *)

    let+ (payloads, max_size) = match raw_data, named_payloads_and_sizes with
      | Some (raw_data, raw_max_size), (((_, max_size_deflt) :: _) as named_payloads_and_sizes) ->
        let named_payloads, sizes = List.unzip named_payloads_and_sizes in
        let max_size = List.max_elt ~compare:Int.compare  sizes |> Option.value ~default:max_size_deflt in
        let+ named_payloads =
          List.map named_payloads ~f:(fun (name, data) ->
            let+ data = Numeric_Types.extend_to_count max_size data in ret (name, data) ) |> Comp.all in
        (* data on GPU can not be extended, however, we may be able to extend CPU data *)
        if Int.(max_size < raw_max_size) then begin
          let max_size = raw_max_size in
          let data_mapping =
            List.fold ~init:StringMap.empty
              ~f:(fun map (name,data) -> StringMap.set map ~key:name ~data:(`Snd data)) named_payloads
            |> (fun map -> List.fold ~init:map ~f:(fun map (name,data)  ->
                let field = StringMap.find shader.attributes name in
                match field with | None -> map
                | Some field -> StringMap.set map ~key:name ~data:(`Fst ((raw_max_size, data), field))) raw_data
              )  in
          ret (data_mapping, max_size)
        end else begin
          let data_mapping = List.fold ~init:StringMap.empty ~f:(fun map (name,data) -> StringMap.set map ~key:name ~data:(`Snd data)) named_payloads in
          ret (data_mapping, max_size)
        end
      | Some (raw_data, raw_max_size), [] ->
        let data_mapping = List.fold ~init:StringMap.empty ~f:(fun map (name,data) ->
            let field = StringMap.find shader.attributes name in
            match field with | None -> map | Some field ->
              StringMap.set map ~key:name ~data:(`Fst ((raw_max_size,data), field))) raw_data
        in
        ret (data_mapping, raw_max_size)
      | None, (((_, max_size_deflt) :: _) as named_payloads_and_sizes) ->
        let named_payloads, sizes = List.unzip named_payloads_and_sizes in
        let max_size = List.max_elt ~compare:Int.compare  sizes |> Option.value ~default:max_size_deflt in
        let+ named_payloads =
          List.map named_payloads ~f:(fun (name, data) ->
            let+ data = Numeric_Types.extend_to_count max_size data in ret (name, data) ) |> Comp.all in
        let data_mapping = List.fold ~init:StringMap.empty ~f:(fun map (name,data)  -> StringMap.set map ~key:name ~data:(`Snd data)) named_payloads in
        ret (data_mapping, max_size)
      | None, [] -> ret (StringMap.empty, 0) in
    let vao = VAO.create () in
    let preloaded_data, loaded_data =
      List.partition_map (payloads |> StringMap.to_alist) ~f:(fun (name,v) ->
          match v with | `Snd data -> `Snd (name, data) | `Fst data -> `Fst (name, data)) in
    let preloaded_data, loaded_data =
      Numeric_Types.load_data_list ~raw_data:preloaded_data ~normalize vao loaded_data in
    let clear () =
      let clear_data f vao vbos =
        VAO.delete vao;
        List.iter ~f:(fun value -> VBO.delete (f value)) vbos in
      clear_data (fun (_, (_, vbo)) -> vbo) vao loaded_data in

    let result =
      List.fold preloaded_data
        ~init:(Ok None)
        ~f:(fun acc (name, (size, vbo)) ->
            match acc with
            | Error e -> Error e
            | Ok length ->
              let length = match length with
                | Some value when not Int.(value = size) ->
                  Error (Error.of_string
                           (Printf.sprintf "sizes of inputs are not consistent %d != %d"
                              value size))
                | _ -> Ok (Some size) in
              match length with
              | Error e -> Error e
              | Ok length -> Ok length
          ) in
    let result, vbo_ids =
      List.fold_map
        (loaded_data)
        ~init:result
        ~f:(fun acc (name, (size, vbo)) ->
            match acc with
            | Error e -> Error e, None
            | Ok length ->
              let length = match length with
                | Some value when not Int.(value = size) ->
                  Error (Error.of_string
                           (Printf.sprintf "sizes of inputs are not consistent %d != %d"
                              value size))
                | _ -> Ok (Some size) in
              match length with
              | Error e -> Error e, None
              | Ok length -> Ok length, Some vbo
          ) in
    let vbo_ids = List.filter_opt vbo_ids in
    match result with    
    | Ok (Some length) ->
      let unwrapped = preloaded_data @ loaded_data |> List.map ~f:(fun (name, (size, vbo)) -> (name, vbo)) |> fun ls -> (ls, max_size) in
      ret @@ ({ id= vao; buffers = vbo_ids; length; (* bound_attributes; unbound_attributes *) }, unwrapped)
    | Ok (None) ->
      clear ();
      Comp.fail (Error.of_string (Printf.sprintf "could not determine number of elements"))
    | Error e ->
      clear ();
      Comp.fail e

  let load_textures (shader:t)
      ~(raw_textures: (string * Texture.t) list)
      ~(textures: (string * Texture_Types.data * (Graphics_utils.Texture.Param.t list) * bool * 'a) list) =
    let open Comp.Syntax in
    let+ (texture_payloads, bound_textures, unbound_textures) =
    Comp.fold (List.rev textures)
      ~init:([], StringMap.empty, [])
      ~f:(fun (ls, bound_textures, unbound_textures) (name, data, params, mipmaps_needed, state) ->
          let o_field = StringMap.find shader.textures name in
          match o_field with
          | None ->
            Comp.return (ls, bound_textures, name :: unbound_textures)
          | Some field ->
            let value = Texture_Types.make_payload data params field in
            match value with
            | None ->
              let+ () =
                Comp.warn (Printf.sprintf "texture %s has incorrect type for sampler" name) in
              Comp.return (ls, bound_textures, name :: unbound_textures)
            | Some value ->
              let bound_textures =
                StringMap.set bound_textures
                  ~key:name
                  ~data:(let Texture_Types.(MkField {  texture_unit; _ }) = field in texture_unit)
              in
              Comp.return ((name, value, params, mipmaps_needed, state) :: ls, bound_textures, unbound_textures)
        ) in
    let+ (raw_values, bound_textures, unbound_textures) =
      Comp.fold raw_textures ~init:([], bound_textures, unbound_textures)
        ~f:(fun (ls, bound_textures, unbound_textures) (name, texture) ->
            let o_field = StringMap.find shader.textures name in
            match o_field with
            | None -> ret (ls, bound_textures, name :: unbound_textures)
            | Some Texture_Types.MkField { texture_unit; texture_type; _ } ->
              match texture_type, texture with
              | (Texture_Types.SamplerCube, (Texture.Texture_Cube _ as texture))
              | (Texture_Types.Sampler3D, (Texture.Texture_3D _ as texture))
              | (Texture_Types.Sampler2D, (Texture.Texture_2D _ as texture)) 
              | (Texture_Types.Sampler1D, (Texture.Texture_1D _ as texture)) ->
                ret ((texture_unit, texture) :: ls,
                     StringMap.set bound_textures ~key:name ~data:texture_unit, unbound_textures)
              | _ ->
                let+ () = Comp.warn (Printf.sprintf "load_textures: mismatched texture for binding %s" name) in
                ret (ls, bound_textures, name :: unbound_textures)
          ) in
    let values, loaded_data =
      let result = List.map
          ~f:(fun (name,image,params, mipmaps_needed, state) ->
              name, Texture_Types.load_data ~mipmaps_needed image,params, state)
          texture_payloads in
      List.map ~f:(fun (_, v, _, _) -> v) result, result in
    Comp.return ({values; raw_values; bound_textures; unbound_textures}, loaded_data)

    
  let load_data ?raw_data
      ?(normalize=false)
      ?(vertex_colour: V3.t list option) ?(vertex_normal: V3.t list option)
      ?(tex_coords: V2.t list option) ?(other_attributes = [])
      ?(vertex_position: V3.t list option) (shader: t) =
    let values =
      [
        Option.map vertex_position ~f:(fun vlist ->
            "vertex_position", Numeric_Types.(MkBulkData (vlist, Vec3_Float)));
        Option.map vertex_colour ~f:(fun vlist ->
            "vertex_colour", Numeric_Types.(MkBulkData (vlist, Vec3_Float)));
        Option.map vertex_normal ~f:(fun vlist ->
            "vertex_normal", Numeric_Types.(MkBulkData (vlist, Vec3_Float)));
        Option.map tex_coords ~f:(fun vlist ->
            "tex_coords", Numeric_Types.(MkBulkData (vlist, Vec2_Float)));
      ]
      |> List.filter_opt
      |> fun ls -> ls @ other_attributes
    in
    load_data_raw ?raw_data ~normalize shader values

  let load_uniforms_raw (shader: t) (uniforms: (string * Numeric_Types.data) list) =
    let open Comp.Syntax in
    let+ named_payloads =
      Comp.fold uniforms
        ~init:[]
        ~f:(fun acc (name, data) ->
              let field = StringMap.find shader.uniforms name in
              match field with
              | None ->
                (* let+ () = Comp.warn (Printf.sprintf "unknown uniform %s supplied to basic shader" name) in *)
                ret @@ (None :: acc)
              | Some field ->
                begin
                  match Numeric_Types.make_payload data field with
                  | None ->
                    let+ () = Comp.warn (Printf.sprintf "could not construct payload for uniform %s" name) in
                    ret @@ (None :: acc)
                  | Some v ->
                    ret @@ (Some v :: acc)
                end
          ) in
    let payloads = List.filter_opt named_payloads in
    ret @@  GenericShader.load_uniforms shader.program payloads

  let load_uniforms
      ?(model: M4.t option) ?(view: M4.t option) ?(projection: M4.t option)
      ?(other_uniforms = [])
      (shader: t) =
    let values =
      [
        Option.map model ~f:(fun vlist ->
            "model", Numeric_Types.(MkData (vlist, Mat4x4_Float)));
        Option.map view ~f:(fun vlist ->
            "view", Numeric_Types.(MkData (vlist, Mat4x4_Float)));
        Option.map projection ~f:(fun vlist ->
            "projection", Numeric_Types.(MkData (vlist, Mat4x4_Float)));
      ]
      |> List.filter_opt
      |> fun ls -> ls @ other_uniforms
    in
    load_uniforms_raw shader values

  (* let data_supported (shader:t) (data:data) =
   *   let other_attributes_match () =
   *     let other_attributes = StringMap.keys shader.attributes in
   *     let rec loop elems = match elems with
   *       | [] -> true
   *       | name :: t ->
   *         let open GenericShader.Types in
   *         match StringMap.find shader.attributes name, StringMap.find data.bound_attributes name  with
   *         | Some MkField (ind, _), Some ind' when not Int.(ind = ind') -> false
   *         | _ -> loop t
   *     in
   *     loop other_attributes in
   *   let unbound_attributes_present () =
   *     let rec loop elems = match elems with
   *       | [] -> false
   *       | name :: t ->
   *         match StringMap.find shader.attributes name  with
   *         | Some _ -> true
   *         | _ -> loop t
   *     in
   *     loop data.unbound_attributes in
   *   if other_attributes_match () then
   *     if not (unbound_attributes_present ()) then
   *       true
   *     else false
   *   else false *)

  type draw_op =
    | Triangles
    | Lines

  type drawable = {
    model: M4.t;
    uniforms: (string * Numeric_Types.data) list;
    textures: texture_data;
    attributes: data;
    draw_op: draw_op
  }

  let draw
      ?(model: M4.t option) ?(view: M4.t option) ?(projection: M4.t option)
      ?(other_uniforms=[])
      (shader:t)  (data: data) (textures: texture_data) (draw: draw_op) =
    let open Comp.Syntax in
    GenericShader.use_program shader.program;
    let () =
      List.iteri textures.values
        ~f:(fun _ind (texture_unit, texture) ->
            Graphics_utils.Texture.use_texture texture texture_unit) in
    let () =
      List.iteri textures.raw_values
        ~f:(fun _ind (texture_unit, texture) ->
            Graphics_utils.Texture.use_texture texture texture_unit) in
    VAO.bind data.id;
    let+ () = load_uniforms ?model ?view ?projection ~other_uniforms shader in
    let gl_command =
      match draw with
      | Triangles -> Gl.triangles
      | Lines -> Gl.lines 
    in
    Gl.draw_arrays gl_command 0 data.length;
    ret @@ ()


  let build_raw shaders =
    let open Comp.Syntax in
    let+ shader = Graphics_utils.GenericShader.build_raw ~shaders ~attribs:[] in
    let+ attributes = Graphics_utils.GenericShader.attributes shader in
    let+ attributes =
      Comp.fold
        attributes
        ~init:StringMap.empty
        ~f:(fun attributes (ind, name, _size, typ) ->
              let attributes : Numeric_Types.field StringMap.t =
                let (MkFieldType typ) = typ in
                StringMap.set attributes
                  ~key:name ~data:Numeric_Types.(MkField (ind, typ))
              in
              ret attributes
          )
    in
    let+ textures = GenericShader.textures shader in
    let textures =
      List.fold
        textures
        ~init:StringMap.empty
        ~f:(fun smap (name, field) ->  StringMap.set smap ~key:name ~data:field )
    in

    let+ uniforms = GenericShader.uniforms shader in
    let+ uniforms =
      let check_value name value expected eq to_str  =
        if eq value expected
        then ret ()
        else Comp.fail (Error.of_string
                          (Printf.sprintf "%s is not the expected value (%s not equal to %s)"
                             name (to_str value) (to_str expected))) in
      Comp.fold
        uniforms
        ~init:StringMap.empty
        ~f:(fun uniforms (ind, name, size, typ) ->
              let+ () = check_value (Printf.sprintf "%s size (arrays not supported)" name)
                  (Int.of_int32_exn size) 1 Int.(=) Int.to_string in
              let uniforms : Numeric_Types.field StringMap.t =
                let (MkFieldType typ) = typ in
                StringMap.set uniforms
                  ~key:name ~data:Numeric_Types.(MkField (ind, typ))
              in
              ret uniforms
          )
    in
    ret @@ {
      program=shader;  attributes; uniforms; textures;
    }  

  let build ~vert_src ~frag_src =
    let open Comp.Syntax in
    let safe_open file =
      try ret (In_channel.read_all file) with
        Sys_error e -> Comp.fail (Error.of_string  e)      
    in
    let+ frag_src = safe_open frag_src in
    let+ vert_src = safe_open vert_src in

    let+ vert_shader = GenericShader.compile_shader vert_src Gl.vertex_shader in
    let+ frag_shader = GenericShader.compile_shader frag_src Gl.fragment_shader in
    build_raw [vert_shader; frag_shader]

  let delete (shader:t) =
    Comp.return @@
    GenericShader.delete_program  shader.program

  let draw_drawable ?uniforms:(additional_uniforms=[])
      shader ({ model; uniforms; textures; attributes; draw_op }: drawable) =
    let open Comp.Syntax in
    let+ () = load_uniforms_raw shader (uniforms @  additional_uniforms) in
    draw ~model ~other_uniforms:uniforms shader attributes textures draw_op 

  let delete_drawable ({ attributes; _ }: drawable) =
    delete_data attributes

end

module BasicShaderSynchronizer = struct

  open Sync

  module Tag = struct
      type t = VertexShader | FragmentShader | GeometryShader [@@deriving sexp, ord, hash]
    end


  module BasicShaderSyncable = struct
    type t = BasicShader.t
    type state = unit
    type data = GenericShader.shader

    module Tag = Tag

    let load_file () (MkDependency (tag, data) : (Tag.t, string) dependency) : (state * data, Error.t) Comp.t =
      let typ = match tag with
          VertexShader -> Gl.vertex_shader | FragmentShader -> Gl.fragment_shader
        | GeometryShader -> Gl.geometry_shader in
      let open Comp.Syntax in
      let+ data = GenericShader.compile_shader data typ in
      ret @@ ((), data)

    let of_dependencies () (shaders: (Tag.t, data) dependency list) : (state * t option, Error.t) Comp.t =
      let open Comp.Syntax in
      let+ result =
        Comp.suppress_error @@
        BasicShader.build_raw (List.map ~f:(fun (MkDependency (_,shader)) -> shader) shaders) in
      ret @@ ((), result)

    let clean_partial_results () (partial_results: (Tag.t, data) dependency list) : (state, Error.t) Comp.t =
      Comp.return @@
      List.iter partial_results
        ~f:(fun (MkDependency (_, shader)) -> GenericShader.delete_shader shader)

    let delete_result () : t -> (state, Error.t) Comp.t =
      BasicShader.delete 
  end

  include (File.Make (BasicShaderSyncable))

end

