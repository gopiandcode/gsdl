open Gg
open Core

module Float = struct
  include Float

  let safe_acos frac =
    if frac <= -1.0
    then pi
    else if frac >= 1.0
    then 0.0
    else acos frac

  let safe_sqrt frac =
    if frac <= 0.0
    then 0.0
    else sqrt frac

end

module V2 = struct
  include V2

  (** project v1 in the direction of v2  *)
  let project v1 v2 =
    let open V2 in
    let mul = (dot v1 v2)/.(dot v2 v2) in
    mul * v2

  type tuple = (float * float) [@@deriving show, ord,sexp, hash]

  let sexp_of_t (v: t) : Ppx_sexp_conv_lib.Sexp.t =
    sexp_of_tuple (to_tuple v)

  let t_of_sexp sexp =
    of_tuple (tuple_of_sexp sexp)

  let compare v1 v2 =
    compare_tuple (to_tuple v1) (to_tuple v2)

  let hash_fold_t hs (v: t) =
    hash_fold_tuple hs (to_tuple v)

  let pp fmt v =
    pp_tuple fmt (to_tuple v)
end

module V3 = struct
  include V3

  (** project v1 in the direction of v2  *)
  let project v1 v2 =
    let open V3 in
    let mul = (dot v1 v2)/.(dot v2 v2) in
    mul * v2

  let hash (v: t) : int =
    Float.hash (x v) lxor Float.hash (y v) lxor Float.hash (z v)

  type tuple = (float * float * float) [@@deriving show, ord,sexp, hash]

  let sexp_of_t (v: t) : Ppx_sexp_conv_lib.Sexp.t =
    sexp_of_tuple (to_tuple v)

  let t_of_sexp sexp =
    of_tuple (tuple_of_sexp sexp)

  let compare v1 v2 =
    compare_tuple (to_tuple v1) (to_tuple v2)

  let hash_fold_t hs (v: t) =
    hash_fold_tuple hs (to_tuple v)

  let pp fmt v =
    pp_tuple fmt (to_tuple v)

end

module V4 = struct
  include V4

  (** project v1 in the direction of v2  *)
  let project v1 v2 =
    let mul = (dot v1 v2)/.(dot v2 v2) in
    mul * v2

  type tuple = (float * float * float * float) [@@deriving show, ord,sexp, hash]

  let sexp_of_t (v: t) : Ppx_sexp_conv_lib.Sexp.t =
    sexp_of_tuple (to_tuple v)

  let t_of_sexp sexp =
    of_tuple (tuple_of_sexp sexp)

  let compare v1 v2 =
    compare_tuple (to_tuple v1) (to_tuple v2)

  let hash_fold_t hs (v: t) =
    hash_fold_tuple hs (to_tuple v)

  let pp fmt v =
    pp_tuple fmt (to_tuple v)

end

module M2 = struct
  include M2


  type tuple = (float * float * float * float) [@@deriving show, ord,sexp, hash]

  let to_tuple v = (M2.e00 v, M2.e01 v, M2.e10 v, M2.e11 v)
  let of_tuple (e00, e01, e10, e11) = M2.v e00 e01 e10 e11

  let sexp_of_t (v: t) : Ppx_sexp_conv_lib.Sexp.t =
    sexp_of_tuple (to_tuple v)

  let t_of_sexp sexp =
    of_tuple (tuple_of_sexp sexp)

  let compare v1 v2 =
    compare_tuple (to_tuple v1) (to_tuple v2)

  let hash_fold_t hs (v: t) =
    hash_fold_tuple hs (to_tuple v)

  let pp fmt v =
    pp_tuple fmt (to_tuple v)

end

module M3 = struct
  include M3

  type tuple = (float * float * float *
                float * float * float *
                float * float * float) [@@deriving show, ord,sexp, hash]

  let to_tuple v = e00 v, e01 v, e02 v, e10 v, e11 v, e12 v, e20 v, e21 v, e22 v
    
  let of_tuple (e00, e01, e02, e10, e11, e12, e20, e21, e22) =
    M3.v     e00 e01 e02 e10 e11 e12 e20 e21 e22

  let sexp_of_t (v: t) : Ppx_sexp_conv_lib.Sexp.t =
    sexp_of_tuple (to_tuple v)

  let t_of_sexp sexp =
    of_tuple (tuple_of_sexp sexp)

  let compare v1 v2 =
    compare_tuple (to_tuple v1) (to_tuple v2)

  let hash_fold_t hs (v: t) =
    hash_fold_tuple hs (to_tuple v)

  let pp fmt v =
    pp_tuple fmt (to_tuple v)

end

module M4 = struct
  include M4

  type tuple = (float * float * float * float *
                float * float * float * float *
                float * float * float * float *
                float * float * float * float) [@@deriving show, ord,sexp, hash]

  let to_tuple v = 
    e00 v, e01 v, e02 v, e03 v, e10 v, e11 v, e12 v, e13 v, e20 v, e21 v, e22 v, e23 v, e30 v, e31 v, e32 v, e33 v  
  let of_tuple (e00, e01, e02, e03, e10, e11, e12, e13, e20, e21, e22, e23, e30, e31, e32, e33) =
    M4.v e00 e01 e02 e03 e10 e11 e12 e13 e20 e21 e22 e23 e30 e31 e32 e33

  let sexp_of_t (v: t) : Ppx_sexp_conv_lib.Sexp.t =
    sexp_of_tuple (to_tuple v)

  let t_of_sexp sexp =
    of_tuple (tuple_of_sexp sexp)

  let compare v1 v2 =
    compare_tuple (to_tuple v1) (to_tuple v2)

  let hash_fold_t hs (v: t) =
    hash_fold_tuple hs (to_tuple v)

  let pp fmt v =
    pp_tuple fmt (to_tuple v)

end


module Comp : sig
  type ('a, 'b) t
  val ( >>= ) : ('a, 'e) t -> ('a -> ('b, 'e) t) -> ('b, 'e) t
  val ( >>| ) : ('a, 'e) t -> ('a -> 'b) -> ('b, 'e) t
  val run : ('a, 'c) t -> ('a * string list, 'c) result
  val unwrap: ('a, 'c) t -> string list -> ('a * string list, 'c) result
  val bind : ('a, 'e) t -> f:('a -> ('b, 'e) t) -> ('b, 'e) t
  val of_result: ('a, 'b) result -> ('a, 'b) t
  val return : 'a -> ('a, 'b) t
  val fold: 'a list -> init:'acc -> f:('acc -> 'a -> ('acc, 'y) t) -> ('acc, 'y) t
  val fold_map: 'a list -> init:'acc -> f:('acc -> 'a -> ('acc * 'b, 'y) t) -> ('acc * 'b list, 'y) t
  val join : (('a, 'e) t, 'e) t -> ('a, 'e) t
  val ignore_m : ('a, 'e) t -> (unit, 'e) t
  val all : ('a, 'e) t list -> ('a list, 'e) t
  val all_unit : (unit, 'e) t list -> (unit, 'e) t
  val ignore : ('a, 'err) t -> (unit, 'err) t
  val fail : 'err -> ('a, 'err) t
  val failf :
    (('a, unit, string, ('c, string) result) format4 -> 'a, unit, string,
     ('b, string) t)
    Core.format4 -> ('a, unit, string, ('c, string) result) format4 -> 'a
  val warnings : (string list, 'b) t
  val peek_warnings : (string list, 'b) t
  val lift: (string list -> ('a * string list, 'b) result) -> ('a,'b) t
  val loop: init:'a -> f:('a -> ('a, 'b) t) -> cont:('a -> bool) -> fin:('a -> 'c) -> ('c, 'b) t
  val warn : string -> (unit, 'b) t
  val suppress_error: ('a, Error.t) t -> ('a option, Error.t) t
  val of_option : 'ok option -> error:'err -> ('ok, 'err) t
  val iter : ('ok, 'a) t -> f:('ok -> unit) -> unit
  val iter_error : ('a, 'err) t -> f:('err -> unit) -> unit
  val map : ('ok, 'err) t -> f:('ok -> 'c) -> ('c, 'err) t
  val map_error : ('ok, 'err) t -> f:('err -> 'c) -> ('ok, 'c) t
  val ok_fst : ('ok, 'err) t -> [ `Fst of 'ok | `Snd of 'err ]
  val ok_if_true : bool -> error:'err -> (unit, 'err) t
  val try_with : (unit -> 'a) -> ('a, exn) t
  val try_with_cleanup: on_error:('b -> unit) -> ('a, 'b) t -> ('a, 'b) t
  val try_finally: finally:(unit -> 'c) -> ('a, 'b) t -> ('a, 'b) t 
  val ok_unit : (unit, 'a) t
  module Syntax :
    sig
      val ( let+ ) : ('a, 'b) t -> ('a -> ('c, 'b) t) -> ('c, 'b) t
      val ret : 'a -> ('a, 'b) t
    end
end = struct

  type ('a, 'b) t = string list -> ('a * string list, 'b) Result.t

  let ( >>= ) (comp: ('a, 'e) t) (f:('a -> ('b, 'e) t)) : ('b, 'e) t =
    fun s -> 
    Result.(>>=) (comp s) (fun (value,warnings) ->
        Result.map ((f value) warnings) ~f:(fun (result, warnings) ->
            (result, warnings)))

  let ( >>| ) (comp: ('a, 'e) t) (f:('a -> 'b)) : ('b, 'e) t =
    fun warnings -> 
    Result.(>>|) (comp warnings)
      (fun (value,warnings) -> (f value, warnings))

  let run (comp: ('a, 'c) t) = comp []

  let unwrap (comp: ('a, 'c) t) : string list -> ('a * string list, 'b) Result.t = comp

  let bind (comp: ('a, 'e) t) ~f : ('b, 'e) t =
    (>>=) comp f

  let of_result (res: ('a,'b) Result.t) : ('a,'b) t =
    fun warnings -> Result.map res ~f:(fun value -> value, warnings)

  let return (value: 'a) : ('a, 'b) t =
    fun s ->  Result.return (value,s)

  let try_with_cleanup ~on_error (comp: ('a,'b) t) =
    fun warnings ->
    let res = comp warnings in
    begin match res with Core.Result.Ok _->()|Core.Result.Error error -> on_error error end;
    res

  let try_finally ~finally (comp: ('a,'b) t) =
    fun warnings ->
    let res = comp warnings in
    finally ();
    res


  let fold (ls: 'a list) ~(init: 'acc) ~(f: 'acc -> 'a -> ('acc,'c) t) : ('acc,'y) t =
    fun warnings ->
    let rec loop acc ls warnings = match ls with
      | [] -> Result.Ok ((acc, warnings))
      | h :: t ->
        let value = (f acc h) warnings in
        match value with
        | Ok (acc, warnings) -> loop acc t warnings
        | Error e -> Error e
    in
    loop init ls warnings

  let fold_map (ls: 'a list) ~(init: 'acc) ~(f: 'acc -> 'a -> ('acc * 'b,'c) t) : ('acc * 'b list,'y) t =
    fun warnings ->
    let rec loop acc rev ls warnings = match ls with
      | [] -> Result.Ok (((acc, List.rev rev), warnings))
      | h :: t ->
        let value = (f acc h) warnings in
        match value with
        | Ok ((acc, h'), warnings) -> loop acc (h' :: rev) t warnings
        | Error e -> Error e
    in
    loop init [] ls warnings  
  
  let join (comp : (('a, 'e) t, 'e) t) : ('a, 'e) t =
  (fun warnings -> 
      Result.bind (comp warnings)
        ~f:(fun (inner_comp, warnings) ->
            Result.map (inner_comp warnings)
              ~f:(fun (value, warnings) ->
                  (value, warnings))
          )
    )

  let ignore_m (comp: ('a, 'e) t) : (unit, 'e) t  =
    fun warnings -> Result.map (comp warnings) ~f:(fun (_,warnings) -> ((), warnings))

  let all (comps: ('a, 'e) t list) : ('a list, 'e) t =
    fun warnings -> 
    List.fold ~init:(Result.Ok ([],warnings)) ~f:(fun acc comp ->
        Result.bind acc ~f:(fun (values,warnings) -> comp warnings
                                                     |> Result.map ~f:(fun (value,warnings) ->
                                                         (value :: values, warnings)
                                                       ))
      ) comps
    |> Result.map ~f:(fun (ls, warnings) -> (List.rev ls, warnings))

  let all_unit (comps: (unit, 'e) t list) : (unit, 'e) t =
    fun warnings -> 
    List.fold ~init:(Result.Ok ([],warnings)) ~f:(fun acc comp ->
        Result.bind acc ~f:(fun (values,warnings) -> comp warnings
                                                     |> Result.map ~f:(fun (value,warnings) ->
                                                         (value :: values, warnings)
                                                       ))
      ) comps
    |> Result.map ~f:(fun (_, warnings) -> ((), warnings))

  let ignore (comp: ('a, 'err) t) : (unit, 'err) t =
    fun warnings -> Result.map (comp warnings) ~f:(fun (_,warnings) -> ((), warnings))

  let fail (err: 'err) : ('a, 'err) t =
    fun _ -> Result.fail err

  let failf : ('a, unit, string, ('b, string) t) format4 -> 'a =
    fun _ -> Result.failf

  let warnings : (string list, 'b) t =
    fun warnings -> Result.Ok (List.rev warnings, [])

  let peek_warnings : (string list, 'b) t =
    fun warnings -> Result.Ok (List.rev warnings, warnings)

  let lift f : ('a,'b) t=
    fun warnings -> f warnings

  let loop ~(init:'a) ~(f: 'a -> ('a,'b) t) ~(cont: 'a -> bool) ~(fin:'a -> 'c) : ('c,'b) t =
    fun warnings ->
    let rec iter state warnings =
      let state = f state warnings in
      match state with
      | Error e -> Error e
      | Ok (state, warnings) ->
        if cont state then
          iter state warnings
        else Ok (fin state, warnings)
    in
    iter init warnings


  let warn str : (unit, 'b) t  =
    fun warnings ->  Result.return ((), str::warnings)

  let suppress_error (comp: ('a, Error.t) t) : ('a option, Error.t) t =
    fun warnings ->
    match comp warnings with
    | Error e ->    Ok (None, (Error.to_string_hum e)::warnings)
    | Ok (value, warnings) -> Ok (Some value, warnings)

  let of_option (opt: 'ok option) ~error:(error:'err) : ('ok, 'err) t =
    fun warnings -> 
    Result.of_option opt ~error
    |> Result.map ~f:(fun value -> (value,warnings))

  let iter (comp: ('ok, 'a) t) ~(f:('ok -> unit)) : unit =
    Result.iter (comp []) ~f:(fun (value, _) -> f value)

  let iter_error (comp: ('a, 'err) t) : f:('err -> unit) -> unit =
    Result.iter_error (comp [])

  let map (comp: ('ok, 'err) t) ~(f:('ok -> 'c)) : ('c, 'err) t =
    fun warnings -> Result.map (comp warnings) ~f:(fun (value, warnings) -> (f value, warnings))

  let map_error (comp: ('ok, 'err) t) ~(f:('err -> 'c)) : ('ok, 'c) t =
    fun warnings -> 
    Result.map_error (comp warnings) ~f

  let ok_fst (comp: ('ok, 'err) t)
    : [ `Fst of 'ok | `Snd of 'err ] =
    Result.ok_fst (comp [])
    |> fun value -> match value with
    | `Fst (value, _) -> `Fst value
    | `Snd err -> `Snd err

  let ok_if_true (opt: bool) ~(error:'err) : (unit, 'err) t =
    fun warnings -> 
    Result.ok_if_true opt ~error
    |> Result.map ~f:(fun () -> (),warnings)

  let try_with (comp: (unit -> 'a)) : ('a, exn) t =
    fun warnings -> Result.try_with comp |> Result.map ~f:(fun value -> value,warnings)

  let ok_unit : (unit, 'a) t =
    fun warnings -> 
    Result.Ok ((),warnings)

  module Syntax = struct 
    let (let+) = (>>=)
    let ret v = return v
  end

end

module Option = struct

  include Option

  module Syntax = struct 
    let (let+) = Option.(>>=)
    let ret v = Option.return v
  end
end

module Result = struct

  include Result

  module Syntax = struct 
    let (let+) = Result.(>>=)
    let ret v = Result.return v
  end
end


(* generates bit-masks *)
module GenericBitMask () : sig
  type t

  val fresh : unit -> t

  val (+) : t -> t -> t

  val mem : t -> t -> bool

  val subset : t -> t -> bool

  val empty: t

end = struct

  type t = int

  let n = ref 1

  let empty = 0

  let fresh () =
    let result = !n in
    n := !n lsl 1;
    result

  let (+) a b = a lor b

  let mem a b = (a land b) > 0

  let subset a b = (a land b) = a

end

(** data structure for O(1) access vectors *)
module Vector : sig

  (** data structure for O(1) access vectors *)
  type 'a t

  (** constructs a new vector  *)
  val empty: 'a t

  (** calculates the length of the vector *)
  val length: 'a t -> int

  (** compare two vectors using a comparator *)
  val compare : cmp:('a -> 'a -> int) -> 'a t -> 'a t -> int

  (** convert the vector to a sexp *)
  val t_of_sexp :
    (Ppx_sexp_conv_lib.Sexp.t -> 'a) ->
    Ppx_sexp_conv_lib.Sexp.t -> 'a t

  (** convert a sexp to a vector  *)
  val sexp_of_t :
    ('a -> Ppx_sexp_conv_lib.Sexp.t) ->
    'a t -> Ppx_sexp_conv_lib.Sexp.t

  (** searches for an element in the vector  *)
  val binary_search :
    ('a t, 'a, 'key) Base__Binary_searchable_intf.binary_search

  val binary_search_segmented :
    ('a t, 'a) Base__Binary_searchable_intf.binary_search_segmented

  (** tests for membership of a vector *)
  val mem : 'a t -> 'a -> equal:('a -> 'a -> bool) -> bool

  (** converts a vector to an array  *)
  val to_array: 'a t -> 'a Array.t

  (** iterates through the elements of a vector  *)
  val iter : 'a t -> f:('a -> unit) -> unit

  (** map the elements of a vector  *)
  val map : 'a t -> f:('a -> 'b) -> 'b t

  (** index into a vector *)
  val get: 'a t -> int -> 'a

  (** set an index of a vector *)
  val set: 'a t -> int -> 'a -> 'a t

  (** concatenate two vectors  *)
  val (++): 'a t -> 'a t -> 'a t

  (** append a value onto a vector  *)
  val append: 'a t -> 'a -> 'a t

  (** update the value of a specific index of the vector  *)
  val update: 'a t -> int -> f:('a -> 'a) -> 'a t

  (** update the value of a specific index of the vector lazily *)
  val update_lazy: 'a t -> int -> f:('a -> ('a, 'b) Comp.t) -> ('a t, 'b) Comp.t

  (** test whether a vector is empty  *)
  val is_empty : 'a t -> bool

  (** fold over a vector  *)
  val fold : 'a t -> init:'accum -> f:('accum -> 'a -> 'accum) -> 'accum

  (** fold over a vector with errors  *)
  val fold_result :
    'a t ->
    init:'accum ->
    f:('accum -> 'a -> ('accum, 'e) result) -> ('accum, 'e) result

  (** fold over a vector until a condition  *)
  val fold_until :
    'a t ->
    init:'accum ->
    f:('accum -> 'a -> ('accum, 'final) Continue_or_stop.t) ->
    finish:('accum -> 'final) -> 'final

  (** check whether any element of a vector satisfies a condition  *)
  val exists : 'a t -> f:('a -> bool) -> bool

  (** check whether all elements of a vector satisfy a condition  *)
  val for_all : 'a t -> f:('a -> bool) -> bool

  (** count the number of elements of a vector satisfying a condition  *)
  val count : 'a t -> f:('a -> bool) -> int

  (** sum over a vector   *)
  val sum :
    (module Base__Container_intf.Summable with type t = 'sum) ->
    'a t -> f:('a -> 'sum) -> 'sum

  (** find a particular element in a vector *)
  val find : 'a t -> f:('a -> bool) -> 'a option

  (** find a particular element using a map *)
  val find_map : 'a t -> f:('a -> 'b option) -> 'b option

  (** convert a vector to a list *)
  val to_list : 'a t -> 'a list

  (** find the minimal element in a list with respect to some ordering *)
  val min_elt : 'a t -> compare:('a -> 'a -> int) -> 'a option

  (** find the maximal  element in a list with respect to some ordering *)
  val max_elt : 'a t -> compare:('a -> 'a -> int) -> 'a option

  (** create a vector of the specified values  *)
  val create : len:int -> 'a -> 'a t

  val init : int -> f:(int -> 'a) -> 'a t

  val concat : 'a t list -> 'a t

  val copy : 'a t -> 'a t

  val blit : ('a t, 'a t) Base__Blit_intf.blit

  val blito : ('a t, 'a t) Base__Blit_intf.blito

  val unsafe_blit : ('a t, 'a t) Base__Blit_intf.blit

  val sub : ('a t, 'a t) Base__Blit_intf.sub

  val subo : ('a t, 'a t) Base__Blit_intf.subo

  val of_list : 'a list -> 'a t

  val folding_map : 'a t -> init:'b -> f:('b -> 'a -> 'b * 'c) -> 'c t

  val folding_mapi :
    'a t -> init:'b -> f:(int -> 'b -> 'a -> 'b * 'c) -> 'c t

  val fold_map : 'a t -> init:'b -> f:('b -> 'a -> 'b * 'c) -> 'b * 'c t

  val fold_mapi :
    'a t -> init:'b -> f:(int -> 'b -> 'a -> 'b * 'c) -> 'b * 'c t

  val iteri : 'a t -> f:(int -> 'a -> unit) -> unit

  val mapi : 'a t -> f:(int -> 'a -> 'b) -> 'b t

  val foldi : 'a t -> init:'b -> f:(int -> 'b -> 'a -> 'b) -> 'b

  val fold_right : 'a t -> f:('a -> 'b -> 'b) -> init:'b -> 'b

  val sort :
    ?pos:int -> ?len:int -> 'a t -> cmp:('a -> 'a -> int) -> 'a t

  val stable_sort : 'a t -> cmp:('a -> 'a -> int) -> 'a t

  val is_sorted : 'a t -> cmp:('a -> 'a -> int) -> bool

  val is_sorted_strictly : 'a t -> cmp:('a -> 'a -> int) -> bool

  val concat_map : 'a t -> f:('a -> 'b t) -> 'b t

  val concat_mapi : 'a t -> f:(int -> 'a -> 'b t) -> 'b t

  val partition_tf : 'a t -> f:('a -> bool) -> 'a t * 'a t

  val partitioni_tf : 'a t -> f:(int -> 'a -> bool) -> 'a t * 'a t

  val cartesian_product : 'a t -> 'b t -> ('a * 'b) t

  val transpose : 'a t t -> 'a t t option

  val transpose_exn : 'a t t -> 'a t t

  val filter_opt : 'a option t -> 'a t

  val filter_map : 'a t -> f:('a -> 'b option) -> 'b t

  val filter_mapi : 'a t -> f:(int -> 'a -> 'b option) -> 'b t

  val for_alli : 'a t -> f:(int -> 'a -> bool) -> bool

  val existsi : 'a t -> f:(int -> 'a -> bool) -> bool

  val counti : 'a t -> f:(int -> 'a -> bool) -> int

  val iter2_exn : 'a t -> 'b t -> f:('a -> 'b -> unit) -> unit

  val map2_exn : 'a t -> 'b t -> f:('a -> 'b -> 'c) -> 'c t

  val fold2_exn : 'a t -> 'b t -> init:'c -> f:('c -> 'a -> 'b -> 'c) -> 'c

  val for_all2_exn : 'a t -> 'b t -> f:('a -> 'b -> bool) -> bool

  val exists2_exn : 'a t -> 'b t -> f:('a -> 'b -> bool) -> bool

  val filter : 'a t -> f:('a -> bool) -> 'a t

  val filteri : 'a t -> f:(int -> 'a -> bool) -> 'a t

  val swap : 'a t -> int -> int -> unit

  val of_list_rev : 'a list -> 'a t

  val of_list_map : 'a list -> f:('a -> 'b) -> 'b t

  val of_list_mapi : 'a list -> f:(int -> 'a -> 'b) -> 'b t

  val of_list_rev_map : 'a list -> f:('a -> 'b) -> 'b t

  val of_list_rev_mapi : 'a list -> f:(int -> 'a -> 'b) -> 'b t

  val find_exn : 'a t -> f:('a -> bool) -> 'a

  val find_map_exn : 'a t -> f:('a -> 'b option) -> 'b

  val findi : 'a t -> f:(int -> 'a -> bool) -> (int * 'a) option

  val findi_exn : 'a t -> f:(int -> 'a -> bool) -> int * 'a

  val find_mapi : 'a t -> f:(int -> 'a -> 'b option) -> 'b option

  val find_mapi_exn : 'a t -> f:(int -> 'a -> 'b option) -> 'b

  (** find any consecutive duplicates in a vector *)
  val find_consecutive_duplicate :
    'a t -> equal:('a -> 'a -> bool) -> ('a * 'a) option

  (** reduce over a vector  *)
  val reduce : 'a t -> f:('a -> 'a -> 'a) -> 'a option

  (** reduce over a vector raising an exception if empty *)
  val reduce_exn : 'a t -> f:('a -> 'a -> 'a) -> 'a

  (** construct a new vector that is a random permutation of the given vector *)
  val permute : ?random_state:Base.Random.State.t -> 'a t -> 'a t

  (** retrieve a random element from a vector *)
  val random_element : ?random_state:Base.Random.State.t -> 'a t -> 'a option

  (** retrieve a random element from a vector *)
  val random_element_exn : ?random_state:Base.Random.State.t -> 'a t -> 'a

  (** zip together two vectors *)
  val zip : 'a t -> 'b t -> ('a * 'b) t option

  (** zip together two vectors raising an exception if they are of different lengths *)
  val zip_exn : 'a t -> 'b t -> ('a * 'b) t

  val unzip : ('a * 'b) t -> 'a t * 'b t

  val sorted_copy : 'a t -> compare:('a -> 'a -> int) -> 'a t

  val last : 'a t -> 'a

  val equal : ('a -> 'a -> bool) -> 'a t -> 'a t -> bool

  val to_sequence : 'a t -> 'a Base.Sequence.t

  val to_sequence_mutable : 'a t -> 'a Base.Sequence.t

  module Private = Base.Array.Private

  module Int = Core_kernel__Array.Int

  module Float = Core_kernel__Array.Float

  val normalize : 'a t -> int -> int

  val slice : 'a t -> int -> int -> 'a t

  val pop: 'a t -> int -> 'a t

end = struct

  include Array

  type 'a t  = 'a Array.t

  let empty : 'a t = [| |]

  let length array =
    Array.length array

  let to_array array =
    array

  let iter = Array.iter

  let map = Array.map

  let get = Array.get

  let set array ind value =
    let array = Array.copy array in
    Array.set array ind value;
    array

  let (++) = Array.append

  let append array value =
    Array.init
      (Array.length array + 1)
      ~f:(fun ind ->
          if ind < Array.length array
          then Array.unsafe_get array ind
          else value
        )

  let pop array rem_ind =
    Array.init
      (Array.length array - 1)
      ~f:(fun ind ->
          match ind with
          | v when v < rem_ind ->  
            Array.unsafe_get array ind
          | _ ->
            Array.unsafe_get array (ind + 1)
        )

  let update array ind ~f =
    let array = Array.copy array in
    let value = Array.get array ind in
    let value = f value in
    Array.unsafe_set array ind value;
    array

  let update_lazy array ind ~(f: 'a -> ('a,'b) Comp.t) : ('a t, 'b) Comp.t =
    let open Comp.Syntax in
    let value = Array.get array ind in
    let+ value = f value in
    let array = Array.copy array in
    Array.unsafe_set array ind value;
    ret @@ array

  let compare ~cmp = Array.compare cmp

  let sort ?(pos=0) ?len array ~cmp =
    let array =  Array.slice array pos (match len with None -> Array.length array | Some len -> len) in
    Array.sort  array ~compare:cmp;
    array

  let stable_sort array ~cmp =
    let array = Array.copy array in
    stable_sort array ~compare:cmp;
    array

  let is_sorted array ~cmp = Array.is_sorted array ~compare:cmp

  let is_sorted_strictly array ~cmp = Array.is_sorted_strictly array ~compare:cmp

  let permute ?random_state array =
    let array = Array.copy array in
    Array.permute ?random_state array;
    array

end

(* Implements an immutable mapping from some keyset to data using type
   safe references *)
module IDMap = struct

  (* Abstract interface for IDmaps *)
  module type S = functor (Key: Map.Key) -> sig
    (* map type *)
    type 'a t

    module ID : sig
      type t
      val pp: Format.formatter -> t -> unit
      val compare : t -> t -> Core_kernel__.Import.int
      val t_of_sexp : Sexplib0.Sexp.t -> t
      val sexp_of_t : t -> Sexplib0.Sexp.t
      val hash_fold_t : Ppx_hash_lib.Std.Hash.state -> t -> Ppx_hash_lib.Std.Hash.state                           
      val hash : t -> Ppx_hash_lib.Std.Hash.hash_value
  end

    (** debug convert an id to string  *)
    val id_to_string: ID.t -> string

    (* construct an empty mapping *)
    val empty: 'a t

    (* immutably adds a new element to the mapping, returning the id and a new mapping *)
    val add_mapping: Key.t -> 'a -> 'a t -> ID.t * 'a t

    (* adds a mapping loading the value to be stored in a lazy fashion *)
    val add_mapping_lazy: Key.t -> ('a, 'b) Comp.t -> 'a t -> (ID.t * 'a t, 'b) Comp.t

    (* retrieves the data associated with an id *)
    val get_data: 'a t -> ID.t -> 'a

    (* set the data associated with an id *)
    val set_data: 'a t -> ID.t -> 'a -> 'a t

    (* update the data associated with an id *)
    val update_data: 'a t -> ID.t -> f:('a -> 'a) -> 'a t

    val map_data: 'a t -> f:('a -> 'b) -> 'b t

    val map_keyed_data: 'a t -> f:(Key.t -> 'a -> 'a) -> 'a t

    val map_keyed_data_lazy: 'a t -> f:(Key.t -> 'a -> ('a, 'b) Comp.t) -> ('a t, 'b) Comp.t

    val fold_data: 'a t -> init:'b -> f:('b -> 'a -> 'b) -> 'b

    val fold_keyed_data: 'a t -> init:'b -> f:('b -> Key.t * 'a -> 'b) -> 'b

    val foldmap_keyed_data_lazy:
        'a t -> 'state -> f:('state -> Key.t -> 'a -> ('state * 'a, 'b) Comp.t) -> ('state * 'a t, 'b) Comp.t
  end

  (* constructs a new IDmap with a dataset being the Key   *)
  module Make : S = functor (Key: Map.Key) -> struct

    module M = Map.Make (Key)

    type 'a t = {
      map: int M.t;
      data: 'a Vector.t;
    }


    module ID = struct
      type t = int
      [@@deriving sexp, ord, show, hash]
    end

    let id_to_string (value: ID.t) = Printf.sprintf "ID#%d" value

    let empty =
      {
        map = M.empty;
        data=Vector.empty;
      }

    let add_mapping (name: Key.t) (value: 'a) (map: 'a t) =
      let data = ref map.data in
      let result = ref (-1) in
      let map =
        M.change map.map name
          ~f:(fun ind ->
              begin 
                match ind with
                | None ->
                  let id = Vector.length !data in
                  result := id;
                  data := Vector.append !data value;
                  Some (id)
                | Some id ->
                  result := id;
                  Some id
              end
            ) in
      let data = !data in
      (!result, {data; map;})

    let add_mapping_lazy (name: Key.t) (value: ('a, 'b) Comp.t) (map: 'a t)  =
      let data = ref map.data in
      let result = ref @@ Ok (0, []) in
      Comp.lift @@ fun warnings -> 
      let map =
        M.change map.map name
          ~f:(fun ind ->
              begin 
                match ind with
                | None ->
                  let id = Vector.length !data in
                  let value = (Comp.unwrap value @@ warnings) in begin
                  match value with
                  | Ok (v, warns) -> 
                    data := Vector.append !data v;
                    result := Ok (id, warns);
                    Some (id)
                  | Error e ->
                    result := Error e;
                    None
                  end
                | Some id ->
                  result := Ok (id, warnings);
                  Some id
              end
            ) in
      let data = !data in
      match !result with
      | Ok (id, warns) -> Ok ((id, {data;map}), warns)
      | Error e -> Error e

    let get_data (map: 'a t) (id: ID.t) =
      Vector.get map.data id

    let set_data (map: 'a t) (id: ID.t) (data: 'a) =
      let data = Vector.set map.data id data in
      {map with data}

    let update_data (map: 'a t) (id: ID.t) ~f =
      let data = Vector.update map.data id ~f in
      {map with data}

    let map_data (map: 'a t) ~f =
      let data =  Vector.map map.data ~f in
      {map with data}

    let map_keyed_data (map: 'a t) ~(f: Key.t -> 'a -> 'b) : 'b t =
      let data =
        M.to_alist map.map
        |> List.fold ~init:map.data ~f:(fun acc (key,ind) ->
            Vector.update acc ind ~f:(fun value -> f key value)
          ) in
      {map with data}
    
    let map_keyed_data_lazy (map: 'a t) ~(f: Key.t -> 'a -> ('a,'b) Comp.t) : ('a t,'b) Comp.t =
      let open Comp.Syntax in
      let+ data =
        M.to_alist map.map
        |> Comp.fold ~init:map.data ~f:(fun acc (key,ind) ->
            Vector.update_lazy acc ind ~f:(fun value -> f key value)
          ) in
      ret {map with data}

    let foldmap_keyed_data_lazy (map: 'a t) (state: 'state)
        ~(f: 'state -> Key.t -> 'a -> ('state * 'a,'b) Comp.t) : ('state * 'a t,'b) Comp.t =
      let open Comp.Syntax in
      let+ (state, data) =
        M.to_alist map.map
        |> Comp.fold ~init:(state,map.data) ~f:(fun (state,vector) (key,ind) ->
            let state_ref = ref state in
            let+ vector = 
              Vector.update_lazy vector ind ~f:(fun value ->
                  let+ (state, value) = f state key value in
                  state_ref := state;
                  ret @@ value
                ) in
            ret @@ (!state_ref, vector)
          ) in
      ret (state, {map with data})

    let fold_data (map: 'a t) ~init ~f =
      Vector.fold map.data ~init ~f

    let fold_keyed_data (map: 'a t) ~init ~f =
      M.to_alist map.map
      |> List.map ~f:(fun (key,ind) -> key, Vector.get map.data ind)
      |> List.fold ~init ~f

  end

end

module StringMap = Map.Make (String)

module Timer = struct

  type t = {
    frame_count: int;
    frame_update_time: float;
    delta_time: float;
    last_time: float;
  }

  let init () = {
    frame_count = 0;
    frame_update_time = UnixLabels.gettimeofday ();
    delta_time = 0.0;
    last_time = Unix.gettimeofday ();
  }

  module Periodic = struct
    type 'a event = {last_queued_time: float; periodicity:float; job: 'a -> ('a, Error.t) Comp.t}

    let create (timer:t) ~periodicity job  = Comp.return @@ {last_queued_time=timer.last_time; periodicity; job} 

    let update (timer:t) (evt: 'a event) state =
      (* print_endline (Printf.sprintf "timer updating: %f - %f >= %f === %f >= %f === %b"
       *   timer.last_time evt.last_queued_time evt.periodicity
       *   (timer.last_time -. evt.last_queued_time) evt.periodicity
       *   Float.(timer.last_time - evt.last_queued_time >= evt.periodicity))
       * ; *)
      let open Comp.Syntax in
      if Float.(timer.last_time - evt.last_queued_time >= evt.periodicity) then begin
        let+ state = evt.job state in
        ret @@ ({evt with last_queued_time=timer.last_time}, state)
      end        
      else ret @@ (evt, state)

  end
  

  let update timer =
    let current_time = Unix.gettimeofday () in
     let delta_time = current_time -. timer.last_time
     and last_time = current_time 
     and frame_elapsed = current_time -. timer.frame_update_time in
     let (frame_update_time, frame_count, o_fps) =
       if Float.(frame_elapsed > 1.) then begin
         let fps = let open Float in of_int timer.frame_count / frame_elapsed  in
         current_time, 0, Some fps
       end else begin
         timer.frame_update_time, timer.frame_count, None
       end
    in
    let frame_count = frame_count + 1 in
    current_time, {frame_count; frame_update_time; delta_time; last_time}, o_fps

end

module NeStack: sig
  type 'a t
  val init : 'b -> 'b t
  val push : 'b -> 'b t -> 'b t
  val pop : 'a t -> 'a * 'a t
  val fold : f:('a -> 'b -> 'a) -> init:'a -> 'b t -> 'a
  val iter : 'a t -> f:('a -> unit) -> unit
  val exists : 'a t -> f:('a -> bool) -> bool
  val find : 'a t -> f:('a -> bool) -> 'a option
  val for_all : 'a t -> f:('a -> bool) -> bool
  val map : 'a t -> f:('a -> 'b) -> 'b t
  val fold_map : 'a t -> init:'b -> f:('b -> 'a -> 'b * 'c) -> 'b * ('c t)
  val to_list : 'a t -> 'a list
end = struct
  type 'a t = 'a * 'a list
  let init x = (x,[])
  let push x (h,ls) = x, h :: ls
  let pop = function
    | (h, []) -> (h, (h,[]))
    | (h, t :: ls) -> (h, (t, ls))
  let fold ~f ~init (h,ls) =
    let init = f init h in
    List.fold ~init ~f ls
  let iter (h,ls) ~f = f h; List.iter ~f ls
  let exists (h,ls) ~f = f h || List.exists ~f ls
  let find (h,t) ~f = if f h then Some h else List.find ~f t
  let for_all (h,ls) ~f = f h && List.for_all ~f ls
  let map (h,ls) ~f = (f h, List.map ~f ls)
  let fold_map (h,ls) ~init ~f =
    let init, new_h = f init h in
    let init, new_ls = List.fold_map ~init ~f ls in
    (init, (new_h, new_ls))
  let to_list (h, t) = h :: t
end

module Stack: sig
  type 'a t
  val empty : 'b t
  val push : 'b -> 'b t -> 'b t
  val pop : 'a t -> ('a * 'a t) option
  val fold : f:('a -> 'b -> 'a) -> init:'a -> 'b t -> 'a
  val iter : 'a t -> f:('a -> unit) -> unit
  val exists : 'a t -> f:('a -> bool) -> bool
  val find : 'a t -> f:('a -> bool) -> 'a option
  val for_all : 'a t -> f:('a -> bool) -> bool
  val map : 'a t -> f:('a -> 'b) -> 'b t
  val fold_map : 'a t -> init:'b -> f:('b -> 'a -> 'b * 'c) -> 'b * ('c t)
  val to_list : 'a t -> 'a list
end = struct
  type 'a t = 'a list
  let empty = []
  let push x ls = x :: ls
  let pop = function
    | [] -> None
    | t :: ls -> Some (t,  ls)
  let fold ~f ~init ls = List.fold ~init ~f ls
  let iter ls ~f = List.iter ~f ls
  let exists ls ~f = List.exists ~f ls
  let find t ~f = List.find ~f t
  let for_all ls ~f = List.for_all ~f ls
  let map ls ~f = List.map ~f ls
  let fold_map ls ~init ~f = List.fold_map ~init ~f ls
  let to_list xs = xs
end

