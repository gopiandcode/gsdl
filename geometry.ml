open Gg
open Core
open Utils

let p_vec_angle_cos v1 v2 v3 =
  let d1 =
    let open V3 in
    let vec = v1 - v2 in
    let mag = norm vec in
    vec / mag in
  let d2 =
    let open V3 in
    let vec = v3 - v2 in
    let mag = norm vec in
    vec / mag in
  let open Float in
  (V3.(x d1) * V3.(x d2)) + (V3.(y d1) * V3.(y d2)) + (V3.(z d1) * V3.(z d2))

let p_vec_angle v1 v2 v3 =
  let dot = p_vec_angle_cos v1 v2 v3 in
  if Float.(dot <= -1.0)
  then Float.pi
  else if Float.(dot >= 1.0)
  then 0.0
  else Float.acos(dot)

let p_triangle_angles v1 v2 v3 =
  let a1 = p_vec_angle v3 v1 v2 in
  let a2 = p_vec_angle v1 v2 v3 in
  let a3 = Float.(pi - a2 - a1) in
  (a1, a2, a3)


module Triangle = struct

  type t = {
    normal: V3.t;
    point: (V3.t *  V3.t * V3.t)
  }

  let (=) {normal=n1; point=(p11,p12,p13)} {normal=n2; point=(p21,p22,p23)}  =
    Core.List.fold ~init:true ~f:(&&) [V3.equal n1 n2; V3.equal p11 p21; V3.equal p12 p22; V3.equal p13 p23]

  let create ~normal p1 p2 p3 = {normal; point = (p1,p2,p3)}

  let to_string {normal; point=(p1,p2,p3)} =
    let to_str vec =
      let open Gg.V3 in
      Printf.sprintf "[%f, %f, %f]"  (x vec) (y vec) (z vec) in
    Printf.sprintf
      "Triangle { normal=%s; point=(%s,%s,%s) }"
      (to_str normal)
      (to_str p1)
      (to_str p2)
      (to_str p3)



  let calculate_extent triangles =
    let to_v3 (x,y,z) = V3.v x y z in
    let max,min =
      List.fold  triangles ~init:((-1e10,-1e10,-1e10),(1e10,1e10,1e10))
        ~f:(fun ((max_x,max_y,max_z),(min_x,min_y,min_z)) ({point=(p1,p2,p3); _})  ->
            List.fold [p1;p2;p3] ~init:(((max_x,max_y,max_z),(min_x,min_y,min_z)))
              ~f:(fun (((max_x,max_y,max_z),(min_x,min_y,min_z))) v ->
                  let update_max (vl:float) (vl':float) = if Float.(vl < vl') then vl' else vl in
                  let update_min (vl:float) (vl':float) = if Float.(vl > vl') then vl' else vl in
                  (update_max max_x (V3.x v), update_max max_y (V3.y v), update_max max_z (V3.z v)),
                  (update_min min_x (V3.x v), update_min min_y (V3.y v), update_min min_z (V3.z v))
                )
          ) in
    to_v3 max, to_v3 min

  let calculate_element_count triangles =
    List.length (triangles) * 3 * 3

  let calculate_translate triangles =
    let max,min = calculate_extent triangles in
    let center = V3.(max + min / 2.0) in
    let move = V3.(neg center) in
    move

  let split_data triangles =
    List.map ~f:(fun ({normal; point=(p1,p2,p3)}) -> ([normal; normal; normal], [p1;  p2; p3]))
      triangles
    |> List.unzip
    |> begin fun (normals, points) ->  List.concat normals, List.concat points end

  let serialize_data triangles =
    split_data triangles
    |> begin fun (normals, points) ->
      Graphics_utils.v3list_to_array normals, Graphics_utils.v3list_to_array points
    end


end

module Mesh = struct

  type vertex = MkVert
  type vertex_with_normals = MkVert
  type vertex_with_texture = MkVert
  type vertex_with_texture_and_normals = MkVert

  type 'a face =
    | Vertex : (int * int * int)  -> vertex face
    | Vertex_with_normals : { v1:int;v2:int;v3:int;n1:int; n2:int; n3:int; } -> vertex_with_normals face
    | Vertex_with_textures : { v1:int;v2:int;v3:int; t1:int; t2:int; t3:int } -> vertex_with_texture face
    | Vertex_with_texture_and_normals :
        { v1:int;v2:int;v3:int;
          t1:int; t2:int; t3:int;
          n1:int; n2:int; n3:int; } -> vertex_with_texture_and_normals face

  let pp_face  (type a) (formatter: Format.formatter)  (face: a face) =
    match face with
    | Vertex (v1, v2, v3) ->
      Format.pp_print_string formatter "Vertex";
      Format.pp_print_space formatter ();
      Format.pp_open_box formatter 10;
      Format.pp_print_list (Format.pp_print_int) formatter [v1;v2;v3];
      Format.pp_close_box formatter ()
    | Vertex_with_normals { v1; v2; v3; n1; n2; n3; } ->
      Format.pp_print_string formatter "Vertex_with_normals";
      Format.pp_open_box formatter 10;
      Format.pp_print_space formatter ();
      Format.pp_print_string formatter "vertex";
      Format.pp_print_space formatter ();
      Format.pp_print_string formatter "=";
      Format.pp_print_space formatter ();
      Format.pp_print_list (Format.pp_print_int) formatter [v1;v2;v3];
      Format.pp_print_string formatter ",";
      Format.pp_print_space formatter ();
      Format.pp_print_string formatter "normal";
      Format.pp_print_space formatter ();
      Format.pp_print_string formatter "=";
      Format.pp_print_space formatter ();
      Format.pp_print_list (Format.pp_print_int) formatter [n1;n2;n3];
      Format.pp_close_box formatter ()
    | Vertex_with_textures { v1; v2; v3; t1; t2; t3 } ->
      Format.pp_print_string formatter "Vertex_with_textures";
      Format.pp_open_box formatter 10;
      Format.pp_print_space formatter ();
      Format.pp_print_string formatter "vertex";
      Format.pp_print_space formatter ();
      Format.pp_print_string formatter "=";
      Format.pp_print_space formatter ();
      Format.pp_print_list (Format.pp_print_int) formatter [v1;v2;v3];
      Format.pp_print_string formatter ",";
      Format.pp_print_space formatter ();
      Format.pp_print_string formatter "textures";
      Format.pp_print_space formatter ();
      Format.pp_print_string formatter "=";
      Format.pp_print_space formatter ();
      Format.pp_print_list (Format.pp_print_int) formatter [t1;t2;t3];
      Format.pp_close_box formatter ()
    | Vertex_with_texture_and_normals { v1; v2; v3; t1; t2; t3; n1; n2; n3 } ->
      Format.pp_print_string formatter "Vertex_with_textures_and_normals";
      Format.pp_open_box formatter 10;
      Format.pp_print_space formatter ();
      Format.pp_print_string formatter "vertex";
      Format.pp_print_space formatter ();
      Format.pp_print_string formatter "=";
      Format.pp_print_space formatter ();
      Format.pp_print_list (Format.pp_print_int) formatter [v1;v2;v3];
      Format.pp_print_string formatter ",";
      Format.pp_print_space formatter ();
      Format.pp_print_string formatter "textures";
      Format.pp_print_space formatter ();
      Format.pp_print_string formatter "=";
      Format.pp_print_space formatter ();
      Format.pp_print_list (Format.pp_print_int) formatter [t1;t2;t3];
      Format.pp_print_string formatter ",";
      Format.pp_print_space formatter ();
      Format.pp_print_string formatter "normal";
      Format.pp_print_space formatter ();
      Format.pp_print_string formatter "=";
      Format.pp_print_space formatter ();
      Format.pp_print_list (Format.pp_print_int) formatter [n1;n2;n3];
      Format.pp_close_box formatter ()


  type face_data =
    | MkDataV of vertex face list
    | MkDataVN of vertex_with_normals face list
    | MkDataVT of vertex_with_texture face list
    | MkDataVTN of vertex_with_texture_and_normals face list


  let pp_face_data formatter (data:face_data) =
                match data with
                | MkDataV ls ->
                        Format.pp_print_string formatter "MkDataV";
                        Format.pp_print_space formatter ();
                        Format.pp_open_box formatter 10;
                        Format.pp_print_list pp_face formatter ls;
                        Format.pp_close_box formatter ()
                | MkDataVN ls ->
                        Format.pp_print_string formatter "MkDataVN";
                        Format.pp_print_space formatter ();
                        Format.pp_open_box formatter 10;
                        Format.pp_print_list pp_face formatter ls;
                        Format.pp_close_box formatter ()
                | MkDataVT ls ->
                        Format.pp_print_string formatter "MkDataVT";
                        Format.pp_print_space formatter ();
                        Format.pp_open_box formatter 10;
                        Format.pp_print_list pp_face formatter ls;
                        Format.pp_close_box formatter ()
                | MkDataVTN ls ->
                        Format.pp_print_string formatter "MkDataVTN";
                        Format.pp_print_space formatter ();
                        Format.pp_open_box formatter 10;
                        Format.pp_print_list pp_face formatter ls;
                        Format.pp_close_box formatter ()

  type t = {
    vertex_data: V3.t array;
    normal_data: V3.t array;
    texture_data: V2.t array;
    faces: face_data;
  }
  [@@deriving show]

  let of_triangle_list (ls: Triangle.t list) =
                let id = ref 0 in
                let vertex_data = ref [] in
                let map = Hashtbl.create (module V3) in

                let nid = ref 0 in
                let normal_data = ref [] in
                let nmap = Hashtbl.create (module V3) in

                let vid v = Hashtbl.find_or_add map v
        ~default:(fun () ->
            let result = !id in
            id := !id + 1;
            vertex_data := v :: !vertex_data;
            result) in
                let nid v = Hashtbl.find_or_add nmap v
        ~default:(fun () ->
            let result = !nid in
            nid := !nid + 1;
            normal_data := v :: !normal_data;
            result) in

                let faces =
                        List.map ls
        ~f:(fun { normal; point=(p1, p2, p3) } ->
            (nid normal, (vid p1, vid p2, vid p3)))
                        |> List.map ~f:(fun (normal, (v1,v2,v3)) ->
          Vertex_with_normals {v1;v2;v3;n1=normal; n2=normal; n3=normal}
        )
                        |> fun v -> MkDataVN v
                in
                {
                        vertex_data = Array.of_list (List.rev !vertex_data);
                        normal_data = Array.of_list (List.rev !normal_data);
                        texture_data = Array.create ~len:0 (V2.v 0.0 0.0);
                        faces
                }

  let to_uv_list ({ texture_data; faces; _ }: t) =
                match faces with
                | MkDataVT ls ->
                        Some (
        List.map
          ~f:(fun (Vertex_with_textures { t1; t2; t3; _ }) ->
              texture_data.(t1), texture_data.(t2), texture_data.(t3))
          ls)
                | MkDataVTN ls ->
                        Some (
        List.map
          ~f:(fun (Vertex_with_texture_and_normals { t1; t2; t3; _ }) ->
              texture_data.(t1), texture_data.(t2), texture_data.(t3))
          ls
                             )
                | _ -> None

  let calculate_extent ({ vertex_data; _  }: t) =
    let to_v3 (x,y,z) = V3.v x y z in
    let (min,max) =
      Array.fold vertex_data
        ~init:((-1e10,-1e10,-1e10),(1e10,1e10,1e10))
        ~f:(fun (((max_x,max_y,max_z),(min_x,min_y,min_z))) v ->
            let update_max (vl:float) (vl':float) = if Float.(vl < vl') then vl' else vl in
            let update_min (vl:float) (vl':float) = if Float.(vl > vl') then vl' else vl in
            (update_max max_x (V3.x v), update_max max_y (V3.y v), update_max max_z (V3.z v)),
            (update_min min_x (V3.x v), update_min min_y (V3.y v), update_min min_z (V3.z v))
          )
    in
    to_v3 max, to_v3 min

  let calculate_translate (triangles:t) =
    let max,min = calculate_extent triangles in
    let center = V3.(max + min / 2.0) in
    let move = V3.(neg center) in
    move

  (* returns (vertex_data, normal_data, texture_data) *)
  let serialize_data ({ vertex_data; normal_data; texture_data; faces }: t) =
        match faces with
        | MkDataV ls ->
            (List.concat_map ls
         ~f:(fun (Vertex (v1,v2,v3)) ->
             [vertex_data.(v1); vertex_data.(v2); vertex_data.(v3)]
           )), None, None
        | MkDataVN ls ->
            let vertex_data, normal_data =
        List.map ls
          ~f:(fun (Vertex_with_normals {v1;v2;v3;n1;n2;n3}) ->
              [vertex_data.(v1); vertex_data.(v2); vertex_data.(v3)],
              [normal_data.(n1); normal_data.(n2); normal_data.(n3)]
            )
        |> List.unzip in
            let vertex_data, normal_data =
        List.concat vertex_data, List.concat normal_data in
            vertex_data, (Some normal_data), None
        | MkDataVT ls ->
            let vertex_data, texture_data =
        List.map ls
          ~f:(fun (Vertex_with_textures {v1;v2;v3;t1;t2;t3}) ->
              [vertex_data.(v1); vertex_data.(v2); vertex_data.(v3)],
              [texture_data.(t1); texture_data.(t2); texture_data.(t3)]
            )
        |> List.unzip
            in
            let vertex_data, texture_data =
        List.concat vertex_data, List.concat texture_data in
            vertex_data, None, (Some texture_data)
        | MkDataVTN ls ->
            let vertex_data, normal_data, texture_data =
        List.map ls
          ~f:(fun (Vertex_with_texture_and_normals {v1;v2;v3;n1;n2;n3;t1;t2;t3}) ->
              [vertex_data.(v1); vertex_data.(v2); vertex_data.(v3)],
              [normal_data.(n1); normal_data.(n2); normal_data.(n3)],
              [texture_data.(t1); texture_data.(t2); texture_data.(t3)]
            )
        |> List.unzip3
            in
            let vertex_data, normal_data, texture_data =
        List.concat vertex_data, List.concat normal_data, List.concat texture_data in
            vertex_data, Some normal_data, (Some texture_data)

  let calculate_element_count ({  faces; _ }:t) =
    match faces with
    | MkDataV ls -> List.length ls
    | MkDataVN ls -> List.length ls
    | MkDataVT ls -> List.length ls
    | MkDataVTN ls -> List.length ls

end

