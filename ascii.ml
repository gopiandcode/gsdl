open Core
open Stl_lexer
open Lexing
open Utils

let print_position () lexbuf =
  let pos = lexbuf.lex_curr_p in
  Printf.sprintf "%s:%d:%d" pos.pos_fname pos.pos_lnum (pos.pos_cnum - pos.pos_bol + 1)

let parse_with_error lexbuf =
  let open Comp.Syntax in
  try ret @@ Stl_grammar.spec Stl_lexer.read lexbuf with
  | SyntaxError msg ->
    Comp.fail (Error.of_string (Printf.sprintf "%a: %s\n" print_position lexbuf msg))
  | Stl_grammar.Error ->
    Comp.fail (Error.of_string (Printf.sprintf "%a: %s\n" print_position lexbuf "syntax error"))

let parse_string str =
  let open Comp.Syntax in
  let lexbuf = Lexing.from_string str in
  lexbuf.lex_curr_p <- { lexbuf.lex_curr_p with pos_fname = "" };
  let+ parsed = parse_with_error lexbuf in
  Comp.of_option parsed ~error:(Error.of_string "Invalid stl syntax - ids are not equal")
    



let parse_file filename =
  let open Comp.Syntax in
  try
    In_channel.with_file filename ~f:(fun inx ->
        let lexbuf = Lexing.from_channel inx in
        lexbuf.lex_curr_p <- { lexbuf.lex_curr_p with pos_fname = filename };
        let+ parsed = parse_with_error lexbuf in
        Comp.of_option parsed ~error:(Error.of_string "Invalid stl syntax - ids are not equal")
      )
  with
    Sys_error e -> Comp.fail (Error.of_string e)      
