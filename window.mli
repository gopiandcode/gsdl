open Core
open Utils

type t
type graphics_state = {width:int; height:int; logical_width: int; logical_height: int; resized:bool; x:int; y:int}

val init : ?logical_dimensions:(int * int) -> unit -> (t, Error.t) Comp.t

(* returns the logical dimensions of the window *)
val logical_dims: t -> int * int

(* set the title of the window *)
val set_title: t -> string -> unit

(* poll events *)
val poll_events:
  t -> (t * Input.event list, Error.t) Comp.t

(* retrieve mouse position *)
val mouse_state: t -> (int * int) * bool * bool * bool

val queue_exit: t -> t

val to_graphics_state: t -> graphics_state

(* main loop *)
val main_loop: ?debug:bool -> init:'a -> t -> f:('a -> t -> ('a * t, 'c) Comp.t) -> ('a, 'c) Comp.t
