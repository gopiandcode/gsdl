#include "open_nl/OpenNL_psm.h"
#include <caml/mlvalues.h>
#include <math.h>

void print_data(
                int no_faces, int no_vertices, int faces[][3],
                float meta_components[][4], float uv[][2],
                int pinned_vertex_bool[]
                ) {
  printf("open_nl_lscm_solve called\n");
  printf("faces:\n[\n");
  for(int faces_id = 0; faces_id < no_faces; faces_id++){
    printf("\t %d %d %d \n", faces[faces_id][0], faces[faces_id][1], faces[faces_id][2]);
  }
  printf("\n]\n");
  printf("meta_components:\n[\n");
  for(int faces_id = 0; faces_id < no_faces; faces_id++){
    printf("\t %f %f %f %f \n",
           meta_components[faces_id][0],
           meta_components[faces_id][1],
           meta_components[faces_id][2],
           meta_components[faces_id][3]
           );
  }
  printf("\n]\n");
  printf("uv:\n[\n");
  for(int faces_id = 0; faces_id < no_vertices; faces_id++){
    printf("\t %f %f - locked = %d\n",
           uv[faces_id][0],
           uv[faces_id][1],
           pinned_vertex_bool[faces_id]);
  }
  printf("\n]\n");
  printf("creating NL Context\n");
}

int open_nl_lscm_solve(int no_faces, int no_vertices, 
                       int faces[][3], /* no faces */
                       float meta_components[][4], /* no faces */
                       float uv[][2],              /* no vertices */
                       int pinned_vertex_bool[]    /* no vertices */
                       ) {
  /* print_data( */
  /*           no_faces,  no_vertices,  faces, */
  /*           meta_components,  uv, */
  /*           pinned_vertex_bool */
  /*           ); */

  int success = 0;
  NLContext context = nlNewContext();
  nlMakeCurrent(context);
  nlSolverParameteri(NL_SOLVER, NL_CG);
  nlSolverParameteri(NL_PRECONDITIONER, NL_PRECOND_JACOBI);
  nlSolverParameteri(NL_NB_VARIABLES, (no_vertices * 2));
  nlSolverParameteri(NL_MAX_ITERATIONS, (5*no_vertices));
  nlSolverParameteri(NL_LEAST_SQUARES, NL_TRUE);
  nlSolverParameterd(NL_THRESHOLD, 1e-10);
    //  nlSolverParameterd(NL_THRESHOLD, 0.0);	    

  //  printf("nlBegin(NL_SYSTEM)\n");
  nlBegin(NL_SYSTEM);

  /* set initial solution, and pin variables - use nlSetVariable() */
  for(int pin_id = 0; pin_id < no_vertices; pin_id++) {
    if(pinned_vertex_bool[pin_id]) {
      //      printf("nlLockVariable(%d)\n", 2 * pin_id);
      nlLockVariable(2 * pin_id);
      //      printf("nlLockVariable(%d)\n", 2 * pin_id + 1);
      nlLockVariable(2 * pin_id + 1);
    }
    //    printf("nlSetVariable(%d, %f)\n", 2 * pin_id, uv[pin_id][0]);
    nlSetVariable(2 * pin_id, uv[pin_id][0]);
    //    printf("nlSetVariable(%d, %f)\n", 2 * pin_id + 1, uv[pin_id][1]);
    nlSetVariable(2 * pin_id + 1, uv[pin_id][1]);
  }


  //  printf("nlBegin(NL_MATRIX)\n");
  nlBegin(NL_MATRIX);

  for(int face_id = 0; face_id < no_faces; face_id++ ) {

    float a = meta_components[face_id][0];
    float b = meta_components[face_id][1];
    float c = meta_components[face_id][2];
    float d = meta_components[face_id][3];

    int id0 = faces[face_id][0];
    int id1 = faces[face_id][1];
    int id2 = faces[face_id][2];

    int u0_id = 2 * id0;
    int v0_id = 2 * id0 + 1;
    int u1_id = 2 * id1;
    int v1_id = 2 * id1 + 1;
    int u2_id = 2 * id2;
    int v2_id = 2 * id2 + 1;

    //    printf("nlBegin(NL_ROW)\n");
            nlBegin(NL_ROW);
            //            printf("nlCoefficient(%d, %f)\n", u0_id, -a + c);
		nlCoefficient(u0_id, -a + c);
                //		printf("nlCoefficient(%d, %f)\n", v0_id, b - d);
		nlCoefficient(v0_id, b - d);
                //		printf("nlCoefficient(%d, %f)\n", u1_id,  -c);
		nlCoefficient(u1_id, -c);
                //		printf("nlCoefficient(%d, %f)\n", v1_id,  d);
		nlCoefficient(v1_id, d);
                //		printf("nlCoefficient(%d, %f)\n", u2_id,  a);
		nlCoefficient(u2_id, a);
                //    printf("nlEnd(NL_ROW)\n");
            nlEnd(NL_ROW);


            //    printf("nlBegin(NL_ROW)\n");
            nlBegin(NL_ROW);
            //            printf("nlCoefficient(%d, %f)\n", u0_id,  -b + d);
		nlCoefficient(u0_id, -b + d);
                //		printf("nlCoefficient(%d, %f)\n", u0_id,  -a + c);
		nlCoefficient(u0_id, -a + c);
                //		printf("nlCoefficient(%d, %f)\n", u1_id,  -d);
		nlCoefficient(u1_id, -d);
                //		printf("nlCoefficient(%d, %f)\n", v1_id,  -c);
		nlCoefficient(v1_id, -c);
                //		printf("nlCoefficient(%d, %f)\n", v2_id,  a);
		nlCoefficient(v2_id, a);
                //    printf("nlEnd(NL_ROW)\n");
            nlEnd(NL_ROW);
    
  }

  /* printf("nlEnd(NL_MATRIX)\n"); */
  nlEnd(NL_MATRIX);
  /* printf("nlEnd(NL_SYSTEM)\n"); */
  nlEnd(NL_SYSTEM);

  success = nlSolve();
  /* printf("ran solver with success %d\n", success); */

  if(success) {
    int uid = 0;

    for(int id = 0; id < no_vertices; id++) {
      uv[uid][0] = nlGetVariable(2 * id);
      uv[uid++][1] = nlGetVariable(2 * id + 1);
    }
  }

  nlDeleteContext(context);
  return success;
}

/* NLContext uv_lscm_begin(int no_vertices, int no_faces) { */
/*   NLContext x = nlNewContext(); */
/*   nlSolverParameteri(NL_NB_VARIABLES, no_vertices); */
/*   nlSolverParameteri(NL_ROW, no_faces); */
/*   nlSolverParameteri(NL_LEAST_SQUARES, NL_TRUE); */
/*   return x; */
/* } */

/*
let p_vec_angle_cos v1 v2 v3 =
     let d1 =
       let open V3 in
       let vec = v1 - v2 in
       let mag = norm vec in
       vec / mag in
     let d2 =
       let open V3 in
       let vec = v3 - v2 in
       let mag = norm vec in
       vec / mag in
     let open Float in
       ((x d1) * (x d2)) + ((y d1) * (y d2)) + ((z d1) * (z d2))

 let p_vec_angle v1 v2 v3 =
    let dot = p_vec_angle_cos v1 v2 v3 in
    if Float.(dot <= -1.0)
    then Float.pi
    else if Float.(dot >= 1.0)
    then 0.0
    else Float.acos(dot)

let p_triangle_angles v1 v2 v3 =
   let a1 = p_vec_angle v3 v1 v2 in
   let a2 = p_vec_angle v1 v2 v3 in
   let a3 = Float.(pi - a2 - a1) in
   (a1, a2, a3)

 */


/* void uv_lscm_solve(NLContext context, int length, float angles[][3], float vec[][3], float uv[][2]) { */
/*   nlBegin(NL_SYSTEM); */
/*   nlBegin(NL_MATRIX); */


/*   for(int j = 0; j < length; j++) { */
/*     float a1 = angles[j][0]; */
/*     float a2 = angles[j][1]; */
/*     float a3 = angles[j][2]; */

/*     float sina1 = sin(a1); */
/*     float sina2 = sin(a2); */
/*     float sina3 = sin(a3); */
/*     float sinmax = (sina1 >= sina2 ? (sina1 >= sina3 ? sina1 : sina3) : (sina2 >= sina3 ? sina2 : sina3)); */

/*     if(sina3 != sinmax) { */
/*       v1, v2, v3 = v3, v1, v2; */
/*       a1, a2, a3 = a3, a1, a2; */
/*       sina1, sina2, sina3 = sina3, sina1, sina2; */
/*       if(sin2 == sinmax) { */
/*         v1, v2, v3 = v3, v1, v2; */
/*         a1, a2, a3 = a3, a1, a2; */
/*         sina1, sina2, sina3 = sina3, sina1, sina2; */
/*       } */
/*     } */

/*     float ratio = (sina3 == 0.0) ? 1.0 : sin2 / sin3; */
/*     float cosine = cosf(a1)*ratio; */
/*     float sine = sina1 * ratio; */
    

/*     for(int i = 0; i < 3; i ++) { */
/*       nlBegin(NL_ROW); */
/*       nlCoefficient(i, vec[j][i], cosine - 1.0); */
/*       nlCoefficient(i, vec[j][i + 1], -sine); */
/*       nlCoefficient(i, vec[j][i + 1], -cosine); */
/*       nlCoefficient(i, vec[j][i + 1], sine); */
/*       nlCoefficient(i, vec[j][i + 1], 1.0); */
/*       nlEnd(NL_ROW); */

/*       nlBegin(NL_ROW); */
/*       nlCoefficient(i, vec[j][i], sine); */
/*       nlCoefficient(i, vec[j][i + 1], cosine - 1.0); */
/*       nlCoefficient(i, vec[j][i + 1], -sine); */
/*       nlCoefficient(i, vec[j][i + 1], -cosine); */
/*       nlCoefficient(i, vec[j][i + 1], 1.0); */
/*       nlEnd(NL_ROW); */

/*     } */
/*   } */

/*   nlEnd(NL_MATRIX); */
/*   nlEnd(NL_SYSTEM); */

/*   bool value = nlSolve(); */

/*   if (value) { */
/*     for(int i = 0; i<length; i++) { */
/*       uv[i][0] = nlGetVariable(i); */
/*       uv[i][1] = nlGetVariable(i); */
/*     } */
/*   } */

/*   return value; */
/* } */



