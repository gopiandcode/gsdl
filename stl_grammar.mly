%{
[@@@warning "-33"]
%}
%token <float> FLOAT
%token <string> ID
%token SOLID_START
%token SOLID_END
%token NORMAL
%token FACET_START
%token FACET_END
%token OUTER_START
%token LOOP
%token OUTER_END
%token VERTEX
%token EOF
%start <Geometry.Triangle.t list option> spec
%%

spec:
  | SOLID_START; id1 = ID; facs = facets; SOLID_END; id2 = ID; EOF
    { if String.(id1 = id2) then Some facs else None }
;

facet:
  | FACET_START; NORMAL; norm = vector; ps = outers; FACET_END
    { (match ps with (p1,p2,p3) -> Geometry.Triangle.create ~normal:norm p1 p2 p3) }
;

facets:
  | facs = list(facet) { facs }
;

vector:
  | f1 = FLOAT; f2 = FLOAT; f3 = FLOAT
    { Gg.V3.v f1 f2 f3 }
;

outers:
  | OUTER_START; LOOP; VERTEX; v1 = vector; VERTEX; v2 = vector; VERTEX; v3 = vector; OUTER_END
    { (v1,v2,v3) }
;

