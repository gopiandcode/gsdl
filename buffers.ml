open Core
open Tgl3

module VBO = struct

  type t = int32

  let create () : t =
    let buf = Bigarray.Array1.create Bigarray.Int32 Bigarray.C_layout 1 in
    Gl.gen_buffers 1 buf;
    buf.{0}

  let bind_to_array_buffer t =
    Gl.bind_buffer Gl.array_buffer (Int.of_int32_exn t)

  let buffer_to_array_buffer ~size ~data ~usage_hint =
    Gl.buffer_data Gl.array_buffer size data usage_hint

  let unbind_array_buffer () =
    Gl.bind_buffer Gl.array_buffer 0

  let delete t =
    let buf = Bigarray.Array1.create Bigarray.Int32 Bigarray.C_layout 1 in
    buf.{0} <- t;
    Gl.delete_buffers 1 buf

end


module VAO = struct

  type t = int32

  let create () : t =
    let buf = Bigarray.Array1.create Bigarray.Int32 Bigarray.C_layout 1 in
    Gl.gen_vertex_arrays 1 buf;
    buf.{0}

  let delete t =
    let buf = Bigarray.Array1.create Bigarray.Int32 Bigarray.C_layout 1 in
    buf.{0} <- t;
    Gl.delete_vertex_arrays 1 buf

  let unbind () =
    Gl.bind_vertex_array 0

  let bind (t: Int32.t) =
    Gl.bind_vertex_array (Int.of_int32_exn t)

  let enable_attribute id =
    Gl.enable_vertex_attrib_array id

  let set_attrib_divisor ~position ~divisor =
    Gl.vertex_attrib_divisor position divisor

  let bind_attributes ~position ~size ~typ ~normalized ~stride ~offset =
    Gl.vertex_attrib_pointer
      position
      size
      typ
      normalized
      stride
      (`Offset offset)

end
