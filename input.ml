open Tsdl

type input =
  | Up
  | Down
  | Left
  | Right
  | Enter
  | Escape
  | Backspace
  | Console

type event =
  | ControlInput of input * bool * bool
  | CharacterInput of char * bool * bool
  | MouseMotion of int * int

let process_sdl_event (keycode:int) (repeat: bool) (keyup: bool) : event option  =
  let input_messages =
    let open Sdl.K in
    match keycode with
      v when v = a ->   Some ((CharacterInput ('a',repeat, keyup)))
    | v when v = b ->  Some ((CharacterInput ('b', repeat, keyup)))
    | v when v = c ->  Some ((CharacterInput ('c', repeat, keyup)))
    | v when v = d ->  Some ((CharacterInput ('d', repeat, keyup)))
    | v when v = e ->  Some ((CharacterInput ('e', repeat, keyup)))
    | v when v = f ->  Some ((CharacterInput ('f', repeat, keyup)))
    | v when v = g ->  Some ((CharacterInput ('g', repeat, keyup)))
    | v when v = h ->  Some ((CharacterInput ('h', repeat, keyup)))
    | v when v = i ->  Some ((CharacterInput ('i', repeat, keyup)))
    | v when v = j ->  Some ((CharacterInput ('j', repeat, keyup)))
    | v when v = k ->  Some ((CharacterInput ('k', repeat, keyup)))
    | v when v = l ->  Some ((CharacterInput ('l', repeat, keyup)))
    | v when v = m ->  Some ((CharacterInput ('m', repeat, keyup)))
    | v when v = n ->  Some ((CharacterInput ('n', repeat, keyup)))
    | v when v = o ->  Some ((CharacterInput ('o', repeat, keyup)))
    | v when v = p ->  Some ((CharacterInput ('p', repeat, keyup)))
    | v when v = q ->  Some ((CharacterInput ('q', repeat, keyup)))
    | v when v = r ->  Some ((CharacterInput ('r', repeat, keyup)))
    | v when v = s ->  Some ((CharacterInput ('s', repeat, keyup)))
    | v when v = t ->  Some ((CharacterInput ('t', repeat, keyup)))
    | v when v = u ->  Some ((CharacterInput ('u', repeat, keyup)))
    | v when v = Sdl.K.v ->  Some ((CharacterInput ('v', repeat, keyup)))
    | v when v = w ->  Some ((CharacterInput ('w', repeat, keyup)))
    | v when v = x ->  Some ((CharacterInput ('x', repeat, keyup)))
    | v when v = y ->  Some ((CharacterInput ('y', repeat, keyup)))
    | v when v = z ->  Some ((CharacterInput ('z', repeat, keyup)))

    | v when v = Sdl.K.comma -> Some ((CharacterInput (',',repeat, keyup)))
    | v when v = Sdl.K.stop ->  Some ((CharacterInput ('.',repeat, keyup)))

    | v when v = Sdl.K.k0 ->  Some ((CharacterInput ('0',repeat, keyup)))
    | v when v = Sdl.K.k1 ->  Some ((CharacterInput ('1',repeat, keyup)))
    | v when v = Sdl.K.k2 ->  Some ((CharacterInput ('2',repeat, keyup)))
    | v when v = Sdl.K.k3 ->  Some ((CharacterInput ('3', repeat, keyup)))
    | v when v = Sdl.K.k4 ->  Some ((CharacterInput ('4', repeat, keyup)))
    | v when v = Sdl.K.k5 ->  Some ((CharacterInput ('5', repeat, keyup)))
    | v when v = Sdl.K.k6 ->  Some ((CharacterInput ('6', repeat, keyup)))
    | v when v = Sdl.K.k7 ->  Some ((CharacterInput ('7', repeat, keyup)))
    | v when v = Sdl.K.k8 ->  Some ((CharacterInput ('8', repeat, keyup)))
    | v when v = Sdl.K.k9 ->  Some ((CharacterInput ('9', repeat, keyup)))

    | v when v = space ->  Some ((CharacterInput (' ',repeat, keyup)))
    | v when v = backslash ->  Some ((CharacterInput ('\\',repeat, keyup)))

    | v when v = backspace -> Some ((ControlInput (Backspace, repeat, keyup)))
    | v when v = return -> Some ((ControlInput (Enter, repeat, keyup)))
    | v when v = escape -> Some ((ControlInput (Escape,repeat, keyup)))
    | v when v = Sdl.K.up ->  Some ((ControlInput (Up,repeat, keyup)))
    | v when v = Sdl.K.down -> Some ((ControlInput (Down,repeat, keyup)))
    | v when v = Sdl.K.left -> Some (ControlInput (Left, repeat, keyup))
    | v when v = Sdl.K.right -> Some (ControlInput (Right, repeat, keyup))
    | v when v = Sdl.K.backspace -> Some (ControlInput (Backspace, repeat, keyup))
    | _ -> None
  in
  input_messages
