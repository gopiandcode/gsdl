[@@@warning "-33"]
open Core

let print_program str =
  let lexbuf =   Lexing.from_string ~with_positions:true str in
  let o_expr = Parser.program (Lexer.token) lexbuf in
  o_expr
  
let print_expr str =
  let lexbuf =   Lexing.from_string ~with_positions:true str in
  let o_expr = Parser._expr_only_file (Lexer.token) lexbuf in
  o_expr

let print_type_expr str =
  let lexbuf =   Lexing.from_string ~with_positions:true str in
  let o_expr = Parser._type_expr_only_file (Lexer.token) lexbuf in
  o_expr

let print_decl str =
  let lexbuf =   Lexing.from_string ~with_positions:true str in
  let o_expr = Parser._declaration_only_file (Lexer.token) lexbuf in
  o_expr

let print_data str =
  let lexbuf =   Lexing.from_string ~with_positions:true str in
  let o_expr = Parser._data_only_file (Lexer.token) lexbuf in
  o_expr

let print_modifier str =
  let lexbuf =   Lexing.from_string ~with_positions:true str in
  let o_expr = Parser._modifier_only_file (Lexer.token) lexbuf in
  o_expr

let print_object str =
  let lexbuf =   Lexing.from_string ~with_positions:true str in
  let o_expr = Parser._object_only_file (Lexer.token) lexbuf in
  o_expr


let loc =
  { Location.loc_start = {pos_fname=""; pos_lnum=0; pos_bol=0; pos_cnum=0 };
             loc_end   = {pos_fname=""; pos_lnum=0; pos_bol=0; pos_cnum=0 } }


let%test_unit "parse_bool" =
  [%test_result: Parsetree.expr option]

  (print_expr "true")
  ~expect:(Some (Parsetree.Exp_bool
         { Location.value = true;
           loc;
           }))

let%test_unit "parse_int" =
  [%test_result: Parsetree.expr option]
    (print_expr "1")
    ~expect:(Some (Parsetree.Exp_number
                     { Location.value = (Parsetree.Int 1); loc; }))



let%expect_test "parse_binexpr" =
  [%test_result: Parsetree.expr option]
  (print_expr "1 * (-10.0 + a)")
  ~expect: (Some
    (Parsetree.Exp_binop
       { Location.value =
         (Parsetree.Mul,
          (Parsetree.Exp_number
             { Location.value = (Parsetree.Int 1);
               loc }),
          (Parsetree.Exp_binop
             { Location.value =
               (Parsetree.Plus,
                (Parsetree.Exp_preop
                   { Location.value =
                     (Parsetree.UMinus,
                      (Parsetree.Exp_number
                         { Location.value = (Parsetree.Float 10.);
                           loc;
                           }));
                     loc;
                   }),
                (Parsetree.Exp_variable
                   { Location.value = "a";
                     loc;
                     }));
               loc;
               }));
         loc;
         })) 
 

let%test_unit "parse_boolean" =
  [%test_result: Parsetree.expr option]
  (print_expr "true || !false")
  ~expect: (Some
      (Parsetree.Exp_binop
         { Location.value =
           (Parsetree.Or,
            (Parsetree.Exp_bool
               { Location.value = true;
                 loc;
               }),
            (Parsetree.Exp_preop
               { Location.value =
                 (Parsetree.UNot,
                  (Parsetree.Exp_bool
                     { Location.value = false;
                       loc;
                       }));
                 loc;
                 }));
           loc;
           }))
 

let%test_unit "parse_index" =
  [%test_result: Parsetree.expr option]
  (print_expr "(a + 10)[10 + 2][13 + a]")
  ~expect:
    (Some (Parsetree.Exp_index
       { Location.value =
         ((Parsetree.Exp_index
             { Location.value =
               ((Parsetree.Exp_binop
                   { Location.value =
                     (Parsetree.Plus,
                      (Parsetree.Exp_variable
                         { Location.value = "a";
                           loc;
                           }),
                      (Parsetree.Exp_number
                         { Location.value = (Parsetree.Int 10);
                           loc;
                           }));
                     loc;
                     }),
                (Parsetree.Exp_binop
                   { Location.value =
                     (Parsetree.Plus,
                      (Parsetree.Exp_number
                         { Location.value = (Parsetree.Int 10);
                           loc;
                           }),
                      (Parsetree.Exp_number
                         { Location.value = (Parsetree.Int 2);
                           loc;
                           }));
                     loc;
                     }));
               loc;
               }),
          (Parsetree.Exp_binop
             { Location.value =
               (Parsetree.Plus,
                (Parsetree.Exp_number
                   { Location.value = (Parsetree.Int 13);
                     loc;
                     }),
                (Parsetree.Exp_variable
                   { Location.value = "a";
                     loc;
                     }));
               loc;
               }));
         loc;
         }))

let%expect_test "parse_array" =
  [%test_result: Parsetree.expr option]
  (print_expr "[1; [1;a[10]]]")
  ~expect:
    (Some (Parsetree.Exp_vector
       { Location.value =
         [(Parsetree.Exp_number
             { Location.value = (Parsetree.Int 1);
               loc;
               });
           (Parsetree.Exp_vector
              { Location.value =
                [(Parsetree.Exp_number
                    { Location.value = (Parsetree.Int 1);
                      loc;
                      });
                  (Parsetree.Exp_index
                     { Location.value =
                       ((Parsetree.Exp_variable
                           { Location.value = "a";
                             loc;
                             }),
                        (Parsetree.Exp_number
                           { Location.value = (Parsetree.Int 10);
                             loc;
                             }));
                       loc;
                       })
                  ];
                loc;
                })
           ];
         loc;
         }))

let%test_unit "parse_list" =
  [%test_result: Parsetree.expr option]
  (print_expr "[1, [1, [a; 1][10]]]")
~expect:(Some (Parsetree.Exp_list
     { Location.value =
       [(Parsetree.Exp_number
           { Location.value = (Parsetree.Int 1);
             loc;
             });
         (Parsetree.Exp_list
            { Location.value =
              [(Parsetree.Exp_number
                  { Location.value = (Parsetree.Int 1);
                    loc;
                    });
                (Parsetree.Exp_index
                   { Location.value =
                     ((Parsetree.Exp_vector
                         { Location.value =
                           [(Parsetree.Exp_variable
                               { Location.value = "a";
                                 loc;
                                 });
                             (Parsetree.Exp_number
                                { Location.value = (Parsetree.Int 1);
                                  loc;
                                  })
                             ];
                           loc;
                           }),
                      (Parsetree.Exp_number
                         { Location.value = (Parsetree.Int 10);
                           loc;
                           }));
                     loc;
                     })
                ];
              loc;
              })
         ];
       loc;
       }))

let%test_unit "parse_list_comprehension" =
  [%test_result: Parsetree.expr option]
  (print_expr "[ i * 2 for i in [1; 2; 3]]")
  ~expect:(Some (Parsetree.Exp_list_comprehension
     { Location.value =
       ((Parsetree.Exp_binop
           { Location.value =
             (Parsetree.Mul,
              (Parsetree.Exp_variable
                 { Location.value = "i";
                   loc;
                   }),
              (Parsetree.Exp_number
                 { Location.value = (Parsetree.Int 2);
                   loc;
                   }));
             loc;
             }),
        {value="i"; loc},
        (Parsetree.Exp_vector
           { Location.value =
             [(Parsetree.Exp_number
                 { Location.value = (Parsetree.Int 1);
                   loc;
                   });
               (Parsetree.Exp_number
                  { Location.value = (Parsetree.Int 2);
                    loc;
                    });
               (Parsetree.Exp_number
                  { Location.value = (Parsetree.Int 3);
                    loc;
                    })
               ];
             loc;
             }),
        None);
       loc;
       }))

let%test_unit "parse_list_comprehension" =
  [%test_result: Parsetree.expr option]
  (print_expr "[ i * 2 for i in 1 to 100]")
  ~expect:(Some (Parsetree.Exp_list_comprehension
       { Location.value =
         ((Parsetree.Exp_binop
             { Location.value =
               (Parsetree.Mul,
                (Parsetree.Exp_variable
                   { Location.value = "i";
                     loc;
                     }),
                (Parsetree.Exp_number
                   { Location.value = (Parsetree.Int 2);
                     loc;
                     }));
               loc;
               }),
          {loc; value="i"},
          (Parsetree.Exp_number
             { Location.value = (Parsetree.Int 1);
               loc;
               }),
          (Some (Parsetree.Exp_number
                   { Location.value = (Parsetree.Int 100);
                     loc;
                     })));
         loc;
         }))

let%test_unit "parse_list_comprehension" =
  [%test_result: Parsetree.expr option]
  (print_expr "sin(0.3) + mix([1;2;3], [1;2;3])")
  ~expect:(Some (Parsetree.Exp_binop
       { Location.value =
         (Parsetree.Plus,
          (Parsetree.Exp_function_call
             { Location.value =
               ({value="sin"; loc},
                [(None,
                  (Parsetree.Exp_number
                     { Location.value = (Parsetree.Float 0.3);
                       loc;
                       }))
                  ]);
               loc;
               }),
          (Parsetree.Exp_function_call
             { Location.value =
               ({value="mix"; loc},
                [(None,
                  (Parsetree.Exp_vector
                     { Location.value =
                       [(Parsetree.Exp_number
                           { Location.value = (Parsetree.Int 1);
                             loc;
                             });
                         (Parsetree.Exp_number
                            { Location.value = (Parsetree.Int 2);
                              loc;
                              });
                         (Parsetree.Exp_number
                            { Location.value = (Parsetree.Int 3);
                              loc;
                              })
                         ];
                       loc;
                       }));
                  (None,
                   (Parsetree.Exp_vector
                      { Location.value =
                        [(Parsetree.Exp_number
                            { Location.value = (Parsetree.Int 1);
                              loc;
                              });
                          (Parsetree.Exp_number
                             { Location.value = (Parsetree.Int 2);
                               loc;
                               });
                          (Parsetree.Exp_number
                             { Location.value = (Parsetree.Int 3);
                               loc;
                               })
                          ];
                        loc;
                        }))
                  ]);
               loc;
               }));
         loc;
         }))

let%test_unit "parse_list_comprehension_named_args" =
  [%test_result: Parsetree.expr option]
  (print_expr "sin(0.3) + get_screen_width(x=[1;2;3], y=[1;2;3])")
  ~expect:(Some (Parsetree.Exp_binop
       { Location.value =
         (Parsetree.Plus,
          (Parsetree.Exp_function_call
             { Location.value =
               ({value="sin"; loc},
                [(None,
                  (Parsetree.Exp_number
                     { Location.value = (Parsetree.Float 0.3);
                       loc;
                       }))
                  ]);
               loc;
               }),
          (Parsetree.Exp_function_call
             { Location.value =
               ({value="get_screen_width"; loc},
                [((Some {value="x"; loc}),
                  (Parsetree.Exp_vector
                     { Location.value =
                       [(Parsetree.Exp_number
                           { Location.value = (Parsetree.Int 1);
                             loc;
                             });
                         (Parsetree.Exp_number
                            { Location.value = (Parsetree.Int 2);
                              loc;
                              });
                         (Parsetree.Exp_number
                            { Location.value = (Parsetree.Int 3);
                              loc;
                              })
                         ];
                       loc;
                       }));
                  ((Some {value="y"; loc}),
                   (Parsetree.Exp_vector
                      { Location.value =
                        [(Parsetree.Exp_number
                            { Location.value = (Parsetree.Int 1);
                              loc;
                              });
                          (Parsetree.Exp_number
                             { Location.value = (Parsetree.Int 2);
                               loc;
                               });
                          (Parsetree.Exp_number
                             { Location.value = (Parsetree.Int 3);
                               loc;
                               })
                          ];
                        loc;
                        }))
                  ]);
               loc;
               }));
         loc;
         }))

let%test_unit "parse_if_then_else" =
  [%test_result: Parsetree.expr option]
  (print_expr "if true && false then true && if false then true else false else false")
  ~expect:(Some (Parsetree.Exp_if_then_else
       { Location.value =
         ((Parsetree.Exp_binop
             { Location.value =
               (Parsetree.And,
                (Parsetree.Exp_bool
                   { Location.value = true;
                     loc;
                     }),
                (Parsetree.Exp_bool
                   { Location.value = false;
                     loc;
                     }));
               loc;
               }),
          (Parsetree.Exp_binop
             { Location.value =
               (Parsetree.And,
                (Parsetree.Exp_bool
                   { Location.value = true;
                     loc;
                     }),
                (Parsetree.Exp_if_then_else
                   { Location.value =
                     ((Parsetree.Exp_bool
                         { Location.value = false;
                           loc;
                           }),
                      (Parsetree.Exp_bool
                         { Location.value = true;
                           loc;
                           }),
                      (Parsetree.Exp_bool
                         { Location.value = false;
                           loc;
                           }));
                     loc;
                     }));
               loc;
               }),
          (Parsetree.Exp_bool
             { Location.value = false;
               loc;
               }));
         loc;
         }))


let%test_unit "parse_int_expr" =
  [%test_result: Parsetree.type_expr option]
  (print_type_expr "int")
  ~expect:(Some (Parsetree.TypeInt loc))

let%test_unit "parse_bool_expr" =
  [%test_result: Parsetree.type_expr option]
  (print_type_expr "bool")
  ~expect:(Some (Parsetree.TypeBool
       loc))

let%test_unit "parse_float_expr" =
  [%test_result: Parsetree.type_expr option]
  (print_type_expr "float")
  ~expect:(Some (Parsetree.TypeFloat
       loc))

let%test_unit "parse_texture1d_expr" =
  [%test_result: Parsetree.type_expr option]
  (print_type_expr "texture1D")
  ~expect:(Some (Parsetree.TypeTexture1D
       loc))

let%test_unit "parse_texture2d_expr" =
  [%test_result: Parsetree.type_expr option]
  (print_type_expr "texture2D")
  ~expect:    (Some (Parsetree.TypeTexture2D
       loc))


let%test_unit "parse_texture3d_expr" =
  [%test_result: Parsetree.type_expr option]
  (print_type_expr "texture3D")
  ~expect:    (Some (Parsetree.TypeTexture3D
       loc))


let%test_unit "parse_texture_cube_expr" =
  [%test_result: Parsetree.type_expr option]
  (print_type_expr "texture_cube")
  ~expect:    (Some (Parsetree.TypeTextureCube
       loc))


let%test_unit "parse_numeric_data" =
  [%test_result: Parsetree.type_expr option]
  (print_type_expr "numeric_data")
  ~expect:    (Some (Parsetree.TypeNumericData
       loc))


let%test_unit "parse_type_hole" =
  [%test_result: Parsetree.type_expr option]
  (print_type_expr "_")
  ~expect:    (Some (Parsetree.TypeHole
       loc))


let%test_unit "parse_type_variable" =
  [%test_result: Parsetree.type_expr option]
  (print_type_expr "'alpha")
  ~expect:    (Some (Parsetree.TypeVariable
       { Location.value = "'alpha";
         loc;
         }))


let%test_unit "parse_type_map" =
  [%test_result: Parsetree.type_expr option]
  (print_type_expr "< x = int; y = int >")
  ~expect:    (Some (Parsetree.TypeMap
       { Location.value =
         [{ Location.value =
            ({value="x"; loc},
             (Parsetree.TypeInt
                loc));
            loc;
            };
           { Location.value =
             ({value="y"; loc},
              (Parsetree.TypeInt
                 loc));
             loc;
             }
           ];
         loc;
         }))


let%test_unit "type_product" =
  [%test_result: Parsetree.type_expr option]
  (print_type_expr "int list list list")
  ~expect:    (Some (Parsetree.TypeProduct
       { Location.value =
         ([(Parsetree.TypeProduct
              { Location.value =
                ([(Parsetree.TypeProduct
                     { Location.value =
                       ([(Parsetree.TypeInt
                            loc)
                          ],
                        (Parsetree.TypeList
                           loc));
                       loc;
                       })
                   ],
                 (Parsetree.TypeList
                    loc));
                loc;
                })
            ],
          (Parsetree.TypeList
             loc));
         loc;
         }))


let%test_unit "type_product_pair" =
  [%test_result: Parsetree.type_expr option]
  (print_type_expr "(int, numeric_data, float, bool) vec3")
  ~expect:    (Some (Parsetree.TypeProduct
       { Location.value =
         ([(Parsetree.TypeInt
              loc);
            (Parsetree.TypeNumericData
               loc);
            (Parsetree.TypeFloat
               loc);
            (Parsetree.TypeBool
               loc)
            ],
          (Parsetree.TypeVec3
             loc));
         loc;
         }))




let%test_unit "parse_expr_with_type" =
  [%test_result: Parsetree.expr option]
  (print_expr "(1 + a + [a for a in 1 to 10] : int)")
  ~expect:    (Some (Parsetree.Exp_with_type
       { Location.value =
         ((Parsetree.Exp_binop
             { Location.value =
               (Parsetree.Plus,
                (Parsetree.Exp_binop
                   { Location.value =
                     (Parsetree.Plus,
                      (Parsetree.Exp_number
                         { Location.value = (Parsetree.Int 1);
                           loc;
                           }),
                      (Parsetree.Exp_variable
                         { Location.value = "a";
                           loc;
                           }));
                     loc;
                     }),
                (Parsetree.Exp_list_comprehension
                   { Location.value =
                     ((Parsetree.Exp_variable
                         { Location.value = "a";
                           loc;
                           }),
                      {value="a"; loc},
                      (Parsetree.Exp_number
                         { Location.value = (Parsetree.Int 1);
                           loc;
                           }),
                      (Some (Parsetree.Exp_number
                               { Location.value = (Parsetree.Int 10);
                                 loc;
                                 })));
                     loc;
                     }));
               loc;
               }),
          (Parsetree.TypeInt
             loc));
         loc;
         }))

let%test_unit "parse_function_declaration" =
  [%test_result: Parsetree.declaration option]
  (print_decl "function divby (x : float, y : float = 1) : float begin x / y end")
  ~expect:(Some Parsetree.(DataFunction {
      identifier={value="divby"; loc};
      arguments=[
        {value="x"; loc}, Some (TypeFloat loc), None;
        {value="y"; loc}, Some (TypeFloat loc), Some (Exp_number {value=(Int 1); loc});
      ];
      typ=Some(TypeFloat loc);
      body=Exp_binop {value=(Div, Exp_variable {value="x"; loc}, Exp_variable {value="y"; loc}); loc};
    }
    ))

let%test_unit "parse_variable_declaration" =
  [%test_result: Parsetree.declaration option]
  (print_decl "var divide_result : float = divby(10, y=20);")
  ~expect:(Some Parsetree.(DataVariable {
      identifier={value="divide_result"; loc};
      typ=Some(TypeFloat loc);
      body=Exp_function_call
          {value=({value="divby";loc}, [
               None, Exp_number {value=(Int 10); loc};
               Some {value="y"; loc}, Exp_number {value=(Int 20); loc};
             ]); loc}
    }
    ))

let%test_unit "parse_data_model" =
  [%test_result: Parsetree.data option]
  (print_data "model(\"/Document/models/learning.stl\", material=\"/Documents/models/learning.stl\", width=10, a)")
  ~expect:(Some Parsetree.(DataComponent {value=Model,[
      None, ArgumentString {value="/Document/models/learning.stl";loc};
      Some {value="material"; loc}, ArgumentString {value="/Documents/models/learning.stl";loc};
      Some {value="width"; loc}, ArgumentExpression (Exp_number {value=(Int 10); loc});
      None, ArgumentExpression (Exp_variable {value="a"; loc})
    ];loc}))

let%test_unit "parse_modifier_texture" =
  [%test_result: Parsetree.modifier option]
    (print_modifier
       "texture(\"/Document/models/learning.stl\", material=\"/Documents/models/learning.stl\", width=10, a)")
  ~expect:(Some Parsetree.(BasicModifier {value=Texture,[
      None, ArgumentString {value="/Document/models/learning.stl";loc};
      Some {value="material"; loc}, ArgumentString {value="/Documents/models/learning.stl";loc};
      Some {value="width"; loc}, ArgumentExpression (Exp_number {value=(Int 10); loc});
      None, ArgumentExpression (Exp_variable {value="a"; loc})
    ];loc}))

let%test_unit "parse_modifier_uniform" =
  [%test_result: Parsetree.modifier option]
    (print_modifier
       "uniform(\"/Document/models/learning.stl\", material=\"/Documents/models/learning.stl\", width=10, a)")
  ~expect:(Some Parsetree.(BasicModifier {value=Uniform,[
      None, ArgumentString {value="/Document/models/learning.stl";loc};
      Some {value="material"; loc}, ArgumentString {value="/Documents/models/learning.stl";loc};
      Some {value="width"; loc}, ArgumentExpression (Exp_number {value=(Int 10); loc});
      None, ArgumentExpression (Exp_variable {value="a"; loc})
    ];loc}))

let%test_unit "parse_modifier_attribute" =
  [%test_result: Parsetree.modifier option]
    (print_modifier
       "attribute(\"/Document/models/learning.stl\", material=\"/Documents/models/learning.stl\", width=10, a)")
  ~expect:(Some Parsetree.(BasicModifier {value=Attribute,[
      None, ArgumentString {value="/Document/models/learning.stl";loc};
      Some {value="material"; loc}, ArgumentString {value="/Documents/models/learning.stl";loc};
      Some {value="width"; loc}, ArgumentExpression (Exp_number {value=(Int 10); loc});
      None, ArgumentExpression (Exp_variable {value="a"; loc})
    ];loc}))

let%test_unit "parse_modifier_translate" =
  [%test_result: Parsetree.modifier option]
    (print_modifier
       "translate(\"/Document/models/learning.stl\", material=\"/Documents/models/learning.stl\", width=10, a)")
  ~expect:(Some Parsetree.(BasicModifier {value=Translate,[
      None, ArgumentString {value="/Document/models/learning.stl";loc};
      Some {value="material"; loc}, ArgumentString {value="/Documents/models/learning.stl";loc};
      Some {value="width"; loc}, ArgumentExpression (Exp_number {value=(Int 10); loc});
      None, ArgumentExpression (Exp_variable {value="a"; loc})
    ];loc}))

let%test_unit "parse_modifier_scale" =
  [%test_result: Parsetree.modifier option]
    (print_modifier
       "scale(\"/Document/models/learning.stl\", material=\"/Documents/models/learning.stl\", width=10, a)")
  ~expect:(Some Parsetree.(BasicModifier {value=Scale,[
      None, ArgumentString {value="/Document/models/learning.stl";loc};
      Some {value="material"; loc}, ArgumentString {value="/Documents/models/learning.stl";loc};
      Some {value="width"; loc}, ArgumentExpression (Exp_number {value=(Int 10); loc});
      None, ArgumentExpression (Exp_variable {value="a"; loc})
    ];loc}))

let%test_unit "parse_modifier_rotate" =
  [%test_result: Parsetree.modifier option]
    (print_modifier
       "rotate(\"/Document/models/learning.stl\", material=\"/Documents/models/learning.stl\", width=10, a)")
  ~expect:(Some Parsetree.(BasicModifier {value=Rotate,[
      None, ArgumentString {value="/Document/models/learning.stl";loc};
      Some {value="material"; loc}, ArgumentString {value="/Documents/models/learning.stl";loc};
      Some {value="width"; loc}, ArgumentExpression (Exp_number {value=(Int 10); loc});
      None, ArgumentExpression (Exp_variable {value="a"; loc})
    ];loc}))

let%test_unit "parse_object" =
  [%test_result: Parsetree.obj option]
    (print_object
       "rotate(\"/Document/models/learning.stl\", material=\"/Documents/models/learning.stl\", width=10, a)
       texture(\"/Document/models/learning.stl\", material=\"/Documents/models/learning.stl\", width=10, a)
       scale(\"/Document/models/learning.stl\", material=\"/Documents/models/learning.stl\", width=10, a)
       model(\"/Document/models/learning.stl\", material=\"/Documents/models/learning.stl\", width=10, a)")
    ~expect:
      (Some
         Parsetree.(Object
                      {value=([
                        BasicModifier {value=Rotate,[
                            None, ArgumentString {value="/Document/models/learning.stl";loc};
                            Some {value="material"; loc}, ArgumentString {value="/Documents/models/learning.stl";loc};
                            Some {value="width"; loc}, ArgumentExpression (Exp_number {value=(Int 10); loc});
                            None, ArgumentExpression (Exp_variable {value="a"; loc})
                          ];loc};
                        BasicModifier {value=Texture,[
                            None, ArgumentString {value="/Document/models/learning.stl";loc};
                            Some {value="material"; loc}, ArgumentString {value="/Documents/models/learning.stl";loc};
                            Some {value="width"; loc}, ArgumentExpression (Exp_number {value=(Int 10); loc});
                            None, ArgumentExpression (Exp_variable {value="a"; loc})
                          ];loc};
                        BasicModifier {value=Scale,[
                            None, ArgumentString {value="/Document/models/learning.stl";loc};
                            Some {value="material"; loc}, ArgumentString {value="/Documents/models/learning.stl";loc};
                            Some {value="width"; loc}, ArgumentExpression (Exp_number {value=(Int 10); loc});
                            None, ArgumentExpression (Exp_variable {value="a"; loc})
                          ];loc}
                      ], 
                    ObjectData (DataComponent {value=Model,[
                         None, ArgumentString {value="/Document/models/learning.stl";loc};
                         Some {value="material"; loc}, ArgumentString {value="/Documents/models/learning.stl";loc};
                         Some {value="width"; loc}, ArgumentExpression (Exp_number {value=(Int 10); loc});
                         None, ArgumentExpression (Exp_variable {value="a"; loc})
                       ];loc})); loc}
                   ))

let%test_unit "parse_program_empty" =
  [%test_result: Parsetree.statement list]
    (print_program "")
    ~expect:[]

let%test_unit "parse_program_variable" =
  [%test_result: Parsetree.statement list]
    (print_program {|
var x = 10;
|})
    ~expect:Parsetree.[
        Declaration (DataVariable { identifier={value="x"; loc;};
                                   typ=None; body=Exp_number {value=(Int 10); loc}
                                  })
    ]

let%test_unit "parse_program_drawable" =
  [%test_result: Parsetree.statement list]
    (print_program {|
with shader() light();
|})
    ~expect:Parsetree.[
        Drawable (DrawableShader {value=(BasicShader {value=[]; loc}, Object {value=([], Light {value=[]; loc});loc});loc})
    ]

let%test_unit "parse_program_example_1" =
  [%test_result: Parsetree.statement list]
    (print_program {|
with shader(fragment_shader="/Documents/frag.vert") light();
|})
    ~expect:Parsetree.[
        Drawable (DrawableShader {value=(BasicShader {value=[(Some {value="fragment_shader";loc}, (SceneString {value="/Documents/frag.vert"; loc}))]; loc}, Object {value=([], Light {value=[]; loc});loc});loc})
    ]


let%test_unit "parse_program_example_2" =
  [%test_result: Parsetree.statement list]
    (print_program {|
with shader(
         vertex_shader="/shaders/vert.glsl", 
         fragment_shader="/shaders/frag.glsl") 
    redirecting stencil
        with shader(
             vertex_shader="/shaders/basic_vert.glsl", 
             fragment_shader="/shaders/basic_frag.glsl") 
               translate(0.3, 0.5, 0.8)
               begin
                     scale(0.5,0.5,0.5)
                            model("/models/cube.obj");
               end
         begin
               scale(0.5,0.5,0.5)
                      model("/models/cube.obj");
         end;
|})
    ~expect:[(Parsetree.Drawable
   (Parsetree.DrawableShader
      { Location.value =
        ((Parsetree.BasicShader
            { Location.value =
              [((Some { Location.value = "vertex_shader";
                        loc;
                        }),
                (Parsetree.SceneString
                   { Location.value = "/shaders/vert.glsl";
                     loc;
                     }));
                ((Some { Location.value = "fragment_shader";
                         loc;
                         }),
                 (Parsetree.SceneString
                    { Location.value = "/shaders/frag.glsl";
                      loc;
                      }))
                ];
              loc;
              }),
         (Parsetree.Object
            { Location.value =
              ([(Parsetree.RedirectingModifier
                   { Location.value =
                     ([(None,
                        (Parsetree.Exp_variable
                           { Location.value = "stencil";
                             loc;
                             }))
                        ],
                      (Parsetree.DrawableShader
                         { Location.value =
                           ((Parsetree.BasicShader
                               { Location.value =
                                 [((Some { Location.value = "vertex_shader";
                                           loc;
                                           }),
                                   (Parsetree.SceneString
                                      { Location.value =
                                        "/shaders/basic_vert.glsl";
                                        loc;
                                        }));
                                   ((Some { Location.value =
                                            "fragment_shader";
                                            loc;
                                            }),
                                    (Parsetree.SceneString
                                       { Location.value =
                                         "/shaders/basic_frag.glsl";
                                         loc;
                                         }))
                                   ];
                                 loc;
                                 }),
                            (Parsetree.Object
                               { Location.value =
                                 ([(Parsetree.BasicModifier
                                      { Location.value =
                                        (Parsetree.Translate,
                                         [(None,
                                           (Parsetree.ArgumentExpression
                                              (Parsetree.Exp_number
                                                 { Location.value =
                                                   (Parsetree.Float 0.3);
                                                   loc;
                                                   })));
                                           (None,
                                            (Parsetree.ArgumentExpression
                                               (Parsetree.Exp_number
                                                  { Location.value =
                                                    (Parsetree.Float 0.5);
                                                    loc;
                                                    })));
                                           (None,
                                            (Parsetree.ArgumentExpression
                                               (Parsetree.Exp_number
                                                  { Location.value =
                                                    (Parsetree.Float 0.8);
                                                    loc;
                                                    })))
                                           ]);
                                        loc;
                                        })
                                    ],
                                  (Parsetree.CompoundObject
                                     { Location.value =
                                       [(Parsetree.SceneExpr_object
                                           (Parsetree.Object
                                              { Location.value =
                                                ([(Parsetree.BasicModifier
                                                     { Location.value =
                                                       (Parsetree.Scale,
                                                        [(None,
                                                          (Parsetree.ArgumentExpression
                                                             (Parsetree.Exp_number
                                                                { Location.value =
                                                                  (Parsetree.Float
                                                                    0.5);
                                                                  loc;
                                                                  })));
                                                          (None,
                                                           (Parsetree.ArgumentExpression
                                                              (Parsetree.Exp_number
                                                                 { Location.value =
                                                                   (Parsetree.Float
                                                                    0.5);
                                                                   loc;
                                                                   })));
                                                          (None,
                                                           (Parsetree.ArgumentExpression
                                                              (Parsetree.Exp_number
                                                                 { Location.value =
                                                                   (Parsetree.Float
                                                                    0.5);
                                                                   loc;
                                                                   })))
                                                          ]);
                                                       loc;
                                                       })
                                                   ],
                                                 (Parsetree.ObjectData
                                                    (Parsetree.DataComponent
                                                       { Location.value =
                                                         (Parsetree.Model,
                                                          [(None,
                                                            (Parsetree.ArgumentString
                                                               { Location.value =
                                                                 "/models/cube.obj";
                                                                 loc;
                                                                 }))
                                                            ]);
                                                         loc;
                                                         })));
                                                loc;
                                                }))
                                         ];
                                       loc;
                                       }));
                                 loc;
                                 }));
                           loc;
                           }));
                     loc;
                     })
                 ],
               (Parsetree.CompoundObject
                  { Location.value =
                    [(Parsetree.SceneExpr_object
                        (Parsetree.Object
                           { Location.value =
                             ([(Parsetree.BasicModifier
                                  { Location.value =
                                    (Parsetree.Scale,
                                     [(None,
                                       (Parsetree.ArgumentExpression
                                          (Parsetree.Exp_number
                                             { Location.value =
                                               (Parsetree.Float 0.5);
                                               loc;
                                               })));
                                       (None,
                                        (Parsetree.ArgumentExpression
                                           (Parsetree.Exp_number
                                              { Location.value =
                                                (Parsetree.Float 0.5);
                                                loc;
                                                })));
                                       (None,
                                        (Parsetree.ArgumentExpression
                                           (Parsetree.Exp_number
                                              { Location.value =
                                                (Parsetree.Float 0.5);
                                                loc;
                                                })))
                                       ]);
                                    loc;
                                    })
                                ],
                              (Parsetree.ObjectData
                                 (Parsetree.DataComponent
                                    { Location.value =
                                      (Parsetree.Model,
                                       [(None,
                                         (Parsetree.ArgumentString
                                            { Location.value =
                                              "/models/cube.obj";
                                              loc;
                                              }))
                                         ]);
                                      loc;
                                      })));
                             loc;
                             }))
                      ];
                    loc;
                    }));
              loc;
              }));
        loc;
        }))]


let%test_unit "parse_program_example_3" =
  [%test_result: Parsetree.statement list]
    (print_program {|
overlaying with shader(vertex_shader="/shaders/draw_vert.glsl", fragment_shader="/shaders/draw_frag.glsl")
     redirecting textures=[position, normals, colours]; depth=d; normals
         with shader(
              vertex_shader="/shaders/def_vert.glsl", 
              fragment_shader="/shaders/def_frag.glsl") begin
                scale(0.5,0.5,0.5)
                   texture(model_texture, "/Pictures/model_texture.png")
                       model("/models/cube.obj");

                scale(0.5,0.5,0.5)
                       light(point_light);

                scale(0.5,0.5,0.5)
                       light(spot_light);
         end
     light_data(point_light, light_volume);
|})
    ~expect:[(Parsetree.Drawable
   (Parsetree.OverlayingDrawable
      { Location.value =
        (Parsetree.DrawableShader
           { Location.value =
             ((Parsetree.BasicShader
                 { Location.value =
                   [((Some { Location.value = "vertex_shader";
                             loc; }),
                     (Parsetree.SceneString
                        { Location.value = "/shaders/draw_vert.glsl";
                          loc                          
                          }));
                     ((Some { Location.value = "fragment_shader";
                              loc                              
                              }),
                      (Parsetree.SceneString
                         { Location.value = "/shaders/draw_frag.glsl";
                           loc                           
                           }))
                     ];
                   loc                   
                   }),
              (Parsetree.Object
                 { Location.value =
                   ([(Parsetree.RedirectingModifier
                        { Location.value =
                          ([((Some { Location.value = "textures";
                                     loc                                     
                                     }),
                             (Parsetree.Exp_list
                                { Location.value =
                                  [(Parsetree.Exp_variable
                                      { Location.value = "position";
                                        loc                                        
                                        });
                                    (Parsetree.Exp_variable
                                       { Location.value = "normals";
                                         loc                                         
                                         });
                                    (Parsetree.Exp_variable
                                       { Location.value = "colours";
                                         loc                                         
                                         })
                                    ];
                                  loc                                  
                                  }));
                             ((Some { Location.value = "depth";
                                      loc                                      
                                      }),
                              (Parsetree.Exp_variable
                                 { Location.value = "d";
                                   loc                                   
                                   }));
                             (None,
                              (Parsetree.Exp_variable
                                 { Location.value = "normals";
                                   loc                                   
                                   }))
                             ],
                           (Parsetree.DrawableShader
                              { Location.value =
                                ((Parsetree.BasicShader
                                    { Location.value =
                                      [((Some { Location.value =
                                                "vertex_shader";
                                                loc                                                
                                                }),
                                        (Parsetree.SceneString
                                           { Location.value =
                                             "/shaders/def_vert.glsl";
                                             loc                                             
                                             }));
                                        ((Some { Location.value =
                                                 "fragment_shader";
                                                 loc                                                 
                                                 }),
                                         (Parsetree.SceneString
                                            { Location.value =
                                              "/shaders/def_frag.glsl";
                                              loc                                              
                                              }))
                                        ];
                                      loc                                      
                                      }),
                                 (Parsetree.Object
                                    { Location.value =
                                      ([],
                                       (Parsetree.CompoundObject
                                          { Location.value =
                                            [(Parsetree.SceneExpr_object
                                                (Parsetree.Object
                                                   { Location.value =
                                                     ([(Parsetree.BasicModifier
                                                          { Location.value =
                                                            (Parsetree.Scale,
                                                             [(None,
                                                               (Parsetree.ArgumentExpression
                                                                  (Parsetree.Exp_number
                                                                    { Location.value =
                                                                    (Parsetree.Float
                                                                    0.5);
                                                                    loc                                                                     })));
                                                               (None,
                                                                (Parsetree.ArgumentExpression
                                                                   (Parsetree.Exp_number
                                                                    { Location.value =
                                                                    (Parsetree.Float
                                                                    0.5);
                                                                    loc                                                                     })));
                                                               (None,
                                                                (Parsetree.ArgumentExpression
                                                                   (Parsetree.Exp_number
                                                                    { Location.value =
                                                                    (Parsetree.Float
                                                                    0.5);
                                                                    loc                                                                     })))
                                                               ]);
                                                            loc                                                            
                                                            });
                                                        (Parsetree.BasicModifier
                                                           { Location.value =
                                                             (Parsetree.Texture,
                                                              [(None,
                                                                (Parsetree.ArgumentExpression
                                                                   (Parsetree.Exp_variable
                                                                    { Location.value =
                                                                    "model_texture";
                                                                    loc                                                                     })));
                                                                (None,
                                                                 (Parsetree.ArgumentString
                                                                    { Location.value =
                                                                    "/Pictures/model_texture.png";
                                                                    loc                                                                     }))
                                                                ]);
                                                             loc                                                             
                                                             })
                                                        ],
                                                      (Parsetree.ObjectData
                                                         (Parsetree.DataComponent
                                                            { Location.value =
                                                              (Parsetree.Model,
                                                               [(None,
                                                                 (Parsetree.ArgumentString
                                                                    { Location.value =
                                                                    "/models/cube.obj";
                                                                    loc                                                                     }))
                                                                 ]);
                                                              loc                                                              
                                                              })));
                                                     loc                                                     
                                                     }));
                                              (Parsetree.SceneExpr_object
                                                 (Parsetree.Object
                                                    { Location.value =
                                                      ([(Parsetree.BasicModifier
                                                           { Location.value =
                                                             (Parsetree.Scale,
                                                              [(None,
                                                                (Parsetree.ArgumentExpression
                                                                   (Parsetree.Exp_number
                                                                    { Location.value =
                                                                    (Parsetree.Float
                                                                    0.5);
                                                                    loc                                                                     })));
                                                                (None,
                                                                 (Parsetree.ArgumentExpression
                                                                    (
                                                                    Parsetree.Exp_number
                                                                    { Location.value =
                                                                    (Parsetree.Float
                                                                    0.5);
                                                                    loc                                                                     })));
                                                                (None,
                                                                 (Parsetree.ArgumentExpression
                                                                    (
                                                                    Parsetree.Exp_number
                                                                    { Location.value =
                                                                    (Parsetree.Float
                                                                    0.5);
                                                                    loc                                                                     })))
                                                                ]);
                                                             loc                                                             
                                                             })
                                                         ],
                                                       (Parsetree.Light
                                                          { Location.value =
                                                            [(None,
                                                              (Parsetree.ArgumentExpression
                                                                 (Parsetree.Exp_variable
                                                                    { Location.value =
                                                                    "point_light";
                                                                    loc                                                                     })))
                                                              ];
                                                            loc                                                            
                                                            }));
                                                      loc                                                      
                                                      }));
                                              (Parsetree.SceneExpr_object
                                                 (Parsetree.Object
                                                    { Location.value =
                                                      ([(Parsetree.BasicModifier
                                                           { Location.value =
                                                             (Parsetree.Scale,
                                                              [(None,
                                                                (Parsetree.ArgumentExpression
                                                                   (Parsetree.Exp_number
                                                                    { Location.value =
                                                                    (Parsetree.Float
                                                                    0.5);
                                                                    loc                                                                     })));
                                                                (None,
                                                                 (Parsetree.ArgumentExpression
                                                                    (
                                                                    Parsetree.Exp_number
                                                                    { Location.value =
                                                                    (Parsetree.Float
                                                                    0.5);
                                                                    loc                                                                     })));
                                                                (None,
                                                                 (Parsetree.ArgumentExpression
                                                                    (
                                                                    Parsetree.Exp_number
                                                                    { Location.value =
                                                                    (Parsetree.Float
                                                                    0.5);
                                                                    loc                                                                     })))
                                                                ]);
                                                             loc                                                             
                                                             })
                                                         ],
                                                       (Parsetree.Light
                                                          { Location.value =
                                                            [(None,
                                                              (Parsetree.ArgumentExpression
                                                                 (Parsetree.Exp_variable
                                                                    { Location.value =
                                                                    "spot_light";
                                                                    loc                                                                     })))
                                                              ];
                                                            loc                                                            
                                                            }));
                                                      loc                                                      
                                                      }))
                                              ];
                                            loc                                            
                                            }));
                                      loc                                      
                                      }));
                                loc                                
                                }));
                          loc                          
                          })
                      ],
                    (Parsetree.LightData
                       { Location.value =
                         [(Parsetree.Exp_variable
                             { Location.value = "point_light";
                               loc                               
                               });
                           (Parsetree.Exp_variable
                              { Location.value = "light_volume";
                                loc                                
                                })
                           ];
                         loc                         
                         }));
                   loc                   
                   }));
             loc             
             });
        loc        
        }))]



let%test_unit "parse_program_example_4" =
  [%test_result: Parsetree.statement list]
    (print_program {|
overlaying with shader(
    vertex_shader="/shaders/shadow_map.glsl",
    fragment_shader="/shaders/shadow_map.glsl"
)
     redirecting textures=[shadow_map]
         with shader(vertex_shader="/shaders/basic_vert.glsl")
           light_uniform(light_position, spot_light)
           begin
                scale(0.5,0.5,0.5)
                   texture(model_texture, "/Pictures/model_texture.png")
                       model("/models/cube.obj");

                scale(0.5,0.5,0.5)
                       light(point_light);

                scale(0.5,0.5,0.5)
                       light(spot_light);
           end
     screen();
|})
       ~expect:[(Parsetree.Drawable
   (Parsetree.OverlayingDrawable
      { Location.value =
        (Parsetree.DrawableShader
           { Location.value =
             ((Parsetree.BasicShader
                 { Location.value =
                   [((Some { Location.value = "vertex_shader";
                             loc                             
                             }),
                     (Parsetree.SceneString
                        { Location.value = "/shaders/shadow_map.glsl";
                          loc                          
                          }));
                     ((Some { Location.value = "fragment_shader";
                              loc                              
                              }),
                      (Parsetree.SceneString
                         { Location.value = "/shaders/shadow_map.glsl";
                           loc                           
                           }))
                     ];
                   loc                   
                   }),
              (Parsetree.Object
                 { Location.value =
                   ([(Parsetree.RedirectingModifier
                        { Location.value =
                          ([((Some { Location.value = "textures";
                                     loc                                     
                                     }),
                             (Parsetree.Exp_list
                                { Location.value =
                                  [(Parsetree.Exp_variable
                                      { Location.value = "shadow_map";
                                        loc                                        
                                        })
                                    ];
                                  loc                                  
                                  }))
                             ],
                           (Parsetree.DrawableShader
                              { Location.value =
                                ((Parsetree.BasicShader
                                    { Location.value =
                                      [((Some { Location.value =
                                                "vertex_shader";
                                                loc                                                
                                                }),
                                        (Parsetree.SceneString
                                           { Location.value =
                                             "/shaders/basic_vert.glsl";
                                             loc                                             
                                             }))
                                        ];
                                      loc                                      
                                      }),
                                 (Parsetree.Object
                                    { Location.value =
                                      ([(Parsetree.LightModifier
                                           { Location.value =
                                             [(Parsetree.Exp_variable
                                                 { Location.value =
                                                   "light_position";
                                                   loc                                                   
                                                   });
                                               (Parsetree.Exp_variable
                                                  { Location.value =
                                                    "spot_light";
                                                    loc                                                    
                                                    })
                                               ];
                                             loc                                             
                                             })
                                         ],
                                       (Parsetree.CompoundObject
                                          { Location.value =
                                            [(Parsetree.SceneExpr_object
                                                (Parsetree.Object
                                                   { Location.value =
                                                     ([(Parsetree.BasicModifier
                                                          { Location.value =
                                                            (Parsetree.Scale,
                                                             [(None,
                                                               (Parsetree.ArgumentExpression
                                                                  (Parsetree.Exp_number
                                                                    { Location.value =
                                                                    (Parsetree.Float
                                                                    0.5);
                                                                    loc                                                                     })));
                                                               (None,
                                                                (Parsetree.ArgumentExpression
                                                                   (Parsetree.Exp_number
                                                                    { Location.value =
                                                                    (Parsetree.Float
                                                                    0.5);
                                                                    loc                                                                     })));
                                                               (None,
                                                                (Parsetree.ArgumentExpression
                                                                   (Parsetree.Exp_number
                                                                    { Location.value =
                                                                    (Parsetree.Float
                                                                    0.5);
                                                                    loc                                                                     })))
                                                               ]);
                                                            loc                                                            
                                                            });
                                                        (Parsetree.BasicModifier
                                                           { Location.value =
                                                             (Parsetree.Texture,
                                                              [(None,
                                                                (Parsetree.ArgumentExpression
                                                                   (Parsetree.Exp_variable
                                                                    { Location.value =
                                                                    "model_texture";
                                                                    loc                                                                     })));
                                                                (None,
                                                                 (Parsetree.ArgumentString
                                                                    { Location.value =
                                                                    "/Pictures/model_texture.png";
                                                                    loc                                                                     }))
                                                                ]);
                                                             loc                                                             
                                                             })
                                                        ],
                                                      (Parsetree.ObjectData
                                                         (Parsetree.DataComponent
                                                            { Location.value =
                                                              (Parsetree.Model,
                                                               [(None,
                                                                 (Parsetree.ArgumentString
                                                                    { Location.value =
                                                                    "/models/cube.obj";
                                                                    loc                                                                     }))
                                                                 ]);
                                                              loc                                                              
                                                              })));
                                                     loc                                                     
                                                     }));
                                              (Parsetree.SceneExpr_object
                                                 (Parsetree.Object
                                                    { Location.value =
                                                      ([(Parsetree.BasicModifier
                                                           { Location.value =
                                                             (Parsetree.Scale,
                                                              [(None,
                                                                (Parsetree.ArgumentExpression
                                                                   (Parsetree.Exp_number
                                                                    { Location.value =
                                                                    (Parsetree.Float
                                                                    0.5);
                                                                    loc                                                                     })));
                                                                (None,
                                                                 (Parsetree.ArgumentExpression
                                                                    (
                                                                    Parsetree.Exp_number
                                                                    { Location.value =
                                                                    (Parsetree.Float
                                                                    0.5);
                                                                    loc                                                                     })));
                                                                (None,
                                                                 (Parsetree.ArgumentExpression
                                                                    (
                                                                    Parsetree.Exp_number
                                                                    { Location.value =
                                                                    (Parsetree.Float
                                                                    0.5);
                                                                    loc                                                                     })))
                                                                ]);
                                                             loc                                                             
                                                             })
                                                         ],
                                                       (Parsetree.Light
                                                          { Location.value =
                                                            [(None,
                                                              (Parsetree.ArgumentExpression
                                                                 (Parsetree.Exp_variable
                                                                    { Location.value =
                                                                    "point_light";
                                                                    loc                                                                     })))
                                                              ];
                                                            loc                                                            
                                                            }));
                                                      loc                                                      
                                                      }));
                                              (Parsetree.SceneExpr_object
                                                 (Parsetree.Object
                                                    { Location.value =
                                                      ([(Parsetree.BasicModifier
                                                           { Location.value =
                                                             (Parsetree.Scale,
                                                              [(None,
                                                                (Parsetree.ArgumentExpression
                                                                   (Parsetree.Exp_number
                                                                    { Location.value =
                                                                    (Parsetree.Float
                                                                    0.5);
                                                                    loc                                                                     })));
                                                                (None,
                                                                 (Parsetree.ArgumentExpression
                                                                    (
                                                                    Parsetree.Exp_number
                                                                    { Location.value =
                                                                    (Parsetree.Float
                                                                    0.5);
                                                                    loc                                                                     })));
                                                                (None,
                                                                 (Parsetree.ArgumentExpression
                                                                    (
                                                                    Parsetree.Exp_number
                                                                    { Location.value =
                                                                    (Parsetree.Float
                                                                    0.5);
                                                                    loc                                                                     })))
                                                                ]);
                                                             loc                                                             
                                                             })
                                                         ],
                                                       (Parsetree.Light
                                                          { Location.value =
                                                            [(None,
                                                              (Parsetree.ArgumentExpression
                                                                 (Parsetree.Exp_variable
                                                                    { Location.value =
                                                                    "spot_light";
                                                                    loc                                                                     })))
                                                              ];
                                                            loc                                                            
                                                            }));
                                                      loc                                                      
                                                      }))
                                              ];
                                            loc                                            
                                            }));
                                      loc                                      
                                      }));
                                loc                                
                                }));
                          loc                          
                          })
                      ],
                    (Parsetree.ObjectData
                       (Parsetree.DataComponent
                          { Location.value = (Parsetree.Screen, []);
                            loc                            
                            })));
                   loc                   
                   }));
             loc             
             });
        loc        
        }))]
