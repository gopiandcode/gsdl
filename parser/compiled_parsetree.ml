open Gg
open Core


module OfTyped = struct

  module Map = Map.Make (String)

  type conversion_error =
      UnresolvedReference of Typed_parsetree.Substitution.variable_type Typed_parsetree.reference
    | InvalidShaderType of Typed_parsetree.Substitution.variable_type
    | InvalidTypeVariable of Typed_parsetree.TypeVariable.t
    | UnresolvedType of Typed_parsetree.Type.t
    | UnsupportedType of Typed_parsetree.Type.t
    | UnexpectedDynamicExpression of string
    | InconsistentMulti
    | EmptyCompoundObject
    | UnexpectedMulti
  [@@deriving show]

  exception ConversionException of conversion_error
  let () = Stdlib.Printexc.register_printer (function
        ConversionException err ->
        let fmt = Format.str_formatter in
        pp_conversion_error fmt err;
        let str = Format.flush_str_formatter () in
        Some str
      | _ -> None
    )

  let pp_v2 = V2.pp
  let pp_v3 = V3.pp
  let pp_v4 = V4.pp
  let pp_m2 = M2.pp
  let pp_m3 = M3.pp
  let pp_m4 = M4.pp



  type ctx = {
    camera_position: v3; camera_front: v3; camera_yaw: float; camera_pitch: float;
  }
  [@@deriving show]

  let default_context = {
    camera_position=V3.zero; camera_front=V3.zero; camera_yaw=0.0; camera_pitch=0.0;
  }


  type scalar_type =
    | TyInt
    | TyFloat
  [@@deriving show, eq]

  type data_type =
    | Unary of scalar_type | TyVec2 of scalar_type | TyVec3 of scalar_type | TyVec4 of scalar_type
    | TyMat2 of scalar_type | TyMat3 of scalar_type | TyMat4 of scalar_type
  [@@deriving show, eq]

  type texture_type = TyTexture1D | TyTexture2D | TyTexture3D | TyTextureCube
  [@@deriving show, eq]

  let rec data_of_scalar_type (sc: Typed_parsetree.Type.scalar_type) =
    match sc with
    | Typed_parsetree.Type.TyBool _ -> raise (ConversionException (UnsupportedType Typed_parsetree.Type.(TypeExpr (TyDirect (TyPure (TyScalar sc)))) ))
    | Typed_parsetree.Type.TyInt _ -> TyInt
    | Typed_parsetree.Type.TyFloat _ -> TyFloat
    | Typed_parsetree.Type.TyIntOrFloat _ -> TyFloat
  and data_of_nonscalar_type (ns: Typed_parsetree.Type.non_scalar_type) =
    match ns with
    | Typed_parsetree.Type.TyVec2 {value=TyDirect sc;_} -> TyVec2 (data_of_scalar_type sc)
    | Typed_parsetree.Type.TyVec3 {value=TyDirect sc;_} -> TyVec3 (data_of_scalar_type sc)
    | Typed_parsetree.Type.TyVec4 {value=TyDirect sc;_} -> TyVec4 (data_of_scalar_type sc)
    | Typed_parsetree.Type.TyMat2 {value=TyDirect sc;_} -> TyMat2 (data_of_scalar_type sc)
    | Typed_parsetree.Type.TyMat3 {value=TyDirect sc;_} -> TyMat3 (data_of_scalar_type sc)
    | Typed_parsetree.Type.TyMat4 {value=TyDirect sc;_} -> TyMat4 (data_of_scalar_type sc)
    | _ -> raise (ConversionException (UnsupportedType Typed_parsetree.Type.(TypeExpr (TyDirect (TyPure (TyNonScalar ns))))))
  and data_of_pure_data_type (pdt: Typed_parsetree.Type.pure_data_type) =
    match pdt with
    | Typed_parsetree.Type.TyScalar sc -> Unary (data_of_scalar_type sc)
    | Typed_parsetree.Type.TyNonScalar ns -> data_of_nonscalar_type ns
    | Typed_parsetree.Type.TyNumericData _ ->
      raise (ConversionException (UnresolvedType Typed_parsetree.Type.(TypeExpr (TyDirect (TyPure pdt)))))
  and data_of_texture_type (tt: Typed_parsetree.Type.texture_type) = match tt with
    | Typed_parsetree.Type.TyTexture1D _ -> TyTexture1D
    | Typed_parsetree.Type.TyTexture2D _ -> TyTexture2D
    | Typed_parsetree.Type.TyTexture3D _ -> TyTexture3D
    | Typed_parsetree.Type.TyTextureCube _ -> TyTextureCube
    | Typed_parsetree.Type.TyAnyTexture _ ->
      raise (ConversionException (UnresolvedType Typed_parsetree.Type.(TypeExpr (TyDirect (TyTexture tt)))))

  type 'a map = 'a Map.t

  let pp_map f fmt map =
    let open Format in
    Format.pp_print_list
      ~pp_sep:(fun fmt () -> pp_print_char fmt ';'; pp_print_space fmt ())
      (fun fmt (name, vl) ->
         pp_print_string fmt name;
         pp_print_char fmt '=';
         f fmt vl)
      fmt
      (Map.to_alist map)

  let show_map f map = pp_map f (Format.str_formatter) map; Format.flush_str_formatter ()

  type shader_map = {
    attributes: data_type map; uniforms: data_type map; textures: texture_type map;
  }
  [@@deriving show]

  type 'a tag =
    | Bool : bool tag
    | Int : int tag
    | Float : float tag
    | Vec2 : V2.t tag
    | Vec3 : V3.t tag
    | Vec4 : V4.t tag
    | Mat2 : M2.t tag
    | Mat3 : M3.t tag
    | Mat4 : M4.t tag

  let pp_tag (type a) fmt (tag: a tag) =
    let open Format in
    match tag with
    | Bool -> pp_print_string fmt "bool"
    | Int -> pp_print_string fmt "int"
    | Float -> pp_print_string fmt "float"
    | Vec2 -> pp_print_string fmt "vec2"
    | Vec3 -> pp_print_string fmt "vec3"
    | Vec4 -> pp_print_string fmt "vec4"
    | Mat2 -> pp_print_string fmt "mat2"
    | Mat3 -> pp_print_string fmt "mat3"
    | Mat4 -> pp_print_string fmt "mat4"

  let show_tag tag = pp_tag Format.str_formatter tag; Format.flush_str_formatter ()

  type 'a expr = Expr : 'a tag * 'a -> 'a expr

  let pp_expr _ (type a) fmt (Expr (tag, value): a expr) =
    let open Format in
    match tag with
    | Bool -> pp_print_bool fmt value
    | Int -> pp_print_int fmt value
    | Float -> pp_print_float fmt value
    | Vec2 -> V2.pp fmt value
    | Vec3 -> V3.pp fmt value
    | Vec4 -> V4.pp fmt value
    | Mat2 -> M2.pp fmt value
    | Mat3 -> M3.pp fmt value
    | Mat4 -> M4.pp fmt value
  let show_expr f expr = pp_expr f Format.str_formatter expr; Format.flush_str_formatter ()

  type wrapped_expr =
      Wrap : 'a expr -> wrapped_expr

  let pp_wrapped_expr fmt (Wrap expr) =
    pp_expr () fmt expr
  let show_wrapped_expr expr =
    pp_wrapped_expr Format.str_formatter expr; Format.flush_str_formatter ()

  let show_wrapped_expr_list ls =
    List.map ~f:show_wrapped_expr ls |> String.concat ~sep:"; " |> Printf.sprintf "[%s]"

  type ('ctx, 'a) computable_value = 'ctx -> 'a expr
  [@@deriving show]

  type 'a value =
      Independent of 'a expr
    | Context of (ctx -> 'a expr)
    | UnknownContext of (ctx -> wrapped_expr)

  let pp_value (type a) f fmt (value: a value) =
    match value with
    | Independent expr -> pp_expr f fmt expr
    | Context (f) ->
      Format.pp_print_string fmt "Context(fun () -> ";
      pp_expr () fmt (f default_context);
      Format.pp_print_char fmt ')'
    | UnknownContext (f) ->
      Format.pp_print_string fmt "UnknownContext(fun () -> ";
      pp_wrapped_expr fmt (f default_context);
      Format.pp_print_char fmt ')'
  let show_value f value = pp_value f Format.str_formatter value; Format.flush_str_formatter ()

  type transform =
      Scale of v3 value
    | Rotate of m4 value
    | Translate of v3 value 
  [@@deriving show]
  type wrap_option = Typed_parsetree.wrap_option
  [@@deriving show,sexp, hash,ord]
  type filter_option = Typed_parsetree.filter_option
  [@@deriving show,sexp,hash,ord]
  type texture_option =
    | TextureBaseLevel of int value
    | TextureMaxLevel of int value
    | TextureBorderColor of v4 value
    | WrapS of wrap_option
    | WrapT of wrap_option
    | MinFilter of filter_option
    | MagFilter of filter_option
  [@@deriving show]
  type blend_func = Typed_parsetree.blend_func
  [@@deriving show,sexp,hash, ord]
  type comparison_func = Typed_parsetree.comparison_func
  [@@deriving show,sexp,hash, ord]
  type draw_option =
    | AlphaTest of bool expr | BlendTest of bool expr
    | DepthTest of bool expr | StencilTest of bool expr
    | BlendColor of v4 value
    | BlendFunction of blend_func * blend_func
    | StencilFunction of comparison_func * int value * int value
    | DepthFunction of comparison_func
    | AlphaFunction of comparison_func * float value
  [@@deriving show]
  type texture = {
    name: string; textures: string list; typ: texture_type;
    texture_options: texture_option list;
  }
  [@@deriving show]

  
  type uniform =
      Uniform : {name:string; value: 'a value} -> uniform

  let pp_uniform fmt (Uniform { name; value }: uniform) =
    let open Format in
    pp_open_hvbox fmt 1;
    pp_print_string fmt "uniform";
    pp_print_char fmt '(';
    pp_open_hvbox fmt 1;
    pp_print_string fmt name;
    pp_print_char fmt ',';
    pp_print_space fmt ();
    pp_value () fmt value;
    pp_close_box fmt ();
    pp_print_char fmt ')';
    pp_close_box fmt ()

  let show_uniform uni = pp_uniform Format.str_formatter uni; Format.flush_str_formatter ()

  type attribute =
      Attribute : {name:string; value: 'a expr} -> attribute

  let pp_attribute fmt (Attribute { name; value }: attribute) =
    let open Format in
    pp_open_hvbox fmt 1;
    pp_print_string fmt "attribute";
    pp_print_char fmt '(';
    pp_open_hvbox fmt 1;
    pp_print_string fmt name;
    pp_print_char fmt ',';
    pp_print_space fmt ();
    pp_expr () fmt value;
    pp_close_box fmt ();
    pp_print_char fmt ')';
    pp_close_box fmt ()


  type light_uniform = LightUniform of {
        binding: string; value: (v3 * ctx -> wrapped_expr) 
      }
  let pp_light_uniform fmt (LightUniform { binding=name; value }: light_uniform) =
    let open Format in
    pp_open_hvbox fmt 1;
    pp_print_string fmt "LightUniform";
    pp_print_char fmt '(';
    pp_open_hvbox fmt 1;
    pp_print_string fmt name;
    pp_print_char fmt ',';
    pp_print_space fmt ();
    pp_wrapped_expr fmt (value (V3.oy, default_context));
    pp_close_box fmt ();
    pp_print_char fmt ')';
    pp_close_box fmt ()
  let show_light_uniform vl = pp_light_uniform Format.str_formatter vl; Format.flush_str_formatter ()
  
  type data_elem = DataElem : 'a expr -> data_elem

  let pp_data_elem fmt (DataElem expr: data_elem) =
    let open Format in
    pp_open_hvbox fmt 1;
    pp_print_string fmt "DataElem";
    pp_print_char fmt '(';
    pp_open_hvbox fmt 1;
    pp_expr () fmt expr;
    pp_close_box fmt ();
    pp_print_char fmt ')';
    pp_close_box fmt ()
  
  type data =
    | Model of string
    | Data of ( string * data_elem) list
    | Screen of {
        width: float expr option;
        height: float expr option;
      }
  [@@deriving show]
  type src_or_filename = Typed_parsetree.src_or_filename = Src of string | Filename of string
  [@@deriving show]
  type shader = Shader of {
      vertex_shader: string; fragment_shader: src_or_filename;
      geometry_shader: string option;
      shader_spec: Typed_parsetree.ShaderAnalyzer.shader_spec;
    }
  [@@deriving show]

  type modifiers = {
    transforms: transform list;
    uniforms: uniform list;
    attributes: attribute list;
    draw_options: draw_option list;
    textures: texture list;
    redirection: redirection list;
  }
  and redirection = Redirection of {
      id: int; binding_names: string list; depth: bool; stencil: bool;
      drawable: drawable;
    }
  and obj = modifiers * data
  and light = Light of {
      modifiers: modifiers;
      position: v3 value;
      size: float value;
    }
  and light_data =
      LightVolume of
        {
          uniforms: light_uniform list
        }
    | LightObjects of (light_uniform list * obj) list
  and drawable =
      BasicDrawable of {
        shader:shader;
        objects:obj list;
      }
    | LightDrawable of {
        shader: shader; lights: light list;
        light_data: light_data;
      }
  [@@deriving show]
  type program = (string option * drawable) list
  [@@deriving show]

  type 'b expr_reduction = { f: 'a. 'a expr -> 'b }
  type 'b value_reduction = { f: 'a. 'a value -> 'b }
  type 'b light_expr_reduction = { f: (v3 * ctx-> wrapped_expr) -> 'b }





  type evaluation_error =
    | TypeMismatch of string * string
    | InvalidBoolExpr of string
    | InvalidIntExpr of string
    | InvalidListExpr of string
    | UnboundVariable of string
    | OutOfBounds of int * string
    | InvalidNumericValue of wrapped_expr
    | InvalidBinaryOperands of Parsetree.binop * string * string
    | InvalidUnaryOperand of Parsetree.preop * string
    | InvalidVector of wrapped_expr list
    | EmptyStdLib
    | BadDynamicValue of string
    | InvalidFloatExpression of string
    | InvalidVec2Expression of string
    | InvalidVec3Expression of string
    | InvalidVec4Expression of string
    | InvalidMat2Expression of string
    | InvalidMat3Expression of string
    | InvalidMat4Expression of string
  [@@deriving show]

  exception EvaluationException of evaluation_error
  let () = Stdlib.Printexc.register_printer (function
        EvaluationException err ->
        let fmt = Format.str_formatter in
        pp_evaluation_error fmt err;
        let str = Format.flush_str_formatter () in
        Some str
      | _ -> None
    )


  let lookup (map, _) name = Typed_parsetree.IdentifierTable.lookup map name
  let record_expr_binding (other, map) name value = other, Map.set map ~key:name ~data:(`Single value) 
  let record_expr_list_binding (other, map) name value = other, Map.set map ~key:name ~data:(`Multi value) 

  let lookup_expr (_, map) name =
    match Map.find map name with
    | None -> raise (EvaluationException (UnboundVariable name))
    | Some v -> v



  let rec eval_expr_list map (value:  Typed_parsetree.Substitution.variable_type Typed_parsetree.expr) : wrapped_expr list =
    match value with
    | Typed_parsetree.Exp_variable (name, _) ->
      let name = lookup map name in
      let expr = lookup_expr map name in
      begin match expr with
        | `Single wexp -> raise (EvaluationException (InvalidListExpr (show_wrapped_expr wexp)))
        | `Multi exps -> exps
      end
    | Typed_parsetree.Exp_list (elems, _) -> List.map ~f:(eval_expr_wrapper map) elems
    | Typed_parsetree.Exp_list_comprehension ((expr, id, froe, toe), _) ->
      let name = lookup map id in
      begin match toe with
        | None ->
          let froe = eval_expr_list map froe in
          List.map froe ~f:(fun vl ->
              let map = record_expr_binding map name vl in
              eval_expr_wrapper map expr
            )
        | Some toe ->
          let froe = match eval_int_expr map froe with Expr (Int, vl) -> vl | _ -> assert false in
          let toe = match eval_int_expr map toe with Expr (Int, vl) -> vl | _ -> assert false in
          List.range ~start:`inclusive ~stop:`exclusive froe toe
          |> List.map ~f:(fun vl ->
              let map = record_expr_binding map name (Wrap (Expr (Int, vl))) in
              eval_expr_wrapper map expr
            )
      end
    | exp ->
      raise (EvaluationException
               (InvalidListExpr (Typed_parsetree.pp_expr (fun _ _ -> ()) Format.str_formatter exp; Format.flush_str_formatter ())))
  and expand_wrapper_to_numeric (Wrap Expr (tag, value): wrapped_expr) =
    match tag with
    | Bool -> `Bool (value: bool)
    | Int -> `Int (value : int)
    | Float -> `Float (value: float)
    | Vec2 -> `Vec2 (value: V2.t)
    | Vec3 -> `Vec3 (value: V3.t)
    | Vec4 -> `Vec4 (value: V4.t)
    | Mat2 -> `Mat2 (value: M2.t)
    | Mat3 -> `Mat3 (value: M3.t)
    | Mat4 -> `Mat4 (value: M4.t)
  and expand_wrapped_binop op1 e1 e2 ({f}: 'b expr_reduction) =
    let to_string e1 = match e1 with `Bool _ -> "bool"
                                   | `Float _ -> "float" | `Int _ -> "int" | `Mat2 _ -> "mat2" | `Mat3 _ -> "mat3"
                                   | `Mat4 _ -> "mat4" | `Vec2 _ -> "vec2" | `Vec3 _ -> "vec3" | `Vec4 _ -> "vec4" in
    match op1, e1, e2 with
    | (Parsetree.Mul, `Float n1, `Float n2) -> f (Expr (Float, (n1 *. n2)))
    | (Parsetree.Mul, `Int n2, `Float n1)
    | (Parsetree.Mul, `Float n1, `Int n2) -> f (Expr (Float, (n1 *. Float.of_int n2)))
    | (Parsetree.Mul, `Int n1, `Int n2) -> f (Expr (Int, (n1 * n2)))

    | (Parsetree.Mul, `Float n1, `Mat2 n2) -> f (Expr (Mat2, M2.(smul n1 n2)))
    | (Parsetree.Mul, `Int n1, `Mat2 n2) -> f (Expr (Mat2, M2.(smul (Float.of_int n1) n2)))
    | (Parsetree.Mul, `Float n1, `Vec2 n2) -> f (Expr (Vec2, V2.(smul n1 n2)))
    | (Parsetree.Mul, `Int n1, `Vec2 n2) -> f (Expr (Vec2, V2.(smul (Float.of_int n1) n2)))
    | (Parsetree.Mul,  `Mat2 n2, `Float n1) -> f (Expr (Mat2, M2.(smul n1 n2)))
    | (Parsetree.Mul,  `Mat2 n2, `Int n1) -> f (Expr (Mat2, M2.(smul (Float.of_int n1) n2)))
    | (Parsetree.Mul,  `Vec2 n2, `Float n1) -> f (Expr (Vec2, V2.(smul n1 n2)))
    | (Parsetree.Mul,  `Vec2 n2, `Int n1) -> f (Expr (Vec2, V2.(smul (Float.of_int n1) n2)))
    | (Parsetree.Mul, `Mat2 n1, `Mat2 n2) -> f (Expr (Mat2, M2.(mul n1 n2)))
    | (Parsetree.Mul, `Mat2 n1, `Vec2 n2) -> f (Expr (Vec2, V2.(ltr n1 n2)))
    | (Parsetree.Mul, `Mat3 n1, `Vec2 n2) -> f (Expr (Vec2, V2.(tr n1 n2)))
    | (Parsetree.Mul, `Vec2 n1, `Vec2 n2) -> f (Expr (Vec2, V2.(mul n1 n2)))

    | (Parsetree.Mul, `Float n1, `Mat3 n2) -> f (Expr (Mat3, M3.(smul n1 n2)))
    | (Parsetree.Mul, `Int n1, `Mat3 n2) -> f (Expr (Mat3, M3.(smul (Float.of_int n1) n2)))
    | (Parsetree.Mul, `Float n1, `Vec3 n2) -> f (Expr (Vec3, V3.(smul n1 n2)))
    | (Parsetree.Mul, `Int n1, `Vec3 n2) -> f (Expr (Vec3, V3.(smul (Float.of_int n1) n2)))
    | (Parsetree.Mul,  `Mat3 n2, `Float n1) -> f (Expr (Mat3, M3.(smul n1 n2)))
    | (Parsetree.Mul,  `Mat3 n2, `Int n1) -> f (Expr (Mat3, M3.(smul (Float.of_int n1) n2)))
    | (Parsetree.Mul,  `Vec3 n2, `Float n1) -> f (Expr (Vec3, V3.(smul n1 n2)))
    | (Parsetree.Mul,  `Vec3 n2, `Int n1) -> f (Expr (Vec3, V3.(smul (Float.of_int n1) n2)))
    | (Parsetree.Mul, `Mat3 n1, `Mat3 n2) -> f (Expr (Mat3, M3.(mul n1 n2)))
    | (Parsetree.Mul, `Mat3 n1, `Vec3 n2) -> f (Expr (Vec3, V3.(ltr n1 n2)))
    | (Parsetree.Mul, `Mat4 n1, `Vec3 n2) -> f (Expr (Vec3, V3.(tr n1 n2)))
    | (Parsetree.Mul, `Vec3 n1, `Vec3 n2) -> f (Expr (Vec3, V3.(mul n1 n2)))

    | (Parsetree.Mul, `Float n1, `Mat4 n2) -> f (Expr (Mat4, M4.(smul n1 n2)))
    | (Parsetree.Mul, `Int n1, `Mat4 n2) -> f (Expr (Mat4, M4.(smul (Float.of_int n1) n2)))
    | (Parsetree.Mul, `Float n1, `Vec4 n2) -> f (Expr (Vec4, V4.(smul n1 n2)))
    | (Parsetree.Mul, `Int n1, `Vec4 n2) -> f (Expr (Vec4, V4.(smul (Float.of_int n1) n2)))
    | (Parsetree.Mul, `Mat4 n2, `Float n1) -> f (Expr (Mat4, M4.(smul n1 n2)))
    | (Parsetree.Mul, `Mat4 n2, `Int n1) -> f (Expr (Mat4, M4.(smul (Float.of_int n1) n2)))
    | (Parsetree.Mul, `Vec4 n2, `Float n1) -> f (Expr (Vec4, V4.(smul n1 n2)))
    | (Parsetree.Mul, `Vec4 n2, `Int n1) -> f (Expr (Vec4, V4.(smul (Float.of_int n1) n2)))
    | (Parsetree.Mul, `Mat4 n1, `Mat4 n2) -> f (Expr (Mat4, M4.(mul n1 n2)))
    | (Parsetree.Mul, `Mat4 n1, `Vec4 n2) -> f (Expr (Vec4, V4.(ltr n1 n2)))
    | (Parsetree.Mul, `Vec4 n1, `Vec4 n2) -> f (Expr (Vec4, V4.(mul n1 n2)))

    | (Parsetree.Div, `Float v1, `Float v2) -> f (Expr (Float, v1 /. v2))
    | (Parsetree.Div, `Float v1, `Int v2) -> f (Expr (Float, v1 /. Float.of_int v2))
    | (Parsetree.Div, `Int v1, `Float v2) -> f (Expr (Float, Float.of_int v1 /. v2))
    | (Parsetree.Div, `Int v1, `Int v2) -> f (Expr (Float, Float.of_int v1 /. Float.of_int v2))
    | (Parsetree.Div, `Mat2 v1, `Int v2) -> f (Expr (Mat2, M2.map (fun v -> v /. Float.of_int v2) v1))
    | (Parsetree.Div, `Mat2 v1, `Float v2) -> f (Expr (Mat2, M2.map (fun v -> v /. v2) v1))
    | (Parsetree.Div, `Vec2 v1, `Int v2) -> f (Expr (Vec2, V2.map (fun v -> v /. Float.of_int v2) v1))
    | (Parsetree.Div, `Vec2 v1, `Float v2) -> f (Expr (Vec2, V2.map (fun v -> v /. v2) v1))
    | (Parsetree.Div, `Mat3 v1, `Int v2) -> f (Expr (Mat3, M3.map (fun v -> v /. Float.of_int v2) v1))
    | (Parsetree.Div, `Mat3 v1, `Float v2) -> f (Expr (Mat3, M3.map (fun v -> v /. v2) v1))
    | (Parsetree.Div, `Vec3 v1, `Int v2) -> f (Expr (Vec3, V3.map (fun v -> v /. Float.of_int v2) v1))
    | (Parsetree.Div, `Vec3 v1, `Float v2) -> f (Expr (Vec3, V3.map (fun v -> v /. v2) v1))
    | (Parsetree.Div, `Mat4 v1, `Int v2) -> f (Expr (Mat4, M4.map (fun v -> v /. Float.of_int v2) v1))
    | (Parsetree.Div, `Mat4 v1, `Float v2) -> f (Expr (Mat4, M4.map (fun v -> v /. v2) v1))
    | (Parsetree.Div, `Vec4 v1, `Int v2) -> f (Expr (Vec4, V4.map (fun v -> v /. Float.of_int v2) v1))
    | (Parsetree.Div, `Vec4 v1, `Float v2) -> f (Expr (Vec4, V4.map (fun v -> v /. v2) v1))

    | (Parsetree.Mod, `Float v1, `Float v2) -> f (Expr (Int, Int.of_float v1 % Int.of_float v2))
    | (Parsetree.Mod, `Float v1, `Int v2) -> f (Expr (Int, Int.of_float v1 % v2))
    | (Parsetree.Mod, `Int v1, `Float v2) -> f (Expr (Int, v1 % Int.of_float v2))
    | (Parsetree.Mod, `Int v1, `Int v2) -> f (Expr (Int, v1 %  v2))
    | (Parsetree.Mod, `Mat2 v1, `Int v2) -> f (Expr (Mat2, M2.map (fun v -> Float.of_int @@ Int.of_float v % v2) v1))
    | (Parsetree.Mod, `Mat2 v1, `Float v2) -> f (Expr (Mat2, M2.map (fun v -> Float.of_int @@ Int.of_float v % Int.of_float v2) v1))
    | (Parsetree.Mod, `Vec2 v1, `Int v2) -> f (Expr (Vec2, V2.map (fun v -> Float.of_int @@ Int.of_float v % v2) v1))
    | (Parsetree.Mod, `Vec2 v1, `Float v2) -> f (Expr (Vec2, V2.map (fun v -> Float.of_int @@ Int.of_float v % Int.of_float v2) v1))
    | (Parsetree.Mod, `Mat3 v1, `Int v2) -> f (Expr (Mat3, M3.map (fun v -> Float.of_int @@ Int.of_float v % v2) v1))
    | (Parsetree.Mod, `Mat3 v1, `Float v2) -> f (Expr (Mat3, M3.map (fun v -> Float.of_int @@ Int.of_float v % Int.of_float v2) v1))
    | (Parsetree.Mod, `Vec3 v1, `Int v2) -> f (Expr (Vec3, V3.map (fun v -> Float.of_int @@ Int.of_float v % v2) v1))
    | (Parsetree.Mod, `Vec3 v1, `Float v2) -> f (Expr (Vec3, V3.map (fun v -> Float.of_int @@ Int.of_float v % Int.of_float v2) v1))
    | (Parsetree.Mod, `Mat4 v1, `Int v2) -> f (Expr (Mat4, M4.map (fun v -> Float.of_int @@ Int.of_float v % v2) v1))
    | (Parsetree.Mod, `Mat4 v1, `Float v2) -> f (Expr (Mat4, M4.map (fun v -> Float.of_int @@ Int.of_float v % Int.of_float v2) v1))
    | (Parsetree.Mod, `Vec4 v1, `Int v2) -> f (Expr (Vec4, V4.map (fun v -> Float.of_int @@ Int.of_float v % v2) v1))
    | (Parsetree.Mod, `Vec4 v1, `Float v2) -> f (Expr (Vec4, V4.map (fun v -> Float.of_int @@ Int.of_float v % Int.of_float v2) v1))

    | (Parsetree.Plus, `Float v1, `Float v2) -> f (Expr (Float, v1 +. v2))
    | (Parsetree.Plus, `Float v1, `Int v2) -> f (Expr (Float, v1 +. Float.of_int v2))
    | (Parsetree.Plus, `Int v1, `Float v2) -> f (Expr (Float, Float.of_int v1 +. v2))
    | (Parsetree.Plus, `Int v1, `Int v2) -> f (Expr (Int, v1 + v2))
    | (Parsetree.Plus,  `Int v2, `Mat2 v1) | (Parsetree.Plus, `Mat2 v1, `Int v2) -> f (Expr (Mat2, M2.map (fun v -> v +. Float.of_int v2) v1))
    | (Parsetree.Plus,  `Float v2, `Mat2 v1) | (Parsetree.Plus, `Mat2 v1, `Float v2) -> f (Expr (Mat2, M2.map (fun v -> v +. v2) v1))
    | (Parsetree.Plus,  `Int v2, `Vec2 v1) | (Parsetree.Plus, `Vec2 v1, `Int v2) -> f (Expr (Vec2, V2.map (fun v -> v +. Float.of_int v2) v1))
    | (Parsetree.Plus,  `Float v2, `Vec2 v1) | (Parsetree.Plus, `Vec2 v1, `Float v2) -> f (Expr (Vec2, V2.map (fun v -> v +. v2) v1))
    | (Parsetree.Plus,  `Int v2, `Mat3 v1) | (Parsetree.Plus, `Mat3 v1, `Int v2) -> f (Expr (Mat3, M3.map (fun v -> v +. Float.of_int v2) v1))
    | (Parsetree.Plus,  `Float v2, `Mat3 v1) | (Parsetree.Plus, `Mat3 v1, `Float v2) -> f (Expr (Mat3, M3.map (fun v -> v +. v2) v1))
    | (Parsetree.Plus,  `Int v2, `Vec3 v1) | (Parsetree.Plus, `Vec3 v1, `Int v2) -> f (Expr (Vec3, V3.map (fun v -> v +. Float.of_int v2) v1))
    | (Parsetree.Plus,  `Float v2, `Vec3 v1) | (Parsetree.Plus, `Vec3 v1, `Float v2) -> f (Expr (Vec3, V3.map (fun v -> v +. v2) v1))
    | (Parsetree.Plus,  `Int v2, `Mat4 v1) | (Parsetree.Plus, `Mat4 v1, `Int v2) -> f (Expr (Mat4, M4.map (fun v -> v +. Float.of_int v2) v1))
    | (Parsetree.Plus,  `Float v2, `Mat4 v1) | (Parsetree.Plus, `Mat4 v1, `Float v2) -> f (Expr (Mat4, M4.map (fun v -> v +. v2) v1))
    | (Parsetree.Plus,  `Int v2, `Vec4 v1) | (Parsetree.Plus, `Vec4 v1, `Int v2) -> f (Expr (Vec4, V4.map (fun v -> v +. Float.of_int v2) v1))
    | (Parsetree.Plus,  `Float v2, `Vec4 v1) | (Parsetree.Plus, `Vec4 v1, `Float v2) -> f (Expr (Vec4, V4.map (fun v -> v +. v2) v1))
    | (Parsetree.Plus, `Vec2 v1, `Vec2 v2) -> f (Expr (Vec2, V2.(v1 + v2)))
    | (Parsetree.Plus, `Vec3 v1, `Vec3 v2) -> f (Expr (Vec3, V3.(v1 + v2)))
    | (Parsetree.Plus, `Vec4 v1, `Vec4 v2) -> f (Expr (Vec4, V4.(v1 + v2)))
    | (Parsetree.Plus, `Mat2 v1, `Mat2 v2) -> f (Expr (Mat2, M2.(add v1 v2)))
    | (Parsetree.Plus, `Mat3 v1, `Mat3 v2) -> f (Expr (Mat3, M3.(add v1 v2)))
    | (Parsetree.Plus, `Mat4 v1, `Mat4 v2) -> f (Expr (Mat4, M4.(add v1 v2)))


    | (Parsetree.Minus, `Float v1, `Float v2) -> f (Expr (Float, v1 -. v2))
    | (Parsetree.Minus, `Float v1, `Int v2) -> f (Expr (Float, v1 -. Float.of_int v2))
    | (Parsetree.Minus, `Int v1, `Float v2) -> f (Expr (Float, Float.of_int v1 -. v2))
    | (Parsetree.Minus, `Int v1, `Int v2) -> f (Expr (Int, v1 - v2))
    | (Parsetree.Minus,  `Int v2, `Mat2 v1) | (Parsetree.Minus, `Mat2 v1, `Int v2) -> f (Expr (Mat2, M2.map (fun v -> v -. Float.of_int v2) v1))
    | (Parsetree.Minus,  `Float v2, `Mat2 v1) | (Parsetree.Minus, `Mat2 v1, `Float v2) -> f (Expr (Mat2, M2.map (fun v -> v -. v2) v1))
    | (Parsetree.Minus,  `Int v2, `Vec2 v1) | (Parsetree.Minus, `Vec2 v1, `Int v2) -> f (Expr (Vec2, V2.map (fun v -> v -. Float.of_int v2) v1))
    | (Parsetree.Minus,  `Float v2, `Vec2 v1) | (Parsetree.Minus, `Vec2 v1, `Float v2) -> f (Expr (Vec2, V2.map (fun v -> v -. v2) v1))
    | (Parsetree.Minus,  `Int v2, `Mat3 v1) | (Parsetree.Minus, `Mat3 v1, `Int v2) -> f (Expr (Mat3, M3.map (fun v -> v -. Float.of_int v2) v1))
    | (Parsetree.Minus,  `Float v2, `Mat3 v1) | (Parsetree.Minus, `Mat3 v1, `Float v2) -> f (Expr (Mat3, M3.map (fun v -> v -. v2) v1))
    | (Parsetree.Minus,  `Int v2, `Vec3 v1) | (Parsetree.Minus, `Vec3 v1, `Int v2) -> f (Expr (Vec3, V3.map (fun v -> v -. Float.of_int v2) v1))
    | (Parsetree.Minus,  `Float v2, `Vec3 v1) | (Parsetree.Minus, `Vec3 v1, `Float v2) -> f (Expr (Vec3, V3.map (fun v -> v -. v2) v1))
    | (Parsetree.Minus,  `Int v2, `Mat4 v1) | (Parsetree.Minus, `Mat4 v1, `Int v2) -> f (Expr (Mat4, M4.map (fun v -> v -. Float.of_int v2) v1))
    | (Parsetree.Minus,  `Float v2, `Mat4 v1) | (Parsetree.Minus, `Mat4 v1, `Float v2) -> f (Expr (Mat4, M4.map (fun v -> v -. v2) v1))
    | (Parsetree.Minus,  `Int v2, `Vec4 v1) | (Parsetree.Minus, `Vec4 v1, `Int v2) -> f (Expr (Vec4, V4.map (fun v -> v -. Float.of_int v2) v1))
    | (Parsetree.Minus,  `Float v2, `Vec4 v1) | (Parsetree.Minus, `Vec4 v1, `Float v2) -> f (Expr (Vec4, V4.map (fun v -> v -. v2) v1))
    | (Parsetree.Minus, `Vec2 v1, `Vec2 v2) -> f (Expr (Vec2, V2.(v1 - v2)))
    | (Parsetree.Minus, `Vec3 v1, `Vec3 v2) -> f (Expr (Vec3, V3.(v1 - v2)))
    | (Parsetree.Minus, `Vec4 v1, `Vec4 v2) -> f (Expr (Vec4, V4.(v1 - v2)))
    | (Parsetree.Minus, `Mat2 v1, `Mat2 v2) -> f (Expr (Mat2, M2.(sub v1 v2)))
    | (Parsetree.Minus, `Mat3 v1, `Mat3 v2) -> f (Expr (Mat3, M3.(sub v1 v2)))
    | (Parsetree.Minus, `Mat4 v1, `Mat4 v2) -> f (Expr (Mat4, M4.(sub v1 v2)))
    | (Parsetree.And, `Bool b1, `Bool b2) -> f (Expr (Bool, b1 && b2))
    | (Parsetree.Or, `Bool b1, `Bool b2) -> f (Expr (Bool, b1 || b2))
    | _ -> raise (EvaluationException (InvalidBinaryOperands (op1, to_string e1, to_string e2) ))
  and expand_wrapped_preop op1 e1 ({f}: 'b expr_reduction) =
    let to_string e1 = match e1 with `Bool _ -> "bool"
                                   | `Float _ -> "float" | `Int _ -> "int" | `Mat2 _ -> "mat2" | `Mat3 _ -> "mat3"
                                   | `Mat4 _ -> "mat4" | `Vec2 _ -> "vec2" | `Vec3 _ -> "vec3" | `Vec4 _ -> "vec4" in
    match op1, e1 with
    | (Parsetree.UPlus, `Int n) -> f (Expr (Int, + n))
    | (Parsetree.UPlus, `Float n) -> f (Expr (Float, +. n))
    | (Parsetree.UPlus, `Mat2 n) -> f (Expr (Mat2, n))
    | (Parsetree.UPlus, `Mat3 n) -> f (Expr (Mat3, n))
    | (Parsetree.UPlus, `Mat4 n) -> f (Expr (Mat4, n))
    | (Parsetree.UPlus, `Vec2 n) -> f (Expr (Vec2, n))
    | (Parsetree.UPlus, `Vec3 n) -> f (Expr (Vec3, n))
    | (Parsetree.UPlus, `Vec4 n) -> f (Expr (Vec4, n))

    | (Parsetree.UMinus, `Int n) -> f (Expr (Int, - n))
    | (Parsetree.UMinus, `Float n) -> f (Expr (Float, -. n))
    | (Parsetree.UMinus, `Mat2 n) -> f (Expr (Mat2, M2.map (fun v -> -.v) n))
    | (Parsetree.UMinus, `Mat3 n) -> f (Expr (Mat3, M3.map (fun v -> -.v) n))
    | (Parsetree.UMinus, `Mat4 n) -> f (Expr (Mat4, M4.map (fun v -> -.v) n))
    | (Parsetree.UMinus, `Vec2 n) -> f (Expr (Vec2, V2.map (fun v -> -.v) n))
    | (Parsetree.UMinus, `Vec3 n) -> f (Expr (Vec3, V3.map (fun v -> -.v) n))
    | (Parsetree.UMinus, `Vec4 n) -> f (Expr (Vec4, V4.map (fun v -> -.v)  n))

    | (Parsetree.UNot, `Bool b1) -> f (Expr (Bool, not b1))
    | _ -> raise (EvaluationException (InvalidUnaryOperand (op1, to_string e1) ))                             
  and eval_vector_wrapper map vec =
    match vec with
    | Typed_parsetree.Vector2 (v1, v2) ->
      begin match eval_expr_wrapper map v1, eval_expr_wrapper map v2 with
        | Wrap Expr (Float, v1), Wrap Expr (Float, v2) ->
          Wrap (Expr (Vec2, V2.v v1 v2))
        | Wrap Expr (Float, v1), Wrap Expr (Int, v2) ->
          Wrap (Expr (Vec2, V2.v v1 (Float.of_int v2)))
        | Wrap Expr (Int, v1), Wrap Expr (Float, v2) ->
          Wrap (Expr (Vec2, V2.v (Float.of_int v1) v2))
        | Wrap Expr (Int, v1), Wrap Expr (Int, v2) ->
          Wrap (Expr (Vec2, V2.v (Float.of_int v1) (Float.of_int v2)))
        | Wrap Expr (Vec2, v1), Wrap Expr (Vec2, v2) ->
          Wrap (Expr (Mat2, M2.of_rows v1 v2))
        | v1,v2 -> raise (EvaluationException (InvalidVector [v1;v2]))
      end
    | Typed_parsetree.Vector3 (v1,v2,v3) ->
      begin match eval_expr_wrapper map v1, eval_expr_wrapper map v2, eval_expr_wrapper map v3 with
        | Wrap Expr (Float, v1), Wrap Expr (Float, v2), Wrap Expr (Float, v3) ->
          Wrap (Expr (Vec3, V3.v v1 v2 v3))
        | Wrap Expr (Float, v1), Wrap Expr (Float, v2), Wrap Expr (Int, v3) ->
          Wrap (Expr (Vec3, V3.v v1 v2 (Float.of_int v3)))
        | Wrap Expr (Float, v1), Wrap Expr (Int, v2), Wrap Expr (Float, v3) ->
          Wrap (Expr (Vec3, V3.v v1 (Float.of_int v2) v3))
        | Wrap Expr (Float, v1), Wrap Expr (Int, v2), Wrap Expr (Int, v3) ->
          Wrap (Expr (Vec3, V3.v v1 (Float.of_int v2) (Float.of_int v3)))
        | Wrap Expr (Int, v1), Wrap Expr (Float, v2), Wrap Expr (Float, v3) ->
          Wrap (Expr (Vec3, V3.v (Float.of_int v1) v2 v3))
        | Wrap Expr (Int, v1), Wrap Expr (Float, v2), Wrap Expr (Int, v3) ->
          Wrap (Expr (Vec3, V3.v (Float.of_int v1) v2 (Float.of_int v3)))
        | Wrap Expr (Int, v1), Wrap Expr (Int, v2), Wrap Expr (Float, v3) ->
          Wrap (Expr (Vec3, V3.v (Float.of_int v1) (Float.of_int v2) v3))
        | Wrap Expr (Int, v1), Wrap Expr (Int, v2), Wrap Expr (Int, v3) ->
          Wrap (Expr (Vec3, V3.v (Float.of_int v1) (Float.of_int v2) (Float.of_int v3)))
        | Wrap Expr (Vec3, v1), Wrap Expr (Vec3, v2), Wrap Expr (Vec3, v3) ->
          Wrap (Expr (Mat3, M3.of_rows v1 v2 v3))
        | v1,v2,v3 -> raise (EvaluationException (InvalidVector [v1;v2; v3]))
      end
    | Typed_parsetree.Vector4 (v1,v2,v3,v4) ->
      begin match eval_expr_wrapper map v1, eval_expr_wrapper map v2, eval_expr_wrapper map v3, eval_expr_wrapper map v4 with
        | Wrap Expr (Float, v1), Wrap Expr (Float, v2), Wrap Expr (Float, v3), Wrap Expr (Float, v4) ->
          Wrap (Expr (Vec4, V4.v v1 v2 v3 v4))
        | Wrap Expr (Float, v1), Wrap Expr (Float, v2), Wrap Expr (Float, v3), Wrap Expr (Int, v4) ->
          Wrap (Expr (Vec4, V4.v v1 v2 v3 (Float.of_int v4)))
        | Wrap Expr (Float, v1), Wrap Expr (Float, v2), Wrap Expr (Int, v3), Wrap Expr (Float, v4) ->
          Wrap (Expr (Vec4, V4.v v1 v2 (Float.of_int v3)  v4))
        | Wrap Expr (Float, v1), Wrap Expr (Float, v2), Wrap Expr (Int, v3), Wrap Expr (Int, v4) ->
          Wrap (Expr (Vec4, V4.v v1 v2  (Float.of_int v3) (Float.of_int v4)))
        | Wrap Expr (Float, v1), Wrap Expr (Int, v2), Wrap Expr (Float, v3), Wrap Expr (Float, v4) ->
          Wrap (Expr (Vec4, V4.v v1 (Float.of_int v2) v3 v4))
        | Wrap Expr (Float, v1), Wrap Expr (Int, v2), Wrap Expr (Float, v3), Wrap Expr (Int, v4) ->
          Wrap (Expr (Vec4, V4.v v1 (Float.of_int v2) v3 (Float.of_int v4)))
        | Wrap Expr (Float, v1), Wrap Expr (Int, v2), Wrap Expr (Int, v3), Wrap Expr (Float, v4) ->
          Wrap (Expr (Vec4, V4.v v1 (Float.of_int v2) (Float.of_int v3) v4))
        | Wrap Expr (Float, v1), Wrap Expr (Int, v2), Wrap Expr (Int, v3), Wrap Expr (Int, v4) ->
          Wrap (Expr (Vec4, V4.v v1 (Float.of_int v2) (Float.of_int v3) (Float.of_int v4)))
        | Wrap Expr (Int, v1), Wrap Expr (Float, v2), Wrap Expr (Float, v3), Wrap Expr (Float, v4) ->
          Wrap (Expr (Vec4, V4.v (Float.of_int v1) v2 v3 v4))
        | Wrap Expr (Int, v1), Wrap Expr (Float, v2), Wrap Expr (Float, v3), Wrap Expr (Int, v4) ->
          Wrap (Expr (Vec4, V4.v (Float.of_int v1) v2 v3 (Float.of_int v4)))
        | Wrap Expr (Int, v1), Wrap Expr (Float, v2), Wrap Expr (Int, v3), Wrap Expr (Float, v4) ->
          Wrap (Expr (Vec4, V4.v (Float.of_int v1) v2 (Float.of_int v3) v4))
        | Wrap Expr (Int, v1), Wrap Expr (Float, v2), Wrap Expr (Int, v3), Wrap Expr (Int, v4) ->
          Wrap (Expr (Vec4, V4.v (Float.of_int v1) v2 (Float.of_int v3) (Float.of_int v4)))
        | Wrap Expr (Int, v1), Wrap Expr (Int, v2), Wrap Expr (Float, v3), Wrap Expr (Float, v4) ->
          Wrap (Expr (Vec4, V4.v (Float.of_int v1) (Float.of_int v2) v3 v4))
        | Wrap Expr (Int, v1), Wrap Expr (Int, v2), Wrap Expr (Float, v3), Wrap Expr (Int, v4) ->
          Wrap (Expr (Vec4, V4.v (Float.of_int v1) (Float.of_int v2) v3 (Float.of_int v4)))
        | Wrap Expr (Int, v1), Wrap Expr (Int, v2), Wrap Expr (Int, v3), Wrap Expr (Float, v4) ->
          Wrap (Expr (Vec4, V4.v (Float.of_int v1) (Float.of_int v2) (Float.of_int v3) v4))
        | Wrap Expr (Int, v1), Wrap Expr (Int, v2), Wrap Expr (Int, v3), Wrap Expr (Int, v4) ->
          Wrap (Expr (Vec4, V4.v (Float.of_int v1) (Float.of_int v2) (Float.of_int v3) (Float.of_int v4)))
        | Wrap Expr (Vec4, v1), Wrap Expr (Vec4, v2), Wrap Expr (Vec4, v3), Wrap Expr (Vec4, v4) ->
          Wrap (Expr (Mat4, M4.of_rows v1 v2 v3 v4))
        | v1,v2,v3,v4 -> raise (EvaluationException (InvalidVector [v1;v2;v3;v4]))
      end
  and eval_function_call_wrapped _name _args = raise (EvaluationException (EmptyStdLib))
  and eval_expr_or_list map expr =
    try `Multi (eval_expr_list map expr) with
      EvaluationException (InvalidListExpr _) -> `Single (eval_expr_wrapper map expr)
  and eval_expr_wrapper map (expr: Typed_parsetree.Substitution.variable_type Typed_parsetree.expr) = match expr with
    | Typed_parsetree.Exp_bool (vl, _) -> Wrap (Expr (Bool, vl))
    | Typed_parsetree.Exp_number (Int n, _) -> Wrap (Expr (Int, n))
    | Typed_parsetree.Exp_number (Float n, _) -> Wrap (Expr (Float, n))
    | Typed_parsetree.Exp_variable (name, _) ->
      let name = lookup map name in
      let expr = lookup_expr map name in
      begin match expr with
        | `Single expr -> expr
        | `Multi ls -> raise (EvaluationException (InvalidListExpr (show_wrapped_expr_list ls)))
      end
    | Typed_parsetree.Exp_binop ((op1, e1, e2), _) ->
      let e1 = expand_wrapper_to_numeric @@ eval_expr_wrapper map e1 in
      let e2 = expand_wrapper_to_numeric @@ eval_expr_wrapper map e2 in
      expand_wrapped_binop op1 e1 e2 {f=fun expr -> Wrap expr}
    | Typed_parsetree.Exp_preop ((op1, e1), _) ->
      let e1 = expand_wrapper_to_numeric @@ eval_expr_wrapper map e1 in
      expand_wrapped_preop op1 e1 {f=fun expr -> Wrap expr}
    | Typed_parsetree.Exp_index ((ls, ind), _) ->
      let ls = eval_expr_or_list map ls in
      let ind = match eval_int_expr map ind with | Expr (Int, n) -> n | _ -> assert false in
      begin match ls with
        | `Multi ls ->
          if ind > List.length ls
          then raise (EvaluationException (OutOfBounds (ind, show_wrapped_expr_list ls)))
          else (List.nth_exn ls ind)
        | `Single (Wrap Expr (Vec2, v) as wexp) ->
          if ind > 1
          then raise (EvaluationException (OutOfBounds (ind, show_wrapped_expr wexp)))
          else Wrap (Expr (Float, V2.comp ind v))
        | `Single (Wrap Expr (Vec3, v) as wexp) ->
          if ind > 2
          then raise (EvaluationException (OutOfBounds (ind, show_wrapped_expr wexp)))
          else Wrap (Expr (Float, V3.comp ind v))
        | `Single (Wrap Expr (Vec4, v) as wexp) ->
          if ind > 3
          then raise (EvaluationException (OutOfBounds (ind, show_wrapped_expr wexp)))
          else Wrap (Expr (Float, V4.comp ind v))
        | `Single (Wrap Expr (Mat2, v) as wexp) ->
          if ind > 1
          then raise (EvaluationException (OutOfBounds (ind, show_wrapped_expr wexp)))
          else Wrap (Expr (Vec2, M2.row ind v))
        | `Single (Wrap Expr (Mat3, v) as wexp) ->
          if ind > 2
          then raise (EvaluationException (OutOfBounds (ind, show_wrapped_expr wexp)))
          else Wrap (Expr (Vec3, M3.row ind v))
        | `Single (Wrap Expr (Mat4, v) as wexp) ->
          if ind > 3
          then raise (EvaluationException (OutOfBounds (ind, show_wrapped_expr wexp)))
          else Wrap (Expr (Vec4, M4.row ind v))
        | `Single expr ->
          raise (EvaluationException (InvalidListExpr (show_wrapped_expr expr)))
      end
    | Typed_parsetree.Exp_if_then_else ((cond, ift, elst), _) -> 
      let cond = eval_bool_expr map cond in
      begin match (cond: bool expr) with
        | Expr (Bool, (true: bool)) -> eval_expr_wrapper map ift
        | Expr (Bool, (false: bool)) -> eval_expr_wrapper map elst
        | _ -> assert false
      end
    | Typed_parsetree.Exp_vector (vec, _) -> eval_vector_wrapper map vec
    | Typed_parsetree.Exp_function_call (name, Typed_parsetree.FunctionArguments { positional_args; _ }, _) ->
      let name = lookup map name in
      let args = List.map positional_args ~f:(eval_expr_wrapper map) in
      eval_function_call_wrapped name args 
    | _ ->
      raise (EvaluationException (InvalidListExpr (Typed_parsetree.pp_expr (fun _ _ -> ()) Format.str_formatter expr; Format.flush_str_formatter ())))
  and eval_bool_expr map (value: Typed_parsetree.Substitution.variable_type Typed_parsetree.expr) : bool expr =
    let unwrap_bool_expr expr =
      begin
        match expr with
        | Wrap Expr (Bool, (b: bool)) ->  Expr (Bool, b)
        | Wrap exp -> raise (EvaluationException (InvalidBoolExpr (show_expr () exp)))
      end in
    let expr = eval_expr_wrapper map value in
    unwrap_bool_expr expr
  and eval_int_expr map (value: Typed_parsetree.Substitution.variable_type Typed_parsetree.expr) : int expr =
    let unwrap_int_expr expr =
      begin
        match expr with
        | Wrap Expr (Int, (n: int)) ->  Expr (Int, n)
        | Wrap exp -> raise (EvaluationException (InvalidIntExpr (show_expr () exp)))
      end in
    let expr = eval_expr_wrapper map value in
    unwrap_int_expr expr

  let unify_expr_type  (ty: data_type) (expr: wrapped_expr) : wrapped_expr =
    match ty,expr with
    | (Unary TyFloat, Wrap Expr (Float, v)) -> Wrap (Expr (Float, v))
    | (Unary TyFloat, Wrap Expr (Int, v)) -> Wrap (Expr (Float, Float.of_int v))
    | (Unary TyInt, Wrap Expr (Int, v)) -> Wrap (Expr (Int, v))
    | (Unary TyInt, Wrap Expr (Float, v)) -> Wrap (Expr (Int, Int.of_float v))
    | (TyVec2 _, Wrap Expr (Vec2, v)) -> Wrap (Expr (Vec2, v))
    | (TyVec3 _, Wrap Expr (Vec3, v)) -> Wrap (Expr (Vec3, v))
    | (TyVec4 _, Wrap Expr (Vec4, v)) -> Wrap (Expr (Vec4, v))
    | (TyMat2 _, Wrap Expr (Mat2, v)) -> Wrap (Expr (Mat2, v))
    | (TyMat3 _, Wrap Expr (Mat3, v)) -> Wrap (Expr (Mat3, v))
    | (TyMat4 _, Wrap Expr (Mat4, v)) -> Wrap (Expr (Mat4, v))
    | _ -> raise (EvaluationException (TypeMismatch (show_data_type ty, show_wrapped_expr expr)))

  let eval_expr_alt
      ?ty map (expr: Typed_parsetree.Substitution.variable_type Typed_parsetree.expr) ({f}: _ value_reduction) =
    try
      let expr =
        (match ty with
           Some ty -> unify_expr_type ty
         | None -> fun v -> v) @@
        eval_expr_wrapper map expr in
      match expr with
      | Wrap (Expr (tag, expr)) ->
        f (Independent (Expr (tag, expr)))
    with
      EvaluationException (UnboundVariable "camera_position") 
    | EvaluationException (UnboundVariable "camera_front") 
    | EvaluationException (UnboundVariable "camera_yaw") 
    | EvaluationException (UnboundVariable "camera_pitch") ->
      let func = (fun ctx ->
          let map = record_expr_binding map "camera_position" (Wrap (Expr (Vec3, ctx.camera_position))) in
          let map = record_expr_binding map "camera_front" (Wrap (Expr (Vec3, ctx.camera_front))) in
          let map = record_expr_binding map "camera_yaw" (Wrap (Expr (Float, ctx.camera_yaw))) in
          let map = record_expr_binding map "camera_pitch" (Wrap (Expr (Float, ctx.camera_pitch))) in
          let expr = eval_expr_wrapper map expr in
          expr
        ) in
      f (UnknownContext func)


  let eval_expr
      ?ty map (expr: Typed_parsetree.Substitution.variable_type Typed_parsetree.expr) ({f=g}: 'b expr expr_reduction)  (f: 'b value -> 'c) =
    try
      let expr =
        (match ty with
           Some ty -> unify_expr_type ty
         | None -> fun v -> v) @@
        eval_expr_wrapper map expr in
      match expr with
      | Wrap (Expr (tag, expr)) ->
        f (Independent (g (Expr (tag, expr))))
    with
      EvaluationException (UnboundVariable "camera_position") 
    | EvaluationException (UnboundVariable "camera_front") 
    | EvaluationException (UnboundVariable "camera_yaw") 
    | EvaluationException (UnboundVariable "camera_pitch") ->
      let func = (fun ctx ->
          let map = record_expr_binding map "camera_position" (Wrap (Expr (Vec3, ctx.camera_position))) in
          let map = record_expr_binding map "camera_front" (Wrap (Expr (Vec3, ctx.camera_front))) in
          let map = record_expr_binding map "camera_yaw" (Wrap (Expr (Float, ctx.camera_yaw))) in
          let map = record_expr_binding map "camera_pitch" (Wrap (Expr (Float, ctx.camera_pitch))) in
          let expr = eval_expr_wrapper map expr in
          expr
        ) in
      f (UnknownContext func)

  let eval_int_value map (it: Typed_parsetree.Substitution.variable_type Typed_parsetree.expr) : int value =
    eval_expr map it
      ({f=fun (type a) (v: a expr) ->
           match v with
           | Expr (Int, (n: int)) -> (Expr (Int,n))
           | expr -> raise (EvaluationException (InvalidIntExpr (show_expr () expr))) })
      (fun (v: int value) ->
         match v with
         | Independent (Expr (Int, n)) -> Independent (Expr (Int, n))
         | Context f -> Context f
         | UnknownContext f -> UnknownContext f
         | Independent (expr: 'a expr) -> raise (EvaluationException (InvalidIntExpr (show_expr () expr))))
  let eval_v4_value map (it: Typed_parsetree.Substitution.variable_type Typed_parsetree.expr) : v4 value =
    eval_expr map it
      ({f=fun (type a) (v: a expr) ->
           match v with
           | Expr (Vec4, (n: v4)) -> (Expr (Vec4,n))
           | expr -> raise (EvaluationException (InvalidVec4Expression (show_expr () expr))) })
      (fun (v: v4 value) ->
         match v with
         | Independent (Expr (Vec4, n)) -> Independent (Expr (Vec4, n))
         | Context f -> Context f
         | UnknownContext f -> UnknownContext f
         | Independent (expr: 'a expr) -> raise (EvaluationException (InvalidVec4Expression (show_expr () expr))))
  let eval_v3_value map (it: Typed_parsetree.Substitution.variable_type Typed_parsetree.expr) : v3 value =
    eval_expr map it
      ({f=fun (type a) (v: a expr) ->
           match v with
           | Expr (Vec3, (n: v3)) -> (Expr (Vec3,n))
           | expr -> raise (EvaluationException (InvalidVec3Expression (show_expr () expr))) })
      (fun (v: v3 value) ->
         match v with
         | Independent (Expr (Vec3, n)) -> Independent (Expr (Vec3, n))
         | Context f -> Context f
         | UnknownContext f -> UnknownContext f
         | Independent (expr: 'a expr) -> raise (EvaluationException (InvalidVec3Expression (show_expr () expr))))
  let eval_float_value map (value: Typed_parsetree.Substitution.variable_type Typed_parsetree.expr) : float value =
    eval_expr map value
      ({f=fun (type a) (v: a expr) ->
           match v with
           | Expr (Float, (n: float)) -> (Expr (Float,n))
           | Expr (Int, (n: int)) -> (Expr (Float,Float.of_int n))
           | expr -> raise (EvaluationException (InvalidFloatExpression (show_expr () expr))) })
      (fun (v: float value) ->
         match v with
         | Independent (Expr (Float, n)) -> Independent (Expr (Float, n))
         | Context f -> Context f
         | UnknownContext f -> UnknownContext f
         | Independent (expr: 'a expr) -> raise (EvaluationException (InvalidFloatExpression (show_expr () expr))))
  let eval_light_uniform
      ?ty map (value: Typed_parsetree.Substitution.variable_type Typed_parsetree.expr) ({f=f}: 'b light_expr_reduction) : 'b =
    f (fun (light_pos, ctx) ->
          let map = record_expr_binding map "light_position" (Wrap (Expr (Vec3, light_pos))) in
          let map = record_expr_binding map "camera_position" (Wrap (Expr (Vec3, ctx.camera_position))) in
          let map = record_expr_binding map "camera_front" (Wrap (Expr (Vec3, ctx.camera_front))) in
          let map = record_expr_binding map "camera_yaw" (Wrap (Expr (Float, ctx.camera_yaw))) in
          let map = record_expr_binding map "camera_pitch" (Wrap (Expr (Float, ctx.camera_pitch))) in
          let expr = (match ty with
           Some ty -> unify_expr_type ty
         | None -> fun v -> v) @@ eval_expr_wrapper map value in
          expr
      )


  type temp =
    Typed_parsetree.Substitution.variable_type Typed_parsetree.drawable
  [@@deriving show]

  type temp2 =
    [ `LightVolume of
        string * (light * light_uniform list) list * light_uniform list
    | `Plain of modifiers * data * (string * light_uniform) list ] list
  [@@deriving show]

  type temp3 =
    Typed_parsetree.Substitution.variable_type Typed_parsetree.obj
  [@@deriving show]


  let rec eval_statement ~id map (statement: Typed_parsetree.Substitution.variable_type Typed_parsetree.statement) =
    match statement with
    | Typed_parsetree.Drawable drawable -> (None, eval_toplevel_drawable ~id map drawable)
    | Typed_parsetree.LayeredDrawable (tag, drawable) -> (Some tag, eval_toplevel_drawable ~id map drawable)
  and eval_shader map shader = (match shader with
      | Typed_parsetree.ShaderReference reference ->
        raise (ConversionException (UnresolvedReference reference))
      | Typed_parsetree.Shader { vertex_shader; fragment_shader; geometry_shader; shader_spec; typ } ->
        let mapping = match typ with
          | Typed_parsetree.Substitution.Internal
              (TyScene (TyShader
                          (TyDirect MkTyMapping Base
                             (Base TyDirect attr, Base TyDirect unif, Base TyDirect text), _outcount, _)))
          | Typed_parsetree.Substitution.Scene
              (TyShader (TyDirect MkTyMapping Base
                           (Base TyDirect attr, Base TyDirect unif, Base TyDirect text), _outcount, _)) ->
            let of_pdt f attr =
              Typed_parsetree.IdentifierTable.MMap.to_alist attr
              |> List.map ~f:(fun (id, pdt) ->
                  let id = lookup map id in
                  let pdt = match pdt with
                    | Typed_parsetree.Type.TyDirect pdt -> f pdt
                    | Typed_parsetree.Type.TyVariable var ->
                      raise (ConversionException (InvalidTypeVariable var)) in
                  id,pdt) |> Map.of_alist_exn in
            let attr = of_pdt data_of_pure_data_type attr in
            let unif = of_pdt data_of_pure_data_type unif in
            let text = of_pdt data_of_texture_type text in
            let shader = Shader {
                vertex_shader; fragment_shader; geometry_shader; shader_spec;
              } in
            (shader, {attributes=attr; uniforms=unif; textures=text})
          | _ -> raise (ConversionException (InvalidShaderType (typ))) in
        mapping
    )
  and eval_modifier ~id (shader_mapping: shader_map) map modifier : _ * (string * light * light_uniform list) list option =
    match modifier with
    | Typed_parsetree.Texture (name, textures, topts, _) ->
      let name = lookup map name in
      let ty = Map.find shader_mapping.textures name in
      let texture_options = List.map ~f:(eval_texture_option map) topts in
      begin match ty with
        | None -> None, None
        | Some typ -> Some (`Texture {name; textures; texture_options; typ}), None
      end
    | Typed_parsetree.Uniform (name, value, _) ->
      let name = lookup map name in
      let ty = Map.find shader_mapping.uniforms name in
      let result =
        eval_expr_alt ?ty map value
          {f=(function
               | Independent expr -> (Uniform {name; value=Independent expr})
               | Context f ->
                 Uniform {name; value=Context f}
               | UnknownContext f -> Uniform {name; value=UnknownContext f}
          )}
      in
      Some (`Uniform result), None
    | Typed_parsetree.Attribute (name, value, _) ->
      let name = lookup map name in
      let ty = Map.find shader_mapping.attributes name in
      let result = eval_expr_alt ?ty map value
          {f=(function
               | Independent expr -> (Attribute {name; value=expr})
               | UnknownContext f -> 
                 raise (EvaluationException (BadDynamicValue (show_wrapped_expr  (f default_context))))
               | Context f ->
                 raise (EvaluationException (BadDynamicValue (show_expr ()  (f default_context))))
          )} in
      Some (`Attribute result), None
    | Typed_parsetree.Translate (dopt, _) ->
      (Some (`Transform (Translate (eval_dimension_opt map dopt))), None) 
    | Typed_parsetree.Scale (dopt, _) ->
      (Some (`Transform (Scale (eval_dimension_opt map dopt))), None) 
    | Typed_parsetree.Rotate (ropt, _) ->
      (Some (`Transform (Rotate (eval_rotate_opt map ropt))), None)         
    | Typed_parsetree.DrawOptions (dopts, _) ->
      (Some (`DrawOptions (List.map ~f:(eval_draw_opt map) dopts)), None)
    | Typed_parsetree.LightUniform (tag, binding, value, _) ->
      let tag = lookup map tag in
      let binding = lookup map binding in
      let ty = Map.find shader_mapping.uniforms binding in
      begin match ty with
        | None -> None, None
        | Some ty ->
          let result =
            eval_light_uniform ~ty map value {f=(fun value -> LightUniform {
                binding; value;
              })}
          in
          (Some (`LightUniform (tag, result)), None)
      end
    | Typed_parsetree.Redirecting { textures; depth; stencil; drawable; _ } ->
      let binding_names = List.map ~f:(lookup map) textures in
      let drawable, lights = eval_drawable ~id map drawable in
      let id = let tmp = !id in id := !id + 1; tmp in
      Some (`Redirection (Redirection {
          id; binding_names; depth; stencil; drawable;
        })), Some lights
    | Typed_parsetree.ModifierReference reference ->
      raise (ConversionException (UnresolvedReference reference))
  and eval_texture_option map topt = (match topt with
      | Typed_parsetree.TextureBaseLevel tbl -> TextureBaseLevel (eval_int_value map tbl)
      | Typed_parsetree.TextureMaxLevel tbl -> TextureMaxLevel (eval_int_value map tbl)
      | Typed_parsetree.TextureBorderColor tbl -> TextureBorderColor (eval_v4_value map tbl)
      | Typed_parsetree.WrapS wopt -> WrapS wopt
      | Typed_parsetree.WrapT wopt -> WrapT wopt
      | Typed_parsetree.MinFilter fopt -> MinFilter fopt
      | Typed_parsetree.MagFilter fopt -> MagFilter fopt)
  and eval_draw_opt map it =
    match it with
    | Typed_parsetree.AlphaTest value -> let value = eval_bool_expr map value in AlphaTest value
    | Typed_parsetree.BlendTest value -> let value = eval_bool_expr map value in BlendTest value
    | Typed_parsetree.DepthTest value -> let value = eval_bool_expr map value in DepthTest value
    | Typed_parsetree.StencilTest value -> let value = eval_bool_expr map value in StencilTest value
    | Typed_parsetree.BlendColor Typed_parsetree.ColourVector value -> let value = eval_v4_value map value in BlendColor value
    | Typed_parsetree.BlendFunction (bf1, bf2) -> BlendFunction (bf1, bf2)
    | Typed_parsetree.StencilFunction (cf, i1, i2) ->
      let i1 = eval_int_value map i1 in let i2 = eval_int_value map i2 in StencilFunction (cf, i1, i2)
    | Typed_parsetree.DepthFunction cf -> DepthFunction cf
    | Typed_parsetree.AlphaFunction (cf, value) -> let value = eval_float_value map value in AlphaFunction (cf, value)      
  and eval_dimension_opt map it : v3 value =
    match it with
    | Typed_parsetree.DimensionVector exp -> eval_v3_value map exp
    | Typed_parsetree.DimensionXZY { x; y; z } ->
      let get vl : float value = match vl with
          Some vl -> eval_float_value map vl
        | None -> Independent (Expr (Float, 0.)) in
      match (get x: float value), (get y: float value), (get z: float value)  with
      | (Independent (Expr (Float, v1)), Independent (Expr (Float, v2)), Independent  (Expr (Float, v3))) ->
        Independent  (Expr (Vec3, (V3.v v1 v2 v3)))
      | (Context f, Independent  (Expr (Float, v2)), Independent  (Expr (Float, v3))) ->
        Context (fun ctx -> let v1 = match f ctx with Expr (Float, v1) -> v1 | _ -> assert false in (Expr (Vec3, V3.v v1 v2 v3)))
      | (UnknownContext f, Independent  (Expr (Float, v2)), Independent  (Expr (Float, v3))) ->
        Context (fun ctx -> let v1 = match f ctx with Wrap Expr (Float, (v1: float)) -> v1 | _ -> assert false in (Expr (Vec3, V3.v v1 v2 v3)))
      | (Independent  (Expr (Float, v1)), Context f2, Independent  (Expr (Float, v3))) ->
        Context (fun ctx ->
            let v2 = match f2 ctx with | Expr (Float, v) -> v | _ -> assert false in
            Expr (Vec3, V3.v v1 v2 v3))
      | (Independent  (Expr (Float, v1)), UnknownContext f2, Independent  (Expr (Float, v3))) ->
        Context (fun ctx ->
            let v2 = match f2 ctx with | Wrap Expr (Float, (v: float)) -> v   | expr -> raise (EvaluationException (InvalidFloatExpression (show_wrapped_expr expr))) in
            Expr (Vec3, V3.v v1 v2 v3))
      | (Independent  (Expr (Float, v1)), Independent  (Expr (Float, v2)), Context f3) ->
        Context (fun ctx ->
            let v3 = match f3 ctx with | Expr (Float, v) -> v | _ -> assert false in
            Expr (Vec3, V3.v v1 v2 v3))
      | (Independent  (Expr (Float, v1)), Independent  (Expr (Float, v2)), UnknownContext f3) ->
        Context (fun ctx ->
            let v3 = match f3 ctx with | Wrap Expr (Float, (v: float)) -> v   | expr -> raise (EvaluationException (InvalidFloatExpression (show_wrapped_expr expr))) in
            Expr (Vec3, V3.v v1 v2 v3))
      | (Context f1, Context f2, Independent  (Expr (Float, v3))) ->
        Context (fun ctx ->
            let v1 = match f1 ctx with | Expr (Float, v) -> v | _ -> assert false in
            let v2 = match f2 ctx with | Expr (Float, v) -> v | _ -> assert false in
            Expr (Vec3, V3.v v1 v2 v3))

      | (UnknownContext f1, Context f2, Independent  (Expr (Float, v3))) ->
        Context (fun ctx ->
            let v1 = match f1 ctx with | Wrap Expr (Float, (v: float)) -> v   | expr -> raise (EvaluationException (InvalidFloatExpression (show_wrapped_expr expr))) in
            let v2 = match f2 ctx with | Expr (Float, v) -> v | _ -> assert false in
            Expr (Vec3, V3.v v1 v2 v3))

        | (Context f1, UnknownContext f2, Independent  (Expr (Float, v3))) ->
        Context (fun ctx ->
            let v1 = match f1 ctx with | Expr (Float, v) -> v | _ -> assert false in
            let v2 = match f2 ctx with | Wrap Expr (Float, (v: float)) -> v   | expr -> raise (EvaluationException (InvalidFloatExpression (show_wrapped_expr expr))) in
            Expr (Vec3, V3.v v1 v2 v3))

        | (UnknownContext f1, UnknownContext f2, Independent  (Expr (Float, v3))) ->
        Context (fun ctx ->
            let v1 = match f1 ctx with | Wrap Expr (Float, (v: float)) -> v   | expr -> raise (EvaluationException (InvalidFloatExpression (show_wrapped_expr expr))) in
            let v2 = match f2 ctx with | Wrap Expr (Float, (v: float)) -> v   | expr -> raise (EvaluationException (InvalidFloatExpression (show_wrapped_expr expr))) in
            Expr (Vec3, V3.v v1 v2 v3))
  
      | (Independent  (Expr (Float, v1)), Context f2, Context f3) ->
        Context (fun ctx ->
            let v2 = match f2 ctx with | Expr (Float, v) -> v | _ -> assert false in
            let v3 = match f3 ctx with | Expr (Float, v) -> v | _ -> assert false in
            Expr (Vec3, V3.v v1 v2 v3))

      | (Independent  (Expr (Float, v1)), UnknownContext f2, Context f3) ->
        Context (fun ctx ->
            let v2 = match f2 ctx with | Wrap Expr (Float, (v: float)) -> v   | expr -> raise (EvaluationException (InvalidFloatExpression (show_wrapped_expr expr))) in
            let v3 = match f3 ctx with | Expr (Float, v) -> v | _ -> assert false in
            Expr (Vec3, V3.v v1 v2 v3))
  
      | (Independent  (Expr (Float, v1)), Context f2, UnknownContext f3) ->
        Context (fun ctx ->
            let v2 = match f2 ctx with | Expr (Float, v) -> v | _ -> assert false in
            let v3 = match f3 ctx with | Wrap Expr (Float, (v: float)) -> v   | expr -> raise (EvaluationException (InvalidFloatExpression (show_wrapped_expr expr))) in
            Expr (Vec3, V3.v v1 v2 v3))

      | (Independent  (Expr (Float, v1)), UnknownContext f2, UnknownContext f3) ->
        Context (fun ctx ->
            let v2 = match f2 ctx with | Wrap Expr (Float, (v: float)) -> v   | expr -> raise (EvaluationException (InvalidFloatExpression (show_wrapped_expr expr))) in
            let v3 = match f3 ctx with | Wrap Expr (Float, (v: float)) -> v   | expr -> raise (EvaluationException (InvalidFloatExpression (show_wrapped_expr expr))) in
            Expr (Vec3, V3.v v1 v2 v3))

      | (Context f1, Independent  (Expr (Float, v2)), Context f3) ->
        Context (fun ctx ->
            let v1 = match f1 ctx with | Expr (Float, v) -> v | _ -> assert false in
            let v3 = match f3 ctx with | Expr (Float, v) -> v | _ -> assert false in
            Expr (Vec3, V3.v v1 v2 v3))

      | (UnknownContext f1, Independent  (Expr (Float, v2)), Context f3) ->
        Context (fun ctx ->
            let v1 = match f1 ctx with | Wrap Expr (Float, (v: float)) -> v   | expr -> raise (EvaluationException (InvalidFloatExpression (show_wrapped_expr expr))) in
            let v3 = match f3 ctx with | Expr (Float, v) -> v | _ -> assert false in
            Expr (Vec3, V3.v v1 v2 v3))

      | (Context f1, Independent  (Expr (Float, v2)), UnknownContext f3) ->
        Context (fun ctx ->
            let v1 = match f1 ctx with | Expr (Float, v) -> v | _ -> assert false in
            let v3 = match f3 ctx with | Wrap Expr (Float, (v: float)) -> v  | expr -> raise (EvaluationException (InvalidFloatExpression (show_wrapped_expr expr))) in
            Expr (Vec3, V3.v v1 v2 v3))

      | (UnknownContext f1, Independent  (Expr (Float, v2)), UnknownContext f3) ->
        Context (fun ctx ->
            let v1 = match f1 ctx with | Wrap Expr (Float, (v: float)) -> v  | expr -> raise (EvaluationException (InvalidFloatExpression (show_wrapped_expr expr))) in
            let v3 = match f3 ctx with | Wrap Expr (Float, (v: float)) -> v | expr -> raise (EvaluationException (InvalidFloatExpression (show_wrapped_expr expr))) in
            Expr (Vec3, V3.v v1 v2 v3))
  
      | (Context f1, Context f2, Context f3) ->
        Context (fun ctx ->
            let v1 = match f1 ctx with | Expr (Float, v) -> v | _ -> assert false in
            let v2 = match f2 ctx with | Expr (Float, v) -> v | _ -> assert false in
            let v3 = match f3 ctx with | Expr (Float, v) -> v | _ -> assert false in
            Expr (Vec3, V3.v v1 v2 v3))

      | ((UnknownContext _ as f1), (UnknownContext _ as f2), (UnknownContext _ as f3))
      | ((Context _ as f1), (UnknownContext _ as f2), (UnknownContext _ as f3))
      | ((UnknownContext _ as f1), (Context _ as f2), (UnknownContext _ as f3))
      | ((UnknownContext _ as f1), (UnknownContext _ as f2), (Context _ as f3))
      | ((Context _ as f1), (Context _ as f2), (UnknownContext _ as f3)) 
      | ((Context _ as f1), (UnknownContext _ as f2), (Context _ as f3)) 
      | ((UnknownContext _ as f1), (Context _ as f2), (Context _ as f3))  ->
        Context (fun ctx ->
            let unwrap f1 = match f1 with
              | Context f1 -> begin match f1 ctx with Expr (Float, v) -> v | _ -> assert false end
              | UnknownContext f1 -> begin match f1 ctx with Wrap Expr (Float, (v:float)) -> v | expr -> raise (EvaluationException (InvalidFloatExpression (show_wrapped_expr expr))) end
              | _ -> assert false
            in
            let v1 = unwrap f1 in
            let v2 = unwrap f2 in
            let v3 = unwrap f3 in
            Expr (Vec3, V3.v v1 v2 v3))
  
      | _ -> assert false
  and eval_rotate_opt map (Typed_parsetree.RotateAngle { x; y; z; vector }) =
    let get vl : float value = match vl with
        Some vl -> eval_float_value map vl
      | None -> Independent  (Expr (Float, 0.)) in
    let getv vl : v3 value option = match vl with
        Some vl -> Some (eval_v3_value map vl)
      | None -> None in
    let construct ?vec x y z : m4 =
      let to_rad x = (x /. 360.) *. (2. *. Float.pi) in
      match vec with
      | Some vec -> (M4.rot3_axis (V3.map to_rad vec) x)
      | None -> (M4.rot3_zyx (V3.v (to_rad x) (to_rad y) (to_rad z))) in
    match get x, get y, get z, getv vector with
    | (Independent  (Expr (Float, x)), Independent  (Expr (Float, y)), Independent  (Expr (Float, z)), Some Independent  (Expr (Vec3, vec))) ->
      Independent  (Expr (Mat4, (construct ~vec x y z)))
    | (Independent  (Expr (Float, x)), Independent  (Expr (Float, y)), Independent  (Expr (Float, z)), None) ->
      Independent  (Expr (Mat4, (construct x y z)))
    | (x, y, z, vec) ->
      Context (fun ctx ->
          let unwrap vl = match (vl: float value) with
            | Independent  (Expr (Float, vl)) -> vl
            | Context f -> begin match f ctx with Expr (Float, v1) -> v1 | _ -> assert false end
            | UnknownContext f -> begin match f ctx with Wrap Expr (Float, v1) -> v1 | expr -> raise (EvaluationException (InvalidFloatExpression (show_wrapped_expr expr))) end
            | _ -> assert false
          in
          let vec = match vec with
            | None -> None
            | Some v ->
              match v with
              | Independent  (Expr (Vec3, v)) -> Some v
              | UnknownContext f -> begin match f ctx with Wrap Expr (Vec3, v1) -> Some v1 | expr ->
                  raise (EvaluationException (InvalidVec3Expression (show_wrapped_expr expr))) end
              | Context f -> Some (match f ctx with Expr (Vec3, v1) -> v1 | _ -> assert false)
              | _ -> assert false
          in
          Expr (Mat4, construct ?vec (unwrap x) (unwrap y) (unwrap z))
        )
  and eval_data (shader_mapping: shader_map) map data =
    match data with
    | Typed_parsetree.Model (name, _) -> Model (name)
    | Typed_parsetree.Data (bindings, _) ->
      let bindings = List.filter_map bindings ~f:(fun (name, value) ->
          let name = lookup map name in
          let typ = Map.find shader_mapping.attributes name in
          match typ with
          | None -> None
          | Some ty ->
            eval_expr_alt ~ty map value {f=(fun value ->
                match value with
                | Independent value -> Some (name, DataElem value)
                | _ -> raise (ConversionException (UnexpectedDynamicExpression name))
              )}
        ) in
      Data bindings
    | Typed_parsetree.Screen { width; height; _ } ->
      let unwrap value = match value with
        | None -> None
        | Some value ->
          match eval_float_value map value with
          | Independent  (Expr (Float, value)) -> Some  (Expr (Float, value))
          | _ -> raise (ConversionException (UnexpectedDynamicExpression "width")) in
      let width = unwrap width in
      let height = unwrap height in
      Screen {  width; height  }
  and union_modifiers
      { transforms=t1; uniforms=u1; attributes=at1; draw_options=d1; textures=tt1; redirection=r1 }
      { transforms=t2; uniforms=u2; attributes=at2; draw_options=d2; textures=tt2; redirection=r2 } =
    { transforms=t1 @ t2; uniforms=u1 @ u2; attributes=at1 @ at2; draw_options=d1 @ d2; textures=tt1 @ tt2; redirection=r1 @ r2 }
  and eval_obj_expr ~id:(id_: int ref) shader_mapping map value = match value with
    | Typed_parsetree.PlainObject obj -> [eval_obj ~id:id_ shader_mapping map obj]
    | Typed_parsetree.ObjectIfThenElse (cond, e1, e2, _) ->
      let cond = eval_bool_expr map cond in
      let expr = match cond with | Expr (Bool, true) -> Some e1 | Expr (Bool, false) -> e2 | _ -> assert false in
      begin match expr with
        | None -> []
        | Some expr -> eval_obj_expr ~id:id_ shader_mapping map expr
      end
    | Typed_parsetree.ObjectFor ((id, frome, toe, body), _) ->
      let id = lookup map id in
      match toe with
      | None ->
        let ine = eval_expr_list map frome in
        List.concat_map ine ~f:(fun value ->
            let map = record_expr_binding map id value in
            List.concat_map body ~f:(eval_obj_expr ~id:id_ shader_mapping map)
          )
      | Some toe ->
        let unwrap (x: int expr) = match x with Expr (Int, v) -> v | _ -> assert false in
        let frome = unwrap @@ eval_int_expr map frome in
        let toe = unwrap @@ eval_int_expr map toe in
        List.range ~start:`inclusive ~stop:`exclusive frome toe
        |> List.concat_map ~f:(fun value ->
            let value = Wrap (Expr (Int, value)) in
            let map = record_expr_binding map id value in
            List.concat_map body ~f:(eval_obj_expr ~id:id_ shader_mapping map)
          )
  and filter_lights tag modifiers lights =
    let lights =
      List.filter_map lights ~f:(fun (name, l, lu) ->
          if String.(name = tag) then Some (l,lu) else None)
      |> List.map ~f:(fun (Light { modifiers=m'; position; size }, lu) ->
          (Light { modifiers=union_modifiers modifiers m'; position; size },lu))
    in
    lights
  and eval_base_object ~(id: int ref) (shader_mapping: shader_map) map it =
    match it with
    | Typed_parsetree.Light { name; position; size; _ } ->
      let tag = lookup map name in
      let position = eval_v3_value map position in
      let size = eval_float_value map size in
      `Light (tag, (position, size))
    | Typed_parsetree.LightData { light_tag; _ } ->
      let tag = lookup map light_tag in
      (* let binding = lookup map value in *)
      `LightVolume (tag)
    | Typed_parsetree.ObjectData data -> `Data (eval_data shader_mapping map data)
    | Typed_parsetree.CompoundObject ((h, t), _) ->
      let body = List.concat_map ~f:(eval_obj_expr ~id shader_mapping map) (h::t) |> List.unzip
                 |> (fun (a,b) -> List.concat a, List.concat b) in
      `Multi body
    | Typed_parsetree.ObjectReference reference -> raise (ConversionException (UnresolvedReference reference))
  and eval_toplevel_drawable ~id map drawable =
    fst @@ eval_drawable ~id map drawable
  and eval_obj ~id shader_mapping map (Typed_parsetree.Object (modifiers, bobj, _))  =
    let modifiers, lights = List.map modifiers ~f:(eval_modifier ~id shader_mapping map)
                            |> List.unzip |> (fun (a,b) -> List.filter_opt a, List.filter_opt b |> List.concat) in
    let (transforms, uniforms, attributes, dopts, txts, redirs), light_uniforms =
      List.fold_map modifiers ~init:([],[],[],[],[],[]) ~f:(fun (transforms, uniforms, attributes, dopts, txts, redirs) v ->
          match v with
          | `Attribute attribute ->
            let attributes = attribute :: attributes in
            (transforms, uniforms, attributes, dopts, txts, redirs), None
          | `DrawOptions draw_options ->
            let dopts = List.rev draw_options @ dopts in
            (transforms, uniforms, attributes, dopts, txts, redirs), None
          | `LightUniform light_uniform ->
            (transforms, uniforms, attributes, dopts, txts, redirs), Some light_uniform
          | `Redirection redirection ->
            let redirs = redirection :: redirs in
            (transforms, uniforms, attributes, dopts, txts, redirs), None
          | `Texture texture ->
            let txts = texture :: txts in
            (transforms, uniforms, attributes, dopts, txts, redirs), None
          | `Transform transform ->
            let transforms = transform :: transforms in
            (transforms, uniforms, attributes, dopts, txts, redirs), None
          | `Uniform uniform  ->
            let uniforms = uniform :: uniforms in
            (transforms, uniforms, attributes, dopts, txts, redirs), None
        ) |> (fun (a,b) -> a, List.filter_opt b) in
    let elems = eval_base_object ~id shader_mapping map bobj in
    match elems with
    (*
       redirecting into=[model_texture]
          light (..)
       light(...) *)
    | `Light (tag, (position, size)) ->
      let (transforms, uniforms, attributes, dopts, txts, redirs) =
        (List.rev transforms, List.rev uniforms, List.rev attributes, List.rev dopts, List.rev txts, List.rev redirs)
      in
      [], lights @ [tag, Light {
          modifiers={
            transforms; uniforms; attributes; draw_options=dopts; textures=txts;
            redirection=redirs;
          }; position; size;
        }, List.filter_map light_uniforms ~f:(fun (name, vl) -> if String.(name=tag) then Some vl else None)]
    | `Data data ->
      let (transforms, uniforms, attributes, dopts, txts, redirs) =
        (List.rev transforms, List.rev uniforms, List.rev attributes, List.rev dopts, List.rev txts, List.rev redirs)
      in
      [
        `Plain ({
            transforms; uniforms; attributes; draw_options=dopts; textures=txts;
            redirection=redirs;
          }, data, light_uniforms)
      ], lights
    | `LightVolume tag ->
      let (transforms, uniforms, attributes, dopts, txts, redirs) =
        (List.rev transforms, List.rev uniforms, List.rev attributes, List.rev dopts, List.rev txts, List.rev redirs)
      in
      [`LightVolume (tag, filter_lights tag {
           transforms; uniforms; attributes; draw_options=dopts; textures=txts;
           redirection=redirs;
         } lights,
                     List.filter_map light_uniforms ~f:(fun (name, vl) -> if String.(name=tag) then Some vl else None)
         )], lights
    | `Multi (data, more_lights) ->
      let (transforms, uniforms, attributes, dopts, txts, redirs) =
        (List.rev transforms, List.rev uniforms, List.rev attributes, List.rev dopts, List.rev txts, List.rev redirs) in
      let modifiers = {
        transforms; uniforms; attributes; draw_options=dopts; textures=txts;
        redirection=redirs;
      } in
      let data = List.map data ~f:(fun v ->
          match v with
          | `LightVolume (tag, more_lights, more_light_uniforms) ->
            `LightVolume (tag, 
                          List.filter_map lights
                            ~f:(fun (name,l,lu) ->
                                if String.(name = tag) then Some (l,lu) else None)
                          @ more_lights,
                          List.filter_map light_uniforms
                            ~f:(fun (name, vl) -> if String.(name=tag) then Some vl else None)
                          @ more_light_uniforms)
          | `Plain (new_modifiers, data, more_light_uniforms)  ->
            `Plain (union_modifiers modifiers new_modifiers, data, light_uniforms @ more_light_uniforms)
        ) in
      let more_lights =
        List.map more_lights ~f:(fun (tag, Light { modifiers=old_modifiers; position; size }, light_uniforms) ->
            (tag, Light { modifiers = union_modifiers old_modifiers modifiers ; position; size}, light_uniforms)
        ) in
      let lights = lights @ more_lights in
      data, lights
  and eval_drawable ~id map it =
    match it with
    | Typed_parsetree.DrawableShader (shader, obj, _) -> begin
        let shader, shader_map = eval_shader map shader in
        let objs, lights = eval_obj ~id shader_map map obj in
        let objs = match objs with
          | [] -> raise (ConversionException (EmptyCompoundObject))
          | (`Plain (_, _, []) :: _) as ls ->
            `Basic (List.map ls ~f:(fun v ->
                match v with
                | `Plain (modifiers, data, []) -> (modifiers, data)
                | _ -> raise (ConversionException (InconsistentMulti)) 
              ))
          | _ -> raise (ConversionException (UnexpectedMulti))
        in
        match objs with
        | `Basic objects -> BasicDrawable { shader; objects}, lights
      end
    | Typed_parsetree.OverlayingDrawable (Typed_parsetree.DrawableShader (shader, obj, _), _) ->
      begin
        let shader, shader_map = eval_shader map shader in
        let objs, lights = eval_obj ~id shader_map map obj in
        let objs = match objs with
          | [] -> raise (ConversionException (EmptyCompoundObject))
          | ((`Plain (_, _, (tag, _) :: _ )) :: _) as ls ->
            `LightObjects (tag, List.map ls ~f:(fun v ->
                match v with
                | `Plain (modifiers, data, ((_ :: _) as light_uniforms)) ->
                  (List.filter_map
                     light_uniforms
                     ~f:(fun (name, vl) -> if String.(tag = name) then Some vl else None)
                  , (modifiers, data))
                | _ -> raise (ConversionException (InconsistentMulti))
              ))
          | ((`LightVolume (tag, _, _)) :: _) as ls ->
            `LightVolume (tag,  List.map ls ~f:(fun v ->
                match v with
                | `LightVolume (name,  lights, light_uniforms)
                  when String.(tag = name) -> (light_uniforms, lights)
                | _ -> raise (ConversionException (InconsistentMulti))
              ) |> List.unzip |> (fun (a,b) -> List.concat a, List.concat b))
          | ls  -> begin
            match List.find_map ls ~f:(fun v ->
                match v with
                | `Plain (_, _, (tag, _) :: _) -> Some tag
                | `LightVolume (tag, _, _) -> Some tag
                | _ -> None) with
            | None -> raise (ConversionException (InconsistentMulti))
            | Some tag ->
              `LightObjects (tag, List.map ls ~f:(fun v ->
                  match v with
                  | `Plain (modifiers, data, ((_ :: _) as light_uniforms)) ->
                    (List.filter_map
                       light_uniforms
                       ~f:(fun (name, vl) -> if String.(tag = name) then Some vl else None)
                    , (modifiers, data))
                  | _ -> raise (ConversionException (InconsistentMulti))
                ))
            end
        in
        match objs with
        | `LightObjects (tag, objects) ->
          let lights', _ = List.filter_map lights ~f:(fun (name, light,uniforms) ->
              if String.(tag = name) then Some (light, uniforms) else None
            ) |> List.unzip |> (fun (a,b) -> a, List.concat b) in
          LightDrawable {
            shader; lights=lights';
            light_data=LightObjects objects;
          }, lights
        | `LightVolume (_, (light_uniforms, lights')) ->
          let other_lights, lu = lights' |> List.unzip |> (fun (a,b) -> a, List.concat b) in
          let uniforms = light_uniforms @ lu in
          LightDrawable {
            shader; lights=other_lights;
            light_data=LightVolume {
                uniforms;
              }
          }, lights
      end
    | Typed_parsetree.DrawableReference reference ->
      raise (ConversionException (UnresolvedReference reference))
    | _ -> assert false
  let eval_program tbl program =
    let id = ref 0 in
    List.map ~f:(eval_statement ~id (tbl, Map.empty)) program

end
