open Core
open Lexing 


let pp_position
    (fmt: Format.formatter) ({
        pos_fname;
        pos_lnum;
        pos_bol;
        pos_cnum }:position) : unit =
  Format.pp_print_string fmt "Lexing.position";
  Format.pp_print_space fmt ();
  Format.pp_print_string fmt "{";
  Format.pp_print_string fmt "pos_fname";
  Format.pp_print_string fmt "=";
  Format.pp_print_string fmt pos_fname;
  Format.pp_print_string fmt ";";
  Format.pp_print_space fmt ();
  Format.pp_print_string fmt "pos_lnum";
  Format.pp_print_string fmt "=";
  Format.pp_print_int fmt pos_lnum;
  Format.pp_print_string fmt ";";
  Format.pp_print_space fmt ();
  Format.pp_print_string fmt "pos_bol";
  Format.pp_print_string fmt "=";
  Format.pp_print_int fmt pos_bol;
  Format.pp_print_string fmt ";";
  Format.pp_print_space fmt ();
  Format.pp_print_string fmt "pos_cnum";
  Format.pp_print_string fmt "=";
  Format.pp_print_int fmt pos_cnum;
  Format.pp_print_space fmt ();
  Format.pp_print_string fmt "}"

type position = Lexing.position = {
  pos_fname : string;
  pos_lnum : int;
  pos_bol : int;
  pos_cnum : int;
} [@@deriving sexp,hash]

type t = { loc_start: position; loc_end: position; }
[@@deriving show, sexp,hash]


type 'a loc = { value: 'a; loc: t }
[@@deriving show, sexp, hash]


let equal (_a:t) (_b: t) = true

let equal_loc eq (a: 'a loc) (b: 'b loc)  = eq a.value b.value

let compare (_a:t) (_b:t) = 0


let compare_loc f (a: 'a loc) (b: 'a loc) =
  f a.value b.value

let map_loc ~f {value;loc} = {value =f value; loc}

let t_of_sexp (s: Sexp.t) : t =
  Unit.t_of_sexp s;
  {loc_start=Lexing.{pos_fname=""; pos_lnum=0;pos_cnum=0;pos_bol=0};
   loc_end=Lexing.{pos_fname=""; pos_lnum=0;pos_cnum=0;pos_bol=0} }


let sexp_of_t (_t: t) = Unit.sexp_of_t ()

let loc_of_sexp f (s: Sexp.t) : 'a loc =
  {value=f s; loc={loc_start=Lexing.{pos_fname=""; pos_lnum=0;pos_cnum=0;pos_bol=0};
                   loc_end=Lexing.{pos_fname=""; pos_lnum=0;pos_cnum=0;pos_bol=0} }}

let sexp_of_loc f (loc: 'a loc) =
  f loc.value

