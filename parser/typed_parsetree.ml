(* open Location *)
open Core


type 'a non_empty_list = 'a * 'a list
[@@deriving show, sexp, fold, map, iter, eq]

let map_ne_list ~f ((h,t): 'a non_empty_list) = f h, List.map ~f t
let unzip_ne_list  (((h_a, h_b),t): ('a * 'b) non_empty_list) =
  let (t_a, t_b) = List.unzip t in (h_a, t_a), (h_b, t_b)
let concat_ne_list ((h, t): 'a list non_empty_list) : 'a list = h @ List.concat t
let ne_list_to_list (h,t) = h :: t
let concat_nene_list (((h, t1), t2): 'a non_empty_list non_empty_list) : 'a non_empty_list =
  h, t1 @ List.concat (List.map ~f:ne_list_to_list t2)
let cons_ne_list h (h', t) = h, h'::t

let fold_ne_list ~f ~init ((h,t): 'a non_empty_list) =
  let init = f init h in List.fold ~f ~init t
let fold_map_ne_list ~f ~init ((h,t): 'a non_empty_list) =
  let init, h = f init h in
  let init, t = List.fold_map ~f ~init t in
  init, (h,t)
let option_all_ne_list ((h,t): 'a option non_empty_list) : 'a non_empty_list option =
  let t = Option.all t in
  Option.both h t

type 'a loc = 'a Location.loc = { value: 'a; loc: Location.t}
[@@deriving show, sexp]
let equal_loc _ _ _ = true

let default_pos : Lexing.position = Lexing.{
    pos_lnum = (-1);
    pos_bol=(-1);
    pos_cnum=(-1);
    pos_fname=""
  }
let default_loc : Location.t = Location.{
    loc_start = default_pos;
    loc_end = default_pos;
  }

let location_min ({ pos_fname; pos_lnum=xl; pos_bol=xbol; pos_cnum=xcnum } as x: Lexing.position)
    ({  pos_fname=yname; pos_lnum=yl; pos_bol=ybol; pos_cnum=ycnum; _ } as y: Lexing.position) =
  let lt a b = match a,b with
    | -1,-1 -> `Unk
    | _,  -1 -> `Left
    | -1, _ -> `Right
    | a, b when a < b -> `Left
    | a,b  when b > a -> `Right
    | _ -> `Eq in
  let pos_fname = match String.is_empty pos_fname, String.is_empty yname with
    | true, true -> pos_fname
    | false, true -> pos_fname
    | true, false -> yname
    | _ -> pos_fname in
  match lt xl yl with
  | `Unk -> {x with pos_fname}
  | `Left -> {x with pos_fname}
  | `Right -> {y with pos_fname}
  | `Eq ->
    let pos_lnum = xl in
    let pos_bol, pos_cnum = if xcnum < ycnum then xbol, xcnum else ybol, ycnum in
    {pos_lnum; pos_bol; pos_cnum; pos_fname}

let location_max ({ pos_fname; pos_lnum=xl; pos_bol=xbol; pos_cnum=xcnum } as x: Lexing.position)
    ({  pos_fname=yname; pos_lnum=yl; pos_bol=ybol; pos_cnum=ycnum; _ } as y: Lexing.position) =
  let gt a b = match a,b with
    | -1,-1 -> `Unk
    | _,  -1 -> `Left
    | -1, _ -> `Right
    | a, b when a > b -> `Left
    | a,b  when b < a -> `Right
    | _ -> `Eq in
  let pos_fname = match String.is_empty pos_fname, String.is_empty yname with
    | true, true -> pos_fname
    | false, true -> pos_fname
    | true, false -> yname
    | _ -> pos_fname in
  match gt xl yl with
  | `Unk -> {x with pos_fname}
  | `Left -> {x with pos_fname}
  | `Right -> {y with pos_fname}
  | `Eq ->
    let pos_lnum = xl in
    let pos_bol, pos_cnum = if xcnum > ycnum then xbol, xcnum else ybol, ycnum in
    {pos_lnum; pos_bol; pos_cnum; pos_fname}

let union_loc ({ loc_start=xs; loc_end=xe; }: Location.t) ({ loc_start=ys; loc_end=ye }: Location.t) =
  let loc_start = List.fold ~init:xs ~f:location_min [xe; ys; ye] in
  let loc_end = List.fold ~init:xs ~f:location_max [xe; ys; ye] in
  Location.{loc_start; loc_end}

let default_fragment_shader = {|
#version 330 core

out vec4 frag_colour;

void main() { frag_colour = vec4(0.0, 0.0, 0.0, 0.0); }
|}

module type EQUIV = sig
  type v

  module Map : Hashtbl.S with type key = v
  module Set : Set.S with type Elt.t = v

end

module EquivalenceClass (Equiv:EQUIV) = struct

  open Equiv
  type t = {
    set_map: Set.t Map.t;
    rev_map: v Map.t;
  }

  let init () = { set_map=Map.create (); rev_map= Map.create ();}

  let get_cannonical_form t a =
    match Map.find t.rev_map a with
    | None ->
      Map.set t.set_map ~key:a ~data:(Set.singleton a);
      Map.set t.rev_map ~key:a ~data:a;
      a
    | Some v -> v

  let set_equal t a b =
    match Map.find t.rev_map a, Map.find t.rev_map b with
    | None, None ->
      Map.set t.rev_map ~key:a ~data:a;
      Map.set t.rev_map ~key:b ~data:a;
      Map.set t.set_map ~key:a ~data:(Set.of_list [a;b]);
      a
    | Some base, None
    | None, Some base ->
      let base_set = Map.find_exn t.set_map base  in
      Map.set t.set_map ~key:base ~data:(Set.union base_set (Set.of_list [a;b]));
      Map.set t.rev_map ~key:a ~data:base;
      Map.set t.rev_map ~key:b ~data:base;
      base
    | Some a_cannonical, Some b_cannonical ->
      let a_set = Map.find_exn t.set_map a_cannonical in
      let b_set = Map.find_exn t.set_map b_cannonical in
      Map.remove t.set_map b_cannonical;
      Map.set t.set_map ~key:a_cannonical ~data:(Set.union a_set b_set);
      Set.iter b_set ~f:(fun elem ->
          Map.set t.rev_map ~key:elem ~data:a_cannonical;
        );
      a_cannonical

end



module TopologicalSort = struct


  module type S = sig

    (*  type of element to be sorted *)
    type t
    (*  type of dependencies *)
    type dep
    (* we just need sets *)
    module Set : Set.S with type Elt.t = dep

    val compare: t -> t -> int
    val show: t -> string
    val show_dep: dep -> string

    val dependencies : t -> Set.t
    val exports : t -> Set.t

  end

  module Make (Src: S) = struct

    open Src

    type sort_error =
      | Stalled of t list

    exception SortingException of sort_error

    let sort ?(base_supplies=Set.empty) (ls: t list) =
      let sort_remaining  (supplies, sorted, remaining) =
        let (supplies, new_layer, any_changes), remaining  =
          List.fold_map remaining
            ~init:(supplies, [], false) ~f:(fun (supplies, acc, found_any) (t, deps, exps) ->
                if (Set.is_subset deps ~of_:supplies)
                then (Set.union exps supplies, t :: acc, true), None
                else (supplies, acc, found_any), Some (t, deps, exps)
              ) in
        let remaining = List.filter_opt remaining in

        let sorted = List.sort ~compare:(fun a b -> compare a b) new_layer @ sorted in
        (* let () = List.map ~f:show new_layer |> String.concat ~sep:"\n" |> Printf.printf "layer:[%s]\n" in *)

        if not any_changes then begin
          let unbound_variables = List.fold remaining ~init:Set.empty ~f:(fun set (_, deps, exps) ->
              Set.diff (Set.union deps set) exps) in
          if Set.is_empty (Set.diff unbound_variables supplies) then begin
            raise (SortingException (Stalled ((List.map ~f:(fun (t, _,_) -> t) remaining) @ sorted)))
          end  else begin
            (Set.union supplies unbound_variables, sorted, remaining)
          end;
        end
        else (supplies, sorted, remaining) in
      (* first preprocess list into roots and heads *)
      let roots, remaining = List.map ls ~f:(fun t ->
          let deps = dependencies t in
          let exps = exports t in
          match Set.is_empty deps  with
          | true -> (Some (t, exps), None)
          | false -> (None, Some (t, deps, exps))
        ) |> List.unzip |> (fun (a,b) ->
          List.filter_opt a |> List.sort ~compare:(fun (a,_) (b,_) -> compare a b),
          List.filter_opt b) in

      let base_supplies, roots =
        List.fold_map roots ~init:base_supplies ~f:(fun set (t, exps) -> Set.union set exps, t) in

      let rec loop (supplies, sorted, remaining) =
        match remaining with
        | _ :: _ ->
          let res = try `Ok (sort_remaining (supplies, sorted, remaining)) with
              SortingException (Stalled list) -> `Failed list in
          begin match res with
            | `Ok v -> loop v
            | `Failed ls -> List.rev ls
          end
        | [] -> 
          let res = List.rev sorted in
          (* let () =
           *   List.map sorted ~f:show |> String.concat ~sep:"\n"
           *   |> Printf.printf "pre_sorting: [%s]\n" in
           * let () =
           *   List.map res ~f:show |> String.concat ~sep:"\n"
           *   |> Printf.printf "post_sorting: [%s]\n" in *)
          res

      in

      loop (base_supplies, roots, remaining)

  end

end


module IdentifierTable = Sanitized_parsetree.IdentifierTable
module R =  Sanitized_parsetree.Raw
module S =  Sanitized_parsetree.Output
module IDMap = IdentifierTable.MMap
module IDMutMap = IdentifierTable.HMap

module TypeVariable : sig 
  type src
  type t

  module HMap : Hashtbl.S with type key = t

  module Map : Map.S with type Key.t = t

  module Set : Set.S with type Elt.t = t

  val show : t -> string

  val equal : t -> t -> bool

  val compare : t -> t -> t

  val t_of_sexp : Base.Sexp.t -> t

  val sexp_of_t : t -> Base.Sexp.t

  val pp : Format.formatter -> t -> unit

  val init : unit -> src

  val gen_new_type : loc:Location.t -> src -> unit -> t

  val get_loc: src -> t -> Location.t 

  val find_nearest_type: Lexing.position -> src -> t option

end = struct

  type src = { mutable current: int; type_pos: (int, Location.t) Hashtbl.t }

  type t = int
  [@@deriving eq, ord, sexp]

  module HMap = Hashtbl.Make (Int)
  module Map = Map.Make (Int)
  module Set = Set.Make (Int)

  let pp fmt (t:t) =
    Format.pp_print_string fmt "'";
    if (Char.to_int 'z' >= t)
    then Format.pp_print_string fmt (Char.escaped (Char.of_int_exn t))
    else begin
      Format.pp_print_string fmt "type_";
      Format.pp_print_int fmt (t - Char.to_int 'z' + 1)
    end

  let show (t:t) =
    if (Char.to_int 'z' >= t)
    then Printf.sprintf "'%s"  (Char.escaped (Char.of_int_exn t))
    else Printf.sprintf "'type_%d" (t - Char.to_int 'z' + 1) 


  let init () = {current=Char.to_int 'a'; type_pos = Hashtbl.create (module Int)}

  let gen_new_type ~loc src () =
    let c1 = src.current in
    Hashtbl.set src.type_pos ~key:c1 ~data:loc;
    src.current <- src.current + 1;
    c1

  let get_loc src t =
    Hashtbl.find_exn src.type_pos t

  let find_nearest_type (loc: Lexing.position) ({ type_pos; _ }: src) =
    let in_region
        ({ pos_cnum; _ }: Lexing.position)
        ({ loc_start={pos_cnum=st; _}; loc_end={pos_cnum=ed; _} }: Location.t) =
      st <= pos_cnum && pos_cnum <= ed in
    let is_smaller_region
        ({ loc_start={pos_cnum=st1; _}; loc_end={pos_cnum=ed1; _} }: Location.t)
        ({ loc_start={pos_cnum=st2; _}; loc_end={pos_cnum=ed2; _} }: Location.t) =
      ed1 - st1 <= ed2 - st2 in

    let distance_from_point
        ({ pos_cnum; _ }: Lexing.position)
        ({ loc_start={pos_cnum=st; _}; loc_end={pos_cnum=ed; _} }: Location.t)
      = Int.(min (abs (st - pos_cnum)) (abs (ed - pos_cnum))) in

    let elems = HMap.to_alist type_pos in
    let res = elems |> List.fold ~init:None ~f:(fun args (typ, reg) ->
        if in_region loc reg
        then match args with
            None -> Some (typ,reg)
          | Some (_,prev_reg) ->
            if is_smaller_region reg prev_reg
            then Some (typ, reg)
            else args
        else args) in
    match res with
    | Some (res, _) -> Some res
    | None -> elems |> List.fold ~init:None ~f:(fun args (typ, reg) ->
        match args with
          None -> Some (typ,reg)
        | Some (_,prev_reg) ->
            if distance_from_point loc reg <= distance_from_point loc prev_reg
            then Some (typ, reg)
            else args
        ) |> Option.map ~f:(fun (typ,_) -> typ)



end

module CannonicalTypeMap = EquivalenceClass(struct
    type v = TypeVariable.t
    module Map = TypeVariable.HMap
    module Set = TypeVariable.Set
  end)



module Type = struct

  type scalar_type =
      TyBool of Location.t | TyInt of Location.t | TyFloat of Location.t | TyIntOrFloat of Location.t
  [@@deriving show, eq, sexp]

  let scalar_loc _src (sc: scalar_type) = (match sc with
      | TyBool loc
      | TyInt loc
      | TyFloat loc
      | TyIntOrFloat loc -> [loc])


  let pp_scalar_type (fmt: Format.formatter) = function
    | TyBool _ -> Format.pp_print_string fmt "bool"
    | TyInt _ -> Format.pp_print_string fmt "int"
    | TyFloat _ -> Format.pp_print_string fmt "float"
    | TyIntOrFloat _ -> Format.pp_print_string fmt "int+float"

  type texture_type = TyTexture1D of Location.t  | TyTexture2D of Location.t
                    | TyTexture3D of Location.t  | TyTextureCube of Location.t
                    | TyAnyTexture of Location.t
  [@@deriving show, eq, sexp]

  let texture_loc _src (tt: texture_type) =
    match tt with
    | TyTexture1D loc
    | TyTexture2D loc
    | TyTexture3D loc
    | TyTextureCube loc
    | TyAnyTexture loc -> [loc]

  let pp_texture_type fmt vl = Format.pp_print_string fmt @@ match vl with
    | TyTexture1D _ -> "texture1D"
    | TyTexture2D _ -> "texture2D"
    | TyTexture3D _ -> "texture3D"
    | TyTextureCube _ -> "texture_cube"
    | TyAnyTexture _ -> "texture?"

  type 'a or_variable = TyDirect of 'a | TyVariable of TypeVariable.t
  [@@deriving show, eq, sexp, iter, fold]

  let map_or_variable g f (v: 'a or_variable) =
    match v with | TyDirect v -> TyDirect (g f v) | TyVariable v -> TyVariable (f v)

  let or_variable_loc f src (v: 'a or_variable) =
    match v with
    | TyDirect sc -> f src sc
    | TyVariable var -> [TypeVariable.get_loc src var]

  let pp_or_variable f fmt = function
    | TyDirect vl -> f fmt vl
    | TyVariable var ->
      Format.pp_print_string fmt (TypeVariable.show var)

  type non_scalar_type =
      TyVec2 of scalar_type or_variable loc |
      TyVec3 of scalar_type or_variable loc |
      TyVec4 of scalar_type or_variable loc |
      TyMat2 of scalar_type or_variable loc |
      TyMat3 of scalar_type or_variable loc |
      TyMat4 of scalar_type or_variable loc
  [@@deriving show, eq, sexp]

  let non_scalar_loc src (ns: non_scalar_type) = match ns with
    | TyVec2 v
    | TyVec3 v
    | TyVec4 v
    | TyMat2 v
    | TyMat3 v
    | TyMat4 v -> or_variable_loc scalar_loc src v.value

  let map_non_scalar_type f (v: non_scalar_type) = match v with
    | TyVec2 {value=l; loc} -> TyVec2 {value=(map_or_variable (fun _ v -> v) f l); loc}
    | TyVec3 {value=l; loc} -> TyVec3 {value=(map_or_variable (fun _ v -> v) f l); loc}
    | TyVec4 {value=l; loc} -> TyVec4 {value=(map_or_variable (fun _ v -> v) f l); loc}
    | TyMat2 {value=l; loc} -> TyMat2 {value=(map_or_variable (fun _ v -> v) f l); loc}
    | TyMat3 {value=l; loc} -> TyMat3 {value=(map_or_variable (fun _ v -> v) f l); loc}
    | TyMat4 {value=l; loc} -> TyMat4 {value=(map_or_variable (fun _ v -> v) f l); loc}

  let pp_non_scalar_type fmt vl =
    Format.pp_open_hbox fmt ();
    begin
      let vl, str = match vl with
        | TyVec2 {value=vl;_} -> vl, "vec2"
        | TyVec3 {value=vl;_} -> vl, "vec3"
        | TyVec4 {value=vl;_} -> vl, "vec4"
        | TyMat2 {value=vl;_} -> vl, "mat2"
        | TyMat3 {value=vl;_} -> vl, "mat3"
        | TyMat4 {value=vl;_} -> vl, "mat4" in
      pp_or_variable (pp_scalar_type) fmt vl;
      Format.pp_print_space fmt ();
      Format.pp_print_string fmt str
    end;
    Format.pp_close_box fmt ()

  type pure_data_type =
      TyScalar of scalar_type
    | TyNonScalar of non_scalar_type
    | TyNumericData of Location.t
  [@@deriving show, eq, sexp, iter, fold]

  let pure_data_loc src (pdt: pure_data_type) = match pdt with
    | TyScalar sc -> scalar_loc src sc
    | TyNonScalar ns -> non_scalar_loc src ns
    | TyNumericData loc -> [loc]

  let map_pure_data_type f (pdt: pure_data_type) =
    match pdt with
    | TyNonScalar ns -> TyNonScalar (map_non_scalar_type f ns)
    | v -> v

  let pp_pure_data_type fmt = function
    | TyScalar sc -> pp_scalar_type fmt sc
    | TyNonScalar sc -> pp_non_scalar_type fmt sc
    | TyNumericData _ -> Format.pp_print_string fmt "numeric_data"

  type 'a typemap = 'a IDMap.t [@opaque]
  [@@deriving eq, sexp]

  let typemap_loc f src (typemap : 'a typemap) =
    IDMap.data typemap |> List.concat_map ~f:(f src)

  let fold_typemap (f: 'a -> 'b -> 'a) (init: 'a) (map: 'b typemap) : 'a =
    IDMap.fold map ~init ~f:(fun ~key:_ ~data acc -> f data acc)
  let iter_typemap (f: 'a -> unit) (map: 'a typemap) : unit = IDMap.iter map ~f
  let map_typemap (f: 'a -> 'b) (map: 'a typemap) : 'b typemap = IDMap.map map ~f

  let pp_typemap f fmt typemap =
    let id_map = IDMap.to_alist typemap in
    Format.pp_print_char fmt '<';
    Format.pp_open_hvbox fmt 1;
    Format.pp_print_list
      ~pp_sep:(fun fmt () -> Format.pp_print_char fmt ';'; Format.pp_print_space fmt () )
      (fun fmt (id, value) ->
         Format.pp_open_hbox fmt ();
         IdentifierTable.pp_m fmt id;
         Format.pp_print_char fmt '=';
         f fmt value;
         Format.pp_close_box fmt ();
      ) fmt id_map;
    Format.pp_close_box fmt ();
    Format.pp_print_char fmt '>'

  let show_typemap f typemap =
    pp_typemap f Format.str_formatter typemap;
    Format.flush_str_formatter ()

  type 'a or_union = Base of 'a | Union of 'a list
  [@@deriving show, eq, sexp]

  let or_union_loc f src (un: 'a or_union) =
    match un with
    | Base v -> f src v
    | Union ls ->  List.concat_map ~f:(f src) ls

  let map_or_union g f (v: 'a or_union) =
    match v with
    | Base v -> Base (g f v)
    | Union ls -> Union (List.map ~f:(g f) ls)

  let pp_or_union f fmt (v: 'a or_union) = match v with
    | Base v -> f fmt v
    | Union ls ->
      Format.pp_print_string fmt "union[";
      Format.pp_open_hvbox fmt 1;

      Format.pp_print_list
        ~pp_sep:(fun fmt () -> Format.pp_print_char fmt ','; Format.pp_print_space fmt ())
        f fmt ls;

      Format.pp_close_box fmt ();
      Format.pp_print_char fmt ']'

  type 'a or_any = Specialized of 'a | Any
  [@@deriving show, eq, sexp]

  let or_any_loc loc f src (un: 'a or_any) =
    match un with
    | Specialized v -> cons_ne_list loc (f src v)
    | Any -> loc, []
  
  let map_or_any g f (v: 'a or_any) =
    match v with
    | Any -> Any
    | Specialized v -> Specialized (g f v)

  let pp_or_any f fmt (v: 'a or_any) = match v with
    | Specialized v -> f fmt v
    | Any -> Format.pp_print_string fmt "any?"

  type mapping = MkTyMapping of  (pure_data_type or_variable typemap or_variable or_union *
                                  pure_data_type or_variable typemap or_variable or_union *
                                  texture_type or_variable typemap or_variable or_union) or_union
  [@@deriving show, eq, sexp]

  let mapping_loc src (MkTyMapping v: mapping) =
    or_union_loc (fun src (a,b,c) ->
        (or_union_loc (or_variable_loc (typemap_loc (or_variable_loc pure_data_loc))) src a @
         or_union_loc (or_variable_loc (typemap_loc (or_variable_loc pure_data_loc))) src b @
         or_union_loc (or_variable_loc (typemap_loc (or_variable_loc texture_loc))) src c)) src v

  let map_mapping f (MkTyMapping v: mapping) =
    MkTyMapping (map_or_union (fun f (attr_map, unif_map, tt_map) ->
        let attr_map =
          map_or_union (map_or_variable (fun f v ->
              map_typemap (fun v -> map_or_variable map_pure_data_type f v) v
            )) f attr_map in
        let unif_map =
          map_or_union (map_or_variable (fun f v ->
              map_typemap (fun v -> map_or_variable map_pure_data_type f v) v
            )) f unif_map in
        let tt_map =
          map_or_union (map_or_variable (fun f v ->
              map_typemap (fun v -> map_or_variable (fun _ v -> v) f v) v
            )) f tt_map in
        (attr_map, unif_map, tt_map)
      ) f v)

  let pp_mapping_internal fmt ((attributes, uniforms, textures)) =
    Format.pp_open_hbox fmt ();
    Format.pp_open_hovbox fmt 1;
    Format.pp_print_char fmt '(';
    Format.pp_open_hvbox fmt 1;
    pp_or_union (pp_or_variable (pp_typemap (pp_or_variable pp_pure_data_type))) fmt attributes;
    Format.pp_print_char fmt ',';
    Format.pp_print_space fmt ();
    pp_or_union (pp_or_variable (pp_typemap (pp_or_variable pp_pure_data_type))) fmt uniforms;
    Format.pp_print_char fmt ',';
    Format.pp_print_space fmt ();
    pp_or_union (pp_or_variable (pp_typemap (pp_or_variable pp_texture_type))) fmt textures;
    Format.pp_close_box fmt ();
    Format.pp_print_char fmt ')';
    Format.pp_close_box fmt ();
    Format.pp_print_space fmt ();
    Format.pp_print_string fmt "map";
    Format.pp_close_box fmt ()

  let pp_mapping fmt (MkTyMapping v) =
    pp_or_union pp_mapping_internal fmt v


  type scene_type =
      TyObject of mapping or_any or_variable * mapping or_any or_variable * Location.t
    (* for light data - represents an object that uses variables that are unbound,
       and must be filled by modifiers
    *)
    | TyObjectFrom of scene_type or_variable list * scene_type or_variable

    | TyCompoundObjectFrom of scene_type or_variable non_empty_list

    | TyModifier of mapping or_any or_variable * mapping or_any or_variable * Location.t
    | TyUnresolvedModifier of mapping or_variable
    | TyLight of mapping or_variable * Location.t
    | TyShader of mapping or_variable * int or_variable * Location.t

    | TyDrawableFrom of scene_type or_variable * scene_type or_variable

    | TyDrawable of mapping or_any or_variable * int or_variable * Location.t

    | TyMulti of scene_type or_variable

    | TyIfScene of scene_type or_variable
  [@@deriving show, eq, sexp]

  let rec scene_loc src (st: scene_type) : Location.t list =
    let rec_ref src st = begin match st with
        | TyDirect v -> scene_loc src v
        | TyVariable v -> [TypeVariable.get_loc src v]
      end in
    match st with
    | TyObject (_, _, loc) -> [loc]
    | TyObjectFrom (sts, st) -> List.concat_map ~f:(rec_ref src) sts @ rec_ref src st
    | TyDrawableFrom (st1, st2) -> rec_ref src st1 @ rec_ref src st2
    | TyCompoundObjectFrom (h,ls) -> rec_ref src h @ List.concat_map ~f:(rec_ref src) ls
    | TyModifier (_, _, loc) -> [loc]
    | TyUnresolvedModifier mov -> or_variable_loc mapping_loc src mov
    | TyLight (_, loc) -> [loc]
    | TyShader (_, _, loc) -> [loc]
    | TyDrawable (_, _, loc) -> [loc]
    | TyMulti v -> rec_ref src v
    | TyIfScene v -> rec_ref src v

  let rec map_scene_type f (st: scene_type) = match st with
    | TyObject (mav, mav1, loc) ->
      let mav = map_or_variable (map_or_any map_mapping) f mav in
      let mav1 = map_or_variable (map_or_any map_mapping) f mav1 in
      TyObject (mav, mav1, loc)
    | TyObjectFrom (svl, sv) ->
      let svl = List.map ~f:(map_or_variable map_scene_type f) svl in
      let sv = map_or_variable map_scene_type f sv in
      TyObjectFrom (svl, sv)
    | TyCompoundObjectFrom svl ->
      let svl = map_ne_list ~f:(map_or_variable map_scene_type f) svl in
      TyCompoundObjectFrom svl
    | TyModifier (mav, mav1, loc) ->
      let mav = map_or_variable (map_or_any map_mapping) f mav in
      let mav1 = map_or_variable (map_or_any map_mapping) f mav1 in
      TyModifier (mav, mav1, loc)      
    | TyUnresolvedModifier mv ->
      let mv = map_or_variable map_mapping f mv in
      TyUnresolvedModifier mv
    | TyLight (mv, loc) ->
      let mv = map_or_variable map_mapping f mv in
      TyLight (mv, loc)
    | TyShader (mv, iv, loc) ->
      let mv = map_or_variable map_mapping f mv in
      let iv = map_or_variable (fun _ v -> v) f iv in
      TyShader (mv, iv, loc)
    | TyDrawableFrom (sv, sv1) ->
      let sv = map_or_variable map_scene_type f sv in
      let sv1 = map_or_variable map_scene_type f sv1 in
      TyDrawableFrom (sv, sv1)
    | TyDrawable (mav, iv, loc) ->
      let mav = map_or_variable (map_or_any map_mapping) f mav in
      let iv = map_or_variable (fun _ v -> v) f iv in
      TyDrawable (mav, iv, loc)
    | TyMulti sv ->
      let sv = map_or_variable map_scene_type f sv in
      TyMulti sv
    | TyIfScene sv ->
      let sv = map_or_variable map_scene_type f sv in
      TyIfScene sv

  let rec pp_scene_type fmt (st: scene_type) = match st with
    | TyCompoundObjectFrom (h, objs) ->
      Format.pp_open_hovbox fmt 1;
      Format.pp_print_char fmt '[';
      Format.pp_open_hvbox fmt 1;

      Format.pp_print_list
        ~pp_sep:(fun fmt () -> Format.pp_print_char fmt ','; Format.pp_print_space fmt ())
        (pp_or_variable (pp_scene_type)) fmt (h :: objs);

      Format.pp_close_box fmt ();

      Format.pp_print_char fmt ']';
      Format.pp_print_space fmt ();
      Format.pp_print_string fmt "object*";
      Format.pp_close_box fmt ()

    | TyObjectFrom (modifiers, base_obj) ->
      Format.pp_open_hovbox fmt 1;
      Format.pp_print_char fmt '[';
      Format.pp_open_hvbox fmt 1;

      begin match modifiers with
        | [] -> ()
        | _ ->
          Format.pp_print_string fmt "modifiers";
          Format.pp_print_char fmt '=';
      end;

      Format.pp_print_list
        ~pp_sep:(fun fmt () -> Format.pp_print_char fmt ','; Format.pp_print_space fmt ())
        (pp_or_variable (pp_scene_type)) fmt modifiers;
      begin match modifiers with [] -> () | _ -> Format.pp_print_char fmt ','; Format.pp_print_space fmt () end;

      Format.pp_print_string fmt "base_obj";
      Format.pp_print_char fmt '=';
      pp_or_variable (pp_scene_type) fmt base_obj;

      Format.pp_close_box fmt ();

      Format.pp_print_char fmt ']';
      Format.pp_print_space fmt ();
      Format.pp_print_string fmt "object";
      Format.pp_close_box fmt ()
    | TyDrawableFrom (shader, obj) ->
      Format.pp_open_hovbox fmt 1;
      Format.pp_print_char fmt '[';
      Format.pp_open_hvbox fmt 1;
      Format.pp_print_list
        ~pp_sep:(fun fmt () -> Format.pp_print_char fmt ','; Format.pp_print_space fmt ())
        (pp_or_variable (pp_scene_type)) fmt [shader; obj];
      Format.pp_close_box fmt ();
      Format.pp_print_char fmt ']';
      Format.pp_print_space fmt ();
      Format.pp_print_string fmt "->drawable";
      Format.pp_close_box fmt ()

    | TyObject (data, lights, _) ->
      Format.pp_open_hovbox fmt 1;
      Format.pp_print_char fmt '(';
      Format.pp_open_hvbox fmt 1;
      pp_or_variable (pp_or_any (pp_mapping)) fmt data;
      Format.pp_print_char fmt ',';
      Format.pp_print_space fmt ();
      pp_or_variable (pp_or_any (pp_mapping)) fmt lights;
      Format.pp_close_box fmt ();
      Format.pp_print_char fmt ')';
      Format.pp_print_space fmt ();
      Format.pp_print_string fmt "object";
      Format.pp_close_box fmt ()
    | TyModifier (data, lights, _) ->
      Format.pp_open_hovbox fmt 1;
      Format.pp_print_char fmt '(';
      Format.pp_open_hvbox fmt 1;
      pp_or_variable (pp_or_any (pp_mapping)) fmt data;
      Format.pp_print_char fmt ',';
      Format.pp_print_space fmt ();
      pp_or_variable (pp_or_any (pp_mapping)) fmt lights;
      Format.pp_close_box fmt ();
      Format.pp_print_char fmt ')';
      Format.pp_print_space fmt ();
      Format.pp_print_string fmt "modifier";
      Format.pp_close_box fmt ()
    | TyUnresolvedModifier mapping ->
      Format.pp_open_hovbox fmt 1;
      Format.pp_print_char fmt '(';
      Format.pp_open_hvbox fmt 1;
      pp_or_variable (pp_mapping) fmt mapping;
      Format.pp_print_char fmt ')';
      Format.pp_print_space fmt ();
      Format.pp_print_string fmt "modifier?";
      Format.pp_close_box fmt ()
    | TyLight (mapping, _) ->
      Format.pp_open_hovbox fmt 1;
      Format.pp_print_char fmt '(';
      Format.pp_open_hvbox fmt 1;
      pp_or_variable (pp_mapping) fmt mapping;
      Format.pp_print_char fmt ')';
      Format.pp_print_space fmt ();
      Format.pp_print_string fmt "light";
      Format.pp_close_box fmt ()
    | TyShader (data, outcount, _) ->
      Format.pp_open_hovbox fmt 1;
      Format.pp_print_char fmt '(';
      Format.pp_open_hvbox fmt 1;
      pp_or_variable (pp_mapping) fmt data;
      Format.pp_print_char fmt ',';
      Format.pp_print_space fmt ();
      pp_or_variable (Format.pp_print_int) fmt outcount;
      Format.pp_close_box fmt ();
      Format.pp_print_char fmt ')';
      Format.pp_print_space fmt ();
      Format.pp_print_string fmt "shader";
      Format.pp_close_box fmt ()      
    | TyDrawable (light,  outcount, _) ->
      Format.pp_open_hovbox fmt 1;
      Format.pp_print_char fmt '(';
      Format.pp_open_hvbox fmt 1;
      pp_or_variable (pp_or_any (pp_mapping)) fmt light;
      Format.pp_print_char fmt ',';
      Format.pp_print_space fmt ();
      pp_or_variable (Format.pp_print_int) fmt outcount;
      Format.pp_close_box fmt ();
      Format.pp_print_char fmt ')';
      Format.pp_print_space fmt ();
      Format.pp_print_string fmt "drawable";
      Format.pp_close_box fmt ()
    | TyMulti st ->
      Format.pp_open_hovbox fmt 1;
      pp_or_variable (pp_scene_type) fmt st;
      Format.pp_print_space fmt ();
      Format.pp_print_string fmt "multi";
      Format.pp_close_box fmt ()
    | TyIfScene st ->
      Format.pp_open_hovbox fmt 1;
      pp_or_variable (pp_scene_type) fmt st;
      Format.pp_print_space fmt ();
      Format.pp_print_string fmt "option";
      Format.pp_close_box fmt ()


  type internal_type_expr =
    | TyPure of pure_data_type
    | TyTexture of texture_type
    | TyScene of scene_type
    (* used to capture the types of if expressions (with no else) in scenes *)
    | TyList of internal_type_expr or_variable
  and type_expr = TypeExpr of internal_type_expr or_variable
  [@@deriving show, eq, sexp, iter, fold]

  let rec internal_type_loc src (itt: internal_type_expr) =
    let rec_ref src v = match v with
      | TyDirect v -> internal_type_loc src v
      | TyVariable var -> [TypeVariable.get_loc src var]
    in
    match itt with
    | TyPure pure -> pure_data_loc src pure
    | TyTexture texture -> texture_loc src texture
    | TyScene scene  -> scene_loc src scene
    | TyList itt -> rec_ref src itt

  let rec map_internal_type_expr f (tt: internal_type_expr) =
    match tt with
    | TyPure pdt -> TyPure (map_pure_data_type f pdt)
    | TyTexture tt -> TyTexture tt
    | TyScene st -> TyScene (map_scene_type f st)
    | TyList iv -> TyList (map_or_variable map_internal_type_expr f iv)

  let rec pp_internal_type_expr fmt (itt: internal_type_expr) = match itt with
    | TyPure pur -> pp_pure_data_type fmt pur
    | TyTexture tex -> pp_texture_type fmt tex
    | TyScene st -> pp_scene_type fmt st
    | TyList st ->
      Format.pp_open_hovbox fmt 1;
      pp_or_variable (pp_internal_type_expr) fmt st;
      Format.pp_print_space fmt ();
      Format.pp_print_string fmt "list";
      Format.pp_close_box fmt ()
  and pp_type_expr fmt (TypeExpr st: type_expr) = 
    pp_or_variable (pp_internal_type_expr) fmt st

  type t = type_expr
  [@@deriving show, eq, sexp]

  let type_loc src (TypeExpr itt_or_var: t) = match itt_or_var with
    | TyDirect v -> internal_type_loc src v
    | TyVariable var -> [TypeVariable.get_loc src var]

  let map_type_expr f (TypeExpr iv:type_expr) = TypeExpr (map_or_variable map_internal_type_expr f iv)

  let pp fmt (t: t) = pp_type_expr fmt t

  (* 1 if a is > b, -1 if b > a, 0 if a = b *)
  let compare_or_variable f (a: 'a or_variable) (b: 'a or_variable) =
    let (a_is_more_specialized, b_is_more_specialized, ab_equal) = (1, -1, 0) in
    match a,b with
    | TyDirect a, TyDirect b -> f a b
    | TyVariable _, TyDirect _ -> b_is_more_specialized
    | TyDirect _, TyVariable _ -> a_is_more_specialized
    | TyVariable _ , TyVariable _ -> ab_equal

  let compare_or_any f (a: 'a or_any) (b: 'a or_any) = match a,b with
    | Any, Any -> 0
    | Specialized v1, Specialized v2 -> f v1 v2
    | Specialized _, _ -> 1
    | _, Specialized _ -> -1

  let compare_or_union f (a: 'a or_union) (b: 'a or_union) = match a,b with
    | Base a, Base b -> f a b
    | Union a, Union b -> List.compare f a b
    | _ -> 0

  let compare_typemap f (a: 'a typemap) (b: 'a typemap) =
    IDMap.compare_direct f a b

  let rec compare_type_expr (TypeExpr a) (TypeExpr b) =
    compare_or_variable compare_internal_type_expr a b 
  and compare_internal_type_expr a b = match a,b with
    | (TyPure a, TyPure b) -> compare_pure_data_type a b
    | (TyTexture a, TyTexture b) -> compare_texture_type a b
    | (TyScene a, TyScene b) -> compare_scene_type a b
    | (TyList a, TyList b) -> compare_or_variable compare_internal_type_expr a b
    | _, _ -> 0
  and compare_texture_type a b =
    let (a_is_more_specialized, b_is_more_specialized, ab_equal) = (1, -1, 0) in
    match a,b with
    | (TyAnyTexture _, TyAnyTexture _) -> ab_equal
    | (_, TyAnyTexture _) -> a_is_more_specialized
    | (TyAnyTexture _,  _) -> b_is_more_specialized
    | _ -> ab_equal
  and compare_pure_data_type a b =
    let (a_is_more_specialized, b_is_more_specialized, ab_equal) = (1, -1, 0) in
    match a,b with
    | (TyScalar a, TyScalar b) -> compare_scalar_type a b
    | (TyNonScalar a, TyNonScalar b) -> compare_non_scalar_type a b 
    | (TyNumericData _, TyNumericData _) -> ab_equal
    | (_, TyNumericData _) -> a_is_more_specialized
    | (TyNumericData _, _) -> b_is_more_specialized
    | _, _ -> ab_equal
  and compare_scalar_type a b =
    let (a_is_more_specialized, b_is_more_specialized, ab_equal) = (1, -1, 0) in
    match a,b with
    | TyIntOrFloat _, TyIntOrFloat _ -> ab_equal
    | TyIntOrFloat _, _ -> b_is_more_specialized | _, TyIntOrFloat _ -> a_is_more_specialized
    | TyFloat _, TyInt _ -> a_is_more_specialized | TyInt _, TyFloat _ -> b_is_more_specialized
    | _ -> 0
  and compare_non_scalar_type a b =
    let unwrap a =
      match a with
      |(TyVec2 {value=a;_})|(TyVec3 {value=a;_})|(TyVec4 {value=a;_})|(TyMat2 {value=a;_})
      |(TyMat3 {value=a;_})|(TyMat4 {value=a;_}) -> a in
    compare_or_variable compare_scalar_type (unwrap a) (unwrap b)
  and compare_scene_type a b =
    let rec join ls  = match ls with
      | [] -> 0
      | h :: t -> let res = h () in if Int.(res = 0) then join t else res in
    match a,b with
    | (TyModifier (pt1, at1, _), TyModifier (pt2, at2, _))
    | (TyObject (pt1, at1, _), TyObject (pt2, at2, _)) ->
      join [
        (fun () -> compare_or_variable (compare_or_any compare_mapping) pt1 pt2);
        fun () -> compare_or_variable (compare_or_any compare_mapping) at1 at2
      ]
    | (TyLight (mov1, _), TyLight (mov2, _)) -> compare_or_variable compare_mapping mov1 mov2

    | (TyDrawable (pt1, at1, _), TyDrawable (pt2, at2, _)) ->
      join [
        (fun () -> compare_or_variable (compare_or_any compare_mapping) pt1 pt2);
        fun () -> compare_or_variable (fun _ _ -> 0) at1 at2
      ]
    | (TyShader (pt1, at1, _), TyShader (pt2, at2, _)) ->
      join [
        (fun () -> compare_or_variable compare_mapping pt1 pt2);
        fun () -> compare_or_variable (fun _ _ -> 0) at1 at2
      ]
    | (TyIfScene stov1, TyIfScene stov2) 
    | (TyMulti stov1, TyMulti stov2) -> compare_or_variable compare_scene_type stov1 stov2

    | (TyDrawableFrom (_, _), TyDrawableFrom (_, _))
    | (TyUnresolvedModifier _, TyUnresolvedModifier _)
    | (TyObjectFrom (_, _), TyObjectFrom (_, _)) 
    | (TyCompoundObjectFrom _, TyCompoundObjectFrom _)
    | _ -> 0
  and compare_mapping (MkTyMapping m1) (MkTyMapping m2) =
    let rec join ls  = match ls with
      | [] -> 0
      | h :: t -> let res = h () in if Int.(res = 0) then join t else res in
    compare_or_union (fun (at1, ut1, tt1) (at2, ut2, tt2) ->
        join [
          (fun () ->
             compare_or_union
               (compare_or_variable (compare_typemap (compare_or_variable compare_pure_data_type)))
               at1 at2
          );
          (fun () ->
             compare_or_union
               (compare_or_variable (compare_typemap (compare_or_variable compare_pure_data_type)))
               ut1 ut2
          );
          (fun () ->
             compare_or_union
               (compare_or_variable (compare_typemap (compare_or_variable compare_texture_type)))
               tt1 tt2
          )
        ]

      ) m1 m2


  type constr = [
      `Eq of t * t
    | `IntEq of TypeVariable.t * int
    | `MappingEq of TypeVariable.t * mapping or_any
    (* | `SubType of t * t *)
    | `Indexed of t *  t (* t1 * t3 where (e1: t1)[e2 : t2] : t3 *)
      (* | `UnionOf of  t * t list (\* t2 * t1s a for expression with type t2, composed of elements of type t1  *\)
       * 
       * | `ObjectFrom of t * t * t list (\* used when constructing objects from an object type and
       *                                    a list of modifier types  *\)
       * | `DrawableFrom of t * t * t (\* constructs a drawable with type t, using the subsequent two types *\) *)
  ]
  [@@deriving eq, sexp]

  let compare_constr (a: constr) (b:constr) = match a,b with
    | (`Eq (_, t1), `Eq (_, t2)) -> compare_type_expr t1 t2
    | _ -> 0

  let map_constr f (constr: constr) =
    match constr with
    | `IntEq (var, n) -> `IntEq (f var, n)
    | `Eq (t1,t2) -> `Eq (map_type_expr f t1, map_type_expr f t2)
    | `Indexed (t1, t2) -> `Indexed (map_type_expr f t1, map_type_expr f t2)
    | `MappingEq (t1, t2) -> `MappingEq (f t1, map_or_any map_mapping f t2)
    (* | `SubType (t1, t2) -> `SubType (map_type_expr f t1, map_type_expr f t2) *)


  let pp_constr fmt (constr: constr) = match constr with
    | `IntEq (t1, n) ->
      Format.pp_open_hovbox fmt 1;
      TypeVariable.pp fmt t1;
      Format.pp_print_space fmt ();
      Format.pp_print_char fmt ':';
      Format.pp_print_char fmt '=';
      Format.pp_print_space fmt ();
      Format.pp_print_int fmt n;
      Format.pp_close_box fmt ()
    | `MappingEq (t1, n) ->
      Format.pp_open_hovbox fmt 1;
      TypeVariable.pp fmt t1;
      Format.pp_print_space fmt ();
      Format.pp_print_char fmt ':';
      Format.pp_print_char fmt '=';
      Format.pp_print_space fmt ();
      pp_or_any pp_mapping fmt n;
      Format.pp_close_box fmt ()

    | `Eq (t1, t2) ->
      Format.pp_open_hovbox fmt 1;
      pp fmt t1;
      Format.pp_print_space fmt ();
      Format.pp_print_char fmt '=';
      Format.pp_print_space fmt ();
      pp fmt t2;
      Format.pp_close_box fmt ()
    (* | `SubType (t1, t2) ->
     *   Format.pp_open_hovbox fmt 1;
     *   pp fmt t1;
     *   Format.pp_print_space fmt ();
     *   Format.pp_print_string fmt "<:";
     *   Format.pp_print_space fmt ();
     *   pp fmt t2;
     *   Format.pp_close_box fmt () *)
    | `Indexed (t1, t2) ->
      Format.pp_open_hovbox fmt 1;
      pp fmt t1;
      Format.pp_print_space fmt ();
      Format.pp_print_char fmt '=';
      Format.pp_print_space fmt ();
      Format.pp_print_string fmt "indexed";
      Format.pp_print_space fmt ();
      pp fmt t2;
      Format.pp_close_box fmt ()      
  (* | `UnionOf (t1, ts) ->
   *   Format.pp_open_hovbox fmt 1;
   *   pp fmt t1;
   *   Format.pp_print_space fmt ();
   *   Format.pp_print_char fmt '=';
   *   Format.pp_print_space fmt ();
   *   Format.pp_print_string fmt "union of";
   *   Format.pp_print_space fmt ();
   *   Format.pp_print_string fmt "[";
   *   Format.pp_open_hvbox fmt 1;
   *   Format.pp_print_list
   *     ~pp_sep:(fun fmt () -> Format.pp_print_char fmt ';'; Format.pp_print_space fmt () )
   *     (fun fmt v -> pp fmt v) fmt ts;
   *   Format.pp_close_box fmt ();
   *   Format.pp_print_string fmt "]";
   *   Format.pp_close_box fmt ()
   * | `ObjectFrom (t1, t2, ts) ->
   *   Format.pp_open_hovbox fmt 1;
   *   pp fmt t1;
   *   Format.pp_print_space fmt ();
   *   Format.pp_print_char fmt '=';
   *   Format.pp_print_space fmt ();
   *   Format.pp_print_string fmt "object from";
   *   Format.pp_print_space fmt ();
   *   Format.pp_open_hvbox fmt 1;
   *   Format.pp_print_list
   *     ~pp_sep:(fun fmt () -> Format.pp_print_char fmt ','; Format.pp_print_space fmt () )
   *     (fun fmt v -> pp fmt v) fmt ts;
   * 
   *   begin match ts with
   *     | [] -> ()
   *     | _ :: _ ->
   *       Format.pp_print_char fmt ',';
   *       Format.pp_print_space fmt ()
   *   end;
   *   pp fmt t2;
   *   Format.pp_close_box fmt ();
   *   Format.pp_close_box fmt ()
   * | `DrawableFrom (t1, t2, t3) ->
   *   Format.pp_open_hovbox fmt 1;
   *   pp fmt t1;
   *   Format.pp_print_space fmt ();
   *   Format.pp_print_char fmt '=';
   *   Format.pp_print_space fmt ();
   *   Format.pp_print_string fmt "drawable from";
   *   Format.pp_print_space fmt ();
   *   Format.pp_open_hvbox fmt 1;
   *   pp fmt t2;
   *   Format.pp_print_char fmt ',';
   *   Format.pp_print_space fmt ();
   *   pp fmt t3;
   *   Format.pp_close_box fmt ();
   *   Format.pp_close_box fmt () *)

  let pp_constr_list fmt (ls: constr list) =
    Format.pp_print_char fmt '[';
    Format.pp_open_hvbox fmt 1;
    Format.pp_print_list
      ~pp_sep:(fun fmt () -> Format.pp_print_char fmt ';'; Format.pp_print_space fmt ())
      (fun fmt v -> pp_constr fmt v) fmt ls;
    Format.pp_close_box fmt ();
    Format.pp_print_char fmt ']'

  let show_constr =
    Format.sprintf "%a" (fun () v -> pp_constr Format.str_formatter v; Format.flush_str_formatter ())

  let show_constr_list =
    Format.sprintf "%a" (fun () v -> pp_constr_list Format.str_formatter v; Format.flush_str_formatter ())

  module C = CannonicalTypeMap

  let to_cannonical_or_variable f m (var: 'a or_variable) =
    match var with
    | TyDirect v -> TyDirect (f m v)
    | TyVariable var -> TyVariable (C.get_cannonical_form m var)

  let to_cannonical_or_any f m (var: 'a or_any) =
    match var with
    | Specialized v -> Specialized (f m v)
    | Any -> Any

  let to_cannonical_or_union f m (var: 'a or_union) =
    match var with
    | Base v -> Base (f m v)
    | Union ls -> Union (List.map ~f:(f m) ls)

  let to_cannonical_typemap f m (v: 'a typemap) =
    IDMap.map v ~f:(fun v -> f m v)

  let rec to_cannonical_type_expr (m: C.t) (TypeExpr itt: type_expr) =
    TypeExpr (to_cannonical_or_variable to_cannonical_internal_type_expr m itt)
  and to_cannonical_internal_type_expr m (itt: internal_type_expr) =
    match itt with
    | TyPure pure -> TyPure (to_cannonical_pure_data m pure)
    | TyTexture tex -> TyTexture tex
    | TyScene st -> TyScene (to_cannonical_scene m st)
    | TyList itt -> TyList (to_cannonical_or_variable to_cannonical_internal_type_expr m itt)
  and to_cannonical_pure_data m (p: pure_data_type) =
    match p with
    | TyScalar sc -> TyScalar sc
    | TyNonScalar ns -> TyNonScalar (to_cannonical_non_scalar m ns)
    | TyNumericData loc -> TyNumericData loc
  and to_cannonical_non_scalar m (ns: non_scalar_type) =
    match ns with
    | TyVec2 {value=sc; loc} -> TyVec2 {value=(to_cannonical_or_variable (fun _ v -> v) m sc); loc}
    | TyVec3 {value=sc; loc} -> TyVec3 {value=(to_cannonical_or_variable (fun _ v -> v) m sc); loc}
    | TyVec4 {value=sc; loc} -> TyVec4 {value=(to_cannonical_or_variable (fun _ v -> v) m sc); loc}
    | TyMat2 {value=sc; loc} -> TyMat2 {value=(to_cannonical_or_variable (fun _ v -> v) m sc); loc}
    | TyMat3 {value=sc; loc} -> TyMat3 {value=(to_cannonical_or_variable (fun _ v -> v) m sc); loc}
    | TyMat4 {value=sc; loc} -> TyMat4 {value=(to_cannonical_or_variable (fun _ v -> v) m sc); loc}
  and to_cannonical_scene m (s: scene_type) =
    match s with
    | TyObject (data, light, loc) ->
      let f = to_cannonical_or_variable (to_cannonical_or_any to_cannonical_mapping) m in
      TyObject (f data, f light, loc)
    | TyObjectFrom (stvs, stv) ->
      let f = to_cannonical_or_variable to_cannonical_scene m in
      TyObjectFrom (List.map ~f stvs, f stv)
    | TyCompoundObjectFrom ls ->
      let f = to_cannonical_or_variable to_cannonical_scene m in      
      TyCompoundObjectFrom (map_ne_list ~f ls)
    | TyModifier (data, light, loc) ->
      let f = to_cannonical_or_variable (to_cannonical_or_any to_cannonical_mapping) m in
      TyModifier (f data, f light, loc)
    | TyUnresolvedModifier mov ->
      let f = to_cannonical_or_variable to_cannonical_mapping m in
      TyUnresolvedModifier (f mov)
    | TyLight (mov, loc) ->
      let f = to_cannonical_or_variable to_cannonical_mapping m in      
      TyLight (f mov, loc)
    | TyShader (mov, cov, loc) ->
      let f = to_cannonical_or_variable to_cannonical_mapping m in            
      TyShader (f mov, to_cannonical_or_variable (fun _ v -> v) m cov, loc)
    | TyDrawableFrom (mov, cov) ->
      let f = to_cannonical_or_variable to_cannonical_scene m in            
      TyDrawableFrom (f mov, to_cannonical_or_variable (fun _ v -> v) m cov)
    | TyDrawable (data, cov, loc) ->
      let f = to_cannonical_or_variable (to_cannonical_or_any to_cannonical_mapping) m in
      TyDrawable (f data, to_cannonical_or_variable (fun _ v -> v) m cov, loc)
    | TyMulti stov ->
      TyMulti (to_cannonical_or_variable to_cannonical_scene m stov)
    | TyIfScene stov ->
      TyIfScene (to_cannonical_or_variable to_cannonical_scene m stov)
  and to_cannonical_mapping m (MkTyMapping elems: mapping) =
    MkTyMapping (to_cannonical_or_union (fun m (attr, unif, texts) ->
        let f = to_cannonical_or_union (
            to_cannonical_or_variable
              (to_cannonical_typemap (to_cannonical_or_variable to_cannonical_pure_data))) m in
        let g = to_cannonical_or_union (
            to_cannonical_or_variable
              (to_cannonical_typemap (to_cannonical_or_variable (fun _ v -> v)))) m in
        (f attr, f unif, g texts)
      ) m elems)
  and to_cannonical_constr m (const: constr) : constr =
    match const with
    | `IntEq (t, n) -> `IntEq (C.get_cannonical_form m t, n)
    | `Eq (t1,t2) -> `Eq (to_cannonical_type_expr m t1, to_cannonical_type_expr m t2)
    | `Indexed (t1,t2) -> `Indexed (to_cannonical_type_expr m t1, to_cannonical_type_expr m t2)
    | `MappingEq (t1,n) -> `MappingEq (C.get_cannonical_form m t1, n)
    (* | `SubType (t1,t2) -> `SubType (to_cannonical_type_expr m t1, to_cannonical_type_expr m t2) *)

end


module Substitution = struct
  open Type

  type variable_type =
    | Scalar of scalar_type
    | Pure of pure_data_type
    | Texture of texture_type
    | PureDataMap of pure_data_type or_variable typemap
    | TextureDataMap of texture_type or_variable typemap
    | Mapping of mapping
    | Scene of scene_type
    | Internal of internal_type_expr
    | Int of int
    | Variable of TypeVariable.t
    | VAny
  [@@deriving eq]

  let of_scalar sc = Scalar sc
  let of_pure_data sc = Pure sc
  let of_texture sc = Texture sc
  let of_pure_data_mapping sc = PureDataMap sc
  let of_texture_data_mapping sc = TextureDataMap sc
  let of_mapping sc = Mapping sc
  let of_scene_type sc = Scene sc

  let of_internal_type (internal_type: internal_type_expr) =
    match internal_type with
    | TyPure pdt -> Pure pdt
    | TyTexture tt -> Texture tt
    | TyScene st -> Scene st
    | TyList var -> (match var with | TyDirect st -> Internal st | TyVariable var -> Variable var)

  let of_type (TypeExpr var: type_expr) =
    match var with | TyDirect internal -> Internal internal | TyVariable var -> Variable var

  let of_variable (var: TypeVariable.t) = Variable var

  let pp_variable_type fmt (var: variable_type) = match var with
    | Scalar sc -> Type.pp_scalar_type fmt sc
    | Pure pt -> Type.pp_pure_data_type fmt pt
    | Texture tt -> Type.pp_texture_type fmt tt 
    | PureDataMap tm -> Type.pp_typemap (Type.pp_or_variable (Type.pp_pure_data_type)) fmt tm
    | TextureDataMap tt -> Type.pp_typemap (Type.pp_or_variable (Type.pp_texture_type)) fmt tt
    | Mapping mapping -> Type.pp_mapping fmt mapping
    | Scene st -> Type.pp_scene_type fmt st
    | Internal itt -> Type.pp_internal_type_expr fmt itt
    | Int int -> Format.pp_print_int fmt int
    | VAny -> Format.pp_print_string fmt "any"
    | Variable var -> TypeVariable.pp fmt var

  let show_variable_type v =
    pp_variable_type Format.str_formatter v;
    Format.flush_str_formatter ()

  type substitution_error =
    | IncompatibleTypes of TypeVariable.t * variable_type * string

  exception SubstitutionException of substitution_error

  let () = Stdlib.Printexc.register_printer (function
        SubstitutionException (IncompatibleTypes (ty_var, var_type, name)) ->
        let fmt = Format.str_formatter in
        let prf = Format.flush_str_formatter in
        let ty_var = TypeVariable.pp fmt  ty_var; prf () in
        let var_type = pp_variable_type fmt var_type; prf () in
        let str = Printf.sprintf "Substitution error %s/%s != %s" ty_var var_type name  in
        Some str
      | _ -> None
    )


  let of_type_map map = TypeVariable.Map.map ~f:of_type map

  let of_variable_map map = TypeVariable.Map.map ~f:of_variable map

  let substitute_or_union f map (v: 'a or_union) =
    match v with
    | Base v -> Base (f map v)
    | Union ls -> Union (List.map ~f:(f map) ls)

  let substitute_or_any f map (v: 'a or_any) =
    match v with | Specialized v -> Specialized (f map v) | Any -> Any

  let rec substitute_internal_type_expr (map: variable_type TypeVariable.Map.t) (t: internal_type_expr) =
    match t with
    | TyPure pure -> TyPure (substitute_pure_data map pure)
    | TyTexture texture -> TyTexture texture
    | TyScene st -> TyScene (substitute_scene_type map st)
    | TyList itt -> TyList (substitute_internal_type_or_variable map itt)
  and substitute_scene_type map (st: scene_type) =
    match st with
    | TyIfScene st -> TyIfScene (substitute_scene_type_or_variable map st)
    | TyObject (m1, m2, loc) -> TyObject (
        substitute_mapping_or_variable  map m1,
        substitute_mapping_or_variable  map m2,
        loc
      )
    | TyObjectFrom (modifiers, base_obj) ->
      TyObjectFrom
        (
          List.map ~f:(substitute_scene_type_or_variable map) modifiers,
          substitute_scene_type_or_variable map base_obj
        )
    | TyCompoundObjectFrom (objs) ->
      TyCompoundObjectFrom (
        map_ne_list ~f:(substitute_scene_type_or_variable map) objs
      )
    | TyDrawableFrom (shader, obj) ->
      TyDrawableFrom
        (
          substitute_scene_type_or_variable map shader,
          substitute_scene_type_or_variable map obj
        )
    | TyModifier (m1, m2,loc) ->
      TyModifier (
        substitute_mapping_or_variable map m1,
        substitute_mapping_or_variable map m2,loc
      )
    | TyUnresolvedModifier m1 -> TyUnresolvedModifier (substitute_mapping_or_variable_direct map m1)
    | TyLight (m1, loc) -> TyLight (substitute_mapping_or_variable_direct map m1, loc)
    | TyShader (m1, m2,loc) ->
      TyShader (substitute_mapping_or_variable_direct map m1, substitute_int_or_variable map m2,loc)
    | TyDrawable (m2,  outcount,loc) ->
      TyDrawable (
        substitute_mapping_or_variable map m2,
        substitute_int_or_variable map outcount,
        loc
      )      
    | TyMulti sr ->
      TyMulti (substitute_scene_type_or_variable map sr)
  and substitute_pure_data map (pure: pure_data_type) = match pure with
    | TyScalar sc -> TyScalar sc
    | TyNonScalar ns -> TyNonScalar (substitute_non_scalar map ns)
    | TyNumericData loc -> TyNumericData loc
  and substitute_non_scalar map (ns: non_scalar_type) =
    match ns with
    | TyVec2 l -> nested_scalar_vec2 map l
    | TyVec3 l -> nested_scalar_vec3 map l
    | TyVec4 l -> nested_scalar_vec4 map l
    | TyMat2 l -> TyMat2 (substitute_scalar_or_variable map l)
    | TyMat3 l -> TyMat3 (substitute_scalar_or_variable map l)
    | TyMat4 l -> TyMat4 (substitute_scalar_or_variable map l)
  and nested_scalar_vec2 map ({ value=st; loc }: scalar_type or_variable loc) =
    match st with
    | TyDirect sc -> TyVec2 {value=(TyDirect sc); loc}
    | TyVariable var ->
      let rec loop var = 
        match TypeVariable.Map.find map var with
        | None -> TyVec2 {value=(TyVariable var); loc}
        | Some (vl: variable_type) -> (match vl with
            | Pure (TyNumericData _)
            | Internal (TyPure (TyNumericData _)) -> TyVec2 {value=TyVariable var;loc}

            | Pure (TyScalar sc)
            | Internal (TyPure (TyScalar sc))
            | Scalar sc -> TyVec2 {value=(TyDirect sc); loc}
            | Variable v -> loop v
            | Internal (TyPure (TyNonScalar (TyVec2 l)))
            | Pure (TyNonScalar (TyVec2 l)) -> TyMat2 (substitute_scalar_or_variable map l)
            | ls  -> raise (SubstitutionException (IncompatibleTypes (var, ls, "scalar_type or vec2")))) in
      loop var
  and nested_scalar_vec3 map ({ value=st; loc }: scalar_type or_variable loc) =
    match st with
    | TyDirect sc -> TyVec3 {value=(TyDirect sc); loc}
    | TyVariable var ->
      let rec loop var = 
        match TypeVariable.Map.find map var with
        | None -> TyVec3 {value=(TyVariable var); loc}
        | Some (vl: variable_type) -> (match vl with
            | Pure (TyNumericData _)
            | Internal (TyPure (TyNumericData _)) -> TyVec3 {value=TyVariable var;loc}

            | Pure (TyScalar sc)
            | Internal (TyPure (TyScalar sc))
            | Scalar sc -> TyVec3 {value=(TyDirect sc); loc}
            | Variable v -> loop v
            | Internal (TyPure (TyNonScalar (TyVec3 l)))
            | Pure (TyNonScalar (TyVec3 l)) -> TyMat3 (substitute_scalar_or_variable map l)
            | ls  -> raise (SubstitutionException (IncompatibleTypes (var, ls, "scalar_type or vec3")))) in
      loop var

  and nested_scalar_vec4 map ({ value=st; loc }: scalar_type or_variable loc) =
    match st with
    | TyDirect sc -> TyVec4 {value=(TyDirect sc); loc}
    | TyVariable var ->
      let rec loop var = 
        match TypeVariable.Map.find map var with
        | None -> TyVec4 {value=(TyVariable var); loc}
        | Some (vl: variable_type) -> (match vl with
            | Pure (TyNumericData loc)
            | Internal (TyPure (TyNumericData loc)) -> TyVec4 {value=TyVariable var;loc}


            | Pure (TyScalar sc)
            | Internal (TyPure (TyScalar sc))
            | Scalar sc -> TyVec4 {value=(TyDirect sc);loc}
            | Variable v -> loop v
            | Internal (TyPure (TyNonScalar TyVec4 l))
            | Pure (TyNonScalar (TyVec4 l)) -> TyMat4 (substitute_scalar_or_variable map l)
            | ls  -> raise (SubstitutionException (IncompatibleTypes (var, ls, "scalar_type or vec4")))) in
      loop var

  and substitute_scalar_or_variable map ({ value=s; loc }: scalar_type or_variable loc) =
    match s with
    | TyDirect s -> {value=TyDirect s; loc}
    | TyVariable var ->
      let rec loop var = 
        match TypeVariable.Map.find map var with
        | None -> TyVariable var
        | Some (vl: variable_type) -> (match vl with
            | Pure (TyNumericData _)
            | Internal (TyPure (TyNumericData _)) -> TyVariable var
            | Pure (TyScalar sc)
            | Internal (TyPure (TyScalar sc))
            | Scalar sc -> TyDirect sc
            | Variable v -> loop v
            | ls  ->
              raise (SubstitutionException (IncompatibleTypes (var, ls, "scalar_type")))) in
      {value=loop var; loc}
  and substitute_pure_or_variable map s =
    match s with
    | TyDirect s -> TyDirect (substitute_pure_data map s)
    | TyVariable var ->
      let rec loop var = 
        match TypeVariable.Map.find map var with
        | None -> TyVariable var
        | Some (vl: variable_type) -> (match vl with
            | Internal (TyPure sc) 
            | Pure sc -> TyDirect sc
            | Scalar sc -> TyDirect (TyScalar sc)
            | Variable v -> loop v
            | ls  -> raise (SubstitutionException (IncompatibleTypes (var, ls, "pure_data_type")))) in
      loop var
  and substitute_texture_or_variable map s =
    match s with
    | TyDirect s -> TyDirect s
    | TyVariable var ->
      let rec loop var =
        match TypeVariable.Map.find map var with
        | None -> TyVariable var
        | Some (vl: variable_type) -> (match vl with
            | Internal (TyTexture sc)
            | Texture sc -> TyDirect sc
            | Variable v -> loop v
            | ls  -> raise (SubstitutionException (IncompatibleTypes (var, ls, "texture_type")))) in
      loop var
  and substitute_pure_data_map_or_variable map s =
    match s with
    | TyDirect s -> TyDirect (IDMap.map ~f:(substitute_pure_or_variable map) s)
    | TyVariable var ->
      let rec loop var = 
        match TypeVariable.Map.find map var with
        | None -> TyVariable var
        | Some (vl: variable_type) -> (match vl with
            | PureDataMap sc -> TyDirect (IDMap.map ~f:(substitute_pure_or_variable map) sc)
            | Variable v -> loop v
            | ls  ->
              raise (SubstitutionException (IncompatibleTypes (var, ls, "pure_data_type or_variable typemap")))) in
      loop var
  and substitute_texture_data_map_or_variable map s =
    match s with
    | TyDirect s -> TyDirect (IDMap.map ~f:(substitute_texture_or_variable map) s)
    | TyVariable var ->
      let rec loop var = 
        match TypeVariable.Map.find map var with
        | None -> TyVariable var
        | Some (vl: variable_type) -> (match vl with
            | TextureDataMap sc -> TyDirect (IDMap.map ~f:(substitute_texture_or_variable map) sc)
            | Variable v -> loop v
            | ls  ->
              raise (SubstitutionException (IncompatibleTypes (var, ls, "texture_type or_variable typemap")))) in
      loop var
  and substitute_mapping_or_variable_internal map (pdt1, pdt2, tt1) =
    (
      substitute_or_union substitute_pure_data_map_or_variable map pdt1,
      substitute_or_union substitute_pure_data_map_or_variable map pdt2,
      substitute_or_union substitute_texture_data_map_or_variable map tt1
    )
  and substitute_mapping_or_variable map (s: _ or_variable) : _ or_variable =
    match s with
    | TyDirect Any -> TyDirect Any
    | TyDirect Specialized (MkTyMapping v) ->
      TyDirect (Specialized (MkTyMapping (substitute_or_union substitute_mapping_or_variable_internal map v)))
    | TyVariable var ->
      let rec loop var = 
        match TypeVariable.Map.find map var with
        | None -> TyVariable var
        | Some (vl: variable_type) -> (match vl with
            | VAny -> TyDirect Any
            | Mapping sc -> TyDirect (Specialized (substitute_mapping map sc))
            | Variable v -> loop v
            | ls  -> raise (SubstitutionException (IncompatibleTypes (var, ls, "mapping")))) in
      loop var
  and substitute_mapping_or_variable_direct map (s: _ or_variable) : _ or_variable =
    match s with
    | TyDirect (MkTyMapping v) ->
      TyDirect (MkTyMapping (substitute_or_union substitute_mapping_or_variable_internal map v))
    | TyVariable var ->
      let rec loop var = 
        match TypeVariable.Map.find map var with
        | None -> TyVariable var
        | Some (vl: variable_type) -> (match vl with
            | Mapping sc -> TyDirect (substitute_mapping map sc)
            | Variable v -> loop v
            | VAny  -> raise (SubstitutionException (IncompatibleTypes (var, VAny, "mapping<-any")))
            | ls  -> raise (SubstitutionException (IncompatibleTypes (var, ls, "mapping")))) in
      loop var
  and substitute_mapping map (s: _) =
    match s with MkTyMapping v -> MkTyMapping (substitute_or_union substitute_mapping_or_variable_internal map v)
  and substitute_scene_type_or_variable map (st: scene_type or_variable) : scene_type or_variable =
    match st with
    | TyDirect v -> TyDirect (substitute_scene_type map v)
    | TyVariable var ->
      let rec loop var = 
        match TypeVariable.Map.find map var with
        | None -> TyVariable var
        | Some (vl: variable_type) -> (match vl with
            | Internal (Type.TyScene sc)
            | Scene sc -> TyDirect (substitute_scene_type map sc)
            | Variable v -> loop v
            | ls  -> raise (SubstitutionException (IncompatibleTypes (var, ls, "scene_type")))) in
      loop var
  and substitute_internal_type_or_variable map (st: internal_type_expr or_variable) =
    match st with
    | TyDirect v -> TyDirect (substitute_internal_type_expr map v)
    | TyVariable var ->
      let rec loop var = match TypeVariable.Map.find map var with
        | None -> TyVariable var
        | Some (vl: variable_type) ->
          (match vl with
           | Scalar sc -> TyDirect (TyPure (TyScalar sc))
           | Pure sc -> TyDirect (TyPure (substitute_pure_data map sc))
           | Texture tt -> TyDirect (TyTexture tt)
           | Scene sc -> TyDirect (TyScene (substitute_scene_type map sc))
           | Internal sc -> TyDirect (substitute_internal_type_expr map sc)
           | Variable v -> loop v
           | ls  -> raise (SubstitutionException (IncompatibleTypes (var, ls, "internal_type")))) in
      loop var
  and substitute_int_or_variable map st =
    match st with
    | TyDirect v -> TyDirect v
    | TyVariable var ->
      match TypeVariable.Map.find map var with
      | None -> TyVariable var
      | Some (vl: variable_type) -> (match vl with
          | Int sc -> TyDirect sc
          | Variable v -> TyVariable v
          | ls  -> raise (SubstitutionException (IncompatibleTypes (var, ls, "int"))))
  and substitute_type_expr map (TypeExpr st: type_expr) =
    TypeExpr (substitute_internal_type_or_variable map st)

  let variables_or_variable f set (var: 'a or_variable) =
    match var with | TyDirect v -> f set v | TyVariable v -> TypeVariable.Set.add set v

  let variables_or_any f set (var: 'a or_any) =
    match var with | Specialized v -> f set v | Any -> set

  let variables_non_scalar_type set (ns: non_scalar_type) = match ns with
    | TyVec2 { value; _ }
    | TyVec3 {value; _}
    | TyVec4 {value;_}
    | TyMat2 {value;_}
    | TyMat3 {value;_}
    | TyMat4 {value;_} -> variables_or_variable (fun set _ -> set) set value

  let variables_pure_data_type set (pdt: pure_data_type) = match pdt with
    | TyNumericData _
    | TyScalar _ -> set
    | TyNonScalar ns -> variables_non_scalar_type set ns

  let fold_typemap f set (tp: 'a typemap) =
    IDMap.fold tp ~init:set ~f:(fun ~key:_ ~data set -> f set data)

  let fold_union f set (tp: 'a or_union) =
    match tp with | Base v -> f set v | Union ls -> List.fold ls ~init:set ~f:(fun set data -> f set data)


  let variables_mapping_internal set (pt1, pt2, tt1) =
    let set =
      fold_union
        (variables_or_variable (fold_typemap (variables_or_variable (variables_pure_data_type)))) set pt1 in
    let set =
      fold_union
        (variables_or_variable (fold_typemap (variables_or_variable (variables_pure_data_type)))) set pt2 in
    let set =
      fold_union
        (variables_or_variable (fold_typemap (variables_or_variable (fun set _ -> set)))) set tt1 in
    set

  let variables_mapping set (MkTyMapping v) =
    fold_union variables_mapping_internal set v


  let rec variables_scene_type set (st: scene_type) = match st with
    | TyModifier (m1, m2, _)
    | TyObject (m1, m2, _) ->
      let set = variables_or_variable (variables_or_any variables_mapping) set m1 in
      let set = variables_or_variable (variables_or_any variables_mapping) set m2 in
      set
    | TyLight (m1, _) 
    | TyUnresolvedModifier m1 ->
      let set = variables_or_variable variables_mapping set m1 in
      set      
    | TyDrawable (m2, i1,_) ->
      let set = variables_or_variable (variables_or_any variables_mapping) set m2 in
      let set = variables_or_variable (fun set _ -> set) set i1 in
      set
    | TyShader (m1, i1, _) ->
      let set = variables_or_variable variables_mapping set m1 in
      let set = variables_or_variable (fun set _ -> set) set i1 in
      set
    | TyIfScene st
    | TyMulti st ->
      variables_or_variable (variables_scene_type) set st
    | TyObjectFrom (modifiers, base_obj) ->
      let set = List.fold
          modifiers
          ~init:set
          ~f:(variables_or_variable variables_scene_type) in
      variables_or_variable variables_scene_type set base_obj
    | TyCompoundObjectFrom (objs) ->
      let set = fold_ne_list
          objs
          ~init:set
          ~f:(variables_or_variable variables_scene_type) in
      set
    | TyDrawableFrom (shader, obj) ->
      let set = List.fold
          [shader;obj]
          ~init:set
          ~f:(variables_or_variable variables_scene_type) in
      set





  let rec variables_internal_type_expr set (itt: internal_type_expr) = match itt with
    | TyPure pure -> variables_pure_data_type set pure
    | TyTexture _ -> set
    | TyScene st -> variables_scene_type set st
    | TyList st -> variables_or_variable (variables_internal_type_expr) set st

  let variables_type_expr ?(set=TypeVariable.Set.empty) (TypeExpr itt: type_expr) =
    variables_or_variable variables_internal_type_expr set itt

  let dependencies (constr: constr) = match constr with
    | `MappingEq (_, Specialized m) -> variables_mapping TypeVariable.Set.empty m
    | `MappingEq (_, Any)
    | `IntEq (_, _) -> TypeVariable.Set.empty
    (* | `SubType (_, t1)  *)
    | `Indexed (_, t1)
    | `Eq (_, t1) -> variables_type_expr t1
  (* | `UnionOf (_, ts) ->
   *   List.fold ~init:(TypeVariable.Set.empty) ~f:(fun set ty -> variables_type_expr ~set ty) ts
   * | `ObjectFrom (_, t1, _) ->
   *   List.fold ~init:(TypeVariable.Set.empty) ~f:(fun set ty -> variables_type_expr ~set ty) (t1 :: [])      
   * | `DrawableFrom (_, t1, t2) ->
   *   List.fold ~init:(TypeVariable.Set.empty) ~f:(fun set ty -> variables_type_expr ~set ty) ( t1 :: t2 :: []) *)


  let exports (constr: constr) = match constr with
    | `MappingEq (t1, _) 
    | `IntEq (t1, _) -> TypeVariable.Set.singleton t1
    (* | `SubType (t1, _) *)
    | `Indexed (t1, _) 
    (* | `UnionOf (t1, _)  *)
    (* | `DrawableFrom (t1, _, _)  *)
    | `Eq (t1, _) -> variables_type_expr t1
    (* | `ObjectFrom (t1, _, ts) ->
     *   List.fold ~init:(TypeVariable.Set.empty) ~f:(fun set ty -> variables_type_expr ~set ty) ( t1 :: ts) *)



end



module Normalize = struct

  let empty_mapping = Type.MkTyMapping (Union [])

  module Unification = struct

    open Type

    type unification_error =
      | UnboundShaderRequirement of IdentifierTable.m * t * type_expr typemap * type_expr typemap
    [@@deriving show]

    exception UnificationException of unification_error

    let () = Stdlib.Printexc.register_printer (function
          UnificationException v ->
          Some (show_unification_error v)
        | _ -> None
      )


    let unify_typemap eq (to_exp: 'a -> t) map tmap : 'a typemap * constr list =
      let map = IDMap.merge map tmap
          ~f:(fun ~key:_ data  ->
              match data with
              | `Both (a,b) ->
                if eq a b then Some (a, None)
                else Some (a, Some (`Eq (to_exp a, to_exp b)))
              | `Left v -> Some (v, None)
              | `Right v -> Some (v, None)) in
      let right_map =
        let right_map =
          IDMap.filter_map map ~f:(fun (_, v) -> v)
          |> IDMap.to_alist
          |> List.map ~f:(fun (_, v) -> v) in
        right_map in
      let left_map = IDMap.map map ~f:(fun (v, _) -> v) in
      left_map, right_map    

    let intersect_typemap eq (to_exp: 'a -> t) map tmap : 'a typemap * constr list =
      let map = IDMap.merge map tmap
          ~f:(fun ~key:_ data  ->
              match data with
              | `Both (a,b) ->
                if eq a b then Some (a, None)
                else Some (a, Some (`Eq (to_exp a, to_exp b)))
              (* ignore entries in only one of the maps  *)
              | `Left _ -> None
              | `Right _ -> None) in
      let right_map =
        let right_map =
          IDMap.filter_map map ~f:(fun (_, v) -> v)
          |> IDMap.to_alist
          |> List.map ~f:(fun (_, v) -> v) in
        right_map in
      let left_map = IDMap.map map ~f:(fun (v, _) -> v) in
      left_map, right_map    


    let unify_mapping_internal (MkTyMapping m1: mapping) (MkTyMapping m2: mapping) : mapping * constr list =
      let (attribute_map, uniform_map, texture_map) = 
        let unwrap a = match a with Base v -> [v] | Union ls -> ls in
        let flat = List.concat_map ~f:unwrap in
        let m1 = unwrap m1 |> List.unzip3 |> (fun (a,b,c) -> flat a, flat b, flat c) in
        let m2 = unwrap m2 |> List.unzip3 |> (fun (a,b,c) -> flat a, flat b, flat c) in
        let tuple3con (a,b,c) (d,e,f) = (a @ d, b @ e, c @ f) in
        tuple3con m1 m2 in
      let expand ls = List.map ~f:(fun v -> match v with TyDirect v -> Some v | _ -> None) ls
                      |> Option.all in
      match expand attribute_map, expand uniform_map, expand texture_map with
      | Some attribute_map, Some uniform_map, Some texture_map ->
        let expand e f ls =
          List.fold_map ls ~init:IDMap.empty ~f:(unify_typemap e f)
          |> (fun (map, cons) -> (map, List.concat cons)) in

        let pdt_e = Type.(equal_or_variable equal_pure_data_type) in
        let pdt_f = Type.(fun v -> match v with
            | TyDirect v -> TypeExpr (TyDirect (TyPure v))
            | TyVariable var -> TypeExpr (TyVariable var)) in

        let tt_e = Type.(equal_or_variable equal_texture_type) in
        let tt_f = Type.(fun v -> match v with
            | TyDirect v -> TypeExpr (TyDirect (TyTexture v))
            | TyVariable var -> TypeExpr (TyVariable var)) in
        let attribute_map, c1 = expand pdt_e pdt_f attribute_map in
        let uniform_map, c2 = expand pdt_e pdt_f uniform_map in
        let texture_map, c3 = expand tt_e tt_f texture_map in
        let constraints = c1 @ c2 @ c3 in
        MkTyMapping (Base (
            Base (TyDirect attribute_map),
            Base (TyDirect uniform_map),
            Base (TyDirect texture_map))), constraints
      | _ -> MkTyMapping (Base (Union attribute_map, Union uniform_map, Union texture_map)), []

    let unify_mapping (m1: [`Specialized of mapping | `Any ]) (m2: [`Specialized of mapping | `Any])
      : [`Specialized of mapping | `Any ] * constr list =
      match m1, m2 with
      | `Any, `Any -> `Any, []
      | `Specialized m1, `Any -> `Specialized m1, []
      | `Any, `Specialized m2 -> `Specialized m2, []
      | `Specialized m1, `Specialized m2 ->
        unify_mapping_internal m1 m2 |> (fun (a,b) -> (`Specialized a, b))

    let intersect_mapping_internal (MkTyMapping m1: mapping) (MkTyMapping m2: mapping) : mapping * constr list =
      let (attribute_map, uniform_map, texture_map) = 
        let unwrap a = match a with Base v -> [v] | Union ls -> ls in
        let flat = List.concat_map ~f:unwrap in
        let m1 = unwrap m1 |> List.unzip3 |> (fun (a,b,c) -> flat a, flat b, flat c) in
        let m2 = unwrap m2 |> List.unzip3 |> (fun (a,b,c) -> flat a, flat b, flat c) in
        let tuple3con (a,b,c) (d,e,f) = ((a @ d), (b @ e), (c @ f)) in
        tuple3con m1 m2 in
      let expand ls = List.map ~f:(fun v -> match v with TyDirect v -> Some v | _ -> None) ls
                      |> Option.all in
      match expand attribute_map,  expand uniform_map, expand texture_map with
      | Some attribute_map, Some uniform_map, Some texture_map ->

        let expand e f ls =
          match ls with
          | h :: t ->
            let (res,cons) = List.fold_map t ~init:h ~f:(intersect_typemap e f)
                             |> (fun (map, cons) -> (map, List.concat cons)) in
            (res,cons)
          | _ -> IDMap.empty, []
        in

        let pdt_e = Type.(equal_or_variable equal_pure_data_type) in
        let pdt_f = Type.(fun v -> match v with
            | TyDirect v -> TypeExpr (TyDirect (TyPure v))
            | TyVariable var -> TypeExpr (TyVariable var)) in

        let tt_e = Type.(equal_or_variable equal_texture_type) in
        let tt_f = Type.(fun v -> match v with
            | TyDirect v -> TypeExpr (TyDirect (TyTexture v))
            | TyVariable var -> TypeExpr (TyVariable var)) in
        let attribute_map, c1 = expand pdt_e pdt_f attribute_map in
        let uniform_map, c2 = expand pdt_e pdt_f uniform_map in
        let texture_map, c3 = expand tt_e tt_f texture_map in
        let constraints = c1 @ c2 @ c3 in
        MkTyMapping (Base (
            Base (TyDirect attribute_map),
            Base (TyDirect uniform_map),
            Base (TyDirect texture_map))), constraints
      | _ -> MkTyMapping (Base (Union (attribute_map), Union uniform_map, Union texture_map)), []

    let intersect_mapping
        (m1: [`Any | `Specialized of mapping])
        (m2: [`Any | `Specialized of mapping]) : [`Specialized of mapping | `Any ] * constr list =
      match m1, m2 with
      | `Any, `Any -> `Any, []
      | `Specialized m1, `Any -> `Specialized m1, []
      | `Any, `Specialized m2 -> `Specialized m2, []
      | `Specialized m1, `Specialized m2 ->
        intersect_mapping_internal m1 m2 |> (fun (a,b) -> (`Specialized a, b))

    let constrain_requirements_typemap e f (supplied: 'a typemap) (req: 'a typemap) : constr list  =
      let res = IDMap.merge supplied req ~f:(fun ~key v ->
          match v with
          | `Both (a,b) ->
            if e a b
            then None
            else Some (`Eq (f a, f b))
          | `Left _ -> None
          | `Right b -> raise (UnificationException
                                 (UnboundShaderRequirement
                                    (key, f b,
                                     IDMap.map ~f supplied,
                                     IDMap.map ~f req)
                                 ))
        ) |> IDMap.to_alist |> List.map ~f:(fun (_,b) -> b) in
      res


    let constrain_requirements_internal (MkTyMapping m1: mapping) (MkTyMapping m2: mapping) =
      let pdt_e = Type.(equal_or_variable equal_pure_data_type) in
      let pdt_f = Type.(fun v -> match v with
          | TyDirect v -> TypeExpr (TyDirect (TyPure v))
          | TyVariable var -> TypeExpr (TyVariable var)) in
      let tt_e = Type.(equal_or_variable equal_texture_type) in
      let tt_f = Type.(fun v -> match v with
          | TyDirect v -> TypeExpr (TyDirect (TyTexture v))
          | TyVariable var -> TypeExpr (TyVariable var)) in
      let expand m1 = 
        let unwrap a = match a with Base v -> [v] | Union ls -> ls in
        let flat = List.concat_map ~f:unwrap in
        let (attr,unif,text) = unwrap m1 |> List.unzip3 |> (fun (a,b,c) -> flat a, flat b, flat c) in
        let expand ls = List.map ~f:(fun v -> match v with TyDirect v -> Some v | _ -> None) ls
                        |> Option.all in
        match expand attr, expand unif, expand text with
        | Some attribute_map, Some uniform_map, Some texture_map ->
          let expand e f ls =
            List.fold_map ls ~init:IDMap.empty ~f:(unify_typemap e f)
            |> (fun (map, cons) -> (map, List.concat cons)) in
          let attribute_map, c1 = expand pdt_e pdt_f attribute_map in
          let uniform_map, c2 = expand pdt_e pdt_f uniform_map in
          let texture_map, c3 = expand tt_e tt_f texture_map in
          Some ((attribute_map, uniform_map, texture_map), c1 @ c2 @ c3)
        | _ -> None
      in
      match expand m1, expand m2 with
      | Some ((m1_attr, m1_uni, m1_tt), c1), Some ((m2_attr, m2_uni, m2_tt), c2) ->
        let c3 = constrain_requirements_typemap pdt_e pdt_f m1_attr m2_attr in
        let c4 = constrain_requirements_typemap pdt_e pdt_f m1_uni m2_uni in
        let c5 = constrain_requirements_typemap tt_e tt_f m1_tt m2_tt in
        Some (c1 @ c2 @ c3 @ c4 @ c5)
      | _ -> None

    let constrain_requirements (m1: [`Any | `Specialized of mapping]) (m2: mapping) =
      match m1 with
      | `Any -> Some ([])
      | `Specialized m1 ->
        constrain_requirements_internal m1 m2

  end


  open Type

  type normalize_error =
    | InvalidShaderType of scene_type 
    | InvalidDrawableType of scene_type
    | InvalidModifierType of scene_type
    | InvalidObjectType of scene_type
    | IfOnlyDrawable of  Location.t
    | EmptyCompoundObject of Location.t
  [@@deriving show, sexp]

  exception NormalizeException of normalize_error


  let () = Stdlib.Printexc.register_printer (function
        NormalizeException v ->
        Some (show_normalize_error v)
      | _ -> None
    )




  type 'a unwrapped_or_any = [ `Any | `Specialized of 'a ]
  [@@deriving show]

  type unwrapped_modifier_type = 
    [ `Modifier of mapping unwrapped_or_any * mapping unwrapped_or_any * Location.t
    | `Multi of unwrapped_modifier_type | `Unresolved of mapping ]
  [@@deriving show]


  type unwrapped_object_type =
    [ `CompoundObjectFrom of unwrapped_object_type non_empty_list
    | `IfScene of unwrapped_object_type | `Light of mapping * Location.t
    | `Multi of unwrapped_object_type
    | `Object of mapping unwrapped_or_any * mapping unwrapped_or_any * Location.t
    | `ObjectFrom of unwrapped_modifier_type list * unwrapped_object_type ]
  [@@deriving show]

  type unwrapped_shader_type = [ `Shader of mapping * int or_variable * Location.t ]
  [@@deriving show]

  type unwrapped_drawable_type = [
      `Drawable of  mapping unwrapped_or_any * int or_variable * Location.t
    | `DrawableFrom of unwrapped_shader_type * unwrapped_object_type
    | `Multi of unwrapped_drawable_type
  ] [@@deriving show]

  let normalize_or_variable f (var: 'a or_variable) =
    match var with
    | TyDirect v -> let v, c1 = f v in TyDirect v, c1
    | TyVariable var -> TyVariable var, []

  let rec unwrap_modifier (st: scene_type or_variable) : unwrapped_modifier_type option =
    match st with
    | TyVariable _ -> None
    | TyDirect v -> match v with
      | TyUnresolvedModifier (TyVariable _)
      | TyModifier (TyVariable _ , _, _)
      | TyModifier (_ , TyVariable _, _) -> None
      | TyModifier (TyDirect Any, TyDirect Specialized light, loc) ->
        Some (`Modifier (`Any, `Specialized light, loc))
      | TyModifier (TyDirect Specialized data, TyDirect Any, loc) ->
        Some (`Modifier (`Specialized data, `Any, loc))
      | TyModifier (TyDirect Any, TyDirect Any, loc) ->
        Some (`Modifier (`Any, `Any, loc))
      | TyModifier (TyDirect Specialized data, TyDirect Specialized light, loc) ->
        Some (`Modifier (`Specialized data, `Specialized light, loc))
      | TyUnresolvedModifier (TyDirect data) -> Some (`Unresolved data)
      | TyMulti v -> begin match unwrap_modifier v with
          | None -> None
          | Some vl -> Some (`Multi vl)
        end
      | v -> raise (NormalizeException (InvalidModifierType v))

  let rec unwrap_object (obj: scene_type or_variable) : unwrapped_object_type option =
    match obj with
    | TyVariable _ -> None
    | TyDirect st -> match st with
      | TyObject (TyVariable _, _, _) -> None
      | TyObject (_, TyVariable _, _) -> None

      | TyObject (TyDirect Specialized data, TyDirect Specialized light, loc) ->
        Some (`Object (`Specialized data,`Specialized light, loc))

      | TyObject (TyDirect Any, TyDirect Specialized light,loc) ->
        Some (`Object (`Any,`Specialized light, loc))

      | TyObject (TyDirect Specialized data, TyDirect Any,loc) ->
        Some (`Object (`Specialized data,`Any,loc))

      | TyObject (TyDirect Any, TyDirect Any,loc) ->
        Some (`Object (`Any,`Any,loc))

      | TyObjectFrom (modifiers, obj) -> begin
          match List.map ~f:unwrap_modifier modifiers |> Option.all with
          | None -> None
          | Some modifiers ->
            match unwrap_object obj with
            | None -> None
            | Some obj -> Some (`ObjectFrom (modifiers, obj))
        end
      | TyCompoundObjectFrom ls ->
        begin match map_ne_list ~f:unwrap_object ls |> option_all_ne_list with
          | None -> None
          | Some st -> Some (`CompoundObjectFrom st)
        end
      | TyLight (TyVariable _, _) -> None
      | TyLight (TyDirect ls, loc) -> Some (`Light (ls, loc))
      | TyIfScene st -> begin match unwrap_object st with
          | None -> None
          | Some st -> Some (`IfScene st) 
        end
      | TyMulti st -> begin match unwrap_object st with
          | None -> None
          | Some st -> Some (`Multi st) 
        end
      | v -> raise (NormalizeException (InvalidObjectType v))

  let unwrap_shader (st: scene_type or_variable) : unwrapped_shader_type option =
    match st with
    | TyVariable _ -> None
    | TyDirect v -> match v with
      | TyShader (TyDirect input_req, count, loc) -> Some (`Shader (input_req, count, loc))
      | TyShader (_, _, _) -> None
      | v -> raise (NormalizeException (InvalidShaderType v))

  let rec unwrap_drawable (st: scene_type or_variable) : unwrapped_drawable_type option =
    match st with
    | TyVariable _ -> None
    | TyDirect v -> match v with
      | TyDrawableFrom (shader, obj) ->
        let shader = unwrap_shader shader in
        let obj = unwrap_object obj in
        begin match shader, obj with
          | Some shader, Some obj -> Some (`DrawableFrom (shader,obj))
          | _ -> None
        end
      | TyDrawable (TyDirect Specialized light_mapping, count,loc) ->
        Some (`Drawable (`Specialized light_mapping, count,loc))

      | TyDrawable (TyDirect Any, count,loc) ->
        Some (`Drawable (`Any, count, loc))

      | TyDrawable (TyVariable _, _, _) ->
        None


      | TyMulti st -> begin match unwrap_drawable st with
          | None -> None
          | Some v -> Some (`Multi v)
        end
      | v -> raise (NormalizeException (InvalidDrawableType v))


  let rec propagate_multi_internal obj = match obj with
    | `ObjectFrom (modifiers, obj) ->
      let any_multi_modifiers =
        List.exists modifiers ~f:(fun v -> match v with `Multi _ -> true | _ -> false) in begin
        let any_multi, res = match propagate_multi_internal obj with
          | `Multi v -> true, v
          | v -> any_multi_modifiers, v
        in
        if any_multi
        then `Multi (`ObjectFrom (modifiers, res))
        else `ObjectFrom (modifiers, res)
      end
    | `CompoundObjectFrom vs ->
      let any_multi, vs =
        fold_map_ne_list vs ~init:false
          ~f:(fun any_multi v -> match propagate_multi_internal v with `Multi v -> true, v | v -> any_multi, v) in
      if any_multi then `Multi (`CompoundObjectFrom vs) else `CompoundObjectFrom vs
    | `IfScene v ->
      (match propagate_multi_internal v with `Multi v -> `Multi (`IfScene v) | v -> `IfScene v)
    | `Multi v -> `Multi (`Multi (propagate_multi_internal v))
    | v -> v
  and flatten_multi obj = match obj with
    | `ObjectFrom (modifiers, obj) -> `ObjectFrom (modifiers, flatten_multi obj)
    | `CompoundObjectFrom objs -> `CompoundObjectFrom (map_ne_list ~f:flatten_multi objs)
    | `IfScene obj -> `IfScene (flatten_multi obj)
    | `Multi obj -> begin match flatten_multi obj with
        | `Multi v -> `Multi v
        | v -> `Multi v
      end
    | v -> v
  and propagate_multi obj = propagate_multi_internal obj |> flatten_multi

  let rec propagate_multi_drawable_internal (drawable: unwrapped_drawable_type) =
    match drawable with
    | `DrawableFrom (shader, obj) -> begin
        match propagate_multi_internal obj with
        | `Multi obj -> `Multi (`DrawableFrom (shader, flatten_multi obj))
        | obj -> `DrawableFrom (shader, flatten_multi obj)
      end
    | `Drawable (light_mapping, count, loc) -> `Drawable (light_mapping, count, loc)
    | `Multi v -> `Multi (`Multi (propagate_multi_drawable_internal v))
  and flatten_multi_drawable (drawable: unwrapped_drawable_type) =
    match drawable with
    | `Multi obj -> begin match flatten_multi_drawable obj with
        | `Multi v -> `Multi v
        | v -> `Multi v
      end
    | v -> v
  and propagate_multi_drawable drawable = propagate_multi_drawable_internal drawable |> flatten_multi_drawable

  let rec expand_into_compound_object obj  =
    match obj with
    | `CompoundObjectFrom v -> List.concat_map ~f:expand_into_compound_object (fst v :: snd v)
    | `IfScene obj -> [`IfScene obj]
    | `Light (mapping, loc) -> [`Light (mapping, loc)]
    | `Multi unwrapped_object_type ->  [`Multi unwrapped_object_type]
    | `Object (m1,m2,loc) ->  [`Object (m1,m2,loc)]
    | `ObjectFrom (a,b) -> [`ObjectFrom (a,b)]

  let rec expand_modifier modifier  = match modifier with
    | `Modifier (a,b,loc) -> `Left (a,b, loc)
    | `Unresolved c -> `Right c
    | `Multi v -> expand_modifier v

  let rec expand_unwrapped_object v = match v with
    | `Light (map,loc) -> `Light (map,loc)
    | `Object (a,b, loc) -> `Object (a,b,loc)
    | `IfScene v
    | `Multi v -> expand_unwrapped_object v


  let rec normalize_unwrapped_object_type_internal (_obj: unwrapped_object_type) =
    match _obj with
    | `ObjectFrom (modifiers, obj) ->
      let obj, c1 =
        normalize_unwrapped_object_type_internal obj
        |> (fun (a,b) -> expand_unwrapped_object a, b) in
      let modifiers = List.map ~f:expand_modifier modifiers in
      begin match obj with
        | `Light (v, loc) ->
          let modifiers = List.concat_map modifiers ~f:(function `Left (a,b,_) -> [a;b]
                                                               | `Right a -> [`Specialized a]) in
          let light, constraints =
            List.fold_map (v :: modifiers)
              ~init:`Any ~f:(Unification.unify_mapping)
            |> (fun (a,b) -> (a, List.concat b)) in
          let result = `Light (light, loc) in

          result, c1 @ constraints 
        | `Object (data,light,loc) ->
          let data, light =
            List.map modifiers ~f:(function `Left (a,b,_) -> a, Some b | `Right a -> `Specialized a, None)
            |> List.unzip
            |> (fun (a,b) -> data :: a, light :: List.filter_opt b) in
          let data, c2 =
            match data with
            | [] -> `Any, []
            | _ -> 
              List.fold_map data
                ~init:`Any ~f:(Unification.unify_mapping)
              |> (fun (a,b) -> (a, List.concat b)) in
          let light, c3 =
            match light with
            | [] -> `Any, []
            | _ ->
              List.fold_map light
                ~init:`Any ~f:(Unification.unify_mapping)
              |> (fun (a,b) -> (a, List.concat b)) in
          let result = `Object (data, light,loc) in
          result, c1 @ c2 @ c3
      end
    | `IfScene v ->
      let (v, c1) = normalize_unwrapped_object_type_internal v in `IfScene v, c1
    | `Multi v ->
      let (v, c1) = normalize_unwrapped_object_type_internal v in `Multi v, c1
    | `Object v -> `Object v, []
    | `Light (v,loc) -> `Light (`Specialized v,loc), []
    | `CompoundObjectFrom ls ->
      let ls = List.concat_map ~f:expand_into_compound_object (fst ls :: snd ls) in
      let ls, c1 = List.map ls ~f:normalize_unwrapped_object_type_internal
                   |> List.unzip |> (fun (a,b) -> a, List.concat b) in
      let rec split v =
        match v with
        | `Light (m,loc) -> `Right (`Light (m,loc))
        | `Object (`Any, b,loc) -> `Right (`Object (`Any, b,loc))
        | `Object (a,b,loc) -> `Left (`Object (a,b,loc))
        | `IfScene v -> begin match split v with
            | `Left v -> `Right v
            | `Right v -> `Right v
          end
        | `Multi v -> match split v with
          | `Left l -> `Left l
          | `Right l -> `Right l in
      let direct_maps, indirect_maps =
        List.map ls
          ~f:(fun l -> match split l with `Left l -> (Some l, None) | `Right l -> (None, Some l))
        |> List.unzip |> (fun (a,b) -> List.filter_opt a, List.filter_opt b) in
      begin match direct_maps,indirect_maps with
        | [],[] -> assert false
        | [], h :: _ -> let loc = match h with `Light (_,loc) -> loc | `Object (_,_,loc) -> loc in
          raise (NormalizeException (EmptyCompoundObject loc))
        | _ ->
          let ls = direct_maps @ indirect_maps in
          let (data, light, loc), constraints =
            List.fold_map ~init:(`Any, `Any, default_loc) ls ~f:(fun (data, light,loc) v -> match v with
                | `Light (mapping,loc') -> begin
                    let light, c2 = Unification.intersect_mapping light mapping in
                    (data, light,union_loc loc loc'), c1 @ c2
                  end
                | `Object (data_mapping, light_mapping,loc) -> begin
                    let light, c2 = Unification.intersect_mapping light light_mapping in
                    let data, c3 = Unification.intersect_mapping data data_mapping in
                    (data, light, loc), c1 @ c2 @ c3
                  end
              ) in
          let constraints = List.concat constraints in
          match data, light with
          | `Any, `Specialized map -> `Light (`Specialized map, loc), constraints
          | `Any, `Any -> raise (NormalizeException (EmptyCompoundObject loc))
          | data, light -> `Object (data, light, loc), constraints
      end

  let rec expand_unwrapped_object_ifscene v =
    match v with
    | `Object (a,b,loc) -> `Direct (a,b,loc)
    | `Multi v -> expand_unwrapped_object_ifscene v
    | `Light (b,loc) -> `Light (b,loc)
    | `IfScene v -> match expand_unwrapped_object_ifscene v with
      | `IfScene v -> `IfScene v
      | `Direct (a,b,loc) -> `IfScene (`Direct (a,b,loc))
      | `Light (v,loc) -> `IfScene (`Light (v,loc))




  let rec normalize_unwrapped_drawable_type_internal (drawable: unwrapped_drawable_type) =
    match drawable with
    | `DrawableFrom (`Shader (requirements, count, _loc), ori_obj_type) ->
      let ori_obj_type = propagate_multi ori_obj_type in

      let ori_obj_type, c1 = normalize_unwrapped_object_type_internal ori_obj_type in

      let obj_type = expand_unwrapped_object_ifscene ori_obj_type in
      begin match obj_type with
        | `IfScene (`Direct (`Specialized _,`Specialized _, loc)) ->
          raise (NormalizeException (IfOnlyDrawable (loc)))
        | `IfScene (`Direct (`Specialized _,`Any, loc)) ->
          raise (NormalizeException (IfOnlyDrawable loc))
        | `IfScene (`Direct (`Any,`Specialized _, loc)) ->
          raise (NormalizeException (IfOnlyDrawable loc))
        | `IfScene (`Light (_, loc)) -> 
          raise (NormalizeException (IfOnlyDrawable loc))
        | `IfScene (`Direct (`Any,`Any, _)) -> assert false
        | `Direct (data_mapping, light_mapping, loc) ->
          let c2 = Unification.constrain_requirements data_mapping requirements in
          begin match c2 with
            | Some c2 -> Some (`Drawable (light_mapping, count, loc), c1 @ c2)
            | None -> None
          end
        | `Light (data_mapping, loc) ->
          let c2 = Unification.constrain_requirements (data_mapping) requirements in
          begin match c2 with
            | Some c2 -> Some (`Drawable (`Any, count, loc), c1 @ c2)
            | None -> None
          end

      end
    | `Multi v -> begin match normalize_unwrapped_drawable_type_internal v with
        | Some (v, c1) -> Some (`Multi v, c1)
        | None -> None
      end
    | `Drawable (light_mapping, count, loc) ->
      Some (`Drawable (light_mapping, count, loc), [])

  let wrap_unwrapped_or_any f (a: 'a unwrapped_or_any) =
    match a with | `Any -> Any | `Specialized v -> Specialized (f v)

  let rec wrap_normalized_unwrapped_object_type v =
    let ty_dir = (fun v -> v) in
    match v with
    | `Light (`Specialized mapping,loc) -> TyLight (TyDirect mapping, loc)
    | `Light (`Any,loc) -> TyLight (TyDirect empty_mapping, loc)
    | `Object (a, b,loc) -> TyObject (
        TyDirect (wrap_unwrapped_or_any ty_dir a),
        TyDirect (wrap_unwrapped_or_any ty_dir b),
        loc
      )
    | `IfScene v -> TyIfScene (TyDirect (wrap_normalized_unwrapped_object_type v))
    | `Multi v -> TyMulti (TyDirect (wrap_normalized_unwrapped_object_type v))

  let rec wrap_normalized_unwrapped_drawable_type v =
    match v with
    | `Drawable (light_mapping, count,loc) ->
      TyDrawable (TyDirect (wrap_unwrapped_or_any (fun v -> v) light_mapping), count, loc)
    | `Multi v -> TyMulti (TyDirect (wrap_normalized_unwrapped_drawable_type v))

  let normalize_unwrapped_object_type (obj: unwrapped_object_type) : scene_type * constr list =
    let obj = propagate_multi obj in
    normalize_unwrapped_object_type_internal obj
    |> (fun (a,b) -> wrap_normalized_unwrapped_object_type a, b) 

  let normalize_unwrapped_drawable_type (obj: unwrapped_drawable_type) : (scene_type * constr list) option =
    let obj = propagate_multi_drawable obj in
    normalize_unwrapped_drawable_type_internal obj
    |> Option.map ~f:(fun (a,b) -> wrap_normalized_unwrapped_drawable_type a, b) 

  let rec normalize_type_expr (TypeExpr itt: type_expr) =
    let itt, c1 = normalize_or_variable normalize_internal_type_expr itt in
    TypeExpr itt, c1
  and normalize_internal_type_expr (itt: internal_type_expr) = match itt with
    | TyScene st ->
      let st, c1 = normalize_scene_type st in      TyScene st, c1
    | TyList st ->
      let st,c1 = normalize_or_variable normalize_internal_type_expr st in
      TyList st, c1
    | v -> v, []
  and normalize_scene_type (st: scene_type) =
    (match st with
     | TyObject (_, _, _) 
     | TyObjectFrom (_, _)
     | TyCompoundObjectFrom _ 
     | TyLight _ 
     | TyIfScene _ -> begin
         let unwrapped = unwrap_object (TyDirect st) in
         match unwrapped with
         | None -> st, []
         | Some st -> normalize_unwrapped_object_type st
       end
     | TyMulti _ -> begin
         let unwrapped = try unwrap_object (TyDirect st) with NormalizeException _ -> None in
         match unwrapped with
         | Some st -> normalize_unwrapped_object_type st
         | None ->
           let unwrapped = try unwrap_drawable (TyDirect st) with NormalizeException _ -> None in
           match unwrapped with
           | Some _st -> Option.value ~default:(st, []) (normalize_unwrapped_drawable_type _st)
           | None -> st, []
       end
     | TyModifier (_, _, _) 
     | TyUnresolvedModifier _ 
     | TyShader (_, _, _) -> st, []
     | TyDrawableFrom (_, _)
     | TyDrawable (_, _, _) ->
       let unwrapped = unwrap_drawable (TyDirect st) in
       match unwrapped with
       | None -> st, []
       | Some _st ->
         Option.value ~default:(st, []) (normalize_unwrapped_drawable_type _st)
    )

  and normalize_constraint (con: constr) =
    match con with
    | `Eq (t1, t2) ->
      (* let t1, c1 = normalize_type_expr t1 in *) let c1 = [] in
      let t2, c2 = normalize_type_expr t2 in
      `Eq (t1, t2) :: c1 @ c2
    | `Indexed (t1, TypeExpr (TyDirect (TyList it))) ->
      [`Eq (t1, TypeExpr it)]
    | `Indexed (t1, TypeExpr (TyDirect (TyPure (TyNonScalar vs)))) ->
      let vs = match vs with TyVec2 v | TyVec3 v | TyVec4 v | TyMat2 v | TyMat3 v | TyMat4 v -> v in
      let Location.{value=vs; _} = vs in
      let vs = match vs with
          TyDirect sc -> TypeExpr (TyDirect (TyPure (TyScalar sc)))
        | TyVariable var -> TypeExpr (TyVariable var) in
      [`Eq (t1, vs)]
    | _ -> [con]

end

module ConstraintSort = TopologicalSort.Make (struct
  type t = Type.constr
  type dep = TypeVariable.t
  module Set = TypeVariable.Set

  let compare = Type.compare_constr


  let show =
    Printf.sprintf "%a" (fun () v -> Type.pp_constr (Format.str_formatter) v; Format.flush_str_formatter ())

  let show_dep = TypeVariable.show

  let dependencies = Substitution.dependencies
  let exports = Substitution.exports
end)


module ShaderAnalyzer = struct

  type analysis_error =
    | FileNotFound of string
    | UnsupportedShaderType of string
    | InvalidAttributeType of string * Type.texture_type
    | DuplicateAttributes of IdentifierTable.m
  [@@deriving show, eq]

  exception AnalysisException of analysis_error

  type shader_type =
    | Data of Type.pure_data_type
    | Texture of Type.texture_type

  let shader_type_to_data name = function
    | Data v -> v
    | Texture v -> raise (AnalysisException (InvalidAttributeType (name, v)))

  let split_shader_type = function
    | Data v -> Some v, None
    | Texture v -> None, Some v

  let to_type ~loc str = match str with
    | "float" -> Data (Type.TyScalar (Type.TyFloat loc))
    | "int" -> Data (Type.TyScalar (Type.TyInt loc))

    | "vec4" -> Data (Type.TyNonScalar (Type.TyVec4 {value=(Type.TyDirect (Type.TyFloat loc)); loc}))
    | "vec3" -> Data (Type.TyNonScalar (Type.TyVec3 {value=(Type.TyDirect (Type.TyFloat loc)); loc}))
    | "vec2" -> Data (Type.TyNonScalar (Type.TyVec2 {value=(Type.TyDirect (Type.TyFloat loc)); loc}))

    | "mat4" -> Data (Type.TyNonScalar (Type.TyMat4 {value=(Type.TyDirect (Type.TyFloat loc)); loc}))
    | "mat3" -> Data (Type.TyNonScalar (Type.TyMat3 {value=(Type.TyDirect (Type.TyFloat loc)); loc}))
    | "mat2" -> Data (Type.TyNonScalar (Type.TyMat2 {value=(Type.TyDirect (Type.TyFloat loc)); loc}))

    | "sampler1D" -> Texture (Type.TyTexture1D loc)
    | "sampler2D" -> Texture (Type.TyTexture2D loc)
    | "sampler3D" -> Texture (Type.TyTexture3D loc)
    | "samplerCube" -> Texture (Type.TyTextureCube loc)

    | _ -> raise (AnalysisException (UnsupportedShaderType str))


  let find_uniforms ~loc str =
    let uniform_regexp = Re2.of_string {|uniform +([a-zA-Z0-9]+) +([a-zA-Z0-9_]+) *;|} in
    let found_pos = Re2.get_matches uniform_regexp str in
    match found_pos with
    | Ok v ->
      List.map v ~f:(fun v -> to_type ~loc (Re2.Match.get_exn ~sub:(`Index 1) v), Re2.Match.get_exn ~sub:(`Index 2) v)
    | Error _ -> []

  let find_inputs ~loc str =
    let input_regexp = Re2.of_string {|in +([a-zA-Z0-9]+) +([a-zA-Z0-9_]+) *;|} in
    let found_pos = Re2.get_matches input_regexp str in
    match found_pos with
    | Ok v ->
      List.map v ~f:(fun v -> to_type ~loc (Re2.Match.get_exn ~sub:(`Index 1) v), Re2.Match.get_exn ~sub:(`Index 2) v)
    | Error _ -> []

  let find_frag_data str =
    let frag_regexp = Re2.of_string {|gl_FragData\[ *([a-zA-Z_0-9]+) *\] *=|} in
    let found_pos = Re2.get_matches frag_regexp str in
    match found_pos with
    | Ok v ->
      List.map v ~f:(fun v -> Re2.Match.get_exn ~sub:(`Index 1) v)
    | Error _ -> []

  let find_outputs ~loc str =
    let uniform_regexp = Re2.of_string {|(?:layout *\( *location *= *(?P<loc>[0-9_]+)\))? *out +([a-zA-Z0-9]+) +([a-zA-Z0-9_]+) *;|} in
    let found_pos = Re2.get_matches uniform_regexp str in
    match found_pos with
    | Ok v ->
      List.map v ~f:(fun v -> to_type ~loc (Re2.Match.get_exn ~sub:(`Index 2) v), Re2.Match.get_exn ~sub:(`Index 3) v, Re2.Match.get ~sub:(`Name "loc") v)
    | Error _ -> []


  let preprocess_str str =
    let single_line_comment_whitespace = Re2.of_string {|//.*\n|} in
    (* not 100% correct, sue me. *)
    let multi_line_comment_whitespace = Re2.of_string {|/\*(?:.|\n)*\*/|} in
    let str = Re2.replace_exn multi_line_comment_whitespace ~f:(fun _ -> "") str in
    let str = Re2.replace_exn single_line_comment_whitespace ~f:(fun _ -> "") str in
    str

  type output_spec =
    | NamedOutputs of (Type.pure_data_type * string * int) list * int
    | NumberedOutputs of (int list * int) option
  [@@deriving show]

  type shader_spec = {
    attributes: (Type.pure_data_type * string) list;
    uniforms: (Type.pure_data_type * string) list;  
    textures: (Type.texture_type * string) list;  
    outputs: output_spec;
  }
  [@@deriving show]

  let spec_to_type ~loc tbl fresh ({ attributes; uniforms; textures; outputs }: shader_spec) =
    let to_map_var elems =
      List.map elems ~f:(fun (ty, name) -> IdentifierTable.add_identifier tbl name, Type.TyDirect ty)
      |> IDMap.of_alist
      |> fun v -> (match v with
          | `Duplicate_key dup ->
            raise (AnalysisException (DuplicateAttributes dup))
          | `Ok map -> map) in
    let attributes = to_map_var attributes in
    let uniforms = to_map_var uniforms in
    let textures = to_map_var textures in
    let mapping =
      Type.MkTyMapping (Type.Base
                          (Type.Base
                             (Type.TyDirect attributes),
                           Type.Base (Type.TyDirect uniforms),
                           Type.Base (Type.TyDirect textures))) in
    let count = match outputs with
      | NamedOutputs (_, v) -> Type.TyDirect v
      | NumberedOutputs v -> (match v with | None -> Type.TyVariable (fresh ()) | Some (_, n) -> Type.TyDirect n)
    in
    let shader = Type.TyShader (Type.TyDirect mapping, count, loc) in
    shader





  type input_spec = | Filename of string | RawSource of string


  let calculate_shader_specification ~loc
      ~vertex_shader_file ~fragment_shader_file ?geometry_shader_file () =
    let open_data data = match data with
      | Filename string ->
        begin try preprocess_str (In_channel.read_all string)
          with
            Failure _msg -> raise (AnalysisException (FileNotFound string))
          | Sys_error _msg -> raise (AnalysisException (FileNotFound string))
        end
      | RawSource string -> preprocess_str string in
    let load_data opt = match opt with
      | None -> None
      | Some data -> Some (open_data data) in
    let vertex_shader = open_data vertex_shader_file in
    let fragment_shader = open_data fragment_shader_file in
    let geometry_shader = load_data geometry_shader_file in
    let all_sources = [vertex_shader; fragment_shader] @ Option.to_list geometry_shader in
    let uniforms = List.concat_map ~f:(find_uniforms ~loc) all_sources in
    let attributes = find_inputs ~loc vertex_shader in
    let attributes = List.map attributes ~f:(fun (typ, name) -> shader_type_to_data name typ, name) in

    let uniforms, textures = List.map uniforms ~f:(fun (typ, name) ->
        match typ with
        | Data v -> Some (v, name), None | Texture v -> None, Some (v, name)
      ) |> List.unzip in
    let uniforms, textures = List.filter_opt uniforms, List.filter_opt textures in
    let outputs =
      match find_outputs ~loc fragment_shader, find_frag_data fragment_shader with
      | [], []
      | _ :: _, _ :: _ -> raise (Failure "InvalidOutputSpecification")
      | ls, [] ->
        let max_out = ref (List.length ls) in
        let sanitize_out out =
          let value = 
            try Int.of_string out with Failure _ -> raise (Failure "InvalidPlacementPosition") in
          if value >= !max_out then max_out := value + 1;
          value in
        let unbound_elems, bound_elems = List.map ls ~f:(fun (typ, name, pos) -> match pos with
            | None -> Some (typ,name), None
            | Some pos -> None, Some (typ, name, sanitize_out pos))
                                         |> List.unzip in
        let unbound_elems, bound_elems = List.filter_opt unbound_elems, List.filter_opt bound_elems in
        let arr = Option_array.create ~len:!max_out in
        List.iter bound_elems ~f:(fun (typ, name, ind) ->
            if Option_array.is_some arr ind then raise (Failure "RepeatedPlacementAssignment");
            Option_array.set_some arr ind  (typ,name,ind)
          );
        let pos = ref 0 in
        let find_next_pos () = while Option_array.is_some arr !pos do incr pos done in
        List.iter unbound_elems ~f:(fun (typ, name) ->
            find_next_pos ();
            Option_array.set_some arr !pos (typ,name, !pos)
          );
        let output_bindings =
          List.range 0 (!max_out) |> List.map ~f:(fun ind -> Option_array.get arr ind) |> List.filter_opt in
        let output_bindings = List.map output_bindings ~f:(fun (shader_type, name, pos) ->
            let shader_type = shader_type_to_data name shader_type in
            shader_type, name, pos
          ) in
        NamedOutputs (output_bindings, !max_out)
      | [], ls ->
        let ls = List.map ls ~f:(fun ind -> try Some (Int.of_string ind) with Failure _ -> None)
                 |> Option.all in
        let ls = match ls with
          | None -> None
          | Some ls ->
            let max_elt = List.max_elt ~compare:Int.compare ls in
            Some (ls, Option.value_exn max_elt) in
        NumberedOutputs ls
    in
    {
      attributes;
      uniforms;
      textures;
      outputs;
    }

end

type 'a id_map_unwrapped = (IdentifierTable.m * 'a) list [@@deriving hash]

type 'a id_map = 'a IDMap.t [@@deriving eq, sexp, ord]
let fold_id_map f init map =
  IDMap.fold map ~init ~f:(fun ~key:_ ~data acc -> f data acc)
let iter_id_map f map = IDMap.iter map ~f
let map_id_map f map = IDMap.map map ~f

let hash_fold_id_map f hash_state map =
  hash_fold_id_map_unwrapped f hash_state (IDMap.to_alist map)

type wrap_option =
  | Repeat
  | MirroredRepeat
  | ClampToEdge
  | ClampToBorder
[@@deriving iter, map, eq,sexp,hash,ord]
type filter_option =
  | Nearest
  | Linear  
  | NearestMipmapNearest
  | NearestMipmapLinear
  | LinearMipmapNearest
  | LinearMipmapLinear
[@@deriving iter, map, eq,sexp,hash,ord]
type blend_func =
  | Zero
  | One
  | Src_color
  | One_minus_src_color
  | Dst_color
  | One_minus_dst_color
  | Src_alpha
  | One_minus_src_alpha
  | Dst_alpha
  | One_minus_dst_alpha
  | Constant_color
  | One_minus_constant_color
  | Constant_alpha
  | One_minus_constant_alpha
  | Src_alpha_saturate
[@@deriving iter, map, eq,sexp,hash,ord]
type comparison_func =
  | Stencil_never
  | Stencil_less
  | Stencil_lequal
  | Stencil_greater
  | Stencil_gequal
  | Stencil_equal
  | Stencil_notequal
  | Stencil_always
[@@deriving iter, map, eq,sexp,hash,ord]


type 'a expr =
  | Exp_bool of bool * 'a
  | Exp_number of Parsetree.num * 'a
  | Exp_variable of IdentifierTable.m * 'a
  | Exp_binop of (Parsetree.binop * 'a expr * 'a expr) * 'a
  | Exp_preop of (Parsetree.preop * 'a expr) * 'a
  | Exp_index of ('a expr * 'a expr)  * 'a
  | Exp_vector of 'a vector * 'a
  | Exp_list of 'a expr list * 'a
  | Exp_list_comprehension of ('a expr * IdentifierTable.m * 'a expr * 'a expr option) * 'a
  | Exp_function_call of IdentifierTable.m * 'a function_args * 'a
  | Exp_if_then_else of ('a expr * 'a expr * 'a expr) * 'a
and 'a function_args = FunctionArguments of
    {
      keyword_args: 'a expr id_map;
      positional_args: 'a expr list;
    }
and 'a vector =
  | Vector2 of ('a expr * 'a expr)
  | Vector3 of ('a expr * 'a expr * 'a expr)
  | Vector4 of ('a expr * 'a expr * 'a expr * 'a expr)

and 'a data =
  | Model of string * 'a         (* path to model *)
  | Data of (IdentifierTable.m* 'a expr) list * 'a (* list of name to value mappings  *)
  | Screen of {width: 'a expr option; height: 'a expr option; typ: 'a}
and 'a texture_option =
  | TextureBaseLevel of 'a expr 
  | TextureMaxLevel of 'a expr
  | TextureBorderColor of 'a expr
  | WrapS of wrap_option
  | WrapT of wrap_option
  | MinFilter of filter_option
  | MagFilter of filter_option
and 'a dimension_options =
  | DimensionVector of 'a expr
  | DimensionXZY of {x: 'a expr option; y: 'a expr option; z: 'a expr option }
and 'a rotate_options =
  | RotateAngle of {x: 'a expr option; y: 'a expr option; z: 'a expr option; vector: 'a expr option}
and 'a blend_color_option =
  | ColourVector of 'a expr
  (* | ColourArgs of {red: expr option; green: expr option; blue: expr option; alpha: expr option} *)
and 'a draw_option =
  | AlphaTest of 'a expr
  | BlendTest of 'a expr
  | DepthTest of 'a expr
  | StencilTest of 'a expr
  | BlendColor of 'a blend_color_option
  | BlendFunction of blend_func * blend_func
  | StencilFunction of comparison_func * 'a expr * 'a expr
  | DepthFunction of comparison_func
  | AlphaFunction of comparison_func * 'a expr
[@@deriving iter, map, eq,sexp,hash]


type 'a modifier =
  | Texture of IdentifierTable.m * string list * 'a texture_option list * 'a    (* texture name, texture file,  *)
  | Uniform of IdentifierTable.m * 'a expr * 'a                            (* uniform name, expression *)
  | Attribute of IdentifierTable.m * 'a expr * 'a
  | Translate of 'a dimension_options * 'a
  | Scale of 'a dimension_options * 'a
  | Rotate of 'a rotate_options * 'a
  | DrawOptions of 'a draw_option list * 'a
  | Redirecting of {
      textures: IdentifierTable.m list;
      depth: bool; stencil: bool;
      drawable: 'a drawable;
      typ: 'a;
    }
  | LightUniform of IdentifierTable.m * IdentifierTable.m * 'a expr * 'a
  | ModifierReference of 'a reference
and 'a base_object =
  | Light of {
      name: IdentifierTable.m;
      position: 'a expr;
      size: 'a expr;
      typ: 'a
    }
  | LightData of {
      light_tag: IdentifierTable.m;
      typ: 'a;
    }
  | ObjectData of 'a data
  | ObjectReference of 'a reference
  | CompoundObject of 'a obj_expr non_empty_list * 'a
and 'a obj = Object of 'a modifier list * 'a base_object * 'a
and 'a obj_expr =
  | PlainObject of 'a obj
  | ObjectIfThenElse of 'a expr * 'a obj_expr * 'a obj_expr option * 'a
  | ObjectFor of (IdentifierTable.m * 'a expr * 'a expr option * 'a obj_expr list)* 'a
and 'a reference =
  | DirectReference of IdentifierTable.m * 'a
  | IndirectReference of IdentifierTable.m * 'a macro_arguments * 'a
and src_or_filename =
  | Src of string
  | Filename of string
and 'a shader =
  | ShaderReference of 'a reference
  | Shader of {
      vertex_shader: string; fragment_shader: src_or_filename; geometry_shader: string option;
      shader_spec: ShaderAnalyzer.shader_spec;
      typ: 'a
    }
and 'a drawable =
  | OverlayingDrawable of 'a drawable * 'a
  | DrawableShader of 'a shader * 'a obj * 'a  
  | DrawableReference of 'a reference
and 'a scene_arg =
  | ArgReference of 'a reference
  | SceneDataExpression of 'a expr
  | SceneInlineExpression of 'a scene_expr
and 'a macro_arguments = 
    MacroArguments of {
      keyword_args: 'a scene_arg id_map; 
      positional_args: 'a scene_arg list;
    }
and 'a scene_expr =
  (* | SceneExprVariable of IdentifierTable.m loc
   * | SceneExprFuncall of (IdentifierTable.m* (stringoption * scene_arg) list) *)
  | SceneExprObject of 'a obj
  | SceneExprShader of 'a shader
  | SceneExprModifier of 'a modifier
  | SceneExprDrawable of 'a drawable
[@@deriving iter, map]

(* type 'a declaration =
 *   | DataFunction of
 *       { identifier: IdentifierTable.m;
 *         keyword_arguments: ('a expr) id_map;
 *         positional_arguments: (IdentifierTable.m * 'a) list;
 *         body: 'a expr;
 *       }
 *   | DataVariable of
 *       {
 *         identifier: IdentifierTable.m;
 *         body: 'a expr;
 *       }
 *   | MacroFunction of 
 *       { identifier: IdentifierTable.m;
 *         keyword_arguments: ('a scene_expr) id_map;
 *         positional_arguments: (IdentifierTable.m * 'a) list;
 *         body: 'a scene_expr
 *       }
 *   | MacroVariable of
 *       {
 *         identifier: IdentifierTable.m;
 *         body: 'a scene_expr;
 *       } *)
type 'a statement =
  (* | Declaration of 'a declaration *)
  | Drawable of 'a drawable
  | LayeredDrawable of string * 'a drawable
[@@deriving iter, map]


let with_hbox fmt f =
  let open Format in
  pp_open_hbox fmt ();
  f fmt ();
  pp_close_box fmt ()

let with_hvbox fmt n f =
  let open Format in
  pp_open_hvbox fmt n;
  f fmt ();
  pp_close_box fmt ()

let with_hovbox fmt n f =
  let open Format in
  pp_open_hovbox fmt n;
  f fmt ();
  pp_close_box fmt ()

let w_typ_helper ?(print_type=true) f fmt pp_v v ty = 
  let open Format in
  if print_type then
    pp_print_char fmt '(';
  pp_v fmt v;
  if print_type then 
    with_hbox fmt  (fun fmt () ->
        pp_print_char fmt ':';
        pp_print_space fmt ();
        with_hovbox fmt 1 (fun fmt () ->
            f fmt ty
          )
      );
  if print_type then
    pp_print_char fmt ')'



let rec pp_expr ?(print_type=true) f fmt (expr: 'a expr) =
  let open Format in
  let w_typ pp_v v ty = w_typ_helper ~print_type f fmt pp_v v ty in
  match expr with
  | Exp_bool (v, ty) -> w_typ pp_print_bool v ty
  | Exp_number (n, ty) -> (match n with
      | Parsetree.Int n -> w_typ pp_print_int n ty
      | Parsetree.Float n -> w_typ pp_print_float n ty)
  | Exp_variable (id, ty) ->
    w_typ IdentifierTable.pp_m id ty
  | Exp_binop (v, ty) ->
    w_typ (fun fmt ((binop, e1, e2): Parsetree.binop * 'a expr * 'a expr) ->
        with_hvbox fmt 1 (fun fmt () ->
            pp_expr ~print_type f fmt e1;
            pp_print_space fmt ();
            begin pp_print_string fmt (match binop with
                | Parsetree.Mul -> "*"
                | Parsetree.Div -> "/"
                | Parsetree.Mod -> "%"
                | Parsetree.Plus -> "+"
                | Parsetree.Minus -> "-"
                | Parsetree.And -> "&&"
                | Parsetree.Or -> "||")  end;
            pp_print_space fmt ();
            pp_expr ~print_type f fmt e2;
          )
      ) v ty
  | Exp_preop (v, ty) ->
    w_typ (fun fmt ((preop, e1): Parsetree.preop * _) ->
        with_hbox fmt (fun fmt () ->
            pp_print_string fmt (match preop with
                | Parsetree.UPlus -> "+"
                | Parsetree.UMinus -> "-"
                | Parsetree.UNot -> "!");
            pp_print_break fmt 0 0;
            pp_expr ~print_type f fmt e1
          )
      ) v ty
  | Exp_index (v, ty) ->
    w_typ (fun fmt ((e1,e2): 'a expr * 'a expr) ->
        with_hbox fmt (fun fmt () ->
            pp_expr ~print_type f fmt e1;
            pp_print_char fmt '[';
            with_hvbox fmt 1 (fun fmt () -> pp_expr ~print_type f fmt e2);
            pp_print_char fmt ']';
          )
      ) v ty
  | Exp_vector (vec, ty) ->
    w_typ (pp_vector ~print_type f) vec ty
  | Exp_list (elist, ty) ->
    w_typ (fun fmt elist ->
        pp_print_char fmt '[';
        with_hvbox fmt 1 (fun fmt () ->
            pp_print_list
              ~pp_sep:(fun fmt () -> pp_print_char fmt ','; pp_print_space fmt ())
              (pp_expr ~print_type f) fmt elist;
          );
        pp_print_char fmt ']';
      ) elist ty
  | Exp_list_comprehension (v, ty) ->
    w_typ (fun fmt ((e1, id, efrom, eto): 'a expr * IdentifierTable.m * 'a expr * 'a expr option) ->
        pp_print_char fmt '[';
        with_hbox fmt (fun fmt () ->
            pp_expr ~print_type f fmt e1;
            pp_print_space fmt ();
            pp_print_string fmt "for";
            pp_print_space fmt ();
            IdentifierTable.pp_m fmt id;
            pp_print_space fmt ();
            pp_print_string fmt "in";
            pp_print_space fmt ();
            pp_expr ~print_type f fmt efrom;
            begin match eto with
              | None -> ()
              | Some eto ->
                pp_print_space fmt ();
                pp_print_string fmt "to";
                pp_print_space fmt ();
                pp_expr ~print_type f fmt eto;
            end;
          );
        pp_print_char fmt ']'
      ) v ty
  | Exp_function_call (fname, args, ty) ->
    w_typ
      (fun fmt (fname, FunctionArguments { keyword_args; positional_args }) ->
         with_hbox fmt (fun fmt () ->
             IdentifierTable.pp_m fmt fname;
             pp_print_char fmt '(';
             with_hvbox fmt 1 (fun fmt () ->
                 pp_print_list
                   ~pp_sep:(fun fmt () -> pp_print_char fmt ','; pp_print_space fmt ())
                   (fun fmt (id,exp) ->
                      pp_open_hbox fmt ();
                      IdentifierTable.pp_m fmt id;
                      pp_print_break fmt 0 0;
                      pp_print_char fmt '=';
                      pp_print_break fmt 0 0;
                      pp_expr ~print_type f fmt exp;
                      pp_close_box fmt ();
                   ) fmt (IDMap.to_alist keyword_args);
                 begin match (IDMap.to_alist keyword_args) with
                   | [] -> ()
                   | _ :: _  -> pp_print_char fmt ','; pp_print_space fmt ()
                 end;
                 pp_print_list
                   ~pp_sep:(fun fmt () -> pp_print_char fmt ','; pp_print_space fmt ())
                   (pp_expr ~print_type f) fmt positional_args;

               );
             pp_print_char fmt ')';
           )
      ) (fname,args) ty
  | Exp_if_then_else (ift, ty) ->
    w_typ
      (fun fmt  ((cond, e1, e2):'a expr * 'a expr * 'a expr) ->
         pp_open_hvbox fmt 1;

         pp_open_hbox fmt ();
         pp_print_string fmt "if";
         pp_print_space fmt ();
         pp_expr ~print_type f fmt cond;
         pp_close_box fmt ();
         pp_print_space fmt ();

         pp_open_hbox fmt ();
         pp_print_string fmt "then";
         pp_print_space fmt ();
         pp_expr ~print_type f fmt e1;
         pp_close_box fmt ();
         pp_print_space fmt ();

         pp_open_hbox fmt ();
         pp_print_string fmt "else";
         pp_print_space fmt ();
         pp_expr ~print_type f fmt e2;
         pp_close_box fmt ();

         pp_close_box fmt ();
      )
      ift ty
and pp_vector ?(print_type=true) f fmt (vec: 'a vector) =
  let open Format in
  let pr_sep fmt () =
    pp_print_char fmt ';';
    pp_print_space fmt ();
  in
  let pp_boxed fmt f =
    pp_print_char fmt '[';
    pp_open_hvbox fmt 1;
    f fmt ();
    pp_close_box fmt ();
    pp_print_char fmt ']'; in
  (match vec with
   | Vector2 (e1,e2) ->
     pp_boxed fmt (fun fmt () ->
         pp_expr ~print_type f fmt e1;
         pr_sep fmt ();
         pp_expr ~print_type f fmt e2;
       )
   | Vector3 (e1, e2, e3) ->
     pp_boxed fmt (fun fmt () ->
         pp_expr ~print_type f fmt e1;
         pr_sep fmt ();
         pp_expr ~print_type f fmt e2;
         pr_sep fmt ();
         pp_expr ~print_type f fmt e3;
       )
   | Vector4 (e1,e2,e3,e4) ->
     pp_boxed fmt (fun fmt () ->
         pp_expr ~print_type f fmt e1;
         pr_sep fmt ();
         pp_expr ~print_type f fmt e2;
         pr_sep fmt ();
         pp_expr ~print_type f fmt e3;
         pr_sep fmt ();
         pp_expr ~print_type f fmt e4;
       )
  )
and pp_data ?(print_type=true) f fmt (data: 'a data) =
  let open Format in
  let w_typ pp_v v ty = w_typ_helper ~print_type f fmt pp_v v ty in
  match data with
  | Model (strlit, ty) ->
    w_typ (fun fmt strlit ->
        pp_open_hbox fmt ();
        pp_print_string fmt "model";
        pp_print_char fmt '(';
        pp_open_hvbox fmt 1;

        pp_open_hbox fmt ();
        pp_print_char fmt '"';
        pp_print_string fmt strlit;
        pp_print_char fmt '"';
        pp_close_box fmt ();

        pp_close_box fmt ();
        pp_print_char fmt ')';
        pp_close_box fmt ();
      ) strlit ty
  | Data (mapping, ty) ->
    w_typ (fun fmt (mapping: (IdentifierTable.m * 'a expr) list) ->
        pp_open_hbox fmt ();
        pp_print_string fmt "data";
        pp_print_char fmt '(';
        pp_open_hvbox fmt 1;
        pp_print_list
          ~pp_sep:(fun fmt () -> pp_print_char fmt ','; pp_print_space fmt ())
          (fun fmt (id, exp) ->
             pp_open_hbox fmt ();
             IdentifierTable.pp_m fmt id;
             pp_print_char fmt '=';
             pp_expr ~print_type f fmt exp;
             pp_close_box fmt ();
          ) fmt mapping;
        pp_close_box fmt ();
        pp_print_char fmt ')';
        pp_close_box fmt ();
      ) mapping ty
  | Screen { width; height; typ } ->
    w_typ (fun fmt (width, height) ->
        let pp_opt name exp =
          pp_open_hbox fmt ();
          pp_print_string fmt name;
          pp_print_char fmt '=';
          pp_expr ~print_type f fmt exp;
          pp_close_box fmt () in

        pp_open_hbox fmt ();
        pp_print_string fmt "data";
        pp_print_char fmt '(';
        pp_open_hvbox fmt 1;

        begin match width,height with
          | None, None -> ()
          | Some exp, None -> pp_opt "width" exp
          | None, Some exp -> pp_opt "height" exp
          | Some e1, Some e2 ->
            pp_opt "width" e1;
            pp_print_char fmt ',';
            pp_print_space fmt ();
            pp_opt "height" e2
        end;
        pp_close_box fmt ();
        pp_print_char fmt ')';
        pp_close_box fmt ();
      ) (width,height) typ
and pp_wrap_option fmt (wrap_option: wrap_option) = Format.pp_print_string fmt @@ match wrap_option with
  | Repeat -> "repeat"
  | MirroredRepeat -> "mirrored_repeat"
  | ClampToEdge -> "clamp_to_edge"
  | ClampToBorder -> "clamp_to_border"
and pp_filter_option fmt (filter_option: filter_option) = Format.pp_print_string fmt @@ (match filter_option with
    | Nearest -> "nearest"
    | Linear -> "linear"
    | NearestMipmapNearest -> "nearest_mipmap_nearest"
    | NearestMipmapLinear -> "nearest_mipmap_linear"
    | LinearMipmapNearest -> "linear_mipmap_nearest"
    | LinearMipmapLinear -> "linear_mipmap_linear")
and pp_texture_option ?(print_type=true) f fmt (texture_option: 'a texture_option) =
  let open Format in
  pp_open_hbox fmt ();
  begin match texture_option with
    | TextureBaseLevel exp ->
      pp_print_string fmt "texture_base_level";
      pp_print_char fmt '=';
      pp_expr ~print_type f fmt exp;
    | TextureMaxLevel exp ->
      pp_print_string fmt "texture_max_level";
      pp_print_char fmt '=';
      pp_expr ~print_type f fmt exp;      
    | TextureBorderColor exp ->
      pp_print_string fmt "texture_border_color";
      pp_print_char fmt '=';
      pp_expr ~print_type f fmt exp;
    | WrapS wrap_option ->
      pp_print_string fmt "wrap_s";
      pp_print_char fmt '=';
      pp_wrap_option fmt wrap_option;
    | WrapT wrap_option ->
      pp_print_string fmt "wrap_t";
      pp_print_char fmt '=';
      pp_wrap_option fmt wrap_option;      
    | MinFilter fopt ->
      pp_print_string fmt "min_filter";
      pp_print_char fmt '=';
      pp_filter_option fmt fopt;      
    | MagFilter fopt ->
      pp_print_string fmt "mag_filter";
      pp_print_char fmt '=';
      pp_filter_option fmt fopt;
  end;
  pp_close_box fmt ()
and pp_dimension_options ~print_type f fmt (dopt: 'a dimension_options) =
  let open Format in
  match dopt with
  | DimensionVector vec -> pp_expr ~print_type f fmt vec
  | DimensionXZY { x; y; z } ->
    let ls =
      Option.map x ~f:(fun v -> ("x",v)) ::
      Option.map y ~f:(fun v -> ("y",v)) ::
      Option.map z ~f:(fun v -> ("z",v)) :: []
      |> List.filter_opt in
    pp_open_hvbox fmt 1;
    pp_print_list
      ~pp_sep:(fun fmt () -> pp_print_char fmt ','; pp_print_space fmt ())
      (fun fmt (name, expr) ->
         pp_open_hbox fmt ();
         pp_print_string fmt name;
         pp_print_char fmt '=';
         pp_expr ~print_type f fmt expr;
         pp_close_box fmt ();
      ) fmt ls;
    pp_close_box fmt ()
and pp_rotate_options ?(print_type=true) f fmt (RotateAngle { x; y; z; vector }: 'a rotate_options) =
  let open Format in
  let ls =
    Option.map x ~f:(fun v -> ("x",v)) ::
    Option.map y ~f:(fun v -> ("y",v)) ::
    Option.map z ~f:(fun v -> ("z",v)) ::
    Option.map vector ~f:(fun v -> ("vector",v)) ::
    []
    |> List.filter_opt in
  pp_open_hvbox fmt 1;
  pp_print_list
    ~pp_sep:(fun fmt () -> pp_print_char fmt ','; pp_print_space fmt ())
    (fun fmt (name, expr) ->
       pp_open_hbox fmt ();
       pp_print_string fmt name;
       pp_print_char fmt '=';
       pp_expr ~print_type f fmt expr;
       pp_close_box fmt ();
    ) fmt ls;
  pp_close_box fmt ()
and pp_blend_func fmt (blend_func: blend_func) = Format.pp_print_string fmt @@ match blend_func with
  | Zero -> "zero"
  | One -> "one"
  | Src_color -> "src_color"
  | One_minus_src_color -> "one_minus_src_color"
  | Dst_color -> "dst_color"
  | One_minus_dst_color -> "one_minus_dst_color"
  | Src_alpha -> "src_alpha"
  | One_minus_src_alpha -> "one_minus_src_alpha"
  | Dst_alpha -> "dst_alpha"
  | One_minus_dst_alpha -> "one_minus_dst_alpha"
  | Constant_color -> "constant_color"
  | One_minus_constant_color -> "one_minus_constant_color"
  | Constant_alpha -> "constant_alpha"
  | One_minus_constant_alpha -> "one_minus_constant_alpha"
  | Src_alpha_saturate -> "src_alpha_saturate"
and pp_comparison_func fmt (cfun: comparison_func) = Format.pp_print_string fmt @@ match cfun with
  | Stencil_never -> "stencil_never"
  | Stencil_less -> "stencil_less"
  | Stencil_lequal -> "stencil_lequal"
  | Stencil_greater -> "stencil_greater"
  | Stencil_gequal -> "stencil_gequal"
  | Stencil_equal -> "stencil_equal"
  | Stencil_notequal -> "stencil_notequal"
  | Stencil_always -> "stencil_always"
and pp_blend_color_option ?(print_type=true) f fmt (ColourVector vec: 'a blend_color_option) =
  pp_expr ~print_type f fmt vec
and pp_draw_option ?(print_type=true) f fmt (dopt: 'a draw_option) =
  let open Format in
  pp_open_hbox fmt ();
  begin match dopt with
    | AlphaTest exp -> pp_print_string fmt "alpha_test"; pp_print_char fmt '='; pp_expr ~print_type f fmt exp
    | BlendTest exp -> pp_print_string fmt "blend_test"; pp_print_char fmt '='; pp_expr ~print_type f fmt exp
    | DepthTest exp -> pp_print_string fmt "depth_test"; pp_print_char fmt '='; pp_expr ~print_type f fmt exp
    | StencilTest exp -> pp_print_string fmt "stencil_test"; pp_print_char fmt '='; pp_expr ~print_type f fmt exp
    | BlendColor bcol ->
      pp_print_string fmt "blend_color"; pp_print_char fmt '='; pp_blend_color_option ~print_type f fmt bcol
    | BlendFunction (f1, f2) ->
      pp_print_string fmt "blend_function"; pp_print_char fmt '=';
      pp_print_char fmt '[';
      pp_open_hvbox fmt 1;
      pp_blend_func fmt f1;
      pp_print_char fmt ',';
      pp_print_space fmt ();
      pp_blend_func fmt f2;
      pp_close_box fmt ();
      pp_print_char fmt ']'
    | StencilFunction (cf, e1, e2) ->
      pp_print_string fmt "stencil_function"; pp_print_char fmt '=';
      pp_print_char fmt '[';
      pp_open_hvbox fmt 1;
      pp_comparison_func fmt cf;
      pp_print_char fmt ',';
      pp_print_space fmt ();
      pp_expr ~print_type f fmt e1;
      pp_print_char fmt ',';
      pp_print_space fmt ();
      pp_expr ~print_type f fmt e2;
      pp_close_box fmt ();
      pp_print_char fmt ']'
    | DepthFunction cf ->
      pp_print_string fmt "depth_function"; pp_print_char fmt '=';
      pp_comparison_func fmt cf;
    | AlphaFunction (cf, e1) ->
      pp_print_string fmt "alpha_function"; pp_print_char fmt '=';
      pp_print_char fmt '[';
      pp_open_hvbox fmt 1;
      pp_comparison_func fmt cf;
      pp_print_char fmt ',';
      pp_print_space fmt ();
      pp_expr ~print_type f fmt e1;
      pp_close_box fmt ();
      pp_print_char fmt ']'
  end;
  pp_close_box fmt ();
and pp_modifier ?(print_type=true) f fmt (modifier: 'a modifier) =
  let open Format in
  let w_typ pp_v v ty = w_typ_helper ~print_type f fmt pp_v v ty in
  match modifier with
  | Texture (name, strlit, options, ty) ->
    w_typ (fun fmt (name, files, options) ->
        with_hbox fmt (fun fmt () ->
            pp_print_string fmt "texture";
            pp_print_char fmt '(';
            with_hvbox fmt 1 (fun fmt () ->
                IdentifierTable.pp_m fmt name;
                pp_print_char fmt ',';
                pp_print_space fmt ();
                pp_print_list ~pp_sep:(fun fmt () -> pp_print_char fmt ','; pp_print_space fmt ())
                  (fun fmt strlit -> with_hbox fmt (fun fmt () ->
                       pp_print_char fmt '"';
                       pp_print_string fmt strlit;
                       pp_print_char fmt '"';
                     )) fmt files;
                begin match options with
                  | [] -> ()
                  | _ :: _ ->
                    pp_print_char fmt ',';
                    pp_print_space fmt ();
                end;
                pp_print_list
                  ~pp_sep:(fun fmt () -> pp_print_char fmt ','; pp_print_space fmt ())
                  (pp_texture_option ~print_type f) fmt options;
              );
            pp_print_char fmt ')';
          )
      ) (name, strlit, options) ty
  | Uniform (name, exp, ty) ->
    w_typ (fun fmt (name, exp) ->
        with_hbox fmt (fun fmt () ->
            pp_print_string fmt "uniform";
            pp_print_char fmt '(';
            with_hvbox fmt 1 (fun fmt () ->
                IdentifierTable.pp_m fmt name;
                pp_print_char fmt ',';
                pp_print_space fmt ();
                pp_expr ~print_type f fmt exp;
              );
            pp_print_char fmt ')';
          )
      ) (name, exp) ty
  | Attribute (name, exp, ty) ->
    w_typ (fun fmt (name, exp) ->
        with_hbox fmt (fun fmt () ->
            pp_print_string fmt "attribute";
            pp_print_char fmt '(';
            with_hvbox fmt 1 (fun fmt () ->
                IdentifierTable.pp_m fmt name;
                pp_print_char fmt ',';
                pp_print_space fmt ();
                pp_expr ~print_type f fmt exp;
              );
            pp_print_char fmt ')';
          )
      ) (name, exp) ty
  | Translate (dopt, ty) ->
    w_typ (fun fmt dopt ->
        with_hbox fmt (fun fmt () ->
            pp_print_string fmt "translate";
            pp_print_char fmt '(';
            pp_dimension_options ~print_type f fmt dopt;
            pp_print_char fmt ')';
          )
      ) dopt ty
  | Scale (dopt, ty) ->
    w_typ (fun fmt dopt ->
        with_hbox fmt (fun fmt () ->
            pp_print_string fmt "scale";
            pp_print_char fmt '(';
            pp_dimension_options ~print_type f fmt dopt;
            pp_print_char fmt ')';
          )
      ) dopt ty
  | Rotate (ropt, ty) ->
    w_typ (fun fmt ropt ->
        with_hbox fmt (fun fmt () ->
            pp_print_string fmt "rotate";
            pp_print_char fmt '(';
            pp_rotate_options ~print_type f fmt ropt;
            pp_print_char fmt ')';
          )
      ) ropt ty
  | DrawOptions (dopt, ty) ->
    w_typ (fun fmt dopt ->
        with_hbox fmt (fun fmt () ->
            pp_print_string fmt "draw_options";
            pp_print_char fmt '(';
            with_hvbox fmt 1 (fun fmt () ->
                pp_print_list
                  ~pp_sep:(fun fmt () -> pp_print_char fmt ','; pp_print_space fmt ())
                  (pp_draw_option ~print_type f) fmt dopt
              );
            pp_print_char fmt ')';
          )
      ) dopt ty
  | LightUniform (light_name, name, exp, ty) ->
    w_typ (fun fmt (name, exp) ->
        with_hbox fmt (fun fmt () ->
            pp_print_string fmt "light_uniform";
            pp_print_char fmt '(';
            with_hvbox fmt 1 (fun fmt () ->
                IdentifierTable.pp_m fmt name;
                pp_print_char fmt ',';
                pp_print_space fmt ();
                IdentifierTable.pp_m fmt light_name;
                pp_print_char fmt ',';
                pp_print_space fmt ();
                pp_expr ~print_type f fmt exp;
              );
            pp_print_char fmt ')';
          )
      ) (name, exp) ty
  | Redirecting { textures; depth; stencil; drawable; typ } ->
    w_typ (fun fmt (textures, depth, stencil, drawable) ->
        with_hbox fmt (fun fmt () ->
            pp_print_string fmt "redirecting";
            pp_print_space fmt ();
            begin match textures with
              | [] -> ()
              | _ :: _ ->
                pp_print_string fmt "into";
                pp_print_char fmt '=';
                pp_print_char fmt '[';
                with_hvbox fmt 1 (fun fmt () ->
                    pp_print_list
                      ~pp_sep:(fun fmt () -> pp_print_char fmt ','; pp_print_space fmt ())
                      (IdentifierTable.pp_m) fmt textures
                  );
                pp_print_char fmt ']';
                pp_print_space fmt ();
            end;

            begin match depth with
              | false -> ()
              | true -> begin
                  pp_print_string fmt "depth";
                  pp_print_space fmt ();
                end
            end;

            begin match stencil with
              | false -> ()
              | true -> begin
                  pp_print_string fmt "stencil";
                  pp_print_space fmt ();
                end
            end;
            pp_drawable ~print_type f fmt drawable
          )
      ) (textures, depth, stencil, drawable) typ
  | ModifierReference reference ->
    with_hbox fmt (fun fmt () ->
        pp_print_string fmt "modifier";
        pp_print_char fmt '(';
        pp_reference ~print_type f fmt reference;
        pp_print_char fmt ')';
      )
and pp_base_object ?(print_type=true) f fmt (obj: 'a base_object) =
  let open Format in
  let w_typ pp_v v ty = w_typ_helper ~print_type f fmt pp_v v ty in
  match obj with
  | ObjectReference reference ->
    with_hbox fmt (fun fmt () ->
        pp_print_string fmt "object";
        pp_print_char fmt '(';
        pp_reference ~print_type f fmt reference;
        pp_print_char fmt ')';
      )
  | Light { name; position; size; typ } ->
    w_typ (fun fmt (name, position, size) ->
        with_hbox fmt (fun fmt () ->
            pp_print_string fmt "light";
            pp_print_char fmt '(';
            with_hvbox fmt 1 (fun fmt () ->
                IdentifierTable.pp_m fmt name;
                pp_print_char fmt ','; pp_print_space fmt ();
                pp_expr ~print_type f fmt position;
                pp_print_char fmt ','; pp_print_space fmt ();
                pp_expr ~print_type f fmt size;
              );
            pp_print_char fmt ')';
          )
      )
      (name, position, size) typ
  | LightData { light_tag; typ } ->
    w_typ (fun fmt (name) ->
        with_hbox fmt (fun fmt () ->
            pp_print_string fmt "light_data";
            pp_print_char fmt '(';
            with_hvbox fmt 1 (fun fmt () ->
                IdentifierTable.pp_m fmt name;
              );
            pp_print_char fmt ')';
          )
      )
      (light_tag) typ
  | ObjectData data -> pp_data f fmt data
  | CompoundObject (body, ty) ->
    w_typ (fun fmt body ->
        pp_print_string fmt "begin";
        pp_print_break fmt 1 0;
        with_hvbox fmt 1 (fun fmt () ->
            pp_print_list
              ~pp_sep:(fun fmt () -> pp_print_space fmt ())
              (pp_obj_expr ~print_type f)
              fmt
              body
          );
        pp_print_break fmt 1 0;
        pp_print_string fmt "end";
      ) (ne_list_to_list body) ty
and pp_obj ?(print_type=true) f fmt (Object (modifiers, base_objects, ty): 'a obj) =
  let open Format in
  let w_typ pp_v v ty = w_typ_helper ~print_type f fmt pp_v v ty in
  w_typ
    (fun fmt (modifiers, base_object) ->
       let rec loop_modifiers modifiers = match modifiers with
         | [] -> pp_base_object ~print_type f fmt base_object
         | h :: t ->
           pp_open_hovbox fmt 1;
           pp_modifier ~print_type f fmt h;
           pp_print_space fmt ();
           loop_modifiers t;
           pp_close_box fmt () in
       loop_modifiers modifiers
    ) (modifiers, base_objects) ty
and pp_obj_expr ?(print_type=true) f fmt (exp: 'a obj_expr) =
  let open Format in
  let w_typ pp_v v ty = w_typ_helper ~print_type f fmt pp_v v ty in
  match exp with
  | PlainObject obj ->
    with_hbox fmt (fun fmt () ->
        pp_obj ~print_type f fmt obj;
        pp_print_char fmt ';'
      )
  | ObjectIfThenElse (cond, ifcase, elsecase, typ) ->
    with_hbox fmt (fun fmt () ->
        w_typ (fun fmt (cond, ifcase, elsecase) ->
            with_hvbox fmt 1 (fun fmt () ->
                with_hbox fmt (fun fmt () ->
                    pp_print_string fmt "if";
                    pp_print_space fmt ();
                    pp_expr ~print_type f fmt cond;
                  );
                pp_print_space fmt ();
                with_hbox fmt (fun fmt () ->
                    pp_print_string fmt "then";
                    pp_print_space fmt ();
                    pp_obj_expr ~print_type f fmt ifcase;
                  );
                begin match elsecase with
                  | None -> ()
                  | Some elsecase ->
                    pp_print_space fmt ();
                    with_hbox fmt (fun fmt () ->
                        pp_print_string fmt "else";
                        pp_print_space fmt ();
                        pp_obj_expr ~print_type f fmt elsecase;
                      );
                end;
              )
          ) (cond, ifcase, elsecase) typ;
        pp_print_char fmt ';'        
      )
  | ObjectFor ((id, inexp, toexp, body), ty) ->
    w_typ (fun fmt (id, inexp, toexp, body) ->
        with_hbox fmt (fun fmt () ->
            pp_print_string fmt "for";
            pp_print_space fmt ();
            IdentifierTable.pp_m fmt id;
            pp_print_space fmt ();
            pp_print_string fmt "in";
            pp_print_space fmt ();
            pp_expr ~print_type f fmt inexp;
            pp_print_space fmt ();
            begin match toexp with
              | None -> ()
              | Some toexp -> 
                pp_print_string fmt "to";
                pp_print_space fmt ();
                pp_expr ~print_type f fmt toexp;
                pp_print_space fmt ();
            end;
            pp_print_string fmt "begin";
            with_hvbox fmt 1 (fun fmt () ->
                pp_print_list
                  ~pp_sep:(fun fmt () -> pp_print_space fmt ())
                  (pp_obj_expr ~print_type f)
                  fmt
                  body
              );
            pp_print_string fmt "end";
          )
      )
      (id, inexp, toexp, body) ty
(* and pp_declaration f fmt (decl: 'a declaration) =
 *   let open Format in
 *   match decl with
 *   | DataFunction { identifier; keyword_arguments; positional_arguments; body } ->
 *     with_hbox fmt (fun fmt () ->
 *         pp_print_string fmt "function";
 *         pp_print_space fmt ();
 *         IdentifierTable.pp_m fmt identifier;
 *         pp_print_space fmt ();
 *         pp_print_char fmt '(';
 * 
 *         with_hvbox fmt 1 (fun fmt () ->
 *             pp_print_list
 *               ~pp_sep:(fun fmt () -> pp_print_char fmt ','; pp_print_space fmt ())
 *               (fun fmt (id,exp) ->
 *                  pp_open_hbox fmt ();
 *                  IdentifierTable.pp_m fmt id;
 *                  pp_print_break fmt 0 0;
 *                  pp_print_char fmt '=';
 *                  pp_print_break fmt 0 0;
 *                  pp_expr ~print_type f fmt exp;
 *                  pp_close_box fmt ();
 *               ) fmt (IDMap.to_alist keyword_arguments);
 *             begin match (IDMap.to_alist keyword_arguments) with
 *               | [] -> ()
 *               | _ :: _  -> pp_print_char fmt ','; pp_print_space fmt ()
 *             end;
 *             pp_print_list
 *               ~pp_sep:(fun fmt () -> pp_print_char fmt ','; pp_print_space fmt ())
 *               (fun fmt (id, ty) ->
 *                  with_hbox fmt (fun fmt () ->
 *                      IdentifierTable.pp_m fmt id;
 *                      pp_print_space fmt ();
 *                      pp_print_char fmt ':';
 *                      pp_print_space fmt ();
 *                      f fmt ty
 *                    )
 *               ) fmt positional_arguments;
 *           );
 *         pp_print_char fmt ')';
 *         pp_print_space fmt ();
 *         pp_print_string fmt "begin";
 *         with_hvbox fmt 1 (fun fmt () ->
 *             pp_print_space fmt ();
 *             pp_expr ~print_type f fmt body;
 *             pp_print_space fmt ()
 *           );
 *         pp_print_string fmt "end";
 *       )
 *   | DataVariable { identifier; body } ->
 *     with_hbox fmt (fun fmt () ->
 *         pp_print_string fmt "var";
 *         pp_print_space fmt ();
 *         IdentifierTable.pp_m fmt identifier;
 *         pp_print_space fmt ();
 *         pp_print_char fmt '=';
 *         pp_print_space fmt ();
 *         pp_expr ~print_type f fmt body;
 *         pp_print_char fmt ';';
 *       )    
 *   | MacroFunction { identifier; keyword_arguments; positional_arguments; body } ->
 *     with_hbox fmt (fun fmt () ->
 *         pp_print_string fmt "macro";
 *         pp_print_space fmt ();
 *         pp_print_string fmt "function";
 *         pp_print_space fmt ();
 *         IdentifierTable.pp_m fmt identifier;
 *         pp_print_space fmt ();
 *         pp_print_char fmt '(';
 * 
 *         with_hvbox fmt 1 (fun fmt () ->
 *             pp_print_list
 *               ~pp_sep:(fun fmt () -> pp_print_char fmt ','; pp_print_space fmt ())
 *               (fun fmt (id,exp) ->
 *                  pp_open_hbox fmt ();
 *                  IdentifierTable.pp_m fmt id;
 *                  pp_print_break fmt 0 0;
 *                  pp_print_char fmt '=';
 *                  pp_print_break fmt 0 0;
 *                  pp_scene_expr ~print_type f fmt exp;
 *                  pp_close_box fmt ();
 *               ) fmt (IDMap.to_alist keyword_arguments);
 *             begin match (IDMap.to_alist keyword_arguments) with
 *               | [] -> ()
 *               | _ :: _  -> pp_print_char fmt ','; pp_print_space fmt ()
 *             end;
 *             pp_print_list
 *               ~pp_sep:(fun fmt () -> pp_print_char fmt ','; pp_print_space fmt ())
 *               (fun fmt (id, ty) ->
 *                  with_hbox fmt (fun fmt () ->
 *                      IdentifierTable.pp_m fmt id;
 *                      pp_print_space fmt ();
 *                      pp_print_char fmt ':';
 *                      pp_print_space fmt ();
 *                      f fmt ty
 *                    )
 *               ) fmt positional_arguments;
 *           );
 *         pp_print_char fmt ')';
 *         pp_print_space fmt ();
 *         pp_print_string fmt "begin";
 *         with_hvbox fmt 1 (fun fmt () ->
 *             pp_print_space fmt ();
 *             pp_scene_expr ~print_type f fmt body;
 *             pp_print_space fmt ()
 *           );
 *         pp_print_string fmt "end";
 *       )    
 *   | MacroVariable { identifier; body } ->
 *     with_hbox fmt (fun fmt () ->
 *         pp_print_string fmt "macro";
 *         pp_print_space fmt ();
 *         pp_print_string fmt "var";
 *         pp_print_space fmt ();
 *         IdentifierTable.pp_m fmt identifier;
 *         pp_print_space fmt ();
 *         pp_print_char fmt '=';
 *         pp_print_space fmt ();
 *         pp_scene_expr ~print_type f fmt body;
 *         pp_print_char fmt ';';
 *       ) *)
and pp_macro_arguments ?(print_type=true)
    f fmt (MacroArguments { keyword_args; positional_args }: 'a macro_arguments) =
  let open Format in
  with_hvbox fmt 1 (fun fmt () ->
      pp_print_list
        ~pp_sep:(fun fmt () -> pp_print_char fmt ','; pp_print_space fmt ())
        (fun fmt (id,exp) ->
           pp_open_hbox fmt ();
           IdentifierTable.pp_m fmt id;
           pp_print_break fmt 0 0;
           pp_print_char fmt '=';
           pp_print_break fmt 0 0;
           pp_scene_arg ~print_type f fmt exp;
           pp_close_box fmt ();
        ) fmt (IDMap.to_alist keyword_args);
      begin match (IDMap.to_alist keyword_args) with
        | [] -> ()
        | _ :: _  -> pp_print_char fmt ','; pp_print_space fmt ()
      end;
      pp_print_list
        ~pp_sep:(fun fmt () -> pp_print_char fmt ','; pp_print_space fmt ())
        (pp_scene_arg ~print_type f) fmt positional_args;
    )
and pp_scene_arg ?(print_type=true) f fmt (sarg: 'a scene_arg) =
  (match sarg with
   | ArgReference reference -> pp_reference ~print_type f fmt reference
   | SceneDataExpression expr -> pp_expr ~print_type f fmt expr
   | SceneInlineExpression sexpr -> pp_scene_expr ~print_type f fmt sexpr)
and pp_shader ?(print_type=true) f fmt (shader: 'a shader) =
  let open Format in
  let w_typ pp_v v ty = w_typ_helper ~print_type f fmt pp_v v ty in
  match shader with
  | ShaderReference reference ->
    with_hbox fmt (fun fmt () ->
        pp_print_string fmt "shader";
        pp_print_char fmt '(';
        pp_reference ~print_type f fmt reference;
        pp_print_char fmt ')';
      )
  | Shader { vertex_shader; fragment_shader; geometry_shader; shader_spec=_; typ } ->
    let ls =
      Option.map (Some vertex_shader) ~f:(fun v -> ("vertex_shader",v)) ::
      Option.map (match fragment_shader with | Src _ -> None | Filename name -> Some name)
        ~f:(fun v -> ("fragment_shader",v)) ::
      Option.map geometry_shader ~f:(fun v -> ("geometry_shader",v)) ::
      [] |> List.filter_opt in
    w_typ (fun fmt ls ->
        with_hbox fmt (fun fmt () ->
            pp_print_string fmt "shader";
            pp_print_char fmt '(';
            with_hvbox fmt 1 (fun fmt () ->
                pp_print_list
                  ~pp_sep:(fun fmt () -> pp_print_char fmt ','; pp_print_break fmt 0 0; pp_print_space fmt ())
                  (fun fmt (name, strlit) ->
                     with_hbox fmt (fun fmt () ->
                         pp_print_string fmt name;
                         pp_print_char fmt '=';
                         pp_print_char fmt '"';
                         pp_print_string fmt strlit;
                         pp_print_char fmt '"';
                       )
                  ) fmt ls
              );
            pp_print_char fmt ')';
          )      
      ) ls typ
and pp_drawable ?(print_type=true) f fmt (drw: 'a drawable) =
  let open Format in
  let w_typ pp_v v ty = w_typ_helper ~print_type f fmt pp_v v ty in
  match drw with
  | OverlayingDrawable (drawable, ty) ->
    w_typ (fun fmt drawable ->
        with_hbox fmt (fun fmt () ->
            pp_print_string fmt "overlaying";
            pp_print_space fmt ();
            pp_drawable ~print_type f fmt drawable;
          )
      ) drawable ty
  | DrawableShader (shader, obj, ty) ->
    w_typ (fun fmt (shader, obj) ->
        with_hbox fmt (fun fmt () ->
            pp_print_string fmt "with";
            pp_print_space fmt ();
            with_hvbox fmt 1 (fun fmt () ->
                pp_shader ~print_type f fmt shader;
                pp_print_space fmt ();
                pp_obj ~print_type f fmt obj;
              )
          )
      ) (shader,obj) ty
  | DrawableReference reference ->
    with_hbox fmt (fun fmt () ->
        pp_print_string fmt "drawable";
        pp_print_char fmt '(';
        with_hvbox fmt 1 (fun fmt () ->
            pp_reference ~print_type f fmt reference
          );
        pp_print_char fmt ')';
      )
and pp_scene_expr ?(print_type=true) f fmt (sexpr: 'a scene_expr) = (match sexpr with
    | SceneExprObject obj -> pp_obj ~print_type f fmt obj
    | SceneExprShader shdd -> pp_shader ~print_type f fmt shdd
    | SceneExprModifier modifier -> pp_modifier ~print_type f fmt modifier
    | SceneExprDrawable drawable -> pp_drawable ~print_type f fmt drawable)
and pp_reference ?(print_type=true) f fmt (reference: 'a reference) =
  let open Format in
  let w_typ pp_v v ty = w_typ_helper ~print_type f fmt pp_v v ty in
  (match reference with
   | DirectReference (id, ty) ->
     w_typ (fun fmt id -> pp_print_char fmt '@'; IdentifierTable.pp_m fmt id) id ty
   | IndirectReference (fname, args, ty) ->
     w_typ (fun fmt (fname, args) ->
         with_hbox fmt (fun fmt () ->
             pp_print_char fmt '@'; IdentifierTable.pp_m fmt fname;
             pp_print_char fmt '(';
             with_hvbox fmt 1 (fun fmt () -> pp_macro_arguments ~print_type f fmt args);
             pp_print_char fmt ')';
           )
       ) (fname, args) ty
  )
and pp_statement ?(print_type=true) f fmt (stmt: 'a statement) =
  let open Format in
  match stmt with
  (* | Declaration decl -> pp_declaration f fmt decl *)
  | Drawable drawable -> pp_drawable ~print_type f fmt drawable; pp_print_char fmt ';'
  | LayeredDrawable (layer_name, drawable) ->
    with_hvbox fmt 1 (fun fmt () ->
        with_hbox fmt (fun fmt () ->
            pp_print_string fmt "layer";
            pp_print_char fmt '(';
            with_hbox fmt (fun fmt () ->
                pp_print_char fmt '"';
                pp_print_string fmt layer_name;
                pp_print_char fmt '"';
              );
            pp_print_char fmt ')';
            pp_print_char fmt ':';
          );
        pp_drawable ~print_type f fmt drawable; pp_print_char fmt ';'
      )
and pp_program ?(print_type=true) f fmt (prog: 'a statement list) =
  let open Format in 
  pp_print_list
    ~pp_sep:(fun fmt () -> pp_force_newline fmt ())
    (pp_statement ~print_type f) fmt prog

let show_prog =
  Format.sprintf "%a" (fun () v ->
      pp_program (TypeVariable.pp) Format.str_formatter v; Format.flush_str_formatter ())



module AstUtils = struct

  let type_expr (exp: 'a expr) : 'a = (match exp with
      | Exp_bool (_, v) -> v
      | Exp_number (_, v) -> v
      | Exp_variable (_, v) -> v
      | Exp_binop (_, v) -> v
      | Exp_preop (_, v) -> v
      | Exp_index (_, v) -> v
      | Exp_vector (_, v) -> v
      | Exp_list (_, v) -> v
      | Exp_list_comprehension (_, v) -> v
      | Exp_function_call (_, _, v) -> v
      | Exp_if_then_else (_, v) -> v)


  let type_drawable (drawable: 'a drawable) = match drawable with
    | OverlayingDrawable (_, v) -> v
    | DrawableShader (_, _, v) -> v
    | DrawableReference v -> (match v with
        | DirectReference (_, v) -> v
        | IndirectReference (_, _, v) -> v) 


  let type_shader (shader: 'a shader) = match shader with
    | Shader { typ; _ } -> typ 
    | ShaderReference v -> (match v with
        | DirectReference (_, v) -> v
        | IndirectReference (_, _, v) -> v) 

  let type_obj (obj: 'a obj) = match obj with | Object (_, _, v) -> v

  let type_obj_expr (obj: 'a obj_expr) = match obj with
    | PlainObject obj -> type_obj obj
    | ObjectIfThenElse (_, _, _, v) -> v
    | ObjectFor (_, v) -> v

  let type_data (data: 'a data) = match data with
    | Model (_, v) -> v
    | Data (_, v) -> v
    | Screen { typ; _ } -> typ 


  let type_base_object (obj: 'a base_object) = match obj with
    | Light {  typ; _ } -> typ
    | LightData {  typ; _ } -> typ
    | ObjectData data -> type_data data
    | CompoundObject (_, v) -> v
    | ObjectReference rf -> (match rf with
        | DirectReference (_, v) -> v
        | IndirectReference (_, _, v) -> v)

  let type_modifier (modifier: 'a modifier) = match modifier with
    | Texture (_, _, _, v) -> v
    | Uniform (_, _, v) -> v
    | Attribute (_, _, v) -> v
    | Translate (_, v) -> v
    | Scale (_, v) -> v
    | Rotate (_, v) -> v
    | DrawOptions (_, v) -> v
    | Redirecting { typ; _ } -> typ
    | LightUniform (_, _, _, v) -> v
    | ModifierReference value -> (match value with
        | DirectReference (_, v) -> v
        | IndirectReference (_, _, v) -> v)



  let type_scene_expr (sexpr: 'a scene_expr) = match sexpr with
    | SceneExprObject obj -> type_obj obj
    | SceneExprShader shader -> type_shader shader
    | SceneExprModifier modifier -> type_modifier modifier
    | SceneExprDrawable drawable -> type_drawable drawable 


  let type_scene_arg (sarg: 'a scene_arg) = match sarg with
    | ArgReference reference -> (match reference with
        | DirectReference (_, v) -> v
        | IndirectReference (_, _, v) -> v)
    | SceneDataExpression expr -> type_expr expr
    | SceneInlineExpression sexpr -> type_scene_expr sexpr


end

module type ELEM = sig
  type t
  val pp : Formatter.t -> t -> unit
end

module Expander = struct

  type 'a argument =
    | ArgExpression of 'a expr
    | ArgObject of 'a obj
    | ArgShader of 'a shader
    | ArgModifier of 'a modifier
    | ArgDrawable of 'a drawable
    | ArgRef of 'a reference
  [@@deriving show, map, iter]

  let of_scene_expr (sexp: 'a scene_expr) = match sexp with
    | SceneExprObject obj -> ArgObject obj
    | SceneExprShader shader -> ArgShader shader
    | SceneExprModifier modifier -> ArgModifier modifier
    | SceneExprDrawable drawable -> ArgDrawable drawable

  let of_scene_arg (sexp: 'a scene_arg) = match sexp with
    | ArgReference reference -> ArgRef reference
    | SceneDataExpression expr -> ArgExpression expr
    | SceneInlineExpression sexpr -> of_scene_expr sexpr


  module Make  (Elem: ELEM) = struct
    type substitution_error =
      | InvalidExpression of IdentifierTable.m * Elem.t argument
      | InvalidObject of IdentifierTable.m * Elem.t argument
      | InvalidModifier of IdentifierTable.m * Elem.t argument
      | InvalidDrawable of IdentifierTable.m * Elem.t argument
      | InvalidShader of IdentifierTable.m * Elem.t argument
    [@@deriving show, map, iter]

    exception SubstitutionException of substitution_error

    let () = Stdlib.Printexc.register_printer (function
          SubstitutionException v ->
          Some (show_substitution_error v)
        | _ -> None
      )

    let rec expand_expr (map: Elem.t argument IDMap.t) (expr: Elem.t expr) =
      match expr with
      | Exp_variable (id, _) -> begin match IDMap.find map id with
          | None -> expr
          | Some (ArgExpression expr) -> expr
          | Some v -> raise (SubstitutionException (InvalidExpression (id, v)))
        end
      | Exp_binop ((op, e1, e2), var) ->
        let e1 = expand_expr map e1 in
        let e2 = expand_expr map e2 in
        Exp_binop ((op, e1, e2), var)
      | Exp_preop ((op, e1), var) ->
        let e1 = expand_expr map e1 in
        Exp_preop ((op, e1), var)
      | Exp_index ((e1, e2), var) ->
        let e1 = expand_expr map e1 in
        let e2 = expand_expr map e2 in
        Exp_index ((e1, e2), var)
      | Exp_vector (vec, var) ->
        let vec = expand_vector map vec in
        Exp_vector (vec, var)
      | Exp_list (exprs, var) ->
        let exprs = List.map ~f:(expand_expr map) exprs in
        Exp_list (exprs, var)
      | Exp_list_comprehension ((expr, id, froe, toe), var) ->
        let froe = expand_expr map froe in
        let toe = Option.map ~f:(expand_expr map) toe in
        let expr = let map = IDMap.remove map id in expand_expr map expr in
        Exp_list_comprehension ((expr, id, froe, toe), var)
      | Exp_function_call (fname, fargs, var) ->
        let fargs = expand_function_args map fargs in
        Exp_function_call (fname, fargs, var)
      | Exp_if_then_else ((cond, e1, e2), var) ->
        let e1 = expand_expr map e1 in
        let e2 = expand_expr map e2 in
        let cond = expand_expr map cond in
        Exp_if_then_else ((cond, e1, e2), var)
      | v -> v
    (* | Exp_bool (e1, v) -> (??)
     * | Exp_number (_, _) -> (??) *)
    and expand_vector map vec =
      match vec with
      | Vector2 (e1, e2) ->
        let e1 = expand_expr map e1 in
        let e2 = expand_expr map e2 in
        Vector2 (e1, e2)
      | Vector3 (e1, e2, e3) ->
        let e1 = expand_expr map e1 in
        let e2 = expand_expr map e2 in
        let e3 = expand_expr map e3 in
        Vector3 (e1,e2,e3)
      | Vector4 (e1,e2,e3,e4) ->
        let e1 = expand_expr map e1 in
        let e2 = expand_expr map e2 in
        let e3 = expand_expr map e3 in
        let e4 = expand_expr map e4 in
        Vector4 (e1,e2,e3,e4)
    and expand_function_args map (FunctionArguments { keyword_args; positional_args }) =
      let keyword_args = map_id_map (expand_expr map) keyword_args in
      let positional_args = List.map ~f:(expand_expr map) positional_args in
      FunctionArguments { keyword_args; positional_args }
    and expand_data map (data: Elem.t data) = match data with
      | Data (mapping, var) ->
        let mapping = List.map ~f:(fun (id, expr) -> (id, expand_expr map expr)) mapping in
        Data (mapping, var)      
      | Screen { width; height; typ } ->
        let width = Option.map width ~f:(expand_expr map) in
        let height = Option.map height ~f:(expand_expr map) in
        Screen { width; height; typ }
      | v -> v
    and expand_texture_option map (texture_option: Elem.t texture_option) =
      match texture_option with
      | TextureBaseLevel expr -> TextureBaseLevel (expand_expr map expr)
      | TextureMaxLevel expr -> TextureMaxLevel (expand_expr map expr)
      | TextureBorderColor expr -> TextureBorderColor (expand_expr map expr)
      | v -> v
    and expand_dimension_options map (dopts: Elem.t dimension_options) =
      match dopts with
      | DimensionVector expr -> DimensionVector (expand_expr map expr)
      | DimensionXZY { x; y; z } ->
        let x = Option.map ~f:(expand_expr map) x in
        let y = Option.map ~f:(expand_expr map) y in
        let z = Option.map ~f:(expand_expr map) z in
        DimensionXZY { x; y; z }
    and expand_rotate_options map (RotateAngle { x; y; z; vector }: Elem.t rotate_options) = 
      let x = Option.map ~f:(expand_expr map) x in
      let y = Option.map ~f:(expand_expr map) y in
      let z = Option.map ~f:(expand_expr map) z in
      let vector = Option.map ~f:(expand_expr map) vector in
      RotateAngle { x; y; z; vector }
    and expand_blend_color_option map (ColourVector expr: Elem.t blend_color_option) =
      ColourVector (expand_expr map expr)
    and expand_draw_option map (dopt: Elem.t draw_option) = match dopt with
      | AlphaTest expr -> AlphaTest (expand_expr map expr)
      | BlendTest expr -> BlendTest (expand_expr map expr)
      | DepthTest expr -> DepthTest (expand_expr map expr)
      | StencilTest expr -> StencilTest (expand_expr map expr)
      | BlendColor bopt -> BlendColor (expand_blend_color_option map bopt)
      | StencilFunction (cf, e1, e2) -> StencilFunction (cf, expand_expr map e1, expand_expr map e2)
      | AlphaFunction (cf, e1) -> AlphaFunction (cf, expand_expr map e1)
      | v -> v
    and expand_base_object map (bobj: Elem.t base_object) =
      match bobj with
      | Light { name; position; size; typ } ->
        let position = expand_expr map position in
        let size = expand_expr map size in
        `Left (Light { name; position; size; typ })
      | LightData { light_tag; typ } ->
        `Left (LightData { light_tag; typ })
      | ObjectData data -> `Left (ObjectData (expand_data map data))
      | CompoundObject (exprs, var) ->
        let exprs = map_ne_list ~f:(expand_obj_expr map) exprs in
        `Left (CompoundObject (exprs, var))
      | ObjectReference reference ->
        (match reference with
         | IndirectReference (fname, margs, var) ->
           `Left (ObjectReference (IndirectReference (fname, expand_macro_args map margs, var)))
         | DirectReference (name, var) -> begin match IDMap.find map name with
             | None -> `Left (ObjectReference (DirectReference (name, var)))
             | Some (ArgRef var) -> `Left (ObjectReference var)
             | Some (ArgObject obj) -> `Right obj
             | Some v -> raise (SubstitutionException (InvalidObject (name, v)))
           end
        )
    and expand_macro_args map (MacroArguments { keyword_args; positional_args }) =
      let keyword_args = map_id_map (expand_scene_arg map) keyword_args in
      let positional_args = List.map ~f:(expand_scene_arg map) positional_args in
      MacroArguments { keyword_args; positional_args }
    and expand_obj_expr map (obje: Elem.t obj_expr) =
      match obje with
      | PlainObject obj -> PlainObject (expand_obj map obj)
      | ObjectIfThenElse (cond, e1, oe2, var) ->
        let cond = expand_expr map cond in
        let e1 = expand_obj_expr map e1 in
        let oe2 = Option.map ~f:(expand_obj_expr map) oe2 in
        ObjectIfThenElse (cond, e1, oe2, var)
      | ObjectFor ((id, fore, toe, body), var) ->
        let fore = expand_expr map fore in
        let toe = Option.map ~f:(expand_expr map) toe in
        let body =
          let map = IDMap.remove map id in
          List.map ~f:(expand_obj_expr map) body in
        ObjectFor ((id, fore, toe, body), var)
    and expand_obj map (Object (modifiers, bobj, var): Elem.t obj) =
      let modifiers = List.map ~f:(expand_modifier map) modifiers in
      let modifiers, bobj =
        match expand_base_object map bobj with
        | `Left bobj -> modifiers, bobj
        | `Right (Object (other_modifiers, bobj, _)) -> modifiers @ other_modifiers, bobj
      in
      Object (modifiers, bobj, var)    
    and expand_scene_arg map sarg = match sarg with
      | SceneDataExpression expr -> SceneDataExpression (expand_expr map expr)
      | SceneInlineExpression sexpr -> SceneInlineExpression (expand_scene_expr map sexpr)
      | ArgReference reference -> (match reference with
          | DirectReference (name, var) -> begin match IDMap.find map name with
              | None -> (ArgReference (DirectReference (name, var)))
              | Some (ArgRef expr) -> ArgReference expr
              | Some (ArgExpression expr) -> SceneDataExpression expr
              | Some (ArgObject obj) -> SceneInlineExpression (SceneExprObject obj)
              | Some (ArgShader shader) -> SceneInlineExpression (SceneExprShader shader)
              | Some (ArgModifier modifier) -> SceneInlineExpression (SceneExprModifier modifier)
              | Some (ArgDrawable drawable) -> SceneInlineExpression (SceneExprDrawable drawable)
            end
          | IndirectReference (fname, margs, var) ->
            ArgReference (IndirectReference (fname, expand_macro_args map margs, var)))
    and expand_modifier map (modifier: Elem.t modifier) =
      match modifier with
      | Texture (name, filename, topts, var) ->
        let topts = List.map ~f:(expand_texture_option map) topts in
        Texture (name, filename, topts, var)
      | Uniform (name, expr, var) ->
        let expr = expand_expr map expr in
        Uniform (name, expr, var)
      | Attribute (name, expr, var) ->
        let expr = expand_expr map expr in
        Attribute (name, expr, var)
      | Translate (dopts, var) ->
        Translate (expand_dimension_options map dopts, var)
      | Scale (dopts, var) ->
        Scale (expand_dimension_options map dopts, var)
      | Rotate (ropts, var) ->
        Rotate (expand_rotate_options map ropts, var)      
      | DrawOptions (dopts, var) ->
        let dopts = List.map ~f:(expand_draw_option map) dopts in
        DrawOptions (dopts, var)
      | Redirecting { textures; depth; stencil; drawable; typ } ->
        let drawable = expand_drawable map drawable in
        Redirecting { textures; depth; stencil; drawable; typ }
      | LightUniform (name, id, expr, var) ->
        let expr = expand_expr map expr in
        LightUniform (name, id, expr, var)
      | ModifierReference reference -> (match reference with
          | DirectReference (name, var) -> begin match IDMap.find map name with
              | None -> (ModifierReference (DirectReference (name, var)))
              | Some (ArgRef expr) -> ModifierReference expr
              | Some (ArgModifier modifier) -> modifier
              | Some v -> raise (SubstitutionException (InvalidModifier (name, v)))
            end          
          | IndirectReference (fname, margs, var) ->
            ModifierReference (IndirectReference (fname, expand_macro_args map margs, var)))
    and expand_drawable map (drawable: Elem.t drawable) = match drawable with
      | OverlayingDrawable (drawable, var) ->
        OverlayingDrawable (expand_drawable map drawable, var)
      | DrawableShader (shader, obj, var) ->
        DrawableShader (expand_shader map shader, expand_obj map obj, var)      
      | DrawableReference reference ->
        match reference with
        | DirectReference (name, var) -> begin match IDMap.find map name with
            | None -> (DrawableReference (DirectReference (name, var)))
            | Some (ArgRef expr) -> DrawableReference expr
            | Some (ArgDrawable drawable) -> drawable
            | Some v -> raise (SubstitutionException (InvalidDrawable (name, v)))
          end
        | IndirectReference (fname, margs, var) ->
          DrawableReference (IndirectReference (fname, expand_macro_args map margs, var))        
    and expand_shader map (shader: Elem.t shader) =
      match shader with
      | ShaderReference reference ->
        begin match reference with
          | DirectReference (name, var) -> begin match IDMap.find map name with
              | None -> (ShaderReference (DirectReference (name, var)))
              | Some (ArgRef expr) -> ShaderReference expr
              | Some (ArgShader shader) -> shader
              | Some v -> raise (SubstitutionException (InvalidShader (name, v)))
            end
          | IndirectReference (fname, margs, var) ->
            ShaderReference (IndirectReference (fname, expand_macro_args map margs, var))
        end
      | v -> v
    and expand_scene_expr map (sexpr: Elem.t scene_expr) =
      match sexpr with
      | SceneExprObject obj -> SceneExprObject (expand_obj map obj)
      | SceneExprShader shader -> SceneExprShader (expand_shader map shader)
      | SceneExprModifier modifier -> SceneExprModifier (expand_modifier map modifier)
      | SceneExprDrawable drawable -> SceneExprDrawable (expand_drawable map drawable)

  end
end

module OfRaw = struct

  module AstExpander = Expander.Make (struct
      type t = TypeVariable.t
      let pp = TypeVariable.pp
    end)

  let default_loc = Location.{
      loc_start=Lexing.dummy_pos;
      loc_end=Lexing.dummy_pos;
    }

  (* default variables always present *)
  let default_variables tbl fresh =
    let camera_position_var = fresh () in
    let camera_up_var = fresh () in
    let camera_front_var = fresh () in
    let camera_yaw_var = fresh () in
    let camera_pitch_var = fresh () in
    let local_variables = [
      IdentifierTable.add_identifier tbl "camera_position",
      camera_position_var;
      IdentifierTable.add_identifier tbl "camera_up",
      camera_up_var;
      IdentifierTable.add_identifier tbl "camera_front",
      camera_front_var;
      IdentifierTable.add_identifier tbl "camera_yaw",
      camera_yaw_var;
      IdentifierTable.add_identifier tbl "camera_pitch",
      camera_pitch_var
    ] in
    let constraints = [
      `Eq (
        Type.TypeExpr (Type.TyVariable camera_position_var),
        Type.TypeExpr (Type.TyDirect
                         (Type.TyPure
                            (Type.TyNonScalar
                               (Type.TyVec3
                                  {value=(Type.TyDirect (Type.TyFloat default_loc)); loc=default_loc}
                               ))))
      );
      `Eq (
        Type.TypeExpr (Type.TyVariable camera_up_var),
        Type.TypeExpr (Type.TyDirect
                         (Type.TyPure
                            (Type.TyNonScalar
                               (Type.TyVec3
                                  {value=(Type.TyDirect (Type.TyFloat default_loc)); loc=default_loc}
                               ))))
      );
      `Eq (
        Type.TypeExpr (Type.TyVariable camera_front_var),
        Type.TypeExpr (Type.TyDirect
                         (Type.TyPure
                            (Type.TyNonScalar
                               (Type.TyVec3
                                  {value=(Type.TyDirect (Type.TyFloat default_loc)); loc=default_loc}
                               ))))
      );
      `Eq (
        Type.TypeExpr (Type.TyVariable camera_pitch_var),
        Type.TypeExpr (Type.TyDirect
                         (Type.TyPure
                            (Type.TyScalar (Type.TyFloat default_loc))))
      );
      `Eq (
        Type.TypeExpr (Type.TyVariable camera_yaw_var),
        Type.TypeExpr (Type.TyDirect
                         (Type.TyPure
                            (Type.TyScalar (Type.TyFloat default_loc))))
      );
    ] in
    local_variables, constraints


  let light_uniform_variables loc tbl fresh =
    let light_position_var = fresh () in
    let local_variables = [
      IdentifierTable.add_identifier tbl "light_position",
      light_position_var
    ] in
    let constraints = [
      `Eq (
        Type.TypeExpr (Type.TyVariable light_position_var),
        Type.TypeExpr (Type.TyDirect
                         (Type.TyPure
                            (Type.TyNonScalar
                               (Type.TyVec3
                                  {value=(Type.TyDirect (Type.TyFloat loc)); loc}
                               )
                            )
                         )
                      )
      )
    ] in
    local_variables, constraints

  let clone_variable ~loc src map var =
    match TypeVariable.Map.find map var with
    | None ->
      (* if the type has not been seen before, clone it *)
      let new_type = TypeVariable.gen_new_type ~loc src () in
      let map = TypeVariable.Map.add_exn map ~key:var ~data:new_type in
      map, new_type
    | Some new_type -> map, new_type

  let clone_scene_expr ~loc src map exp =
    let map = ref map in
    let exp = map_scene_expr (fun ty -> let new_map, ty = clone_variable ~loc src !map ty in map := new_map; ty) exp in
    !map, exp

  let clone_scene_arg ~loc src map exp =
    let map = ref map in
    let exp = map_scene_arg (fun ty -> let new_map, ty = clone_variable ~loc src !map ty in map := new_map; ty) exp in
    !map, exp

  let clone_expr ~loc src map exp =
    let map = ref map in
    let exp = map_expr (fun ty -> let new_map, ty = clone_variable ~loc src !map ty in map := new_map; ty) exp in
    !map, exp

  let variables_scene_expr ?(set=TypeVariable.Set.empty) exp =
    let set = ref set in
    iter_expr (fun var -> set := TypeVariable.Set.add !set var) exp;
    !set

  let variables_expr ?(set=TypeVariable.Set.empty) exp =
    let set = ref set in
    iter_expr (fun var -> set := TypeVariable.Set.add !set var) exp;
    !set



  type 'a function_specification = {
    optional_arguments: TypeVariable.t IDMap.t;
    optional_arguments_expr: 'a IDMap.t;
    positional_arguments: (IdentifierTable.m * TypeVariable.t) list;
    typ: TypeVariable.t;
    body: 'a option;
    constraints: Type.constr list;
  }

  type env = {
    (* tracking global variable bindings *)
    global_variable_mapping: TypeVariable.t IDMutMap.t;
    global_macro_variable_mapping: TypeVariable.t IDMutMap.t;

    mutable constant_types: TypeVariable.t list;

    global_function_mapping: TypeVariable.t expr function_specification IDMutMap.t;
    global_macro_function_mapping: TypeVariable.t scene_arg function_specification IDMutMap.t;

    mutable global_variable_map: TypeVariable.t Expander.argument IDMap.t;

    (* tracking local value bindings (macro and data variables are shared) *)
    local_scope: TypeVariable.t IDMap.t;
    (* tracking local type bindings *)
    local_type_scope: TypeVariable.t IDMap.t;
    
    default_constraints: Type.constr list;
  }

  let init_env ?(std_lib=[]) tbl src () =
    let default_bindings, default_constraints = default_variables tbl
        (TypeVariable.gen_new_type ~loc:default_loc src) in
    let global_function_mapping = IDMutMap.create () in

    List.iter std_lib ~f:(fun (id, value, typ) ->
        let id = IdentifierTable.add_identifier tbl id in
        let args, constraints = List.map ~f:(fun (id,value) ->
            let id = IdentifierTable.add_identifier tbl id in
            let ty_var = TypeVariable.gen_new_type ~loc:default_loc src () in
            (id, ty_var), `Eq Type.(TypeExpr (TyVariable ty_var), value)
          ) value |> List.unzip in
        ignore @@ IDMutMap.add global_function_mapping
          ~key:id
          ~data:{
            optional_arguments=IDMap.empty; optional_arguments_expr=IDMap.empty;
            positional_arguments=args; typ; body=None; constraints;
          }
      );

    {
    global_variable_mapping= IDMutMap.of_alist_exn default_bindings;
    global_macro_variable_mapping= IDMutMap.create ();

    constant_types=[];

    global_function_mapping;
    global_macro_function_mapping= IDMutMap.create ();

    global_variable_map= IDMap.empty;

    local_scope= IDMap.empty;
    local_type_scope= IDMap.empty;

    default_constraints;
  }


  type conversion_error =
    | UnboundVariable of  IdentifierTable.m * Location.t
    | UnboundMacroVariable of  IdentifierTable.m * Location.t
    | UnboundFunction of  IdentifierTable.m * Location.t
    | UnboundMacroFunction of  IdentifierTable.m * Location.t
    | InvalidModel of string * Location.t
    | DuplicateMapping of IdentifierTable.m * Location.t 
    | MissingFile of string * Location.t
    | AnalysisError of ShaderAnalyzer.analysis_error * Location.t
    | UnsupportedKeywordArgument of IdentifierTable.m * Location.t
    | TooManyArguments of Location.t
    | TooFewArguments of Location.t
    | RepeatedPositionalArgument of Location.t
    | FunctionExpansionError of AstExpander.substitution_error * Location.t


  exception ConversionException of conversion_error

  let with_local_variables ?(value_mapping=[]) ?(type_mapping=[]) (env: env)
      ~(f: env -> 'b) : 'b =
    let local_scope = List.fold value_mapping
        ~init:(env.local_scope) ~f:(fun map (id, ty) -> IDMap.set map ~key:id ~data:(ty)) in
    let local_type_scope = List.fold type_mapping
        ~init:(env.local_type_scope) ~f:(fun map (id, ty) -> IDMap.set map ~key:id ~data:(ty)) in
    let global_variable_map =
      List.fold value_mapping ~init:env.global_variable_map
        ~f:(fun map (id,_) -> IDMap.remove map id) in
    f {env with local_scope; local_type_scope; global_variable_map}

  (* looks up a data expression *)
  let variable_lookup ?required_variables src loc identifier (env:env)  =
    let fresh = TypeVariable.gen_new_type ~loc src in
    match IDMap.find env.local_scope identifier with
    | Some v -> v
    | None -> match IDMutMap.find env.global_variable_mapping identifier with
      | None ->
        begin match required_variables with
          | None -> raise (ConversionException (UnboundVariable (identifier, loc)))
          | Some out_ref ->
            let new_var = fresh () in
            out_ref := (identifier, new_var) :: !out_ref;
            new_var
        end
      | Some v -> v

  (* looks up a macro variable *)
  let macro_variable_lookup _src loc identifier (env:env)  =
    (* let fresh = TypeVariable.gen_new_type ~loc src in *)
    match IDMap.find env.local_scope identifier with
    | Some v -> v
    | None -> match IDMutMap.find env.global_macro_variable_mapping identifier with
      | Some typ -> typ
      | _ -> raise (ConversionException (UnboundVariable (identifier, loc)))


  let type_variable_lookup ~loc src identifier (env:env)  =
    let local_type_scope =
      IDMap.update env.local_type_scope identifier
        ~f:(fun v -> match v with
            | None -> TypeVariable.gen_new_type ~loc src ()
            | Some v -> v )
    in
    IDMap.find_exn local_type_scope identifier,  {env with local_type_scope}

  let of_typemap f src env (exp: 'a Type.typemap) : 'b Type.typemap * env =
    let env = ref env in
    Map.map exp ~f:(fun args ->
        let (vl, env') = f src !env args in
        env := env';
        vl), !env

  let add_macro_variable (env: env) identifier value body =
    IDMutMap.set env.global_macro_variable_mapping
      ~key:identifier ~data:value;
    env.global_variable_map <- IDMap.set env.global_variable_map ~key:identifier ~data:body

  let add_macro_function (env: env) identifier value =
    IDMutMap.set env.global_macro_function_mapping
      ~key:identifier ~data:value

  let add_variable (env: env) identifier value body =
    IDMutMap.set env.global_variable_mapping
      ~key:identifier ~data:value;
    env.global_variable_map <- IDMap.set env.global_variable_map ~key:identifier ~data:body

  let add_function (env: env) identifier value =
    IDMutMap.set env.global_function_mapping
      ~key:identifier ~data:value

  let lookup_function ~loc env identifier  =
    match IDMutMap.find env.global_function_mapping identifier with
    | None -> raise (ConversionException (UnboundFunction (identifier, loc)))
    | Some v -> v

  let lookup_macro_function ~loc env identifier  =
    match IDMutMap.find env.global_macro_function_mapping identifier with
    | None -> raise (ConversionException (UnboundMacroFunction (identifier, loc)))
    | Some v -> v


  let of_with_variable ?loc:_ f src env exp =
    match exp with
    | R.TyDirect v -> let ty,env = f src env v in Type.TyDirect ty, env
    | R.TyVariable { value=id; loc } ->
      let ty,env = type_variable_lookup ~loc src id env in
      Type.TyVariable ty, env      
    | R.Hole loc ->
      Type.TyVariable (TypeVariable.gen_new_type ~loc src ()), env

  let of_with_variable_any ?loc:_ f src env exp =
    match exp with
    | R.TyDirect v -> let ty,env = f src env v in Type.TyDirect ty, env
    | R.TyVariable { value=id; loc } ->
      let ty,env = type_variable_lookup ~loc src  id env in
      Type.TyVariable ty, env      
    | R.Hole loc ->
      Type.TyVariable (TypeVariable.gen_new_type ~loc src ()), env

  let wrap_any_f f src env v =
    let ty, env = f src env v in
    Type.Specialized ty, env 

  let rec of_internal_type src env (exp: S.internal_type_expr) =
    match exp with
    | S.TyPure ty -> let (ty,env) = (of_pure_data_type src env ty) in Type.TyPure ty, env
    | S.TyTexture ty -> let (ty,env) = (of_texture_type src env ty) in Type.TyTexture ty, env
    | S.TyScene ty -> let (ty,env) = (of_scene_type src env ty) in Type.TyScene ty, env
    | S.TyList S.TypeExpr v ->
      let (ty,env) = of_with_variable of_internal_type src env v in Type.TyList (ty), env
  and of_mapping src env (R.MkTyMapping {value=(attrs, uniforms, textures); loc}: R.mapping) : Type.mapping * _ =
    let (attrs, env) =
      of_with_variable ~loc (of_typemap (of_with_variable ~loc of_pure_data_type)) src env attrs in
    let (uniforms, env) =
      of_with_variable ~loc (of_typemap (of_with_variable ~loc of_pure_data_type)) src env uniforms in
    let (textures, env) =
      of_with_variable ~loc (of_typemap (of_with_variable ~loc of_texture_type)) src env textures in
    Type.MkTyMapping (Type.Base (Type.Base attrs, Type.Base uniforms, Type.Base textures)), env
  and of_scene_type src env (exp: R.scene_type) = match exp with
    | R.TyObject Location.{value=(req, lights);loc} ->
      let (req, env) = of_with_variable (wrap_any_f of_mapping) src env req in
      let (lights, env) = of_with_variable (wrap_any_f of_mapping) src env lights in
      Type.TyObject (req, lights,loc), env
    | R.TyModifier {value=(req, lights); loc=loc} ->
      let (req, env) = of_with_variable (wrap_any_f of_mapping) src env req in
      let (lights, env) = of_with_variable (wrap_any_f of_mapping) src env lights in
      Type.TyModifier (req, lights, loc), env
    | R.TyLight {value=req; loc} ->
      let (req, env) = of_with_variable of_mapping src env req in
      Type.TyLight (req,loc), env
    | R.TyShader {value=(mapping, count); loc} ->
      let (mapping, env) = of_with_variable of_mapping src env mapping  in
      let (count, env) = of_with_variable (fun _src env v -> v, env) src env count in
      Type.TyShader (mapping,count,loc),env
    | R.TyDrawable {value=(light_mapping, count);loc} ->
      let (light_mapping, env) = of_with_variable (wrap_any_f of_mapping) src env light_mapping  in
      let (count, env) = of_with_variable (fun _src env v -> v, env) src env count in
      Type.TyDrawable (light_mapping, count,loc),env
    | R.TyMulti {value=int;loc=_} ->
      let (int, env) = of_with_variable of_scene_type src env int  in
      Type.TyMulti int, env
  and of_texture_type _src env (exp: R.texture_type) = match exp with
    | R.TyTexture1D loc -> (Type.TyTexture1D loc), env
    | R.TyTexture2D loc -> Type.TyTexture2D loc, env
    | R.TyTexture3D loc -> Type.TyTexture3D loc, env
    | R.TyTextureCube loc -> Type.TyTextureCube loc, env
  and of_pure_data_type src env (exp: R.pure_data_type) =
    match exp with
    | R.TyScalar ty -> let (ty,env) = (of_scalar_type src env ty) in Type.TyScalar ty, env
    | R.TyNonScalar ty -> let (ty,env) = (of_non_scalar_type src env ty) in Type.TyNonScalar ty, env
    | R.TyNumericData loc -> Type.TyNumericData loc, env
  and of_scalar_type _src env (exp: R.scalar_type) = match exp with
    | R.TyBool loc -> Type.TyBool loc, env
    | R.TyInt loc -> Type.TyInt loc, env
    | R.TyFloat loc -> Type.TyFloat loc, env
  and of_non_scalar_type src env (exp: R.non_scalar_type) = match exp with
    | R.TyVec2 { value=v; loc } ->
      let (ty,env) = of_with_variable of_scalar_type src env v in
      Type.TyVec2 {value=ty;loc},env
    | R.TyVec3 {value=v; loc} ->
      let (ty,env) = of_with_variable of_scalar_type src env v in
      Type.TyVec3 {value=ty; loc},env
    | R.TyVec4 {value=v; loc} ->
      let (ty,env) = of_with_variable of_scalar_type src env v in
      Type.TyVec4 {value=ty; loc},env      
    | R.TyMat2 {value=v; loc} ->
      let (ty,env) = of_with_variable of_scalar_type src env v in
      Type.TyMat2 {value=ty; loc},env      
    | R.TyMat3 {value=v; loc} ->
      let (ty,env) = of_with_variable of_scalar_type src env v in
      Type.TyMat3 {value=ty; loc},env
    | R.TyMat4 {value=v; loc} ->
      let (ty,env) = of_with_variable of_scalar_type src env v in
      Type.TyMat4 {value=ty; loc},env
  let of_type src env (S.TypeExpr expr: S.type_expr) =
    let (expr, env) = of_with_variable of_internal_type src env expr in
    Type.TypeExpr expr, env

  let from_pure pure =
    Type.TypeExpr (Type.TyDirect (Type.TyPure pure))
  let from_scalar scalar = from_pure (Type.TyScalar scalar) 
  let from_texture texture = Type.TypeExpr (Type.TyDirect (Type.TyTexture texture))
  let from_non_scalar non_scalar = from_pure (Type.TyNonScalar non_scalar) 
  let from_variable var = Type.TypeExpr (TyVariable var)
  let from_scene_type st =
    Type.TypeExpr (Type.TyDirect (Type.TyScene st))
  let list_from_variable var =
    Type.TypeExpr (Type.TyDirect (Type.TyList (Type.TyVariable var)))

  let make_mapping ~loc ?(attributes=[]) ?(uniforms=[]) ?(textures=[]) () =
    let of_result (type a) (result: [`Duplicate_key of IdentifierTable.m | `Ok of a ]) = match result with
      | `Duplicate_key vl -> raise (ConversionException (DuplicateMapping (vl, loc)))
      | `Ok v -> Type.Base (Type.TyDirect v) in
    Type.MkTyMapping 
      (Type.Base (of_result @@ IDMap.of_alist attributes,
                  of_result @@ IDMap.of_alist uniforms,
                  of_result @@ IDMap.of_alist textures))


  let gen_global_type_map env =
      let global_vars = IDMutMap.data env.global_variable_mapping in
      let global_macro_vars = IDMutMap.data env.global_macro_variable_mapping in
      let local_scope_vars = IDMap.data env.local_scope in
      let local_type_scope_vars = IDMap.data env.local_type_scope in
      let constant_vars = env.constant_types in
      let vars = global_vars @ global_macro_vars @ local_scope_vars @ local_type_scope_vars @ constant_vars in
      let map =
        List.fold vars ~init:(TypeVariable.Map.empty)
          ~f:(fun map var -> TypeVariable.Map.set map ~key:var ~data:var) in
      map

  (* returns (applied to light, applied to modifier information) *)
  let annotate_function_application
      ~loc
      ~function_specification:({
          optional_arguments;
          optional_arguments_expr;
          positional_arguments;
          body; constraints; typ
        })
      ~keyword_args:(input_kwargs: 'a IDMap.t) ~positional_args:(input_positional: 'a list)
      ~annotate_value ~f_type_value ~f_to_expander
      ~global_type_map ~global_variable_map ~clone_body ~expand_body ~gen_var =
    let function_positional = List.mapi positional_arguments ~f:(fun ind v -> (ind, v)) in
    let split id ls =
      let rec loop acc elems = match elems with
        | [] -> raise (ConversionException (UnsupportedKeywordArgument (id, loc)))
        | (ind, (h, v)) :: t ->
          if (IdentifierTable.equal_m h id)
          then (ind, (h,v)), (List.rev acc @ t)
          else loop ((ind, (h,v)) :: acc) t
      in
      loop [] ls in

    match body with
    | Some body ->
      let (function_positional, type_map), ls =
        List.fold_map
          ~init:(function_positional, global_type_map)
          (IDMap.to_alist input_kwargs)
          ~f:(fun (function_positional, type_map) (keyword, value) ->
              let value, c1 = annotate_value value in
              let ty_var = f_type_value value in
              let value = f_to_expander value in
              match IDMap.find optional_arguments keyword with
              | Some function_ty_var ->
                let constraints = c1 in
                let type_map = TypeVariable.Map.set  type_map ~key:function_ty_var ~data:ty_var  in
                (function_positional, type_map), (Some (keyword, value), None , constraints)
              | None ->
                let (ind, (_, function_ty_var)), function_positional = split keyword function_positional in
                let constraints = c1 in
                let type_map = TypeVariable.Map.set type_map ~key:function_ty_var ~data:ty_var  in
                (function_positional, type_map), (None, Some (ind, (keyword, value)), constraints)
            ) in
      let kwargs, positionals, c1 = List.unzip3 ls in
      let kwargs, positionals, c1 = List.filter_opt kwargs,
                                    List.filter_opt positionals,
                                    List.concat  c1 in
      let remaining_positional = match List.zip function_positional input_positional with
        | Base.List.Or_unequal_lengths.Unequal_lengths -> raise (ConversionException (TooManyArguments loc))
        | Base.List.Or_unequal_lengths.Ok ls -> ls in
      let type_map, (remaining_positional, c2) =
        List.fold_map remaining_positional ~init:type_map ~f:(fun type_map ((ind, (keyword, function_ty_var)), value) ->
            let value, c1 = annotate_value value in
            let ty_var = f_type_value value in
            let value = f_to_expander value in
            let constraints = c1 in
            let type_map = TypeVariable.Map.set type_map ~key:function_ty_var ~data:ty_var  in
            type_map, ((ind, (keyword, value)), constraints)
          ) |> (fun (a,b) -> a, List.unzip b) in
      let c2 = List.concat c2 in
      let positionals =
        let elems = positionals @ remaining_positional in
        let arr = Option_array.create ~len:(List.length elems) in
        List.iter elems
          ~f:(fun (ind, value) ->
              if Option_array.is_some arr ind then raise (ConversionException (RepeatedPositionalArgument loc));
              Option_array.set_some arr ind value
            );
        List.range 0 (List.length elems) |> List.map ~f:(fun ind ->
            if Option_array.is_none arr ind then raise (ConversionException (TooFewArguments loc));
            Option_array.get_some_exn arr ind
          ) 
      in
      let expander_map =
        let local_map = IDMap.of_alist_exn (kwargs @ positionals) in
        let local_map = IDMap.merge local_map optional_arguments_expr ~f:(fun ~key:_ v ->
            match v with
            | `Both (l,_) -> Some l | `Left l -> Some l | `Right r -> Some (f_to_expander r)
          ) in
        IDMap.merge local_map global_variable_map
          ~f:(fun ~key:_ v -> match v with
              | `Both (local, _) -> Some (local)
              | `Left v -> Some v
              | `Right v -> Some v)
      in

      let result_var = gen_var () in
      let type_map = TypeVariable.Map.set type_map ~key:typ ~data:result_var in

      let ((type_map, body): TypeVariable.t TypeVariable.Map.t * _) =
        clone_body type_map body in

      let expanded_body = expand_body expander_map body in

      let constraints = List.map ~f:(fun constr ->
          Type.map_constr
            (fun var -> match TypeVariable.Map.find type_map var with | None -> var | Some var -> var) constr
        ) constraints in
      `Left (expanded_body, constraints @ c1 @ c2, result_var)
    | None -> 
      let (function_positional, type_map), ls =
        List.fold_map
          ~init:(function_positional, global_type_map)
          (IDMap.to_alist input_kwargs)
          ~f:(fun (function_positional, type_map) (keyword, value) ->
              let value, c1 = annotate_value value in
              let ty_var = f_type_value value in
              match IDMap.find optional_arguments keyword with
              | Some function_ty_var ->
                let constraints = c1 in
                let type_map = TypeVariable.Map.set  type_map ~key:function_ty_var ~data:ty_var  in
                (function_positional, type_map), (Some (keyword, value), None , constraints)
              | None ->
                let (ind, (_, function_ty_var)), function_positional = split keyword function_positional in
                let constraints = c1 in
                let type_map = TypeVariable.Map.set type_map ~key:function_ty_var ~data:ty_var  in
                (function_positional, type_map), (None, Some (ind,  value), constraints)
            ) in
      let kwargs, positionals, c1 = List.unzip3 ls in
      let kwargs, positionals, c1 = List.filter_opt kwargs,
                                    List.filter_opt positionals,
                                    List.concat  c1 in
      let remaining_positional = match List.zip function_positional input_positional with
        | Base.List.Or_unequal_lengths.Unequal_lengths -> raise (ConversionException (TooManyArguments loc))
        | Base.List.Or_unequal_lengths.Ok ls -> ls in
      let type_map, (remaining_positional, c2) =
        List.fold_map remaining_positional ~init:type_map ~f:(fun type_map ((ind, (_, function_ty_var)), value) ->
            let value, c1 = annotate_value value in
            let ty_var = f_type_value value in
            let constraints = c1 in
            let type_map = TypeVariable.Map.set type_map ~key:function_ty_var ~data:ty_var  in
            type_map, ((ind, value), constraints)
          ) |> (fun (a,b) -> a, List.unzip b) in
      let c2 = List.concat c2 in
      let positionals =
        let elems = positionals @ remaining_positional in
        let arr = Option_array.create ~len:(List.length elems) in
        List.iter elems
          ~f:(fun (ind, value) ->
              if Option_array.is_some arr ind then raise (ConversionException (RepeatedPositionalArgument loc));
              Option_array.set_some arr ind value
            );
        List.range 0 (List.length elems) |> List.map ~f:(fun ind ->
            if Option_array.is_none arr ind then raise (ConversionException (TooFewArguments loc));
            Option_array.get_some_exn arr ind
          ) 
      in
      let kwargs =
        let local_map = IDMap.of_alist_exn kwargs in
        let local_map = IDMap.merge local_map optional_arguments_expr ~f:(fun ~key:_ v ->
            match v with
            | `Both (l,_) -> Some l | `Left l -> Some l | `Right r -> Some r
          ) in
        local_map
      in
      let result_var = gen_var () in
      let type_map = TypeVariable.Map.set type_map ~key:typ ~data:result_var in
      let constraints = List.map ~f:(fun constr ->
          Type.map_constr
            (fun var -> match TypeVariable.Map.find type_map var with | None -> gen_var () | Some var -> var)
            constr
        ) constraints in
      `Right (kwargs, positionals, constraints @ c1 @ c2, result_var)

  let expand_expr ~loc map value = try
           AstExpander.expand_expr map value
    with AstExpander.SubstitutionException error -> raise (ConversionException (FunctionExpansionError (error, loc)))
    
  let expand_modifier ~loc map value = try
           AstExpander.expand_modifier map value
    with AstExpander.SubstitutionException error -> raise (ConversionException (FunctionExpansionError (error, loc)))

  let expand_obj ~loc map value = try
           AstExpander.expand_obj map value
    with AstExpander.SubstitutionException error -> raise (ConversionException (FunctionExpansionError (error, loc)))

  let expand_base_object ~loc map value = try
           AstExpander.expand_base_object map value
    with AstExpander.SubstitutionException error -> raise (ConversionException (FunctionExpansionError (error, loc)))
  

  let expand_shader ~loc map value = try
           AstExpander.expand_shader map value
    with AstExpander.SubstitutionException error -> raise (ConversionException (FunctionExpansionError (error, loc)))

  let expand_drawable ~loc map value = try
           AstExpander.expand_drawable map value
    with AstExpander.SubstitutionException error -> raise (ConversionException (FunctionExpansionError (error, loc)))

  let expand_scene_arg ~loc map value = try
           AstExpander.expand_scene_arg map value
    with AstExpander.SubstitutionException error -> raise (ConversionException (FunctionExpansionError (error, loc)))



  let rec annotate_expr ?required_variables (src: TypeVariable.src) (env: env) (expr: S.expr)
    : (TypeVariable.t expr * Type.constr list) =
    let fresh ~loc = TypeVariable.gen_new_type ~loc src in
    (match expr with
     | S.Exp_bool { value; loc } ->
       let ty_var = fresh ~loc () in
       (* boolean values have type bool *)
       env.constant_types <- ty_var :: env.constant_types;
       Exp_bool (value, ty_var), [`Eq (TypeExpr (TyVariable ty_var), from_scalar (Type.TyBool loc))]

     | S.Exp_number { value; loc } ->
       let ty_var = fresh ~loc () in
       let ty = Type.TyIntOrFloat loc in
       env.constant_types <- ty_var :: env.constant_types;
       Exp_number (value, ty_var), [`Eq (from_variable ty_var, from_scalar ty)] 

     | S.Exp_variable { value; loc } ->
       let ty_var = variable_lookup ?required_variables src loc value env in
       let value = Exp_variable (value, ty_var) in
       let value = expand_expr ~loc  env.global_variable_map value in
       value, []
     | S.Exp_binop { value=(binop, e1, e2); loc } ->
       let ty_var = fresh ~loc () in
       let e1, c1 = annotate_expr ?required_variables src env e1 in
       let e2, c2 = annotate_expr ?required_variables src env e2 in
       let constraints =
         `Eq (from_variable ty_var, from_pure (Type.TyNumericData loc)) ::
         `Eq (from_variable (AstUtils.type_expr e1), from_pure (Type.TyNumericData loc)) ::
         `Eq (from_variable (AstUtils.type_expr e2), from_pure (Type.TyNumericData loc)) ::
         c1 @ c2 in
       Exp_binop ((binop, e1, e2), ty_var) , constraints

     | S.Exp_preop { value=(preop, expr); loc } ->
       let ty_var = fresh ~loc () in
       let expr, c1 = annotate_expr ?required_variables src env expr in
       let constraints =
         `Eq (from_variable ty_var, from_pure (Type.TyNumericData loc)) ::
         `Eq (from_variable (AstUtils.type_expr expr), from_pure (Type.TyNumericData loc)) ::
         c1 in
       Exp_preop ((preop, expr), ty_var) , constraints
     | S.Exp_index { value=(e1,e2); loc } ->
       (* e1 : t1 [ e2 : t2 ] : ty_var *)
       let ty_var = fresh ~loc () in
       let e1, c1 = annotate_expr ?required_variables src env e1 in
       let e2, c2 = annotate_expr ?required_variables src env e2 in
       let constraints =
         (* ty_var is produced by indexing into t1 *)
         `Indexed (from_variable ty_var, from_variable (AstUtils.type_expr e1)) ::
         (* t2 must be numeric *)
         `Eq (from_variable (AstUtils.type_expr e2), from_scalar (Type.TyInt loc)) ::
         c1 @ c2 in
       Exp_index ((e1,e2), ty_var), constraints
     | S.Exp_vector { value; loc } ->
       let ty_var = fresh ~loc () in
       let (e1,c1) = annotate_vector ty_var loc src env value in
       Exp_vector (e1, ty_var), c1
     | S.Exp_list { value; loc } ->
       let ty_var = fresh ~loc () in
       begin match value with
         | [] ->
           (* empty list - elements could be any type *)
           let elem_var = fresh ~loc () in
           Exp_list ([], ty_var), [`Eq (from_variable ty_var,
                                        Type.TypeExpr (Type.TyDirect
                                                         (Type.TyList (Type.TyVariable elem_var))))]
         | (h :: t)  ->
           let (h, c) = annotate_expr ?required_variables src env h in
           let elem_ty = AstUtils.type_expr h in
           let (values, constraints) = List.fold t ~init:([h],c) ~f:(fun (ls, constraints) exp ->
               let (exp, c) =  annotate_expr ?required_variables src env exp in
               let constraints =
                 `Eq (from_variable elem_ty, from_variable @@ AstUtils.type_expr exp) ::
                 c @ constraints
               in
               (exp :: ls, constraints)
             ) in
           let constraints =
             `Eq (from_variable ty_var,
                  Type.TypeExpr (Type.TyDirect
                                   (Type.TyList (Type.TyVariable elem_ty)))) ::  constraints in
           Exp_list (List.rev values, ty_var), constraints
       end
     | S.Exp_list_comprehension { value=(comprehension_expression, { value=id; loc=id_loc }, from_exp, to_exp); loc } ->
       (* if to exp present then, this is a from ... to expression (from_exp to_exp are numeric)
          if not, then from is a list expression
       *)
       let ty_var = fresh ~loc () in
       begin match to_exp with
         | Some to_exp ->
           let from_exp, c1 = annotate_expr ?required_variables src env from_exp in
           let to_exp, c2 = annotate_expr ?required_variables src env to_exp in

           (* both must be numeric types *)
           let from_exp_type = AstUtils.type_expr from_exp in
           let to_exp_type = AstUtils.type_expr to_exp in
           let id_type = fresh ~loc:id_loc () in

           let comprehension_expression, c3 =
             with_local_variables
               ~value_mapping:[id, id_type] env ~f:(fun env ->
                   annotate_expr ?required_variables src env comprehension_expression) in
           (* type of elements of the comprehension *)
           let elem_ty = AstUtils.type_expr comprehension_expression in

           let constraints =
             (* from and to expressions must be integer ranges *)
             `Eq (from_variable from_exp_type, from_scalar (Type.TyInt loc)) ::
             `Eq (from_variable to_exp_type, from_scalar (Type.TyInt loc)) ::
             (* id variable is assigned a type which is int  *)
             `Eq (from_variable id_type, from_scalar (Type.TyInt loc)) ::

             (* type of resulting list is a subtype of list instantiated with element type *)
             `Eq (from_variable ty_var,
                  Type.TypeExpr (Type.TyDirect
                                   (Type.TyList (Type.TyVariable elem_ty)))) ::
             c1 @ c2 @ c3
           in
           Exp_list_comprehension ((comprehension_expression, id, from_exp, Some to_exp), ty_var), constraints
         | None ->
           (* not present so from expr must be a list type  *)
           let from_exp, c1 = annotate_expr ?required_variables src env from_exp in
           let from_exp_type = AstUtils.type_expr from_exp in

           (* identifier will be the type of list  *)
           let id_type = fresh ~loc:id_loc () in

           let comprehension_expression, c2 =
             with_local_variables
               ~value_mapping:[id, id_type] env ~f:(fun env ->
                   annotate_expr ?required_variables src env comprehension_expression) in
           (* type of elements of the comprehension *)
           let elem_ty = AstUtils.type_expr comprehension_expression in
           let constraints =
             (* from type must be a list with elements of idtype *)
             `Eq (from_variable from_exp_type, list_from_variable id_type) ::
             (* type of resulting list is a subtype of list instantiated with element type *)
             `Eq (from_variable ty_var, list_from_variable elem_ty) ::
             c1 @ c2
           in
           Exp_list_comprehension ((comprehension_expression, id, from_exp, None), ty_var), constraints
       end
     | S.Exp_if_then_else { value=(cond, e1, e2); loc } ->
       let ty_var = fresh ~loc () in
       let e1, c1 = annotate_expr ?required_variables src env e1 in
       let e2, c2 = annotate_expr ?required_variables src env e2 in
       let cond, c3 = annotate_expr ?required_variables src env cond in
       let constraints =
         (* type of condition must be boolean! *)
         `Eq (from_variable (AstUtils.type_expr cond), from_scalar (Type.TyBool loc)) ::
         (* type of branches must be equal *)
         `Eq (from_variable (AstUtils.type_expr e1), from_variable (AstUtils.type_expr e2)) ::
         (* type of result is equal to the expressions *)
         `Eq (from_variable (AstUtils.type_expr e1), from_variable ty_var) ::
         c1 @ c2 @ c3 in
       Exp_if_then_else ((cond, e1, e2), ty_var), constraints
     | S.Exp_function_call ({ value=fname; loc },
                            S.FunctionArguments { keyword_args; positional_args }) -> begin
         let global_type_map = gen_global_type_map env in
         let function_specification = lookup_function ~loc env fname in

         let result =
           annotate_function_application
             ~loc
             ~function_specification
             ~keyword_args
             ~positional_args
             ~annotate_value:(fun sarg ->
                 let expr, c1 = annotate_expr src env sarg in
                 expr, c1
               )
             ~f_type_value:(AstUtils.type_expr)
             ~f_to_expander:(fun e -> Expander.ArgExpression e)
             ~global_type_map
             ~global_variable_map:env.global_variable_map
             ~clone_body:(clone_expr ~loc src)
             ~expand_body:(expand_expr ~loc)
             ~gen_var:(fresh ~loc)
         in
         match result with
         | `Left (e1, constraints, _) ->
           e1, constraints
         | `Right (keyword_args, positional_args, constraints, typ)  ->
           Exp_function_call (
             fname, FunctionArguments {
               keyword_args;
               positional_args}, typ
           ), constraints
       end
     | S.Exp_with_type { value=(exp, ty_exp); _ } ->
       (*  if we type an expression, we first convert the user provided type to our types
           note: we create a new env which has any type variables the user referenced *)
       let ty, env = of_type src env ty_exp in
       (* we type the expression using updated environment *)
       let exp, c1 = annotate_expr ?required_variables src env exp in
       (* introduce a new constraint that the type of the expression must be a subtype of the user's type *)
       let ty_var = AstUtils.type_expr exp in
       let constraints =
         `Eq (from_variable ty_var, ty) ::
         (* type of resulting list is a subtype of list instantiated with element type *)
         c1 in
       exp, constraints
    )
  and annotate_vector ?required_variables
      parent loc (src: TypeVariable.src) (env: env) (expr: S.vector) = match expr with
    | S.Vector2 (e1, e2) ->
      let e1, c1 = annotate_expr ?required_variables src env e1 in
      let e2, c2 = annotate_expr ?required_variables src env e2 in
      let elem_type = (AstUtils.type_expr e1) in
      let constraints =
        `Eq (from_variable parent, from_non_scalar @@ Type.TyVec2 {value=(TyVariable elem_type); loc} ) ::
        `Eq (from_variable (AstUtils.type_expr e1), from_variable (AstUtils.type_expr e2)) ::
        c1 @ c2 in
      Vector2 (e1,e2), constraints
    | S.Vector3 (e1,e2,e3) ->
      let e1, c1 = annotate_expr ?required_variables src env e1 in
      let e2, c2 = annotate_expr ?required_variables src env e2 in
      let e3, c3 = annotate_expr ?required_variables src env e3 in
      let elem_type = (AstUtils.type_expr e1) in
      let constraints =
        `Eq (from_variable parent, from_non_scalar @@ Type.TyVec3 {value=(TyVariable elem_type); loc} ) ::
        `Eq (from_variable @@ AstUtils.type_expr e1, from_variable @@ AstUtils.type_expr e2) ::
        `Eq (from_variable @@ AstUtils.type_expr e1, from_variable @@ AstUtils.type_expr e3) ::
        c1 @ c2 @ c3 in
      Vector3 (e1,e2, e3), constraints
    | S.Vector4 (e1,e2,e3,e4) ->
      let e1, c1 = annotate_expr ?required_variables src env e1 in
      let e2, c2 = annotate_expr ?required_variables src env e2 in
      let e3, c3 = annotate_expr ?required_variables src env e3 in
      let e4, c4 = annotate_expr ?required_variables src env e4 in
      let elem_type = (AstUtils.type_expr e1) in
      let constraints =
        `Eq (from_variable parent, from_non_scalar @@ Type.TyVec4 {value=(TyVariable elem_type);loc} ) ::
        `Eq (from_variable @@ AstUtils.type_expr e1, from_variable @@ AstUtils.type_expr e2) ::
        `Eq (from_variable @@ AstUtils.type_expr e1, from_variable @@ AstUtils.type_expr e3) ::
        `Eq (from_variable @@ AstUtils.type_expr e1, from_variable @@ AstUtils.type_expr e4) ::
        c1 @ c2 @ c3 @ c4 in
      Vector4 (e1,e2, e3, e4), constraints
  and annotate_data
      tbl  (src: TypeVariable.src) (env: env) (data: S.data) =
    let loc = match data with
      | S.Model { loc;_ } -> loc
      | S.Data {loc; _} -> loc
      | S.Screen {loc; _} -> loc in
    let fresh ?(loc=loc) = TypeVariable.gen_new_type ~loc src in
    let uniforms =
      [
        IdentifierTable.add_identifier tbl "model",
        Type.TyDirect (Type.TyNonScalar (Type.TyMat4 {value=(Type.TyDirect (Type.TyFloat loc)); loc}));
        IdentifierTable.add_identifier tbl "view",
        Type.TyDirect (Type.TyNonScalar (Type.TyMat4 {value=(Type.TyDirect (Type.TyFloat loc)); loc}));
        IdentifierTable.add_identifier tbl "projection",
        Type.TyDirect (Type.TyNonScalar (Type.TyMat4 {value=(Type.TyDirect (Type.TyFloat loc)); loc}));
      ] in
    match data with
    | S.Model { value; loc } ->
      let components = String.split ~on:'.' value |> List.last in
      begin match components with
        | Some "obj" ->
          let object_mapping =
            make_mapping ~loc
              ~attributes:[
                IdentifierTable.add_identifier tbl "vertex_position",
                Type.TyDirect (Type.TyNonScalar (Type.TyVec3 {value=(Type.TyDirect (Type.TyFloat loc));loc}));
                IdentifierTable.add_identifier tbl "vertex_normals",
                Type.TyDirect (Type.TyNonScalar (Type.TyVec3 {value=(Type.TyDirect (Type.TyFloat loc));loc}));
                IdentifierTable.add_identifier tbl "tex_coords",
                Type.TyDirect (Type.TyNonScalar (Type.TyVec2 {value=(Type.TyDirect (Type.TyFloat loc));loc}))
              ]
              ~uniforms
              ()
          in
          let ty_var = fresh ~loc () in
          let constraints = [
            `Eq (from_variable ty_var,
                 from_scene_type @@
                 Type.TyObject (
                   TyDirect (Specialized (object_mapping)),
                   TyDirect Any, loc
                 )
                )
          ] in
          Model (value, ty_var), constraints
        | Some "stl" ->
          let ty_var = fresh ~loc () in
          let object_mapping =
            make_mapping ~loc
              ~attributes:[
                IdentifierTable.add_identifier tbl "vertex_position",
                Type.TyDirect (Type.TyNonScalar (Type.TyVec3 {value=(Type.TyDirect (Type.TyFloat loc));loc}));
                IdentifierTable.add_identifier tbl "vertex_normals",
                Type.TyDirect (Type.TyNonScalar (Type.TyVec3 {value=(Type.TyDirect (Type.TyFloat loc)); loc}))
              ]
              ~uniforms
              ()
          in
          let constraints = [
            `Eq (from_variable ty_var,
                 from_scene_type @@
                 Type.TyObject (
                   TyDirect (Specialized (object_mapping)),
                   TyDirect Any, loc
                 )
                )   ] in
          Model (value, ty_var), constraints
        | _ -> raise (ConversionException (InvalidModel (value, loc)))
      end
    | S.Data { value=elems; loc } ->
      let elems, constraints, attributes =
        List.map elems
          ~f:(fun ({ value=id; loc }, expr)  ->
              let (expr, constraints) = annotate_expr src env expr in
              (loc, (id, expr)), constraints, (id, (Type.TyVariable (AstUtils.type_expr expr)))
            )
        |> List.unzip3 in
      let ids, elems = List.unzip elems in
      begin match ids with
        | [] ->
          let ty_var = fresh ~loc () in
          let empty_map = Type.TyDirect IDMap.empty in
          Data ([], ty_var), [
            `Eq
              Type.(from_variable ty_var,
                    from_scene_type @@
                    TyObject (
                      TyDirect (Specialized (MkTyMapping
                                               (Base (Base empty_map, Base empty_map, Base empty_map)))),
                      TyDirect Any, loc
                    ))]
        | loc :: _ ->
          let ty_var = fresh ~loc () in
          let object_mapping = make_mapping ~loc ~attributes ~uniforms () in

          let constraints =
            `Eq (
              from_variable ty_var,
              from_scene_type @@ Type.TyObject (
                TyDirect (Specialized object_mapping),
                TyDirect Any, loc
              )) ::
            (List.concat constraints)
          in
          Data (elems, ty_var), constraints
      end
    | S.Screen { width; height; loc } ->
      let ty_var = fresh ~loc () in
      let extract_option vl = match vl with
        | None -> None, []
        | Some expr ->
          let expr, constraints = annotate_expr  src env expr in
          Some expr, constraints in
      let width, c1 = extract_option width in
      let height, c2 = extract_option height in
      let object_mapping =
        make_mapping
          ~loc
          ~attributes:[
            IdentifierTable.add_identifier tbl "vertex_position",
            Type.TyDirect (Type.TyNonScalar (Type.TyVec2 {value=(Type.TyDirect (Type.TyFloat loc)); loc}));
            IdentifierTable.add_identifier tbl "tex_coords",
            Type.TyDirect (Type.TyNonScalar (Type.TyVec2 {value=(Type.TyDirect (Type.TyFloat loc)); loc}))
          ]
          ~uniforms
          ()
      in
      let constraints =
        `Eq (
          from_variable ty_var,
          from_scene_type @@ Type.TyObject (
            TyDirect (Specialized object_mapping),
            TyDirect Any, loc
          )) ::
        c1 @ c2 in
      Screen { width; height; typ=ty_var }, constraints
  and annotate_filter_option (fopt: S.filter_option) = match fopt with
    | S.Nearest -> Nearest
    | S.Linear -> Linear
    | S.NearestMipmapNearest -> NearestMipmapNearest
    | S.NearestMipmapLinear -> NearestMipmapLinear
    | S.LinearMipmapNearest -> LinearMipmapNearest
    | S.LinearMipmapLinear -> LinearMipmapLinear
  and annotate_wrap_option (wopt: S.wrap_option) = match wopt with
    | S.Repeat -> Repeat
    | S.MirroredRepeat -> MirroredRepeat
    | S.ClampToEdge -> ClampToEdge
    | S.ClampToBorder -> ClampToBorder
  and annotate_texture_option
      (src: TypeVariable.src) (env: env) (topt: S.texture_option) =
    match topt with
    | S.TextureBaseLevel expr ->
      let loc = (S.expr_to_loc expr) in
      let expr, constraints = annotate_expr  src env expr in
      let ty_var = AstUtils.type_expr expr in
      (* must be an integer value *)
      let constraints =
        `Eq (from_variable ty_var, from_scalar (Type.TyInt loc)) ::
        constraints in
      TextureBaseLevel expr, constraints
    | S.TextureMaxLevel expr ->
      let loc = (S.expr_to_loc expr) in
      let expr, constraints = annotate_expr  src env expr in
      let ty_var = AstUtils.type_expr expr in
      (* must be an integer value *)
      let constraints =
        `Eq (from_variable ty_var, from_scalar (Type.TyInt loc)) ::
        constraints in
      TextureMaxLevel expr, constraints
    | S.TextureBorderColor expr ->
      let loc = (S.expr_to_loc expr) in
      let expr, constraints = annotate_expr  src env expr in
      let ty_var = AstUtils.type_expr expr in
      let constraints = 
        (* TODO: technically integer values are also supported, but hey. *)
        `Eq (from_variable ty_var,
             from_non_scalar @@ Type.TyVec4 {value=(Type.TyDirect (Type.TyFloat loc)); loc}) ::
        constraints in
      TextureBorderColor expr, constraints
    | S.WrapS wopt -> WrapS (annotate_wrap_option wopt), []
    | S.WrapT wopt -> WrapT (annotate_wrap_option wopt), []
    | S.MinFilter fopt -> MinFilter (annotate_filter_option fopt), []
    | S.MagFilter fopt -> MagFilter (annotate_filter_option fopt), []
  and annotate_dimension_options
      (src: TypeVariable.src) (env: env) (dopt: S.dimension_options) =
    match dopt with
    | S.DimensionVector expr ->
      let loc = (S.expr_to_loc expr) in
      let expr, constraints = annotate_expr  src env expr in
      let ty_var = AstUtils.type_expr expr in
      let constraints = 
        (* must be a vec3 of floats *)
        `Eq (from_variable ty_var,
             from_non_scalar @@ Type.TyVec3 {value=(Type.TyDirect (Type.TyFloat loc)); loc}) ::
        constraints in
      DimensionVector expr, constraints
    | S.DimensionXZY { x; y; z; loc } ->
      let unwrap_annotate opt = match opt with
        | None -> None, []
        | Some expr ->
          let expr, constraints = annotate_expr src env expr in
          let ty_var = AstUtils.type_expr expr in
          let constraints =
            (*  must be a float *)
            `Eq (from_variable ty_var,
                 from_scalar @@ (Type.TyFloat loc)) ::
            constraints in
          Some expr, constraints in
      let x, c1 = unwrap_annotate x in
      let y, c2 = unwrap_annotate y in
      let z, c3 = unwrap_annotate z in
      DimensionXZY {x;y;z}, c1 @ c2 @ c3
  and annotate_rotate_options
      (src: TypeVariable.src) (env: env) (S.RotateAngle { x; y; z; vector; loc }: S.rotate_options) =
    let unwrap_annotate ?(parent=from_scalar (Type.TyFloat loc)) opt = match opt with
      | None -> None, []
      | Some expr ->
        let expr, constraints = annotate_expr  src env expr in
        let ty_var = AstUtils.type_expr expr in
        let constraints =
          (*  must be a float *)
          `Eq (from_variable ty_var,
               parent) ::
          constraints in
        Some expr, constraints in
    let x, c1 = unwrap_annotate x in
    let y, c2 = unwrap_annotate y in
    let z, c3 = unwrap_annotate z in
    let vector, c4 =
      unwrap_annotate
        ~parent:(from_non_scalar (Type.TyVec3 {value=(Type.TyDirect (Type.TyFloat loc)); loc}))
        vector in
    RotateAngle {x;y;z;vector}, c1 @ c2 @ c3 @ c4
  and annotate_blend_func (func: S.blend_func) = match func with
    | S.Zero -> Zero
    | S.One -> One
    | S.Src_color -> Src_color
    | S.One_minus_src_color -> One_minus_src_color
    | S.Dst_color -> Dst_color
    | S.One_minus_dst_color -> One_minus_dst_color
    | S.Src_alpha -> Src_alpha
    | S.One_minus_src_alpha -> One_minus_src_alpha
    | S.Dst_alpha -> Dst_alpha
    | S.One_minus_dst_alpha -> One_minus_dst_alpha
    | S.Constant_color -> Constant_color
    | S.One_minus_constant_color -> One_minus_constant_color
    | S.Constant_alpha -> Constant_alpha
    | S.One_minus_constant_alpha -> One_minus_constant_alpha
    | S.Src_alpha_saturate -> Src_alpha_saturate
  and annotate_comparison_func (func: S.comparison_func) = match func with
    | S.Stencil_never -> Stencil_never
    | S.Stencil_less -> Stencil_less
    | S.Stencil_lequal -> Stencil_lequal
    | S.Stencil_greater -> Stencil_greater
    | S.Stencil_gequal -> Stencil_gequal
    | S.Stencil_equal -> Stencil_equal
    | S.Stencil_notequal -> Stencil_notequal
    | S.Stencil_always -> Stencil_always
  and annotate_blend_color_option src env (S.ColourVector expr: S.blend_color_option) =
    let loc = (S.expr_to_loc expr) in
    let expr, constraints = annotate_expr src env expr in
    let ty_var = AstUtils.type_expr expr in
    let constraints = 
      (* must be a vec4 of floats *)
      `Eq (from_variable ty_var,
           from_non_scalar @@ Type.TyVec4 {value=(Type.TyDirect (Type.TyFloat loc)); loc}) ::
      constraints in
    ColourVector expr, constraints
  and annotate_draw_option src env (opt: S.draw_option) =
    let conv_bool_expr expr =
      let loc = (S.expr_to_loc expr) in
      let expr, constraints = annotate_expr src env expr in
      let ty_var = AstUtils.type_expr expr in
      let constraints = 
        (* must be a boolean *)
        `Eq (from_variable ty_var,
             from_scalar @@ (Type.TyBool loc)) ::
        constraints in
      expr, constraints in
    match opt with
    | S.AlphaTest expr ->
      let (expr, constraints) = conv_bool_expr expr in AlphaTest expr, constraints
    | S.BlendTest expr ->
      let (expr, constraints) = conv_bool_expr expr in BlendTest expr, constraints      
    | S.DepthTest expr ->
      let (expr, constraints) = conv_bool_expr expr in DepthTest expr, constraints 
    | S.StencilTest expr ->
      let (expr, constraints) = conv_bool_expr expr in StencilTest expr, constraints       
    | S.BlendColor bopt ->
      let bopt, constraints = annotate_blend_color_option src env bopt in
      BlendColor bopt, constraints
    | S.BlendFunction (f1, f2) ->
      let f1 = annotate_blend_func f1 in
      let f2 = annotate_blend_func f2 in
      BlendFunction (f1,f2), []
    | S.StencilFunction (f1, e1, e2) ->
      let f1 = annotate_comparison_func f1 in
      let loc1 = (S.expr_to_loc e1) in
      let loc2 = (S.expr_to_loc e2) in

      let e1, c1 = annotate_expr src env e1 in
      let e2, c2 = annotate_expr src env e2 in

      let constraints =
        (* ref value must be int *)
        `Eq (from_variable (AstUtils.type_expr e1),
             from_scalar @@ Type.TyInt loc1) ::
        (* mask value must be int *)
        `Eq (from_variable (AstUtils.type_expr e2),
             from_scalar @@ Type.TyInt loc2) ::
        c1 @ c2 in
      StencilFunction (f1, e1, e2), constraints
    | S.DepthFunction f1 ->
      DepthFunction (annotate_comparison_func f1), []
    | S.AlphaFunction (f1, e1) ->
      let loc = (S.expr_to_loc e1) in

      let f1 = annotate_comparison_func f1 in
      let e1, c1 = annotate_expr src env e1 in
      let constraints =
        (* ref value must be float *)
        `Eq (from_variable (AstUtils.type_expr e1),
             from_scalar @@ (Type.TyFloat loc)) ::
        c1 in
      AlphaFunction (f1, e1), constraints
  and annotate_modifier tbl src env (modifier: S.modifier) =
    let fresh ~loc = TypeVariable.gen_new_type ~loc src in
    match modifier with
    | S.Texture ({ loc; value=id; } , files, options) ->
      let options, constraints = List.map ~f:(annotate_texture_option src env) options |> List.unzip in
      let ty_var = fresh ~loc () in

      let files = List.map files ~f:(fun {value=file;loc} -> begin match Sys.file_exists file with
        | `No | `Unknown -> raise (ConversionException (MissingFile (file, loc)))
        | `Yes -> file
      end) in

      let output_mapping =
        (make_mapping ~loc
           ~textures:[
             id, Type.TyDirect (Type.TyAnyTexture loc)
           ] ()) in

      let constraints =
        `Eq (
          from_variable ty_var,
          from_scene_type @@ Type.TyUnresolvedModifier (Type.TyDirect output_mapping)
        ) :: List.concat constraints in
      Texture (id, files, options, ty_var), constraints
    | S.Uniform ({ value=id; loc }, expr) ->
      let expr, c1 = annotate_expr src env expr in
      let expr_ty_var = AstUtils.type_expr expr in
      let ty_var = fresh ~loc () in

      let output_mapping =
        (make_mapping ~loc
           ~uniforms:[
             id, Type.TyVariable expr_ty_var
           ] ()) in
      let constraints =
        `Eq (
          from_variable ty_var,
          from_scene_type @@ Type.TyUnresolvedModifier (Type.TyDirect output_mapping)
        ) :: 
        c1 in
      Uniform (id, expr, ty_var), constraints
    | S.Attribute ({ value=id; loc }, expr) ->
      let expr, c1 = annotate_expr src env expr in
      let expr_ty_var = AstUtils.type_expr expr in
      let ty_var = fresh ~loc () in

      let output_mapping =
        (make_mapping ~loc
           ~attributes:[
             id, Type.TyVariable expr_ty_var
           ] ()) in
      let constraints =
        `Eq (
          from_variable ty_var,
          from_scene_type @@ Type.TyUnresolvedModifier (Type.TyDirect output_mapping)
        ) :: 
        c1 in
      Attribute (id, expr, ty_var), constraints
    | S.Translate dopt ->
      let loc = match dopt with | S.DimensionVector expr -> S.expr_to_loc expr | S.DimensionXZY {loc;_} -> loc in
      let dopt, c1 = annotate_dimension_options src env dopt in
      let ty_var = fresh ~loc () in
      let output_mapping = (make_mapping ~loc ()) in
      let constraints =
        `Eq (
          from_variable ty_var,
          from_scene_type @@ Type.TyUnresolvedModifier (Type.TyDirect output_mapping)
        ) :: c1 in
      Translate (dopt, ty_var), constraints
    | S.Scale dopt ->
      let loc = match dopt with | S.DimensionVector expr -> S.expr_to_loc expr | S.DimensionXZY {loc;_} -> loc in
      let dopt, c1 = annotate_dimension_options src env dopt in
      let ty_var = fresh ~loc () in
      let output_mapping = (make_mapping ~loc ()) in
      let constraints =
        `Eq (
          from_variable ty_var,
          from_scene_type @@ Type.TyUnresolvedModifier (Type.TyDirect output_mapping)
        ) :: c1 in
      Scale (dopt, ty_var), constraints      
    | S.Rotate ropts ->
      let loc = match ropts with | S.RotateAngle { loc; _ } -> loc in
      let ropts, c1 = annotate_rotate_options src env ropts in
      let ty_var = fresh ~loc () in
      let output_mapping = (make_mapping ~loc ()) in
      let constraints =
        `Eq (
          from_variable ty_var,
          from_scene_type @@ Type.TyUnresolvedModifier (Type.TyDirect output_mapping)
        ) :: c1 in
      Rotate (ropts, ty_var), constraints      
    | S.DrawOptions { value=dopts; loc } ->
      let opts, _ =
        List.map ~f:(fun {value; loc} -> annotate_draw_option src env value,loc) dopts
        |> List.unzip in
      let opts, constraints = List.unzip opts in
      let output_mapping = (make_mapping ~loc ()) in
      let ty_var = fresh ~loc () in
      let constraints =
        `Eq (
          from_variable ty_var,
          from_scene_type @@ Type.TyUnresolvedModifier (Type.TyDirect output_mapping)
        ) :: List.concat constraints in
      DrawOptions (opts, ty_var), constraints
    | S.Redirecting { textures=t; depth; stencil; drawable; loc } ->
      let textures = List.map t ~f:(fun { value=v; _ } -> v) in
      let mapping =
        let v =
          List.map t ~f:(fun { value=v; loc } -> (v, Type.TyVariable (fresh ~loc ())))
          |> IDMap.of_alist
          |> fun v -> match v with
          | `Duplicate_key id -> raise (ConversionException (DuplicateMapping (id, loc)))
          |`Ok map -> map
        in
        Type.MkTyMapping (Type.Base (
            Type.Base (Type.TyDirect IDMap.empty),
            Type.Base (Type.TyDirect IDMap.empty),
            Type.Base (Type.TyDirect v)
          ))
      in
      let ty_var = fresh ~loc () in
      let drawable, c1 = annotate_drawable tbl src env  drawable in
      let light_var = Type.TyVariable (fresh ~loc ()) in
      let constraints =
        `Eq (
          from_variable (AstUtils.type_drawable drawable),
          from_scene_type @@
          Type.TyDrawable (light_var,
                           TyDirect (List.length textures), loc)
        ) ::
        `Eq (
          from_variable ty_var,
          from_scene_type @@
          Type.TyModifier (TyDirect (Specialized mapping), light_var,loc)
        ) ::
        c1 in
      Redirecting {
        textures;
        depth; stencil;
        drawable;
        typ=ty_var;
      }, constraints
    | S.LightUniform ({ value=light_name; loc }, {value=id; _}, expr) ->
      let value_mapping, constraints = light_uniform_variables loc tbl (fresh ~loc) in
      let expr, c1 =
        with_local_variables env ~value_mapping ~f:(fun env -> annotate_expr src env expr) in
      let output_mapping = (make_mapping
                              ~uniforms:[
                                id, Type.TyVariable (AstUtils.type_expr expr)
                              ]
                              ~loc ()) in
      let ty_var = fresh ~loc () in
      let constraints =
        `Eq (
          from_variable ty_var,
          from_scene_type @@ Type.TyMulti (Type.TyDirect
                                             (Type.TyUnresolvedModifier (Type.TyDirect output_mapping)))
        ) ::  constraints @ c1 in
      LightUniform (light_name, id, expr, ty_var), constraints
    | S.ModifierReference reference ->
      (match reference with
       | S.DirectReference { value=identifier; loc } ->
         let ty_var = macro_variable_lookup src loc identifier env in
         let constraints =
           `Eq (
             from_variable ty_var,
             from_scene_type @@
             Type.TyModifier (Type.TyVariable (fresh ~loc ()), Type.TyVariable (fresh ~loc ()), loc)
           )
           :: [] in

         let value = ModifierReference (DirectReference (identifier, ty_var)) in
         let value = expand_modifier ~loc env.global_variable_map value in
         value, constraints
       | S.IndirectReference (
           Location.{ value=fname; loc },
           S.MacroArguments { keyword_args; positional_args }) ->

         let global_type_map = gen_global_type_map env in

         let function_specification = lookup_macro_function ~loc env fname in
         let result =
           annotate_function_application
             ~loc
             ~function_specification
             ~keyword_args
             ~positional_args
             ~annotate_value:(fun sarg -> annotate_scene_arg tbl src env sarg)
             ~f_type_value:(AstUtils.type_scene_arg)
             ~f_to_expander:(fun e -> Expander.of_scene_arg e)
             ~global_type_map
             ~global_variable_map:env.global_variable_map
             ~clone_body:(clone_scene_arg ~loc src)
             ~expand_body:(expand_scene_arg ~loc)
             ~gen_var:(fresh ~loc) in
         match result with
         | `Left (e1, constraints, typ) -> begin
             let constraints =
               `Eq (
                 from_variable typ,
                 from_scene_type @@
                 Type.TyModifier (Type.TyVariable (fresh ~loc ()), Type.TyVariable (fresh ~loc ()), loc)
               ) ::
               constraints
             in
             match e1 with
             | ArgReference reference -> ModifierReference reference, constraints
             | SceneInlineExpression (SceneExprModifier modif) -> modif, constraints
             | v -> raise AstExpander.(ConversionException (FunctionExpansionError (InvalidModifier (fname, Expander.of_scene_arg v), loc)))
           end
         | `Right (keyword_args, positional_args, constraints, typ) ->  
           let constraints =
             `Eq (
               from_variable typ,
               from_scene_type @@
               Type.TyModifier (Type.TyVariable (fresh ~loc ()), Type.TyVariable (fresh ~loc ()), loc)
             ) ::
             constraints
           in
           ModifierReference (
             IndirectReference (fname, MacroArguments { keyword_args; positional_args}, typ)
           ), constraints
      )
  and annotate_scene_arg tbl src env (sarg: S.scene_arg) : _ scene_arg * _ =
    let fresh ~loc = TypeVariable.gen_new_type ~loc src in
    match sarg with
    | S.ArgReference reference -> begin (match reference with
        | S.DirectReference { value=reference; loc } ->
          let ty_var = macro_variable_lookup src loc reference env in
          let value = ArgReference (DirectReference (reference, ty_var)) in
          let value = expand_scene_arg ~loc env.global_variable_map value in
          value, []
        | S.IndirectReference ({ value=fname; loc }, S.MacroArguments
                                 { keyword_args; positional_args }) ->
          let global_type_map = gen_global_type_map env in
          let function_specification = lookup_macro_function ~loc env fname in
          let result =
            annotate_function_application
              ~loc
              ~function_specification
              ~keyword_args
              ~positional_args
              ~annotate_value:(fun sarg -> annotate_scene_arg tbl src env sarg)
              ~f_type_value:(AstUtils.type_scene_arg)
              ~f_to_expander:(fun e -> Expander.of_scene_arg e)
              ~global_type_map
              ~global_variable_map:env.global_variable_map
              ~clone_body:(clone_scene_arg ~loc src)
              ~expand_body:(expand_scene_arg  ~loc)
              ~gen_var:(fresh ~loc) in
          match result with
          | `Left (e1, constraints, _) -> e1, constraints
          | `Right (keyword_args, positional_args, constraints, typ) ->  
            ArgReference (
              IndirectReference (fname, MacroArguments { keyword_args; positional_args}, typ)
            ), constraints
      )
      end
    | S.SceneDataExpression expr ->
      let expr, c1 = annotate_expr src env expr in
      SceneDataExpression expr, c1
    | S.SceneInlineExpression sexpr ->
      let sexpr, c1 = annotate_scene_expr tbl src env sexpr in
      SceneInlineExpression sexpr, c1
  and annotate_base_object tbl src env (obj: S.base_object) =
    let fresh ~loc = TypeVariable.gen_new_type ~loc src in
    match obj with
    | S.ObjectReference reference ->
      (match reference with
       | S.DirectReference { value=identifier; loc } ->
         let ty_var = macro_variable_lookup src loc identifier env in
         let constraints =
           `Eq (
             from_variable ty_var,
             from_scene_type @@
             Type.TyObject (Type.TyVariable (fresh ~loc ()), Type.TyVariable (fresh ~loc ()), loc)
           ) ::
            [] in
         let value = ObjectReference (DirectReference (identifier, ty_var)) in
         let value = expand_base_object ~loc env.global_variable_map value in
         let value = match value with
           | `Left v -> `Left v
           | `Right v -> `Right v in
         value, constraints
       | S.IndirectReference (
           Location.{ value=fname; loc },
           S.MacroArguments { keyword_args; positional_args }) -> begin
           let global_type_map = gen_global_type_map env in

           let function_specification = lookup_macro_function ~loc env fname in
           let result =
             annotate_function_application
               ~loc
               ~function_specification
               ~keyword_args
               ~positional_args
               ~annotate_value:(fun sarg -> annotate_scene_arg tbl src env sarg)
               ~f_type_value:(AstUtils.type_scene_arg)
               ~f_to_expander:(fun e -> Expander.of_scene_arg e)
               ~global_type_map
               ~global_variable_map:env.global_variable_map
               ~clone_body:(clone_scene_arg ~loc src)
               ~expand_body:(expand_scene_arg ~loc)
               ~gen_var:(fresh ~loc) in
           match result with
           | `Left (e1, constraints,_typ) -> begin
               let constraints =
                 `Eq (
                   from_variable _typ,
                   from_scene_type @@
                   Type.TyObject (Type.TyVariable (fresh ~loc ()), Type.TyVariable (fresh ~loc ()), loc)
                 ) ::
                 constraints
               in
               match e1 with
               | ArgReference reference -> `Left (ObjectReference reference), constraints
               | SceneInlineExpression (SceneExprObject obj) -> `Right obj, constraints
               | v -> raise AstExpander.(ConversionException (FunctionExpansionError (InvalidObject (fname, Expander.of_scene_arg v), loc)))
             end
           | `Right (keyword_args, positional_args, constraints, typ) ->  
             let constraints =
               `Eq (
                 from_variable typ,
                 from_scene_type @@
                 Type.TyObject (Type.TyVariable (fresh ~loc ()), Type.TyVariable (fresh ~loc ()), loc)
               ) ::
               constraints
             in
             `Left (ObjectReference (
                 IndirectReference (fname, MacroArguments { keyword_args; positional_args}, typ)
               )), constraints
         end


      )

    | S.Light { name={ value=name; loc }; position; size } ->
      let position, c1 = annotate_expr src env position in
      let size, c2 = annotate_expr src env size in
      let ty_var = fresh ~loc () in
      let light_mapping = (make_mapping ~loc ()) in
      let constraints =
        `Eq (from_variable (AstUtils.type_expr position),
             from_non_scalar @@ Type.TyVec3 {value=(Type.TyDirect (Type.TyFloat loc)); loc}
            ) ::
        `Eq (from_variable (AstUtils.type_expr size),
             from_scalar @@ Type.TyFloat loc
            ) ::
        `Eq (from_variable ty_var,
             from_scene_type @@ Type.TyLight (Type.TyDirect light_mapping, loc)
            ) ::
        c1 @ c2 in
      `Left (Light {name; position; size; typ=ty_var }), constraints
    | S.LightData { name={ value=light_tag; loc }; (* value={ value=light_comp; loc=light_comp_doc } *) } ->
      (* parse the expression, but ignore any unbound variables -
         these will be expected to be filled by the modifiers *)
      (* let var = fresh ~loc () in
       * let c1 = [
       *   `Eq (
       *     from_variable (var),
       *     from_pure (Type.TyNumericData loc)
       *   )
       * ] in *)
      let uniforms =
        [
          IdentifierTable.add_identifier tbl "model",
          Type.TyDirect (Type.TyNonScalar (Type.TyMat4 {value=(Type.TyDirect (Type.TyFloat loc)); loc}));
          IdentifierTable.add_identifier tbl "view",
          Type.TyDirect (Type.TyNonScalar (Type.TyMat4 {value=(Type.TyDirect (Type.TyFloat loc)); loc}));
          IdentifierTable.add_identifier tbl "projection",
          Type.TyDirect (Type.TyNonScalar (Type.TyMat4 {value=(Type.TyDirect (Type.TyFloat loc)); loc}));
        ] in
      let attributes = [
        IdentifierTable.add_identifier tbl "vertex_position",
        Type.TyDirect (Type.TyNonScalar (Type.TyVec3 {value=(Type.TyDirect (Type.TyFloat loc)); loc}));
        IdentifierTable.add_identifier tbl "vertex_normals",
        Type.TyDirect (Type.TyNonScalar (Type.TyVec3 {value=(Type.TyDirect (Type.TyFloat loc)); loc}))
      ] in
      let object_mapping =
        make_mapping ~loc
          ~uniforms
          ~attributes () in
      let ty_var = fresh ~loc () in

      let constraints =
        `Eq (from_variable ty_var,
             from_scene_type @@
             Type.TyMulti (Type.TyDirect (Type.TyLight
                                            (TyDirect object_mapping, loc)))
            ) :: [] in
      `Left (LightData {light_tag; typ=ty_var}), constraints
    | S.ObjectData data ->
      let data, constraints = annotate_data tbl src env data in
      `Left (ObjectData data), constraints
    | S.CompoundObject {value=exprs;loc} ->
      let constraints, variables =
        map_ne_list exprs ~f:(fun obj_expr ->
            annotate_obj_expr tbl src env obj_expr
          ) |> unzip_ne_list in
      let exprs, constraints = unzip_ne_list constraints in
      let variables, constraints = concat_nene_list variables, concat_ne_list constraints in
      let ty_var = fresh ~loc () in
      let constraints =
        `Eq (from_variable ty_var,
             from_scene_type @@ Type.TyCompoundObjectFrom
               (map_ne_list variables ~f:(fun var -> (Type.TyVariable var)))) ::
        constraints in
      `Left (CompoundObject (exprs, ty_var)), constraints
  and annotate_obj tbl src env (S.Object (modifiers, base_obj, loc): S.obj) =
    let fresh ~loc = TypeVariable.gen_new_type ~loc src in
    let base_obj, c1 = annotate_base_object tbl src env base_obj in
    let modifiers, constraints = List.map modifiers ~f:(fun modifier ->
        annotate_modifier tbl src env modifier
      ) |> List.unzip in
    let modifiers, base_obj, ty_var = match base_obj with
      | `Right (Object (other_modifiers, base_obj, _ty_var)) ->
        modifiers @ other_modifiers, base_obj, fresh ~loc () (* ty_var *)
      | `Left base_obj ->  modifiers, base_obj, fresh ~loc () in
    let modifier_types = List.map modifiers ~f:(fun v -> AstUtils.type_modifier v) in
    let base_obj_type = AstUtils.type_base_object base_obj in
    let modifier_types = List.map modifier_types ~f:(fun ty -> Type.TyVariable ty) in
    let constraints =
      `Eq (
        from_variable @@ ty_var,
        from_scene_type @@ Type.TyObjectFrom (modifier_types, Type.TyVariable base_obj_type)
      ) ::
      c1 @ List.concat constraints in
    Object (modifiers, base_obj, ty_var), constraints
  and annotate_obj_expr tbl src env v =
    let fresh ~loc = TypeVariable.gen_new_type ~loc src in
    match v with
    | S.PlainObject obj ->
      let (obj, constraints) = annotate_obj tbl src env obj in
      let obj_typ = AstUtils.type_obj obj in
      (PlainObject obj,constraints), (obj_typ, [])
    | S.ObjectIfThenElse (cond, e1, e2) ->
      let cloc = S.expr_to_loc cond in
      let cond, c1 = annotate_expr src env cond  in
      let (e1, c2), v1 = annotate_obj_expr tbl src env e1  in
      let (e2,c3), v2 = match e2 with | None -> (None, []),[]
                                      | Some e2 -> let ((e2, c3), v2) = annotate_obj_expr tbl src env e2 in
                                        (Some e2, c3), ne_list_to_list v2 in
      let ty_var = fresh ~loc:cloc () in
      let constraints =
        `Eq (
          from_variable (AstUtils.type_expr cond),
          from_scalar (Type.TyBool cloc)
        ) ::
        match e2 with
        | None ->
          (* ty var will be a "if wrapper" around the type of e1 *)
          `Eq (
            from_variable ty_var,
            from_scene_type @@ Type.TyIfScene (Type.TyVariable (AstUtils.type_obj_expr e1))
          ) ::
          c1 @ c2 @ c3
        | Some e2 ->
          (* ty var will be the same type as e1 and e2 *)
          `Eq (
            from_variable (AstUtils.type_obj_expr e1),
            from_variable ty_var
          ) ::
          `Eq (
            from_variable (AstUtils.type_obj_expr e1),
            from_variable (AstUtils.type_obj_expr e2)
          ) ::
          c1 @ c2 @ c3
      in
      (ObjectIfThenElse (cond, e1, e2, ty_var), constraints), (ty_var, ne_list_to_list v1 @ v2)
    | S.ObjectFor { value=({ value=iter_var; loc=id_loc; }, start_exp, end_exp, exprs); loc } ->
      
      let start_exp,c1 = annotate_expr src env start_exp in
      let elem_type = fresh ~loc:id_loc () in

      let exprs, vars  = map_ne_list exprs ~f:(fun obj_expr ->
          with_local_variables ~value_mapping:[ iter_var, elem_type ] env
            ~f:(fun env -> annotate_obj_expr tbl src env obj_expr)
        ) |> unzip_ne_list in
      let vars = concat_nene_list vars in
      let exprs, constraints = unzip_ne_list exprs in
      let constraints = concat_ne_list constraints in
      match end_exp with
      | None ->
        let ty_var = fresh ~loc () in
        let constraints =
          (* start expression should be a list of elements of type elem type *)
         `Indexed (from_variable elem_type, from_variable (AstUtils.type_expr start_exp)) ::
          (* `Eq (
           *   from_variable (AstUtils.type_expr start_exp),
           *   Type.TypeExpr (Type.TyDirect
           *                    (Type.TyList (Type.TyVariable elem_type)))
           * ) :: *)
          (* type of for loop will be composed of the types within it  *)
          `Eq (
            from_variable ty_var,
            from_scene_type @@
            Type.TyCompoundObjectFrom (map_ne_list ~f:(fun var -> Type.TyVariable var) vars )
          ) ::
          c1 @ constraints in
        (ObjectFor  ((iter_var, start_exp, None,  ne_list_to_list exprs), ty_var), constraints), vars
      | Some end_exp ->
        let end_exp,c2 = annotate_expr src env end_exp in
        let ty_var = fresh ~loc () in

        let constraints =
          (* start expression should be a subtype of type int *)
          `Eq (
            from_variable (AstUtils.type_expr start_exp),
            from_scalar (Type.TyInt loc)
          ) ::
          (* end expression should be a subtype of type int *)
          `Eq (
            from_variable (AstUtils.type_expr end_exp),
            from_scalar (Type.TyInt loc)
          ) ::
          (* elem expression should also be a subtype of type int *)
          `Eq (
            from_variable elem_type,
            from_scalar (Type.TyInt loc)
          ) ::
          (* type of for loop will be composed of the types within it  *)
          `Eq (
            from_variable ty_var,
            from_scene_type @@
            Type.TyCompoundObjectFrom (map_ne_list ~f:(fun var -> Type.TyVariable var) vars) 
          ) ::
          c1 @ c2 @ constraints in
        (ObjectFor  ((iter_var, start_exp, None,  ne_list_to_list exprs), ty_var), constraints), vars
  and annotate_shader tbl src env (shader: S.shader) =
    let fresh ~loc = TypeVariable.gen_new_type ~loc src in
    match shader with
    | S.ShaderReference reference ->
      (match reference with
       | S.DirectReference { value=identifier; loc } ->
         let ty_var = macro_variable_lookup src loc identifier env in
         let constraints =
           `Eq (
             from_variable ty_var,
             from_scene_type @@
             Type.TyShader (Type.TyVariable (fresh ~loc ()), Type.TyVariable (fresh ~loc ()), loc)
           )
           :: [] in
         let value = ShaderReference (DirectReference (identifier, ty_var)) in
         let value = expand_shader ~loc env.global_variable_map value in
         value, constraints
       | S.IndirectReference ({ value=fname; loc }, S.MacroArguments
                                { keyword_args; positional_args }) ->
         let global_type_map = gen_global_type_map env in

         let function_specification = lookup_macro_function ~loc env fname in
         let result =
           annotate_function_application
             ~loc
             ~function_specification
             ~keyword_args
             ~positional_args
             ~annotate_value:(fun sarg -> annotate_scene_arg tbl src env sarg)
             ~f_type_value:(AstUtils.type_scene_arg)
             ~f_to_expander:(fun e -> Expander.of_scene_arg e)
             ~global_type_map
             ~global_variable_map:env.global_variable_map
             ~clone_body:(clone_scene_arg ~loc src)
             ~expand_body:(expand_scene_arg ~loc)
             ~gen_var:(fresh ~loc) in
         match result with
         | `Left (e1, constraints, typ) -> begin
             let constraints =
               `Eq (
                 from_variable typ,
                 from_scene_type @@
                 Type.TyShader (Type.TyVariable (fresh ~loc ()), Type.TyVariable (fresh ~loc ()), loc)
               ) ::
               constraints
             in
             match e1 with
             | ArgReference reference -> ShaderReference reference, constraints
             | SceneInlineExpression (SceneExprShader modif) -> modif, constraints
             | v -> raise AstExpander.(ConversionException (FunctionExpansionError (InvalidShader (fname, Expander.of_scene_arg v), loc)))
           end
         | `Right (keyword_args, positional_args, constraints, typ) ->  
           let constraints =
             `Eq (
               from_variable typ,
               from_scene_type @@
               Type.TyShader (Type.TyVariable (fresh ~loc ()), Type.TyVariable (fresh ~loc ()), loc)
             ) ::
             constraints
           in
           ShaderReference (
             IndirectReference (fname, MacroArguments { keyword_args; positional_args}, typ)
           ), constraints
      )
    | S.Shader { vertex_shader={ value=vertex_shader; loc }; fragment_shader; geometry_shader } ->
      let vertex_shader_file = ShaderAnalyzer.Filename vertex_shader in
      let fragment_shader_file, fragment_shader = match fragment_shader with
        | None -> ShaderAnalyzer.RawSource default_fragment_shader, Src default_fragment_shader
        | Some { value; _ } -> ShaderAnalyzer.Filename value, Filename value in
      let geometry_shader_file, geometry_shader = match geometry_shader with
        | None -> None, None
        | Some { value; _ } -> Some (ShaderAnalyzer.Filename value), Some value in
      let shader_spec =
        try
          ShaderAnalyzer.calculate_shader_specification ~loc
            ~vertex_shader_file ~fragment_shader_file ?geometry_shader_file ()
        with
        | ShaderAnalyzer.AnalysisException err ->
          raise (ConversionException (AnalysisError (err, loc))) in
      let shader_type =
        try
          ShaderAnalyzer.spec_to_type ~loc tbl (fresh ~loc) shader_spec
        with
        | ShaderAnalyzer.AnalysisException err ->
          raise (ConversionException (AnalysisError (err, loc))) in
      let ty_var = fresh ~loc () in
      let shader_expr = Shader {
          vertex_shader; fragment_shader; geometry_shader;
          shader_spec;
          typ=ty_var;
        } in
      let constraints = [
        `Eq (
          from_variable ty_var,
          from_scene_type @@ shader_type
        )
      ] in
      shader_expr, constraints
  and annotate_drawable tbl src env (drawable: S.drawable) =
    let fresh ~loc = TypeVariable.gen_new_type ~loc src in
    match drawable with
    | S.OverlayingDrawable { value=drawable; loc } ->
      let ty_var = fresh ~loc () in
      let drawable, c1 = annotate_drawable tbl src env drawable in
      let constraints = 
        `Eq (from_variable @@ (AstUtils.type_drawable drawable),
             from_scene_type @@ Type.TyMulti (Type.TyVariable ty_var)) ::
        c1
      in
      OverlayingDrawable (drawable, ty_var), constraints
    | S.DrawableShader { value=(shader, obj); loc } ->
      let shader, c1 = annotate_shader tbl src env shader  in
      let obj, c2 = annotate_obj tbl src env obj  in
      let ty_var = fresh ~loc () in
      let constraints =
        `Eq (from_variable ty_var,
             from_scene_type @@ Type.TyDrawableFrom (
               Type.TyVariable (AstUtils.type_shader shader),
               Type.TyVariable (AstUtils.type_obj obj)
             )
            ) ::
        c1 @ c2
      in
      DrawableShader (shader, obj, ty_var), constraints
    | S.DrawableReference reference ->
      (match reference with
       | S.DirectReference { value=identifier; loc } ->
         let ty_var = macro_variable_lookup src loc identifier env in
         let constraints =
           `Eq (
             from_variable ty_var,
             from_scene_type @@
             Type.TyDrawable (
               Type.TyVariable (fresh ~loc ()),
               Type.TyVariable (fresh ~loc ()), loc)
           )
           :: [] in
         let value = DrawableReference (DirectReference (identifier, ty_var)) in
         let value = expand_drawable ~loc env.global_variable_map value in
         value, constraints
       | S.IndirectReference ({ value=fname; loc }, S.MacroArguments
                                { keyword_args; positional_args }) ->
         let global_type_map = gen_global_type_map env in
         let function_specification = lookup_macro_function ~loc env fname in
         let result =
           annotate_function_application
             ~loc
             ~function_specification
             ~keyword_args
             ~positional_args
             ~annotate_value:(fun sarg -> annotate_scene_arg tbl src env sarg)
             ~f_type_value:(AstUtils.type_scene_arg)
             ~f_to_expander:(fun e -> Expander.of_scene_arg e)
             ~global_type_map
             ~global_variable_map:env.global_variable_map
             ~clone_body:(clone_scene_arg ~loc src)
             ~expand_body:(expand_scene_arg ~loc)
             ~gen_var:(fresh ~loc) in
         match result with
         | `Left (e1, constraints, typ) -> begin
             let constraints =
               `Eq (
                 from_variable typ,
                 from_scene_type @@
                 Type.TyDrawable (Type.TyVariable (fresh ~loc ()), Type.TyVariable (fresh ~loc ()), loc)
               ) ::
               constraints
             in
             match e1 with
             | ArgReference reference -> DrawableReference reference, constraints
             | SceneInlineExpression (SceneExprDrawable modif) -> modif, constraints
             | v -> raise AstExpander.(ConversionException (FunctionExpansionError (InvalidDrawable (fname, Expander.of_scene_arg v), loc)))
           end
         | `Right (keyword_args, positional_args, constraints, typ) ->  
           let constraints =
             `Eq (
               from_variable typ,
               from_scene_type @@
               Type.TyDrawable (
                 Type.TyVariable (fresh ~loc ()),
                 Type.TyVariable (fresh ~loc ()), loc)
             ) ::
             constraints
           in
           DrawableReference (
             IndirectReference (fname, MacroArguments { keyword_args; positional_args}, typ)
           ), constraints
      )
  and annotate_scene_expr tbl src env (scene_expr: S.scene_expr) = match scene_expr with
    | S.SceneExprObject obj ->
      let obj,c1 = annotate_obj tbl src env obj in
      SceneExprObject obj, c1
    | S.SceneExprShader shader ->
      let shader, c1 = annotate_shader tbl src env shader in
      SceneExprShader shader, c1
    | S.SceneExprModifier modifier ->
      let modifier, c1 = annotate_modifier tbl src env modifier in
      SceneExprModifier modifier, c1
    | S.SceneExprDrawable drawable ->
      let drawable,c1 = annotate_drawable tbl src env drawable in
      SceneExprDrawable drawable, c1
    | S.SceneExprTyped (sexpr, type_expr) ->
      let ty, env = of_type src env type_expr in
      (* we type the expression using updated environment *)
      let sexpr, c1 = annotate_scene_expr tbl src env sexpr in
      (* introduce a new constraint that the type of the expression must be a subtype of the user's type *)
      let ty_var = AstUtils.type_scene_expr sexpr in
      let constraints =
        `Eq (from_variable ty_var, ty) ::
        (* type of resulting list is a subtype of list instantiated with element type *)
        c1 in
      sexpr, constraints
  and annotate_declaration tbl src env (decl: S.declaration) =
    let fresh ~loc = TypeVariable.gen_new_type ~loc src in
    match decl with
    | S.DataFunction {
        identifier={ value=identifier; _ };
        keyword_arguments;
        positional_arguments;
        typ;
        body } ->
      let env, optional_arguments, optional_ty_vars, optional_ty_vars_map, c1 =
        let env, ls =
          keyword_arguments
          |> IDMap.to_alist
          |> List.fold_map ~init:env ~f:(fun env (id, (oty, expr)) ->
              let expr, c1 = annotate_expr src env expr in
              let ty_var = AstUtils.type_expr expr in
              let constraints, env =
                match oty with
                | None -> c1, env
                | Some ty ->
                  let ty, env = of_type src env ty in
                  `Eq (from_variable ty_var, ty) :: c1, env in
              env, ((id, expr), ((id, ty_var), constraints))
            ) in
        let alist, ty_var_constraints = List.unzip ls  in
        let ty_vars, constraints = List.unzip ty_var_constraints in
        let keyword_arguments = IDMap.of_alist_exn alist in
        let ty_vars_map = IDMap.of_alist_exn ty_vars in
        env, keyword_arguments, ty_vars, ty_vars_map, List.concat constraints
      in
      let env, positional_arguments, c2 =
        let env, ls = positional_arguments
                      |> List.fold_map ~init:env ~f:(fun env (Location.{ value=id; loc }, oty) ->
                          let ty_var = fresh ~loc () in
                          let constraints, env =
                            match oty with
                            | None -> [], env
                            | Some ty ->
                              let ty, env = of_type src env ty in
                              `Eq (from_variable ty_var, ty) :: [], env in
                          env, ((id, ty_var), constraints)
                        ) in
        let positional_arguments, constraints = List.unzip ls in
        env, positional_arguments, List.concat constraints
      in
      let env, c3 = match typ with
        | None -> env, (fun _ -> [])
        | Some ty ->
          let ty, env = of_type src env ty in
          env, (fun ty_var -> [`Eq (from_variable ty_var, ty)])
      in
      let body, c4 =
        with_local_variables env
          ~value_mapping:(optional_ty_vars @ positional_arguments)
          ~f:(fun env -> annotate_expr src env body)
      in
      let ty_var = AstUtils.type_expr body in
      let constraints = c1 @ c2 @ (c3 ty_var) @ c4 in
      let optional_arguments_expr =  optional_arguments in
      let function_specification : _ function_specification = {
        optional_arguments=optional_ty_vars_map;
        positional_arguments;
        optional_arguments_expr;
        typ=ty_var;
        constraints;
        body=Some body;
      } in
      (* record data variable into env *)
      add_function env identifier function_specification;
      (* return *)
      []
    | S.DataVariable { identifier=Location.{ value=identifier; _ }; typ; body } ->
      let env, c1 = match typ with
        | None -> env, (fun _ -> [])
        | Some ty ->
          let ty, env = of_type src env ty in
          env, (fun ty_var -> [`Eq (from_variable ty_var, ty)]) in
      let body, c2 = annotate_expr src env body in
      let ty_var = AstUtils.type_expr body in
      let constraints = (c1 ty_var) @ c2 in
      (* record data variable into env *)
      add_variable env identifier ty_var Expander.(ArgExpression body);
      constraints
    | S.MacroFunction {
        identifier=Location.{ value=identifier; _ };
        keyword_arguments; positional_arguments; typ; body
      } ->
      let env, optional_arguments, optional_ty_vars, optional_ty_vars_map, c1 =
        let env, ls =
          keyword_arguments
          |> IDMap.to_alist
          |> List.fold_map ~init:env ~f:(fun env (id, (oty, expr)) ->
              let expr, c1 = annotate_scene_expr tbl src env expr in
              let ty_var = AstUtils.type_scene_expr expr in
              let constraints, env =
                match oty with
                | None -> c1, env
                | Some ty ->
                  let ty, env = of_type src env ty in
                  `Eq (from_variable ty_var, ty) :: c1, env in
              env, ((id, expr), ((id, ty_var), constraints))
            ) in
        let alist, ty_var_constraints = List.unzip ls  in
        let ty_vars, constraints = List.unzip ty_var_constraints in
        let keyword_arguments = IDMap.of_alist_exn alist in
        let ty_vars_map = IDMap.of_alist_exn ty_vars in
        env, keyword_arguments, ty_vars, ty_vars_map, List.concat constraints in
      let env, positional_arguments, c2 =
        let env, ls = positional_arguments
                      |> List.fold_map ~init:env ~f:(fun env (Location.{ value=id; loc }, oty) ->
                          let ty_var = fresh ~loc () in
                          let constraints, env =
                            match oty with
                            | None -> [], env
                            | Some ty ->
                              let ty, env = of_type src env ty in
                              `Eq (from_variable ty_var, ty) :: [], env in
                          env, ((id, ty_var), constraints)
                        ) in
        let positional_arguments, constraints = List.unzip ls in
        env, positional_arguments, List.concat constraints
      in
      let env, c3 = match typ with
        | None -> env, (fun _ -> [])
        | Some ty ->
          let ty, env = of_type src env ty in
          env, (fun ty_var -> [`Eq (from_variable ty_var, ty)])
      in
      let body, c4 =
        with_local_variables env
          ~value_mapping:(optional_ty_vars @ positional_arguments)
          ~f:(fun env -> annotate_scene_expr tbl src env body)
      in
      let ty_var = AstUtils.type_scene_expr body in
      let constraints = c1 @ c2 @ (c3 ty_var) @ c4 in
      let function_specification : _ function_specification = {
        optional_arguments=optional_ty_vars_map;
        optional_arguments_expr=IDMap.map ~f:(fun v -> SceneInlineExpression v) optional_arguments;
        positional_arguments=positional_arguments;

        typ=ty_var;
        body=Some (SceneInlineExpression body);
        constraints;
      } in
      (* record data variable into env *)
      add_macro_function env identifier function_specification;
      (* return *)
      []
    | S.MacroVariable { identifier={ value=identifier; _ }; typ; body } ->
      let env, c1 = match typ with
        | None -> env, (fun _ -> [])
        | Some ty ->
          let ty, env = of_type src env ty in
          env, (fun ty_var -> [`Eq (from_variable ty_var, ty)]) in
      let body, c2 = annotate_scene_expr tbl src env body in
      let ty_var = AstUtils.type_scene_expr body in
      let constraints = (c1 ty_var) @ c2 in
      (* record data variable into env *)
      add_macro_variable env identifier ty_var (Expander.of_scene_expr body);
      constraints
  and annotate_statement tbl src env (statement: S.statement) = match statement with
    | S.Declaration decl ->
      let c1 = annotate_declaration tbl src env decl in
      None, c1
    | S.Drawable drawable ->
      let drawable, c1 = annotate_drawable tbl src env drawable in
      Some (Drawable drawable), c1
    | S.LayeredDrawable (layer, drawable) ->
      let drawable, c1 = annotate_drawable tbl src env drawable in
      Some (LayeredDrawable (layer, drawable)), c1
  and annotate_program tbl src env (prog: S.statement list)  =
    let statements, constraints =
      List.map prog
        ~f:(fun statement -> annotate_statement tbl src env statement)
      |> List.unzip in
    List.filter_opt statements, List.concat constraints @ env.default_constraints

end

module Unification = struct


  type unification_error =
      InternalMismatch of Type.internal_type_expr * Type.internal_type_expr
    | PureDataMismatch of Type.pure_data_type * Type.pure_data_type
    | ScalarMismatch of Type.scalar_type * Type.scalar_type
    | NonScalarMismatch of Type.non_scalar_type * Type.non_scalar_type
    | TextureMismatch of Type.texture_type * Type.texture_type
    | SceneMismatch of Type.scene_type * Type.scene_type
    | OutputMismatch of int * int * Location.t * Location.t
    | MissingKey of IdentifierTable.m * Type.t
  [@@deriving show]

  exception UnificationException of unification_error

  let () = Stdlib.Printexc.register_printer (function
        UnificationException v ->
        Some (show_unification_error v)
      | _ -> None
    )


  module Map = TypeVariable.Map

  let cannonicalize_constraints map (const: Type.constr) : Substitution.variable_type Map.t * Type.constr =
    (match const with
     | `Eq (Type.TypeExpr (Type.TyVariable var), texp) ->
       let var = match Map.find map var with Some Substitution.Variable var -> var | _ -> var  in
       let texp = Substitution.substitute_type_expr map texp in
       map, (`Eq (Type.TypeExpr (Type.TyVariable var), texp))
     | `Eq (t1, t2) ->
       let t1 = Substitution.substitute_type_expr map t1 in
       let t2 = Substitution.substitute_type_expr map t2 in
       map, `Eq (t1, t2)
     | `Indexed (t1, t2) ->
       let t1 = Substitution.substitute_type_expr map t1 in
       let t2 = Substitution.substitute_type_expr map t2 in
       map, `Indexed (t1, t2)
     | `IntEq (t1, n) ->
       let t1 = match TypeVariable.Map.find map t1 with  Some Substitution.Variable var -> var | _ -> t1 in
       map, `IntEq (t1, n)
     | `MappingEq (t1, n) ->
       let open Type in
       let t1 = match TypeVariable.Map.find map t1 with  Some Substitution.Variable var -> var | _ -> t1 in
       let n = match n with Specialized n -> Specialized (Substitution.substitute_mapping map n) | Any -> Any in
       map, `MappingEq (t1, n)
     (* | `SubType (t1, t2) ->
      *   let t1 = Substitution.substitute_type_expr map t1 in
      *   map, `SubType (t1, t2) *)
    )


  let instantiate map (const: Type.constr) : _ * (Type.constr option * Type.constr option) =
    (match const with
     | `Eq (Type.TypeExpr (Type.TyVariable var), texp) ->
       let texp = Substitution.substitute_type_expr map texp in
       let constr = `Eq (Type.TypeExpr (Type.TyVariable var), texp) in
       begin match Map.mem map var with
         | false ->
           let map = Map.set map ~key:var ~data:(Substitution.of_type texp) in
           map, (Some constr, None)
         | true ->
           map, (None, Some constr)
       end
     | `MappingEq (var, texp) ->
       let constr, texp = match texp with
           Any -> `MappingEq (var, texp), Substitution.VAny
         | Specialized texp ->
           let texp = (Substitution.substitute_mapping map texp) in
           `MappingEq (var, Specialized texp), Substitution.Mapping texp  in
       Map.set map ~key:var ~data:texp, (Some constr, None)
     | `IntEq (var, n) ->
       let texp = Substitution.Int n in
       Map.set map ~key:var ~data:texp, (Some (`IntEq (var,n)), None)
     | `Indexed (t1, texp) ->
       let t1 = Substitution.substitute_type_expr map t1 in
       let texp = Substitution.substitute_type_expr map texp in
       let constr = `Indexed (t1, texp) in
       map, (None, Some constr)
     | _ -> map, (None, Some const))

  let split_constraints constraints =
    List.map constraints ~f:(fun (constr: Type.constr) ->
        match constr with
        | `Eq (
            Type.TypeExpr (Type.TyVariable v1),
            Type.TypeExpr (Type.TyVariable v2)) -> Some (v1,v2), None
        | _ -> None, Some constr
      ) |> List.unzip |> (fun (a,b) -> List.filter_opt a, List.filter_opt b)


  let unify_or_variable t f (a: 'a Type.or_variable) (b: 'a Type.or_variable) : Type.constr list option =
    let open Type in
    match a,b with
    | TyDirect v1, TyDirect v2 -> f v1 v2
    | TyDirect o, TyVariable v
    | TyVariable v, TyDirect o ->
      Some [`Eq (TypeExpr (TyVariable v), t o)]
    | TyVariable v1, TyVariable v2 ->
      Some [`Eq (TypeExpr (TyVariable v1), TypeExpr (TyVariable v2))]      




  let rec unify_constraint map (constraints: Type.constr)  =
    let sub = Substitution.substitute_type_expr map in
    match constraints with
    | `Eq ((Type.TypeExpr (TyVariable var) as t1), t2) ->
      begin match unify_eq_constr ~var (sub t1) (sub t2) with
        | None -> [constraints]
        | Some ls -> ls
      end
    | `Eq (t1, t2) ->
      begin match unify_eq_constr (sub t1) (sub t2) with
        | None -> [constraints]
        | Some ls -> ls
      end

    (* | `Indexed _ -> (??)
     * | `SubType _ -> (??) *)
    | v -> [v]
  and unify_eq_constr ?var (t1: Type.type_expr) (t2: Type.type_expr) =
    let open Type in
    match t1,t2 with
    | (TypeExpr TyDirect v1, TypeExpr TyDirect v2) ->
      unify_internal ?var v1 v2
    | _ -> None
  and unify_internal ?var v1 v2 =
    let open Type in
    match v1,v2 with
    (* pure data *)
    | (TyPure pdt1, TyPure pdt2) -> unify_pure_data ?var pdt1 pdt2
    (* texture data *)
    | (TyTexture t1, TyTexture t2) -> unify_texture ?var t1 t2
    (* scene data *)
    | (TyScene st1, TyScene st2) -> unify_scene ?var st1 st2
    (* list data *)
    | TyList it1, TyList it2 ->
      unify_or_variable
        (fun v -> TypeExpr (TyDirect v))
        (unify_internal ?var)
        it1 it2
    | (_, _) -> raise (UnificationException (InternalMismatch (v1,v2)))
  and unify_pure_data ?var pdt1 pdt2 =
    let open Type in
    match pdt1, pdt2, var with
    | TyScalar sc1, TyScalar sc2, _ -> unify_scalar ?var sc1 sc2
    | TyNonScalar ns1, TyNonScalar ns2,_ -> unify_non_scalar ?var ns1 ns2
    | TyNonScalar _, TyNumericData _,_ 
    | TyScalar _, TyNumericData _,_ 
    | TyNumericData _, TyNumericData _,_ -> Some []

    | TyNumericData _, TyNonScalar sc, Some var -> Some [`Eq (
        TypeExpr (TyVariable var),
        TypeExpr (TyDirect (TyPure (TyNonScalar sc)))
      )]
    | TyNumericData _, TyScalar sc, Some var -> Some [`Eq (
        TypeExpr (TyVariable var),
        TypeExpr (TyDirect (TyPure (TyScalar sc)))
      )]
    | TyNumericData _, TyNonScalar _, _ -> None
    | TyNumericData _, TyScalar _, _ -> None

  
    | _ -> raise (UnificationException (PureDataMismatch (pdt1,pdt2)))
  and unify_scalar ?var sc1 sc2 =
    let open Type in
    match sc1, sc2,var with
    | TyBool _, TyBool _,_
    | TyInt _, TyInt _,_
    | TyFloat _, TyFloat _,_
    (* | (TyIntOrFloat _, TyInt _)
     * | (TyIntOrFloat _, TyFloat _) *)
    | TyInt _, TyIntOrFloat _,_
    | TyFloat _, TyIntOrFloat _,_
    | TyIntOrFloat _, TyIntOrFloat _,_ -> Some []
    | TyIntOrFloat _, TyInt loc, Some var ->
      Some [`Eq (
        TypeExpr (TyVariable var),
        TypeExpr (TyDirect (TyPure (TyScalar (TyInt loc))))
      )]      
    | TyIntOrFloat _, TyFloat loc, Some var ->
      Some [`Eq (
        TypeExpr (TyVariable var),
        TypeExpr (TyDirect (TyPure (TyScalar (TyFloat loc))))
      )]      
    | TyInt _, TyFloat loc, Some var ->
      Some [`Eq (
        TypeExpr (TyVariable var),
        TypeExpr (TyDirect (TyPure (TyScalar (TyFloat loc))))
      )]      

    | _ -> raise (UnificationException (ScalarMismatch (sc1, sc2)))
  and unify_non_scalar ?var ns1 ns2 =
    let open Type in
    let unify_internal_scalar vs1 vs2 = begin match vs1, vs2 with
        | (TyDirect s1, TyDirect s2) -> unify_scalar ?var s1 s2
        | (TyDirect s, TyVariable v) 
        | (TyVariable v, TyDirect s) ->
          Some [`Eq (TypeExpr (TyVariable v), TypeExpr (TyDirect (TyPure (TyScalar s))) )]
        | (TyVariable v1, TyVariable v2) ->
          Some [`Eq (TypeExpr (TyVariable v1), TypeExpr (TyVariable v2))]
      end in
    match ns1, ns2 with
    | (TyVec2 {value=vs1; _}, TyVec2 {value=vs2; _})
    | (TyVec3 {value=vs1; _}, TyVec3 {value=vs2; _})
    | (TyVec4 {value=vs1; _}, TyVec4 {value=vs2; _})
    | (TyMat2 {value=vs1; _}, TyMat2 {value=vs2; _})
    | (TyMat3 {value=vs1; _}, TyMat3 {value=vs2; _})
    | (TyMat4 {value=vs1; _}, TyMat4 {value=vs2; _}) -> unify_internal_scalar vs1 vs2
    | _ -> raise (UnificationException (NonScalarMismatch (ns1, ns2)))
  and unify_texture ?var t1 t2 =
    let open Type in
    match t1,t2,var with
    | TyTexture1D _, TyTexture1D _, _
    | TyTexture2D _, TyTexture2D _, _
    | TyTexture3D _, TyTexture3D _, _
    | TyTextureCube _, TyTextureCube _, _
    | _, TyAnyTexture _, _ -> Some []

    | TyAnyTexture _, tt, Some var -> Some [`Eq (
        TypeExpr (TyVariable var),
        TypeExpr (TyDirect (TyTexture tt))
      )]
    | TyAnyTexture _, _, _ -> None
    | _ -> raise (UnificationException (TextureMismatch (t1, t2)))
  and unify_scene ?var s1 s2 =
    let open Type in
    let union_results result1 result2 =
      begin match result1, result2 with
        | Some result1, Some result2 ->
          Some (result1 @ result2)
        | Some result1, None
        | None, Some result1 ->
          Some (result1)
        | None, None ->
          None
      end in
    let map_variable f (v: 'a or_variable) = match v with
      | TyVariable v -> TyVariable v
      | TyDirect v -> TyDirect (f v) in
    match s1,s2 with
    | (TyObject (a_m1, a_m2, _), TyObject (b_m1, b_m2, _)) ->
      let result1 = unify_mapping_or_variable var a_m1 b_m1 in
      let result2 = unify_mapping_or_variable var a_m2 b_m2 in
      union_results result1 result2

    | (TyCompoundObjectFrom _, TyMulti _) | (TyMulti _, TyCompoundObjectFrom _)
    | (TyObjectFrom (_, _), TyMulti _) | (TyMulti _, TyObjectFrom (_, _))
    | (TyObject (_, _, _), TyMulti _)
    (* | (TyMulti _, TyObject (_, _, _))    --  multi can not unify with object *)

    | (TyCompoundObjectFrom _, TyLight _)
    | (TyObjectFrom (_, _), TyLight _)
    | (TyLight _, TyCompoundObjectFrom _)
    | (TyLight _, TyObjectFrom (_, _))


    | (TyCompoundObjectFrom _, TyObjectFrom (_, _))
    | (TyObjectFrom (_, _), TyCompoundObjectFrom _)
    | (TyCompoundObjectFrom _, TyObject (_, _, _))
    | (TyObject (_, _, _), TyCompoundObjectFrom _)

    | (TyObject (_, _, _), TyObjectFrom (_, _))
    | (TyObjectFrom (_, _), TyObject (_, _, _)) ->
      None

    | (TyLight (TyDirect l1, _), TyObject (_, TyDirect l2, _)) -> unify_mapping_or_any var (Specialized l1) l2
    | (TyObject (_, TyDirect l1, _), TyLight (TyDirect l2, _))  -> unify_mapping_or_any var l1 (Specialized l2)

    | (TyLight _, TyObject (_, _, _)) 
    | (TyObject (_, _, _), TyLight _)  -> None

    | TyLight (l1,_), TyLight (l2,_) ->
      unify_mapping_or_variable  var
        (map_variable (fun v -> Specialized v) l1)
        (map_variable (fun v -> Specialized v) l2)

    | (TyIfScene v1, TyIfScene v2)
    | (TyMulti v1, TyMulti v2) ->
      unify_or_variable (fun v -> TypeExpr (TyDirect (TyScene v))) (unify_scene ?var) v1 v2

    | (TyObjectFrom (_a_d, a_l), TyObjectFrom (_b_d, b_l)) ->
      (* let result1 = unify_scene_type_list a_d b_d in *)
      let result2 = unify_or_variable (fun v -> TypeExpr (TyDirect (TyScene v))) (unify_scene ?var) a_l b_l in
      union_results None result2
    | (TyCompoundObjectFrom _t1, TyCompoundObjectFrom _t2) ->
      (* unify_scene_type_list t1 t2 *) None

    | (TyDrawable _, TyMulti _) | (TyDrawableFrom (_, _), TyMulti _) | (TyMulti _, TyDrawableFrom (_, _)) 
    | (TyDrawable (_, _, _), TyDrawableFrom (_, _)) 
    | (TyDrawableFrom (_, _), TyDrawable (_, _, _)) 
    | (TyDrawableFrom (_, _), TyDrawableFrom (_, _)) -> None  

    | (TyDrawable (m1, c1,l1), TyDrawable (m2, c2,l2)) ->
      let result1 = unify_mapping_or_variable var  m1 m2 in
      let result2 = unify_int_or_variable (c1,l1) (c2,l2) in
      union_results result1 result2

    | (TyUnresolvedModifier _, TyMulti _) | (TyModifier _, TyMulti _) 
    | (TyUnresolvedModifier _, TyModifier _)
    | (TyModifier _, TyUnresolvedModifier _) 
    | (TyMulti _, TyUnresolvedModifier _) -> None

    | (TyModifier (d1, l1, _), TyModifier (d2, l2, _)) ->
      let result1 = unify_mapping_or_variable var d1 d2 in
      let result2 = unify_mapping_or_variable var l1 l2 in
      union_results result1 result2

    | (TyUnresolvedModifier m1, TyUnresolvedModifier m2) ->
      let result1 = unify_mapping_or_variable var
          (map_variable (fun v -> Specialized v) m1)
          (map_variable (fun v -> Specialized v) m2) in
      result1

    | (TyShader (m1, c1, l1), TyShader (m2, c2, l2)) ->
      let result1 = unify_mapping_or_variable var
          (map_variable (fun v -> Specialized v) m1)
          (map_variable (fun v -> Specialized v) m2) in
      let result2 = unify_int_or_variable (c1,l1) (c2,l2) in
      union_results result1 result2

    | _ -> raise (UnificationException (SceneMismatch (s1, s2)))
  and unify_mapping_or_variable var
      (m1: Type.mapping Type.or_any Type.or_variable)
      (m2: Type.mapping Type.or_any Type.or_variable) =
    match m1,m2 with
    | (TyDirect m1, TyDirect m2) -> unify_mapping_or_any var m1 m2
    | (TyDirect m2, TyVariable m1)
    | (TyVariable m1, TyDirect m2) ->
      Some [`MappingEq (m1, m2)]
    | (TyVariable v1, TyVariable v2) ->
      Some [ `Eq (TypeExpr (TyVariable v1), TypeExpr (TyVariable v2))]
  and unify_mapping_or_any var (m1: Type.mapping Type.or_any) (m2: Type.mapping Type.or_any) =
    let open Type in
    match m1,m2 with
    | Any, Any -> Some []
    | Specialized _, Any -> Some []
    | Any, Specialized _ -> Some []
    | Specialized MkTyMapping Base (x1, y1, z1), Specialized MkTyMapping Base (x2, y2, z2) ->
      let pdt_to_constr = unify_or_variable (fun pdt -> TypeExpr (TyDirect (TyPure pdt))) (unify_pure_data ?var) in
      let pdt_to_t ty =  match ty with TyVariable var -> TypeExpr (TyVariable var)
                                     | TyDirect pdt -> TypeExpr (TyDirect (TyPure pdt)) in
      let tt_to_constr = unify_or_variable (fun pdt -> TypeExpr (TyDirect (TyTexture pdt))) (unify_texture ?var) in
      let tt_to_t ty =  match ty with TyVariable var -> TypeExpr (TyVariable var)
                                    | TyDirect pdt -> TypeExpr (TyDirect (TyTexture pdt)) in

      let unify t f x1 x2 = match x1,x2 with
        | Base TyDirect m1, Base TyDirect m2 ->
          let res = IDMap.merge m1 m2 ~f:(fun ~key v -> match v with
              | `Right ty ->
                let res = f ty in
                raise (UnificationException (MissingKey (key, res)))
              | `Left _ -> None
              | `Both (pdt1, pdt2) -> t pdt1 pdt2) in
          res |> IDMap.to_alist |> List.map ~f:(fun (_,b) -> b) |> List.concat |> (fun v -> Some v)
        | Base TyVariable v1, Base TyVariable v2 ->
          Some [`Eq (TypeExpr (TyVariable v1), TypeExpr (TyVariable v2))]
        | _, _ -> None
      in
      let x12 = unify pdt_to_constr pdt_to_t x1 x2 in
      let y12 = unify pdt_to_constr pdt_to_t y1 y2 in
      let z12 = unify tt_to_constr tt_to_t z1 z2 in
      begin match x12, y12, z12 with
        | Some v1, Some v2, Some v3 -> Some (v1 @ v2 @ v3)
        | None, Some v1, Some v2
        | Some v1, None, Some v2
        | Some v1, Some v2, None -> Some (v1 @ v2)
        | None, None, Some v1
        | None, Some v1, None
        | Some v1, None, None -> Some v1        
        | None, None, None -> None
      end
    | Specialized _, Specialized _ -> None


  and unify_int_or_variable (m1,loc1) (m2, loc2) =
    match m1,m2 with
    | TyDirect m1, TyDirect m2 ->
      if m1 > m2 then raise (UnificationException (OutputMismatch (m1, m2, loc1,loc2))) else Some []
    | TyVariable v1, TyDirect n
    | TyDirect n, TyVariable v1 -> Some [`IntEq (v1, n)]
    | TyVariable v1, TyVariable v2 -> Some [`Eq (TypeExpr (TyVariable v1), TypeExpr (TyVariable v2))]

  let normalize_type_equalities ?(map=Map.empty) (ls: (TypeVariable.t * TypeVariable.t) list) =
    let open TypeVariable in
    let base_types = List.fold ls ~init:map ~f:(fun map (a,b) ->
        match Map.mem map a, Map.mem map b with
        | true, true ->
          let s_a = Map.find_exn map a in
          let s_b = Map.find_exn map a in
          let s_ab = Set.union s_a s_b in
          Map.set map ~key:a ~data:s_ab |> (fun map -> Map.remove map b)
        | true, _ ->
          Map.update map a ~f:(fun v ->
              match v with None -> assert false | Some v ->
                Set.add v b
            )
        | _, true -> 
          Map.update map b ~f:(fun v ->
              match v with None -> assert false | Some v ->
                Set.add v a
            )
        | false, false ->
          let vl = Map.to_alist map |> List.find ~f:(fun (_, st) -> Set.mem st a || Set.mem st b) in
          match vl with
          | None -> Map.add_exn map ~key:a ~data:(Set.singleton b)
          | Some (key, st) -> Map.set map ~key ~data:(Set.add (Set.add st a) b)
      ) in
    Map.to_alist base_types |> List.concat_map ~f:(fun (key, st) -> Set.to_list st
                                                                    |> List.map ~f:(fun v -> (v,key)))
    |> Map.of_alist_exn, (Map.keys base_types |> Set.of_list)


  let solve_internal can_type constraints =
    let module C = CannonicalTypeMap in
    let rec loop constraints =
      let constraints =
        let type_equalities, constraints = split_constraints constraints in
        List.iter type_equalities ~f:(fun (t1,t2) -> ignore @@ C.set_equal can_type t1 t2);
        List.map ~f:(Type.to_cannonical_constr can_type) constraints in

      (* Printf.printf "constraints: [%s]\n" (Type.show_constr_list constraints); *)

      let map = Map.empty in
      let constraints = ConstraintSort.sort constraints in
      (*  first_pass  *)
      let map, constraints = List.fold_map ~init:map ~f:instantiate constraints in
      let equalities, constraints =
        List.unzip constraints |> fun (a,b) -> List.filter_opt a, List.filter_opt b in

      let equalities, new_constraints =
        let equalities = List.concat_map equalities ~f:(Normalize.normalize_constraint) in
        List.partition_tf equalities
          ~f:(fun v -> match v with
              | `Eq (TypeExpr TyDirect _, _) -> false
              | _ -> true) in

      (* Printf.printf "constraints: [%s]\n" (Type.show_constr_list constraints); *)
      let constraints =
        let new_constraints = List.concat_map constraints ~f:(Normalize.normalize_constraint) in
        new_constraints in
      let constraints = new_constraints @ constraints in

      let start_constraints = constraints in

      let constraints =
        let new_constraints = List.concat_map constraints ~f:(unify_constraint map) in
        new_constraints in

      let any_eq = List.equal Type.equal_constr constraints start_constraints in

      let constraints = equalities @ constraints in
      if not any_eq then loop constraints
      else constraints in

    let constraints = loop constraints in
    let constraints = ConstraintSort.sort constraints in
    let expr_map, _constraints = List.fold_map ~init:Map.empty ~f:instantiate constraints in

    expr_map


end

type type_error =
  | SortingFailed
  | TypeSubstitution of TypeVariable.t * Substitution.variable_type * string
  | ShaderRequirement of IdentifierTable.m * Type.t * Type.type_expr Type.typemap
  | InvalidShaderType of Type.scene_type 
  | InvalidDrawableType of Type.scene_type 
  | InvalidModifierType of Type.scene_type 
  | InvalidObjectType of Type.scene_type 
  | EmptyCompoundObject of Location.t
  | IndeterminateDrawable of Location.t
  | UnificationError of Unification.unification_error

exception TypingException of type_error

let solve can_type constraints =
  try Unification.solve_internal can_type constraints with
  | ConstraintSort.SortingException (ConstraintSort.Stalled _) -> raise (TypingException SortingFailed)
  | Substitution.SubstitutionException (IncompatibleTypes (ty, var, name)) -> raise (TypingException (TypeSubstitution (ty, var, name)))
  | Normalize.Unification.UnificationException (UnboundShaderRequirement (name, var, map, _)) ->
    raise (TypingException (ShaderRequirement (name, var, map)))
  | Normalize.NormalizeException (InvalidShaderType sc) ->
    raise (TypingException (InvalidShaderType sc))
  | Normalize.NormalizeException (InvalidDrawableType sc) ->
    raise (TypingException (InvalidDrawableType sc))
  | Normalize.NormalizeException (InvalidModifierType sc) ->
    raise (TypingException (InvalidModifierType sc))
  | Normalize.NormalizeException (InvalidObjectType sc) ->
    raise (TypingException (InvalidObjectType sc))
  | Normalize.NormalizeException (IfOnlyDrawable loc) ->
    raise (TypingException (IndeterminateDrawable loc))
  | Normalize.NormalizeException (EmptyCompoundObject loc) ->
    raise (TypingException (EmptyCompoundObject loc))

  | Unification.UnificationException (error) ->
    raise (TypingException (UnificationError error))



module Make (Output: Pretty_error.ErrorOut) = struct



  let type_check ?std_lib tbl (program) =
    let (let+) x f = Option.bind x ~f in
    Format.pp_set_max_indent Format.str_formatter 1000;
    let src = TypeVariable.init () in
    let env = OfRaw.init_env ?std_lib tbl src () in
    let+ statements, constraints = try
        Some (OfRaw.annotate_program tbl src env program)
      with OfRaw.ConversionException v -> begin
          let lookup_tbl = IdentifierTable.serialize tbl in
          (match v with
           | OfRaw.UnboundVariable (id, loc) ->
             Output.output_error loc "use of unbound variable %s" (IdentifierTable.lookup lookup_tbl id)
           | OfRaw.UnboundMacroVariable (id, loc) ->
             Output.output_error loc "use of unbound macro variable %s"
               (IdentifierTable.lookup lookup_tbl id)          
           | OfRaw.UnboundFunction (id, loc) ->
             Output.output_error loc "call to unbound function %s"
               (IdentifierTable.lookup lookup_tbl id)          
           | OfRaw.UnboundMacroFunction (id, loc) ->
             Output.output_error loc "call to unbound macro function %s"
               (IdentifierTable.lookup lookup_tbl id)          
           | OfRaw.InvalidModel (str, loc) ->
             Output.output_error loc "invalid model file %s" str
           | OfRaw.DuplicateMapping (id, loc) ->
             Output.output_error loc "use of duplicate mapping %s" (IdentifierTable.lookup lookup_tbl id)
           | OfRaw.MissingFile (str, loc) ->
             Output.output_error loc "unable to open file %s" str          
           | OfRaw.AnalysisError (analysis_error, loc) -> begin match analysis_error with
               | ShaderAnalyzer.FileNotFound str -> 
                 Output.output_error loc "unable to open file %s" str          
               | ShaderAnalyzer.UnsupportedShaderType str ->
                 Output.output_error loc "use of unsupported glsl variable type %s" str
               | ShaderAnalyzer.InvalidAttributeType (name, _) ->
                 Output.output_error loc "use of textures type as attribute parameter %s" name
               | ShaderAnalyzer.DuplicateAttributes id ->
                 Output.output_error loc "use of duplicate attribute %s in shader"
                   (IdentifierTable.lookup lookup_tbl id)
             end

           | OfRaw.UnsupportedKeywordArgument (id, loc) ->
             Output.output_error loc "use of unbound keyword %s in function call"
               (IdentifierTable.lookup lookup_tbl id)
           | OfRaw.TooManyArguments loc ->
             Output.output_error loc "too many positional arguments to function call"
           | OfRaw.TooFewArguments loc ->
             Output.output_error loc "too few positional arguments to function call"          
           | OfRaw.RepeatedPositionalArgument loc ->
             Output.output_error loc "repeated positional arguments to function call"
           | OfRaw.FunctionExpansionError (v, loc) ->
             let expand_var (var: TypeVariable.t Expander.argument) =
               let show f v = f ~print_type:false TypeVariable.pp Format.str_formatter v; Format.flush_str_formatter () in
               let ty, value = match var with
                 | Expander.ArgExpression expr -> "expression", show (fun ~print_type -> pp_expr ~print_type) expr
                 | Expander.ArgObject obj -> "object", show (fun ~print_type -> pp_obj ~print_type) obj
                 | Expander.ArgShader shader -> "shader", show (fun ~print_type -> pp_shader ~print_type) shader
                 | Expander.ArgModifier modifier -> "modifier", show (fun ~print_type -> pp_modifier ~print_type) modifier
                 | Expander.ArgDrawable drawable -> "drawable", show (fun ~print_type -> pp_drawable ~print_type) drawable
                 | Expander.ArgRef reference -> "ref", show (fun ~print_type -> pp_reference ~print_type) reference
               in

               let value =
                 if String.length value > 30 then (String.sub value ~pos:0 ~len:27) ^ "..." else value in
               let value = String.tr ~target:'\n' ~replacement:' ' value in
               Printf.sprintf "%s(%s)" ty value
             in
             (match v with
              | OfRaw.AstExpander.InvalidExpression (name, var) ->
                let name = IdentifierTable.to_string tbl name in
                let var = expand_var var in
                Output.output_error loc "attempted invalid expansion of variable %s as an expression (actual value: %s)" name var
              | OfRaw.AstExpander.InvalidObject (name, var) ->
                let name = IdentifierTable.to_string tbl name in
                let var = expand_var var in
                Output.output_error loc "attempted invalid expansion of variable %s as an object (actual value: %s)" name var                
              | OfRaw.AstExpander.InvalidModifier (name, var) ->
                let name = IdentifierTable.to_string tbl name in
                let var = expand_var var in
                Output.output_error loc "attempted invalid expansion of variable %s as an modifier (actual value: %s)" name var                
              | OfRaw.AstExpander.InvalidDrawable (name, var) ->
                let name = IdentifierTable.to_string tbl name in
                let var = expand_var var in
                Output.output_error loc "attempted invalid expansion of variable %s as an drawable (actual value: %s)" name var                
              | OfRaw.AstExpander.InvalidShader (name, var) ->
                let name = IdentifierTable.to_string tbl name in
                let var = expand_var var in
                Output.output_error loc "attempted invalid expansion of variable %s as an shader (actual value: %s)" name var
             )
          );
          None
        end in
    let can_type = CannonicalTypeMap.init () in
    let+ mapping = try Some (solve can_type constraints) with
      | TypingException v ->
        let lookup_tbl = IdentifierTable.serialize tbl in
        IdentifierTable.with_reference_table lookup_tbl (fun () ->
            let norm_str str =
              let str = if String.length str > 200 then String.sub str ~pos:0 ~len:197 ^ "..."  else str in
              let str = String.tr ~target:'\n' ~replacement:' ' str in
              str
            in

            let show (st:Type.scene_type) =
              let str = Type.pp_scene_type Format.str_formatter st; Format.flush_str_formatter () in
              let name = match st with
                | Type.TyObject (_, _, _) -> "object"
                | Type.TyObjectFrom (_, _) -> "object"
                | Type.TyCompoundObjectFrom _ -> "object"
                | Type.TyModifier (_, _, _) -> "modifier"
                | Type.TyUnresolvedModifier _ -> "modifier"
                | Type.TyLight (_, _) -> "object"
                | Type.TyShader (_, _, _) -> "shader"
                | Type.TyDrawableFrom (_, _) -> "drawable"
                | Type.TyDrawable (_, _, _) -> "drawable"
                | Type.TyMulti _ -> "multi scene_type"
                | Type.TyIfScene _ -> "optional scene_type" in
              let str = norm_str str in
              name, str
            in
            let print f v = f Format.str_formatter v; Format.flush_str_formatter () |> norm_str in

            (match v with
             | UnificationError error -> (match error with
                 | Unification.InternalMismatch (internal_type_expr_1, internal_type_expr_2) ->
                   let locs = Type.internal_type_loc src internal_type_expr_1 @
                              Type.internal_type_loc src internal_type_expr_2 in
                   let t1 = print Type.pp_internal_type_expr internal_type_expr_1 in
                   let t2 = print Type.pp_internal_type_expr internal_type_expr_2 in

                   List.iter locs ~f:(fun loc ->
                       Output.output_error loc "failed to unify type %s with %s" t1 t2
                     )
                 | Unification.PureDataMismatch (pure_data_type_1, pure_data_type_2) ->
                   let locs =
                     Type.pure_data_loc src pure_data_type_1 @
                     Type.pure_data_loc src pure_data_type_2
                   in
                   let t1 = print Type.pp_pure_data_type pure_data_type_1 in
                   let t2 = print Type.pp_pure_data_type pure_data_type_2 in

                   List.iter locs ~f:(fun loc ->
                       Output.output_error loc "failed to unify pure data type %s with %s" t1 t2
                     )
                 | Unification.ScalarMismatch (scalar_type_1, scalar_type_2) ->
                   let locs =
                     Type.scalar_loc src scalar_type_1 @
                     Type.scalar_loc src scalar_type_2
                   in
                   let t1 = print Type.pp_scalar_type scalar_type_1 in
                   let t2 = print Type.pp_scalar_type scalar_type_2 in

                   List.iter locs ~f:(fun loc ->
                       Output.output_error loc "failed to unify scalar type %s with %s" t1 t2
                     )
                 | Unification.NonScalarMismatch (non_scalar_type_1, non_scalar_type_2) ->
                   let locs =
                     Type.non_scalar_loc src non_scalar_type_1 @
                     Type.non_scalar_loc src non_scalar_type_2
                   in
                   let t1 = print Type.pp_non_scalar_type non_scalar_type_1 in
                   let t2 = print Type.pp_non_scalar_type non_scalar_type_2 in

                   List.iter locs ~f:(fun loc ->
                       Output.output_error loc "failed to unify non scalar type %s with %s" t1 t2
                     )
                 | Unification.TextureMismatch (texture_type_1, texture_type_2) ->
                   let locs =
                     Type.texture_loc src texture_type_1 @
                     Type.texture_loc src texture_type_2
                   in
                   let t1 = print Type.pp_texture_type texture_type_1 in
                   let t2 = print Type.pp_texture_type texture_type_2 in

                   List.iter locs ~f:(fun loc ->
                       Output.output_error loc "failed to unify texture type %s with %s" t1 t2
                     )
                 | Unification.SceneMismatch (scene_type_1, scene_type_2) ->
                   let locs =
                     Type.scene_loc src scene_type_1 @
                     Type.scene_loc src scene_type_2
                   in
                   let t1 = print Type.pp_scene_type scene_type_1 in
                   let t2 = print Type.pp_scene_type scene_type_2 in
                   List.iter locs ~f:(fun loc ->
                       Output.output_error loc "failed to unify scene type %s with %s" t1 t2
                     )
                 | Unification.OutputMismatch (c1, c2, loc1, loc2) ->
                   List.iter [loc1;loc2] ~f:(fun loc ->
                       Output.output_error loc "failed to unify shader/drawable output %d with %d" c1 c2
                     )
                 | Unification.MissingKey (id, texp) ->
                   let id = IdentifierTable.show_m id in
                   let locs = Type.type_loc src texp in
                   let texp = print Type.pp texp in
                   List.iter locs ~f:(fun loc ->
                       Output.output_error loc
                         "the type of this term is missing a required key %s of type %s" id texp
                     )
               )
             | SortingFailed ->
               Output.output_error default_loc "too weak type constraints - could not sort outputs"
             | TypeSubstitution (var, variable_type, name) ->
               let loc = TypeVariable.get_loc src var in
               let var_type_name, ft = match variable_type with
                 | Substitution.Scalar scalar_type -> "scalar", print Type.pp_scalar_type scalar_type
                 | Substitution.Pure pure_data_type -> "Pure", print Type.pp_pure_data_type pure_data_type
                 | Substitution.Texture texture_type -> "Texture", print Type.pp_texture_type texture_type
                 | Substitution.PureDataMap _ -> "PureDataMap", "[hidden]"
                 | Substitution.TextureDataMap _ -> "TextureDataMap", "[hidden]"
                 | Substitution.Mapping mapping -> "Mapping", print Type.pp_mapping mapping
                 | Substitution.Scene scene_type -> "Scene", print Type.pp_scene_type scene_type
                 | Substitution.Internal internal_type_expr -> "Internal", print Type.pp_internal_type_expr internal_type_expr
                 | Substitution.Int int -> "Int", print Format.pp_print_int int
                 | Substitution.Variable var -> "variable", TypeVariable.show var
                 | Substitution.VAny -> "any", "any" in
               Output.output_error loc
                 "invalid substitition of a %s expression into a variable expecting %s terms (full type: %s)"
                 var_type_name name ft
             | ShaderRequirement (id, texp, obindings) ->
               let locs = Type.type_loc src texp in

               let id = IdentifierTable.show_m id in
               let texp = Type.pp Format.str_formatter texp; Format.flush_str_formatter () |> norm_str in
               let other_bindings =
                 IDMap.to_alist obindings |> List.map ~f:(fun (id, ty) ->
                     Printf.sprintf "%s = %s" (IdentifierTable.show_m id)
                       (norm_str (Type.pp Format.str_formatter ty; Format.flush_str_formatter ()))
                   ) |> String.concat ~sep:"; " |> norm_str in

               List.iter locs ~f:(fun loc ->
                   Output.output_error loc "shader requires an input named %s of type %s that is not provided (other bindings: %s)"
                     id texp other_bindings
                 )
             | InvalidShaderType st ->
               let locs = Type.scene_loc src st in
               let name, ft = (show st) in
               List.iter locs ~f:(fun loc ->
                   Output.output_error loc "expression used to construct a shader type but actual type is %s  (full_type=%s)" name ft
                 )
             | InvalidDrawableType st ->
               let locs = Type.scene_loc src st in
               let name, ft = (show st) in
               List.iter locs ~f:(fun loc ->
                   Output.output_error loc "expression used to construct a drawable type but actual type is %s  (full_type=%s)" name ft
                 )               
             | InvalidModifierType st ->
               let locs = Type.scene_loc src st in
               let name, ft = (show st) in
               List.iter locs ~f:(fun loc ->
                   Output.output_error loc "expression used to construct a modifier type but actual type is %s  (full_type=%s)" name ft
                 )               
             | InvalidObjectType st ->
               let locs = Type.scene_loc src st in
               let name, ft = (show st) in
               List.iter locs ~f:(fun loc ->
                   Output.output_error loc "expression used to construct a object type but actual type is %s  (full_type=%s)" name ft
                 )               
             | EmptyCompoundObject loc ->
               Output.output_error loc "empty compound object - compound objects must contain at least one non-optional object"               
             | IndeterminateDrawable loc ->
               Output.output_error loc "indeterminate drawable expression - drawables must contain at least one non-optional object"
            ) 
          );
            None
    in
    Option.return @@ (statements, mapping,src, can_type) 

  let type_check_and_expand ?std_lib tbl program =
    let (let+) x f = Option.bind x ~f in
    let+ (statements, mapping,_,can_type) = type_check ?std_lib tbl program in
    let f variable =
      let variable = CannonicalTypeMap.get_cannonical_form can_type variable in
      TypeVariable.Map.find_exn  mapping variable in
    let+ statements = try
        Some (List.map ~f:(map_statement f) statements)
      with Not_found_s sexp ->
        Printf.eprintf "Error: %s" (Sexp.to_string_hum sexp);
        None in
    Option.return statements
  
  let print_typed_program ?(print_type=true) ?std_lib tbl program =
    let (let+) x f = Option.bind x ~f in
    let+ (statements, mapping,_,can_type) = type_check ?std_lib tbl program in
    let f variable =
      let variable = CannonicalTypeMap.get_cannonical_form can_type variable in
      TypeVariable.Map.find_exn  mapping variable in
    let+ statements = try
        Some (List.map ~f:(map_statement f) statements)
      with Not_found_s sexp ->
        Printf.eprintf "Error: %s" (Sexp.to_string_hum sexp);
        None in
    let fmt = Format.str_formatter in
    pp_program ~print_type
      (fun fmt stmt -> Substitution.pp_variable_type fmt stmt) fmt statements;
    let str = Format.flush_str_formatter () in
    Option.return (str)

  let print_nearest_type ?std_lib tbl program loc =
    let (let+) x f = Option.bind x ~f in
    let+ (_, mapping,src,can_type) = type_check ?std_lib tbl program in
    let+ typ = TypeVariable.find_nearest_type loc src in
    let typ = CannonicalTypeMap.get_cannonical_form can_type typ in
    let+ var_type = TypeVariable.Map.find mapping typ in
    let str_typ =
        let lookup_tbl = IdentifierTable.serialize tbl in
        let str_typ = ref "" in
        IdentifierTable.with_reference_table lookup_tbl (fun () ->
            let print f v = f Format.str_formatter v; Format.flush_str_formatter () in
            str_typ := match var_type with
              | Substitution.Scalar scalar_type -> print Type.pp_scalar_type scalar_type
              | Substitution.Pure pure_data_type -> print Type.pp_pure_data_type pure_data_type
              | Substitution.Texture texture_type -> print Type.pp_texture_type texture_type
              | Substitution.PureDataMap _ -> "mapping [hidden]"
              | Substitution.TextureDataMap _ -> "mapping [hidden]"
              | Substitution.Mapping mapping -> print Type.pp_mapping mapping
              | Substitution.Scene scene_type -> print Type.pp_scene_type scene_type
              | Substitution.Internal internal_type_expr -> print Type.pp_internal_type_expr internal_type_expr
              | Substitution.Int int -> print Format.pp_print_int int
              | Substitution.Variable var -> TypeVariable.show var
              | Substitution.VAny -> "any"
          ); !str_typ
    in
    Option.return (str_typ)

end
