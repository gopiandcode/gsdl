open Core

type 'a vec3 = 'a * 'a * 'a


type 'a value =
  | Float : float -> float value
  | Int : int -> int value
  | Vec2 : float vec3 -> float vec3 value

type ctx = unit

type ('ctx, 'a) reducable = 'ctx -> 'a

type 'a comp = [
    `Completed of 'a value | `Partial of (ctx, 'a) reducable
  ]

let example_function (x: float comp)  (y: float comp) = match x,y with
  | `Partial f1, `Partial f2 -> `Completed (Float (f1 () +. f2 ()))
  | `Completed (Float v1), `Partial f2 -> `Completed (Float (v1 +. f2 ()))
  | `Partial f1, `Completed (Float v2) -> `Completed (Float (f1 () +. v2))
  | `Completed (Float v1), `Completed (Float v2) -> `Completed (Float (v1 +. v2))
  | _ -> .
  
