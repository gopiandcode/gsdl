open Core
open Gsdl

let usage_msg = "GopSDL Type checker - Standalone program to parse and type check a GSDL specification file."
let readme = "Program that exposes the GSDL parser and compiler as a standalone executable."


let safe_open_file filename =
  try In_channel.read_all filename with
    Sys_error msg -> Printf.exitf "%s" msg ()

let type_check_file filename =
  let module Parse = Parse.Make (PrettyError.FlycheckOut) in
  let module Typing = TypedParsetree.Make (PrettyError.FlycheckOut) in
  let module Sanitized = SanitizedParsetree.Sanitizer (PrettyError.FlycheckOut) in
  let file = safe_open_file filename in
  let lexbuf = Lexing.from_string file in
  let result = Parse.parse lexbuf in
  let tbl = SanitizedParsetree.IdentifierTable.init () in
  let result = Sanitized.sanitize_program tbl result in
  let result = SanitizedParsetree.Output.extract_program result in
  match result with
  | None -> Printf.exitf  "failed to sanitize" ()
  | Some result -> begin
      match Typing.type_check tbl result with
      | None -> Printf.exitf "failed to typecheck" ()
      | Some _ -> Printf.printf "program type checked successfully";
    end

let print_compiled_program filename =
  let module Parse = Parse.Make (PrettyError.FlycheckOut) in
  let module Typing = TypedParsetree.Make (PrettyError.FlycheckOut) in
  let module Sanitized = SanitizedParsetree.Sanitizer (PrettyError.FlycheckOut) in
  let file = safe_open_file filename in
  let lexbuf = Lexing.from_string file in
  let result = Parse.parse lexbuf in
  let tbl = SanitizedParsetree.IdentifierTable.init () in
  let result = Sanitized.sanitize_program tbl result in
  let result = SanitizedParsetree.Output.extract_program result in
  match result with
  | None -> Printf.exitf  "failed to sanitize" ()
  | Some result -> begin
      match Typing.type_check_and_expand tbl result with
      | None -> Printf.exitf "failed to typecheck" ()
      | Some statements ->
        let compiled_program = CompiledParsetree.OfTyped.eval_program
            (SanitizedParsetree.IdentifierTable.serialize tbl) statements in        
        Printf.printf "%s" (CompiledParsetree.OfTyped.show_program compiled_program);
    end


let print_typed_file ~print_type filename =
  let module Parse = Parse.Make (PrettyError.FlycheckOut) in
  let module Typing = TypedParsetree.Make (PrettyError.FlycheckOut) in
  let module Sanitized = SanitizedParsetree.Sanitizer (PrettyError.FlycheckOut) in
  let file = safe_open_file filename in
  let lexbuf = Lexing.from_string file in
  let result = Parse.parse lexbuf in
  let tbl = SanitizedParsetree.IdentifierTable.init () in
  let result = Sanitized.sanitize_program tbl result in
  let result = SanitizedParsetree.Output.extract_program result in
  match result with
  | None -> Printf.exitf  "failed to sanitize" ()
  | Some result -> begin
      match Typing.print_typed_program ~print_type tbl result with
      | None -> Printf.exitf "failed to typecheck" ()
      | Some str -> Printf.printf "%s" str; ()
    end


let print_nearest_type filename line column cnum =
  (* suppress any typing errors - we only care about the type name  *)
  let module Parse = Parse.Make (PrettyError.SilentOut) in
  let module Typing = TypedParsetree.Make (PrettyError.SilentOut) in
  let module Sanitized = SanitizedParsetree.Sanitizer (PrettyError.SilentOut) in
  let pos = Lexing.{pos_fname=filename; pos_lnum=line; pos_cnum=cnum; pos_bol=(cnum -column) } in
  let file = safe_open_file filename in
  let lexbuf = Lexing.from_string file in
  let result = Parse.parse lexbuf in
  let tbl = SanitizedParsetree.IdentifierTable.init () in
  let result = Sanitized.sanitize_program tbl result in
  let result = SanitizedParsetree.Output.extract_program result in
  match result with
  | None -> Printf.exitf "Error: failed to sanitize program" ()
  | Some result -> begin
      match Typing.print_nearest_type tbl result pos with
      | None -> Printf.exitf "Error: failed to typecheck" ()
      | Some ty -> Printf.printf "%s" ty
    end


let () =
  let nearest_type_command  =
    let open Command.Spec in
    let cmd =
      let open Command.Let_syntax in
      let%map filename = anon ("filename" %: string) 
      and line = anon ("line" %: int)
      and column = anon ("column" %: int)
      and cnum = anon ("character-number" %: int) in
      fun () -> print_nearest_type filename line column cnum in
    Command.basic
      ~summary:"prints the type of the nearest (enclosing) expression \
                to the specified position in the input file."
      cmd
  in

  let print_program_command  =
    let open Command.Spec in
    let cmd = 
      let open Command.Let_syntax in
      let%map filename = anon ("filename" %: string) 
      and untyped =
        flag "-untyped" 
          ~doc:"print the program without type annotations (typechecking will still be performed internally)."
          no_arg in
      fun ()  -> print_typed_file ~print_type:(not untyped) filename in
    Command.basic
      ~summary:"type check the input file and pretty print the resulting program with types."
      cmd
  in

  let print_compiled_program_command  =
    let open Command.Spec in
    let cmd = 
      let open Command.Let_syntax in
      let%map filename = anon ("filename" %: string) in
      fun ()  -> print_compiled_program filename in
    Command.basic
      ~summary:"compile input file and pretty print the resulting shader program."
      cmd
  in  


  let type_check_command  =
    let open Command.Spec in
    let cmd = 
      let open Command.Let_syntax in
      let%map filename = anon ("filename" %: string) in
      fun ()  -> type_check_file filename in
    Command.basic
      ~summary:"parse and type check the input file - prints any parsing/type errors to stdout."
      cmd
  in

  let main_cmd =
    Command.group ~summary:usage_msg
      ~readme:(fun () -> readme)
      [
        "type-check", type_check_command;
        "print-type", nearest_type_command;
        "print-program", print_program_command;
        "compile", print_compiled_program_command
      ] in

  Command.run
    ~version:"0.0.1"
    ~verbose_on_parse_error:true
    main_cmd









