%{
[@@@warning "-33"]
open Parsetree


let mkloc ~loc:(loc_start,loc_end) value = Location.{value; loc={loc_start; loc_end}}
let mklocation ~loc:(loc_start,loc_end) = Location.{loc_start; loc_end}

%}

%token <int> INT
%token <float> FLOAT
%token <bool> BOOL  
%token <string> STRING_LITERAL
%token <string> IDENTIFIER
%token <string> MACRO_IDENTIFIER

%token LPAREN RPAREN
%token LSQBRAC RSQBRAC
%token LANGLE RANGLE
%token SEMI
%token COLON
%token COMMA

%token AND OR PLUS MINUS  MUL DIV MOD NOT
%token EQ

%left COLON
%left THEN
%left ELSE
%left AND OR
%left PLUS MINUS
%left MUL DIV MOD
%left LSQBRAC
%nonassoc UPLUS UMINUS NOT

%token FOR
%token IN
%token TO
%token IF
%token THEN
%token ELSE
%token FUNCTION
%token BEGIN
%token END
%token VAR
%token MODEL
%token DATA
%token SCREEN

%token TEXTURE
%token UNIFORM
%token ATTRIBUTE
%token TRANSLATE
%token SCALE
%token ROTATE
%token DRAW_OPTIONS
%token LAYER

%token WITH
%token REDIRECTING
%token MACRO
%token LIGHT_DATA
%token LIGHT_UNIFORM
%token OVERLAYING

%token TYP_INT
%token TYP_BOOL
%token TYP_FLOAT
%token TYP_TEXTURE1D
%token TYP_TEXTURE2D
%token TYP_TEXTURE3D
%token TYP_TEXTURE_CUBE
%token TYP_HOLE
%token <string> TYP_VARIABLE
%token TYP_NUMERIC_DATA
%token TYP_MAP
%token TYP_VEC2
%token TYP_VEC3
%token TYP_VEC4
%token TYP_MAT2
%token TYP_MAT3
%token TYP_MAT4
%token TYP_LIST
%token TYP_MULTI
%token TYP_OBJECT
%token TYP_MODIFIER
%token TYP_LIGHT
%token TYP_SHADER
%token TYP_DRAWABLE
%token DRAWABLE

%token EOF

%start <Parsetree.expr option> _expr_only_file
%start <Parsetree.type_expr option> _type_expr_only_file
%start <Parsetree.declaration option> _declaration_only_file
%start <Parsetree.data option> _data_only_file
%start <Parsetree.modifier option> _modifier_only_file
%start <Parsetree.obj option> _object_only_file
%start <Parsetree.scene_expr option> _scene_expr_only_file
%start <Parsetree.statement list> program
%%

_expr_only_file:
  | EOF { None }
  | r = expr EOF { Some r}
;

_type_expr_only_file:
  | EOF { None }
  | r = type_expr EOF { Some r }
;

_declaration_only_file:
  | EOF { None }
  | r = declaration EOF { Some r }
;

_data_only_file:
  | EOF { None }
  | r = data EOF { Some r }
;

_object_only_file:
  | EOF { None }
  | r = obj EOF { Some r }
;
  
_modifier_only_file:
  | EOF { None }
  | r = modifier EOF { Some r }
;

_scene_expr_only_file:
  | EOF { None }
  | r = scene_expr EOF { Some r }
;

identifier:
  | id = IDENTIFIER { mkloc ~loc:$loc id }
;

type_name:
  | TYP_VEC2 { TypeVec2 (mklocation ~loc:$loc) }
  | TYP_VEC3 { TypeVec3 (mklocation ~loc:$loc) }
  | TYP_VEC4 { TypeVec4 (mklocation ~loc:$loc) }
  | TYP_MAT2 { TypeMat2 (mklocation ~loc:$loc) }
  | TYP_MAT3 { TypeMat3 (mklocation ~loc:$loc) }
  | TYP_MAT4 { TypeMat4 (mklocation ~loc:$loc) }
  | TYP_LIST { TypeList (mklocation ~loc:$loc) }
  | TYP_MULTI { TypeMulti (mklocation ~loc:$loc) }
  | TYP_OBJECT { TypeObject (mklocation ~loc:$loc) }
  | TYP_MODIFIER { TypeModifier (mklocation ~loc:$loc) }
  | TYP_LIGHT { TypeLight (mklocation ~loc:$loc) }
  | TYP_SHADER { TypeShader (mklocation ~loc:$loc) }
  | TYP_DRAWABLE { TypeDrawable (mklocation ~loc:$loc) }
  | TYP_MAP { TypeMapTag (mklocation ~loc:$loc) }
;

type_map:
  | id = identifier; EQ; expr = type_expr { mkloc ~loc:$loc (id, expr) }
;

type_expr:
  | TYP_INT { TypeInt (mklocation ~loc:$loc)}
  | TYP_BOOL { TypeBool (mklocation ~loc:$loc)}
  | TYP_FLOAT { TypeFloat (mklocation ~loc:$loc)}
  | TYP_TEXTURE1D { TypeTexture1D (mklocation ~loc:$loc)}
  | TYP_TEXTURE2D { TypeTexture2D (mklocation ~loc:$loc)}
  | TYP_TEXTURE3D { TypeTexture3D (mklocation ~loc:$loc)}
  | TYP_TEXTURE_CUBE { TypeTextureCube (mklocation ~loc:$loc)}
  | TYP_NUMERIC_DATA { TypeNumericData (mklocation ~loc:$loc)}
  | TYP_HOLE { TypeHole (mklocation ~loc:$loc)}
  | id = TYP_VARIABLE { TypeVariable (mkloc ~loc:$loc id)}
  | i = INT { TypeNumber (mkloc ~loc:$loc i)}
  | LPAREN; ls = separated_list(COMMA, type_expr); RPAREN; name = type_name
             { TypeProduct (mkloc ~loc:$loc (ls,name))}
  | LANGLE; ls = separated_nonempty_list(SEMI, type_map); RANGLE { TypeMap (mkloc ~loc:$loc ls) }
  | e = type_expr; name = type_name
             { TypeProduct (mkloc ~loc:$loc ([e],name))}
;

expr:
  | b = BOOL  { Exp_bool (mkloc ~loc:$loc b) }
  | i = INT   { Exp_number (mkloc ~loc:$loc (Int i)) }
  | f = FLOAT { Exp_number (mkloc ~loc:$loc (Float f)) }
  | id = IDENTIFIER { Exp_variable (mkloc ~loc:$loc id) }

  | e1 = expr; MUL; e2 = expr { Exp_binop (mkloc ~loc:$loc
                                              (Mul, e1, e2))}
  | e1 = expr; DIV; e2 = expr { Exp_binop (mkloc ~loc:$loc
                                              (Div, e1, e2))}
  | e1 = expr; MOD; e2 = expr { Exp_binop (mkloc ~loc:$loc
                                              (Mod, e1, e2))}
  | e1 = expr; PLUS; e2 = expr { Exp_binop (mkloc ~loc:$loc
                                              (Plus, e1, e2))}
  | e1 = expr; MINUS; e2 = expr { Exp_binop (mkloc ~loc:$loc
                                              (Minus, e1, e2))}
  | e1 = expr; AND; e2 = expr { Exp_binop (mkloc ~loc:$loc
                                              (And, e1, e2))}
  | e1 = expr; OR; e2 = expr { Exp_binop (mkloc ~loc:$loc
                                              (Or, e1, e2))}
  | MINUS; e = expr; %prec UMINUS { Exp_preop (mkloc ~loc:$loc
                                               (UMinus, e))}
  | PLUS; e = expr; %prec UPLUS { Exp_preop (mkloc ~loc:$loc
                                               (UPlus, e))}
  | NOT; e = expr { Exp_preop (mkloc ~loc:$loc
                                               (UNot, e))}
  | e1 = expr; LSQBRAC; e2 = expr; RSQBRAC { Exp_index (mkloc ~loc:$loc (e1,e2)) }
  | LSQBRAC; e1 = expr; SEMI; ls = separated_nonempty_list(SEMI, expr); RSQBRAC { Exp_vector (mkloc ~loc:$loc (e1 :: ls))}
  | LSQBRAC;  ls = separated_list(COMMA, expr); RSQBRAC { Exp_list (mkloc ~loc:$loc (ls))}
  | LSQBRAC;
    e1 = expr;
    FOR;
    ident = identifier;
    IN;
    e2 = expr;
    e3 = option(TO e = expr { e });
    RSQBRAC { Exp_list_comprehension (mkloc ~loc:$loc (e1, ident, e2, e3))}
  | id = identifier; LPAREN; ls = separated_list(COMMA, argument); RPAREN {
                                                                       Exp_function_call
                                                                     (mkloc ~loc:$loc (id, ls))
                                                                     }
  | IF; e1 = expr; THEN; e2 = expr; ELSE; e3 = expr {
                                                   Exp_if_then_else
                                                 (mkloc ~loc:$loc (e1,e2,e3))
                                                 }
  | LPAREN; e = expr; RPAREN { e }
  | e = expr; COLON; ty = type_expr { Exp_with_type (mkloc ~loc:$loc (e, ty))}
;

argument:
  | e = expr { (None, e) }
  | id = identifier; EQ; e = expr { (Some id, e) }
;

argument_decl:
  | id = identifier;
    typ = option(COLON ty = type_expr { ty });
    exp = option(EQ exp = expr {exp}) { (id, typ, exp) }
;

macro_argument_decl:
  | id = identifier;
    typ = option(COLON ty = type_expr { ty });
    exp = option(EQ exp = scene_expr {exp}) { (id, typ, exp) }
;


declaration:
  | FUNCTION; identifier = identifier;
    LPAREN; arguments = separated_list(COMMA, argument_decl); RPAREN;
    typ = option(COLON ty = type_expr { ty });
    BEGIN; body = expr; END { DataFunction {identifier; arguments; typ; body;} }
  | VAR;
    identifier = identifier;
    typ= option(COLON ty = type_expr { ty });
    EQ;
    body = expr;
    SEMI { DataVariable { identifier; typ; body }}
  | MACRO; FUNCTION; identifier = identifier;
    LPAREN; arguments = separated_list(COMMA, macro_argument_decl); RPAREN;
    typ = option(COLON ty = type_expr { ty });
    BEGIN; body = scene_expr; END { MacroFunction {identifier; arguments; typ; body;} }
  | MACRO; VAR;
    identifier = identifier;
    typ= option(COLON ty = type_expr { ty });
    EQ;
    body = scene_expr;
    SEMI { MacroVariable { identifier; typ; body }}
;

string_literal:  
  | str = STRING_LITERAL {mkloc ~loc:$loc str}
;

expr_or_string:
  | str = string_literal { ArgumentString str }
  | exp = expr { ArgumentExpression exp }
;

data_argument:
  | id = identifier; EQ; exp = expr_or_string { Some id, exp }
  | exp = expr_or_string { None, exp }
;

modifier:  
  | TYP_MODIFIER; LPAREN;
    ls = separated_list(COMMA, scene_argument);
    RPAREN;
    { ModifierReference (mkloc ~loc:$loc ls)}
  | DRAW_OPTIONS;
    LPAREN; ls = separated_list(COMMA, data_argument);
    RPAREN { BasicModifier (mkloc ~loc:$loc (DrawOptions, ls))}
  | TEXTURE;
    LPAREN; ls = separated_list(COMMA, data_argument);
    RPAREN { BasicModifier (mkloc ~loc:$loc (Texture, ls))}
  | LIGHT_UNIFORM;
    LPAREN; ls = separated_list(COMMA, expr);
    RPAREN { LightModifier (mkloc ~loc:$loc ls)}
  | UNIFORM;
    LPAREN; ls = separated_list(COMMA, data_argument);
    RPAREN { BasicModifier (mkloc ~loc:$loc (Uniform, ls))}
  | ATTRIBUTE;
    LPAREN; ls = separated_list(COMMA, data_argument);
    RPAREN { BasicModifier (mkloc ~loc:$loc (Attribute, ls))}
  | TRANSLATE;
    LPAREN; ls = separated_list(COMMA, data_argument);
    RPAREN { BasicModifier (mkloc ~loc:$loc (Translate, ls))}
  | SCALE;
    LPAREN; ls = separated_list(COMMA, data_argument);
    RPAREN { BasicModifier (mkloc ~loc:$loc (Scale, ls))}
  | ROTATE;
    LPAREN; ls = separated_list(COMMA, data_argument);
    RPAREN { BasicModifier (mkloc ~loc:$loc (Rotate, ls))}
  | REDIRECTING; ls = separated_nonempty_list(SEMI, argument); 
    d = drawable;
    { RedirectingModifier (mkloc ~loc:$loc (ls, d)) }
;


data:
  | MODEL; LPAREN; ls = separated_list(COMMA, data_argument); RPAREN { DataComponent (mkloc ~loc:$loc (Model, ls))  }
  | SCREEN; LPAREN; ls = separated_list(COMMA, data_argument); RPAREN
                                                                 { DataComponent (mkloc ~loc:$loc (Screen, ls))  }
  | DATA; LPAREN; ls = separated_list(COMMA, data_argument); RPAREN
                                                               { DataComponent (mkloc ~loc:$loc (Data, ls))  }
;

base_object:
  | TYP_LIGHT; LPAREN; ls = separated_list(COMMA, data_argument); RPAREN { Light (mkloc ~loc:$loc ls) }
  | LIGHT_DATA; LPAREN; ls = separated_list(COMMA, expr); RPAREN { LightData (mkloc ~loc:$loc ls) }
  | d = data { ObjectData d }
  | TYP_OBJECT; LPAREN; ls = separated_list(COMMA, scene_argument); RPAREN {
                                                                        ObjectReference (mkloc ~loc:$loc ls)
                                                                      }
  | BEGIN; ls = list(scene_expr_seq); END { CompoundObject (mkloc ~loc:$loc ls)}
;

obj:  
  | ls = list(modifier); o = base_object { Object (mkloc ~loc:$loc (ls,o))}
;                               

maybe_scene_expr:
  | exp = expr { SceneDataExpression exp }
  | exp = scene_expr { SceneInlineExpression exp }
  | exp = string_literal { SceneString exp }
;

scene_argument:
  | id = identifier; EQ; exp = maybe_scene_expr { Some id, exp }
  | exp = maybe_scene_expr { None, exp }
;

shader:
  | TYP_SHADER; LPAREN; ls = separated_list(COMMA, scene_argument); RPAREN { BasicShader (mkloc ~loc:$loc ls)}
;

drawable:
  | OVERLAYING; d = drawable { OverlayingDrawable (mkloc ~loc:$loc d)}
  | WITH; s = shader; o = obj { DrawableShader (mkloc ~loc:$loc (s,o))}
  | DRAWABLE;
    LPAREN; ls = separated_list(COMMA, scene_argument);
    RPAREN { DrawableReference (mkloc ~loc:$loc ls)}
;

macro_identifier:
  | id = MACRO_IDENTIFIER { mkloc ~loc:$loc id }

scene_expr:
  | id = macro_identifier;
    LPAREN; ls = separated_list(COMMA, scene_argument); RPAREN;
    { SceneExpr_macro_call (mkloc ~loc:$loc (id, ls)) }
  | id = macro_identifier
    { SceneExpr_variable id }
  | o = obj { SceneExpr_object o }
  | s = shader { SceneExpr_shader s }
  | d = drawable { SceneExpr_drawable d }
  | m = modifier { SceneExpr_modifier m }
  | exp = scene_expr; COLON; typ = type_expr { SceneExpr_with_type (mkloc ~loc:$loc (exp, typ)) }
  | IF; e1 = expr; THEN; e2 = scene_expr;
    ELSE;  e3 = scene_expr;
    { SceneExpr_if_then_else (mkloc ~loc:$loc (e1,e2,Some e3))}
  | IF; e1 = expr; THEN; e2 = scene_expr;
    { SceneExpr_if_then_else (mkloc ~loc:$loc (e1,e2,None))}
  | LPAREN; exp = scene_expr; RPAREN; { exp }
  | FOR; id = identifier; IN;
    e1 = expr;
    e2 = option(TO exp = expr { exp });
    BEGIN; e3 = list(scene_expr_seq) END;
    { SceneExpr_for (mkloc ~loc:$loc (id, e1, e2, e3))}
;                                                          

scene_expr_seq:
  (* | id = macro_identifier;
   *   LPAREN; ls = separated_list(COMMA, scene_argument); RPAREN; SEMI;
   *   { SceneExpr_macro_call (mkloc ~loc:$loc (id, ls)) }
   * | id = macro_identifier; SEMI;
   *   { SceneExpr_variable id } *)
  | o = obj; SEMI { SceneExpr_object o }
  | s = shader; SEMI { SceneExpr_shader s }
  | d = drawable; SEMI { SceneExpr_drawable d }
  | m = modifier; SEMI { SceneExpr_modifier m }
  (* | LPAREN; exp = scene_expr; RPAREN; SEMI; { exp } *)
  | IF; e1 = expr; THEN; e2 = scene_expr;
    ELSE;  e3 = scene_expr; SEMI;
    { SceneExpr_if_then_else (mkloc ~loc:$loc (e1,e2,Some e3))}
  | IF; e1 = expr; THEN; e2 = scene_expr; SEMI;
    { SceneExpr_if_then_else (mkloc ~loc:$loc (e1,e2,None))}
  | FOR; id = identifier; IN;
    e1 = expr;
    e2 = option(TO exp = expr { exp });
    BEGIN; e3 = list(scene_expr_seq) END;
    { SceneExpr_for (mkloc ~loc:$loc (id, e1, e2, e3))}
;                                                          

layer:
  | LAYER; LPAREN; ls = separated_list(COMMA, data_argument); RPAREN; COLON;
    { mkloc ~loc:$loc (ls) }
;

statement:
  | decl = declaration; { Declaration decl }
  | draw = drawable; SEMI { Drawable draw}
  | ls = layer;  draw = drawable; SEMI { LayeredDrawable (ls, draw )}
;

program:
  | ls = list(statement) EOF { ls }
;
