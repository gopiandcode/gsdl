
let rec pop_until pred env =
  let open Parser.MenhirInterpreter in
  match top env with
  | None -> []
  | Some elt ->
    match pred elt with
    | [] -> begin match pop env with
        | None -> assert false
        | Some env -> pop_until pred env
      end
    | l -> l

let keep_predictions predictions (production, focus) =
  let open Parser.MenhirInterpreter in
  if focus < List.length (rhs production) then (lhs production) :: predictions else predictions

let elemnt_contains_prediction_items elt =
  let open Parser.MenhirInterpreter in
  match elt with
  | Element (state, _, _, _) ->
    items state |> List.fold_left keep_predictions []

let find_context =
  let open Parser.MenhirInterpreter in
  function
  | InputNeeded env ->
    pop_until elemnt_contains_prediction_items env
  | _ ->
    assert false

module Make : functor (Output : Pretty_error.ErrorOut) ->
sig

  val parse : Lexing.lexbuf -> Parsetree.statement list

  val parse_safe : Lexing.lexbuf -> Parsetree.statement list option

end = functor  (Output: Pretty_error.ErrorOut) -> struct

  let any_errors = ref false

  let contextual_error_msg lexbuf checkpoint =
    let location = Location.{
        loc_start=Lexing.lexeme_start_p lexbuf;
        loc_end=Lexing.lexeme_end_p lexbuf;
      } in
    find_context checkpoint |> fun nonterminals ->
    any_errors := true;
    Output.output_error location
      "error here while analyzing a %s" 
      (String.concat " or a " @@ List.map Symbol.string_of_symbol nonterminals)

  let next_token = ref Parser.EOF

  let init lexbuf =
    next_token := Lexer.token lexbuf

  let token lexbuf =
    let token = !next_token in
    next_token := Lexer.token lexbuf;
    token

  let pos lexbuf = Lexing.(lexeme_start_p lexbuf, lexeme_end_p lexbuf)

  (* skips token forward such that the output from token will return a token matching pred *)
  let skip_until_before lexbuf pred =
    let rec loop () =
      if pred !next_token
      then ()
      else begin ignore (token lexbuf); loop () end in
    loop ()

  let resume_on_error last_reduction lex =
    match last_reduction with
    | `FoundDrawableAt checkpoint ->
      skip_until_before lex (fun t -> t = Parser.SEMI);
      checkpoint
    | (`FoundNothingAt checkpoint | `FoundDeclarationAt checkpoint) ->
      skip_until_before lex (function
            Parser.EOF | Parser.LAYER | Parser.VAR | Parser.MACRO | Parser.FUNCTION -> true
          | _ -> false);
      checkpoint

  let update_last_reduction _checkpoint production last_reduction =
    let open Parser.MenhirInterpreter in
    match (lhs production) with
    (* | X (N N_drawable) ->
     *   print_endline "using checkpoint drawable";
     *   `FoundDrawableAt checkpoint
     * | X (N N_declaration) ->
     *   print_endline "using checkpoint declaration";
     *   `FoundDeclarationAt checkpoint *)
    | _ ->
      last_reduction

  let rec on_error last_reduction lexer checkpoint =
    contextual_error_msg lexer checkpoint;
    resume_on_error last_reduction lexer
  and run last_reduction inp_needed lexer checkpoint =
    let open Parser.MenhirInterpreter in
    match checkpoint with
    | InputNeeded _ ->
      let token = token lexer in
      let (star,ed) = pos lexer in
      run last_reduction checkpoint lexer (offer checkpoint (token, star,ed))
    | Accepted x -> x
    | Rejected
    | HandlingError _  ->
      let after_error = on_error last_reduction lexer inp_needed in
      run last_reduction inp_needed lexer after_error
    | Shifting _ ->
      run last_reduction inp_needed lexer (resume checkpoint)
    | AboutToReduce (_, production) ->
      run
        (update_last_reduction inp_needed production last_reduction)
        inp_needed
        lexer
        (resume checkpoint)

  let w_catching_lexing_errors f onf =
    begin try f () with
    | Lexer.Error (err, loc) ->
      let () = match err with
        | Lexer.Unterminated_comment ->
          Output.output_error loc "lexing error due to end of file in unterminated comment"
        | Lexer.Illegal_character char ->
          Output.output_error loc "lexing error due to use of invalid character '%c'" char
        | Lexer.Reserved_sequence (str1, Some str2) ->
          Output.output_error loc "lexing error due to use of reserved sequence %s %s" str1 str2
        | Lexer.Reserved_sequence (str1, None) ->
          Output.output_error loc "lexing error due to use of reserved sequence %s" str1
        | Lexer.Unterminated_string ->
          Output.output_error loc "lexing error due to unterminated string literal"
        | Lexer.Invalid_literal str ->
          Output.output_error loc "lexing error due to invalid literal \"%s\"" str in
      onf
    end


  let parse lexbuf =
    init lexbuf;
    w_catching_lexing_errors (fun () ->
        let checkpoint = Parser.Incremental.program lexbuf.lex_curr_p in
        run (`FoundNothingAt checkpoint) checkpoint lexbuf checkpoint
      ) []
    

  let parse_safe lexbuf =
    any_errors := false;
    init lexbuf;
    w_catching_lexing_errors (fun () -> 
        let checkpoint = Parser.Incremental.program lexbuf.lex_curr_p in
        let res = (run (`FoundNothingAt checkpoint) checkpoint lexbuf checkpoint) in
        if not !any_errors then Some res else None
      ) None
  

end
