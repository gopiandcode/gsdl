open Core

module type ErrorOut = sig

  val output_error: Location.t -> ('a, unit, string, unit) format4 -> 'a

end

(* output errors in a format suitable for reading from flycheck *)
module FlycheckOut : ErrorOut = struct

  let output_error ({ loc_start=lst; loc_end=_led }:Location.t)  =
    let to_str loc =
      let open Lexing in
      Printf.sprintf "%d-%d"
        loc.pos_lnum
        (loc.pos_cnum - loc.pos_bol + 1)
    in
    Printf.ksprintf (fun result ->
        Printf.printf "%s: ERROR: %s\n"
          (to_str lst)
          (* (to_str led) *)
          result
      ) 


end

module SilentOut : ErrorOut = struct

  let output_error (_loc:Location.t)  =
    Printf.ksprintf (fun _ -> () ) 

end
