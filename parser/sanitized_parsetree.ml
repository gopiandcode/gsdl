open Core

open Location
open Pretty_error

module IdentifierTable : sig
  type t
  type m
  type a

  module MMap : Map.S with type Key.t = m
  module HMap : Hashtbl.S with type key = m

  val init : unit -> t
  val add_identifier: t -> string -> m
  val equal: m -> m -> bool

  val with_reference_table: a -> (unit -> unit) -> unit

  val pp_m : Format.formatter -> m -> unit
  val show_m : m -> string
  val equal_m : m -> m -> bool
  val compare_m : m -> m -> m
  val m_of_sexp : Sexp.t -> m
  val sexp_of_m : m -> Sexp.t
  val to_string : t -> m -> string
  val serialize : t -> a
  val lookup: a -> m -> string
  val hash_fold_m :
    Base_internalhash_types.state -> m -> Base_internalhash_types.state

end = struct

  module M = Hashtbl.Make (String)

  module MMap = Map.Make (Int)
  module HMap = Hashtbl.Make (Int)

  type t = { mutable next_id: int; mapping: int M.t; mutable elems: string list }
  type m = int
  [@@deriving sexp, eq, ord, hash]

  type a = string Array.t

  let reference_table = ref None

  let with_reference_table (vl: a) f =
    reference_table := Some vl;
    f ();
    reference_table := None

  let to_string tbl m =
    List.nth_exn tbl.elems (List.length tbl.elems - m)


  let pp_m fmt (id:m)  = match !reference_table with
    | None ->
      Format.pp_print_string fmt "id_";
      Format.pp_print_int fmt id
    | Some tbl ->
      Format.pp_print_string fmt (tbl.(id))

  let show_m (id: m) = match !reference_table with
    | None -> Printf.sprintf "id_%d"  id
    | Some tbl -> tbl.(id)

  let serialize st = List.rev st.elems |> List.to_array

  let lookup arr id = arr.(id)

  let init () = {
    next_id = 0;
    mapping = M.create ();
    elems = []
  }

  let add_identifier state str =
    let result = ref state.next_id in
    M.update
      state.mapping str
      ~f:(fun vl -> match vl with
          | None ->
            state.next_id <- state.next_id + 1;
            state.elems <- str :: state.elems;
            !result
          | Some v ->
            result := v;
            v
        );
    !result

  let equal id id' = id = id'

end

module Raw = struct

  type expr =
    | Exp_bool of bool loc
    | Exp_number of Parsetree.num loc
    | Exp_variable of IdentifierTable.m loc
    | Exp_binop of (Parsetree.binop * expr * expr) loc
    | Exp_preop of (Parsetree.preop * expr) loc
    | Exp_index of (expr * expr) loc
    | Exp_vector of vector loc
    | Exp_list of expr list loc
    | Exp_list_comprehension of (expr * IdentifierTable.m loc * expr * expr option) loc
    | Exp_function_call of IdentifierTable.m loc * function_args
    | Exp_if_then_else of (expr * expr * expr) loc
    | Exp_with_type of (expr * type_expr) loc
    | ExpFail
  and function_args = FunctionArguments of
      {
        keyword_args: expr IdentifierTable.MMap.t;
        [@printer fun fmt elem ->
          Format.pp_print_list (fun fmt (m, sexpr) ->
              Format.pp_print_string fmt "(";
              IdentifierTable.pp_m fmt m;
              Format.pp_print_string fmt ",";                
              pp_expr fmt sexpr;
              Format.pp_print_string fmt ")")
            fmt (IdentifierTable.MMap.to_alist elem) ]
        positional_args: expr list;
      }
  and scalar_type = TyBool of Location.t | TyInt of Location.t | TyFloat of Location.t
  and texture_type = TyTexture1D of Location.t  | TyTexture2D of Location.t
                   | TyTexture3D of Location.t  | TyTextureCube  of Location.t
  and non_scalar_type =
      TyVec2 of scalar_type or_variable loc | TyVec3 of scalar_type or_variable loc | TyVec4 of scalar_type or_variable loc |
      TyMat2 of scalar_type or_variable loc | TyMat3 of scalar_type or_variable loc | TyMat4 of scalar_type or_variable loc
  and pure_data_type =
      TyScalar of scalar_type
    | TyNonScalar of non_scalar_type
    | TyNumericData of Location.t
  and 'a typemap = 'a IdentifierTable.MMap.t [@opaque]
  and 'a or_variable = TyDirect of 'a | TyVariable of IdentifierTable.m loc | Hole of Location.t
  and mapping = MkTyMapping of  (pure_data_type or_variable typemap or_variable *
                              pure_data_type or_variable typemap or_variable *
                              texture_type or_variable typemap or_variable) loc
  and scene_type =
      TyObject of (mapping or_variable * mapping or_variable) loc
    | TyModifier of (mapping or_variable * mapping or_variable) loc
    | TyLight of mapping or_variable loc
    | TyShader of (mapping or_variable * int or_variable) loc
    | TyDrawable of (mapping or_variable * int or_variable) loc
    | TyMulti of (scene_type or_variable) loc
  and internal_type_expr =
    | TyPure of pure_data_type
    | TyTexture of texture_type
    | TyScene of scene_type
    | TyList of type_expr
  and type_expr = TypeExpr of internal_type_expr or_variable
                | TypeFail
  and vector =
    | Vector2 of (expr * expr) 
    | Vector3 of (expr * expr * expr)
    | Vector4 of (expr * expr * expr * expr)
    | VectorFail
  and data =
    | Model of string loc         (* path to model *)
    | Data of (IdentifierTable.m loc * expr) list loc (* list of name to value mappings  *)
    | Screen of {width: expr option; height: expr option; loc:Location.t} 
    | DataFail
  and wrap_option =
    | Repeat
    | MirroredRepeat
    | ClampToEdge
    | ClampToBorder
  and filter_option =
    | Nearest
    | Linear  
    | NearestMipmapNearest
    | NearestMipmapLinear
    | LinearMipmapNearest
    | LinearMipmapLinear
  and texture_option =
    | TextureBaseLevel of expr
    | TextureMaxLevel of expr
    | TextureBorderColor of expr
    | WrapS of wrap_option
    | WrapT of wrap_option
    | MinFilter of filter_option
    | MagFilter of filter_option
  and dimension_options =
    | DimensionVector of expr
    | DimensionXZY of {x: expr option; y: expr option; z: expr option; loc: Location.t; }
  and rotate_options =
    | RotateAngle of {x: expr option; y:expr option; z: expr option; vector: expr option; loc: Location.t}
  and blend_func =
    | Zero
    | One
    | Src_color
    | One_minus_src_color
    | Dst_color
    | One_minus_dst_color
    | Src_alpha
    | One_minus_src_alpha
    | Dst_alpha
    | One_minus_dst_alpha
    | Constant_color
    | One_minus_constant_color
    | Constant_alpha
    | One_minus_constant_alpha
    | Src_alpha_saturate
  and comparison_func =
    | Stencil_never
    | Stencil_less
    | Stencil_lequal
    | Stencil_greater
    | Stencil_gequal
    | Stencil_equal
    | Stencil_notequal
    | Stencil_always
  and blend_color_option =
    | ColourVector of expr
    (* | ColourArgs of {red: expr option; green: expr option; blue: expr option; alpha: expr option} *)
  and draw_option =
    | AlphaTest of expr
    | BlendTest of expr
    | DepthTest of expr
    | StencilTest of expr
    | BlendColor of blend_color_option
    | BlendFunction of blend_func * blend_func
    | StencilFunction of comparison_func * expr * expr
    | DepthFunction of comparison_func
    | AlphaFunction of comparison_func * expr
  and modifier =
    | Texture of IdentifierTable.m loc * string loc list *
                 texture_option list    (* texture name, texture file,  *)
    | Uniform of IdentifierTable.m loc * expr                             (* uniform name, expression *)
    | Attribute of IdentifierTable.m loc * expr 
    | Translate of dimension_options
    | Scale of dimension_options
    | Rotate of rotate_options
    | DrawOptions of draw_option loc list loc
    | Redirecting of {
        textures: IdentifierTable.m loc list;
        depth: bool; stencil: bool;
        drawable: drawable;
        loc: Location.t;          
      }
    | LightUniform of IdentifierTable.m loc * IdentifierTable.m loc * expr
    | ModifierReference of reference
    | ModifierFail
  and base_object =
    | Light of {
        name: IdentifierTable.m loc;
        position: expr;
        size: expr;
      }
    | LightData of {
        name: IdentifierTable.m loc;
        }
    | ObjectData of data
    | ObjectReference of reference
    | CompoundObject of (obj_expr * obj_expr list) loc
    | ObjectFail
  and obj = Object of modifier list * base_object * Location.t
  and obj_expr =
    | PlainObject of obj
    | ObjectIfThenElse of expr * obj_expr * obj_expr option
    | ObjectFor of (IdentifierTable.m loc * expr * expr option * (obj_expr * obj_expr list)) loc
    | ObjectExprFail
  and declaration =
    | DataFunction of
        { identifier: IdentifierTable.m loc;
          keyword_arguments: (type_expr option * expr) IdentifierTable.MMap.t;
        [@printer fun fmt elem ->
          Format.pp_print_list (fun fmt (m, (texpr, sexpr)) ->
              Format.pp_print_string fmt "(";
              IdentifierTable.pp_m fmt m;
              Format.pp_print_string fmt ",";                
              Format.pp_print_string fmt "(";
              (match texpr with
               | None -> Format.pp_print_string fmt "None"
               | Some texpr -> pp_type_expr fmt texpr );
              Format.pp_print_string fmt ",";                
              pp_expr fmt sexpr;
              Format.pp_print_string fmt ")";
              Format.pp_print_string fmt ")")
            fmt (IdentifierTable.MMap.to_alist elem) ]

          positional_arguments: (IdentifierTable.m loc * type_expr option) list;
          typ: type_expr option;
          body: expr
        }
    | DataVariable of
        { identifier: IdentifierTable.m loc; typ: type_expr option; body: expr;  }
    | MacroFunction of 
        { identifier: IdentifierTable.m loc;
          keyword_arguments: (type_expr option * scene_expr) IdentifierTable.MMap.t;
        [@printer fun fmt elem ->
          Format.pp_print_list (fun fmt (m, (texpr, sexpr)) ->
              Format.pp_print_string fmt "(";
              IdentifierTable.pp_m fmt m;
              Format.pp_print_string fmt ",";                
              Format.pp_print_string fmt "(";
              (match texpr with
               | None -> Format.pp_print_string fmt "None"
               | Some texpr -> pp_type_expr fmt texpr );
              Format.pp_print_string fmt ",";                
              pp_scene_expr fmt sexpr;
              Format.pp_print_string fmt ")";
              Format.pp_print_string fmt ")")
            fmt (IdentifierTable.MMap.to_alist elem) ]

          positional_arguments: (IdentifierTable.m loc * type_expr option) list;
          typ: type_expr option;
          body: scene_expr
        }
    | MacroVariable of
        { identifier: IdentifierTable.m loc; typ: type_expr option; body: scene_expr }
    | DeclarationFail
  and reference =
    | DirectReference of IdentifierTable.m loc
    | IndirectReference of IdentifierTable.m loc * macro_arguments
  and shader =
    | ShaderReference of reference
    | Shader of { vertex_shader: string loc; fragment_shader: string loc option; geometry_shader: string loc option; }
    | ShaderFail
  and drawable =
    | OverlayingDrawable of drawable loc
    | DrawableShader of (shader * obj) loc
    | DrawableReference of reference
    | DrawableFail
  and scene_arg =
    | ArgReference of reference
    | SceneDataExpression of expr
    | SceneInlineExpression of scene_expr
  and macro_arguments =
      MacroArguments of {
        keyword_args: scene_arg IdentifierTable.MMap.t;
        [@printer fun fmt elem ->
          Format.pp_print_list (fun fmt (m, sarg) ->
              Format.pp_print_string fmt "(";
              IdentifierTable.pp_m fmt m;
              Format.pp_print_string fmt ",";                
              pp_scene_arg fmt sarg;
              Format.pp_print_string fmt ")")
            fmt (IdentifierTable.MMap.to_alist elem) ]
        positional_args: scene_arg list;
      }
  and scene_expr =
    (* | SceneExprVariable of IdentifierTable.m loc
     * | SceneExprFuncall of (IdentifierTable.m loc * (string loc option * scene_arg) list) *)
    | SceneExprObject of obj
    | SceneExprShader of shader
    | SceneExprModifier of modifier
    | SceneExprDrawable of drawable
    | SceneExprTyped of scene_expr * type_expr
    | SceneExprFail
  and statement =
    | Declaration of declaration
    | Drawable of drawable
    | LayeredDrawable of string * drawable
    | StatementFail
  [@@deriving show]
end


module Sanitizer (Output: ErrorOut) = struct

  open Raw

  let extract_type_map tbl loc (type a)  : ((IdentifierTable.t -> Parsetree.type_expr -> a or_variable option) ->
    Parsetree.type_expr ->  a or_variable typemap or_variable option) = fun f exp -> 
    let add = IdentifierTable.add_identifier tbl in
    match exp with
    | Parsetree.TypeHole l -> Some (Hole l)
    | Parsetree.TypeVariable { value; loc } -> let value = add value in Some (TyVariable {value;loc})
    | Parsetree.TypeMap { value=ls; _ } ->
      let (let+) x f = Option.bind ~f x in
      begin
        let+ map = List.fold ls ~init:(Some IdentifierTable.MMap.empty)
            ~f:(fun acc { value=(name, vl); loc } -> match acc with
                | None -> None
                | Some map ->
                  let name_id = add name.value in
                  let+ vl = f tbl vl in
                  match IdentifierTable.MMap.add map ~key:name_id ~data:vl with
                  | `Duplicate ->
                    Output.output_error loc "repeated identifier \"%s\" in map type" name.value;
                    None
                  | `Ok map -> Some map
              ) in
        Some (TyDirect map)
      end
    | _ -> 
      Output.output_error loc "invalid type arguments to map";
      None


  let elems_to_non_scalar
      tbl loc (elems: Parsetree.type_expr list)
      (f: scalar_type or_variable -> non_scalar_type) out default =
    let add = IdentifierTable.add_identifier tbl in
    match elems with
    | [s] -> begin match s with
        | Parsetree.TypeInt l -> out (TyNonScalar (f (TyDirect (TyInt l))))
        | Parsetree.TypeBool l -> out (TyNonScalar (f (TyDirect (TyBool l))))
        | Parsetree.TypeFloat l -> out (TyNonScalar (f (TyDirect (TyFloat l))))
        | Parsetree.TypeTexture1D loc
        | Parsetree.TypeTexture2D loc
        | Parsetree.TypeTexture3D loc
        | Parsetree.TypeTextureCube loc -> 
          Output.output_error loc "textures not supported as arguments to nonscalar type";
          default
        | Parsetree.TypeHole l -> out (TyNonScalar (f (Hole l)))
        | Parsetree.TypeVariable { value=vr; loc } ->
          let vr = add vr in
          out (TyNonScalar (f (TyVariable {value=vr;loc})))
        | Parsetree.TypeNumber _ ->
          Output.output_error loc "raw number not supported as argument to nonscalar type";
          default
        | Parsetree.TypeNumericData _ ->
          Output.output_error loc "numeric data not supported as argument to nonscalar type";
          default
        | Parsetree.TypeProduct _ ->
          Output.output_error loc "type product not supported as argument to nonscalar type";
          default
        | Parsetree.TypeMap _ ->
          Output.output_error loc "type map not supported as argument to nonscalar type";            
          default
      end
    | ls ->
      Output.output_error loc "invalid arity of arguments to non-scalar constructor (%d != 1)"
        (List.length ls);
      default



  let rec sanitize_expr tbl (e:Parsetree.expr) : expr =
    let add = IdentifierTable.add_identifier tbl in
    let simpl = sanitize_expr tbl  in
    match e with
    | Parsetree.Exp_bool b -> Exp_bool b
    | Parsetree.Exp_number l -> Exp_number l
    | Parsetree.Exp_variable l -> Exp_variable (map_loc l ~f:add)
    | Parsetree.Exp_binop l ->
      Exp_binop (map_loc l ~f:(fun (op, e1, e2) -> (op, simpl e1, simpl e2)))
    | Parsetree.Exp_preop l -> Exp_preop (map_loc l ~f:(fun (op, e1) -> (op, simpl e1)))
    | Parsetree.Exp_index l ->
      let l = map_loc l ~f:(fun (e1, e2) -> simpl e1, simpl e2) in
      Exp_index l
    | Parsetree.Exp_vector ({ value=_; loc } as l) ->
      let l = map_loc l ~f:(fun ls ->
          match ls with
          | [e1;e2] -> Vector2 (simpl e1, simpl e2)
          | [e1;e2;e3] -> Vector3 (simpl e1, simpl e2, simpl e3)
          | [e1;e2;e3;e4] -> Vector4 (simpl e1, simpl e2, simpl e3, simpl e4)
          | _ ->
            Output.output_error loc "invalid arity to vector (%d not in [2,3,4])" (List.length ls);
            VectorFail
        ) in
      Exp_vector l
    | Parsetree.Exp_list l ->
      let l = map_loc l ~f:(fun ls -> List.map ~f:simpl ls) in
      Exp_list l
    | Parsetree.Exp_list_comprehension l ->
      let l = map_loc l ~f:(fun (e1, id, e2, e3) ->
          simpl e1, map_loc ~f:add id, simpl e2, Option.map ~f:simpl e3) in
      Exp_list_comprehension l
    | Parsetree.Exp_function_call { value=(fname,args); _ } ->
        let fname = map_loc ~f:add fname in
        let args = List.fold args ~init:(Some (IdentifierTable.MMap.empty, []))
            ~f:(fun acc (arg_name, e) -> match acc with
                | None -> None
                | Some (map,pos) -> 
                  let e = simpl e in
                  match arg_name with
                  | None -> Some (map, e :: pos)
                  | Some { value=o_name; loc } ->
                    let name = add o_name in
                    let map = IdentifierTable.MMap.add map ~key:name ~data:e in
                    match map with
                    | `Duplicate ->
                      Output.output_error loc "duplicate keyword argument %s to data function call" o_name;
                      None
                    | `Ok map -> (Some (map, pos))
              ) in
        begin match args with
          | None -> ExpFail
          | Some (map, pos) -> Exp_function_call (fname, FunctionArguments {
              keyword_args=map;
              positional_args=List.rev pos})
        end
    | Parsetree.Exp_if_then_else l ->
      let l = map_loc l ~f:(fun (e1, e2, e3) ->  simpl e1, simpl e2, simpl e3) in
      Exp_if_then_else l
    | Parsetree.Exp_with_type l ->
      let l = map_loc l ~f:(fun (e1, ty) -> simpl e1, sanitize_type_expr tbl ty) in
      Exp_with_type l
  and extract_pure_data_type tbl (ty: Parsetree.type_expr) : pure_data_type or_variable option =
    let add = IdentifierTable.add_identifier tbl in
    let elems_to_non_scalar loc elems f =
      elems_to_non_scalar tbl loc elems f (fun v -> Some (TyDirect v)) None  in
    match ty with
    | Parsetree.TypeInt l -> Some (TyDirect (TyScalar (TyInt l)))
    | Parsetree.TypeBool l -> Some (TyDirect (TyScalar (TyBool l)))
    | Parsetree.TypeFloat l -> Some (TyDirect (TyScalar (TyFloat l)))
    | Parsetree.TypeHole l -> Some (Hole l)
    | Parsetree.TypeVariable { value; loc } -> let value = add value in Some (TyVariable {value;loc})
    | Parsetree.TypeNumericData l -> Some (TyDirect (TyNumericData l))
    | Parsetree.TypeProduct { value=(elems,name); loc } -> begin match name with
        | Parsetree.TypeVec2 loc ->
          elems_to_non_scalar loc elems (fun ns -> TyVec2 {value=ns; loc})
        | Parsetree.TypeVec3 loc ->
          elems_to_non_scalar loc elems (fun ns -> TyVec3 {value=ns; loc}) 
        | Parsetree.TypeVec4 loc ->
          elems_to_non_scalar loc elems (fun ns -> TyVec4 {value=ns; loc})
        | Parsetree.TypeMat2 loc ->
          elems_to_non_scalar loc elems (fun ns -> TyMat2 {value=ns; loc}) 
        | Parsetree.TypeMat3 loc ->
          elems_to_non_scalar loc elems (fun ns -> TyMat3 {value=ns; loc})
        | Parsetree.TypeMat4 loc ->
          elems_to_non_scalar loc elems (fun ns -> TyMat4 {value=ns; loc})
        | _ ->
          Output.output_error loc "invalid polymorphic type expecting a non-scalar type";
          None
      end
    | Parsetree.TypeTexture1D loc |Parsetree.TypeTexture2D loc |Parsetree.TypeTexture3D loc
    | Parsetree.TypeTextureCube loc  | Parsetree.TypeNumber {loc; _}  | Parsetree.TypeMap {loc; _} ->
      Output.output_error loc "invalid type argument (expecting a pure data type)";
      None
  and extract_texture_data_type tbl (ty: Parsetree.type_expr) : texture_type or_variable option =
    let add = IdentifierTable.add_identifier tbl in
    (match ty with
     | Parsetree.TypeTexture1D l -> Some (TyDirect (TyTexture1D l))
     | Parsetree.TypeTexture2D l -> Some (TyDirect (TyTexture2D l))
     | Parsetree.TypeTexture3D l -> Some (TyDirect (TyTexture3D l))
     | Parsetree.TypeTextureCube l -> Some (TyDirect (TyTextureCube l))
     | Parsetree.TypeHole l -> Some (Hole l)
     | Parsetree.TypeVariable {value;loc}  -> let value = add value in  Some (TyVariable {value;loc})
     | Parsetree.TypeInt loc 
     | Parsetree.TypeBool loc 
     | Parsetree.TypeFloat loc 
     | Parsetree.TypeNumber {loc;_} 
     | Parsetree.TypeNumericData loc 
     | Parsetree.TypeProduct {loc;_} 
     | Parsetree.TypeMap {loc;_} ->
       Output.output_error loc "invalid type argument expecting a texture type";
       None
    )
  and sanitize_mapping_type (tbl: IdentifierTable.t) (st: Parsetree.type_expr) :
    mapping or_variable option = 
    let add = IdentifierTable.add_identifier tbl in
    match st with
    | Parsetree.TypeInt loc 
    | Parsetree.TypeBool loc 
    | Parsetree.TypeFloat loc 
    | Parsetree.TypeTexture1D loc 
    | Parsetree.TypeTexture2D loc 
    | Parsetree.TypeTexture3D loc 
    | Parsetree.TypeTextureCube loc 
    | Parsetree.TypeNumber { loc; _ } 
    | Parsetree.TypeNumericData loc 
    | Parsetree.TypeMap { loc; _ } ->
      Output.output_error loc "invalid type argument to polymorphic type (expecting a mapping)";
      None
    | Parsetree.TypeHole l -> Some (Hole l)
    | Parsetree.TypeVariable { value; loc } -> let value = add value in Some (TyVariable {value;loc})
    | Parsetree.TypeProduct { value=(elems, name); loc } ->
      match name with
      | Parsetree.TypeMapTag loc  -> begin match elems with
          | [c1; c2; c3] ->
            let c1 = extract_type_map tbl loc extract_pure_data_type c1 in
            let c2 = extract_type_map tbl loc extract_pure_data_type c2 in
            let c3 = extract_type_map tbl loc extract_texture_data_type c3 in
            begin match c1,c2,c3 with
              | Some c1, Some c2, Some c3 -> Some (TyDirect (MkTyMapping {value=(c1,c2,c3); loc}))
              | _ -> None
            end
          | ls ->
            Output.output_error loc "invalid arity to map type (%d != 3)" (List.length ls);
            None
        end
      | _ -> 
        Output.output_error loc "invalid type argument to polymorphic type (expecting a mapping)";
        None
  and sanitize_type_expr tbl (ty: Parsetree.type_expr) =
    let add = IdentifierTable.add_identifier tbl in
    (* let simpl = sanitize_type_expr tbl  in *)
    let elems_to_non_scalar loc elems f  =
      elems_to_non_scalar tbl loc elems f (fun v -> TypeExpr (TyDirect (TyPure v))) TypeFail  in
    match ty with
    | Parsetree.TypeInt l -> TypeExpr (TyDirect (TyPure (TyScalar (TyInt l))))
    | Parsetree.TypeBool l -> TypeExpr (TyDirect (TyPure (TyScalar (TyBool l))))
    | Parsetree.TypeFloat l -> TypeExpr (TyDirect (TyPure (TyScalar (TyFloat l))))
    | Parsetree.TypeTexture1D l -> TypeExpr (TyDirect (TyTexture (TyTexture1D l)))
    | Parsetree.TypeTexture2D l -> TypeExpr (TyDirect (TyTexture (TyTexture2D l)))
    | Parsetree.TypeTexture3D l -> TypeExpr (TyDirect (TyTexture (TyTexture3D l)))
    | Parsetree.TypeTextureCube l -> TypeExpr (TyDirect (TyTexture (TyTextureCube l)))
    | Parsetree.TypeHole l -> TypeExpr (Hole l)
    | Parsetree.TypeVariable { value; loc } ->
      let value = add value in
      TypeExpr (TyVariable {value;loc})
    | Parsetree.TypeNumber { loc; _ } ->
      Output.output_error loc "raw numbers are not supported as types - perhaps you are missing a shader or drawable?";  TypeFail
    | Parsetree.TypeNumericData l ->
      TypeExpr (TyDirect (TyPure (TyNumericData l)))
    | Parsetree.TypeProduct {loc; value=(elems, name)} -> begin match name with
        | Parsetree.TypeMapTag _ ->
          Output.output_error loc "raw type maps are not a supported type";
          TypeFail
        | Parsetree.TypeVec2 l ->
          elems_to_non_scalar loc elems (fun ns -> TyVec2 {value=ns; loc=l})
        | Parsetree.TypeVec3 l ->
          elems_to_non_scalar loc elems (fun ns -> TyVec3 {value=ns; loc=l}) 
        | Parsetree.TypeVec4 l ->
          elems_to_non_scalar loc elems (fun ns -> TyVec4 {value=ns; loc=l}) 
        | Parsetree.TypeMat2 l ->
          elems_to_non_scalar loc elems (fun ns -> TyMat2 {value=ns; loc=l}) 
        | Parsetree.TypeMat3 l ->
          elems_to_non_scalar loc elems (fun ns -> TyMat3 {value=ns; loc=l}) 
        | Parsetree.TypeMat4 l ->
          elems_to_non_scalar loc elems (fun ns -> TyMat4 {value=ns; loc=l}) 
        | Parsetree.TypeList loc -> begin match elems with
            | [s] ->
              let ty_expr = sanitize_type_expr tbl s in
              TypeExpr (TyDirect (TyList ty_expr))
            | ls ->
              Output.output_error loc "invalid arity of arguments to list type (%d != 1)" (List.length ls);
              TypeFail
          end
        | Parsetree.TypeMulti _ ->
          begin match elems with
            | [s] ->
              let ty_expr = sanitize_scene_type tbl s in
              (match ty_expr with
               | None -> TypeFail
               | Some ty_expr -> TypeExpr (TyDirect (TyScene (TyMulti {value=ty_expr; loc}))))
            | ls ->
              Output.output_error loc "invalid arity of arguments to multi type (%d != 1)" (List.length ls);
              TypeFail
          end
        | Parsetree.TypeObject _ ->
          begin match elems with
            | [s1; s2] ->
              let s1 = sanitize_mapping_type tbl s1 in
              let s2 = sanitize_mapping_type tbl s2 in
              (match s1,s2 with
               | None, _
               | _, None -> TypeFail
               | Some s1, Some s2 -> TypeExpr (TyDirect (TyScene (TyObject {value=(s1, s2);loc}))))
            | ls ->
              Output.output_error loc "invalid arity of arguments to object type (%d != 2)" (List.length ls);
              TypeFail
          end
        | Parsetree.TypeModifier _ ->
          begin match elems with
            | [s1; s2] ->
              let s1 = sanitize_mapping_type tbl s1 in
              let s2 = sanitize_mapping_type tbl s2 in
              (match s1,s2 with
               | None, _
               | _, None -> TypeFail
               | Some s1, Some s2 -> TypeExpr (TyDirect (TyScene (TyModifier {value=(s1, s2);loc}))))
            | ls ->
              Output.output_error loc "invalid arity of arguments to modifier type (%d != 2)" (List.length ls);
              TypeFail
          end
        | Parsetree.TypeLight _ ->
          begin match elems with
            | [s1] ->
              let s1 = sanitize_mapping_type tbl s1 in
              (match s1 with
               | None -> TypeFail
               | Some s1 -> TypeExpr (TyDirect (TyScene (TyLight {value=s1;loc}))))
            | ls ->
              Output.output_error loc "invalid arity of arguments to light type (%d != 1)" (List.length ls);
              TypeFail
          end
        | Parsetree.TypeShader _ ->
          begin match elems with
            | [s1; s2] ->
              let s1 = sanitize_mapping_type tbl s1 in
              let s2 = sanitize_numeric_type tbl s2 in
              (match s1,s2 with
               | None, _
               | _, None -> TypeFail
               | Some s1, Some s2 -> TypeExpr (TyDirect (TyScene (TyShader {value=(s1, s2);loc}))))
            | ls ->
              Output.output_error loc "invalid arity of arguments to shader type (%d != 2)" (List.length ls);
              TypeFail
          end
        | Parsetree.TypeDrawable _ ->
          begin match elems with
            | [s1; s2] ->
              let s1 = sanitize_mapping_type tbl s1 in
              let s2 = sanitize_numeric_type tbl s2 in
              (match s1,s2 with
               | None, _
               | _, None -> TypeFail
               | Some s1, Some s2 -> TypeExpr (TyDirect (TyScene (TyDrawable {value=(s1, s2);loc}))))
            | ls ->
              Output.output_error loc "invalid arity of arguments to drawable type (%d != 2)" (List.length ls);
              TypeFail
          end
      end
    | Parsetree.TypeMap {  loc; _ } ->
      Output.output_error
        loc "raw mapping not supported as a valid type - perhaps you are missing a shader or drawable?";
      TypeFail
  and sanitize_numeric_type tbl st : int or_variable option =
    let add = IdentifierTable.add_identifier tbl in
    begin match st with
      | Parsetree.TypeInt loc
      | Parsetree.TypeBool loc
      | Parsetree.TypeFloat loc
      | Parsetree.TypeTexture1D loc
      | Parsetree.TypeTexture2D loc
      | Parsetree.TypeTexture3D loc
      | Parsetree.TypeTextureCube loc
      | Parsetree.TypeNumericData loc
      | Parsetree.TypeProduct {loc;_} 
      | Parsetree.TypeMap { loc; _ }  ->
        Output.output_error
          loc "invalid type argument to shader/drawable (expecting integer value or type variable)";
        None
      | Parsetree.TypeHole l -> Some (Hole l)
      | Parsetree.TypeNumber { value; _ } -> Some (TyDirect value)
      | Parsetree.TypeVariable { value; loc } -> let value = add value in Some (TyVariable {value;loc})
    end

  and sanitize_scene_type tbl (st: Parsetree.type_expr) : scene_type or_variable option =
    let add = IdentifierTable.add_identifier tbl in
    begin match st with
      | Parsetree.TypeInt loc
      | Parsetree.TypeBool loc
      | Parsetree.TypeFloat loc
      | Parsetree.TypeTexture1D loc
      | Parsetree.TypeTexture2D loc
      | Parsetree.TypeTexture3D loc
      | Parsetree.TypeTextureCube loc
      | Parsetree.TypeNumber { loc; _ }
      | Parsetree.TypeNumericData loc
      | Parsetree.TypeMap { loc; _ } ->
        Output.output_error
          loc "invalid type argument (expecting scene type)";
        None
      | Parsetree.TypeHole l -> Some (Hole l)
      | Parsetree.TypeVariable { value; loc } -> let value = add value in Some (TyVariable {value;loc})
      | Parsetree.TypeProduct { value=(elems, name); loc } -> (match name with
          | Parsetree.TypeVec2 _ 
          | Parsetree.TypeVec3 _ 
          | Parsetree.TypeVec4 _ 
          | Parsetree.TypeMat2 _ 
          | Parsetree.TypeMat3 _ 
          | Parsetree.TypeMat4 _ 
          | Parsetree.TypeList _
          | Parsetree.TypeMapTag _ ->
            Output.output_error
              loc "invalid type format for scene expression  (expecting one of object,modifier, light, shader,drawable or multi)";
            None
          | Parsetree.TypeMulti _ -> begin match elems with
              | [expr] -> (sanitize_scene_type tbl expr) |> Option.map ~f:(fun e -> TyDirect (TyMulti {value=e;loc}))
              | ls ->             
                Output.output_error
                  loc "invalid arity to multi type (%d != 1)" (List.length ls);
                None
            end
          | Parsetree.TypeShader _ ->
            begin match elems with
              | [s1; s2] ->
                let s1 = sanitize_mapping_type tbl s1 in
                let s2 = sanitize_numeric_type tbl s2 in
                (match s1,s2 with
                 | None, _
                 | _, None -> None
                 | Some s1, Some s2 -> Some (TyDirect (TyShader {value=(s1, s2);loc})))
              | ls ->
                Output.output_error loc "invalid arity of arguments to shader type (%d != 2)" (List.length ls);
                None
            end            
          | Parsetree.TypeObject _ ->
            begin match elems with
              | [s1; s2] ->
                let s1 = sanitize_mapping_type tbl s1 in
                let s2 = sanitize_mapping_type tbl s2 in
                (match s1,s2 with
                 | None, _
                 | _, None -> None
                 | Some s1, Some s2 -> Some (TyDirect (TyObject {value=(s1, s2);loc})))
              | ls ->
                Output.output_error loc "invalid arity of arguments to object type (%d != 2)" (List.length ls);
                None
            end
          | Parsetree.TypeModifier _ ->
            begin match elems with
              | [s1; s2] ->
                let s1 = sanitize_mapping_type tbl s1 in
                let s2 = sanitize_mapping_type tbl s2 in
                (match s1,s2 with
                 | None, _
                 | _, None -> None
                 | Some s1, Some s2 -> Some (TyDirect (TyModifier {value=(s1, s2);loc})))
              | ls ->
                Output.output_error loc "invalid arity of arguments to modifier type (%d != 2)" (List.length ls);
                None
            end            
          | Parsetree.TypeLight _ ->
            begin match elems with
              | [s1] ->
                let s1 = sanitize_mapping_type tbl s1 in
                (match s1 with
                 | None -> None
                 | Some s1 -> Some (TyDirect (TyLight {value=s1;loc})))
              | ls ->
                Output.output_error loc "invalid arity of arguments to light type (%d != 1)" (List.length ls);
                None
            end            
          | Parsetree.TypeDrawable _ ->
            begin match elems with
              | [s1; s2] ->
                let s1 = sanitize_mapping_type tbl s1 in
                let s2 = sanitize_numeric_type tbl s2 in
                (match s1,s2 with
                 | None, _
                 | _, None -> None
                 | Some s1, Some s2 -> Some (TyDirect (TyDrawable {value=(s1, s2);loc})))
              | ls ->
                Output.output_error loc "invalid arity of arguments to drawable type (%d != 2)" (List.length ls);
                None
            end
        )
    end
  and sanitize_data tbl (Parsetree.DataComponent { value; loc }: Parsetree.data) =
    let add = IdentifierTable.add_identifier tbl in
    match value with
    | (Model, elem) -> begin match elem with
        | [None, ArgumentString { value; _ } ]
        |  [Some {value="filename";_}, ArgumentString { value; _ } ] ->
          Model {value; loc}
        | [Some {value;_}, _ ] ->
          Output.output_error loc
            "invalid arguments to model data (unknown keyword argument %s - only supported keyword argument is filename)"
            value;
          DataFail
        | l ->
          Output.output_error loc
            "invalid arity to model data (%d != 1)" (List.length l);
          DataFail
      end
    | (Data, ls) -> begin
        match ls with
        | [] ->
          Output.output_error loc
            "invalid arity to data operation (0 < 1)";
          DataFail
        | ls ->
          let ls =
            List.map ls ~f:(fun (name, arg) ->
                match name, arg with
                | Some name, ArgumentExpression e ->
                  let name = map_loc ~f:add name in
                  let expr = sanitize_expr tbl e in
                  Some (name,expr)
                | _ ->
                  Output.output_error loc "invalid arguments to data element (all arguments must be labelled)";
                  None
              ) in
          match Option.all ls with
          | None -> DataFail
          | Some ls -> Data {value=ls; loc}
      end
    | (Screen, ls) ->
      let result =
        List.fold ls ~init:(Some (None, None)) ~f:(fun acc (name, arg) ->
            match acc, arg with
            | None, _ ->   None
            | _, ArgumentString _ ->
              Output.output_error loc "string literals are not supported arguments repeated screen objects";
              None
            | Some (width, height), ArgumentExpression arg ->
              let arg = sanitize_expr tbl arg in
              match name with
              | Some { value="width"; loc } -> begin
                  match width with
                  | None -> Some (Some arg, height)
                  | Some _ ->
                    Output.output_error loc "argument width repeated to screen object";
                    None
                end
              | Some { value="height"; loc=_ } -> begin
                  match height with
                  | None -> Some (width, Some arg)
                  | Some _ ->
                    Output.output_error loc "argument height repeated to screen object";
                    None
                end
              | Some { value; _ } -> begin
                  Output.output_error loc "unknown argument %s passed to screen object" value;
                  None
                end
              | None -> match width, height with
                | None, None -> Some (Some arg, None)
                | Some v, None -> Some (Some v, Some arg)
                | _ ->
                  Output.output_error loc "invalid arity arguments passed to screen object";
                  None
          )
      in
      begin match result with
        | None -> DataFail
        | Some (width, height) -> Screen {width;height;loc}
      end
  and sanitize_modifier tbl (modifier: Parsetree.modifier) =
    let add = IdentifierTable.add_identifier tbl in
    match modifier with
    | Parsetree.ModifierReference { value; loc } -> begin
        match value with
        | [o_name, arg] -> begin
            match o_name, arg with
            | Some { value; loc }, _ ->
              Output.output_error loc "unknown keyword argument %s passed to modifier" value;
              ModifierFail
            | None, Parsetree.SceneString _ ->
              Output.output_error loc "string literals not supported as arguments to modifier";
              ModifierFail
            | None, Parsetree.SceneDataExpression _ ->
              Output.output_error loc "data expressions not supported as arguments to modifier";
              ModifierFail
            | (None, Parsetree.SceneInlineExpression (Parsetree.SceneExpr_variable var)) ->
              let var = map_loc ~f:add var in
              ModifierReference (DirectReference var)
            | (None, Parsetree.SceneInlineExpression (Parsetree.SceneExpr_modifier modifier)) ->
              sanitize_modifier tbl modifier
            | (None, Parsetree.SceneInlineExpression (Parsetree.SceneExpr_macro_call call)) -> begin
                match sanitize_macro_args tbl call with
                | None -> ModifierFail
                | Some (name, args) ->  ModifierReference (IndirectReference (name, args))
              end
            | (None, Parsetree.SceneInlineExpression _) ->
              Output.output_error loc "data expressions not supported as arguments to modifier";
              ModifierFail
          end
        | ls -> 
          Output.output_error loc "invalid arity of arguments for modifier (%d != 1)" (List.length ls);
          ModifierFail
      end
    | Parsetree.BasicModifier { value=(form, args); loc } -> begin
        match form with
        | Parsetree.Texture -> sanitize_texture_args tbl loc args
        | Parsetree.Uniform -> sanitize_uniform_args tbl loc args
        | Parsetree.Attribute -> sanitize_attribute_args tbl loc args
        | Parsetree.Translate -> sanitize_translate_args tbl loc args
        | Parsetree.Scale -> sanitize_scale_args tbl loc args
        | Parsetree.Rotate -> sanitize_rotate_args tbl loc args
        | Parsetree.DrawOptions -> sanitize_draw_args tbl loc args
      end
    | Parsetree.LightModifier { value=ls; loc } -> begin
        match ls with
        | [Exp_variable light_name; Exp_variable uniform_name; e2] ->
          let light_name = map_loc ~f:add light_name in
          let uniform_name = map_loc ~f:add uniform_name in
          let e2 = sanitize_expr tbl e2 in
          LightUniform (light_name, uniform_name, e2)
        | [_; Exp_variable _; _] ->
          Output.output_error loc "first argument to light uniform should be an identifier for the light tag";
          ModifierFail
        | [Exp_variable _; _; _] ->
          Output.output_error
            loc "second argument to light uniform should be an identifier for the uniform name";
          ModifierFail
        | _ ->
          Output.output_error loc "invalid arity of arguments for light uniform";
          ModifierFail
      end
    | Parsetree.RedirectingModifier { value=(mapping, drawable); loc } ->
      let (textures,depth,stencil,failed) = List.fold mapping ~init:(None,false,false,false)
          ~f:(fun (textures, depth, stencil, failed) (o_name, e) ->
              if failed then (textures,depth,stencil,failed) else begin
                match o_name, e with
                | None, (Exp_variable {value="stencil"; loc}) -> begin
                    match stencil with
                    | false ->
                      (textures, depth, true, failed)
                    | true ->
                      Output.output_error
                        loc "repeated stencil targets passed to redirection modifier";
                      (textures, depth, stencil, true)
                  end
                | None, (Exp_variable {value="depth"; loc}) -> begin
                    match depth with
                    | false ->
                      (textures, true, stencil, failed)
                    | true ->
                      Output.output_error
                        loc "repeated depth targets passed to redirection modifier";
                      (textures, depth, stencil, true)
                  end
                | Some { value="into"; loc }, (Exp_variable texture) -> begin
                    match textures with
                    | None ->
                      let texture = map_loc ~f:add texture in
                      (Some [texture], depth, stencil, failed)
                    | Some _ ->
                      Output.output_error
                        loc "repeated redirection targets passed to redirection modifier";
                      (textures, depth, stencil, true)
                  end
                | Some { value="into"; loc }, (Exp_list {value=ls; _}) -> begin
                    let ls = List.map ~f:(fun v ->
                        match v with
                        | Exp_variable v -> Some v
                        | _ -> None
                      ) ls in
                    let ls = Option.all ls in
                    begin match ls with
                      | None ->
                        Output.output_error
                          loc "invalid redirection targets specification";
                        (textures, depth, stencil, true)
                      | Some ls ->
                        let contains_dup =
                          List.contains_dup ~compare:(fun a b -> String.compare a.value b.value) ls in
                        if contains_dup then begin
                          Output.output_error
                            loc "invalid redirection targets specification - targets are not unique";
                          (textures, depth, stencil, true)
                        end else begin
                          let ls = List.map ~f:(map_loc ~f:add) ls in
                          match textures with
                          | None -> (Some ls, depth, stencil, failed)
                          | Some _ ->
                            Output.output_error
                              loc "repeated redirection targets passed to redirection modifier";
                            (textures, depth, stencil, true)
                        end
                    end
                  end
                | (Some {value=target; loc }, _) ->
                        Output.output_error
                          loc "unknown keyword argument %s to redirection - redirection targets must be specified by into" target;
                  (textures, depth, stencil, true)
                | (None, Exp_variable { value=target; _ }) ->
                        Output.output_error
                          loc "unknown argument %s to redirection (must be one of into, depth or stencil)" target;

                  (textures, depth, stencil, true)


                | (None, _) ->
                        Output.output_error
                          loc "unknown argument to redirection (must be one of into, depth or stencil)";

                  (textures, depth, stencil, true)
              end
            ) in
      let textures = Option.value textures ~default:[] in
      if failed then ModifierFail else begin
        let drawable = sanitize_drawable tbl drawable in
        Redirecting {textures;depth;stencil;drawable; loc}
      end
  and sanitize_drawable tbl drawable =
    let add = IdentifierTable.add_identifier tbl in
    match drawable with
    | Parsetree.OverlayingDrawable drawable ->
      OverlayingDrawable (map_loc ~f:(sanitize_drawable tbl) drawable)
    | Parsetree.DrawableShader { value=(shader,obj); loc } ->
      let shader = sanitize_shader tbl shader in
      let obj = sanitize_object tbl obj in
      DrawableShader {value=(shader,obj); loc}
    | Parsetree.DrawableReference { value=args; loc } -> begin
        match args with
        | [o_name, arg] -> begin
            match o_name, arg with
            | Some { value; loc }, _ ->
              Output.output_error loc "unknown keyword argument %s passed to drawable" value;
              DrawableFail
            | None, Parsetree.SceneString _ ->
              Output.output_error loc "string literals not supported as arguments to drawable";
              DrawableFail
            | None, Parsetree.SceneDataExpression _ ->
              Output.output_error loc "data expressions not supported as arguments to drawable";
              DrawableFail
            | (None, Parsetree.SceneInlineExpression (Parsetree.SceneExpr_variable var)) ->
              let var = map_loc ~f:add var in
              DrawableReference (DirectReference var)
            | (None, Parsetree.SceneInlineExpression (Parsetree.SceneExpr_drawable drawable)) ->
              sanitize_drawable tbl drawable
            | (None, Parsetree.SceneInlineExpression (Parsetree.SceneExpr_macro_call call)) -> begin
                match sanitize_macro_args tbl call with
                | None -> DrawableFail
                | Some (name, args) ->  DrawableReference (IndirectReference (name, args))
              end
            | (None, Parsetree.SceneInlineExpression _) ->
              Output.output_error loc "unsupported macro argument passed drawable";
              DrawableFail
          end
        | ls -> 
          Output.output_error loc "invalid arity of arguments for drawable call (%d != 1)" (List.length ls);
          DrawableFail
      end
  and sanitize_shader tbl (Parsetree.BasicShader { value=args; loc }: Parsetree.shader) =
    let add = IdentifierTable.add_identifier tbl in
    let (vs, fs, gs, rf, _, failed) =
      List.fold args
        ~init:(None, None, None, None, Some 0, false)
        ~f:(fun (vs, fs, gs, rf, ind, failed) v ->
            let fail  = (None, None, None, None, None, true) in
            if failed then fail else begin
              match v with
              | (None, SceneString ({  loc; _ } as s)) -> begin match ind with
                  | Some 0 -> begin match vs with
                      | Some _ ->
                        Output.output_error loc "vertex shader argument repeated twice";
                        fail
                      | None ->
                        (Some s, fs, gs, rf, Some 1, false)
                    end
                  | Some 1 -> begin match fs with
                      | Some _ ->
                        Output.output_error loc "fragment shader argument repeated twice";
                        fail
                      | None ->
                        (vs, Some s, gs, rf, Some 2, false)
                    end
                  | Some 2 -> begin match fs with
                      | Some _ ->
                        Output.output_error loc "geometry shader argument repeated twice";
                        fail
                      | None ->
                        (vs, fs, Some s, rf, Some 3, false)
                    end
                  | Some _ ->
                    Output.output_error loc "too many arguments to shader";
                    fail
                  | None ->
                    Output.output_error loc "mixing string literals and variables are not supported by the shader operator ";
                    fail
                end
              | Some {value="vertex_shader"; loc}, SceneString s -> begin match vs,ind with
                  | Some _, _ ->
                    Output.output_error
                      loc "vertex shader argument repeated to shader operator";
                    fail
                  | _, None -> 
                    Output.output_error
                      loc "mixing string literals and shaders are not supported by the shader";
                    fail
                  | None, _ ->
                    (Some s, fs, gs, rf, ind, failed)
                end
              | Some {value="vertex_shader"; loc}, _ ->
                Output.output_error loc "vertex shader argument must be a string literal"; fail

              | Some {value="fragment_shader"; loc}, SceneString s -> begin match fs,ind with
                  | Some _,_ ->
                    Output.output_error
                      loc "fragment shader argument repeated to shader operator";
                    fail
                  | _, None -> 
                    Output.output_error
                      loc "mixing string literals and shaders are not supported by the shader";
                    fail
                  | None, _ ->
                    (vs, Some s, gs, rf, ind, failed)
                end
              | Some {value="fragment_shader"; loc}, _ ->
                Output.output_error loc "fragment shader argument must be a string literal"; fail

              | Some {value="geometry_shader"; loc}, SceneString s -> begin match gs,ind with
                  | Some _, _ ->
                    Output.output_error
                      loc "geometry shader argument repeated to shader operator";
                    fail
                  | _, None -> 
                    Output.output_error
                      loc "mixing string literals and shaders are not supported by the shader";
                    fail
                  | None, _ ->
                    (vs, fs, Some s, rf, ind, failed)
                end
              | Some {value="geometry_shader"; loc}, _ ->
                Output.output_error loc "geometry shader argument must be a string literal"; fail
              | Some {value; loc}, _ ->
                Output.output_error loc "unknown keyword argument %s passed to shader" value;
                fail
              | None, SceneInlineExpression expr -> begin match ind with
                  | Some 0 -> begin match expr with
                      | Parsetree.SceneExpr_variable name ->
                        let name = map_loc ~f:add name in
                        let rf = ShaderReference (DirectReference name) in
                        (vs, fs, gs, Some rf, None, failed)
                      | Parsetree.SceneExpr_macro_call call -> begin
                          match sanitize_macro_args tbl call with
                          | None -> fail
                          | Some (name, args) ->
                            let rf = ShaderReference (IndirectReference (name, args)) in
                            (vs, fs, gs, Some rf, None, failed)
                        end
                      | Parsetree.SceneExpr_shader s ->
                        let rf = sanitize_shader tbl s in
                        (vs, fs, gs, Some rf, None, failed)
                      | _ -> 
                        Output.output_error
                          loc "argument to shader must be either a string literal or macro variable/function (type casts not supported)";
                        fail
                    end
                  | Some _ ->
                    Output.output_error
                      loc "mixing string literals and macro variables not supported for shader operation";
                    fail
                  | None ->
                    Output.output_error
                      loc "invalid arity of arguments to shader (only either 1-3 string literals or 1 macro argument supported)";
                    fail
                end
              | _, SceneDataExpression _ ->
                Output.output_error loc "data expressions not \
                                         supported as arguments to \
                                         shader (did you forget to \
                                         prefix a macro reference with \
                                         @)";
                fail
            end
          ) in
    match failed, rf, vs with
    | _, None, None ->
      Output.output_error loc "a vertex shader must be provided in order to construct a shader";
      ShaderFail
    | true, _, _ -> ShaderFail
    | _, Some sd, _ -> sd
    | _, None, Some vs ->
      Shader { vertex_shader = vs; fragment_shader = fs; geometry_shader= gs; }
  and sanitize_base_object tbl (obj: Parsetree.base_object) : [`Left of base_object | `Right of obj] =
    let add = IdentifierTable.add_identifier tbl in
    match obj with
    | Parsetree.ObjectData data -> `Left (ObjectData (sanitize_data tbl data))
    | Parsetree.ObjectReference { value; loc } ->
      begin
        match value with
        | [o_name, arg] -> begin
            match o_name, arg with
            | Some { value; loc }, _ ->
              Output.output_error loc "unknown keyword argument %s passed to object" value;
              `Left ObjectFail
            | None, Parsetree.SceneString _ ->
              Output.output_error loc "string literals not supported as arguments to object";
              `Left ObjectFail
            | None, Parsetree.SceneDataExpression _ ->
              Output.output_error loc "data expressions not supported as arguments to object";
              `Left ObjectFail
            | (None, Parsetree.SceneInlineExpression (Parsetree.SceneExpr_variable var)) ->
              let var = map_loc ~f:add var in
              `Left (ObjectReference (DirectReference var))
            | (None, Parsetree.SceneInlineExpression (Parsetree.SceneExpr_object obj)) ->
              `Right (sanitize_object tbl obj)
            | (None, Parsetree.SceneInlineExpression (Parsetree.SceneExpr_macro_call call)) -> begin
                match sanitize_macro_args tbl call with
                | None -> `Left ObjectFail
                | Some (name, args) ->  `Left (ObjectReference (IndirectReference (name, args)))
              end
            | (None, Parsetree.SceneInlineExpression _) ->
              Output.output_error loc "data expressions not supported as arguments to object";
          `Left ObjectFail
          end
        | ls -> 
          Output.output_error loc "invalid arity of arguments for object (%d != 1)" (List.length ls);
          `Left ObjectFail
      end
    | Parsetree.Light { value; loc } -> begin
        match value with
        | [None, ArgumentExpression (Exp_variable txt);
           None, ArgumentExpression e2;
           Some ({value="position"; _}), ArgumentExpression e1;]
        | [None, ArgumentExpression (Exp_variable txt);
           Some {value="size"; _}, ArgumentExpression e2;
           None, ArgumentExpression e1]
        | [None, ArgumentExpression (Exp_variable txt);
           Some {value="size"; _}, ArgumentExpression e2;
           Some {value="position"; _}, ArgumentExpression e1]
        | [None, ArgumentExpression (Exp_variable txt);
           Some ({value="position"; _}), ArgumentExpression e1;
           None, ArgumentExpression e2]
        | [None, ArgumentExpression (Exp_variable txt);
           None, ArgumentExpression e1;
           Some {value="size"; _}, ArgumentExpression e2]
        | [None, ArgumentExpression (Exp_variable txt);
           Some {value="position"; _}, ArgumentExpression e1;
           Some {value="size"; _}, ArgumentExpression e2]
        | [None, ArgumentExpression (Exp_variable txt);
           None, ArgumentExpression e1;
           None, ArgumentExpression e2] ->
          let position = sanitize_expr tbl e1 in
          let size = sanitize_expr tbl e2 in
          let name = map_loc ~f:add txt in
          `Left (Light { name; position; size})
        | [Some _, _;
           _, _;
           _, _] ->
          Output.output_error
            loc "arguments to a light must begin with an identifier, without a keyword argument";
          `Left ObjectFail
        | [_,   _; _, _; _, ArgumentString _]
        | [_,   _; _, ArgumentString _; _, _]
        | [_,  ArgumentString _; _, _; _, _]  ->
          Output.output_error
            loc "string literals are not supported as arguments to lights";
          `Left ObjectFail
        | _ ->
          Output.output_error
            loc "invalid number of arguments to light constructor (expecting 3 arguments: light_tag, position and size)";
          `Left ObjectFail
      end
    | Parsetree.LightData {value=args; loc} -> begin match args with
        | [Exp_variable name] ->
          let name = map_loc ~f:add name in
          (* let value = map_loc ~f:add value in *)
          (* let value = sanitize_expr tbl e in *)
          `Left (LightData { name }) 
        (* | [Exp_variable _ ] ->
         *   Output.output_error
         *     loc "second argument to light data must be name of the variable being used";
         *   `Left ObjectFail *)
        (* | [_ ; Exp_variable _] ->
         *   Output.output_error
         *     loc "first argument to light data must be the tag of the expression";
         *   `Left ObjectFail *)
        | _ as ls ->
          Output.output_error
            loc "invalid arity of arguments to light data operator (%d != 1)" (List.length ls);
          `Left ObjectFail
      end
    | Parsetree.CompoundObject { value; loc } ->
      begin match value with
        | [] ->
          Output.output_error
            loc "compound expressions must contain at least expression";
          `Left ObjectFail
        | h :: t ->
          `Left (CompoundObject {value=(
              sanitize_obj_expr tbl h,
              (List.map ~f:(sanitize_obj_expr tbl) t)); loc})
      end
  and sanitize_object tbl (Parsetree.Object { value=(modifiers, base_obj); loc }: Parsetree.obj) =
    let modifiers = List.map modifiers ~f:(sanitize_modifier tbl) in
    let base_obj = sanitize_base_object tbl base_obj in
    (match base_obj with
     | `Right Object (modifiers2, base_obj, loc) -> Object(modifiers @ modifiers2, base_obj, loc)
     | `Left base_obj -> Object (modifiers, base_obj, loc))
  and sanitize_obj_expr tbl (arg: Parsetree.scene_expr) =
    let add = IdentifierTable.add_identifier tbl in
    match arg with
    | Parsetree.SceneExpr_object obj -> PlainObject (sanitize_object tbl obj)
    | Parsetree.SceneExpr_with_type {value=(expr, _); _} -> sanitize_obj_expr tbl expr
    | Parsetree.SceneExpr_if_then_else { value=(cond, e1, o_e2); _ } ->
      let cond = sanitize_expr tbl cond in
      let e1 = sanitize_obj_expr tbl e1 in
      let o_e2 = Option.map ~f:(sanitize_obj_expr tbl) o_e2 in
      ObjectIfThenElse (cond, e1, o_e2)
    | Parsetree.SceneExpr_for { value=(ident, from_start, to_end, body); loc } ->
      let ident = map_loc ~f:add ident in
      let from_start = sanitize_expr tbl from_start in
      let to_end = Option.map ~f:(sanitize_expr tbl) to_end in
      let body = List.map ~f:(sanitize_obj_expr tbl) body in
      begin match body with
        | [] ->
          Output.output_error
            loc "invalid object expression - for loops must have at least one expression within them";
          ObjectExprFail
        | h :: t ->
          ObjectFor {value=(ident, from_start, to_end, (h,t)); loc}
      end
    | Parsetree.SceneExpr_variable { loc; _ }
    | Parsetree.SceneExpr_shader Parsetree.BasicShader { loc; _ }
    | Parsetree.SceneExpr_drawable (Parsetree.OverlayingDrawable {  loc; _ })
    | Parsetree.SceneExpr_drawable (Parsetree.DrawableReference { loc; _ })
    | Parsetree.SceneExpr_drawable (Parsetree.DrawableShader {  loc; _ })
    | Parsetree.SceneExpr_modifier (Parsetree.ModifierReference { loc; _ })
    | Parsetree.SceneExpr_modifier (Parsetree.BasicModifier { loc; _ })
    | Parsetree.SceneExpr_modifier (Parsetree.LightModifier {  loc; _ })
    | Parsetree.SceneExpr_modifier (Parsetree.RedirectingModifier {  loc; _ })
    | Parsetree.SceneExpr_macro_call { loc; _} ->
      Output.output_error
        loc "invalid non-object expression inside compound object expression";
      ObjectExprFail
  and sanitize_texture_args tbl loc args =
    let add = IdentifierTable.add_identifier tbl in
    match args with
    | (None, ArgumentExpression (Exp_variable name)) ::
      (None, ArgumentString filename) :: args ->
      let name = map_loc ~f:add name in
      let other_files,args = List.split_while args ~f:(fun (kwarg, _) -> Option.is_none kwarg) in

      let other_files = List.map other_files ~f:(fun (_, data) ->
          (match data with
           | Parsetree.ArgumentString value -> Some value
           | Parsetree.ArgumentExpression (Parsetree.Exp_bool { loc; _ })
           | Parsetree.ArgumentExpression (Parsetree.Exp_number {loc; _})
           | Parsetree.ArgumentExpression (Parsetree.Exp_variable {loc; _})
           | Parsetree.ArgumentExpression (Parsetree.Exp_binop {loc; _})
           | Parsetree.ArgumentExpression (Parsetree.Exp_preop {loc; _})
           | Parsetree.ArgumentExpression (Parsetree.Exp_index {loc; _})
           | Parsetree.ArgumentExpression (Parsetree.Exp_vector {loc; _})
           | Parsetree.ArgumentExpression (Parsetree.Exp_list {loc; _})
           | Parsetree.ArgumentExpression (Parsetree.Exp_list_comprehension {loc; _})
           | Parsetree.ArgumentExpression (Parsetree.Exp_function_call {loc; _})
           | Parsetree.ArgumentExpression (Parsetree.Exp_if_then_else {loc; _})
           | Parsetree.ArgumentExpression (Parsetree.Exp_with_type {loc;_}) ->
             Output.output_error loc "texture files passed to texture must be strings";
             None
        )) in


      let args = List.map ~f:(fun (keyword, arg) ->
          match keyword, arg with
          | None, _ ->
            Output.output_error loc "extra texture arguments must all have keyword specifiers";
            None
          | Some { value="texture_base_level"; _ }, ArgumentExpression e
            -> Some (TextureBaseLevel (sanitize_expr tbl e))
          | Some { value="texture_max_level"; _ }, ArgumentExpression e
            -> Some (TextureMaxLevel (sanitize_expr tbl e))
          | Some { value="texture_border_color"; _ }, ArgumentExpression e
            -> Some (TextureBorderColor (sanitize_expr tbl e))
          | Some { value="wrap_s"; _ }, ArgumentExpression (Exp_variable {value="repeat"; _})
            -> Some (WrapS Repeat)
          | Some { value="wrap_s"; _ }, ArgumentExpression (Exp_variable {value="mirrored_repeat"; _})
            -> Some (WrapS MirroredRepeat)
          | Some { value="wrap_s"; _ }, ArgumentExpression (Exp_variable {value="clamp_to_edge"; _})
            -> Some (WrapS ClampToEdge)
          | Some { value="wrap_s"; _ }, ArgumentExpression (Exp_variable {value="clamp_to_border"; _})
            -> Some (WrapS ClampToBorder)

          | Some { value="wrap_t"; _ }, ArgumentExpression (Exp_variable {value="repeat"; _})
            -> Some (WrapT Repeat)
          | Some { value="wrap_t"; _ }, ArgumentExpression (Exp_variable {value="mirrored_repeat"; _})
            -> Some (WrapT MirroredRepeat)
          | Some { value="wrap_t"; _ }, ArgumentExpression (Exp_variable {value="clamp_to_edge"; _})
            -> Some (WrapT ClampToEdge)
          | Some { value="wrap_t"; _ }, ArgumentExpression (Exp_variable {value="clamp_to_border"; _})
            -> Some (WrapT ClampToBorder)
          | Some { value="min_filter"; _ }, ArgumentExpression (Exp_variable {value="nearest"; _})
            -> Some (MinFilter Nearest)
          | Some { value="min_filter"; _ }, ArgumentExpression (Exp_variable {value="linear"; _})
            -> Some (MinFilter Linear)
          | Some { value="min_filter"; _ }, ArgumentExpression (Exp_variable {value="nearest_mipmap_nearest"; _})
            -> Some (MinFilter NearestMipmapNearest)
          | Some { value="min_filter"; _ }, ArgumentExpression (Exp_variable {value="nearest_mipmap_linear"; _})
            -> Some (MinFilter NearestMipmapLinear)
          | Some { value="min_filter"; _ }, ArgumentExpression (Exp_variable {value="linear_mipmap_nearest"; _})
            -> Some (MinFilter LinearMipmapNearest)
          | Some { value="min_filter"; _ }, ArgumentExpression (Exp_variable {value="linear_mipmap_linear"; _})
            -> Some (MinFilter LinearMipmapLinear)

          | Some { value="mag_filter"; _ }, ArgumentExpression (Exp_variable {value="nearest"; _})
            -> Some (MagFilter Nearest)
          | Some { value="mag_filter"; _ }, ArgumentExpression (Exp_variable {value="linear"; _})
            -> Some (MagFilter Linear)
          | Some { value="mag_filter"; _ }, ArgumentExpression (Exp_variable {value="nearest_mipmap_nearest"; _})
            -> Some (MagFilter NearestMipmapNearest)
          | Some { value="mag_filter"; _ }, ArgumentExpression (Exp_variable {value="nearest_mipmap_linear"; _})
            -> Some (MagFilter NearestMipmapLinear)
          | Some { value="mag_filter"; _ }, ArgumentExpression (Exp_variable {value="linear_mipmap_nearest"; _})
            -> Some (MagFilter LinearMipmapNearest)
          | Some { value="mag_filter"; _ }, ArgumentExpression (Exp_variable {value="linear_mipmap_linear"; _})
            -> Some (MagFilter LinearMipmapLinear)

          | Some {loc; value=kword}, ArgumentExpression (Exp_variable {value; _}) ->
            Output.output_error loc "unknown texture option %s = %s" kword value;
            None
          | Some {loc; value=kword}, _ ->
            Output.output_error loc "invalid texture option %s type" kword;
            None
        ) args in
      begin
        match Option.all other_files, Option.all args with
        | None, _
        | _, None -> ModifierFail
        | Some other_files, Some args ->
          Texture (name, filename :: other_files, args)
      end
    | (Some _, _) :: _
    | (None, _) :: (Some _, _) :: _ ->
      Output.output_error loc "first two parameters of a texture modifier must be the name and filename (without keywords)";
      ModifierFail
    | (None, ArgumentString _) :: _
    | (None, ArgumentExpression (Exp_variable _)) ::
      (None, ArgumentExpression _) :: _
    | (None, ArgumentExpression _) :: _ 
    | [] ->
      Output.output_error loc "first two parameters of a texture modifier must be the name (an identifier) and filename (a string literal)";
      ModifierFail
  and sanitize_uniform_args tbl loc args =
    let add = IdentifierTable.add_identifier tbl in
    match args with
    | [Some _, _; None, _]
    | [None, _; Some _, _ ]
    | [Some _, _; Some _, _ ] ->
      Output.output_error loc "uniform parameters do not support keyword arguments";
      ModifierFail
    | [None, ArgumentExpression (Exp_variable name); None, ArgumentExpression e] ->
      let name = map_loc ~f:add name in
      let expr = sanitize_expr tbl e in
      Uniform (name, expr)
    | [None, ArgumentExpression (Exp_variable _); _] ->
      Output.output_error loc "uniform parameters do not support string literals";
      ModifierFail
    | [None, ArgumentExpression _ ; _] ->
      Output.output_error loc "first argument to uniform parameter must be the name of the uniform slot";
      ModifierFail
    | ls ->
      Output.output_error loc "invalid arity passed to uniform (%d != 2)" (List.length ls);
      ModifierFail
  and sanitize_attribute_args tbl loc args =
    let add = IdentifierTable.add_identifier tbl in
    match args with
    | [Some _, _; None, _]
    | [None, _; Some _, _ ]
    | [Some _, _; Some _, _ ] ->
      Output.output_error loc "attribute parameters do not support keyword arguments";
      ModifierFail
    | [None, ArgumentExpression (Exp_variable name); None, ArgumentExpression e] ->
      let name = map_loc ~f:add name in
      let expr = sanitize_expr tbl e in
      Attribute (name, expr)
    | [None, ArgumentExpression (Exp_variable _); _] ->
      Output.output_error loc "attribute parameters do not support string literals";
      ModifierFail
    | [None, ArgumentExpression _ ; _] ->
      Output.output_error loc "first argument to attribute parameter must be the name of the attribute slot";
      ModifierFail
    | ls ->
      Output.output_error loc "invalid arity passed to attribute (%d != 2)" (List.length ls);
      ModifierFail
  and sanitize_dimension_args name f tbl loc args =
    match args with
    | [Some { value="x"; loc }, Parsetree.ArgumentExpression e] ->
      f (DimensionXZY {x = Some (sanitize_expr tbl e); y=None; z=None; loc})
    | [Some { value="y"; loc }, ArgumentExpression e] ->
      f (DimensionXZY {y = Some (sanitize_expr tbl e); x=None; z=None; loc})
    | [Some { value="z"; loc }, ArgumentExpression e] ->
      f (DimensionXZY {z = Some (sanitize_expr tbl e); x=None; y=None; loc})
    | [Some { value="vector"; _ }, ArgumentExpression e] ->
      f (DimensionVector (sanitize_expr tbl e))
    | [Some { value; _ }, _] ->
      Output.output_error loc "unknown keyword argument %s passed to %s modifier" value name;
      ModifierFail
    | [None, Parsetree.ArgumentExpression e] -> f (DimensionVector (sanitize_expr tbl e))
    | [None, _] ->
      Output.output_error loc "%s modifier does not support string literal parameters." name;
      ModifierFail
    | ls ->
      let (x,y,z,failed, _all_empty) =
        List.fold ls ~init:(None, None, None, false, Some 0) ~f:(fun (x,y,z,failed, all_empty) elem ->
            if failed then (x,y,z,failed, all_empty) else begin
              match elem, all_empty with
              | (_, ArgumentString _),_ ->
                Output.output_error loc "%s modifier does not support string literals" name;
                (x,y,z, true, all_empty)
              | (None, ArgumentExpression e),_ -> begin
                  let e = Some (sanitize_expr tbl e) in
                  match all_empty with
                  | Some 0 -> (e, y, z, failed, Some 1)
                  | Some 1 -> (x, e, z, failed, Some 2)
                  | Some 2 -> (x, y, e, failed, Some 3)
                  | Some _ ->
                    Output.output_error loc "too many arguments passed to %s modifier" name;
                    (x,y,z, true, all_empty)
                  | None ->
                    Output.output_error loc "%s modifier does not support mixing positional and keyword arguments"
                      name;
                    (x,y,z, true, all_empty)
                end
              | (_,_), Some i when i >= 1 -> 
                Output.output_error loc "%s modifier does not support mixing positional and keyword arguments"
                  name;
                (x,y,z, true, all_empty)
              | (Some { value="x"; loc }, ArgumentExpression e),_ -> begin match x with
                  | None -> (Some (sanitize_expr tbl e), y, z, failed, None)
                  | Some _ ->
                    Output.output_error loc "repeated argument x to %s modifier" name;
                    (x,y,z, true, None)
                end
              | (Some { value="y"; loc }, ArgumentExpression e), _ -> begin match y with
                  | None -> (x, Some (sanitize_expr tbl e), z, failed, None)
                  | Some _ ->
                    Output.output_error loc "repeated argument y to %s modifier" name;
                    (x,y,z, true, None)
                end
              | (Some { value="z"; loc }, ArgumentExpression e), _ -> begin match z with
                  | None -> (x, y, Some (sanitize_expr tbl e), failed, None)
                  | Some _ ->
                    Output.output_error loc "repeated argument z to %s modifier" name;
                    (x,y,z, true, None)
                end
              | (Some {value; _}, _), _  ->
                Output.output_error loc "unknown keyword argument %s to %s modifier" value name;
                (x,y,z, true, None)
            end) in
      if failed then
        ModifierFail
      else f (DimensionXZY {x;y;z; loc})
  and sanitize_translate_args  tbl loc args =
    sanitize_dimension_args "translate" (fun vc -> Translate vc) tbl loc args
  and sanitize_scale_args tbl loc args =
    sanitize_dimension_args "scale" (fun vc -> Scale vc) tbl loc args
  and sanitize_rotate_args tbl loc args =
    match args with
    | [Some { value="x"; loc }, ArgumentExpression e] -> Rotate (RotateAngle {
        x = Some (sanitize_expr tbl e); y=None; z=None; vector=None; loc;
      })
    | [Some { value="y"; loc }, ArgumentExpression e] -> Rotate (RotateAngle {
        y = Some (sanitize_expr tbl e); x=None; z=None; vector=None; loc;
      })
    | [Some { value="z"; loc }, ArgumentExpression e] -> Rotate (RotateAngle {
        z = Some (sanitize_expr tbl e); x=None; y=None; vector=None; loc;
      })
    | [Some { value="vector"; loc }, ArgumentExpression e] -> Rotate (RotateAngle {
        vector = Some (sanitize_expr tbl e); x=None; y=None; z=None; loc;
      })
    | [Some _, _] -> 
      Output.output_error loc "rotate modifier does not support string literal parameters.";
      ModifierFail
    | ls ->
      let (x,y,z, vector,failed,_all_empty) =
        List.fold ls ~init:(None, None, None, None, false, Some 0) ~f:(fun (x,y,z,vec,failed, all_empty) elem ->
            if failed then (x,y,z,vec,failed, all_empty) else begin
              match elem, all_empty with
              | (None, ArgumentExpression e), _ -> begin
                  let e = Some (sanitize_expr tbl e) in
                  match all_empty with
                  | Some 0 ->
                    (e, y, z, vec, failed, Some 1)
                  | Some 1 ->
                    (x, e, z, vec, failed, Some 2)
                  | Some 2 ->
                    (x, y, e, vec, failed, Some 3)
                  | Some 3 ->
                    (x, y, z, e, failed, Some 4)
                  | Some _ ->
                    Output.output_error loc "too many positional arguments";
                    (x,y,z,vec, true, all_empty)                
                  | None ->
                    Output.output_error loc "mixing of positional and keyword arguments are not supported";
                    (x,y,z, vec, true, all_empty)                
                end
              | (_, _), Some i when i >= 1  ->
                Output.output_error loc "mixing of positional and keyword arguments not supported";
                (x,y,z,vec, true, all_empty)                
              | (_, ArgumentString _), _ ->
                Output.output_error loc "rotate modifier does not support string literals";
                (x,y,z,vec, true, all_empty)                
              | (Some { value="x"; loc }, ArgumentExpression e), _ -> begin match x with
                  | None -> (Some (sanitize_expr tbl e), y, z, vec, failed, None)
                  | Some _ ->
                    Output.output_error loc "repeated argument x to rotate modifier";
                    (x,y,z,vec, true, all_empty)
                end
              | (Some { value="y"; loc }, ArgumentExpression e), _ -> begin match y with
                  | None -> (x, Some (sanitize_expr tbl e), z, vec, failed, None)
                  | Some _ ->
                    Output.output_error loc "repeated argument y to rotate modifier";
                    (x,y,z, vec, true, all_empty)
                end
              | (Some { value="z"; loc }, ArgumentExpression e), _ -> begin match z with
                  | None -> (x, y, Some (sanitize_expr tbl e), vec, failed, None)
                  | Some _ ->
                    Output.output_error loc "repeated argument z to rotate modifier";
                    (x,y,z, vec, true, all_empty)
                end
              | (Some { value="vector"; loc }, ArgumentExpression e), _ -> begin match z with
                  | None -> (x, y, z, Some (sanitize_expr tbl e), failed, None)
                  | Some _ ->
                    Output.output_error loc "repeated argument z to rotate modifier";
                    (x,y,z, vec, true, all_empty)
                end
              | (Some {value; _}, _), _  ->
                Output.output_error loc "unknown keyword argument %s to rotate modifier" value;
                (x,y,z, vec, true, all_empty)
            end) in
      if failed then ModifierFail
      else (Rotate (RotateAngle {x;y;z;vector; loc})) 
  and sanitize_draw_args tbl loc args =
    let exp_to_comparison_function (exp: Parsetree.expr) = match exp with
      | Parsetree.Exp_variable { value="never"; _ } -> Some Stencil_never
      | Parsetree.Exp_variable { value="less"; _ } -> Some Stencil_less
      | Parsetree.Exp_variable { value="lequal"; _ } -> Some Stencil_lequal
      | Parsetree.Exp_variable { value="greater"; _ } -> Some Stencil_greater
      | Parsetree.Exp_variable { value="gequal"; _ } -> Some Stencil_gequal
      | Parsetree.Exp_variable { value="equal"; _ } -> Some Stencil_equal
      | Parsetree.Exp_variable { value="notequal"; _ } -> Some Stencil_notequal
      | Parsetree.Exp_variable { value="always"; _ } -> Some Stencil_always
      | _ -> None in
    let exp_to_blend_function (exp: Parsetree.expr) = match exp with
      | Parsetree.Exp_variable { value="zero"; _ } -> Some Zero
      | Parsetree.Exp_variable { value="one"; _ } -> Some One
      | Parsetree.Exp_variable { value="src_color"; _ } -> Some Src_color
      | Parsetree.Exp_variable { value="one_minus_src_color"; _ } -> Some One_minus_src_color
      | Parsetree.Exp_variable { value="dst_color"; _ } -> Some Dst_color
      | Parsetree.Exp_variable {value="one_minus_dst_color"; _} -> Some One_minus_dst_color
      | Parsetree.Exp_variable {value="src_alpha"; _} -> Some Src_alpha
      | Parsetree.Exp_variable {value="one_minus_src_alpha"; _} -> Some One_minus_src_alpha
      | Parsetree.Exp_variable {value="dst_alpha"; _} -> Some Dst_alpha
      | Parsetree.Exp_variable {value="one_minus_dst_alpha"; _} -> Some One_minus_dst_alpha
      | Parsetree.Exp_variable {value="constant_color"; _} -> Some Constant_color
      | Parsetree.Exp_variable {value="one_minus_constant_color"; _} -> Some One_minus_constant_color
      | Parsetree.Exp_variable {value="constant_alpha"; _} -> Some Constant_alpha
      | Parsetree.Exp_variable {value="one_minus_constant_alpha"; _} -> Some One_minus_constant_alpha
      | Parsetree.Exp_variable {value="src_alpha_saturate"; _} -> Some Src_alpha_saturate
      | _ -> None in

    let rec loop (failed, acc)  args = match args with
      | [] -> if failed then ModifierFail else DrawOptions {value=(List.rev acc);loc}
      | h :: t -> match h with
        | _ , Parsetree.ArgumentString _ ->
          Output.output_error loc "string literals are not supported arguments to draw option";
          loop (true, acc) t          
        | Some {value="alpha_test"; loc}, Parsetree.ArgumentExpression e ->
          let e = sanitize_expr tbl e  in
          let elem = AlphaTest e in
          loop (false, {value=elem; loc} :: acc) t
        | Some {value="blend_test"; loc}, Parsetree.ArgumentExpression e ->
          let e = sanitize_expr tbl e  in
          let elem = BlendTest e in
          loop (false, {value=elem; loc} :: acc) t
        | Some {value="depth_test"; loc}, Parsetree.ArgumentExpression e ->
          let e = sanitize_expr tbl e  in
          let elem = DepthTest e in
          loop (false, {value=elem; loc} :: acc) t  
        | Some {value="stencil_test"; loc}, Parsetree.ArgumentExpression e ->
          let e = sanitize_expr tbl e  in
          let elem = StencilTest e in
          loop (false, {value=elem; loc} :: acc) t

        | Some {value="blend_color"; loc}, Parsetree.ArgumentExpression e ->
          let e = sanitize_expr tbl e  in
          let elem = BlendColor (ColourVector e) in
          loop (false, {value=elem; loc} :: acc) t

        | Some {value="blend_function"; loc}, Parsetree.ArgumentExpression (Exp_list {value=[e1;e2]; _}) -> begin
            let e1 = exp_to_blend_function e1 in
            let e2 = exp_to_blend_function e2 in
            match e1,e2 with
            | Some e1, Some e2 -> 
              let elem = BlendFunction (e1, e2) in
              loop (false, {value=elem; loc} :: acc) t
            | _ -> 
              Output.output_error loc "invalid blend function arguments to draw option";
              loop (true, acc) t
          end

        | Some {value="blend_function"; loc}, Parsetree.ArgumentExpression e1 -> begin
            let e1 = exp_to_blend_function e1 in
            match e1 with
            | Some e1 -> 
              let elem = BlendFunction (e1, e1) in
              loop (false, {value=elem; loc} :: acc) t
            | _ -> 
              Output.output_error loc "invalid blend function arguments to draw option";
              loop (true, acc) t
          end

        | Some {value="stencil_function"; _},
          Parsetree.ArgumentExpression (Exp_list {value=[e1;e2;e3]; _}) -> begin
            let e1 = exp_to_comparison_function e1 in
            match e1 with
            | Some e1 ->
              let e2 = sanitize_expr tbl e2 in
              let e3 = sanitize_expr tbl e3 in
              let elem = StencilFunction (e1, e2, e3) in
              loop (false, {value=elem; loc} :: acc) t
            | _ -> 
              Output.output_error loc "invalid stencil function arguments to draw option";
              loop (true, acc) t
          end
        | Some {value="stencil_function"; _}, Parsetree.ArgumentExpression _ -> begin
            Output.output_error loc "arguments to stencil function must be a list of 3 elements";
            loop (true, acc) t
          end

        | Some {value="depth_function"; _},
          Parsetree.ArgumentExpression e1 -> begin
            let e1 = exp_to_comparison_function e1 in
            match e1 with
            | Some e1 ->
              let elem = DepthFunction e1 in
              loop (false, {value=elem; loc} :: acc) t
            | _ -> 
              Output.output_error loc "invalid depth function arguments in draw option";
              loop (true, acc) t
          end
        | Some {value="alpha_function"; _},
          Parsetree.ArgumentExpression (Exp_list {value=[e1;e2]; _}) -> begin
            let e1 = exp_to_comparison_function e1 in
            match e1 with
            | Some e1 ->
              let e2 = sanitize_expr tbl e2 in
              let elem = AlphaFunction (e1, e2) in
              loop (false, {value=elem; loc} :: acc) t
            | _ -> 
              Output.output_error loc "invalid stencil function arguments to draw option";
              loop (true, acc) t
          end
        | Some {value="alpha_function"; _},
          Parsetree.ArgumentExpression _ -> begin
            Output.output_error loc "alpha function arguments must be a list of 2 elements";
            loop (true, acc) t
          end
        | Some { value; loc }, ArgumentExpression (Exp_variable {value=const; _}) ->
          Output.output_error loc "unknown keyword argument %s=%s passed to draw option" value const;
          loop (true, acc) t
        | Some { value; loc }, _ ->
          Output.output_error loc "unknown keyword argument %s passed to draw option" value;
          loop (true, acc) t
        | _ ->
          Output.output_error loc "all arguments to draw option must be keywords";
          loop (true, acc) t
    in let result = loop (false, []) args in result
  and sanitize_scene_expr tbl (scene_expr: Parsetree.scene_expr) : scene_expr =
    match scene_expr with
    | Parsetree.SceneExpr_variable { loc; _ } ->
      Output.output_error loc "The use of raw macro variables are not supported for scene expressions (missing object(),shader(),drawable()?)";
      SceneExprFail
    | Parsetree.SceneExpr_macro_call { loc; _ } ->
      Output.output_error loc "The use of raw macro function calls are not supported for scene expressions (missing object(),shader(),drawable()?)";
      SceneExprFail
    | Parsetree.SceneExpr_object obj -> SceneExprObject (sanitize_object tbl obj)
    | Parsetree.SceneExpr_shader shader -> SceneExprShader (sanitize_shader tbl shader)
    | Parsetree.SceneExpr_drawable drawable -> SceneExprDrawable (sanitize_drawable tbl drawable)
    | Parsetree.SceneExpr_modifier modifier -> SceneExprModifier (sanitize_modifier tbl modifier)
    | Parsetree.SceneExpr_with_type { value=(expr, typ); _ } ->
      let expr = sanitize_scene_expr tbl expr in
      let typ = sanitize_type_expr tbl typ in
      SceneExprTyped (expr,typ)
    | Parsetree.SceneExpr_if_then_else { loc; _ } ->
      Output.output_error loc "if then else operations are only supported inside compound object declarations";
      SceneExprFail
    | Parsetree.SceneExpr_for { loc; _ } ->
      Output.output_error loc "for operations are only supported inside compound object declarations";
      SceneExprFail
  and sanitize_macro_args tbl ({ value=(fname, args); _ }:
                                 (string loc * (string loc option * Parsetree.scene_arg) list) loc) =
    let add = IdentifierTable.add_identifier tbl in
    let fname = map_loc ~f:add fname in
    let (keyword_args, positional_args, failed) =
      List.fold args ~init:(IdentifierTable.MMap.empty, [], false) ~f:(fun (map, positional, failed) v ->
          let conditional_out expr name =
            match name with
            | None -> (map, expr :: positional, failed)
            | Some { value; loc } ->
              let name = add value in
              let map' = IdentifierTable.MMap.add map ~key:name ~data:expr in
              match map' with
              | `Duplicate ->
                Output.output_error loc "repeated keyword argument %s to macro call" value;
                (map, positional, true)
              | `Ok map ->
                (map, positional, failed)
          in
          if failed then (map, positional, failed) else begin
            match v with
            | (_, Parsetree.SceneString { loc; _ }) ->
              Output.output_error loc "string literals can not be used as arguments macro calls";
              (map, positional, true)
            | (name, Parsetree.SceneDataExpression expr) ->
              let expr = SceneDataExpression (sanitize_expr tbl expr) in
              conditional_out expr name
            | (name, Parsetree.SceneInlineExpression expr) ->
              let expr = SceneInlineExpression (sanitize_scene_expr tbl expr) in
              conditional_out expr name
          end)
    in
    if failed
    then None
    else Some (fname, MacroArguments { keyword_args; positional_args = List.rev positional_args; })
  and sanitize_declaration tbl (decl: Parsetree.declaration) =
    let add = IdentifierTable.add_identifier tbl in
    match decl with
    | Parsetree.DataVariable { identifier; typ; body } ->
      let identifier = map_loc ~f:add identifier in
      let typ = Option.map ~f:(sanitize_type_expr tbl) typ in
      let body = sanitize_expr tbl body in
      DataVariable {identifier; typ; body}
    | Parsetree.MacroVariable { identifier; typ; body } ->
      let identifier = map_loc ~f:add identifier in
      let typ = Option.map ~f:(sanitize_type_expr tbl) typ in
      let body = sanitize_scene_expr tbl body in
      MacroVariable {identifier; typ; body}
    | Parsetree.DataFunction { identifier; arguments; typ; body } ->
      let any_dup = List.map arguments ~f:(fun (name, _, _) -> name.value)
                    |> List.contains_dup ~compare:String.compare in
      if any_dup then begin
        Output.output_error identifier.loc "duplicate argument names for function";
        DeclarationFail
      end else begin
        let args = List.fold arguments ~init:(Some (IdentifierTable.MMap.empty, [])) ~f:(fun acc (name, oty, odfl) ->
            match acc with
            | None -> None
            | Some (map, pos) ->  match odfl with
              | None ->
                let name = map_loc ~f:add name in
                let oty = Option.map ~f:(sanitize_type_expr tbl) oty in
                Some (map, (name,oty) :: pos)
              | Some expr ->
                let name_new = map_loc ~f:add name in
                let expr = sanitize_expr tbl expr in
                let oty = Option.map ~f:(sanitize_type_expr tbl) oty in
                let map = IdentifierTable.MMap.add ~key:name_new.value ~data:(oty, expr) map in
                match map with
                | `Duplicate ->
                  Output.output_error name.loc "duplicate keyword %s in function declaration" name.value;
                  None
                | `Ok map -> Some (map, pos)
          ) in
        begin match args with
          | None -> DeclarationFail
          | Some (keyword_arguments, positional_arguments) ->
            let identifier = map_loc ~f:add identifier in
            let typ = Option.map ~f:(sanitize_type_expr tbl) typ in
            let body = sanitize_expr tbl body in
            DataFunction {
              identifier;
              typ;
              body;
              keyword_arguments;
              positional_arguments = List.rev positional_arguments;
            }
        end
      end
    | Parsetree.MacroFunction { identifier; arguments; typ; body } ->
      let any_dup = List.map arguments ~f:(fun (name, _, _) -> name.value)
                    |> List.contains_dup ~compare:String.compare in
      if any_dup then begin
        Output.output_error identifier.loc "duplicate argument names for macro function declaration";
        DeclarationFail
      end else begin
        let args =
          List.fold arguments
            ~init:(Some (IdentifierTable.MMap.empty, []))
            ~f:(fun acc (name, oty, odfl) ->
                match acc with
                | None -> None
                | Some (map, pos) ->  match odfl with
                  | None ->
                    let name = map_loc ~f:add name in
                    let oty = Option.map ~f:(sanitize_type_expr tbl) oty in
                    Some (map, (name,oty) :: pos)
                  | Some expr ->
                    let name_new = map_loc ~f:add name in
                    let expr = sanitize_scene_expr tbl expr in
                    let oty = Option.map ~f:(sanitize_type_expr tbl) oty in
                    let map = IdentifierTable.MMap.add ~key:name_new.value ~data:(oty, expr) map in
                    match map with
                    | `Duplicate ->
                      Output.output_error name.loc
                        "duplicate keyword %s in macro function declaration" name.value;
                      None
                    | `Ok map -> Some (map, pos)
              ) in
        begin match args with
          | None -> DeclarationFail
          | Some (keyword_arguments, positional_arguments) ->
            let identifier = map_loc ~f:add identifier in
            let typ = Option.map ~f:(sanitize_type_expr tbl) typ in
            let body = sanitize_scene_expr tbl body in
            MacroFunction {
              identifier;
              typ;
              body;
              keyword_arguments;
              positional_arguments = List.rev positional_arguments;
            }
        end
      end
  and sanitize_statement tbl (statement: Parsetree.statement) =
    match statement with
    | Parsetree.Declaration decl ->
      Declaration (sanitize_declaration tbl decl)
    | Parsetree.Drawable drawable ->
      Drawable (sanitize_drawable tbl drawable)
    | Parsetree.LayeredDrawable ({ value=[None, ArgumentString s]; _ }, drawable) ->
      LayeredDrawable (s.value, sanitize_drawable tbl drawable)
    | Parsetree.LayeredDrawable ({ value=[None, _]; loc }, _) ->
      Output.output_error loc "Layer annotations must specify layer name inside a string literal";
      StatementFail
    | Parsetree.LayeredDrawable ({ value=[_, _]; loc }, _) ->
      Output.output_error loc "Layer annotations do not support keyword arguments";
      StatementFail
    | Parsetree.LayeredDrawable ({ value=_; loc }, _) ->
      Output.output_error loc "invalid arity of arguments to layer declaration";
      StatementFail
  and sanitize_program tbl (program: Parsetree.statement list) =
    List.map ~f:(sanitize_statement tbl) program

end

module Output = struct

  open Raw

  type expr =
    | Exp_bool of bool loc
    | Exp_number of Parsetree.num loc
    | Exp_variable of IdentifierTable.m loc
    | Exp_binop of (Parsetree.binop * expr * expr) loc
    | Exp_preop of (Parsetree.preop * expr) loc
    | Exp_index of (expr * expr) loc
    | Exp_vector of vector loc
    | Exp_list of expr list loc
    | Exp_list_comprehension of (expr * IdentifierTable.m loc * expr * expr option) loc
    | Exp_function_call of IdentifierTable.m loc * function_args
    | Exp_if_then_else of (expr * expr * expr) loc
    | Exp_with_type of (expr * type_expr) loc
  and type_expr = TypeExpr of internal_type_expr or_variable
  and internal_type_expr =
    | TyPure of pure_data_type
    | TyTexture of texture_type
    | TyScene of scene_type
    | TyList of type_expr
  and function_args = FunctionArguments of
      {
        keyword_args: expr IdentifierTable.MMap.t;
        positional_args: expr list;
      }
  and vector =
    | Vector2 of (expr * expr) 
    | Vector3 of (expr * expr * expr)
    | Vector4 of (expr * expr * expr * expr)
  and data =
    | Model of string loc         (* path to model *)
    | Data of (IdentifierTable.m loc * expr) list loc (* list of name to value mappings  *)
    | Screen of {width: expr option; height: expr option; loc: Location.t}
  and wrap_option =
    | Repeat
    | MirroredRepeat
    | ClampToEdge
    | ClampToBorder
  and filter_option =
    | Nearest
    | Linear  
    | NearestMipmapNearest
    | NearestMipmapLinear
    | LinearMipmapNearest
    | LinearMipmapLinear
  and texture_option =
    | TextureBaseLevel of expr
    | TextureMaxLevel of expr
    | TextureBorderColor of expr
    | WrapS of wrap_option
    | WrapT of wrap_option
    | MinFilter of filter_option
    | MagFilter of filter_option
  and dimension_options =
    | DimensionVector of expr
    | DimensionXZY of {x: expr option; y: expr option; z: expr option; loc: Location.t } 
  and rotate_options =
    | RotateAngle of {x: expr option; y:expr option; z: expr option; vector: expr option; loc: Location.t}
  and blend_func =
    | Zero
    | One
    | Src_color
    | One_minus_src_color
    | Dst_color
    | One_minus_dst_color
    | Src_alpha
    | One_minus_src_alpha
    | Dst_alpha
    | One_minus_dst_alpha
    | Constant_color
    | One_minus_constant_color
    | Constant_alpha
    | One_minus_constant_alpha
    | Src_alpha_saturate
  and comparison_func =
    | Stencil_never
    | Stencil_less
    | Stencil_lequal
    | Stencil_greater
    | Stencil_gequal
    | Stencil_equal
    | Stencil_notequal
    | Stencil_always
  and blend_color_option =
    | ColourVector of expr
    (* | ColourArgs of {red: expr option; green: expr option; blue: expr option; alpha: expr option} *)
  and draw_option =
    | AlphaTest of expr
    | BlendTest of expr
    | DepthTest of expr
    | StencilTest of expr
    | BlendColor of blend_color_option
    | BlendFunction of blend_func * blend_func
    | StencilFunction of comparison_func * expr * expr
    | DepthFunction of comparison_func
    | AlphaFunction of comparison_func * expr
  and modifier =
    | Texture of IdentifierTable.m loc * string loc list * texture_option list    (* texture name, texture file,  *)
    | Uniform of IdentifierTable.m loc * expr                            (* uniform name, expression *)
    | Attribute of IdentifierTable.m loc * expr
    | Translate of dimension_options
    | Scale of dimension_options
    | Rotate of rotate_options
    | DrawOptions of draw_option loc list loc
    | Redirecting of {
        textures: IdentifierTable.m loc list;
        depth: bool; stencil: bool;
        drawable: drawable;
        loc: Location.t;
      }
    | LightUniform of IdentifierTable.m loc * IdentifierTable.m loc * expr
    | ModifierReference of reference
  and base_object =
    | Light of {
        name: IdentifierTable.m loc;
        position: expr;
        size: expr;
      }
    | LightData of {
        name: IdentifierTable.m loc;
      }
    | ObjectReference of reference
    | ObjectData of data
    | CompoundObject of (obj_expr * obj_expr list) loc
  and obj = Object of modifier list * base_object * Location.t
  and obj_expr =
    | PlainObject of obj
    | ObjectIfThenElse of expr * obj_expr * obj_expr option
    | ObjectFor of (IdentifierTable.m loc * expr * expr option * (obj_expr * obj_expr list)) loc
  and declaration =
    | DataFunction of
        { identifier: IdentifierTable.m loc;
          keyword_arguments: (type_expr option * expr) IdentifierTable.MMap.t;
          positional_arguments: (IdentifierTable.m loc * type_expr option) list;
          typ: type_expr option;
          body: expr
        }
    | DataVariable of
        { identifier: IdentifierTable.m loc; typ: type_expr option; body: expr }
    | MacroFunction of 
        { identifier: IdentifierTable.m loc;
          keyword_arguments: (type_expr option * scene_expr) IdentifierTable.MMap.t;
          positional_arguments: (IdentifierTable.m loc * type_expr option) list;
          typ: type_expr option;
          body: scene_expr
        }
    | MacroVariable of
        { identifier: IdentifierTable.m loc; typ: type_expr option; body: scene_expr }
  and reference =
    | DirectReference of IdentifierTable.m loc
    | IndirectReference of IdentifierTable.m loc * macro_arguments
  and shader =
    | ShaderReference of reference
    | Shader of { vertex_shader: string loc; fragment_shader: string loc option; geometry_shader: string loc option; }
  and drawable =
    | OverlayingDrawable of drawable loc
    | DrawableShader of (shader * obj) loc
    | DrawableReference of reference
  and scene_arg =
    | ArgReference of reference
    | SceneDataExpression of expr
    | SceneInlineExpression of scene_expr
  and macro_arguments =
      MacroArguments of {
        keyword_args: scene_arg IdentifierTable.MMap.t;
        positional_args: scene_arg list;
      }
  and scene_expr =
    (* | SceneExprVariable of IdentifierTable.m loc
     * | SceneExprFuncall of (IdentifierTable.m loc * (string loc option * scene_arg) list) *)
    | SceneExprObject of obj
    | SceneExprShader of shader
    | SceneExprModifier of modifier
    | SceneExprDrawable of drawable
    | SceneExprTyped of scene_expr * type_expr
  and statement =
    | Declaration of declaration
    | Drawable of drawable
    | LayeredDrawable of string * drawable

  let expr_to_loc (expr: expr) =
    match expr with
    | Exp_bool { loc; _ } -> loc
    | Exp_number {  loc; _ } -> loc
    | Exp_variable {  loc; _ } -> loc
    | Exp_binop {  loc; _ } -> loc
    | Exp_preop { loc; _ } -> loc
    | Exp_index {  loc; _ } -> loc
    | Exp_vector {  loc; _ } -> loc
    | Exp_list {  loc; _ } -> loc
    | Exp_list_comprehension {  loc; _ } -> loc
    | Exp_function_call ({loc; _}, _) -> loc
    | Exp_if_then_else {loc; _} -> loc
    | Exp_with_type {loc; _} -> loc


  let (let+) x f = Option.bind x ~f
  let ret = Option.return

  let omap o ~f = match o with
    | None -> Some None
    | Some v -> match f v with
      | None -> None
      | Some v -> Some (Some v)

  let rec extract_expr (expr: Raw.expr) =
    match expr with
    | Raw.Exp_bool l -> Some (Exp_bool l)
    | Raw.Exp_number l -> Some (Exp_number l)
    | Raw.Exp_variable l -> Some (Exp_variable l)
    | Raw.Exp_binop { value=(binop, e1, e2); loc } ->
      let+ e1 = extract_expr e1 in
      let+ e2 = extract_expr e2 in
      Some (Exp_binop {value=(binop, e1,e2); loc})
    | Raw.Exp_preop { value=(preop, e1); loc } ->
      let+ e1 = extract_expr e1 in
      Some (Exp_preop {value=(preop, e1); loc})
    | Raw.Exp_index { value=(e1, e2); loc } ->
      let+ e1 = extract_expr e1 in
      let+ e2 = extract_expr e2 in
      Some (Exp_index {value=(e1,e2); loc})
    | Raw.Exp_vector { value=vector; loc } ->
      let+ vector = extract_vector vector in
      Some (Exp_vector {value=vector; loc})
    | Raw.Exp_list { value=ls; loc } ->
      let+ ls = List.map ~f:(extract_expr) ls |> Option.all in
      Some (Exp_list {value=ls;loc})
    | Raw.Exp_list_comprehension { value=(cond, id, e1, e2); loc } ->
      let+ cond = extract_expr cond in
      let+ e1 = extract_expr e1 in
      let+ e2 = omap ~f:extract_expr e2 in
      Some (Exp_list_comprehension {value=(cond, id, e1, e2); loc})
    | Raw.Exp_function_call (id,args) ->
      let+ args = extract_function_args args in
      Some (Exp_function_call (id,args))
    | Raw.Exp_if_then_else { value=(cond, e1, e2); loc } ->
      let+ cond = extract_expr cond in
      let+ e1 = extract_expr e1 in
      let+ e2 = extract_expr e2 in
      Some (Exp_if_then_else {value=(cond,e1,e2); loc})
    | Raw.Exp_with_type { value=(e1,ty); loc } ->
      let+ e1 = extract_expr e1 in
      let+ ty = sanitize_type_expr ty in
      Some (Exp_with_type {value=(e1,ty); loc})
    | Raw.ExpFail -> None
  and sanitize_type_expr ty = match ty with
  | TypeFail -> None
  | TypeExpr v -> (match v with
      | TyDirect v ->  (sanitize_internal_type_expr v) |> Option.map ~f:(fun v -> TypeExpr (TyDirect v))
      | TyVariable v -> Some (TypeExpr (TyVariable v))
      | Hole l -> Some (TypeExpr (Hole l)))
  and sanitize_internal_type_expr v = (match v with
      | Raw.TyPure v -> Some (TyPure v)
      | Raw.TyTexture v -> Some (TyTexture v)
      | Raw.TyScene v -> Some (TyScene v)
      | Raw.TyList v ->
        (sanitize_type_expr v) |> Option.map ~f:(fun v -> TyList v)
    )
  and extract_vector vec =  match vec with
    | Raw.Vector2 (e1, e2) ->
      let+ e1 = extract_expr e1 in
      let+ e2 = extract_expr e2 in
      Some (Vector2 (e1, e2))
    | Raw.Vector3 (e1,e2,e3) ->
      let+ e1 = extract_expr e1 in
      let+ e2 = extract_expr e2 in
      let+ e3 = extract_expr e3 in
      Some (Vector3 (e1, e2, e3))
    | Raw.Vector4 (e1, e2, e3, e4) ->
      let+ e1 = extract_expr e1 in
      let+ e2 = extract_expr e2 in
      let+ e3 = extract_expr e3 in
      let+ e4 = extract_expr e4 in
      Some (Vector4 (e1, e2, e3, e4))
    | Raw.VectorFail -> None
  and extract_function_args (Raw.FunctionArguments { keyword_args; positional_args }) =
      let+ keyword_args = IdentifierTable.MMap.to_alist keyword_args
                 |>  List.map ~f:(fun (id, expr) -> extract_expr expr
                                                         |> Option.map ~f:(fun v -> (id,v)))
                 |> Option.all
                 |> Option.map ~f:(fun ls -> IdentifierTable.MMap.of_alist_exn ls) in
      let+ positional_args = List.map ~f:extract_expr positional_args |> Option.all in
      ret @@ FunctionArguments {keyword_args; positional_args}
  and extract_data (data: Raw.data) : data option = match data with
    | Raw.Model l -> Some (Model l)
    | Raw.Data {value=ls;loc} ->
      let+ ls = List.map ls ~f:(fun (id, expr) -> extract_expr expr
                                                  |> Option.map ~f:(fun v -> (id,v)))
                |> Option.all in
      ret @@ Data {value=ls; loc}
    | Raw.Screen { width; height; loc } ->
      let+ width = omap width ~f:extract_expr in
      let+ height = omap height ~f:extract_expr in
      ret @@ Screen { width; height; loc }
    | Raw.DataFail -> None
  and extract_wrap_option (wp: Raw.wrap_option) = match wp with
    | Raw.Repeat -> Repeat
    | Raw.MirroredRepeat -> MirroredRepeat
    | Raw.ClampToEdge -> ClampToEdge
    | Raw.ClampToBorder -> ClampToBorder
  and extract_filter_option (fp: Raw.filter_option) = match fp with
    | Raw.Nearest -> Nearest
    | Raw.Linear -> Linear
    | Raw.NearestMipmapNearest -> NearestMipmapNearest
    | Raw.NearestMipmapLinear -> NearestMipmapLinear
    | Raw.LinearMipmapNearest -> LinearMipmapNearest
    | Raw.LinearMipmapLinear -> LinearMipmapLinear
  and extract_texture_option (tp: Raw.texture_option) = match tp with
    | Raw.TextureBaseLevel e1 -> extract_expr e1 |> Option.map ~f:(fun v -> TextureBaseLevel v)
    | Raw.TextureMaxLevel e1 -> extract_expr e1 |> Option.map ~f:(fun v -> TextureMaxLevel v)
    | Raw.TextureBorderColor e1 -> extract_expr e1 |> Option.map ~f:(fun v -> TextureBorderColor v)
    | Raw.WrapS wrap_option -> Some (WrapS (extract_wrap_option wrap_option))
    | Raw.WrapT wrap_option -> Some (WrapT (extract_wrap_option wrap_option))
    | Raw.MinFilter filter_option -> Some (MinFilter (extract_filter_option filter_option))
    | Raw.MagFilter filter_option -> Some (MagFilter (extract_filter_option filter_option))
  and extract_dimension_options (dm: Raw.dimension_options) =
    match dm with
    | Raw.DimensionVector e1 -> extract_expr e1 |> Option.map ~f:(fun v -> DimensionVector v)
    | Raw.DimensionXZY { x; y; z; loc } ->
      let+ x = omap x ~f:(extract_expr) in
      let+ y = omap y ~f:(extract_expr) in
      let+ z = omap z ~f:(extract_expr) in
      ret @@ DimensionXZY { x; y; z; loc}
  and extract_rotate_options (Raw.RotateAngle { x; y; z; vector; loc }: Raw.rotate_options) =
      let+ x = omap x ~f:(extract_expr) in
      let+ y = omap y ~f:(extract_expr) in
      let+ z = omap z ~f:(extract_expr) in
      let+ vector = omap vector ~f:(extract_expr) in
      ret @@ RotateAngle {x;y;z;vector; loc}
  and extract_blend_function (blend_func: Raw.blend_func) : blend_func  = match blend_func with
    | Raw.Zero -> Zero
    | Raw.One -> One
    | Raw.Src_color -> Src_color
    | Raw.One_minus_src_color -> One_minus_src_color
    | Raw.Dst_color -> Dst_color
    | Raw.One_minus_dst_color -> One_minus_dst_color
    | Raw.Src_alpha -> Src_alpha
    | Raw.One_minus_src_alpha -> One_minus_src_alpha
    | Raw.Dst_alpha -> Dst_alpha
    | Raw.One_minus_dst_alpha -> One_minus_dst_alpha
    | Raw.Constant_color -> Constant_color
    | Raw.One_minus_constant_color -> One_minus_constant_color
    | Raw.Constant_alpha -> Constant_alpha
    | Raw.One_minus_constant_alpha -> One_minus_constant_alpha
    | Raw.Src_alpha_saturate -> Src_alpha_saturate

  and extract_comparison_func (comparison_func: Raw.comparison_func) : comparison_func = match comparison_func with
    | Raw.Stencil_never -> Stencil_never
    | Raw.Stencil_less -> Stencil_less
    | Raw.Stencil_lequal -> Stencil_lequal
    | Raw.Stencil_greater -> Stencil_greater
    | Raw.Stencil_gequal -> Stencil_gequal
    | Raw.Stencil_equal -> Stencil_equal
    | Raw.Stencil_notequal -> Stencil_notequal
    | Raw.Stencil_always -> Stencil_always
  and extract_blend_color_option (Raw.ColourVector e: Raw.blend_color_option) =
    let+ e = extract_expr e in
    ret @@ ColourVector e
  and extract_draw_option (draw_option: Raw.draw_option) = match draw_option with
    | Raw.AlphaTest e ->
      let+ e = extract_expr e in
      ret @@ AlphaTest e
    | Raw.BlendTest e ->
      let+ e = extract_expr e in
      ret @@ BlendTest e
    | Raw.DepthTest e ->
      let+ e = extract_expr e in
      ret @@ DepthTest e
    | Raw.StencilTest e ->
      let+ e = extract_expr e in
      ret @@ StencilTest e
    | Raw.BlendColor e ->
      let+ e = extract_blend_color_option e in
      ret @@ BlendColor e
    | Raw.BlendFunction (e1, e2) ->
      ret @@ BlendFunction (extract_blend_function e1, extract_blend_function e2)
    | Raw.StencilFunction (c1, e2, e3) ->
      let c1 = extract_comparison_func c1 in
      let+ e2 = extract_expr e2 in
      let+ e3 = extract_expr e3 in
      ret @@ StencilFunction (c1, e2, e3)
    | Raw.DepthFunction cf ->
      ret @@ DepthFunction (extract_comparison_func cf)
    | Raw.AlphaFunction (c1, e1) ->
      let c1 = extract_comparison_func c1 in
      let+ e1 = extract_expr e1 in
      ret @@ AlphaFunction (c1, e1)
  and extract_modifier (modifier: Raw.modifier)  = match modifier with
    | Raw.Texture (name, filename, options) ->
      let+ options = List.map options ~f:extract_texture_option |> Option.all in
      ret @@ Texture (name, filename, options)
    | Raw.Uniform (name, e) ->
      let+ e = extract_expr e in
      ret @@ Uniform (name, e)
    | Raw.Attribute (name, e) ->
      let+ e = extract_expr e in
      ret @@ Attribute (name, e)
    | Raw.Translate dop ->
      let+ dop = (extract_dimension_options dop) in
      ret @@ Translate dop
    | Raw.Scale dop ->
      let+ dop =  (extract_dimension_options dop) in
      ret @@ Scale dop
    | Raw.Rotate ropt ->
      let+ ropt = extract_rotate_options ropt in
      ret @@ Rotate ropt
    | Raw.DrawOptions {value=dopts;loc} ->
      let+ dopts = List.map ~f:(fun {value;loc} ->
          Option.map (extract_draw_option value) ~f:(fun value -> {value;loc})
        ) dopts |> Option.all in
      ret @@ DrawOptions {value=dopts;loc}
    | Raw.Redirecting { textures; depth; stencil; drawable; loc } ->
      let+ drawable = extract_drawable drawable in
      ret @@ Redirecting {textures; depth; stencil; drawable; loc}
    | Raw.LightUniform (name, uniform, e) ->
      let+ e = extract_expr e in
      ret @@ LightUniform (name, uniform, e)
    | Raw.ModifierReference rf ->
      let+ rf = extract_reference rf in
      ret @@ ModifierReference rf
    | Raw.ModifierFail -> None
  and extract_drawable (drawable: Raw.drawable) = match drawable with
    | Raw.OverlayingDrawable { value; loc } ->
      let+ value = extract_drawable value in
      ret @@ OverlayingDrawable {value; loc}
    | Raw.DrawableShader { value=(shader,obj); loc } ->
      let+ shader = extract_shader shader in
      let+ obj = extract_object obj in
      ret @@ DrawableShader { value = (shader, obj); loc }
    | Raw.DrawableReference rf ->
      let+ rf  = extract_reference rf in
      ret @@ DrawableReference rf 
    | Raw.DrawableFail -> None
  and extract_reference (reference: Raw.reference) = match reference with
    | Raw.DirectReference pl -> ret @@ DirectReference pl
    | Raw.IndirectReference (name, args) ->
      let+ args = extract_macro_arguments args in
      ret @@ IndirectReference (name, args)
  and extract_macro_arguments (Raw.MacroArguments { keyword_args; positional_args }: Raw.macro_arguments) =
      let+ keyword_args = IdentifierTable.MMap.to_alist keyword_args
                 |>  List.map ~f:(fun (id, expr) -> extract_scene_arg expr
                                                         |> Option.map ~f:(fun v -> (id,v)))
                 |> Option.all
                 |> Option.map ~f:(fun ls -> IdentifierTable.MMap.of_alist_exn ls) in
      let+ positional_args = List.map ~f:extract_scene_arg positional_args |> Option.all in
      ret @@ MacroArguments {keyword_args; positional_args}
  and extract_scene_arg (scene_arg: Raw.scene_arg) = match scene_arg with
    | Raw.ArgReference rf ->
      let+ rf = extract_reference rf in
      ret @@ ArgReference rf
    | Raw.SceneDataExpression e1 ->
      let+ e1 = extract_expr e1 in
      ret @@ SceneDataExpression e1
    | Raw.SceneInlineExpression e1 ->
      let+ e1 = extract_scene_expr e1 in
      ret @@ SceneInlineExpression e1
  and extract_scene_expr (expr: Raw.scene_expr) = match expr with
    | Raw.SceneExprObject obj ->
      let+ obj = extract_object obj in
      ret @@ SceneExprObject obj
    | Raw.SceneExprShader shader ->
      let+ shader = extract_shader shader in
      ret @@ SceneExprShader shader
    | Raw.SceneExprModifier modifier ->
      let+ modifier = extract_modifier modifier in
      ret @@ SceneExprModifier modifier
    | Raw.SceneExprDrawable drawable ->
      let+ drawable = extract_drawable drawable in
      ret @@ SceneExprDrawable drawable
    | Raw.SceneExprTyped (sexp, ty) ->
      let+ sexp = extract_scene_expr sexp in
      let+ ty = sanitize_type_expr ty in
      ret @@ SceneExprTyped (sexp, ty)
    | Raw.SceneExprFail -> None
  and extract_base_object (obj: Raw.base_object) = match obj with
    | Raw.Light { name; position; size } ->
      let+ position = extract_expr position in
      let+ size = extract_expr size in
      ret @@ Light { name; position; size }
    | Raw.LightData { name } ->
      (* let+ value = extract_expr value in *)
      ret @@ LightData {name}
    | Raw.ObjectReference rf ->
      let+ rf = extract_reference rf in
      ret @@ ObjectReference rf
    | Raw.ObjectData data ->
      let+ data = extract_data data in
      ret @@ ObjectData data
    | Raw.CompoundObject {value=(h, obj_expr); loc} ->
      let+ h = extract_obj_expr h in
      let+ obj_expr = List.map ~f:extract_obj_expr obj_expr |> Option.all  in
      ret @@ CompoundObject {value=(h, obj_expr);loc}
    | Raw.ObjectFail -> None
  and extract_object (Raw.Object (modifiers, bobj, loc): Raw.obj) =
    let+ modifiers = List.map modifiers ~f:extract_modifier |> Option.all in
    let+ bobj = extract_base_object bobj in
    ret @@ Object (modifiers, bobj, loc)
  and extract_obj_expr (obj_expr: Raw.obj_expr) = match obj_expr with
    | Raw.PlainObject obj ->
      let+ obj = extract_object obj in
      ret @@ PlainObject obj
    | Raw.ObjectIfThenElse (cond, e1, e2) ->
      let+ cond = extract_expr cond in
      let+ e1 = extract_obj_expr e1 in
      let+ e2 = omap e2 ~f:extract_obj_expr in
      ret @@ ObjectIfThenElse (cond, e1, e2)
    | Raw.ObjectFor { value=(name, fr, t, body); loc } ->
      let+ fr = extract_expr fr in
      let+ t = omap ~f:extract_expr t in
      let+ body = Option.both
          (extract_obj_expr (fst body))
          (List.map (snd body) ~f:extract_obj_expr |> Option.all) in
      ret @@ ObjectFor {value=(name, fr, t, body); loc}
    | Raw.ObjectExprFail -> None
  and extract_shader (shader: Raw.shader) = match shader with
    | Raw.ShaderReference rf ->
      let+ rf = extract_reference rf in
      ret @@ ShaderReference rf
    | Raw.Shader { vertex_shader; fragment_shader; geometry_shader } ->
      ret @@ Shader { vertex_shader; fragment_shader; geometry_shader }
    | Raw.ShaderFail -> None
  and extract_declaration (decl: Raw.declaration) = match decl with
    | Raw.DataFunction { identifier; keyword_arguments; positional_arguments; typ; body } ->
      let+ keyword_arguments =
        IdentifierTable.MMap.to_alist keyword_arguments
        |>  List.map ~f:(fun (id, (ty, expr)) ->
            let+ expr = extract_expr expr in
            let+ typ = omap ~f:sanitize_type_expr ty in
            ret @@ (id, (typ, expr)))
        |> Option.all
        |> Option.map ~f:(fun ls -> IdentifierTable.MMap.of_alist_exn ls) in
      let+ positional_arguments =
        List.map positional_arguments ~f:(fun (name, expr) ->
            let+ expr = omap ~f:sanitize_type_expr expr in
            ret @@ (name, expr)
          ) |> Option.all in
      let+ typ = omap ~f:sanitize_type_expr typ in
      let+ body = extract_expr body in
      ret @@ DataFunction {identifier; keyword_arguments; positional_arguments; typ; body}
    | Raw.MacroFunction { identifier; keyword_arguments; positional_arguments; typ; body } ->
      let+ keyword_arguments =
        IdentifierTable.MMap.to_alist keyword_arguments
        |>  List.map ~f:(fun (id, (ty, expr)) ->
            let+ expr = extract_scene_expr expr in
            let+ typ = omap ~f:sanitize_type_expr ty in
            ret @@ (id, (typ, expr)))
        |> Option.all
        |> Option.map ~f:(fun ls -> IdentifierTable.MMap.of_alist_exn ls) in
      let+ positional_arguments =
        List.map positional_arguments ~f:(fun (name, expr) ->
            let+ expr = omap ~f:sanitize_type_expr expr in
            ret @@ (name, expr)
          ) |> Option.all in
      let+ typ = omap ~f:sanitize_type_expr typ in
      let+ body = extract_scene_expr body in
      ret @@ MacroFunction {identifier; keyword_arguments; positional_arguments; typ; body}
    | Raw.DataVariable { identifier; typ; body } ->
      let+ body = extract_expr body in
      let+ typ = omap ~f:sanitize_type_expr typ in
      ret @@ DataVariable { identifier; typ; body }
    | Raw.MacroVariable { identifier; typ; body } ->
      let+ typ = omap ~f:sanitize_type_expr typ in
      let+ body = extract_scene_expr body in
      ret @@ MacroVariable { identifier; typ; body }
    | Raw.DeclarationFail -> None
  and extract_statement (statement: Raw.statement) = match statement with
    | Raw.Declaration decl ->
      let+ decl = extract_declaration decl in
      ret @@ Declaration decl
    | Raw.Drawable drawable ->
      let+ drawable = extract_drawable drawable in
      ret @@ Drawable drawable
    | Raw.LayeredDrawable (name, drawable) ->
      let+ drawable = extract_drawable drawable in
      ret @@ LayeredDrawable (name, drawable)
    | Raw.StatementFail -> None
  and extract_program (program: Raw.statement list) =
    List.map ~f:extract_statement program |> Option.all

end

module Dependencies = struct
  open Output

  module S = Set.Make (String)

  type t = { shaders: S.t; textures: S.t; models: S.t }
  let empty = {shaders=S.empty; textures=S.empty; models=S.empty}


  let rec dependencies_program (t:t) (program: statement list) =
    List.fold ~init:t ~f:(dependencies_statement) program
  and dependencies_statement t statement =
    match statement with
    | Declaration decl -> dependencies_declaration t decl
    | LayeredDrawable (_, drawable)
    | Drawable drawable -> dependencies_drawable t drawable
  and dependencies_declaration t decl = match decl with
    | DataFunction {
        keyword_arguments;
        body; _ } ->
      let t = Map.data keyword_arguments |> List.map ~f:snd
              |> List.fold ~init:t ~f:dependencies_expr in
      dependencies_expr t body
    | DataVariable {  body; _ } -> dependencies_expr t body
    | MacroFunction {  keyword_arguments; body; _ } ->
      let t = Map.data keyword_arguments |> List.map ~f:snd
              |> List.fold ~init:t ~f:dependencies_scene_expr in
      dependencies_scene_expr t body
    | MacroVariable {  body; _ } -> dependencies_scene_expr t body
  and dependencies_drawable t drawable = match drawable with
    | OverlayingDrawable {value=drawable;_} -> dependencies_drawable t drawable
    | DrawableShader {value=(shader, obj); _} ->
      let t = dependencies_shader t shader in
      dependencies_obj t obj 
    | DrawableReference reference -> dependencies_reference t reference
  and dependencies_expr t _expr = t
  and dependencies_scene_expr t sexpr = match sexpr with
    | SceneExprObject obj -> dependencies_obj t obj
    | SceneExprShader shader -> dependencies_shader t shader
    | SceneExprModifier modifier -> dependencies_modifier t modifier
    | SceneExprDrawable drawable -> dependencies_drawable t drawable
    | SceneExprTyped (sexpr, _) -> dependencies_scene_expr t sexpr
  and dependencies_shader t shader = match shader with
    | ShaderReference reference -> dependencies_reference t reference
    | Shader {
        vertex_shader={value=vertex_shader;_};
        fragment_shader;
        geometry_shader;
      } ->
      let unwrap = function None -> fun v -> v | Some {value=name; _} -> (fun s -> S.add s name) in
      let shaders = t.shaders |> (fun s -> S.add s vertex_shader)
                    |> unwrap fragment_shader |> unwrap geometry_shader in
      {t with shaders}
  and dependencies_obj t (Object (modifiers, bobj, _)) =
    let t = List.fold ~init:t ~f:dependencies_modifier modifiers in
    dependencies_base_object t bobj
  and dependencies_reference t reference = match reference with
    | DirectReference _ -> t
    | IndirectReference (_, MacroArguments { keyword_args; positional_args }) ->
      List.fold ~init:t ~f:dependencies_scene_arg @@ Map.data keyword_args @ positional_args
  and dependencies_modifier t modifier = match modifier with
    | Texture (_, files, _) ->
      let textures = List.map files ~f:(fun { value; _ } -> value) |> List.fold ~init:t.textures ~f:S.add in
      {t with textures}
    | Redirecting {   drawable; _ } -> dependencies_drawable t drawable
    | ModifierReference reference -> dependencies_reference t reference
    | _ -> t
  and dependencies_base_object t bobj = match bobj with
    | Light _ -> t
    | LightData _ -> t
    | ObjectReference reference -> dependencies_reference t reference
    | ObjectData data -> dependencies_data t data
    | CompoundObject {value=(h,tl);_} ->
      List.fold (h :: tl) ~init:t ~f:dependencies_obj_expr
  and dependencies_scene_arg t sarg = match sarg with
    | ArgReference reference -> dependencies_reference t reference
    | SceneDataExpression expr -> dependencies_expr t expr
    | SceneInlineExpression sexpr -> dependencies_scene_expr t sexpr
  and dependencies_obj_expr t obje = match obje with
    | PlainObject obj -> dependencies_obj t obj
    | ObjectIfThenElse (cond, obj1, obj2) ->
      let t = dependencies_expr t cond in
      let t = dependencies_obj_expr t obj1 in
      begin match obj2 with None -> t | Some obj2 -> dependencies_obj_expr t obj2 end
    | ObjectFor { value=(_, froe, toe, (h,tl)); _ } ->
      let t = dependencies_expr t froe in
      let t = match toe with None -> t | Some toe -> dependencies_expr t toe in
      List.fold (h :: tl) ~init:t ~f:dependencies_obj_expr      
  and dependencies_data t data = match data with
    | Model {value=filename;_} ->
      let models = S.add t.models filename in
      {t with models}
    | _ -> t


end
