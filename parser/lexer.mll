{
open Parser
module B = Buffer

type error =
  | Illegal_character of char
  | Reserved_sequence of string * string option
  | Unterminated_string
  | Unterminated_comment
  | Invalid_literal of string
;;                     

exception Error of error * Location.t

let update_loc lexbuf line absolute chars =
  let open Lexing in
  let pos = lexbuf.lex_curr_p in
  lexbuf.lex_curr_p <- { pos with
                       pos_lnum = if absolute then line else pos.pos_lnum + line;
                       pos_bol = pos.pos_cnum - chars;
  }
}

let newline = ('\013' * '\010')
let blank = [' ' '\000' '\012']
  
let ident_start = ['a' - 'z' 'A' - 'Z' '_']
let ident_char = ['a' - 'z' 'A' - 'Z' '0' - '9' '_']
let ident = ident_start ident_char*

let digit =  ['0' - '9']
let digit_char = ['0' - '9' '_']
let integral_number = digit digit_char*

let number = integral_number ('.' digit_char* )? (['e' 'E'] ['+' '-']? integral_number)?

rule token = parse
  | newline  { update_loc lexbuf 1 false 0; token lexbuf }
  | blank+   {token lexbuf}

  | "/*" { comments 0 lexbuf }
  | "//" { single_comment lexbuf }

  | "true" { BOOL true }
  | "false" { BOOL false }
  | number {
      match int_of_string_opt (Lexing.lexeme lexbuf) with
      | None -> FLOAT (float_of_string (Lexing.lexeme lexbuf))
      | Some i -> INT i
    }
  | "(" {LPAREN}
  | ")" {RPAREN}
  | "[" {LSQBRAC}
  | "]" {RSQBRAC}
  | "<" {LANGLE}
  | ">" {RANGLE}

  | ";" {SEMI}
  | ":" {COLON}
  | "," {COMMA}

  | "&&" {AND}
  | "||" {OR}
  | "*"  {MUL}
  | "/"  {DIV}
  | "%"  {MOD}
  | "+"  {PLUS}
  | "-"  {MINUS}
  | "!"  {NOT}
  | "="  {EQ}

  | "for" {FOR}
  | "in" {IN}
  | "to" {TO}
  | "if" {IF}
  | "then" {THEN}
  | "else" {ELSE}
  | "function" {FUNCTION}
  | "begin" {BEGIN}
  | "end" {END}
  | "var" {VAR}
  | "model" {MODEL}
  | "data" {DATA}
  | "screen" {SCREEN}

  | "texture" {TEXTURE}
  | "uniform" {UNIFORM}
  | "attribute" {ATTRIBUTE}
  | "translate" {TRANSLATE}
  | "scale" {SCALE}
  | "rotate" {ROTATE}
  | "draw_options" {DRAW_OPTIONS}

  | "with" {WITH}
  | "redirecting" { REDIRECTING }
  | "macro" {MACRO}
  | "light_data" {LIGHT_DATA}
  | "light_uniform" {LIGHT_UNIFORM}
  | "overlaying" {OVERLAYING}

  | "int" {TYP_INT}
  | "bool" {TYP_BOOL}
  | "float" {TYP_FLOAT}
  | "texture1D" {TYP_TEXTURE1D}
  | "texture2D" {TYP_TEXTURE2D}
  | "texture3D" {TYP_TEXTURE3D}
  | "texture_cube" {TYP_TEXTURE_CUBE}
  | "numeric_data" {TYP_NUMERIC_DATA}

  | "vec2" { TYP_VEC2 }
  | "vec3" { TYP_VEC3 }
  | "vec4" { TYP_VEC4 }
  | "mat2" { TYP_MAT2 }
  | "mat3" { TYP_MAT3 }
  | "mat4" { TYP_MAT4 }
  | "map" { TYP_MAP }
  | "list" { TYP_LIST }
  | "multi" { TYP_MULTI }
  | "object" { TYP_OBJECT }
  | "modifier" { TYP_MODIFIER }
  | "light" { TYP_LIGHT }
  | "shader" { TYP_SHADER }
  | "Drawable" { TYP_DRAWABLE }
  | "drawable" { DRAWABLE }
  | "layer" {LAYER}

  | '"' { STRING_LITERAL (string (B.create 256) lexbuf)}

  | "_" {TYP_HOLE}
  | '\'' ident {TYP_VARIABLE (Lexing.lexeme lexbuf)}

  | '@' ident { MACRO_IDENTIFIER (Lexing.lexeme lexbuf |> (fun s -> String.sub s 1 (String.length s - 1)))}

  | ident { IDENTIFIER (Lexing.lexeme lexbuf )}
  | (_ as c) { raise (Error (Illegal_character c, Location.{loc_start=Lexing.lexeme_start_p lexbuf; loc_end=Lexing.lexeme_end_p lexbuf})) }

  | eof { EOF }
and string buf = parse
  | [^'"' '\n' '\\']+ {B.add_string buf @@ Lexing.lexeme lexbuf; string buf lexbuf}
  | '\n' {B.add_string buf @@ Lexing.lexeme lexbuf ; update_loc lexbuf 1 false 0; string buf lexbuf }
  | '\\' '"' {B.add_char buf '"'; string buf lexbuf }
  | '\\' {B.add_char buf '\\' ; string buf lexbuf }
  | '"' {B.contents buf}
  | eof { raise (Error (Unterminated_string, Location.{loc_start=Lexing.lexeme_start_p lexbuf; loc_end=Lexing.lexeme_end_p lexbuf})) }
and comments level = parse
  | "*/" { if level = 0 then token lexbuf else comments (level - 1) lexbuf}
  | "/*" { comments (level + 1) lexbuf }
  | _ { comments level lexbuf }
  | eof { raise (Error (Unterminated_comment, Location.{loc_start=Lexing.lexeme_start_p lexbuf; loc_end=Lexing.lexeme_end_p lexbuf})) }
and single_comment = parse
  | "\n" { token lexbuf }
  | "\\\n" { single_comment lexbuf}
  | _ { single_comment lexbuf }
