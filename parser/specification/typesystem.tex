This section presents the type system and data model of SDL,
explaining the basic structure of how SDL encodes graphics pipelines.

The SDL type system makes a hard distinction between so-called
``data''-types, used to type the data passed to shaders, and
``scene''-types, used to enforce the structuring of the graphics
pipeline.
%
We note that while generally these two families of types are distinct
and can not be mixed, both can freely be used as variables and
parameters.


\newcommand{\bboy}[1]{\textbf{\texttt{#1}}} \newcommand{\dsep}{~|~}
\begin{figure}[h]
  \centering
  \[
  \begin{array}{l l l l }
    \text{(scalar data types)} & \bboy{sdt} & ::= & \texttt{bool} \dsep \texttt{int} \dsep \texttt{float} \\
    \text{(non-scalar data types)} & \alpha ~ \bboy{nsdt}
                                     & ::= &
                                            \alpha ~  \texttt{vec2} \dsep
                                            \alpha ~ \texttt{vec3} \dsep
                                       \alpha ~ \texttt{vec4}  \\
    & & &  \dsep \alpha ~ \texttt{mat2} \dsep
                                            \alpha ~ \texttt{mat3} \dsep
                                            \alpha ~ \texttt{mat4} \\
    \text{(texture types)} & \bboy{tt} & ::= &  \bboy{texture1d} \dsep \bboy{texture2d} \dsep \bboy{texture3d} \\
    \text{(pure data types)} & \bboy{pdt} & ::= &  \bboy{sdt} \dsep \bboy{sdt} ~ \bboy{nsdt} \\
    \text{(data types)} & \bboy{dt} & ::= &  \bboy{sdt} \dsep \bboy{sdt} ~ \bboy{nsdt}
                                                      \dsep \bboy{dt} ~ \bboy{list}  \\
  \end{array}
\]
\caption{SDL data types}
\label{fig:sdl-data-types}
\end{figure}

Figure \ref{fig:sdl-data-types} presents the data types supported by
SDL - in short, they are intended to encompass most of the primitive
types used in GLSL shaders.
%
As can be seen from the code snippets in Listing
\ref{lst:primitivt-data-type}, expressions corresponding to these
types have a standard form, notably making a distinction between
commas and semicolons to separate matrices from lists.
%
We will go into more detail about the exact synatax and supported
operations of these expressions in the later sections, but provide a
reference here for example.

\begin{listing}[h]
\centering
\begin{minipage}{0.45\linewidth}
\centering
\begin{lstlisting}[language=Sdl]
(10 : int)

([10, 10, 10] : int vec3)

([10, 10, 10] :  float vec3)
\end{lstlisting}
\end{minipage}
\begin{minipage}{0.45\linewidth}
\centering
\begin{lstlisting}[language=Sdl]
([[10, 10],
  [10, 10]]  : float mat2)

([ [10, 10];
   [10, 10];
   [10, 10]; ]: float vec2 list)
\end{lstlisting}
\end{minipage}
\caption{Primitive data expressions}
\label{lst:primitivt-data-type}
\end{listing}

The other type of types supported by SDL are scene-types which encode
the core rendering steps (and data) that form a rendering pipeline.

\begin{figure}[h]
  \centering
  \[
  \begin{array}{l l l l }
    \text{(scene types)} & \bboy{st} & ::= & (\beta,\gamma) ~ \texttt{object} \dsep
                                             (\beta,\gamma) ~ \texttt{modifier} \dsep
                                             \gamma ~ \texttt{light}  \dsep
                                             (\alpha, \text{'count}) ~ \texttt{shader}  \\
& & &  \dsep (\gamma, \text{'count}) ~ \texttt{drawable} \dsep \alpha ~ \texttt{multi} \\
    \text{(data shape)} & \bboy{ds} & ::= & (\bboy{pdt} ~ \bboy{map}, \bboy{pdt} ~ \bboy{map}, \bboy{tt} ~ \bboy{map})  \\
    \text{(mapping)} & \alpha ~ \bboy{map} & ::= & \langle id_1 = \alpha_1, \dots, id_n = \alpha_n \rangle  \\
  \end{array}
\]
\caption{SDL scene types}
\label{fig:sdl-scene-types}
\end{figure}
Figure \ref{fig:sdl-scene-types} lists the main scene data types -
\texttt{object}, \texttt{modifier}, \texttt{light}, \texttt{shader},
\texttt{sequence} and \texttt{drawable}.
%

At high level, semantics of these type are defined as follows:
\begin{itemize}
\item \texttt{object} - represents raw data\footnote{In terms of GLSL
    terminology, we mean \emph{both} attributes, textures and
    uniforms} provided to a shader, with the type parameter $\beta$
  representing the general ``shape'' of the outputs.

  The $\gamma$ parameter, used here and in the other types, represents
  the shape of lighting data, which is threaded through the other data
  types.  These are incorporated to allow capturing non-structural
  dependencies in the pipeline.

\item \texttt{modifier} - represents the type of data modifiers which
  captures data that is not necessarily sufficient to be used for a
  standalone draw call, but can be used to attach or modify the data
  on an existing data object.

\item \texttt{light} - represents the aforementioned non-structural
  ``lighting'' data which is then threaded through the other types -
  note that there are no specific constraints that restrict the usage
  of these components to lighting only, we only use lighting for lack
  of a better name.

%
  For an example situation where this is important, consider rendering
  lights - typically, when rendering lighting or shadows, one needs to
  loop over the light sources separate to the objects being drawn,
  however in practice it is more natural to define light sources as
  part of a data object (i.e a torch with a light on the top).


\item \texttt{shader} - represents a shader in SDL, with the new type
  parameter $\alpha$ representing the shape of required input data,
  and $\text{'count}$ representing the number of drawable outputs from
  the shader.

\item \texttt{drawable} - represents some standalone drawable output -
  a term of this type represents a component that has all the data
  required to be rendered.

\item \texttt{multi} - represents a sequence of one or more data
  outputs, which must first be aggregated into a single drawable at some point.
\end{itemize}

We have referred to the type parameters in these types as representing
the ``shape'' of the required or output data for each component.
%
In particular, these type parameters explicitly capture the names and
types of of data exported by a shader - acting as a sort of mapping,
for which we will define subtyping relations later.  
%

We have used single type parameters here for simplicty, however in
actuality, these type parameters must typically be concretised by a
triple of mappings, each of the form:
\[
  \langle \text{var}_1 : \texttt{type}_1, \dots, \text{var}_n : \texttt{type}_1 \rangle
\]
Together these mappings capture the names of the attribute, uniform
and texture data that a particular drawing produces or requires as
input.


% Note that, while in a normal programming language, this form of typing
% information may be concerning, as it introduces control flow
% dependence on the types of terms, which could make typechecking
% undecidable, in SDL this is not a concern, as SDL requires that
% programs are terminating (no unbounded loops or recursion), ensuring
% that prorams can always be type checked.

We will explore these semantics further as we move on to discuss the
expressions of SDL, but for now, we provide a couple of examples
(Listing \ref{lst:scene-exprs}) to introduce the reader to the typical
usage.

\begin{listing}[h]
\begin{lstlisting}[language=Sdl]
(model("cube.stl")
        :  ((@*$\langle$*@ vertex_position : vec3, vertex_normals: vec3 @*$\rangle$*@, @*$\langle  \rangle$*@,  @*$\langle  \rangle$*@), @*$\gamma$*@) object)

(shader(vertex="v.glsl",fragment="f.glsl")
        :  ((@*$\langle$*@ vertex_position : vec3, vertex_normals: vec3 @*$\rangle$*@, @*$\langle  \rangle$*@, @*$\langle  \rangle$*@), (@*$\langle \rangle$*@, @*$\langle  \rangle$*@, @*$\langle  \rangle$*@, 1) ) shader)
\end{lstlisting}
\caption{Example scene expressions presenting the type of an  object and a shader}
\label{lst:scene-exprs}
\end{listing}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
