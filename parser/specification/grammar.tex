Having covered the basic types and data model of SDL, I will now
specify the formal syntax of SDL, briefly discussing the motivations
of each of the structural components.

We can start with a synatx of data expressions.
\begin{grammar}
<expr> ::= true
\alt false
\alt <number>
\alt <identifier>
\alt `[' <aggregate_expr> `]'
\alt `[' <vector_expr> `]'
\alt <expr> `[' <expr> `]'
\alt <expr> `*' <expr>
\alt <expr> `/' <expr>
\alt <expr> `\%' <expr>
\alt <expr> `+' <expr>
\alt <expr> `-' <expr>
\alt <expr> `&&' <expr>
\alt <expr> `||' <expr>
\alt `+' <expr>
\alt `-' <expr>
\alt `!' <expr>
\alt `(' <expr> `)'
\alt `if' <expr> `then' expr `else' expr
\alt <identifier> `(' <arguments_call> `)'
\alt <expr> `:' <type_expr>

<aggregate_expr> ::= seperated_list(`,', <expr>)
\alt <list_comprehension>

<vector_expr> ::= seperated_list(`;', <expr>)

<list_comprehension> ::= <expr> `for' <identifier> `in' <expr> `to' <expr>

<arguments_call> ::= seperated_list(`,', <argument>)

<argument> ::= <expr>
\alt <identifier> `=' <expr>
\end{grammar}
As can been seen, SDL supports a fairly simple data expression syntax,
supporting the basic numeric and logical operators.

As mentioned earlier, SDL makes a distinction between lists (arbitrary
length) and matrices/vectors (fixed length) by means of commas for
lists versus semicolons for vectors.
%
The justification for this is mostly just to simplify the typing
rules, and remove control flow dependence on the types of expressions
- if we dictated what was and was not a vector by whether we could
infer a specific fixed length, then this would lead to supurious
changes in types as other variables were changed.


As scenes are expected to be fairly static, SDL does not support
arbitrary logical expressions - note that there are no relational
operators, and so boolean expresssions can only be constructed from
literals and thereby can certainly be decided efficiently during type
checking.
%
The aim here is to not support general purpose programming, but ease
of life functionality by supporting the use of variable/argument flags
for optional transformations on the data.

The numeric operators are overloaded in the natural way for the shapes
non-scalar data and support broadcasting - this will be examined in
more detail further in the subsequent sections.
\begin{lstlisting}[language=Sdl,xleftmargin=.4\textwidth]
  1 + 1 = 2
  [1; 2] + [1; 2] = [2; 4]
\end{lstlisting}\vspace{-2em}
Notice that the expressions also include function calls - alongside a
suite of standard mathematic operations ($sin, cos,$ etc.), SDL also
allows users to define their own helper functions to refactor out
repeated code.
\begin{lstlisting}[language=Sdl,,xleftmargin=.4\textwidth]
function incr(x, by : int = 1) begin x + 1 end
\end{lstlisting}\vspace{-2em}
The grammar rules for these kinds of definitions are as follows:
\begin{grammar}
<data_declaration> ::=
  `function'  <identifier> `(' separated_list(`,', <argument>) `)' opt(`:' <type_expr>) `begin' <expr> `end'
\alt `var' <identifier> opt(`:' <type_expr>) = <expr>

<argument> ::= <identifier> opt(`,' <type_expr>)
\alt <identifier> opt(`:' <type_expr>) `=' <expr>
\end{grammar}

I will now move on to scene expressions - these are the main meat of
the SDL language, allowing users to easily define and change graphics
pipelines in a declarative manner.

The most primitive of the scene operations is the raw data object -
this allows a user to either load in an object file, specify raw
attribute data or generate a plain quad over specified screen
coordinates.
\begin{grammar}
<data> ::= `model' `(' <string_literal>  `)'
\alt `data' `(' <arguments_call>  `)'
\alt `screen' `(' <arguments_call>  `)'
\end{grammar}

An expression in this form simply defines a data object using the data
found in the model file combined with a set of implicit standard
components - (i.e position uniforms).
%
SDL allows a user to modify and extend a data object with additional
attributes, uniforms and textures as well as customizing the world
position using modifiers, a la openscad.
\begin{grammar}
<modifier> ::= `texture'  `(' <identifier>, <string_literal> opt(`,' <arguments_call>) `)'
\alt `uniform' `(' <identifier>,  <expr> `)'
\alt `attribute `(' <identifier>,  <expr> `)'
\alt `translate `(' <position_args> `)'
\alt `scale `(' <arguments_call9> `)'
\alt `rotate `(' <arguments_call> `)'
\alt <redirect_modifier>
\end{grammar}

Of note, the texture modifier specifies a texture to be passed to the
sampler uniform with the name given by identifier. The texture source
file and identifier name must be defined explicitly in the code, and
can not be the outcome of arbitrary expressions - this is a consistent
theme throughout all the scene-level SDL expressions, and is a
required to ensure that typing is control flow independent.
%
While this does constrain the types of programs that SDL can
represent, given the main aim of SDL is to  prototype program pipelines,
which are generally static, this is not a major concern.

With these modifiers defined, I will now define objects:
\begin{grammar}
<base_object> ::= <data>
| <light>
| `object' `(' <scene_args_list> `)'
| `begin' separated_list(`;', <scene_expr>) opt(`;') `end'

<object> ::= list(<modifier>) <base_object>

<light> ::= `light' `(' <identifier> `,' <arguments_call> `)'
\end{grammar}
As you can see, an object takes the form of a base object (either a
data specification, a light or a compound object) prefixed by a
sequence of zero or more modifiers.


The terms of the type \lstinline[language=Sdl]{object(...)} are used
to allow the use of so-called variables and expressions at the scene
level - I will present the specifications of these terms later, but
will henceforth refer to these kinds of structures as ``macro''
expressions \footnote{the idea being to emphasize that macro
  expressions are not computations, but rather terms that can be
  unwrapped into a static pipeline}.

The next important expression type is the shader and its
combinators. These expressions are intended to be combined with a data
object to produce a drawable.
\begin{grammar}
<shader> ::= `shader' `(' <arguments_call> `)'
\alt `shader' `(' <scene_args_list> `)'

<shader_drawable> ::= `with' <shader> <object>
\end{grammar}

The next type of operation is another combinator, that allows
redirecting the output of a shader into more data - in terms of types,
this converts a drawable to a modifier.
\begin{grammar}
<redirect_modifier> ::=  `redirecting' `(' <arguments_call> `)' <drawable>

<drawable> ::= <shader_drawable>
\alt `drawable' `(' <scene_args_list> `)'
\end{grammar}

Combining all these components together, it is now possible to define the scene-level expressions
that are supported:

\begin{grammar}
<scene_expr> ::=  <identifier>
\alt <identifier> `(' <scene_args_list> `)'
\alt <object>
\alt <shader>
\alt <drawable>
\alt <modifier>
\alt <scene_expr> `:' <type_expr>
\alt <scene_expr> `<:' <type_expr>
\alt `if' <expr> `then' <scene_expr> opt(`else' <scene_expr>)
\alt `for` <identifier> `in' <expr> `to' <expr> `then' <scene_expr> opt(`else' <scene_expr>)


<scene_args_list> ::= separated_list(`,', <scene_arg>)

<scene_arg> ::= <scene_expr>
\alt <identifier> `=' <scene_expr>
\end{grammar}

The final part of the specification are the top level statements that form a program:
\begin{grammar}
<scene_declaration> ::= `macro' `var' <identifier> opt(`:' <type_expr>) `=' <scene_expr> `;'
\alt `macro' `function' <identifier> `(' separated_list(`,', <macro_argument>) `)' `begin'  separated_list(`;', <scene_expr>) opt(`;') `end'
\alt <drawable> `;'

<macro_argument> ::= <identifier> opt( `:' <type_expr>)
\alt <identifier> opt( `:' <type_expr>) `=' <scene_expr> 
\end{grammar}

This encompasses most of the grammar of SDL - all that remains is the
syntax of types. As the types section has already covered these
components, there is no need for much additional commentary.
\begin{grammar}
<type_expr> ::= `bool'
  \alt `int'
  \alt `float'
  \alt  `texture1D'
  \alt `texture2D'
  \alt `texture3D'
  \alt texture_cube'
  \alt 'numeric_data'
rence  \alt `_'
  \alt `'' <identifier>
  \alt <integer>
  \alt `(' <type_expr> `,' <type_expr> `)'
  \alt `<' separated_list(`,', <type_map>) `>'
  \alt <type_expr> <type_name>

<type_name> ::= `vec2'
  \alt `vec3'
  \alt `vec4'
  \alt `mat2'
  \alt `mat3'
  \alt `mat4'
  \alt `list'
  \alt `multi'
  \alt `object'
  \alt `modifier'
  \alt `light'
  \alt `shader'
  \alt `drawable'
  
<type_map> ::= <identifier> `=' <type_expr>
\end{grammar}

As a final comment, note that the grammar is designed to be as
permissive as possible, to allow offloading many syntax errors from
the parser generator to the ast checker (should allow for better
syntax error messages, as the entire AST (even if invalid) will be
known).

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
