\subsection{Basic drawing}
\label{sec:simple-drawing}
To get the ball rolling, consider how to implement some basic drawing
operations in SDL.

The program in Listing \ref{lst:case-study-basic} uses a single shader
to render two objects straight to the screen. The objects themselves
are loaded from model and object files stored elsewhere.
%
After loading the objects, the program can apply various
transformations to objects themselves to move them around in world
space.
%
Finally, once the objects are positioned, they are fed to a shader to
draw to the screen.

\begin{listing}[H]
\begin{lstlisting}[language=Sdl, numbers=left]
with shader(
     vertex_shader="/shaders/vert.glsl", 
     fragment_shader="/shaders/frag.glsl") begin

       scale(0.5,0.5,0.5)
          texture(model_texture, "/Pictures/model_texture.png")
              model("/models/cube.obj");

       scale(0.5,0.5,0.5)
          texture(model_texture, "/Pictures/model_texture.png")
              model("/models/cube.obj");

end
\end{lstlisting}
\caption{Basic use of SDL}
\label{lst:case-study-basic}
\end{listing}

The syntax of object definitions themselves can be nested and composed
to effectively implement a form of grouping, allowing transforming
multiple elements in sync.

\begin{listing}[H]
\begin{lstlisting}[language=Sdl, numbers=left]
with shader(
     vertex_shader="/shaders/vert.glsl", 
     fragment_shader="/shaders/frag.glsl") 
       translate(0.3, 0.5, 0.8)
       begin
             scale(0.5,0.5,0.5)
                texture(model_texture, "/Pictures/model_texture.png")
                    model("/models/cube.obj");

             scale(0.5,0.5,0.5)
                texture(model_texture, "/Pictures/model_texture.png")
                    model("/models/cube.obj");
       end
\end{lstlisting}
\caption{Object composition in SDL}
\label{lst:case-study-object-comp}
\end{listing}

\subsection{Clipping}
\label{sec:clipping}
Listing \ref{lst:case-study-object-clipping} illustrates how a scene
can implement clipping.

\begin{listing}[H]
\begin{lstlisting}[language=Sdl, numbers=left]
with shader(
         vertex_shader="/shaders/vert.glsl", 
         fragment_shader="/shaders/frag.glsl") 
    redirecting stencil
        with shader(
             vertex_shader="/shaders/basic_vert.glsl", 
             fragment_shader="/shaders/basic_frag.glsl") 
               translate(0.3, 0.5, 0.8)
               begin
                     scale(0.5,0.5,0.5)
                            model("/models/cube.obj");
               end
         begin
               scale(0.5,0.5,0.5)
                      model("/models/cube.obj");
         end
\end{lstlisting}
\caption{Clipping in SDL}
\label{lst:case-study-object-clipping}
\end{listing}


The idea with this program is to first draw the objects to clip by
using some basic shader (line 5). The program then redirects only the
stencil output of the shader (creating a modifier with the stencil
data). The program then draws some other object (line 13) using
another shader (line 1), while clipping using the output of the
redirection.



\subsection{Deferred rendering}
\label{sec:deferred-rendering}

Listing \ref{lst:case-study-deferred-rendering} presents how SDL can be used to encode a
deferred rendering pipeline with little hassle.
\begin{listing}[h]
\begin{lstlisting}[language=Sdl, numbers=left]
overlaying with shader(vertex_shader="/shaders/draw_vert.glsl", fragment_shader="/shaders/draw_frag.glsl")
     redirecting textures=[position, normals, colours]; depth=d; normals 
         with shader(
              vertex_shader="/shaders/def_vert.glsl", 
              fragment_shader="/shaders/def_frag.glsl") begin
                scale(0.5,0.5,0.5)
                   texture(model_texture, "/Pictures/model_texture.png")
                       model("/models/cube.obj");

                scale(0.5,0.5,0.5)
                       light(point_light);

                scale(0.5,0.5,0.5)
                       light(spot_light);
         end
     light_data(point_light, light_volume);
\end{lstlisting}
\caption{Use of SDL for deferred rendering}
\label{lst:case-study-deferred-rendering}
\end{listing}

The idea with this code example is that we first render the objects in
the scene using a preprocessing shader (line 3) that outputs the
position, normal and colour data to 3 separate outputs.
%
This term has type \lstinline[language=Sdl]{(`gamma, 3) drawable}, as
it is a standalone component that could be drawn to screen.  We then
redirect these three outputs (line 2) into textures, returning a term
of type \lstinline[language=Sdl]{modifier}, with the three output
textures now also bound.  To convert this modifier into an object, we
specify the draw region through
\lstinline[language=sdl]{light_position}, which repeats the draw
operation for each light in the context, using the light volume as the
draw region.
%
Finally, we perform the deferred rendering pass by simply running a
drawing shader (line 1) over the result - as the data itself is
generated by looping over light data, the resulting drawable is
encapsulated in a multi wrapper - we dispell this wrapper by the
overlaying command which causes the previous draw operation to be done
repeatedly to the same screen.


\subsection{Shadow mapping}
\label{sec:shadow-mapping}
Listing \ref{lst:case-study-shadow-mapping} illustrates how SDL can
also be used to easily capture complex pipeline steps such as
shadowmapping.
%
The idea with this program is to first draw the objects in question
from the perspective of each light source in the scene, using
\lstinline[language=Sdl]{light_uniform} to extract the light position
vector into a uniform used in the shader. Once again, as this process
is repeated for each light in the scene, the resulting term is
encapsulated in a \lstinline[language=Sdl]{mutli} wrapper.
%
Then, we take the result of the drawing and use
\lstinline[language=Sdl]{redirecting} to convert it into a texture
modifier, and set the draw region to the entire screen.
%
Finally, to draw the resulting object, we use another shader, and
prefix it with overlaying to repeat the draw operation onto a single
screen.


\begin{listing}[h]
\begin{lstlisting}[language=Sdl]
overlaying with shader(
    vertex_shader="/shaders/shadow_map.glsl",
    fragment_shader="/shaders/shadow_map.glsl"
)
     redirecting data=[shadow_map]
         with shader(vertex_shader="/shaders/basic_vert.glsl")
           light_uniform(light_position, spot_light)
           begin
                scale(0.5,0.5,0.5)
                   texture(model_texture, "/Pictures/model_texture.png")
                       model("/models/cube.obj");

                scale(0.5,0.5,0.5)
                       light(point_light);

                scale(0.5,0.5,0.5)
                       light(spot_light);
           end
     screen()      
\end{lstlisting}
\caption{Use of SDL for Shadow mapping}
\label{lst:case-study-shadow-mapping}
\end{listing}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
