[@@@warning "-39"]
(* open Sexplib.Std *)
open Core
module Location = struct
  include Location
  let equal (_a:t) (_b: t) = true

  let equal_loc eq (a: 'a loc) (b: 'b loc)  = eq a.value b.value

  let compare (_a:t) (_b:t) = 0


  let compare_loc f (a: 'a loc) (b: 'a loc) =
    f a.value b.value


  let t_of_sexp (s: Sexp.t) : t =
    Unit.t_of_sexp s;
    {loc_start=Lexing.{pos_fname=""; pos_lnum=0;pos_cnum=0;pos_bol=0};
                                   loc_end=Lexing.{pos_fname=""; pos_lnum=0;pos_cnum=0;pos_bol=0} }


  let sexp_of_t (_t: t) = Unit.sexp_of_t ()

  let loc_of_sexp f (s: Sexp.t) : 'a loc =
    {value=f s; loc={loc_start=Lexing.{pos_fname=""; pos_lnum=0;pos_cnum=0;pos_bol=0};
                                   loc_end=Lexing.{pos_fname=""; pos_lnum=0;pos_cnum=0;pos_bol=0} }}

  let sexp_of_loc f (loc: 'a loc) =
    f loc.value


end

open Location

type binop =
    Mul
  | Div
  | Mod
  | Plus
  | Minus
  | And
  | Or
and preop =
  | UPlus
  | UMinus
  | UNot
and num =
  | Int of int
  | Float of float
and type_name =
   TypeVec2 of Location.t
  | TypeVec3 of Location.t
  | TypeVec4 of Location.t
  | TypeMat2 of Location.t
  | TypeMat3 of Location.t
  | TypeMat4 of Location.t
  | TypeList of Location.t
  | TypeMulti of Location.t
  | TypeObject of Location.t
  | TypeModifier of Location.t
  | TypeLight of Location.t
  | TypeShader of Location.t
  | TypeDrawable of Location.t
  | TypeMapTag of Location.t
and type_expr =
  | TypeInt of Location.t
  | TypeBool of Location.t
  | TypeFloat of Location.t
  | TypeTexture1D of Location.t
  | TypeTexture2D of Location.t
  | TypeTexture3D of Location.t
  | TypeTextureCube of Location.t
  | TypeHole of Location.t
  | TypeVariable of string loc
  | TypeNumber of int loc
  | TypeNumericData of Location.t
  | TypeProduct of (type_expr list * type_name) loc
  | TypeMap of ((string loc * type_expr) loc) list loc
and expr =
    Exp_bool of bool loc
  | Exp_number of num loc
  | Exp_variable of string loc
  | Exp_binop of (binop * expr * expr) loc
  | Exp_preop of (preop * expr) loc
  | Exp_index of (expr * expr) loc
  | Exp_vector of expr list loc
  | Exp_list of expr list loc
  | Exp_list_comprehension of (expr * string loc * expr * expr option) loc
  | Exp_function_call of (string loc * (string loc option * expr) list) loc
  | Exp_if_then_else of (expr * expr * expr) loc
  | Exp_with_type of (expr * type_expr) loc
and data_arg =
  | ArgumentString of string loc
  | ArgumentExpression of expr
and data_form =
  | Model
  | Data
  | Screen
and data =
  | DataComponent of (data_form * (string loc option * data_arg) list) loc
and modifier_form = 
  | Texture
  | Uniform
  | Attribute
  | Translate
  | Scale
  | Rotate
  | DrawOptions
and modifier =
  | ModifierReference of (string loc option * scene_arg) list loc
  | BasicModifier of 
      (modifier_form * (string loc option * data_arg) list) loc
  | LightModifier of 
      expr list loc
  | RedirectingModifier of
      ((string loc option * expr) list * drawable) loc
and base_object =
  | Light of (string loc option * data_arg) list loc
  | LightData of expr list loc
  | ObjectReference of (string loc option * scene_arg) list loc
  | ObjectData of data
  | CompoundObject of scene_expr list loc
and obj =
  | Object of (modifier list * base_object) loc
and
 declaration =
  | DataFunction of
      { identifier: string loc;
        arguments: (string loc * type_expr option * expr option) list;
        typ: type_expr option;
        body: expr
      }
  | DataVariable of
      { identifier: string loc; typ: type_expr option; body: expr }
  | MacroFunction of 
      { identifier: string loc;
        arguments: (string loc * type_expr option * scene_expr option) list;
        typ: type_expr option;
        body: scene_expr
      }
  | MacroVariable of
      { identifier: string loc; typ: type_expr option; body: scene_expr }
and shader =
  | BasicShader of (string loc option * scene_arg) list loc
and drawable =
  | OverlayingDrawable of drawable loc
  | DrawableShader of (shader * obj) loc
  | DrawableReference of (string loc option * scene_arg) list loc
and scene_arg = 
  | SceneString of string loc
  | SceneDataExpression of expr
  | SceneInlineExpression of scene_expr
and scene_expr =
  | SceneExpr_variable of string loc
  | SceneExpr_macro_call of (string loc * (string loc option * scene_arg) list) loc
  | SceneExpr_object of obj
  | SceneExpr_shader of shader
  | SceneExpr_drawable of drawable
  | SceneExpr_modifier of modifier
  | SceneExpr_with_type of (scene_expr * type_expr) loc
  | SceneExpr_if_then_else of (expr * scene_expr * scene_expr option) loc
  | SceneExpr_for of (string loc * expr * expr option * scene_expr list) loc
and statement =
  | Declaration of declaration
  | Drawable of drawable
  | LayeredDrawable of ((string loc option * data_arg) list loc * drawable)
[@@deriving show, eq, sexp, ord, hash]
