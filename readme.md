# GSDL - Gop-scene-definition-language
This is a langugage for implementing shader pipelines/building 3D rendered scenes.

## Features

- Well *clap* typed *clap* GSDL *clap* programs *clap* do not lead to OpenGL errors (or at least pipeline ones)
- Easy exporting projects to images (scripts provided to automate importing into Krita)
- Abstracts away all the low level OpenGL commands - express your rendering pipeline in terms of buffer redirections and objects directly.
- Type expression at point.
- Functions/macros to automate repetitive components.
- Hot reloading - quickly prototype changes to shaders

## Example programs

The following GSDL program implements a deferred rendering pipeline:

```
macro var texture_shader = shader(
     vertex_shader="/home/kirang/Documents/code/ocaml/stl-reader/shaders/textured.vert",
     fragment_shader="/home/kirang/Documents/code/ocaml/stl-reader/shaders/textured.frag"
     );

macro var output_shader = shader(
     vertex_shader="/home/kirang/Documents/code/ocaml/stl-reader/shaders/simple.vert",
     fragment_shader="/home/kirang/Documents/code/ocaml/stl-reader/shaders/simple.frag"
     );


overlaying with shader(@output_shader)
     redirecting into=[model_texture]
     with shader(@output_shader)
     begin
         attribute(vertex_position, [10; 10; 10])
            attribute(vertex_normal, [10; 10; 10])
                uniform(random_value, 10)
                light(spot_light, [10.0; 10.0; 10.0], 0.0);

         model("/home/kirang/Documents/code/scad/cube.obj");
     end
     light_data(spot_light, vertex_position);
```

