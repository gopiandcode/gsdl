Gop-Scene-Definition Language
==================================
This section provides an overview of the specifications of GSDL.


.. toctree::
   :maxdepth: 2

   syntax
   types
   semantics
