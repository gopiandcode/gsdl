.. GopSDL documentation master file, created by
   sphinx-quickstart on Fri Jun 26 04:27:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GopSDL's documentation!
==================================

Gop-Scene-Definition-Language (GSDL) is a staticaly typed declarative
language for quickly prototyping OpenGL GPU pipelines. Abstracting
away the tedium and complexity of managing GPU resources, GSDL aims to
make quickly developing complex shaders easy.

Example usecase
=============================================
A typical usage of GSDL mode would be as follows:

[TODO]: insert pipeline image




.. toctree::
   :maxdepth: 2
   :caption: Contents:

   setup
   language/index
