open Gg
open Core
open Utils
open Graphics_utils


module Object = struct

  module T = Graphics_utils.GenericShader.Types.Data.Numeric
  module I = Graphics_utils.GenericShader.Types.Data.Texture
  module Types = Graphics_utils.GenericShader.Types
  
  module C = Gsdl.CompiledParsetree.OfTyped

  let pp_v4 = V4.pp
  let pp_v3 = V3.pp
  let pp_v2 = V2.pp
  let pp_m4 = M4.pp
  let pp_m3 = M3.pp
  let pp_m2 = M2.pp

  type geometry_id = Geometry_synchronizer.ID.t [@opaque]
  [@@deriving show, sexp]
  type shader_id = Shaders.BasicShaderSynchronizer.ID.t [@opaque]
  [@@deriving show]

  type data =
    | Model of geometry_id
    | Data of (string * T.data) list
    | Screen of {width: float option; height: float option;} 
  [@@deriving show]
  type shader = Shader of shader_id
  [@@deriving show]

  type ctx = Gsdl.CompiledParsetree.OfTyped.ctx =  {
    camera_position : v3; camera_front : v3; camera_yaw : float; camera_pitch : float;
  }
  [@@deriving show]

  type 'a value = ValueIndependent of 'a | ValueContext of (ctx -> 'a)
  [@@deriving show]

  let equal_value eq (v1: 'a value) (v2: 'b value) = match v1,v2 with
    | ValueIndependent v1, ValueIndependent v2 -> eq v1 v2
    | _ -> false

  type dynamic_value =
      DynValueIndependent of T.data
    | DynValueContext of (ctx -> T.data)
  [@@deriving show]
  type transform = Scale of v3 value | Rotate of m4 value | Translate of v3 value [@@deriving show]
  type wrap_option = C.wrap_option [@@deriving show, sexp, hash, ord]
  type filter_option = C.filter_option [@@deriving show, sexp, hash, ord]
  type texture_option =
    | TextureBaseLevel of int value | TextureMaxLevel of int value
    | TextureBorderColor of v4 value | WrapS of wrap_option | WrapT of wrap_option
    | MinFilter of filter_option | MagFilter of filter_option [@@deriving show]
  type blend_func = C.blend_func [@@deriving show,sexp,sexp, hash,ord]
  type comparison_func = C.comparison_func [@@deriving show,sexp,hash,ord]
  type draw_option =
    | AlphaTest of bool | BlendTest of bool | DepthTest of bool | StencilTest of bool
    | BlendColor of v4 value | BlendFunction of blend_func * blend_func
    | StencilFunction of comparison_func * int value * int value
    | DepthFunction of comparison_func | AlphaFunction of comparison_func * float value
  [@@deriving show]
  type texture_type = C.texture_type = TyTexture1D | TyTexture2D | TyTexture3D | TyTextureCube
  [@@deriving show, eq,ord,hash]
  type texture = {
    name: string; textures: Image_synchronizer.ID.t list; [@opaque] typ: texture_type;
    texture_options: texture_option list;
  } [@@deriving show]
  type uniform = Uniform of {name:string; value: dynamic_value} [@@deriving show]
  type attribute = Attribute of {name:string; value: T.data} [@@deriving show]
  type light_uniform = LightUniform of { binding: string; value: (v3 * ctx -> T.data) }
  [@@deriving show]

  type obj = modifiers * data
  and modifiers = {
    transforms: transform list; uniforms: uniform list; attributes: attribute list;
    draw_options: draw_option list; textures: texture list; redirection: redirection list;
  }
  and redirection = Redirection of {
      id: int; binding_names: string list; depth: bool; stencil: bool; drawable: drawable;
    }
  and drawable =
      BasicDrawable of { shader: shader; objects: obj list; }
    | LightDrawable of {
        shader: shader; lights: light list; light_data: light_data;
      }
  and light = Light of {
      modifiers: modifiers;
      position: v3 value;
      size: float value;
    }
  and light_data =
      LightVolume of { uniforms: light_uniform list; }
    | LightObjects of (light_uniform list * obj) list
  [@@deriving show]

  type program = (string option * drawable) list
  [@@deriving show]

  type dep =
    | TextureDep of Image_synchronizer.ID.t * Image_synchronizer.Timestamp.t
    | ShaderDep of Shaders.BasicShaderSynchronizer.ID.t * Shaders.BasicShaderSynchronizer.Timestamp.t
    | MeshDep of Geometry_synchronizer.ID.t * Geometry_synchronizer.Timestamp.t
    | ScreenDep

  module Dependencies = struct

    let get_shader_dep (_,(_, sync),_) sid =
      let open Comp.Syntax in
      let+ timestamp = Shaders.BasicShaderSynchronizer.get_timestamp sid sync in
      ret @@ (ShaderDep (sid, timestamp))

    let get_texture_dep ((_, sync),_,_) sid =
      let open Comp.Syntax in
      let+ timestamp = Image_synchronizer.get_timestamp sid sync in
      ret @@ (TextureDep (sid, timestamp))

    let get_model_dep (_,_,(_, sync)) sid =
      let open Comp.Syntax in
      let+ timestamp = Geometry_synchronizer.get_timestamp sid sync in
      ret @@ (MeshDep (sid, timestamp))

    let rec of_drawable state (drawable: drawable) =
      let open Comp.Syntax in
      match drawable with
      | BasicDrawable { shader; objects } ->
        let+ shader_deps = of_shader state shader in
        let+ obj_deps = List.map ~f:(of_obj state)  objects |> Comp.all in
        let obj_deps = List.concat obj_deps in
        ret @@ (shader_deps @ obj_deps)
      | LightDrawable { shader; lights; light_data } ->
        let+ shader_deps = of_shader state shader in
        let+ light_deps = List.map ~f:(of_light state)  lights |> Comp.all in
        let light_deps = List.concat light_deps in
        let+ light_data_deps = of_light_data state light_data in
        ret @@ (shader_deps @ light_deps @ light_data_deps)
    and of_shader state (Shader sid) =
      let open Comp.Syntax in
      let+ dep = get_shader_dep state sid in
      ret @@ [dep]
    and of_light_data state light_data =
      let open Comp.Syntax in match light_data with
      | LightVolume _ -> ret @@ []
      | LightObjects objs ->
        let+ deps = List.map ~f:snd objs |> List.map ~f:(of_obj state) |> Comp.all in
        let deps = List.concat deps in
        ret @@ deps
    and of_light state light =
      let open Comp.Syntax in match light with
      | Light { modifiers; _ } -> of_modifiers state modifiers
    and of_obj state (modifiers, data) =
      let open Comp.Syntax in 
      let+ modifier_deps = of_modifiers state modifiers in
      let+ data_deps = of_data state data in
      ret @@ (modifier_deps @ data_deps)
    and of_modifiers state {  textures; redirection; _ } =
      let open Comp.Syntax in
      let+ texture_deps = List.map ~f:(of_texture state) textures |> Comp.all in
      let texture_deps = List.concat texture_deps in
      let+ redirection_deps = List.map ~f:(of_redirection state) redirection |> Comp.all in
      let redirection_deps = List.concat redirection_deps in
      ret @@ (texture_deps @ redirection_deps)
    and of_data state data =  let open Comp.Syntax in match data with
      | Model gid ->
        let+ dep = get_model_dep state gid in
        ret [dep] | Data _ -> ret [] | Screen _ -> ret [ScreenDep]
    and of_texture state {  textures; _ } =
      List.map ~f:(get_texture_dep state) textures |> Comp.all
    and of_redirection state (Redirection { drawable; _ }) =
      of_drawable state drawable

    let of_program state (program: program) =
      List.map program ~f:(fun (_, drawable) -> of_drawable state drawable)
      |> Comp.all |> Comp.map ~f:(List.concat)

  end


  module OfCompiled = struct
    let of_expr (type a) (expr: a C.expr) : T.data =
      match expr with
      | C.Expr (C.Bool, bl) -> MkData (bl, T.Bool)
      | C.Expr (C.Int, n) -> MkData (n, T.Int)
      | C.Expr (C.Float, fl) -> MkData (fl, T.Float)
      | C.Expr (C.Vec2, v2) -> MkData (v2, T.Vec2_Float)
      | C.Expr (C.Vec3, v3) -> MkData (v3, T.Vec3_Float)
      | C.Expr (C.Vec4, v4) -> MkData (v4, T.Vec4_Float)
      | C.Expr (C.Mat2, m2) -> MkData (m2, T.Mat2x2_Float)
      | C.Expr (C.Mat3, m3) -> MkData (m3, T.Mat3x3_Float)
      | C.Expr (C.Mat4, m4) -> MkData (m4, T.Mat4x4_Float)

    let of_light_uniform (uniform: C.light_uniform) : light_uniform =
      match uniform with
      | C.LightUniform { binding; value=f } ->
        LightUniform { binding; value = (fun data -> match f data with Wrap expr -> of_expr expr) }


    let of_uniform (uniform: C.uniform): uniform =
      match uniform with
      | C.Uniform { name; value = C.Independent expr } ->
        Uniform {name; value= DynValueIndependent (of_expr expr)}
      | C.Uniform { name; value = C.Context f } ->
        Uniform {
          name; value=DynValueContext (fun ctx -> of_expr @@ f ctx)
        }
      | C.Uniform { name; value = C.UnknownContext f } ->
        Uniform {
          name; value=DynValueContext (fun ctx -> let (Wrap expr) = f ctx in of_expr expr)
        }

    let of_attribute (attribute: C.attribute): attribute =
      match attribute with
      | C.Attribute { name; value = expr } ->
        Attribute {name; value=(of_expr expr)}

    let of_transform (type a) (transform: C.transform) =
      match transform with
      | C.Scale value -> begin
          let value = match value with
            | C.Independent C.Expr (Vec3, v3) -> ValueIndependent v3
            | C.Independent C.Expr (_, _) -> assert false
            | C.Context f ->
              ValueContext (fun ctx ->
                  let value = f ctx in
                  match value with
                  | C.Expr (Vec3, v3) -> v3
                  | C.Expr (_, _) -> assert false
                )
            | C.UnknownContext f ->
              ValueContext (fun ctx ->
                  let value = f ctx in
                  match value with
                  | C.Wrap C.Expr (Vec3, v3) -> v3
                  | C.Wrap C.Expr (_, _) -> assert false
                ) in
          Scale value
        end
      | C.Rotate value -> begin
          let value = match value with
            | C.Independent C.Expr (Mat4, v4) -> ValueIndependent v4
            | C.Independent C.Expr (_, _) -> assert false
            | C.Context f ->
              ValueContext (fun ctx ->
                  let value = f ctx in
                  match value with
                  | C.Expr (Mat4, v3) -> v3
                  | C.Expr (_, _) -> assert false
                )
            | C.UnknownContext f ->
              ValueContext (fun ctx ->
                  let value = f ctx in
                  match value with
                  | C.Wrap C.Expr (Mat4, v3) -> v3
                  | C.Wrap C.Expr (_, _) -> assert false
                ) in
          Rotate value
        end
      | C.Translate value -> begin
          let value = match value with
            | C.Independent C.Expr (Vec3, v3) -> ValueIndependent v3
            | C.Independent C.Expr (_, _) -> assert false
            | C.Context f ->
              ValueContext (fun ctx ->
                  let value = f ctx in
                  match value with
                  | C.Expr (Vec3, v3) -> v3
                  | C.Expr (_, _) -> assert false
                )
            | C.UnknownContext f ->
              ValueContext (fun ctx ->
                  let value = f ctx in
                  match value with
                  | C.Wrap C.Expr (Vec3, v3) -> v3
                  | C.Wrap C.Expr (_, _) -> assert false
                ) in
          Translate value
        end

    let of_texture_option (topt: C.texture_option) = match topt with
      | TextureBaseLevel value -> begin
          let value = match value with
            | C.Independent C.Expr (Int, v3) -> ValueIndependent v3
            | C.Independent C.Expr (_, _) -> assert false
            | C.Context f ->
              ValueContext (fun ctx ->
                  let value = f ctx in
                  match value with
                  | C.Expr (Int, v3) -> v3
                  | C.Expr (_, _) -> assert false
                )
            | C.UnknownContext f ->
              ValueContext (fun ctx ->
                  let value = f ctx in
                  match value with
                  | C.Wrap C.Expr (Int, v3) -> v3
                  | C.Wrap C.Expr (_, _) -> assert false
                ) in
          TextureBaseLevel value
        end
      | TextureMaxLevel value -> begin
          let value = match value with
            | C.Independent C.Expr (Int, v3) -> ValueIndependent v3
            | C.Independent C.Expr (_, _) -> assert false
            | C.Context f ->
              ValueContext (fun ctx ->
                  let value = f ctx in
                  match value with
                  | C.Expr (Int, v3) -> v3
                  | C.Expr (_, _) -> assert false
                )
            | C.UnknownContext f ->
              ValueContext (fun ctx ->
                  let value = f ctx in
                  match value with
                  | C.Wrap C.Expr (Int, v3) -> v3
                  | C.Wrap C.Expr (_, _) -> assert false
                ) in
          TextureMaxLevel value
        end
      | TextureBorderColor value -> begin
          let value = match value with
            | C.Independent C.Expr (Vec4, v3) -> ValueIndependent v3
            | C.Independent C.Expr (_, _) -> assert false
            | C.Context f ->
              ValueContext (fun ctx ->
                  let value = f ctx in
                  match value with
                  | C.Expr (Vec4, v3) -> v3
                  | C.Expr (_, _) -> assert false
                )
            | C.UnknownContext f ->
              ValueContext (fun ctx ->
                  let value = f ctx in
                  match value with
                  | C.Wrap C.Expr (Vec4, v3) -> v3
                  | C.Wrap C.Expr (_, _) -> assert false
                ) in
          TextureBorderColor value
        end
      | WrapS wopt -> WrapS wopt
      | WrapT wopt -> WrapT wopt
      | MinFilter fopt -> MinFilter fopt
      | MagFilter fopt -> MagFilter fopt

    let of_draw_option (dopt: C.draw_option) : draw_option = match dopt with
      | C.AlphaTest C.Expr (Bool, b) -> AlphaTest b
      | C.AlphaTest C.Expr (_, _) -> assert false
      | C.BlendTest C.Expr (Bool, b) -> BlendTest b
      | C.BlendTest C.Expr (_, _) -> assert false
      | C.DepthTest C.Expr (Bool, b) -> DepthTest b
      | C.DepthTest C.Expr (_, _) -> assert false
      | C.StencilTest C.Expr (Bool, b) -> StencilTest b
      | C.StencilTest C.Expr (_, _) -> assert false
      | C.BlendColor value -> begin
          let value = match value with
            | C.Independent C.Expr (Vec4, v3) -> ValueIndependent v3
            | C.Independent C.Expr (_, _) -> assert false
            | C.Context f ->
              ValueContext (fun ctx ->
                  let value = f ctx in
                  match value with
                  | C.Expr (Vec4, v3) -> v3
                  | C.Expr (_, _) -> assert false
                )
            | C.UnknownContext f ->
              ValueContext (fun ctx ->
                  let value = f ctx in
                  match value with
                  | C.Wrap C.Expr (Vec4, v3) -> v3
                  | C.Wrap C.Expr (_, _) -> assert false
                ) in
          BlendColor value
        end
      | C.BlendFunction (bf1, bf2) -> BlendFunction (bf1,bf2)
      | C.StencilFunction (cf1, c1, c2) ->
        let unwrap_int_expr value = begin
          let value = match value with
            | C.Independent C.Expr (Int, v3) -> ValueIndependent v3
            | C.Independent C.Expr (_, _) -> assert false
            | C.Context f ->
              ValueContext (fun ctx ->
                  let value = f ctx in
                  match value with
                  | C.Expr (Int, v3) -> v3
                  | C.Expr (_, _) -> assert false
                )
            | C.UnknownContext f ->
              ValueContext (fun ctx ->
                  let value = f ctx in
                  match value with
                  | C.Wrap C.Expr (Int, v3) -> v3
                  | C.Wrap C.Expr (_, _) -> assert false
                ) in
          value
        end in
        StencilFunction (cf1, unwrap_int_expr c1, unwrap_int_expr c2)
      | C.DepthFunction cf -> DepthFunction cf
      | C.AlphaFunction (cf, value) -> begin
          let value = match value with
            | C.Independent C.Expr (Float, v3) -> ValueIndependent v3
            | C.Independent C.Expr (_, _) -> assert false
            | C.Context f ->
              ValueContext (fun ctx ->
                  let value = f ctx in
                  match value with
                  | C.Expr (Float, v3) -> v3
                  | C.Expr (_, _) -> assert false
                )
            | C.UnknownContext f ->
              ValueContext (fun ctx ->
                  let value = f ctx in
                  match value with
                  | C.Wrap C.Expr (Float, v3) -> v3
                  | C.Wrap C.Expr (_, _) -> assert false
                ) in
          AlphaFunction (cf, value)
        end

    let of_texture
        ((texture_state, texture_sync),shaders,models)
        ({ name; textures; typ; texture_options }: C.texture) =
      let open Comp.Syntax in
      let+ (texture_state, texture_sync), textures =
        Comp.fold_map textures ~init:(texture_state, texture_sync)
          ~f:(fun (texture_state, texture_sync) filename ->
              let+ (texture_state, id, texture_sync) =
                Image_synchronizer.load_data texture_state [(), filename] texture_sync in
              Comp.return ((texture_state, texture_sync), id)) in
      let texture_options = List.map ~f:(of_texture_option) texture_options in
      Comp.return @@ (
        ((texture_state, texture_sync),shaders,models), {name; textures; typ; texture_options;})

    let of_data (type a)
        (textures, shaders,(model_state,model_sync))
        (data: C.data) : _ =
      let open Comp.Syntax in
      match data with
      | C.Model filename ->
        let+ (model_state, id, model_sync) =
          Geometry_synchronizer.load_data model_state ((), filename) model_sync in
        ret @@ ((textures, shaders,(model_state,model_sync)), Model id)
      | C.Data bindings ->
        let bindings = List.map bindings ~f:(fun (name, DataElem value) -> (name, of_expr value)) in
        ret @@ ((textures,shaders,(model_state,model_sync)),  Data bindings)
      | C.Screen { width; height } ->
        let map_expr (v: float C.expr option) =
          match v with None -> None | Some C.Expr (Float, fl) -> Some fl | _ -> assert false in
        ret @@ ((textures,shaders,(model_state,model_sync)),
                Screen { width= map_expr width; height=map_expr height })

    let of_shader
        (textures,(shader_state, shader_sync),models)
        (C.Shader { vertex_shader; fragment_shader; geometry_shader; _ }: C.shader) =
      let open Comp.Syntax in
      let open Sync in
      let vertex_shader = (Shaders.BasicShaderSynchronizer.Tag.VertexShader, vertex_shader) in
      let fragment_shader = match fragment_shader with
        | C.Src _ -> (Shaders.BasicShaderSynchronizer.Tag.FragmentShader, "./shaders/default.frag") 
        | C.Filename fragment_shader ->
          (Shaders.BasicShaderSynchronizer.Tag.FragmentShader, fragment_shader) in
      let geometry_shader = match geometry_shader with
        | None -> []
        | Some geometry_shader ->
          [(Shaders.BasicShaderSynchronizer.Tag.GeometryShader, geometry_shader)] 
      in
      let deps = vertex_shader :: fragment_shader :: geometry_shader in
      let+ (shader_state, id, shader_sync) =
        Shaders.BasicShaderSynchronizer.load_data shader_state deps shader_sync in
      ret @@ ((textures, (shader_state, shader_sync), models), Shader id)

    let rec of_modifiers
      state (modifiers: C.modifiers)  = match modifiers with
      | { transforms; uniforms; attributes; draw_options; textures; redirection } ->
        let open Comp.Syntax in
        let transforms = List.map ~f:of_transform  transforms in
        let uniforms = List.map ~f:of_uniform  uniforms in
        let attributes = List.map ~f:of_attribute  attributes in
        let draw_options = List.map ~f:of_draw_option draw_options in
        let+ (state, textures) = Comp.fold_map ~init:state ~f:of_texture textures in
        let+ (state, redirection) = Comp.fold_map ~init:state ~f:(of_redirection) redirection in
        ret @@ (state, { transforms; uniforms; attributes; draw_options; textures; redirection; })
    and of_redirection state redirection =
      let open Comp.Syntax in
      match redirection with | C.Redirection { id; binding_names; depth; stencil; drawable } ->
        let+ (state, drawable) = of_drawable state drawable in
        ret @@ (state, Redirection { id; binding_names; depth; stencil; drawable })
    and of_drawable state drawable =
      let open Comp.Syntax in match drawable with
      | C.BasicDrawable { shader; objects } ->
        let+ (state,shader) = of_shader state shader in
        let+ (state,objects) = Comp.fold_map ~init:state ~f:(of_obj) objects in
        ret @@ (state, BasicDrawable {shader; objects})
      | C.LightDrawable { shader; lights; light_data } ->
        let+ (state,shader) = of_shader state shader in
        let+ (state,lights) = Comp.fold_map ~init:state ~f:(of_light) lights in
        let+ (state,light_data) = of_light_data state light_data in
        ret @@ (state, LightDrawable { shader; lights; light_data })        
    and of_light state light =
      let open Comp.Syntax in match light with
      | C.Light { modifiers; position; size } ->
        let+ (state, modifiers) = of_modifiers state modifiers in
        let position = match position with
          | C.Independent C.Expr (Vec3, v) -> ValueIndependent v
          | C.Independent C.Expr (_, _) -> assert false
          | C.Context f -> ValueContext (fun ctx -> match f ctx with C.Expr (Vec3, v) -> v | _ -> assert false)
          | C.UnknownContext f ->
            ValueContext (fun ctx -> match f ctx with C.Wrap C.Expr (Vec3, v) -> v | _ -> assert false) in
        let size = match size with
          | C.Independent C.Expr (Float, v) -> ValueIndependent v
          | C.Independent C.Expr (_, _) -> assert false
          | C.Context f -> ValueContext (fun ctx -> match f ctx with C.Expr (Float, v) -> v | _ -> assert false)
          | C.UnknownContext f ->
            ValueContext (fun ctx -> match f ctx with C.Wrap C.Expr (Float, v) -> v | _ -> assert false) in
        ret @@ (state, Light {modifiers; position; size})
    and of_obj state obj =
      let open Comp.Syntax in match obj with | (modifiers, data) ->
        let+ (state,modifiers) = of_modifiers state modifiers in
        let+ (state, data) = of_data state data in
        ret @@ (state, (modifiers, data))
    and of_light_data state light_data =
      let open Comp.Syntax in match light_data with
      | C.LightVolume {  uniforms; _ } ->
        let uniforms = List.map ~f:(of_light_uniform) uniforms in
        ret @@ (state, LightVolume { uniforms})
      | C.LightObjects objs ->
        let+ (state,objs) = Comp.fold_map ~init:state ~f:(fun state (light_uniforms,obj) ->
            let light_uniforms = List.map ~f:of_light_uniform light_uniforms in
            let+ (state,obj) = of_obj state obj in
            ret @@ (state, (light_uniforms, obj))
          ) objs in
        ret @@ (state, LightObjects objs)

    let of_program state (program: C.program) =
      let open Comp.Syntax in
      Comp.fold_map program ~init:state ~f:(fun state (label, drawable) ->
          let+ (state, drawable) = of_drawable state drawable in
          ret (state, (label, drawable))
        )

  end

end

module DrawableObject = struct
  module T = Graphics_utils.GenericShader.Types.Data.Numeric
  module I = Graphics_utils.GenericShader.Types.Data.Texture
  module Types = Graphics_utils.GenericShader.Types
  module TP = Gsdl.TypedParsetree

  let light_data =
    let comp = 
    let open Comp.Syntax in
    let data = [%blob "./data/sphere.stl"] in
    let+ data = Ascii.parse_string data in
    let (normals, points) = Geometry.Triangle.split_data data in
    (* Reference_data.light_data *)
    ret GenericShader.Types.Data.Numeric.[
              "vertex_position", MkBulkData (points, Vec3_Float);
              "vertex_normals", MkBulkData (normals, Vec3_Float);
            ] in
    Comp.run comp |> Result.map_error ~f:(Error.to_exn) |> Result.ok_exn |> fst


  type ctx = Object.ctx = {
    camera_position : v3; camera_front : v3; camera_yaw : float; camera_pitch : float;
  }

  type value =
    | Immediate of T.data
    | Deferred of (ctx -> T.data)

  type buffer_in = Depth | Stencil | DepthStencil

  type obj = {
    (* model matrix maps an object to its position in world space *)
    model: value;
    (* uniforms are a sequence of other uniforms included in the object   *)
    uniforms: (string * value) list;
    (* data is the attribute data being rendered by the object *)
    data: Shaders.BasicShader.data;
    (* texture data is the textures used by the object *)
    texture_data: [
      `Direct of Shaders.BasicShader.texture_data |
      `Deferred of (Shaders.BasicShader.t * ctx -> (Shaders.BasicShader.texture_data, Error.t) Comp.t)
    ];
    (* in fbo is an optional fbo to blit in before drawing *)
    in_fbo: (FBO.t * buffer_in) option;
  }

  let pp_obj fmt ({ model; uniforms; data; texture_data; in_fbo }: obj) =
    let open Format in
    pp_open_vbox fmt 1;
    begin match in_fbo with None -> () | Some (fbo, bin) ->
      let bin = match bin with
        | Depth -> "GL_DEPTH_ATTACHMENT"
        | Stencil -> "GL_STENCIL_ATTACHMENT"
        | DepthStencil -> "GL_DEPTH_STENCIL_ATTACHMENT" in
      pp_open_hbox fmt ();
      pp_print_string fmt "glBlitBuffer(from="; FBO.pp fmt fbo;
      pp_print_string fmt ", to=<current_buffer>, "; pp_print_string fmt bin;
      pp_print_string fmt ");";
      pp_close_box fmt ();
      pp_print_space fmt ();
    end;
    pp_print_string fmt "glDrawTriangles(<opaque>);"; pp_print_space fmt ();
    pp_close_box fmt ()

  type draw_option = Object.draw_option
  type draw_obj = [`DrawOption of draw_option list | `Obj of obj]

  let pp_draw_obj fmt (dob: draw_obj) = match dob with
    | `Obj obj -> pp_obj fmt obj
    | `DrawOption _ -> Format.pp_print_string fmt "glDrawOptions(<opaque>);"

  type drawable =
    (* normal drawable just draws straight to the default framebuffer *)
    | Normal of { shader: Shaders.BasicShader.t; objects: draw_obj list;  }
    (* redirecting drawable draws to a fbo - the other components are
       purely for deleting resources *)
    | Redirecting of {
        id: int; fbo: FBO.t; shader: Shaders.BasicShader.t; objects: draw_obj list;
        textures: Texture.t list; rbo: RBO.t option; 
      }

  let pp_drawable fmt (drawable: drawable) =
    let open Format in match drawable with
    | Normal { shader; objects } ->
      pp_open_vbox fmt 2;
          pp_open_hbox fmt ();
             Shaders.BasicShader.pp fmt shader;
          pp_close_box fmt ();
          pp_print_space fmt ();
          pp_open_vbox fmt 1;
              pp_print_list ~pp_sep:(fun fmt () -> pp_print_space fmt ()) pp_draw_obj fmt objects;
          pp_close_box fmt ();
      pp_close_box fmt ()

    | Redirecting {  fbo; shader; objects; _ } ->
      pp_open_vbox fmt 2;
          pp_open_hbox fmt ();
              pp_print_string fmt "glBindFramebuffer(";
              FBO.pp fmt fbo;
              pp_print_string fmt ");";
          pp_close_box fmt (); 
          pp_print_space fmt ();
          pp_open_hbox fmt ();
             Shaders.BasicShader.pp fmt shader;
          pp_close_box fmt ();
          pp_print_space fmt ();
          pp_open_vbox fmt 1;
              pp_print_list ~pp_sep:(fun fmt () -> pp_print_space fmt ()) pp_draw_obj fmt objects;
          pp_close_box fmt ();
      pp_close_box fmt ()
      
  type draw_operation =  [ `Drawable of drawable | `LabelledDrawable of string * drawable ]

  let pp_draw_operation fmt (dopt: draw_operation) =
    let open Format in
    match dopt with
    | `LabelledDrawable (label, drawable) ->
      pp_open_hbox fmt ();
        pp_print_string fmt "label(\"";
        pp_print_string fmt label;
        pp_print_string fmt "\");";
      pp_close_box fmt (); pp_print_space fmt ();
      pp_drawable fmt drawable      
    | `Drawable drawable -> pp_drawable fmt drawable

  let pp_program fmt program =
    let open Format in
    pp_open_vbox fmt 0;
      pp_print_list ~pp_sep:(fun fmt () -> pp_print_space fmt ())
         pp_draw_operation fmt program;
    pp_close_box fmt ()

  let show_program program = pp_program Format.str_formatter program; Format.flush_str_formatter ()

  module Deletion = struct

    module IntSet = Set.Make (Int)

    let rec delete_program (draw_operations: draw_operation list) =
      List.fold ~init:IntSet.empty draw_operations ~f:delete_draw_operation
    and delete_draw_operation redirecting_set draw_operation = match draw_operation with
      | `LabelledDrawable (_, d)
      | `Drawable d -> delete_drawable redirecting_set d
    and delete_drawable deleted_ids d = match d with
      | Normal {  objects; _ } ->
        List.iter objects ~f:delete_draw_obj;
        deleted_ids
      | Redirecting { id; fbo;  objects; textures; rbo; _ } ->
        let deleted_ids = if IntSet.mem deleted_ids id then deleted_ids else begin
            FBO.delete fbo;
            begin match rbo with None -> () | Some rbo -> RBO.delete rbo end;
            List.iter textures ~f:(fun texture -> Texture.delete texture);
            IntSet.add deleted_ids id
          end in
        List.iter objects ~f:delete_draw_obj;
        deleted_ids
    and delete_draw_obj obj = match obj with
      | `DrawOption _ -> ()
      | `Obj { model; uniforms; data; texture_data; in_fbo } ->
        Shaders.BasicShader.delete_data data;
        begin match texture_data with `Direct v -> Shaders.BasicShader.delete_texture_data v | _ -> () end
    and delete program = ignore(delete_program program)

  end

  module Compile = struct

    module DrawContext = struct
      type t = {
        alpha_test: bool option; blend_test: bool option; depth_test: bool option; stencil_test: bool option;
        blend_colour: v4 Object.value option;
        blend_function: (Object.blend_func * Object.blend_func) option;
        stencil_function: (Object.comparison_func * int Object.value * int Object.value) option;
        depth_function: Object.comparison_func option;
        alpha_function: (Object.comparison_func * float Object.value) option;
      }

      let empty = {
        alpha_test= None; blend_test=None; depth_test=None; stencil_test= None;
        blend_colour= None; blend_function= None; stencil_function=None;
        depth_function=None; alpha_function=None
      }

      let default_alpha_test = true
      let default_blend_test = false
      let default_depth_test = true
      let default_stencil_test = false
      let default_blend_colour_raw = (V4.v 0.0 0.0 0.0 0.0)
      let default_blend_colour = Object.ValueIndependent default_blend_colour_raw
      let default_blend_function = (TP.One, TP.Zero)
      let default_stencil_function_raw = TP.Stencil_always, 1, 0xfffffffffffffff
      let default_stencil_function =
        let (c1, e1, e2) = default_stencil_function_raw in
        c1, Object.ValueIndependent e1, Object.ValueIndependent e2
      let default_depth_function = TP.Stencil_less
      let default_alpha_function_raw = TP.Stencil_always, 0.0
      let default_alpha_function =
        let (c1,e1) = default_alpha_function_raw in c1, Object.ValueIndependent e1

      let of_draw_options ?(init={
          alpha_test= None; blend_test=None; depth_test=None; stencil_test= None;
          blend_colour= None; blend_function= None; stencil_function=None;
          depth_function=None; alpha_function=None
        }) (ls: Object.draw_option list) =
        List.fold ls ~init ~f:(fun ctx draw_option -> match draw_option with
            | Object.AlphaTest value -> {ctx with alpha_test=Some value}
            | Object.BlendTest value -> {ctx with blend_test=Some value}
            | Object.DepthTest value -> {ctx with depth_test=Some value}
            | Object.StencilTest value -> {ctx with stencil_test=Some value}
            | Object.BlendColor value -> {ctx with blend_colour = Some value}
            | Object.BlendFunction (f1, f2) -> {ctx with blend_function = Some (f1,f2)}
            | Object.StencilFunction (cf, i1, i2) -> {ctx with stencil_function = Some (cf,i1,i2)}
            | Object.DepthFunction cf -> {ctx with depth_function=Some cf}
            | Object.AlphaFunction (cf, rf) -> {ctx with alpha_function = Some (cf,rf)}
          )

      let generate_draw_options
          (current_draw_context: t)
          (desired_draw_context: t) : (t * draw_option list) =
        let out = [] in
        let alpha_test, out =
          let desired = match desired_draw_context.alpha_test with None -> default_alpha_test | Some value -> value in
          match current_draw_context.alpha_test with
          | Some current when Bool.equal current desired -> Some current, out 
          | _ -> Some desired, Object.AlphaTest desired :: out in
        let blend_test, out =
          let desired = match desired_draw_context.blend_test with None -> default_blend_test | Some value -> value  in
          match current_draw_context.blend_test with
          | Some current when Bool.equal current desired -> Some current, out
          | _ -> Some desired, Object.BlendTest desired :: out in
        let depth_test, out =
          let desired = match desired_draw_context.depth_test with | None -> default_depth_test | Some value -> value in
          match current_draw_context.depth_test with
          | Some current when Bool.equal current desired -> Some current, out
          | _ -> Some desired, Object.DepthTest desired :: out in
        let stencil_test, out =
          let desired = match desired_draw_context.stencil_test with None -> default_stencil_test | Some value -> value in
          match current_draw_context.stencil_test with
          | Some current when Bool.equal current desired -> Some current, out
          | _ -> Some desired, Object.StencilTest desired :: out in
        let blend_colour, out =
          let desired = match desired_draw_context.blend_colour with None -> default_blend_colour | Some value -> value in
          match current_draw_context.blend_colour with
          | Some current when Object.equal_value V4.equal current desired -> Some current, out
          | _ -> Some desired, Object.BlendColor desired :: out in
        let blend_function, out =
          let desired = match desired_draw_context.blend_function with None -> default_blend_function | Some value -> value in
          match current_draw_context.blend_function with
          | Some current
            when Tuple2.equal ~eq1:TP.equal_blend_func ~eq2:TP.equal_blend_func current desired
            -> Some current, out
          | _ -> Some desired, Object.BlendFunction (fst desired, snd desired) :: out in
        let stencil_function, out =
          let desired = match desired_draw_context.stencil_function with None -> default_stencil_function | Some value -> value in
          match current_draw_context.stencil_function with
          | Some current when
              Tuple3.equal
                ~eq1:TP.equal_comparison_func
                ~eq2:(Object.equal_value Int.equal)
                ~eq3:(Object.equal_value Int.equal) current desired -> Some current, out
          | _ -> Some desired, Object.StencilFunction (fst3 desired, snd3 desired, trd3 desired) :: out in
        let depth_function, out =
          let desired = match desired_draw_context.depth_function with None -> default_depth_function | Some value -> value in
          match current_draw_context.depth_function with
          | Some current when TP.equal_comparison_func current desired -> Some current, out
          | _ -> Some desired, Object.DepthFunction desired :: out in
        let alpha_function, out =
          let desired = match desired_draw_context.alpha_function with None -> default_alpha_function | Some value -> value in
          match current_draw_context.alpha_function with
          | Some current when
              Tuple2.equal
                ~eq1:TP.equal_comparison_func
                ~eq2:(Object.equal_value Float.equal) current desired -> Some current, out
          | _ -> Some desired, Object.AlphaFunction (fst desired, snd desired) :: out in
        {
          alpha_test; blend_test; depth_test; stencil_test;
          blend_colour; blend_function; stencil_function;
          depth_function; alpha_function},
        out

      type tuple4 = float * float * float * float [@@deriving sexp, hash, ord, show]
      let v4_of_sexp sexp = tuple4_of_sexp sexp |> V4.of_tuple
      let sexp_of_v4 vec = V4.to_tuple vec |> sexp_of_tuple4
      let hash_fold_v4 hash vec = V4.to_tuple vec |> hash_fold_tuple4 hash
      let compare_v4 v1 v2 = compare_tuple4 (V4.to_tuple v1) (V4.to_tuple v2)
      let pp_v4 fmt v4 = V4.to_tuple v4 |> pp_tuple4 fmt

      module Unwrap = struct
        (* unwrapped *)
        type t = {
          alpha_test: bool; blend_test: bool; depth_test: bool; stencil_test: bool;
          blend_colour: v4;
          blend_function: (Object.blend_func * Object.blend_func);
          stencil_function: (Object.comparison_func * int * int);
          depth_function: Object.comparison_func;
          alpha_function: (Object.comparison_func * float);
        }
        [@@deriving sexp, hash, ord]
      end

      let unwrap ({ alpha_test; blend_test; depth_test; stencil_test; blend_colour;
                    blend_function; stencil_function; depth_function; alpha_function }: t) =
        let open Option.Syntax in
        let alpha_test = Option.value alpha_test ~default:default_alpha_test in
        let blend_test = Option.value blend_test ~default:default_blend_test in
        let depth_test = Option.value depth_test ~default:default_depth_test in
        let stencil_test = Option.value stencil_test ~default:default_stencil_test in
        let+ blend_colour = match blend_colour with None -> Some default_blend_colour_raw
                                                  | Some (Object.ValueIndependent value) -> Some value
                                                  | Some (Object.ValueContext _) -> None in
        let blend_function = Option.value  blend_function ~default:default_blend_function in
        let+ stencil_function = match stencil_function with
          | None -> Some default_stencil_function_raw
          | Some (cf, Object.ValueIndependent e1, Object.ValueIndependent e2) -> Some (cf, e1, e2)
          | _ -> None in
        let depth_function = Option.value depth_function ~default:default_depth_function in
        let+ alpha_function = match alpha_function with
          | None -> Some default_alpha_function_raw
          | Some (c1, Object.ValueIndependent e1) -> Some (c1, e1)
          | _ -> None in
        ret Unwrap.{
            alpha_test; blend_test; depth_test; stencil_test;
            blend_colour; blend_function;
            stencil_function; depth_function; alpha_function;
        }
    end

    module TextureState = struct
      type tuple4 = float * float * float * float [@@deriving sexp, hash, show, ord]
      let v4_of_sexp sexp = tuple4_of_sexp sexp |> V4.of_tuple
      let sexp_of_v4 vec = V4.to_tuple vec |> sexp_of_tuple4
      let hash_fold_v4 hash vec = V4.to_tuple vec |> hash_fold_tuple4 hash
      let compare_v4 v1 v2 = compare_tuple4 (V4.to_tuple v1) (V4.to_tuple v2)
      let pp_v4 fmt v4 = V4.to_tuple v4 |> pp_tuple4 fmt

      type t = {
        texture_base_level: int; texture_max_level: int;
        texture_border_color: v4;
        min_filter: Object.filter_option;
        mag_filter: Object.filter_option;
        wrap_s: Object.wrap_option; wrap_t: Object.wrap_option;
      }
      [@@deriving show, sexp, hash, ord]

      let mipmaps_needed ({ min_filter; mag_filter; _ }:t) =
        let f fopt = match fopt with
            TP.NearestMipmapNearest | TP.NearestMipmapLinear | TP.LinearMipmapNearest
          | TP.LinearMipmapLinear -> true | _ -> false in
        f min_filter || f mag_filter

      let default_texture_base_level = 0
      let default_texture_max_level = 1000
      let default_texture_border_color = V4.v 0.0 0.0 0.0 0.0
      let default_min_filter = TP.NearestMipmapLinear
      let default_mag_filter = TP.Linear
      let default_wrap_s = TP.Repeat
      let default_wrap_t = TP.Repeat

      let empty = {
       texture_base_level=default_texture_base_level;
       texture_max_level=default_texture_max_level;
       texture_border_color=default_texture_border_color;
       min_filter=default_min_filter;
       mag_filter=default_mag_filter;
       wrap_s=default_wrap_s;
       wrap_t=default_wrap_t;
      }

      let of_texture_parameters (params: Object.texture_option list) =
        let rec loop acc (ls: Object.texture_option list) = match acc,ls with
            None, _ -> None | Some state, [] -> Some state
          | Some state, h :: t ->
            let state = match h with
              | Object.TextureBaseLevel Object.ValueIndependent n -> Some {state with texture_base_level=n}
              | Object.TextureMaxLevel Object.ValueIndependent n -> Some {state with texture_max_level=n}
              | Object.TextureBorderColor Object.ValueIndependent v -> Some {state with texture_border_color=v}
              | Object.WrapS wopt -> Some {state with wrap_s = wopt}
              | Object.WrapT wopt -> Some {state with wrap_t = wopt}
              | Object.MinFilter fopt -> Some {state with min_filter = fopt}
              | Object.MagFilter fopt -> Some {state with mag_filter = fopt}
              | _ -> None in
            loop state t in
        loop (Some empty) params

      let of_texture_parameters_with_context ctx (params: Object.texture_option list) =
        let rec loop state (ls: Object.texture_option list) = match ls with
            [] -> state
          | h :: t ->
            let state = match h with
              | Object.TextureBaseLevel Object.ValueIndependent n -> {state with texture_base_level=n}
              | Object.TextureBaseLevel Object.ValueContext f -> {state with texture_base_level=f ctx}
              | Object.TextureMaxLevel Object.ValueIndependent n -> {state with texture_max_level=n}
              | Object.TextureMaxLevel Object.ValueContext f -> {state with texture_max_level=f ctx}
              | Object.TextureBorderColor Object.ValueIndependent v -> {state with texture_border_color=v}
              | Object.TextureBorderColor Object.ValueContext f -> {state with texture_border_color=f ctx}
              | Object.WrapS wopt -> {state with wrap_s = wopt}
              | Object.WrapT wopt -> {state with wrap_t = wopt}
              | Object.MinFilter fopt -> {state with min_filter = fopt}
              | Object.MagFilter fopt -> {state with mag_filter = fopt} in
            loop state t in
        loop empty params

      let of_texture_parameters_unwrapped (params : Texture.Param.t list) =
        let rec loop state (ls : Texture.Param.t list) = match ls with
          | [] -> state
          | h :: t ->
            let wrap_option_to_option : Texture.Param.wrap_option -> Object.wrap_option = function
              | Texture.Param.Repeat -> TP.Repeat | Texture.Param.MirroredRepeat -> TP.MirroredRepeat
              | Texture.Param.ClampToEdge -> TP.ClampToEdge | Texture.Param.ClampToBorder -> TP.ClampToBorder in
            let filter_option_to_option : Texture.Param.filter_option -> Object.filter_option = function
              | Texture.Param.Nearest -> TP.Nearest | Texture.Param.Linear -> TP.Linear
              | Texture.Param.NearestMipmapNearest -> TP.NearestMipmapNearest
              | Texture.Param.NearestMipmapLinear -> TP.NearestMipmapLinear
              | Texture.Param.LinearMipmapNearest -> TP.LinearMipmapNearest
              | Texture.Param.LinearMipmapLinear -> TP.LinearMipmapLinear in
            let state = match h with
              | Texture.Param.TextureBaseLevel n -> {state with texture_base_level=n}
              | Texture.Param.TextureMaxLevel n -> {state with texture_max_level=n}
              | Texture.Param.TextureBorderColour v -> {state with texture_border_color=v}
              | Texture.Param.WrapS wopt -> {state with wrap_s = wrap_option_to_option wopt}
              | Texture.Param.WrapT wopt -> {state with wrap_t = wrap_option_to_option wopt}
              | Texture.Param.MinFilter fopt -> {state with min_filter = filter_option_to_option fopt}
              | Texture.Param.MagFilter fopt -> {state with mag_filter = filter_option_to_option fopt} in
            loop state t in
        loop empty params

    end

    module IntMap = Map.Make (Int)


    module Context = struct

      module ImageTag = struct
        (** we will cache textures by their id tag and parameters *)
        type t = TextureState.t * Image_synchronizer.ID.t list [@@deriving sexp, ord, hash]
      end

      module GeometryTag = struct
        include Geometry_synchronizer.ID
        (** we will cache model data by mapping geometry to VBO.s  *)
        type t = Geometry_synchronizer.ID.t
      end


      module DataTag = struct
        (** we will cache VAO data by mapping T.data list * Geometry_synchronizer.t to VAO.t  *)
        type t = (string * T.data) list * Geometry_synchronizer.ID.t [@@deriving sexp, ord, hash, show]
      end

      module RawDataTag = struct
        (** we will cache other data by mapping T.data * int to (string * VBO.t) *)
        type t = T.data * int  [@@deriving sexp, ord, hash, show]
      end

      type t = {
        width: int; height: int;
        logical_width: int; logical_height: int;
        shader_manager: Shaders.BasicShaderSynchronizer.t;
        image_manager: Image_synchronizer.t;
        mesh_manager: Geometry_synchronizer.t;
        redirection_map: (FBO.t * (string * Texture.t) list * buffer_in option) IntMap.t;
        draw_context: DrawContext.t;
        result: draw_operation list;

        (* texture cache - map image tag & options to textures  *)
        texture_cache: (ImageTag.t, Texture.t) Hashtbl.t;
        (* first check vao cache - if present, we can just reuse VAO *)
        data_cache: (DataTag.t, Shaders.BasicShader.data) Hashtbl.t;

        (* otherwise, check geometry cache to see if model data has been loaded onto the gpu before  *)
        geometry_cache: (GeometryTag.t, (string * VBO.t) list) Hashtbl.t;
        (* and use raw data cache to avoid reloading data if present *)
        raw_data_cache: (RawDataTag.t, VBO.t) Hashtbl.t;
      }

      let create
        ~logical_width ~logical_height
        ~width ~height ~shader_manager ~image_manager ~mesh_manager () = {
        logical_width;logical_height; 
        width;height; shader_manager;
        image_manager; mesh_manager;
        redirection_map=IntMap.empty;
        draw_context=DrawContext.empty;
        result=[];
        texture_cache=Hashtbl.create (module ImageTag);
        data_cache=Hashtbl.create (module DataTag);
        geometry_cache=Hashtbl.create (module GeometryTag);
        raw_data_cache=Hashtbl.create (module RawDataTag);
      }


      let update_redirection_map context ~key ~data =
        {context with redirection_map = IntMap.set context.redirection_map ~key ~data}

      let ensure_draw_options context draw_options =
        let draw_context = DrawContext.of_draw_options draw_options in
        let (draw_context, draw_options) = DrawContext.generate_draw_options context.draw_context draw_context in
        {context with draw_context}, draw_options

      let add_drawable context drawable =
        {context with result = (`Drawable drawable) :: context.result}

      let record_drawable_with_label context o_label drawable =
        match o_label with
        | None ->         {context with result = (`Drawable drawable) :: context.result}
        | Some label -> {context with result = (`LabelledDrawable (label, drawable)) :: context.result}

      let compile context =
        let program = List.rev context.result in
        program

    end

    let unwrap_transforms_internal ~f ~g ~h transforms translate_command =
      let transforms = List.map (transforms @ translate_command) ~f:(function
          | Object.Scale ValueIndependent v -> (`Left (`Scale v))
          | Object.Rotate ValueIndependent v -> (`Left (`Rotate v))
          | Object.Translate ValueIndependent v -> (`Left (`Translate v))
          | v -> `Right v
        ) in
      let immediate_transforms = List.map transforms ~f:(function `Left v -> Some v | `Right v -> None ) |> Option.all in
      match immediate_transforms with
      | Some transforms ->
        let transforms = List.rev transforms in
        let transform_matrix = List.fold transforms ~init:M4.id ~f:(fun mat v ->
            match v with
            | `Scale scale_vec -> M4.mul (M4.scale3 scale_vec) mat
            | `Rotate rot -> M4.mul rot mat
            | `Translate translate -> M4.mul (M4.move3 translate) mat) in
        h (f (transform_matrix))
      | None ->
        let transforms = List.rev transforms in
        g (fun ctx ->
            let transform_matrix =
              List.fold transforms ~init:M4.id ~f:(fun mat v -> match v with
                  | `Right (Object.Scale ValueIndependent scale_vec)
                  | `Left `Scale scale_vec -> M4.mul (M4.scale3 scale_vec) mat
                  | `Right (Object.Rotate ValueIndependent rot)
                  | `Left `Rotate rot -> M4.mul rot mat
                  | `Right (Object.Translate ValueIndependent translate)
                  | `Left `Translate translate -> M4.mul (M4.move3 translate) mat
                  | `Right (Object.Scale ValueContext f) -> M4.mul (M4.scale3 (f ctx)) mat
                  | `Right (Object.Rotate ValueContext f) -> M4.mul (f ctx) mat
                  | `Right (Object.Translate ValueContext f) -> M4.mul (M4.move3 (f ctx)) mat
                ) in f transform_matrix
          )

    let unwrap_transforms transforms translate_command =
      unwrap_transforms_internal
        transforms
        translate_command
        ~f:(fun mat -> T.MkData (mat, T.Mat4x4_Float))
        ~g:(fun data -> Deferred data)
        ~h:(fun v -> Immediate v)



    let rec of_drawable_cont ~allocations ~of_shader ~of_object ~of_redirection ~of_textures ~of_data_raw
        (type a) : cont:(Context.t -> Shaders.BasicShader.t -> draw_obj list -> (a, Core.Error.t) Utils.Comp.t) ->
      (Graphics_utils.Texture.t Utils.StringMap.t -> Context.t -> Object.drawable -> (a, Core.Error.t) Utils.Comp.t) =
      fun  (type a)  ~(cont: Context.t -> _ -> _ -> (a, Error.t) Comp.t)  map context drawable -> 
      let open Comp.Syntax in
      match drawable with
      | Object.BasicDrawable { shader; objects } ->
        let+ (context,shader) = of_shader context shader in
        let+ context, objects =
          Comp.fold_map ~init:context ~f:(fun context obj ->
              of_object ~allocations ~additional_uniforms:[] map context shader obj) objects in
        let objects = List.concat objects in
        cont context shader objects
      (* let context = Context.record_label context o_label in *)
      | Object.LightDrawable { shader; lights; light_data=ld } ->
        let unwrap_light_uniform transforms position =
          let transforms = unwrap_transforms_internal
              ~h:(fun v -> v) ~f:(fun mat -> `Left mat) ~g:(fun f -> `Right f) transforms [] in
          (
          fun (Object.LightUniform { binding; value=fv }) ->
            (binding,
             Deferred (fun ctx ->
                 let transforms =
                   let rec unwrap transforms =
                     match transforms with
                     | `Left transform -> transform
                     | `Right f -> unwrap (f ctx) in
                   unwrap transforms in
                 let position =
                   match position with
                     Object.ValueIndependent v -> v
                   | Object.ValueContext f -> f ctx in
                 (* Printf.printf
                  *   "got position: %s -> "
                  *   (V3.pp Format.str_formatter position; Format.flush_str_formatter ()); *)
                 let position = V4.ltr transforms (V4.of_v3 ~w:1.0 position) |> V3.of_v4 in
                 (* Printf.printf "%s\n"
                  *   (V3.pp Format.str_formatter position; Format.flush_str_formatter ()); *)
                 fv (position,ctx)))) in
        match ld with
        | Object.LightVolume { (* binding; *) uniforms; _ } ->
          let+ (context,shader) = of_shader context shader in
          let+ (context, objects) =
            Comp.fold_map lights ~init:context ~f:(fun context (Object.Light { modifiers; position; size }) ->
                let+ (map, context, in_fbo) =
                  Comp.fold ~init:(map,context,None) ~f:(fun (map,context,fbo) redirection ->
                    let+ (map, context, fbo') = of_redirection ~allocations map context shader redirection in
                    let fbo = match fbo with
                        None -> fbo'
                      | Some v -> Some v in
                    ret (map, context, fbo)) modifiers.redirection in
                let context, draw_options = Context.ensure_draw_options context modifiers.draw_options in
                let light_uniforms =
                  List.map uniforms ~f:(unwrap_light_uniform modifiers.transforms position) in
                let uniforms = List.map ~f:of_uniform modifiers.uniforms in
                let uniforms = light_uniforms @ uniforms in
                let+ texture_data = of_textures ~allocations map context shader modifiers.textures in
                let+ model,data =
                  of_data_raw ~allocations context shader modifiers.transforms modifiers.attributes
                    (`RawData light_data)
                    [
                      Object.Translate position;
                      Object.Scale (match size with
                          | Object.ValueIndependent s1 -> Object.ValueIndependent (V3.v s1 s1 s1)
                          | Object.ValueContext f ->
                            Object.ValueContext (fun ctx -> let s1 = f ctx in (V3.v s1 s1 s1)));
                    ] in
                let obj = { model; uniforms; data; texture_data; in_fbo; } in
                ret (context, [`DrawOption draw_options; `Obj obj])) in
          let objects = List.concat objects in
          cont context shader objects
        | Object.LightObjects light_objects ->
          let+ (context,shader) = of_shader context shader in
          let+ context, objs =
            Comp.fold_map lights ~init:context ~f:(fun context (Object.Light {  modifiers; position; _ }) ->
                let+ context, light_objects =
                  Comp.fold_map light_objects ~init:context ~f:(fun context (light_uniforms, obj) ->
                      let additional_uniforms = List.map
                          ~f:(unwrap_light_uniform modifiers.transforms position) light_uniforms in
                      of_object ~allocations ~additional_uniforms map context shader obj
                    ) in
                ret (context, List.concat light_objects)
              ) in
          let objects = List.concat objs in
          cont context shader objects    
    and of_dynamic_value value = match value with
      | Object.DynValueIndependent vl -> Immediate vl
      | Object.DynValueContext f -> Deferred f
    and of_uniform (Object.Uniform { name; value }) =
      (name, of_dynamic_value value)
    let store_texture_data allocations texture_data =
      allocations := `TextureData texture_data :: !allocations
    let store_texture allocations texture =
      allocations := `Texture texture :: !allocations
    let store_rbo allocations rbo =
      allocations := `RBO rbo :: !allocations
    let store_fbo allocations fbo =
      allocations := `FBO fbo :: !allocations

    let store_attribute_data allocations attribute_data = 
      allocations := `AttributeData attribute_data :: !allocations


    let rec of_program ~allocations map (context: Context.t) (program: Object.program) =
      let open Comp.Syntax in
      let+ context = Comp.fold program
          ~init:context ~f:(fun context (o_label, drawable) ->
              of_drawable ~allocations map context o_label drawable) in
      ret (Context.compile context)
    and of_data ~allocations context parent_shader transforms attributes data =
      let open Comp.Syntax in
      let+ data, translate_command = match data with
        | Object.Model mid ->
          let+ model = Geometry_synchronizer.get_data mid context.Context.mesh_manager in
          let+ model = Comp.of_option ~error:(Error.of_string "Could not load model") model in
          let (vertex_position, vertex_normals, tex_coords) = Geometry.Mesh.serialize_data model in
          let translate = Geometry.Mesh.calculate_translate model in
          let data = GenericShader.Types.Data.Numeric.(
              Some ("vertex_position", MkBulkData (vertex_position, Vec3_Float))  ::
              Option.map  vertex_normals ~f:(fun vertex_normals ->
                  "vertex_normals", MkBulkData (vertex_normals, Vec3_Float) 
                ) ::
              Option.map tex_coords ~f:(fun tex_coords ->
                  "tex_coords", MkBulkData (tex_coords, Vec2_Float) 
                ) :: [] |> List.filter_opt
            ) in
          ret (`Model (data, mid), [Object.Translate (ValueIndependent translate)])
        | Object.Data data -> ret (`RawData data, [])
        | Object.Screen { width; height } ->
          let width =
            match width with Some v when Float.(v > 0.0) -> v | _ -> Float.of_int context.logical_width in
          let height =
            match height with Some v when Float.(v > 0.0) -> v | _ -> Float.of_int context.logical_height in
          let wr = width /. Float.of_int context.logical_width in
          let hr = height /. Float.of_int context.logical_height in
          ret (`RawData GenericShader.Types.Data.Numeric.[
              "vertex_position", MkBulkData ([
                  V2.v (-1.0 *. wr) (1.0 *. hr); V2.v (-1.0 *. wr) (-1.0 *. hr); V2.v (1.0 *. wr) (-1.0 *. hr);
                  V2.v (-1.0 *. wr) (1.0 *. hr); V2.v (1.0 *. wr) (-1.0 *. hr); V2.v (1.0 *. wr) (1.0 *. hr);
                ], Vec2_Float);
              "tex_coords", MkBulkData ([
                  V2.v (0.0) (1.0); V2.v (0.0) (0.0); V2.v (1.0) (0.0);
                  V2.v (0.0) (1.0); V2.v (1.0) (0.0); V2.v (1.0) (1.0);
                ], Vec2_Float);
            ], []) in
      of_data_raw ~allocations context parent_shader transforms attributes data translate_command
    and of_drawable ~allocations map context o_label drawable =
      let open Comp.Syntax in
      of_drawable_cont ~allocations
        ~of_shader ~of_object ~of_redirection ~of_textures ~of_data_raw
        map context drawable ~cont:(fun context shader objects ->
            let context = Context.record_drawable_with_label context o_label (Normal {shader; objects}) in
            ret context
          )
    and of_drawable_internal ~allocations map context drawable (* : context * shader * obj *) =
      let open Comp.Syntax in
      of_drawable_cont ~allocations
        ~of_shader ~of_object ~of_redirection ~of_textures ~of_data_raw
        map context drawable ~cont:(fun context shader objects ->
            ret (context, shader, objects)
          )
    and of_object ~allocations ~additional_uniforms map context parent_shader
        ({ transforms; attributes;
           textures; redirection; draw_options; uniforms; }, data) = let open Comp.Syntax in
      let+ (map, context, in_fbo) = Comp.fold ~init:(map,context,None) ~f:(fun (map,context,fbo) redirection ->
          let+ (map, context, fbo') = of_redirection ~allocations map context parent_shader redirection in
          let fbo = match fbo with None -> fbo' | Some v -> Some v in ret (map, context, fbo)) redirection in
      let context, draw_options = Context.ensure_draw_options context draw_options in
      let uniforms = List.map ~f:of_uniform uniforms in
      let uniforms = additional_uniforms @ uniforms in
      let+ texture_data = of_textures ~allocations map context parent_shader textures in
      let+ model,data = of_data ~allocations context parent_shader transforms attributes data in
      let obj = {
        model; uniforms; data; texture_data; in_fbo;
      } in
      ret (context, [`DrawOption draw_options; `Obj obj])
    and of_data_raw ~allocations context parent_shader transforms attributes
        (data: [> `Model of (string * GenericShader.Types.Data.Numeric.data) list * Object.geometry_id
               | `RawData of (string * Object.T.data) list ])
        translate_command =
      let open Comp.Syntax in
      let store_loaded_data_non_model loaded_data data_count attribute_data shader_data =
        let attribute_data = List.fold attribute_data ~init:StringMap.empty ~f:(fun map (name,data) ->
            StringMap.set map ~key:name ~data) in
        let model_data, loaded_data = List.partition_map loaded_data ~f:(fun (name, data) ->
            match name with "tex_coords" | "vertex_normals" | "vertex_position" -> `Fst (name,data)
                          | _ -> `Snd (name, data)) in
        List.iter loaded_data ~f:(fun (name, vbo) ->
            match StringMap.find attribute_data name with
            | None -> ()
            | Some data ->
              Hashtbl.update context.raw_data_cache (data, data_count)
                ~f:(function Some v -> v | None -> vbo)
          );
        store_attribute_data allocations shader_data;
        model_data
      in
      let store_loaded_data loaded_data data_count attribute_data model_id shader_data =
        let model_data = store_loaded_data_non_model loaded_data data_count attribute_data shader_data in
        Hashtbl.update context.geometry_cache model_id ~f:(function Some v -> v | None -> model_data);
        Hashtbl.update context.data_cache (attribute_data, model_id)
          ~f:(function Some v -> v | None -> Shaders.BasicShader.clone_data shader_data )
      in
      let transforms = unwrap_transforms transforms translate_command in
      match data with
      | `RawData loaded_model_data ->
        let attribute_data = List.map attributes ~f:(fun (Object.Attribute { name; value }) -> (name,value)) in
        let data_count =
          List.filter_map loaded_model_data ~f:(fun (_, vl) -> Types.Data.Numeric.data_count vl)
          |> List.max_elt ~compare:Int.compare in
        begin match data_count with
          | None ->
            (* context. *)
            let+ shader_data, (loaded_data, data_count) =
              Shaders.BasicShader.load_data parent_shader ~other_attributes:(loaded_model_data @ attribute_data) in
            ignore (store_loaded_data_non_model loaded_data data_count attribute_data shader_data);
            ret (transforms, shader_data)
        | Some data_count -> 
          let data, raw_data =
            List.partition_map (attribute_data @ loaded_model_data) ~f:(fun (name, data) ->
                match Hashtbl.find context.raw_data_cache (data, data_count) with
                  None -> `Fst (name,data) | Some data -> `Snd (name, data)) in
          let+ shader_data, (loaded_data, data_count) =
            Shaders.BasicShader.load_data
              ~raw_data:(raw_data, data_count)
              parent_shader ~other_attributes:data in
              (* store the loaded data into the context *)
          ignore (store_loaded_data_non_model loaded_data data_count attribute_data shader_data);
          ret (transforms, shader_data)
        end
        | `Model (loaded_model_data, model_id) -> 
          let attribute_data =
            List.map attributes ~f:(fun (Object.Attribute { name; value }) -> (name,value))
            |> List.sort ~compare:(fun (n1, _) (n2, _) -> String.compare n1 n2) in
          let data_count =
            List.filter_map loaded_model_data ~f:(fun (_, vl) -> Types.Data.Numeric.data_count vl)
            |> List.max_elt ~compare:Int.compare in
          (* check if entirety of data is cached *)
          match Hashtbl.find context.data_cache (attribute_data, model_id) with
          (* it is cached - hence we can return the preloaded data  *)
          | Some preloaded -> ret (transforms, preloaded)
          | None ->
            (* not cached - see if we cache some of the component attributes  *)
            match data_count with
            | Some data_count ->
              (* process attribute data to check if any of them are in cache  *)
              let attribute_data, raw_attribute_data =
                List.partition_map (attribute_data) ~f:(fun (name, data) ->
                    match Hashtbl.find context.raw_data_cache (data, data_count) with
                      None -> `Fst (name,data) | Some data -> `Snd (name, data)) in
              (* attempt to load model_data from cache *)
              let loaded_model_data, raw_model_data = match Hashtbl.find context.geometry_cache model_id with
                  None -> loaded_model_data, [] | Some loaded_model_data -> [], loaded_model_data in
              let+ shader_data, (loaded_data, data_count) =
                Shaders.BasicShader.load_data
                  ~raw_data:((raw_model_data @ raw_attribute_data), data_count)
                  parent_shader ~other_attributes:(loaded_model_data @ attribute_data) in
              (* store the loaded data into the context *)
              store_loaded_data loaded_data data_count attribute_data model_id shader_data;
              ret (transforms, shader_data)
            | None -> 
              (* context. *)
              let+ shader_data, (loaded_data, data_count) =
                Shaders.BasicShader.load_data parent_shader
                  ~other_attributes:(loaded_model_data @ attribute_data) in
              store_loaded_data loaded_data data_count attribute_data model_id shader_data;
              ret (transforms, shader_data)
    and of_textures ~allocations map context parent_shader textures =
      let open Comp.Syntax in
      let load_data loaded_data = 
        List.iter loaded_data ~f:(fun (name, (_, loaded_data), params, key) ->
            Hashtbl.update context.texture_cache key ~f:(function
                  Some v -> v | None -> loaded_data
              )) in
      (* unbound textures will initially be populated with all textures, and as we bind the provided
         textures, we can remove them  *)
      let unbound_textures = ref (Shaders.BasicShader.texture_set parent_shader) in
      let+ textures = List.map textures ~f:(fun { name; textures; typ; texture_options } ->
          (* retrieve the corresponding slot on the shader *)
          let field = Shaders.BasicShader.texture_find_binding parent_shader name in
          (* if not present, just ignore *)
          match field with None -> ret None | Some field ->
            let immediate_texture_options = List.map texture_options ~f:(function
                  Object.TextureBaseLevel ValueIndependent n ->
                  Some (Texture.Param.TextureBaseLevel n)
                | Object.TextureMaxLevel ValueIndependent n ->
                  Some (Texture.Param.TextureMaxLevel n)                      
                | Object.TextureBorderColor ValueIndependent v ->
                  Some (Texture.Param.TextureBorderColour v)
                | Object.WrapS wrap_option ->
                  Some (Texture.Param.WrapS (of_wrap_option wrap_option))
                | Object.WrapT wrap_option ->
                  Some (Texture.Param.WrapT (of_wrap_option wrap_option))
                | Object.MinFilter filter_option ->
                  Some (Texture.Param.MinFilter (of_filter_option filter_option))
                | Object.MagFilter filter_option ->
                  Some (Texture.Param.MagFilter (of_filter_option filter_option))
                | _ -> None
              ) |> Option.all in
            match immediate_texture_options with
            | None ->
              (* if not all immediate options present, we can't try and cache it *)
              let+ loaded_textures =
                List.map textures ~f:(fun texture ->
                    Image_synchronizer.get_data texture context.image_manager) |> Comp.all in
              let+ loaded_textures = Option.all loaded_textures
                                     |> Comp.of_option ~error:(Error.of_string "textures not present") in
              unbound_textures := Set.remove !unbound_textures name;
              ret @@ Some (`Right (name, GenericShader.Types.Data.Texture.MkData loaded_textures,
                                   texture_options, textures))
            | Some texture_options ->
              (* all texture options are present - we can check the cache *)
              let texture_state = TextureState.of_texture_parameters_unwrapped texture_options in
              match Hashtbl.find context.texture_cache (texture_state, textures) with
              | Some loaded_textures ->
                (* if present in the cache, we can just reuse the previously loaded textures *)
                unbound_textures := Set.remove !unbound_textures name;
                ret @@ Some (`Middle (name, loaded_textures))
              | None ->
                (* not present in the cache, load the data from the image file and prep to load onto the GPU *)
                let+ textures_data =
                  List.map textures ~f:(fun texture ->
                      Image_synchronizer.get_data texture context.image_manager) |> Comp.all in
                let+ textures_data = Option.all textures_data
                                     |> Comp.of_option ~error:(Error.of_string "textures not present") in
                unbound_textures := Set.remove !unbound_textures name;
                ret @@ Some (`Left (name,
                                    GenericShader.Types.Data.Texture.MkData textures_data,
                                    texture_options, (texture_state, textures)))
        ) |> Comp.all in
      (* drop any textures that are not bound to the shader  *)
      let textures = List.filter_map textures ~f:(fun a -> a) in
      (* extract out the raw textures (textures from redirections + preloaded textures),textures (to be loaded)*)
      let raw_textures, textures = 
        let raw_textures =
          (* take any unbound textures, and try look them up in the context
             (i.e textures that have been bound due to framebuffer rebindings) *)
          !unbound_textures |> Set.to_list
          |> List.filter_map ~f:(fun name ->
              let open Option.Syntax in
              let+ data = StringMap.find map name in
              ret (name, data)) in
        (* partition out the preloaded textures from non-preloaded ones *)
        let preloaded_textures, nonloaded_textures =
          List.partition_map textures ~f:(function
                `Middle v -> `Fst v | `Left v -> `Snd (`Left v) | `Right v -> `Snd (`Right v)) in
        raw_textures @ preloaded_textures, nonloaded_textures in
      (* construct a list of the textures that can be constructed immediately  *)
      let immediate_textures = List.map textures
          ~f:(function `Left (name, data, params, (state,ids)) ->
              let mipmaps_needed = TextureState.mipmaps_needed state in
              Some (name, data, params, mipmaps_needed, (state,ids)) | _ -> None) |> Option.all in
      match immediate_textures with
      | Some textures ->
        (* if all the textures are present - then simply load all the textures onto the GPU now *)
        let+ texture_data, loaded_data =
          Shaders.BasicShader.load_textures parent_shader ~raw_textures ~textures in
        load_data loaded_data;
        store_texture_data allocations texture_data;
        ret (`Direct texture_data)
      | None ->
        (* if the not all textures are present, then offset the storage to a later date *)
        ret (`Deferred (fun (parent_shader,ctx) ->
            let textures, raw_textures = 
              let textures, preloaded_textures = List.partition_map textures ~f:(function
                    `Left (name, data, params, (state,ids)) ->
                    let mipmaps_needed = TextureState.mipmaps_needed state in
                    `Fst (name, data, params, mipmaps_needed, (state,ids))
                  | `Right (name, data, topt, textures) ->
                    let topt = List.map topt ~f:(function
                          Object.TextureBaseLevel ValueIndependent n -> Texture.Param.TextureBaseLevel n
                        | Object.TextureBaseLevel ValueContext f -> Texture.Param.TextureBaseLevel (f ctx)
                        | Object.TextureMaxLevel ValueIndependent n -> Texture.Param.TextureMaxLevel n
                        | Object.TextureMaxLevel ValueContext f -> Texture.Param.TextureMaxLevel (f ctx)
                        | Object.TextureBorderColor ValueIndependent v -> Texture.Param.TextureBorderColour v
                        | Object.TextureBorderColor ValueContext f -> Texture.Param.TextureBorderColour (f ctx)
                        | Object.WrapS wrap_option -> Texture.Param.WrapS (of_wrap_option wrap_option)
                        | Object.WrapT wrap_option -> Texture.Param.WrapT (of_wrap_option wrap_option)
                        | Object.MinFilter filter_option ->
                          Texture.Param.MinFilter (of_filter_option filter_option)
                        | Object.MagFilter filter_option ->
                          Texture.Param.MagFilter (of_filter_option filter_option)
                      ) in
                    let state = TextureState.of_texture_parameters_unwrapped topt in
                    let mipmaps_needed = TextureState.mipmaps_needed state in
                    match Hashtbl.find context.texture_cache (state, textures) with
                    | None -> `Fst (name, data, topt, mipmaps_needed, (state, textures))
                    | Some v -> `Snd (name, v)
                ) in (textures, raw_textures @ preloaded_textures) in
            let+ texture_data, loaded_textures =
              Shaders.BasicShader.load_textures parent_shader ~raw_textures ~textures in
            load_data loaded_textures;
            ret texture_data
          ))
    and of_wrap_option = function
      | Gsdl.TypedParsetree.ClampToBorder -> Texture.Param.ClampToBorder
      | Gsdl.TypedParsetree.ClampToEdge -> Texture.Param.ClampToEdge
      | Gsdl.TypedParsetree.Repeat -> Texture.Param.Repeat
      | Gsdl.TypedParsetree.MirroredRepeat -> Texture.Param.MirroredRepeat
    and of_filter_option = function
      | Gsdl.TypedParsetree.Nearest -> Texture.Param.Nearest
      | Gsdl.TypedParsetree.Linear   -> Texture.Param.Linear
      | Gsdl.TypedParsetree.NearestMipmapNearest -> Texture.Param.NearestMipmapNearest
      | Gsdl.TypedParsetree.NearestMipmapLinear -> Texture.Param.NearestMipmapLinear
      | Gsdl.TypedParsetree.LinearMipmapNearest -> Texture.Param.LinearMipmapNearest
      | Gsdl.TypedParsetree.LinearMipmapLinear -> Texture.Param.LinearMipmapLinear
    and of_shader context (Object.Shader sid) =
      let open Comp.Syntax in
      let+ shader = Shaders.BasicShaderSynchronizer.get_data sid context.shader_manager in
      let+ shader = Comp.of_option ~error:(Error.of_string "could not build shader") shader in
      ret (context,shader)
    and of_redirection ~allocations
        map context parent_shader (Object.Redirection { id; binding_names; depth; stencil; drawable }) =
      let open Comp.Syntax in
      (* we cache the redirection modifiers to reuse fbos *)
      match IntMap.find context.redirection_map id with        
      | None ->
        let textures = List.mapi binding_names
            ~f:(fun ind texture_binding ->
                let res = Shaders.BasicShader.texture_find_binding parent_shader texture_binding in
                Option.bind res ~f:(fun (Shaders.Texture_Types.MkField { texture_type; _ }) ->
                    (match texture_type with
                     | Shaders.Texture_Types.Sampler1D ->
                       Some (ind, texture_binding)
                     | Shaders.Texture_Types.Sampler2D ->
                       Some (ind, texture_binding)
                     | Shaders.Texture_Types.Sampler3D | Shaders.Texture_Types.SamplerCube -> None)))
                       |> List.filter_opt in
        if not (List.length textures > 0 || depth || stencil) then begin
          ret (map, context, None)
        end else begin
          let textures = List.mapi binding_names
              ~f:(fun ind texture_binding ->
                  Printf.printf "looking up %s in [%s]"
                    texture_binding (Map.keys parent_shader.textures |> String.concat ~sep:"; ");
                  let res = Shaders.BasicShader.texture_find_binding parent_shader texture_binding in
                  Option.bind res ~f:(fun (Shaders.Texture_Types.MkField { texture_type; _ }) ->
                      (match texture_type with
                       | Shaders.Texture_Types.Sampler1D ->
                         let texture =
                           Texture.Texture_1D (Texture.Texture1D.create_empty
                                                 context.Context.logical_width) in
                         store_texture allocations texture;
                         Some (ind, texture_binding, texture)
                       | Shaders.Texture_Types.Sampler2D ->
                         let texture = Texture.Texture_2D
                             (Texture.Texture2D.create_empty
                                context.Context.logical_width
                                context.Context.logical_height) in
                         store_texture allocations texture;
                         Some (ind, texture_binding, texture)
                       | Shaders.Texture_Types.Sampler3D | Shaders.Texture_Types.SamplerCube -> None)))
                         |> List.filter_opt in
          let+ (context, shader, objects) = of_drawable_internal ~allocations map context drawable in
          let rbo = match depth,stencil with
              false,false -> None
            | _ ->
              let rbo = (RBO.from_depth_or_stencil ~stencil ~depth
                           ~width:context.Context.logical_width ~height:context.Context.logical_height ()) in
              store_rbo allocations rbo;
              Some rbo in
          let fbo =
            let fbo = FBO.create () in
            store_fbo allocations fbo;
            FBO.bind fbo FBO.Both;
            begin match rbo with None -> () | Some rbo -> FBO.bind_renderbuffer ~should_bind:None fbo rbo end;
            List.iter textures ~f:(fun (binding_pos, texture_name, texture) ->
                Printf.printf "binding texture %s\n" texture_name;
                FBO.bind_texture ~should_bind:false fbo texture binding_pos Both
              );
            fbo in
          let out_fbo, buffer_in = match depth,stencil with
              false,false -> None, None | true, false -> Some (fbo, Depth), Some Depth
            | false, true -> Some (fbo, Stencil), Some Stencil
            | true, true -> Some (fbo, DepthStencil), Some DepthStencil in
          let context =
            Context.update_redirection_map context ~key:id
              ~data:(fbo, List.map textures ~f:(fun (_, tn,t) -> (tn,t)), buffer_in) in
          let map, textures =
            List.fold_map textures
              ~init:map ~f:(fun map (bpos, texture_name, texture) ->
                  StringMap.set map ~key:texture_name ~data:texture, texture) in
          let drawable = Redirecting { id; fbo; shader; objects; textures; rbo; } in
          let context = Context.add_drawable context drawable  in
          ret (map, context, out_fbo)
        end
      | Some (fbo, fbo_textures, buffer_in) ->
        let out_fbo = match buffer_in with None -> None | Some bi -> Some (fbo, bi) in
        let map = List.fold fbo_textures ~init:map ~f:(fun map (texture_name, texture) -> StringMap.set map ~key:texture_name ~data:texture) in
        ret (map, context, out_fbo)

    let compile
      ~logical_width ~logical_height
        ~width ~height  ~shader_manager ~image_manager ~mesh_manager (program: Object.program) =
      let context =
        Context.create
          ~logical_width ~logical_height
          ~width ~height ~shader_manager ~image_manager ~mesh_manager () in
(*       Printf.printf "======================================================\n\
 * %s\n\
 * ======================================================\n" (Object.show_program program); *)
      let allocations = ref [] in
      let map = StringMap.empty in
      Comp.try_with_cleanup (of_program ~allocations map context program)
        ~on_error:(fun _ ->
            Printf.printf "running cleanup\n";
            List.iter !allocations
              ~f:(function
                  | `FBO fbo -> FBO.delete fbo
                  | `RBO rbo -> RBO.delete rbo
                  | `TextureData data -> Shaders.BasicShader.delete_texture_data data
                  | `AttributeData data -> Shaders.BasicShader.delete_data data
                  | `Texture texture -> Texture.delete texture
                )
          )

  end

  module Render = struct
    open Tgl3
    external gl_alpha_func : int -> float -> unit = "gl_alpha_func"
    external gl_alpha_test : unit -> int = "gl_alpha_test"
    external gl_depth_test : unit -> int = "gl_depth_test"
    external gl_stencil_test : unit -> int = "gl_stencil_test"
    let gl_alpha_test = gl_alpha_test ()
    let gl_depth_test = gl_depth_test ()
    let gl_stencil_test = gl_stencil_test ()


    type draw_operation =  [ `Drawable of drawable | `LabelledDrawable of string * drawable ]

    type draw_context = {
      ctx: Object.ctx;
      width: int; height:int;
      logical_width: int; logical_height:int;
      x:int; y:int; view: M4.t; projection: M4.t;
      draw_operation: Shaders.BasicShader.draw_op;
      default_fbo: FBO.t;
    }

    let rec draw_program ~allocations ctx (program: draw_operation list) =
      FBO.bind FBO.default_fbo FBO.Both;
      Gl.clear (Gl.color_buffer_bit lor Gl.depth_buffer_bit lor Gl.stencil_buffer_bit);
      Comp.fold program ~init:() ~f:(draw_draw_operation ~allocations ctx)
    and draw_draw_operation ~allocations ctx () draw_operation =
      match draw_operation with
      | `LabelledDrawable (_, drawable)
      | `Drawable drawable -> draw_drawable ~allocations ctx drawable
    and draw_drawable ~allocations ctx drawable =
      let open Comp.Syntax in
      match drawable with
      | Normal { shader; objects } ->
        let+ () = Comp.fold ~init:() ~f:(draw_object ~allocations ctx shader FBO.default_fbo) objects in
        ret ()
      | Redirecting {  fbo; shader; objects;  textures; _ } ->
        FBO.bind fbo FBO.Both;
        FBO.set_draw_buffers (List.length textures);
        Gl.viewport 0 0 ctx.logical_width ctx.logical_height;
        Gl.scissor 0 0 ctx.logical_width ctx.logical_height;
        Gl.clear (Gl.color_buffer_bit lor Gl.depth_buffer_bit lor Gl.stencil_buffer_bit);
        let+ () = Comp.fold ~init:() ~f:(draw_object ~allocations ctx shader fbo) objects in
        Gl.viewport ctx.x ctx.y ctx.width ctx.height;
        Gl.scissor ctx.x ctx.y ctx.width ctx.height;
        FBO.unset_draw_buffers (List.length textures);
        FBO.bind ctx.default_fbo FBO.Both;
        ret ()
    and draw_object ~allocations ctx shader fbo () obj = match obj with
      | `DrawOption draw_options -> List.iter draw_options ~f:(load_draw_option ctx); Comp.return ()
      | `Obj obj -> draw_obj ~allocations ctx shader fbo obj
    and load_draw_option ctx draw_option =
      let conv_comp_func = function
        | Gsdl.TypedParsetree.Stencil_never -> Gl.never
        | Gsdl.TypedParsetree.Stencil_less -> Gl.less 
        | Gsdl.TypedParsetree.Stencil_lequal -> Gl.lequal
        | Gsdl.TypedParsetree.Stencil_greater -> Gl.greater 
        | Gsdl.TypedParsetree.Stencil_gequal -> Gl.gequal
        | Gsdl.TypedParsetree.Stencil_equal -> Gl.equal 
        | Gsdl.TypedParsetree.Stencil_notequal -> Gl.notequal
        | Gsdl.TypedParsetree.Stencil_always -> Gl.always
      in
      let conv_blend_func = function
        | Gsdl.TypedParsetree.Zero -> Gl.zero
        | Gsdl.TypedParsetree.One -> Gl.one
        | Gsdl.TypedParsetree.Src_color -> Gl.src_color
        | Gsdl.TypedParsetree.One_minus_src_color -> Gl.one_minus_src_color 
        | Gsdl.TypedParsetree.Dst_color -> Gl.dst_color
        | Gsdl.TypedParsetree.One_minus_dst_color -> Gl.one_minus_dst_color 
        | Gsdl.TypedParsetree.Src_alpha -> Gl.src_alpha
        | Gsdl.TypedParsetree.One_minus_src_alpha -> Gl.one_minus_src_alpha 
        | Gsdl.TypedParsetree.Dst_alpha -> Gl.dst_alpha
        | Gsdl.TypedParsetree.One_minus_dst_alpha -> Gl.one_minus_dst_alpha 
        | Gsdl.TypedParsetree.Constant_color -> Gl.constant_color 
        | Gsdl.TypedParsetree.One_minus_constant_color -> Gl.one_minus_constant_color 
        | Gsdl.TypedParsetree.Constant_alpha -> Gl.constant_alpha 
        | Gsdl.TypedParsetree.One_minus_constant_alpha -> Gl.one_minus_constant_alpha 
        | Gsdl.TypedParsetree.Src_alpha_saturate -> Gl.src_alpha_saturate
      in
      match draw_option with
      | Object.AlphaTest b -> (* if b then Gl.enable gl_alpha_test else Gl.disable gl_alpha_test *) ()
      | Object.BlendTest b -> if b then Gl.enable Gl.blend else Gl.disable Gl.blend
      | Object.DepthTest b -> if b then Gl.enable gl_depth_test else Gl.disable gl_depth_test
      | Object.StencilTest b -> if b then Gl.enable gl_stencil_test else Gl.disable gl_stencil_test
      | Object.BlendColor Object.ValueIndependent v4 ->
        Gl.blend_color (V4.x v4) (V4.y v4) (V4.z v4) (V4.w v4)
      | Object.BlendColor Object.ValueContext f -> let v4 = f ctx.ctx in
        Gl.blend_color (V4.x v4) (V4.y v4) (V4.z v4) (V4.w v4)
      | Object.BlendFunction (bf1, bf2) ->
        let bf1 = conv_blend_func bf1 in
        let bf2 = conv_blend_func bf2 in
        Gl.blend_func bf1 bf2
      | Object.StencilFunction (cf1, v1, v2) ->
        let v1 = match v1 with | Object.ValueIndependent v -> v | Object.ValueContext f -> f ctx.ctx in
        let v2 = match v2 with | Object.ValueIndependent v -> v | Object.ValueContext f -> f ctx.ctx in
        let cf1 = conv_comp_func cf1 in
        Gl.stencil_func cf1 v1 v2
      | Object.DepthFunction cf1 ->
        let cf1 = conv_comp_func cf1 in
        Gl.depth_func cf1
      | Object.AlphaFunction (cf1, v1) ->
        let _v1 = match v1 with | Object.ValueIndependent v -> v | Object.ValueContext f -> f ctx.ctx in        
        let _cf1 = conv_comp_func cf1 in
        ()
        (* gl_alpha_func cf1 v1 *)
    and draw_obj ~allocations ctx shader fbo { model; uniforms; data; texture_data; in_fbo } =
      let open Comp.Syntax in
      let model_proj = match model with Immediate data -> "model", data | Deferred f -> "model", f ctx.ctx in
      let uniforms =
        List.map uniforms ~f:(fun (name, value) ->
            match value with Immediate value -> name, value | Deferred f -> name, f ctx.ctx ) in
      let uniforms = model_proj :: uniforms in
      let+ texture_data = match texture_data with
          `Direct td -> ret td | `Deferred f ->
          let+ new_td = f (shader,ctx.ctx) in
          allocations := (`TextureData new_td) :: !allocations;
          ret new_td in
      begin match in_fbo with None -> () | Some (in_fbo, buffer_input) -> begin
          let buffer_input = match buffer_input with
            | Depth -> RBO.DepthComponent | Stencil -> RBO.Stencil | DepthStencil -> RBO.DepthStencil in
          (* redirection buffers to a full logical texture, while our current buffer may be a smaller/larger *)
          FBO.blit_between_framebuffers
            ~w_src:ctx.logical_width ~h_src:ctx.logical_height
            ~x_src:0 ~y_src:0 ~x_dst:ctx.x ~y_dst:ctx.y
            ~from_buffer:in_fbo ~to_buffer:fbo ~w:ctx.width ~h:ctx.height buffer_input;
          FBO.bind fbo FBO.Both;
        end
      end;
      Shaders.BasicShader.draw
        ~view:ctx.view ~projection:ctx.projection
        ~other_uniforms:uniforms
        shader data texture_data ctx.draw_operation
    and draw ~camera_position ~camera_front ~camera_yaw ~camera_pitch
        ~camera_up ~width ~height  ~logical_width ~logical_height
        ~x ~y ~view ~projection ~draw_operation program =
      let ctx = {
        ctx={ camera_position; camera_front; camera_yaw; camera_pitch; };
        logical_width; logical_height;
        width; height; x; y; view; projection; draw_operation;
        default_fbo= FBO.default_fbo;
      } in
      let allocations = ref [] in
      Comp.try_finally (draw_program ~allocations ctx program) ~finally:(fun _ ->
          List.iter !allocations ~f:(fun texture_data ->
              match texture_data with
              | `TextureData texture_data -> Shaders.BasicShader.delete_texture_data texture_data
              | _ -> ()
            )
        )

    let rec print_program ~allocations ctx (program: draw_operation list) =
      (* when printing, we don't care about the output space, draw output to a full screen without black bars *)
      (* update scissor and viewport region to entire area *)
      let logical_width, logical_height = ctx.logical_width, ctx.logical_height in

      Gl.viewport 0 0 logical_width logical_height;
      Gl.scissor 0 0 logical_width logical_height;

      let output_fbo =
        let fbo = FBO.create () in
        let rbo =
          RBO.create ~internal_format:RBO.Depth24Stencil8  ~width:logical_width ~height:logical_height ()
        in
        let tex = Texture.Texture_2D (Texture.Texture2D.create_empty logical_width logical_height) in
        allocations :=
          `Texture tex ::
          `RBO rbo ::
          `FBO fbo :: !allocations;
        FBO.bind_texture ~should_bind:true fbo tex 0 FBO.Both;
        FBO.bind_renderbuffer fbo rbo;
        Gl.depth_mask true;
        Gl.stencil_mask 0xfffffffffffffff;
        FBO.validate_exn fbo;
        Gl.clear (Gl.depth_buffer_bit lor Gl.stencil_buffer_bit lor Gl.color_buffer_bit);
        fbo
      in
      let open Comp.Syntax in
      FBO.bind output_fbo FBO.Both;
      let+ ((), outputs) =
        let ctx = {ctx with height = logical_height; width=logical_width; x=0; y=0; default_fbo=output_fbo} in
        Comp.fold_map program ~init:() ~f:(print_draw_operation ~allocations output_fbo ctx)
      in
      FBO.bind ctx.default_fbo FBO.Both;
      (* finally, restore old viewport and scissor options *)
      Gl.viewport ctx.x ctx.y ctx.width ctx.height;
      Gl.scissor ctx.x ctx.y ctx.width ctx.height;
      ret @@ List.filter_opt outputs
    and print_draw_operation ~allocations out_fbo ctx () draw_operation =
      match draw_operation with
      | `LabelledDrawable (label, drawable) -> print_drawable ~allocations label out_fbo ctx drawable
      | `Drawable drawable ->
        let open Comp.Syntax in
        let+ () = draw_drawable ~allocations ctx drawable in
        Comp.return ((), None)
    and print_drawable ~allocations label out_fbo ctx drawable =
      let open Comp.Syntax in
      match drawable with
      | Normal { shader; objects } ->
        Gl.clear (Gl.color_buffer_bit (* lor Gl.depth_buffer_bit lor Gl.stencil_buffer_bit *) );
        let+ () = Comp.fold ~init:() ~f:(draw_object ~allocations ctx shader out_fbo) objects in
        let fb_contents = FBO.read_framebuffer out_fbo ~x:ctx.x ~y:ctx.y ~w:ctx.width ~h:ctx.height in
        ret ((), Some (label, fb_contents))
      | Redirecting {   shader; objects; textures; _ } ->
        Gl.clear (Gl.depth_buffer_bit lor Gl.stencil_buffer_bit lor Gl.color_buffer_bit);
        FBO.set_draw_buffers (List.length textures);
        let+ () = Comp.fold ~init:() ~f:(draw_object ~allocations ctx shader out_fbo) objects in
        let fb_contents = FBO.read_framebuffer out_fbo ~x:ctx.x ~y:ctx.y ~w:ctx.width ~h:ctx.height in
        FBO.unset_draw_buffers (List.length textures);
        ret ((), Some (label, fb_contents))
    and print  ~camera_position ~camera_front ~camera_yaw ~camera_pitch 
        ~camera_up ~width ~height ~x ~y ~logical_width ~logical_height
        ~view ~projection ~draw_operation program =
      let ctx = {
        ctx={ camera_position; camera_front; camera_yaw; camera_pitch; };
        width; height; logical_width; logical_height;
        x; y; view; projection; draw_operation;
        default_fbo=FBO.default_fbo;
      } in
      let allocations = ref [] in
      Comp.try_finally
        (print_program ~allocations ctx program) ~finally:(fun _ ->
          List.iter !allocations ~f:(fun data ->
              match data with
              | `TextureData data -> Shaders.BasicShader.delete_texture_data data
              | `FBO fbo -> FBO.delete fbo
              | `RBO rbo -> RBO.delete rbo
              | `Texture texture -> Texture.delete texture
            )
        )    

  end

end
