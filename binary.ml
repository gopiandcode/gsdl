(* open Gg *)
open Core
open Geometry
open Utils

let buf_size = 15

external buf_of_vec32 :
  (float, Bigarray.float32_elt, Bigarray.c_layout) Bigarray.Array1.t -> Bin_prot.Common.buf = "%identity"


let parse_file filename =
  let open Comp.Syntax in
  (* let open Option.Syntax in *)
  let+ file =
    try ret @@ Unix.openfile ~mode:[Unix.O_RDONLY] filename with
      Sys_error e -> Comp.fail (Error.of_string e)            
    | Unix.Unix_error (_, e,_) -> (Comp.fail (Error.of_string e))
  in
  let buf = Bigstring.create buf_size in
  let pos = ref 0 in
  let cap = ref 0 in
  (* resets the buffer, moving any unread bytes back to the start
                  V
     [------------*******]
             to
      V
     [*******|-----------]
  *)
  let reset_buffer () =
    let rec loop (t, f) =
      assert (t < f);
      match (f < !cap) with
      | true ->
        let b1 = Bigstring.unsafe_get_uint8 buf ~pos:f in
        Bigstring.set_uint8_exn ~pos:t buf b1;
        loop (t + 1, f + 1)
      | false -> t
    in
    if !pos > 0 then begin
    let cap' = loop (0, !pos) in
    cap := cap';
    pos := 0;
    end;
    ()
  in
  (* prints the buffer *)
  let _print_buffer () =
      let offset = 10 in
    for j = 1 to buf_size / offset + 1  do
      let start = (j - 1) * offset in
      let bound =
        let value = j * offset in
        if value > buf_size then buf_size else value
      in
    for i = start to bound - 1 do
      if i = !pos then
        Printf.printf "  v  "
      else 
        Printf.printf "     ";
      if not (i = buf_size - 1) then Printf.printf " "
    done;
    Printf.printf "\n";
    for i = start to bound - 1 do
      if i < !cap then
        begin
          let b1 = Bigstring.unsafe_get_uint8 buf ~pos:i in
          if i < !pos then Printf.printf " %3d " b1
          else Printf.printf "[%3d]" b1
        end
      else Printf.printf "[---]";
      if not (i = buf_size - 1) then Printf.printf " "
    done;
    Printf.printf "\n";
    done;
  in
  (* ensures the buffer has at least size elems *)
  let ensure_buf size =
    let diff = !cap - !pos in
    if diff <= size then begin
      reset_buffer ();
      assert (!cap - !pos = diff);
      let read_size = try
          Bigstring.read (* ~min_len:(size - (!cap - !pos)) *) file buf ~pos:!cap ~len:(buf_size - !cap)
        with
        Bigstring.IOError (n, _) -> n
      in
      cap := !cap + read_size;
      assert (!cap - !pos >= size);
    end
    else ()
  in
  let read_byte () =
    ensure_buf 1;
    Bin_prot.Read.bin_read_char buf ~pos_ref:pos
  in
  let read_int32 () =
    ensure_buf 4;
    let value = Bin_prot.Read.bin_read_int_32bit buf ~pos_ref:pos in
    value
  in
  let read_vec () =
    let open Bigarray in
    let sz = (4 * 3) in
    ensure_buf sz;
    let array = Array1.create float32 c_layout 3 in
    Bigstring.unsafe_blit
      ~src:buf ~src_pos:!pos
      ~dst:(buf_of_vec32 array) ~dst_pos:0
      ~len:sz;
    pos := !pos + sz;
    let x = array.{0} in
    let y = array.{1} in
    let z = array.{2} in
    Gg.V3.v x y z 
  in
  let _print_vec v =
    Printf.printf "read vector: "; 
    Gg.V3.pp Format.std_formatter v;
    Printf.printf "\n"; 
  in
  let seek_forward n =
    ignore @@ Unix.lseek file n ~mode:Unix.SEEK_CUR;
    cap := !pos;
  in
  seek_forward 80L;
  let no_triangles = read_int32 () in
  let read_triangle () =
    let read_vector  () =
      let vec = read_vec () in
      vec
    in
    let normal = read_vector () in
    let v1 = read_vector () in
    let v2 = read_vector () in
    let v3 = read_vector () in
    (* skip 2 unused bytes after each triangle *)
    let _ =  read_byte () in
    let _ =  read_byte () in
    Triangle.create ~normal v1 v2 v3
  in
  let rec loop count acc =
    if count > 0 then
      let triangle = read_triangle () in
      loop (count - 1) (triangle :: acc)
    else
      (List.rev acc)
  in
  let triangles = loop (no_triangles) [] in
  ret triangles
