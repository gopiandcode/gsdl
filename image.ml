open Core
open Tgl3
open Utils

external gl_luminance : unit -> int = "gl_luminance"
external gl_luminance_alpha : unit -> int = "gl_luminance_alpha"

let gl_luminance = gl_luminance ()
let gl_luminance_alpha = gl_luminance_alpha ()

let stbe e = match e with
  | Ok v -> Comp.return v
  | Error `Msg err -> Comp.fail (Error.of_string err)

let op filename =
  let open Comp.Syntax in
  let+ image = stbe (Stb_image.load filename) in
  ret image

type format =
  | Grey
  | GreyAlpha
  | RGB
  | RGBA
[@@deriving sexp,show]

let format_of_int (channel) = match channel with
  | 1 -> Ok Grey
  | 2 -> Ok GreyAlpha
  | 3 -> Ok RGB
  | 4 -> Ok RGBA
  | _ -> Error (Error.of_string (Printf.sprintf "unknown channel count %d" channel))

let format_to_gl = function
  | Grey -> gl_luminance
  | GreyAlpha -> gl_luminance_alpha
  | RGB -> Gl.rgb
  | RGBA -> Gl.rgba

let format_to_component_count format =
  match format with
  | Grey -> 1
  | GreyAlpha -> 2
  | RGB -> 3
  | RGBA -> 4


type t = {
  format: format;
  width: int;  height: int; 
  data: (int, Bigarray.int8_unsigned_elt, Bigarray.c_layout) Bigarray.Array1.t
}

let write_to_file ~filename {format; width; height; data} =
  Stb_image_write.png filename ~w:width ~h:height ~c:(format_to_component_count format) data



let format_to_string (format:format) =
  match format with
  | Grey -> "Grey"
  | GreyAlpha -> "GreyAlpha"
  | RGB -> "RGB"
  | RGBA -> "RGBA"

let to_string ({ format; width; height; _ }: t) =
  Printf.sprintf "Image.t { format: %s; dimension: %d x %d; }"
    (format_to_string format) width height

let to_gl_element_type (_format: format) =
  Gl.unsigned_byte

let pp (f: Format.formatter) ({ format; width; height; _ }: t) =
  Format.pp_print_string f "Image.t";
  Format.pp_print_space f ();
  Format.pp_open_stag f (Format.String_tag "Image.t");
  Format.pp_print_string f "{";
  Format.pp_print_string f "format:";
  Format.pp_print_string f (format_to_string format);
  Format.pp_print_string f ";";
  Format.pp_print_space f ();
  Format.pp_print_string f "dimension:";
  Format.pp_print_int f width;
  Format.pp_print_string f "x";
  Format.pp_print_int f height;
  Format.pp_close_stag f ();
  Format.pp_print_string f "}"

let of_image image =
  let open Result.Syntax in
  let channels = Stb_image.channels image in
  let+ format = format_of_int channels in
  let width = Stb_image.width image  in
  let height = Stb_image.height image  in
  let data = Stb_image.data image  in
  ret { format; width; height; data; }

let load_file file =
  match Stb_image.load file with
  | Ok image ->
    of_image image
  | Error `Msg error_message ->
    Error (Error.of_string (Printf.sprintf "stb_image error: %s" error_message))



