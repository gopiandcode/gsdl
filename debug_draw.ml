open Gg
open Core
module Graphics = Graphics
module Draw : sig
  type t
  val init : ?dimensions:int * int -> unit -> t
  (* val to_image_coords : t -> Gg.V2.t -> Core.Int.t * Core.Int.t
   * val draw_triangle : t -> Gg.V2.t * Gg.V2.t * Gg.V2.t -> unit
   * val draw_triangles : t -> (Gg.V2.t * Gg.V2.t * Gg.V2.t) list -> unit *)
  val main_loop : t -> (Gg.V2.t * Gg.V2.t * Gg.V2.t) list -> unit
end = struct

  type t = (int * int)

  let init ?(dimensions=(1024,1024)) () =
    Graphics.open_graph (Printf.sprintf " %dx%d" (fst dimensions) (snd dimensions));
    dimensions

  let to_image_coords ((w,h):t) (v: V2.t) =
    let w = Float.of_int w in
    let h = Float.of_int h in
    let x = Float.clamp_exn ~min:0.0 ~max:w (w *. (V2.x v)) in
    let y = Float.clamp_exn ~min:0.0 ~max:h (h *. (V2.y v)) in
    (Int.of_float x (* + Random.int_incl (-3) 3 *), Int.of_float y  (* + Random.int_incl (-3) 3 *))

  let draw_triangle ?(fill=true) ?(outline=true) t (v1,v2,v3) =

    (* let moveto (x,y) = Graphics.moveto x y in
     * let lineto (x,y) = Graphics.lineto x y in *)
    let v1 = to_image_coords t v1 in
    let v2 = to_image_coords t v2 in
    let v3 = to_image_coords t v3 in

    if fill then begin
      Graphics.set_color (Graphics.rgb (Random.int_incl 0 256) (Random.int_incl 0 256) (Random.int_incl 0 256));
      Graphics.fill_poly (Array.of_list [v1;v2;v3])
    end;
    if outline then begin
      Graphics.set_color (Graphics.rgb (Random.int_incl 0 256) (Random.int_incl 0 256) (Random.int_incl 0 256));
      Graphics.draw_poly (Array.of_list [v1;v2;v3])
    end
      
      

  let draw_triangles ?fill ?outline t (ls: (V2.t * V2.t * V2.t) list) =
    Graphics.set_color Graphics.black;
    Graphics.moveto 0 0;
    Graphics.draw_string (Printf.sprintf "no_vertices: %d" (3 * List.length ls));
    List.iter ~f:(draw_triangle ?fill ?outline t) ls


  let main_loop t ls =
    let fill = ref true in
    let outline = ref true in
    try
      while true do
        Graphics.clear_graph ();
        draw_triangles ~fill:!fill ~outline:!outline t ls;
        let st = Graphics.wait_next_event [ Key_pressed ] in
        print_endline (Printf.sprintf "fill: %b, outline: %b" !fill !outline);
        if Char.(st.key = 'o') then outline := not !outline
        else if Char.(st.key = 'f') then fill := not !fill
        else if not Char.(st.key = 'r') then raise Exit;
      done
    with Exit -> ()
  
end
